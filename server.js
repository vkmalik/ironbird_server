'use strict';
require('dotenv').config();
const config = require('config');
const express = require('express');
// const PORT = process.env.NODE_PORT || 80;
// const PORT = process.env.NODE_PORT || 3000;
const PORT = process.env.NODE_PORT || 3500;
const IP = config.get('IP');
const cors = require('cors');
const bodyParser = require('body-parser');
const app = express();

app.use(cors());

app.use(express.static('uploads'));
app.use('', express.static('dist'));


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
require('./app/routes')(app);

const onServerStart = () => {
    const ENVIROINMENT = process.env.NODE_ENV || 'development';
    const message = `Server Listening On Port ${PORT}, ENVIROINMENT=${ENVIROINMENT}`;
    console.log(message);
};

app.listen(PORT, IP, onServerStart);