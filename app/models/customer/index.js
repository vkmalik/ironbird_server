'use strict';

const Customer = require('./customer-master');
const Site = require('./site-master');
const GeneralInvoice = require('./generalInvoices');



module.exports = {
    Customer,
    Site,
    GeneralInvoice
};