'use strict';
const { v4: uuid_v4 } = require('uuid');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = {
    timestamps: true
};



const getRequiredFiledMessage = (filed) => {
    const message = `${filed} Should Not Be Empty`;
    return [true, message];
};

const CustomerSchema = new Schema({
    id: { type: String, default: uuid_v4 },
    SNO: { type: Number, default: 0 },
    companyName: { type: String, default: '', trim: true },
    personName: { type: String, default: '', trim: true },
    address: { type: String, default: '', trim: true },
    deliveryAddress: { type: String, default: '', trim: true },
    city: { type: String, default: '', trim: true },
    state: { type: String, default: '', trim: true },
    pin: { type: String, default: '', trim: true },
    country: { type: String, default: '', trim: true },
    mobile: { type: String, default: '', trim: true },
    email: { type: String, default: '', trim: true },
    gst: { type: String, default: '', trim: true },
    pan: { type: String, default: '', trim: true },
    quotaions: { type: Number, default: 0, trim: true },
    agreements: { type: Number, default: 0, trim: true },
    sites: { type: Number, default: 0, trim: true },
    isDeleted: { type: String, default: false, trim: true }
}, options);


const Customer = mongoose.model('Customer', CustomerSchema);
module.exports = Customer;