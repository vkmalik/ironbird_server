'use strict';
const { v4: uuid_v4 } = require('uuid');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = {
    timestamps: true
};



const getRequiredFiledMessage = (filed) => {
    const message = `${filed} Should Not Be Empty`;
    return [true, message];
};

const SiteSchema = new Schema({
    id: { type: String, default: uuid_v4 },
    clientId: { type: String, default: '' },
    siteName: { type: String, default: '', trim: true },
    personName: { type: String, default: '', trim: true },
    address: { type: String, default: '', trim: true },
    mobile: { type: String, default: '', trim: true },
    email: { type: String, default: '', trim: true },
    personMobile: { type: String, default: '', trim: true },
}, options);


const Site = mongoose.model('Site', SiteSchema);
module.exports = Site;