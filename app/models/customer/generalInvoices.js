'use strict';
const { v4: uuid_v4 } = require('uuid');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = {
    timestamps: true
};



const GeneralInvoiceSchema = new Schema({
    id: { type: String, default: uuid_v4 },
    clientId: { type: String, trim: true, default: '' },
    generalInvoices: { type: Array, trim: true, default: [] }
}, options);


const GeneralInvoice = mongoose.model('GeneralInvoice', GeneralInvoiceSchema);
module.exports = GeneralInvoice;