'use strict';
const mongoose = require('mongoose');
const { v4: uuid_v4 } = require('uuid');
const Schema = mongoose.Schema;
const options = {
    timestamps: true
};



const getRequiredFiledMessage = (filed) => {
    const message = `${filed} Should Not Be Empty`;
    return [true, message];
};

const QuotestionAttachmentSchema = new Schema({
    id: { type: String, default: uuid_v4 },
    quotesId: { type: String, default: '' },
    filenames: { type: String, default: '' },
    descriptions: { type: String, default: '' },
    attachement: { type: {}, default: '', trim: true },
    isDeleted: { type: Boolean, default: false }
}, options);


const QuotestionAttachment = mongoose.model('QuotestionAttachment', QuotestionAttachmentSchema);
module.exports = QuotestionAttachment;