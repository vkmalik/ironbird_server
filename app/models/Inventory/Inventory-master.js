'use strict';
const { v4: uuid_v4 } = require('uuid');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = {
    timestamps: true
};



const getRequiredFiledMessage = (filed) => {
    const message = `${filed} Should Not Be Empty`;
    return [true, message];
};

const InventorySchema = new Schema({
    id: { type: String, default: uuid_v4 },
    supplierName: { type: String, default: '', trim: true },
    selectedStage: { type: String, default: '', trim: true },
    description: { type: String, default: '', trim: true },
    PartDetail: { type: String, default: '', trim: true },
    available: { type: Number, default: 0, trim: true },
    partNumber: { type: String, default: '', trim: true },
    reorder: { type: String, default: '', trim: true },
    // stage: { type: String, default: '', trim: true },
    unit: { type: String, default: '', trim: true },
    unitPrice: { type: String, default: '', trim: true },
}, options);


const Inventory = mongoose.model('Inventory', InventorySchema);
module.exports = Inventory;