'use strict'
const AnnualMaintenanceContract = require("./amc");
const AmcService = require('./amc-services');
const AmcSilverService = require('./amc-silver-service');
const AmcMaintanancesService = require('./maintanance');
const AmcPasswordService = require('./passwordsAmc');
const AmcUpload = require('./amc-uploads');
module.exports = {
    AnnualMaintenanceContract,
    AmcService,
    AmcSilverService,
    AmcMaintanancesService,
    AmcPasswordService,
    AmcUpload
}