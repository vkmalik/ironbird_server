'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const { v4: uuid_v4 } = require('uuid');


const amcSilverServiceSchema = new Schema({
    id: { type: String, default: uuid_v4 },
    serviceType: { type: String, default: '', trim: true },

})

const AmcSilverService = mongoose.model('amcSilverService', amcSilverServiceSchema);
module.exports = AmcSilverService;