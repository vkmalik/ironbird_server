'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const { v4: uuid_v4 } = require('uuid');
const options = {
    timestamps: true
};

const amcMaintananceServiceSchema = new Schema({
    id: { type: String, default: uuid_v4 },
    siteId: { type: String, default: '', trime: true },
    PersonName: { type: String, default: '', trime: true },
    telephone: { type: String, default: '', trime: true },
    compaintTime: { type: String, default: '', trime: true },
    jobid: { type: String, default: '', trime: true },
    SlNo: { type: String, default: '', trime: true },
    siteName: { type: String, default: '', trime: true },
    Depot: { type: String, default: '', trime: true },
    lift_type: { type: String, default: '', trime: true },
    make: { type: String, default: '', trime: true },
    contract: { type: String, default: '', trime: true },
    toData: { type: String, default: '', trime: true },
    building: { type: String, default: '', trime: true },
    work: { type: String, default: '', trime: true },
    receivedDate: { type: String, default: '', trime: true },
    receivedTime: { type: String, default: '', trime: true },
    reportTime: { type: String, default: '', trime: true },
    restorationDate: { type: String, default: '', trime: true },
    restorationTime: { type: String, default: '', trime: true },
    Tech_EngrBrach: { type: String, default: '', trime: true },
    rectification: { type: String, default: '', trime: true },
    faultReport: { type: String, default: '', trime: true },
    Replacement_Repair: { type: String, default: '', trime: true },
    observations_remarks: { type: String, default: '', trime: true },
    employee: { type: String, default: '', trime: true },
    employeeSignature: { type: String, default: '', trime: true },
    WorkDate: { type: String, default: '', trime: true },
    remarks_Feedback: { type: String, default: '', trime: true },
    customerName: { type: String, default: '', trime: true },
    customerSignature: { type: String, default: '', trime: true },
    handDate: { type: String, default: '', trime: true },
    clientAddress: { type: String, default: '', trime: true },
    compaintDate: { type: String, default: '', trime: true },
    month: { type: String, default: '', trime: true },
    amcId: { type: String, default: '', trime: true },
}, options)

const AmcMaintanancesService = mongoose.model('amcMaintananceService', amcMaintananceServiceSchema);
module.exports = AmcMaintanancesService;