'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const { v4: uuid_v4 } = require('uuid');
const options = {
    timestamps: true
};

const amcSchema = new Schema({
    id: { type: String, default: uuid_v4 },
    clientId: { type: String, default: '' },
    siteId: { type: String, default: '' },
    clientAddress: { type: String, default: '' },
    siteAddress: { type: String, default: '' },
    PersonName: { type: String, default: '', trime: true },
    customerName: { type: String, default: '', trime: true },
    siteName: { type: String, default: '', trim: true },
    employee: { type: String, default: '', trime: true },
    dates: { type: String, default: '', trime: true },
    // elevatordescription: { type: String, default: '', trim: true },
    // techicaldata: { type: String, default: '', trim: true },
    elevatortype: { type: String, default: '', trim: true },
    // agreementdate: { type: String, default: '', trim: true },
    jobid: { type: String, default: '', trim: true },
    // quotationnumber: { type: String, default: '', trim: true },
    // quantity: { type: String, default: '', trim: true },
    // invoiceItems: { type: Array, default: '', trim: true },
    // unitprice: { type: String, default: '', trim: true },
    totalPrice: { type: String, default: '', trim: true },
    // description: { type: String, default: '', trim: true },
    fromData: { type: String, default: '', trim: true },
    toData: { type: String, default: '', trim: true },
    servies: { type: String, default: '', trim: true },
    serviesPoint: { type: Array, default: [], trim: true },
    serviesPoint0: { type: Array, default: [], trim: true },
    serviesPoint1: { type: Array, default: [], trim: true },
    // payment_conditions: { type: Array, default: '', trim: true },
    amcid: { type: String, default: '', trim: true },
    mobile: { type: String, default: '', trim: true },
    amcNo: { type: String, default: '', trim: true },
    amcPrefix: { type: String, default: '', trim: true },
    amcAmount: { type: String, default: "0", trim: true },
    amcTotalAmount: { type: Number, default: 0, trim: true },
    switch: { type: Boolean, default: true, trim: true }
}, options)


const Amc = mongoose.model('amc', amcSchema);
module.exports = Amc;