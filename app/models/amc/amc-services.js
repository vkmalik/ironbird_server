'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const { v4: uuid_v4 } = require('uuid');
const options = {
    timestamps: true
};




const amcServiceSchema = new Schema({
    id: { type: String, default: uuid_v4 },
    type: { type: String, default: '', trim: true },
    serviesPoint: { type: Array, default: [], trim: true },
    serviesPoint0: { type: Array, default: [], trim: true },
    serviesPoint1: { type: Array, default: [], trim: true }
}, options)


const AmcService = mongoose.model('amcService', amcServiceSchema);
module.exports = AmcService;