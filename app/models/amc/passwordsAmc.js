'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const { v4: uuid_v4 } = require('uuid');


const amcPasswordsServiceSchema = new Schema({
    id: { type: String, default: uuid_v4 },
    siteId: { type: String, default: '', trime: true },
    passType: { type: String, default: '', trime: true },
    passwords: { type: String, default: '', trime: true },
})

const AmcPasswordService = mongoose.model('amcPasswordervice', amcPasswordsServiceSchema);
module.exports = AmcPasswordService;