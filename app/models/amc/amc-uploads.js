'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const { v4: uuid_v4 } = require('uuid');
const options = {
    timestamps: true
};




const amcUploadSchema = new Schema({
    uid: { type: String, default: uuid_v4 },
    amcId: { type: String, default: '' },
    filenames: { type: String, default: '' },
    descriptions: { type: String, default: '' },
    attachement: { type: {}, default: '', trim: true },
    isDeleted: { type: Boolean, default: false }
}, options);


const AmcUpload = mongoose.model('amcUpload', amcUploadSchema);
module.exports = AmcUpload;