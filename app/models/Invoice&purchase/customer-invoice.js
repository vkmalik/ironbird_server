'use strict';
const { v4: uuid_v4 } = require('uuid');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = {
    timestamps: true
};



const getRequiredFiledMessage = (filed) => {
    const message = `${filed} Should Not Be Empty`;
    return [true, message];
};

const CustomerInvoiceSchema = new Schema({
    id: { type: String, default: uuid_v4 },
    customerInvoiceId: { type: String, default: '', trim: true },
    customerInvoiceDate: { type: String, default: '', trim: true },
    supplierAddress: { type: String, default: '', trim: true },
    InvoiceItems: { type: Array },
    taxPercent: { type: Number, default: 0, trim: true },

    isDeleted: { type: String, default: false, trim: true }
}, options);


const customerInvoice = mongoose.model('customerInvoice', CustomerInvoiceSchema);
module.exports = customerInvoice;