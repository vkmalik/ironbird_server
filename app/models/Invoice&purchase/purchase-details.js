'use strict';
const { v4: uuid_v4 } = require('uuid');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = {
    timestamps: true
};



const getRequiredFiledMessage = (filed) => {
    const message = `${filed} Should Not Be Empty`;
    return [true, message];
};

const PurchaseSchema = new Schema({
    id: { type: String, default: uuid_v4 },
    purchaseId: { type: String, default: '', trim: true },
    purchaseDate: { type: String, default: '', trim: true },
    supplierAddress: { type: String, default: '', trim: true },
    InvoiceItems: { type: Array },
    taxPercent: { type: Number, default: 0, trim: true },

    isDeleted: { type: String, default: false, trim: true }
}, options);


const purchase = mongoose.model('purchase', PurchaseSchema);
module.exports = purchase;