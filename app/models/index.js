'use strict';

const db = require('./db');
const User = require('./user');
const UserProfiles = require('./profiles');
const Blog = require('./blog');
const Contact = require('./contact.js');
const Admin = require('./admin');
const Customers = require('./customer');
const Creatjobs = require('./jobs');
const Suppliers = require('./suppliers');
const Quotestion = require('./quotestion');
const Inventory = require('./Inventory');
const InvoicePurchase = require('./Invoice&purchase');
const Amc = require('./amc');

module.exports = {
	db,
	User,
	UserProfiles,
	Blog,
	Contact,
	Admin,
	Customers,
	Creatjobs,
	Suppliers,
	Inventory,
	Quotestion,
	InvoicePurchase,
	Amc
};