'use strict'
const Creatjob = require("./createjobs");
const CreatAttachment = require("./attachments");
const JobStageMaintenance = require('./stage-details');
const JobApproval = require('./approvals')
const WorkPerformance = require('./workPerformance');
const JobDrawings = require('./drawing-details.js');
const JobStageDetails = require('./stages')
const JobMaterialStage = require('./material-stages');
const HandOver = require('./hand-over');
const HandOverTest = require('./hand-over-test');
const Acceptance = require('./acceptance')
const BalanceSheet = require('./balancesheet')
const InvoiceChallan = require('./invoicesAndChallans')

const JobDetails = require('./jobDetails');

module.exports = {
    Creatjob,
    CreatAttachment,
    JobStageMaintenance,
    JobApproval,
    WorkPerformance,
    JobDrawings,
    JobStageDetails,
    JobMaterialStage,
    HandOver,
    Acceptance,
    BalanceSheet,
    HandOverTest,
    JobDetails,
    InvoiceChallan
}