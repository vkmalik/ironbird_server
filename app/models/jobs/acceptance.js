'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const { v4: uuid_v4 } = require('uuid');


const acceptance = new Schema({
    id: { type: String, default: '' },
    clientId: { type: String, default: '' },
    siteId: { type: String, default: '' },

    clientAddress: { type: String, default: '' },
    siteAddress: { type: String, default: '' },
    PersonName: { type: String, default: '', trime: true },
    dates: { type: String, default: '', trime: true },
    employee: { type: String, default: '', trime: true },
    customerName: { type: String, default: '', trime: true },
    siteName: { type: String, default: '', trim: true },
    PersonName: { type: String, default: '', trime: true },
    employee: { type: String, default: '', trime: true },
    dates: { type: String, default: '', trime: true },
    elevatortype: { type: String, default: '', trim: true },
    // agreementdate: { type: String, default: '', trim: true },
    jobid: { type: String, default: '', trim: true },
    // quotationnumber: { type: String, default: '', trim: true },
    // quantity: { type: String, default: '', trim: true },
    // invoiceItems: { type: Array, default: '', trim: true },
    // unitprice: { type: String, default: '', trim: true },
    totalPrice: { type: String, default: '', trim: true },
    // description: { type: String, default: '', trim: true },
    fromData: { type: String, default: '', trim: true },
    toData: { type: String, default: '', trim: true },
    // payment_conditions: { type: Array, default: '', trim: true }
})


const Acceptance = mongoose.model('Acceptance', acceptance);
module.exports = Acceptance;