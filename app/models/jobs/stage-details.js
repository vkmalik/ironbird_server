'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const { v4: uuid_v4 } = require('uuid');
const options = {
    timestamps: true
};

const jobStageSchema = new Schema({
    id: { type: String, default: uuid_v4 },
    stage: { type: String, default: '', trime: true },
    jobId: { type: String, default: '', trime: true },
    jobNo: { type: String, default: '', trime: true },
    handover: { type: Array, default: [], trim: true },
    rectification: { type: Array, default: [], trim: true },
    governor: { type: Array, default: [], trim: true },
    controller: { type: Array, default: [], trim: true },
    hoistway: { type: Array, default: [], trim: true },
    landing: { type: Array, default: [], trim: true },
    car: { type: Array, default: [], trim: true },
    pit: { type: Array, default: [], trim: true },
    operations: { type: Array, default: [], trim: true },
    owners: { type: Array, default: [], trim: true },
    signatures: { type: Array, default: [], trim: true },
}, options)


const JobStageMaintenance = mongoose.model('JobStageMaintenance', jobStageSchema);
module.exports = JobStageMaintenance;