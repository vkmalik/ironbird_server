'use strict';
const mongoose = require("mongoose");
const { v4: uuid_v4 } = require('uuid');

const Schema = mongoose.Schema;
const options = {
    timestamps: true
};

const invoiceAndChallanSchema = new Schema({
    jobId: { type: String, trim: true, default: '' },
    id: { type: String, default: uuid_v4 },
    customerName: { type: String, default: '' },
    clientId: { type: String, default: '' },
    jobNo: { type: String, trim: true, default: '' },
    stage1: { type: Array, trim: true, default: [] },
    stage2: { type: Array, trim: true, default: [] },
    stage3: { type: Array, trim: true, default: [] },
    stageCommissioning: { type: Array, trim: true, default: [] }
}, options)


const InvoiceAndChallan = mongoose.model('InvoiceAndChallan', invoiceAndChallanSchema);
module.exports = InvoiceAndChallan;