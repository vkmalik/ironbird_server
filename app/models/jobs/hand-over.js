'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const { v4: uuid_v4 } = require('uuid');


const handOverSchema = new Schema({
    id: { type: String, default: uuid_v4 },
    jobStages: { type: String, default: '', trime: true },
    jobId: { type: String, default: '', trime: true },
    jobNo: { type: String, default: '', trime: true },
    handoverStages: { type: String, default: '', trime: true },
    handOverData: { type: Array, default: '', trim: true }
})


const HandOverSchema = mongoose.model('handOverSchema', handOverSchema);
module.exports = HandOverSchema;