'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const { v4: uuid_v4 } = require('uuid');
const options = {
    timestamps: true
};

const jobStageDetailsSchema = new Schema({
    id: { type: String, default: uuid_v4 },
    jobStages: { type: String, default: '', trime: true },
    jobId: { type: String, default: '', trime: true },
    jobNo: { type: String, default: '', trime: true },
    stage1Data: { type: Array, default: [], trim: true },
    stage2Data: { type: Array, default: [], trim: true },
    stage3Data: { type: Array, default: [], trim: true },
    comment1: { type: String, default: '', trime: true },
    comment2: { type: String, default: '', trime: true },
    comment3: { type: String, default: '', trime: true },
    // signatures: { type: Array, default: '', trim: true }
}, options)


const jobStageDetails = mongoose.model('jobStageDetails', jobStageDetailsSchema);
module.exports = jobStageDetails;