'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const { v4: uuid_v4 } = require('uuid');


const jobMaterialStageSchema = new Schema({
    id: { type: String, default: uuid_v4 },
    jobStages: { type: String, default: '', trime: true },
    jobId: { type: String, default: '', trime: true },
    jobNo: { type: String, default: '', trime: true },
    materialStageData: { type: Array, default: '', trim: true }
})


const JobMaterialStage = mongoose.model('jobMaterialStage', jobMaterialStageSchema);
module.exports = JobMaterialStage;