'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const { v4: uuid_v4 } = require('uuid');
const options = {
    timestamps: true
};




const balancesheet = new Schema({
    id: { type: String, default: uuid_v4 },
    dates: { type: String, default: '', trime: true },
    agreementId: { type: String, default: '' },
    agreement_id: { type: String, default: '', trime: true },
    paymentType: { type: String, default: '', trime: true },
    refrenceNumber: { type: String, default: '', trime: true },
    amountCredit: { type: Number, default: 0, trime: true },
    amountDebit: { type: Number, default: 0, trime: true },
    paymentFor: { type: String, default: '', trime: true },
    installmentType: { type: String, default: '', trime: true },
    receivedStatus: { type: Boolean, default: false, trim: true },
    users: { type: Array, default: '', trim: true },
    PersonName: { type: String, default: '', trime: true }
}, options)


const BalanceSheet = mongoose.model('BalanceSheet', balancesheet);
module.exports = BalanceSheet;