'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const { v4: uuid_v4 } = require('uuid');
const options = {
    timestamps: true
};

const jobApprovalSchema = new Schema({
        id: { type: String, default: uuid_v4 },
        jobId: { type: String, default: '' },
        description: { type: String, default: '' },
        attachement: { type: {}, default: '', trim: true },
    },
    options
)

const JobApproval = mongoose.model('jobApproval', jobApprovalSchema);
module.exports = JobApproval;