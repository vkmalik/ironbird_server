'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const { v4: uuid_v4 } = require('uuid');


const creatjobDetailsSchema = new Schema({
    uniqueId: { type: String, default: uuid_v4 },
    clientId: { type: String, default: '' },
    quotationId: { type: String, default: '' },
    agreementId: { type: String, default: '' },
    siteId: { type: String, default: '' },
    location: { type: String, default: '', trim: true },
    companyAddress: { type: String, default: '' },
    jobStage: { type: String, default: 'Stage1' },
    managerEmail: { type: String, default: 'byju@ironbirdelevators.com' },
    siteAddress: { type: String, default: '' },
    customerName: { type: String, default: '', trime: true },
    PersonName: { type: String, default: '', trime: true },
    employee: { type: String, default: '', trime: true },
    dates: { type: String, default: '', trime: true },
    siteName: { type: String, default: '', trim: true },
    mobile: { type: String, default: '', trim: true },
    elevatordescription: { type: String, default: '', trim: true },
    techicaldata: { type: String, default: '', trim: true },
    elevatortype: { type: String, default: '', trim: true },
    agreementdate: { type: String, default: '', trim: true },
    jobDetailid: { type: String, default: '', trim: true },
    quotationnumber: { type: String, default: '', trim: true },
    agreementnumber: { type: String, default: '', trim: true },
    quantity: { type: String, default: '', trim: true },
    invoiceItems: { type: Array, default: '', trim: true },
    unitprice: { type: String, default: '', trim: true },
    totalPrice: { type: String, default: '', trim: true },
    description: { type: String, default: '', trim: true },
    SlNo: { type: String, default: '', trim: true },
    payment_conditions: { type: Array, default: '', trim: true },
    listOfMaintance: { type: Array, default: '', trim: true },
    listOfPrepartoryWork: { type: Array, default: '', trim: true },
    contactNumber: { type: String, default: '', trim: true },
    contactPerson: { type: String, default: '', trim: true },
    fromData: { type: String, default: '', trim: true },
    toData: { type: String, default: '', trim: true },
    employee: { type: String, default: '', trim: true },
    notePoints: { type: Array, default: '', trim: true },
    BankAccountNumber: { type: String, default: '', trim: true },
    IFSCCode: { type: String, default: '', trim: true },
    accountHolderName: { type: String, default: '', trim: true },
    companyBankAccountName: { type: String, default: '', trim: true },
    companyBankBranchName: { type: String, default: '', trim: true },
    companyPANNumber: { type: String, default: '', trim: true },
    companyGSTNumber: { type: String, default: '', trim: true },
    MarketingStaffcontactNumber: { type: String, default: '', trim: true },
    MarketingStaffPerson: { type: String, default: '', trim: true },
    TermsAndConditions: { type: Array, default: '', trim: true },
    SpecificationList: { type: Object, default: '', trim: true },
    extraPoints: { type: String, default: '', trim: true },
    jobDetailPrefix: { type: String, default: '', trim: true },
    jobDetailNo: { type: Number, default: '0', trim: true },
})


const JobDetails = mongoose.model('JobDetails', creatjobDetailsSchema);
module.exports = JobDetails;