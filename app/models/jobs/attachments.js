'use strict';
const mongoose = require("mongoose");
const { v4: uuid_v4 } = require('uuid');
const Schema = mongoose.Schema;
const options = {
    timestamps: true
};

const creatAttachmentSchema = new Schema({
    id: { type: String, default: uuid_v4 },
    jobId: { type: String, default: '' },
    description: { type: String, default: '' },
    attachement: { type: {}, default: '', trim: true },
}, options)


const CreatAttachmentSchema = mongoose.model('JobDrawings', creatAttachmentSchema);
module.exports = CreatAttachmentSchema;