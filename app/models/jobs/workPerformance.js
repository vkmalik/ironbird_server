'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const { v4: uuid_v4 } = require('uuid');
const options = {
    timestamps: true
};




const workPerformancelSchema = new Schema({
        id: { type: String, default: uuid_v4 },
        jobId: { type: String, default: '' },
        description: { type: String, default: '' },
        employee: { type: String, default: '' },
        dates: { type: String, default: '' },
        userId: { type: String, default: '' }
    },
    options
)


const WorkPerformance = mongoose.model('workPerformancel', workPerformancelSchema);
module.exports = WorkPerformance;