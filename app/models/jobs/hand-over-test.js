'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const { v4: uuid_v4 } = require('uuid');


const handOverTestSchema = new Schema({
    id: { type: String, default: uuid_v4 },
    jobStages: { type: String, default: '', trime: true },
    jobId: { type: String, default: '', trime: true },
    jobNo: { type: String, default: '', trime: true },

    rectification: { type: Array, default: '', trim: true },
    car: { type: Array, default: '', trim: true },
    controllers: { type: Array, default: '', trim: true },
    governor: { type: Array, default: '', trim: true },
    hoistway: { type: Array, default: '', trim: true },
    landing: { type: Array, default: '', trim: true },
    operations: { type: Array, default: '', trim: true },
    owner: { type: Array, default: '', trim: true },
    pit: { type: Array, default: '', trim: true }
})


const HandOverTestSchema = mongoose.model('handOverTestSchema', handOverTestSchema);
module.exports = HandOverTestSchema;