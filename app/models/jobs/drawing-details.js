'use strict';
const mongoose = require("mongoose");
const { v4: uuid_v4 } = require('uuid');
const Schema = mongoose.Schema;
const options = {
    timestamps: true
};




const jobDrawingSchema = new Schema({
    id: { type: String, trim: true, default: uuid_v4 },
    jobId: { type: String, trim: true, default: '' },
    OverShaftHeight: { type: String, trim: true, default: '' },
    PITShaftHeight: { type: String, trim: true, default: '' },
    lift_type: { type: String, trim: true, default: '' },
    selectedFloorNameString: { type: String, trim: true, default: '' },
    selectedFloorsArray: { type: Array, trim: true, default: [] },
    selectedFloorString: { type: Array, trim: true, default: [] },
    machineDepth: { type: String, trim: true, default: '' },
    machineHeight: { type: String, trim: true, default: '' },
    machineWidth: { type: String, trim: true, default: '' },
    switch: { type: Boolean, trim: true, default: false },
    floorRoomDetails: { type: Array, trim: true, default: '' },
    Shaftwidth: { type: String, trim: true, default: '' },
    Shaftdepth: { type: String, trim: true, default: '' },
    shaftHeight: { type: String, trim: true, default: '' },
    doorOpening: { type: String, trim: true, default: '' },
}, options)


const JobDrawingSchema = mongoose.model('JobDrawingSchema', jobDrawingSchema);
module.exports = JobDrawingSchema;