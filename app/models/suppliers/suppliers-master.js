'use strict';
const { v4: uuid_v4 } = require('uuid');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = {
    timestamps: true
};



const getRequiredFiledMessage = (filed) => {
    const message = `${filed} Should Not Be Empty`;
    return [true, message];
};

const SupplierSchema = new Schema({
    id: { type: String, default: uuid_v4 },
    SupplierName: { type: String, trim: true },
    CompanyName: { type: String, trim: true },
    SupplierAddress: { type: String, default: '', trim: true },
    PickupAddress: { type: String, default: '', trim: true },
    ContactNo: { type: String, trim: true },
    EmailId: { type: String, default: '', trim: true },
    Gst: { type: String, trim: true },
    // ESugamNo: { type: Number,  trim: true },
    supplierItems: { type: Array, default: '', trim: true },
}, options);


const Supplier = mongoose.model('Supplier', SupplierSchema);
module.exports = Supplier;