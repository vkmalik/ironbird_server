'use strict';
const { v4: uuid_v4 } = require('uuid');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = {
    timestamps: true
};

const getRequiredFiledMessage = (filed) => {
    const message = `${filed} Should Not Be Empty`;
    return [true, message];
};

const MaintenanceSchema = new Schema({
    id: { type: String, default: uuid_v4 },
    title: { type: String, required: getRequiredFiledMessage('Titel'), trim: true },
    description: { type: String, required: getRequiredFiledMessage('Description'), trim: true },
    isDeleted: { type: Boolean, default: false }
}, options);


const Maintenance = mongoose.model('Maintenance', MaintenanceSchema);
module.exports = Maintenance;