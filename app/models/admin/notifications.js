'use strict';
const mongoose = require('mongoose');
const { v4: uuid_v4 } = require('uuid');
const Schema = mongoose.Schema;
const options = {
    timestamps: true
};

const NotificationSchema = new Schema({
    id: { type: String, default: uuid_v4 },
    stage: { type: String, default: '', trim: true },
    notifications: { type: Array, default: false },
    jobNo: { type: String, default: '', trim: true },
    jobId: { type: String, default: '', trim: true },
    clientName: { type: String, default: '', trim: true }
}, options);


const Notifications = mongoose.model('Notifications', NotificationSchema);
module.exports = Notifications;