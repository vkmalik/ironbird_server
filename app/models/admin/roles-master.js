'use strict';
const mongoose = require('mongoose');
const { v4: uuid_v4 } = require('uuid');
const Schema = mongoose.Schema;
const options = {
    timestamps: true
};



const RoleSchema = new Schema({
    id: { type: String, default: uuid_v4 },
    role: { type: String, default: '' },
    modules: { type: Array, default: '', trim: true },
    modulesOriginal: { type: Object, default: '', trim: true },
    clientdetails: { type: Boolean, default: false, trim: true },
    clientagreement: { type: Boolean, default: false, trim: true },
    clientquotation: { type: Boolean, default: false, trim: true },
    clientinvoices: { type: Boolean, default: false, trim: true },
    quotdetails: { type: Boolean, default: false, trim: true },
    quotAttachments: { type: Boolean, default: false, trim: true },
    attachdetails: { type: Boolean, default: false, trim: true },
    attachAttachments: { type: Boolean, default: false, trim: true },
    attachlist: { type: Boolean, default: false, trim: true },
    attachBalance: { type: Boolean, default: false, trim: true },
    attachInvoices: { type: Boolean, default: false, trim: true },
    jobdetails: { type: Boolean, default: false, trim: true },
    jobAttachments: { type: Boolean, default: false, trim: true },
    jobDrawings: { type: Boolean, default: false, trim: true },
    jobApproals: { type: Boolean, default: false, trim: true },
    jobWork: { type: Boolean, default: false, trim: true },
    jobStages: { type: Boolean, default: false, trim: true },
    jobHand: { type: Boolean, default: false, trim: true },
    jobAcceptance: { type: Boolean, default: false, trim: true },
    jobMaterial: { type: Boolean, default: false, trim: true },
    amcdetails: { type: Boolean, default: false, trim: true },
    amcAttachments: { type: Boolean, default: false, trim: true },
    amcPass: { type: Boolean, default: false, trim: true },
    amcReports: { type: Boolean, default: false, trim: true },
}, options);


const Role = mongoose.model('Role', RoleSchema);
module.exports = Role;