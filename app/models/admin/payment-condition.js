'use strict';
const { v4: uuid_v4 } = require('uuid');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = {
    timestamps: true
};

const getRequiredFiledMessage = (filed) => {
    const message = `${filed} Should Not Be Empty`;
    return [true, message];
};

const PaymentAndConditionSchema = new Schema({
    id: { type: String, default: uuid_v4 },
    Per_centage: { type: Number, required: getRequiredFiledMessage('percentage And Condition'), trim: true },
    paymentAndCondition: { type: String, required: getRequiredFiledMessage('Payment And Condition'), trim: true },
    isDeleted: { type: Boolean, default: false }
}, options);


const PaymentAndCondition = mongoose.model('PaymentAndCondition', PaymentAndConditionSchema);
module.exports = PaymentAndCondition;