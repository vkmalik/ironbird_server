'use strict';
const { v4: uuid_v4 } = require('uuid');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = {
    timestamps: true
};

// const getRequiredFiledMessage = (filed) => {
//     const message = `${filed} Should Not Be Empty`;
//     return [true, message];
// };

const PasswordSchema = new Schema({
    id: { type: String, default: uuid_v4 },
    deportment: { type: String, default: '', trim: true },
    pswtype: { type: String, default: '', trim: true },
    Passwords: { type: String, default: '', trim: true },
}, options);


const Passwords = mongoose.model('Passwords', PasswordSchema);
module.exports = Passwords;