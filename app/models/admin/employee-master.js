'use strict';
const { v4: uuid_v4 } = require('uuid');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = {
    timestamps: true
};

const getRequiredFiledMessage = (filed) => {
    const message = `${filed} Should Not Be Empty`;
    return [true, message];
};

const EmployeeSchema = new Schema({
    id: { type: String, default: uuid_v4 },
    userId: { type: String, default: '', trim: true },
    roles: { type: String, default: '', trim: true },
    S_NO: { type: String, default: '', trim: true },

    isActive: { type: Boolean, default: true },
    password: { type: String, trim: true },

    firstName: { type: String, default: '', trim: true },
    lastName: { type: String, default: '', trim: true },

    permanentAddr: { type: String, default: '', trim: true },
    permanentCity: { type: String, default: '', trim: true },
    permanentState: { type: String, default: '', trim: true },
    permanentPin: { type: String, default: '', trim: true },
    permanentCountry: { type: String, default: '', trim: true },

    tempAddr: { type: String, default: '', trim: true },
    tempCity: { type: String, default: '', trim: true },
    tempState: { type: String, default: '', trim: true },
    tempPin: { type: String, default: '', trim: true },
    tempCountry: { type: String, default: '', trim: true },

    mobNumber: { type: String, default: '', trim: true },
    altMobNumber: { type: String, default: '', trim: true },

    email: { type: String, default: '', trim: true },
    altEmail: { type: String, default: '', trim: true },

    gender: { type: String, default: '', trim: true },
    marriedStatus: { type: String, default: '', trim: true },

    uploadPic: { type: Array, default: [], trim: true },
    uploadGovtId: { type: Array, default: [], trim: true },
    uploadResume: { type: Array, default: [], trim: true },

    companyEmail: { type: String, default: '', trim: true },
    companyMob: { type: String, default: '', trim: true },

    joinDate: { type: String, default: '', trim: true },
    dOB: { type: String, default: '', trim: true },
    terminationDate: { type: String, default: '', trim: true },
    probationDate: { type: String, default: '', trim: true },

    department: { type: String, default: '', trim: true },
    position: { type: String, default: '', trim: true },

    bankAccountName: { type: String, default: '', trim: true },
    bankAccountNumber: { type: String, default: '', trim: true },
    bankName: { type: String, default: '', trim: true },
    ifscCode: { type: String, default: '', trim: true },

    experence: { type: String, default: '', trim: true },
    designation: { type: String, default: '', trim: true },
    performance: { type: String, default: '', trim: true },
    feedback: { type: String, default: '', trim: true },
    inAndOutEmployee: { type: String, default: 'Inside', trim: true },
    isDeleted: { type: Boolean, default: false, trim: true },
}, options);



const Employee = mongoose.model('Employee', EmployeeSchema);
module.exports = Employee;