'use strict';
const { v4: uuid_v4 } = require('uuid');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = {
    timestamps: true
};

const getRequiredFiledMessage = (filed) => {
    const message = `${filed} Should Not Be Empty`;
    return [true, message];
};

const PreparatorySchema = new Schema({
    id: { type: String, default: uuid_v4 },
    work: { type: String, required: getRequiredFiledMessage('Work'), trim: true },
    isDeleted: { type: Boolean, default: false }
}, options);


const Preparatory = mongoose.model('Preparatory', PreparatorySchema);
module.exports = Preparatory;