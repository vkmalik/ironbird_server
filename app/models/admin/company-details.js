'use strict';
const mongoose = require('mongoose');
const { v4: uuid_v4 } = require('uuid');
const Schema = mongoose.Schema;
const options = {
    timestamps: true
};

const getRequiredFiledMessage = (filed) => {
    const message = `${filed} Should Not Be Empty`;
    return [true, message];
};

const CompanySchema = new Schema({
    id: { type: String, default: uuid_v4 },
    companyEmail: { type: String, required: getRequiredFiledMessage('companyEmail'), trim: true },
    name: { type: String, default: '', trim: true },
    yearOfEstablished: { type: String, default: '', trim: true },
    designation: { type: String, default: '', trim: true },
    contactNumber: { type: String, default: '', trim: true },
    contactPersonEmail: { type: String, default: '', trim: true },
    comapanyState: { type: String, default: '', trim: true },
    companyAddress: { type: String, default: '', trim: true },
    companyPinCode: { type: String, default: '', trim: true },
    webSideURL: { type: String, default: '', trim: true },
    companyHeadNumber: { type: String, default: '', trim: true },
    noOfEmployess: { type: String, default: '', trim: true },
    companyLogo: { type: Array, default: '', trim: true },
    terminationDate: { type: String, default: '', trim: true },
    probationDate: { type: String, default: '', trim: true },
    companyBankAccountName: { type: String, default: '', trim: true },
    companyBankBranchName: { type: String, default: '', trim: true },
    BankAccountNumber: { type: String, default: '', trim: true },
    IFSCCode: { type: String, default: '', trim: true },
    accountHolderName: { type: String, default: '', trim: true },
    companyRegistrationNumber: { type: String, default: '', trim: true },
    companyPANNumber: { type: String, default: '', trim: true },
    companyCGSTNumber: { type: String, default: '', trim: true },
    companyIGSTNumber: { type: String, default: '', trim: true },
    companySGSTNumber: { type: String, default: '', trim: true },
    companyGSTNumber: { type: String, default: '', trim: true },
    companyISONumber: { type: String, default: '', trim: true },
    companyHeadPANNumber: { type: String, default: '', trim: true },
    companyRegistrationDigital: { type: Array, default: '', trim: true },
    companyPANDigital: { type: Array, default: '', trim: true },
    companyCGSTDigital: { type: Array, default: '', trim: true },
    companyIGSTDigital: { type: Array, default: '', trim: true },
    companySGSTDigital: { type: Array, default: '', trim: true },
    companyGSTDigital: { type: Array, default: '', trim: true },
    companyISODigital: { type: Array, default: '', trim: true },
    companyHeadPANDigital: { type: Array, default: '', trim: true },
    companyDigital: { type: Array, default: '', trim: true },
    companyCEODigitalSignature: { type: Array, default: '', trim: true },
    companyHrDigitalSignature: { type: Array, default: '', trim: true },
    BankAccountNumber: { type: String, default: '', trim: true },
    IFSCCode: { type: String, default: '', trim: true },
    accountHolderName: { type: String, default: '', trim: true },
    GST: { type: Number, trim: true },
    SGST: { type: Number, trim: true },
    CGST: { type: Number, trim: true },
    IGST: { type: Number, trim: true },
    salesBillGenNo: { type: String, trim: true },
    purBillGenNo: { type: String, trim: true },
    quotationNo: { type: Number, trim: true },
    jobNo: { type: Number, trim: true },
    isActive: { type: Boolean, default: true },
    isDeleted: { type: Boolean, default: false }
}, options);


const Company = mongoose.model('Company', CompanySchema);
module.exports = Company;