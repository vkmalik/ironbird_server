'use strict';

const Company = require('./company-details');
const State = require('./state-master');
const Elevator = require('./elevator-master');
const Preparatory = require('./preparatory-details');
const Maintenance = require('./maintenance-master');
const TermsAndCondition = require('./terms-condition');
const PaymentAndCondition = require('./payment-condition');
const UserManagment = require('./user-managment');
const Employee = require('./employee-master');
const Quotation = require('./quotestion-master');
const Invoice = require('./invoice-master');
const Purchase = require('./purchase-master');
const Parts = require('./parts-details');
const UserSpecialManagment = require('./SpecialList');
const Passwords = require('./password-details')
const AgreementJob = require('./agreement-jobs');
const Role = require('./roles-master');
const Marketing = require('./marketing');
const Notifications = require('./notifications');

module.exports = {
    Notifications,
    Company,
    State,
    Elevator,
    Preparatory,
    Maintenance,
    TermsAndCondition,
    PaymentAndCondition,
    UserManagment,
    Employee,
    Quotation,
    Invoice,
    Purchase,
    Parts,
    UserSpecialManagment,
    Passwords,
    AgreementJob,
    Role,
    Marketing
};