'use strict';
const { v4: uuid_v4 } = require('uuid');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = {
    timestamps: true
};

const UserSpecialListSchema = new Schema({
    id: { type: String, default: uuid_v4 },
    SpecialList: { type: String, default: '', trim: true },
}, options);


const UserSpecialManagment = mongoose.model('Special', UserSpecialListSchema);
module.exports = UserSpecialManagment;