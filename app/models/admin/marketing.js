'use strict';
const mongoose = require('mongoose');
const { v4: uuid_v4 } = require('uuid');
const Schema = mongoose.Schema;
const options = {
    timestamps: true
};

const MarketingSchema = new Schema({
    listOfImages: { type: Array, default: '', trim: true },
}, options);


const Marketing = mongoose.model('Marketing', MarketingSchema);
module.exports = Marketing;