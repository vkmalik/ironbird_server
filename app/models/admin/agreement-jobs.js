'use strict';
const mongoose = require('mongoose');
const { v4: uuid_v4 } = require('uuid');
const Schema = mongoose.Schema;
const options = {
    timestamps: true
};

const AgreementSchema = new Schema({
    managerEmail: { type: String, default: 'byju@ironbirdelevators.com' },
    id: { type: String, default: uuid_v4 },
    clientId: { type: String, default: '' },
    quotationId: { type: String, default: '' },
    siteId: { type: String, default: '' },
    location: { type: String, default: '', trim: true },
    siteAddress: { type: String, default: '' },
    customerName: { type: String, default: '', trime: true },
    siteName: { type: String, default: '', trim: true },
    agreementdate: { type: String, default: '', trim: true },
    agreementId: { type: String, default: '', trim: true },
    quotationnumber: { type: String, default: '', trim: true },
    totalJobQuantity: { type: String, default: '', trim: true },
    totalJobPrice: { type: String, default: '', trim: true },
    payment_conditions: { type: Array, default: '', trim: true },
    TermsAndConditions: { type: Array, default: '', trim: true },
    agreementPrefix: { type: String, default: '', trim: true },
    agreementNo: { type: Number, default: '0', trim: true },
    PoNumber: { type: Number, default: '0', trim: true },
    JobsList: { type: String, default: '', trim: true },
    Jobs: { type: Array, default: '', trim: true },
    notePoints: { type: Array, default: '', trim: true },
    // BankAccountNumber: { type: String, default: '', trim: true },
    // IFSCCode: { type: String, default: '', trim: true },
    // accountHolderName: { type: String, default: '', trim: true },
    // companyBankAccountName: { type: String, default: '', trim: true },
    // companyBankBranchName: { type: String, default: '', trim: true },
    // companyPANNumber: { type: String, default: '', trim: true },
    // companyGSTNumber: { type: String, default: '', trim: true },
    companyAddress: { type: String, default: '' },
    MarketingStaffcontactNumber: { type: String, default: '', trim: true },
    MarketingStaffPerson: { type: String, default: '', trim: true },
    // listOfMaintance: { type: Array, default: '', trim: true },
    // listOfPrepartoryWork: { type: Array, default: '', trim: true },
    // SpecificationList: { type: Object, default: '', trim: true },
    extraPoints: { type: String, default: '', trim: true },
    contactNumber: { type: String, default: '', trim: true },
    contactPerson: { type: String, default: '', trim: true },
    PersonName: { type: String, default: '', trime: true },
    mobile: { type: String, default: '', trim: true }
}, options);


const AgreementJob = mongoose.model('AgreementJob', AgreementSchema);
module.exports = AgreementJob;