'use strict';
const { v4: uuid_v4 } = require('uuid');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = {
    timestamps: true
};

// const getRequiredFiledMessage = (filed) => {
//     const message = `${filed} Should Not Be Empty`;
//     return [true, message];
// };

const PartsSchema = new Schema({
    id: { type: String, default: uuid_v4 },
    partName: { type: String, default: '', trim: true },
    HSN_Code: { type: String, default: '', trim: true },
    stages: { type: String, default: '', trim: true },
}, options);


const Parts = mongoose.model('Parts', PartsSchema);
module.exports = Parts;