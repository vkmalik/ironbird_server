'use strict';
const { v4: uuid_v4 } = require('uuid');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = {
    timestamps: true
};



const getRequiredFiledMessage = (filed) => {
    const message = `${filed} Should Not Be Empty`;
    return [true, message];
};

const QuotationSchema = new Schema({
    id: { type: String, default: uuid_v4 },
    clientId: { type: String, default: '' },
    siteId: { type: String, default: '' },
    companyAddress: { type: String, default: '', trim: true },
    location: { type: String, default: '', trim: true },
    contactNumber: { type: String, default: '', trim: true },
    contactPerson: { type: String, default: '', trim: true },
    companyName: { type: String, default: '', trim: true },
    invoiceItems: { type: Array, default: '', trim: true },
    // floorDetails: { type: Array, default: '', trim: true },
    // listOfMaintance: { type: Array, default: '', trim: true },
    statuses: { type: String, default: '', trim: true },
    qouteDate: { type: String, default: '', trim: true },
    quoteNo: { type: String, default: '', trim: true },
    siteAddress: { type: String, default: '', trim: true },
    siteName: { type: String, default: '', trim: true },
    TotalQuantity: { type: Number, trim: true },
    // selectedItems: { type: Array, default: '', trim: true },
    TotalPrice: { type: String, trim: true },
    mobile: { type: String, default: '', trim: true },
    listOfPrepartoryWork: { type: Array, default: '', trim: true },
    isDeleted: { type: Boolean, default: false },
    payment_conditions: { type: Array, default: '', trim: true },
    // DrawingItem: { type: Array, default: [], trim: true },
    users: { type: Array, default: '', trim: true },
    // Shaftwidth: { type: String, default: '', trim: true },
    // Shaftdepth: { type: String, default: '', trim: true },
    // PITShaftHeight: { type: String, default: '', trim: true },
    // OverShaftHeight: { type: String, default: '', trim: true },
    // cabineSize: { type: String, default: '', trim: true },
    // selectedItems: { type: Array, trim: true, default: [] },
    qouteDate: { type: String, default: '', trim: true },
    MarketingStaffcontactNumber: { type: String, default: '', trim: true },
    MarketingStaffPerson: { type: String, default: '', trim: true },
    notePoints: { type: Array, default: '', trim: true },
    // BankAccountNumber: { type: String, default: '', trim: true },
    // IFSCCode: { type: String, default: '', trim: true },
    // accountHolderName: { type: String, default: '', trim: true },
    // companyBankAccountName: { type: String, default: '', trim: true },
    // companyBankBranchName: { type: String, default: '', trim: true },
    // companyPANNumber: { type: String, default: '', trim: true },
    // companyGSTNumber: { type: String, default: '', trim: true },
    switch: { type: String, default: '', trim: true },
    SpecificationList: { type: Object, default: '', trim: true },
    TermsAndConditions: { type: Array, default: '', trim: true },
    extraPoints: { type: String, default: '', trim: true },
    quotePrefix: { type: String, default: '', trim: true },
    quoteNum: { type: String, default: '', trim: true },
    extraWorkAmount: { type: Array, default: [], trim: true },
}, options);


const Quotation = mongoose.model('Quotation', QuotationSchema);
module.exports = Quotation;