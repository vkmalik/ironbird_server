'use strict';
const { v4: uuid_v4 } = require('uuid');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = {
    timestamps: true
};



const getRequiredFiledMessage = (filed) => {
    const message = `${filed} Should Not Be Empty`;
    return [true, message];
};

const UserManagmentSchema = new Schema({
    id: { type: String, default: uuid_v4 },
    employeeName: { type: String, required: getRequiredFiledMessage('Employee Name'), trim: true },
    deportment: { type: String, required: getRequiredFiledMessage('Deportment'), trim: true },
    postion: { type: String, required: getRequiredFiledMessage('Postion'), trim: true },
    isActive: { type: Boolean, default: true },
    isDeleted: { type: Boolean, default: false }
}, options);


const UserManagment = mongoose.model('UserManagment', UserManagmentSchema);
module.exports = UserManagment;