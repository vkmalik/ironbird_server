'use strict';
const { v4: uuid_v4 } = require('uuid');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = {
    timestamps: true
};



const getRequiredFiledMessage = (filed) => {
    const message = `${filed} Should Not Be Empty`;
    return [true, message];
};

const TermsAndConditionSchema = new Schema({
    id: { type: String, default: uuid_v4 },
    termsAndCondition: { type: String, required: getRequiredFiledMessage('Terms And Condition'), trim: true },
    isDeleted: { type: Boolean, default: false }
}, options);


const TermsAndCondition = mongoose.model('TermsAndCondition', TermsAndConditionSchema);
module.exports = TermsAndCondition;