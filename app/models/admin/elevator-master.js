'use strict';
const { v4: uuid_v4 } = require('uuid');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = {
    timestamps: true
};

const getRequiredFiledMessage = (filed) => {
    const message = `${filed} Should Not Be Empty`;
    return [true, message];
};

const ElevatorSchema = new Schema({
    id: { type: String, default: uuid_v4 },
    SNO: { type: String, default: '' },
    name: { type: String, required: getRequiredFiledMessage('Name'), trim: true },
    capacity: { type: String, default: '', trim: true },
    modalNumber: { type: String, default: '', trim: true },
    modalName: { type: String, default: '', trim: true },
    warrenty: { type: String, default: '', trim: true },
    isDeleted: { type: Boolean, default: false }
}, options);


const Elevator = mongoose.model('Elevator', ElevatorSchema);
module.exports = Elevator;