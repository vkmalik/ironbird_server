'use strict';
const { v4: uuid_v4 } = require('uuid');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = {
    timestamps: true
};



const getRequiredFiledMessage = (filed) => {
    const message = `${filed} Should Not Be Empty`;
    return [true, message];
};

const StateSchema = new Schema({
    id: { type: String, default: uuid_v4 },
    key: { type: String, required: getRequiredFiledMessage('key Name'), trim: true, unique: true },
    name: { type: String, required: getRequiredFiledMessage('Name'), trim: true, unique: true },
    isDeleted: { type: Boolean, default: false }
}, options);


const State = mongoose.model('State', StateSchema);
module.exports = State;