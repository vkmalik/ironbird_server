'use strict';
const { v4: uuid_v4 } = require('uuid');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = {
    timestamps: true
};

const getRequiredFiledMessage = (filed) => {
    const message = `${filed} Should Not Be Empty`;
    return [true, message];
};

const InvoiceSchema = new Schema({
    id: { type: String, default: uuid_v4 },
    customerName: { type: String, required: getRequiredFiledMessage('Name'), trim: true },
    mobile: { type: String, required: getRequiredFiledMessage('Name'), trim: true },
    address: { type: String, default: '', trim: true },
    pincode: { type: String, default: '', trim: true },
    todayDate: { type: String, default: '', trim: true },
    invoiceItems: { type: Array, default: '', trim: true },
    subTotal: { type: String, default: '', trim: true },
    taxPercent: { type: String, default: '', trim: true },
    tax: { type: String, default: '', trim: true },
    grantTotal: { type: String, default: '', trim: true },
}, options);


const Invoice = mongoose.model('Invoice', InvoiceSchema);
module.exports = Invoice;