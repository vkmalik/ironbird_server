'use strict';
const { v4: uuid_v4 } = require('uuid');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = {
    timestamps: true
};



const getRequiredFiledMessage = (filed) => {
    const message = `${filed} Should Not Be Empty`;
    return [true, message];
};

const LocalProfileSchema = new Schema({
    id: { type: String, default: uuid_v4 },
    userId: { type: String, required: getRequiredFiledMessage('User ID') },
    name: { type: String, required: getRequiredFiledMessage('Name') },
    password: { type: String, required: getRequiredFiledMessage('Password') },
    isEmailVerified: { type: Boolean, default: false },
    provider: { type: String, default: 'local' },
    profilePic: { type: String, default: '' },
    address: { type: String, default: '' },
    otp: String
}, options);


const LocalProfile = mongoose.model('LocalProfile', LocalProfileSchema);
module.exports = LocalProfile;