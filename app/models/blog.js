'use strict';
const { v4: uuid_v4 } = require('uuid');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = {
    timestamps: true
};

const getRequiredFiledMessage = (filed) => {
    const message = `${filed} Should Not Be Empty`;
    return [true, message];
};

const BlogSchema = new Schema({
    id: { type: String, default: uuid_v4 },
    title: { type: String, required: getRequiredFiledMessage('Blog Title'), trim: true },
    url: { type: String, required: getRequiredFiledMessage('Blog URL'), trim: true },
    imageLink: String,
    description: String,
}, options);


const Blog = mongoose.model('Blog', BlogSchema);
module.exports = Blog;