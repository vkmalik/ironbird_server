'use strict';
const express = require('express');
const router = express.Router();
const { Admin, Customers, Quotestion, Creatjobs, Amc } = require('../../models');
const { Creatjob } = Creatjobs;
const { Site, Customer } = Customers;
const { Quotation, AgreementJob, Maintenance } = Admin;
const { AnnualMaintenanceContract } = Amc;
const { AuthServ } = require('../../lib');

function getDateFunction() {
    let date = new Date();
    let day = date.getDate().toString();
    let month0 = date.getMonth() + 1;
    let month = month0.toString();
    let fullYear = date.getFullYear().toString();
    let hours = date.getHours().toString();
    let mini = date.getMinutes().toString();
    let sec = date.getSeconds().toString();
    let year = fullYear.slice(2);
    return { day, date, month, year, hours, mini, sec }
}

function convertToInrFormat(amt) {
    var regExpr = /[₹,]/g;
    var price;
    price = amt;
    if (price[0] === '₹') {
        price = price.replace(regExpr, "");
    }

    var currencySymbol = '₹';
    var result = price.toString().split('.');

    var lastThree = result[0].substring(result[0].length - 3);
    var otherNumbers = result[0].substring(0, result[0].length - 3);
    if (otherNumbers != '')
        lastThree = ',' + lastThree;
    var output = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

    if (result.length > 1) {
        output += "." + result[1];
    } else {
        output += ".00";
    }

    price = currencySymbol + output;
    // console.log(price);
    return price;
}

router.route('/updateFloorsField')
    .put(async(req, res, next) => {
        try {
            const result = await Quotation.find({}).exec();
            result.forEach(async(ele) => {
                if (ele.invoiceItems.length > 0) {
                    ele.invoiceItems.forEach(async(ele0, i) => {
                        if (ele0.selectedItems)

                            if (!ele0.selectedFloorNameString) {
                            let selectedFloorsArray = []
                            let selectedFloorNameString = ''
                            let selectedFloorString = []
                            let length = ele0.selectedItems.length

                            ele0.selectedItems.forEach((ele1, j) => {
                                let obj = {}

                                selectedFloorString.push(ele1.item_text)

                                if (ele1.height == undefined && ele.selectedDoorStatus == undefined) {
                                    obj = {
                                        "item_id": ele1.item_id,
                                        "item_text": ele1.item_text,
                                        "status": 'open',
                                        "height": 0
                                    }
                                } else {
                                    obj = {
                                        "item_id": ele1.item_id,
                                        "item_text": ele1.item_text,
                                        "status": ele1.selectedDoorStatus,
                                        "height": ele1.height
                                    }
                                }

                                if (j == length - 1) {
                                    selectedFloorNameString += ele1.item_text
                                } else {
                                    selectedFloorNameString += ele1.item_text + ', '
                                }

                                selectedFloorsArray.push(obj)


                            });

                            Object.assign(ele0, { selectedFloorsArray, selectedFloorNameString, selectedFloorString })
                        }
                    })
                }
                await Quotation.findOneAndUpdate({ id: ele.id }, { invoiceItems: ele.invoiceItems }).exec();

            })

            res.json('Complete')
        } catch (err) {
            next(err)
        }
    })

router.route('/count')

.get(async(req, res, next) => {
    try {
        const result = await Quotation.find({}).countDocuments()
        res.json(result)
    } catch (err) {
        next(err)
    }
})

router.route('/updateClientDetails/:id')
    .put(async(req, res, next) => {
        const { id } = req.params;
        const { from } = req.body || {}
        const query = { id };
        try {
            let result;
            if (from === 'amc') {
                result = await AnnualMaintenanceContract.findOne(query).exec()
            } else if (from === 'quote') {
                result = await Quotation.findOne(query).exec();
            } else {
                result = await AgreementJob.findOne(query).exec();
            }

            const clientId = result.clientId
            const siteId = result.siteId

            const result0 = await Customer.findOne({ id: clientId }).exec();
            const result1 = await Site.findOne({ id: siteId }).exec();


            const companyName = result0.companyName;
            const siteName = result1.siteName
            const clientAddress = result0.address;
            const siteAddress = result1.address

            if (from === 'amc') {
                const result4 = await AnnualMaintenanceContract.findOneAndUpdate(query, { customerName: companyName, siteName, clientAddress, siteAddress }, { new: true }).exec();
                res.json({ msg: 'success', companyName, siteName, clientAddress, siteAddress })
            } else if (from === 'quote') {
                const result2 = await Quotation.findOneAndUpdate(query, { companyName, siteName }, { new: true }).exec();
                res.json({ msg: 'success', companyName, siteName })
            } else {
                const result3 = await AgreementJob.findOneAndUpdate(query, { customerName: companyName, siteName }, { new: true }).exec();
                res.json({ msg: 'success', companyName, siteName })
            }

        } catch (err) {
            next(err)
        }
    })

router.route('/')

.get(async(req, res, next) => {
    const query = { isDeleted: false };
    try {
        const results = await Quotation.find(query).sort({ quoteNum: 1 }).exec();
        res.json(results);
    } catch (error) {
        next(error);
    }

})

.post(async(req, res, next) => {
    delete req.body.id
    const {
        body
    } = req;
    const query = {};
    try {
        let countCurrent = 1001;
        const { statuses, clientId } = body || {};
        // const users = { userName, userId, statuses, time: new Date() };
        // Object.assign(body, { users });
        // if (!clientId) {
        //     return res.json({ message: "Client Id Must" });
        // }
        await Customer.findOneAndUpdate({ id: clientId }, { $inc: { quotaions: 1 } })
        const quotation = new Quotation(body);
        const result = await quotation.save();
        if (statuses === "Approved & Job Created") {
            result.invoiceItems.forEach(async(data, i) => {
                let count = await Creatjob.find({}).countDocuments();
                const { day, date, month, year, hours, mini, sec } = getDateFunction();
                count = parseInt(count) + parseInt(countCurrent)
                const jobid = `IBE/BNG/${month}-${year}/${count}`;
                const jobPrefix = `IBE/BNG/${month}-${year}/`;
                const jobNo = `${count}`;
                await Customer.findOneAndUpdate({ id: clientId }, { $inc: { agreements: 1 } })
                Object.assign(req.body, {
                    clientId,
                    siteId,
                    quotationnumber: result.quoteNo,
                    customerName: result.companyName,
                    jobid,
                    siteName: result.siteName,
                    invoiceItems: data,
                    quantity: data.qty,
                    unitprice: data.unit,
                    totalPrice: data.totalAmount,
                    description: data.description,
                    payment_conditions: result.payment_conditions,
                    agreementdate: date,
                    listOfPrepartoryWork: result.listOfPrepartoryWork,
                    listOfMaintance: result.listOfMaintance,
                    contactNumber: data.contactNumber,
                    contactPerson: data.contactPerson,
                    elevatortype: data.lift_type,
                    location: result.location,
                    PersonName: result.contactPerson,
                    notePoints: result.notePoints,
                    MarketingStaffPerson: result.MarketingStaffPerson,
                    MarketingStaffcontactNumber: result.MarketingStaffcontactNumber,
                    BankAccountNumber: result.BankAccountNumber,
                    IFSCCode: result.IFSCCode,
                    accountHolderName: result.accountHolderName,
                    companyBankAccountName: result.companyBankAccountName,
                    companyBankBranchName: result.companyBankBranchName,
                    companyPANNumber: result.companyPANNumber,
                    companyGSTNumber: result.companyGSTNumber,
                    jobPrefix,
                    jobNo
                })

                const resultM = await Maintenance.find({}).exec();
                Object.assign(req.body, { listOfMaintance: resultM })

                const creatjob = new Creatjob(req.body);
                const result1 = creatjob.save();
            })
        }
        // body.quotesId = result.id;
        // const quotestionAttachment = new QuotestionAttachment(body);
        // await quotestionAttachment.save();
        res.json(result);
    } catch (error) {
        next(error);
    }

});

router.route('/statuss/:stage')

.get(async(req, res, next) => {
    const { stage } = req.params;
    const query = { "statuses": stage };

    try {
        const results = await Quotation.find(query).exec();

        res.json(results);
    } catch (error) {
        next(error);
    }
})

router.route('/client/:id')

.get(async(req, res, next) => {
    const { id } = req.params;
    const query = { clientId: id };
    try {
        const results = await Quotation.find(query).exec();
        res.json(results);
    } catch (error) {
        next(error);
    }

})

router.route('/:id')

.put(async(req, res, next) => {
    // console.log(req.body)
    const {
        body
    } = req;
    const { id } = req.params;
    try {
        let countCurrent = 1001;
        let count = await Creatjob.find({}).countDocuments();
        let incrementCount = countCurrent + count;
        const results = await Quotation.findOneAndUpdate({ id }, body, { new: true }).exec();
        results.invoiceItems.forEach(async(data, i) => {
            const Jobs = [];
            var total = 0;
            var regExpr = /[₹,]/g;
            var price;
            // console.log(data.isSelected, data.isCreated, 'LOL')
            if (data.isSelected === true && data.isCreated !== 'created') {
                for (var j = 0; j < body.List[i]; j++) {
                    let numberCount = ++incrementCount
                    const { day, date, month, year, hours, mini, sec } = getDateFunction();
                    // console.log(year, 'year........ hahaha')
                    const dt = new Date().toISOString().split('T')[0];
                    const jobid = `IBE/BNG/${month}-${year}/${numberCount}`;
                    const jobPrefix = `IBE/BNG/${month}-${year}/`;
                    const jobNo = `${numberCount}`;
                    await Customer.findOneAndUpdate({ id: results.clientId }, { $inc: { agreements: 1 } });
                    delete req.body._id;
                    const {
                        clientId,
                        siteId,
                        quoteNo: quotationnumber,
                        companyName: customerName,
                        contactNumber: mobile,
                        id: quotationId,
                        siteName,
                        payment_conditions,
                        listOfPrepartoryWork,
                        listOfMaintance,
                        contactPerson: PersonName,
                        notePoints,
                        // BankAccountNumber,
                        // IFSCCode,
                        MarketingStaffPerson,
                        MarketingStaffcontactNumber,
                        extraPoints,
                        // accountHolderName,
                        // companyBankAccountName,
                        // companyBankBranchName,
                        // companyPANNumber,
                        // companyGSTNumber,
                        TermsAndConditions,
                        location,
                        SpecificationList
                    } = results || {};

                    const {
                        qty: quantity,
                        unit: unitprice,
                        totalAmount: totalPrice,
                        description,
                        contactNumber,
                        contactPerson,
                        SlNo,
                        lift_type: elevatortype
                    } = data || {};

                    const getObjData = {
                        clientId,
                        siteId,
                        quotationnumber,
                        customerName,
                        quotationId,
                        agreementdate: dt,
                        siteName,
                        payment_conditions,
                        listOfPrepartoryWork,
                        listOfMaintance,
                        PersonName,
                        mobile,
                        SlNo,
                        notePoints,
                        // BankAccountNumber,
                        // IFSCCode,
                        MarketingStaffPerson,
                        MarketingStaffcontactNumber,
                        // accountHolderName,
                        // companyBankAccountName,
                        // companyBankBranchName,
                        // companyPANNumber,
                        // companyGSTNumber,
                        quantity: '1',
                        unitprice,
                        description,
                        contactNumber,
                        contactPerson,
                        elevatortype,
                        invoiceItems: data,
                        jobid,
                        TermsAndConditions,
                        location,
                        SpecificationList,
                        extraPoints,
                        jobPrefix,
                        jobNo
                    }

                    price = unitprice;
                    if (price[0] === '₹') {
                        price = price.replace(regExpr, "");
                    }

                    if (total !== 0) {
                        if (total[0] === '₹') {
                            total = total.replace(regExpr, "");
                        }
                    }
                    total = parseInt(total) + parseInt(price);
                    total = convertToInrFormat(total);

                    Object.assign(req.body, { totalPrice: unitprice })
                    Object.assign(req.body, getObjData)

                    delete req.body.id;

                    var contactNumber1 = results.contactNumber;
                    var contactPerson1 = results.contactPerson;

                    // console.log(req.body)

                    const resultM = await Maintenance.find({}).exec();
                    Object.assign(req.body, { listOfMaintance: resultM })

                    const creatjob = new Creatjob(req.body);
                    const result1 = await creatjob.save();

                    Jobs.push({ jobid, unitprice, uid: result1.id })

                }

                total = convertToInrFormat(total);
                const { day, date, month, year, hours, mini, sec } = getDateFunction();
                const agreementId = `IBE/BNG/${month}-${year}/${hours}${mini}${sec +i+j}`;
                const agreementPrefix = `IBE/BNG/${month}-${year}/`;
                const agreementNo = `${hours}${mini}${sec +i+j}`;
                Object.assign(req.body, { contactNumber: contactNumber1, contactPerson: contactPerson1, totalJobPrice: total, agreementId, agreementNo, agreementPrefix, Jobs, totalJobQuantity: body.List[i], JobsList: body.List[i] })
                const creatAgreement = new AgreementJob(req.body);
                const resultNw = await creatAgreement.save()

                for (var i = 0; i < Jobs.length; i++) {
                    await Creatjob.findOneAndUpdate({ id: Jobs[i].uid }, { jobAgreementId: resultNw.id, jobAgreementNumber: resultNw.agreementId }, { new: true }).exec();
                }

                // updating quatation to set agreement created
                results.invoiceItems.forEach(data => {
                    if (data.isSelected === true && data.isCreated === undefined) {
                        Object.assign(data, { isCreated: 'created' })
                    }
                })
                const res = await Quotation.findOneAndUpdate({ id }, { invoiceItems: results.invoiceItems }, { new: true }).exec();
            }
        })
        res.json(results);
    } catch (error) {
        next(error);
    }
})

.get(async(req, res, next) => {
    const { id } = req.params;
    const query = { id };
    try {
        const results = await Quotation.find(query).sort({ quoteNum: 1 }).exec();
        res.json(results);
    } catch (error) {
        next(error);
    }

})

.delete(async(req, res, next) => {
    const {
        body
    } = req;
    const { id } = req.params;
    try {
        const result = await Quotation.findOneAndUpdate({ id }, { isDeleted: true }, { new: true }).exec();
        res.json(result);
    } catch (error) {
        next(error);
    }
})

router.route('/company/:siteName')

.get(async(req, res, next) => {
    //console.log(req.params);
    //const { siteNam } = req.params;

    try {
        const result = await Quotation.findOne(req.params).exec();
        res.json(result);
    } catch (error) {
        next(error);
    }
})


module.exports = router;