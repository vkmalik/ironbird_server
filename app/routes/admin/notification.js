'use strict';
const express = require('express');
const router = express.Router();
const { Notifications } = require('../../models/admin');

router.route('/update')
    .post(async(req, res, next) => {
        const { taskId, status, stage, jobNo } = req.body || {}
        try {
            const result = await Notifications.findOne({ jobNo, stage })
            result.notifications.forEach(element => {
                if (element.taskId === taskId) {
                    element.status = status;
                }
            });

            const results = await Notifications.findOneAndUpdate({ jobNo, stage }, { notifications: result.notifications }, { new: true })
            res.json(results)
        } catch (err) {
            next(err)
        }
    })

router.route('/pending')
    .post(async(req, res, next) => {
        const { role } = req.body || {}
        let query = {}
        if (role === 'Admin')
            query = {
                $match: {
                    "notifications.status": 'Pending',
                }
            }
        else
            query = {
                $match: {
                    "notifications.status": 'Pending',
                    "notifications.department": role,
                }
            }

        try {
            const result = await Notifications.aggregate([query,
                {
                    "$project": {
                        notifications: {
                            $filter: {
                                input: "$notifications",
                                as: "text",
                                cond: {
                                    $in: [
                                        "$$text.status", [
                                            'Pending'
                                        ],
                                    ]
                                }
                            }
                        }
                    }
                }
            ]).exec()

            let count = 0
            result.forEach(ele => {
                count += ele.notifications.length
            })

            res.json(count)
        } catch (err) {
            next(err)
        }
    })

router.route('/')
    .post(async(req, res, next) => {
        const { clientName, stage, jobNo, jobId, department, description, taskId } = req.body || {}
        let status = 'Pending';
        try {
            let notifi = { 'description': description, 'taskId': taskId, 'department': department, status }
            const result = await Notifications.findOne({ stage, jobNo, jobId });
            if (result) {
                result.notifications.push(notifi)
                const results = await Notifications.findOneAndUpdate({ stage, jobNo, jobId }, { notifications: result.notifications }, { new: true })
                return res.json(results)
            } else {
                let details = { stage, jobNo, jobId, notifications: notifi, clientName }
                const record = new Notifications(details)
                const results = await record.save();
                return res.json(results)
            }
        } catch (err) {
            next(err)
        }
    })

.get(async(req, res, next) => {
    try {
        let result = await Notifications.find({}).sort({ createdAt: 1 }).sort({ updatedAt: 1 }).exec()
        res.json(result)
    } catch (err) {
        next(err)
    }
})

module.exports = router;