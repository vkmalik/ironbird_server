'use strict';
const express = require('express');
const router = express.Router();
const { User, Admin, UserProfiles } = require('../../models');
const { PasswordServ, TokenServ } = require('../../lib');
const { Employee } = Admin;
const { LocalProfile } = UserProfiles;
const { AuthServ } = require('../../lib');

const fs = require('fs');
const path = require('path');
const path1 = './uploads/employeeAttachments';
const { v4: uuid_v4 } = require('uuid');

const multer = require('multer');
const storage = multer.diskStorage({
    destination: (req, res, cb) => {
        fs.exists(path1, function(exists) {
            if (exists) {
                cb(null, path1);
            } else {
                fs.mkdir(path1, function(err) {
                    if (err) {
                        console.log('Error in folder Creation');
                        cb(null, path1);
                    }
                    cb(null, path1);
                })
            }
        })
    },
    filename: (req, file, cb) => {
        const ext = path.extname(file.originalname);
        const fileName = `${uuid_v4()}${ext}`;
        cb(null, fileName);
    }
});
const upload = multer({ storage });


/*
 * DOB Wish Sending Function
 */

function getDateFunction() {
    let date = new Date();
    let day = date.getDate();
    let month = date.getMonth();
    let fullYear = date.getFullYear();
    month = month === 12 ? 1 : month + 1
    return `${fullYear}-${month}-${day}`
}


(function() {
    const data = getDateFunction();
    Employee.find({ DOB: data }).exec()
        .then(data => {

        })
}());

router.route('/name_id')
    .get(async(req, res, next) => {
        try {
            const result = await Employee.aggregate(
                [{
                    $project: {
                        fullName: { $concat: ["$firstName", " ", "$lastName"] },
                        id: 1,
                        roles: 1,
                        firstName: 1,
                        lastName: 1,
                        userId: 1
                    }
                }]
            ).sort({ fullName: 1 }).exec()

            res.json(result);
        } catch (err) {
            next(err)
        }
    })

router.route('/based_on_role')
    .get(async(req, res, next) => {
        try {
            const result = await Employee.aggregate(
                [{
                        $match: {
                            $or: [
                                { "department": { $eq: "Erection" } },
                                { "department": { $eq: "Maintenance" } }
                            ]
                        }
                    },
                    {
                        $project: {
                            fullName: { $concat: ["$firstName", " ", "$lastName"] },
                            id: 1,
                            roles: 1,
                            firstName: 1,
                            lastName: 1,
                            userId: 1
                        }
                    }
                ]
            ).sort({ fullName: 1 }).exec()

            res.json(result);
        } catch (err) {
            next(err)
        }
    })

router.route('/')

.get(async(req, res, next) => {
    try {
        const results = await Employee.find({}).sort({ S_NO: 1 }).exec();
        res.json(results);
    } catch (error) {
        next(error);
    }

})

.post(async(req, res, next) => {
    delete req.body.id
    const {
        body
    } = req;

    const {
        email,
        firstName,
        lastName,
        role
    } = body;
    const name = `${ firstName } ${ lastName }`
    const query = {
        email
    };
    try {
        const newUser = new User({
            email,
            role: body.department
        });
        const getLocalProfileData = await newUser.save();
        const { id: userId } = getLocalProfileData || "";
        const password = await PasswordServ.hash('test');
        const profileData = {
            name,
            password,
            userId,
            isEmailVerified: true
        };
        const profile = new LocalProfile(profileData);
        await profile.save();
        Object.assign(body, { userId, roles: body.department })
        const employee = new Employee(body);
        const result = await employee.save();
        res.json(result);
    } catch (error) {
        next(error);
    }
});




router.route('/image/:id')
    .put(upload.single('img-file'), async(req, res, next) => {
        const { id } = req.params;
        const attachement = req.file;
        try {
            const result = await Employee.findOneAndUpdate({ userId: id }, { uploadPic: attachement }, { new: true }).exec()
            res.json({ msg: 'success', result })
        } catch (err) {
            next(err)
        }
    })

router.route('/govtID/:id')
    .put(upload.single('govt-file'), async(req, res, next) => {
        const { id } = req.params;
        const attachement = req.file;
        try {
            const result = await Employee.findOneAndUpdate({ userId: id }, { uploadGovtId: attachement }, { new: true }).exec()
            res.json({ msg: 'success', result })
        } catch (err) {
            next(err)
        }
    })

router.route('/resume/:id')
    .put(upload.single('resume-file'), async(req, res, next) => {
        const { id } = req.params;
        const attachement = req.file;
        try {
            const result = await Employee.findOneAndUpdate({ userId: id }, { uploadResume: attachement }, { new: true }).exec()
            res.json({ msg: 'success', result })
        } catch (err) {
            next(err)
        }
    })



router.route('/outside-employee')

.get(async(req, res, next) => {
    const query = { isDeleted: false, inAndOutEmployee: 'Outside' };
    try {
        const results = await Employee.find(query).exec();
        res.json(results);
    } catch (error) {
        next(error);
    }

})


router.route('/:id')

.put(async(req, res, next) => {
    const {
        body
    } = req;
    const { id } = req.params;
    try {
        const res1 = await User.findOneAndUpdate({ id }, { email: body.email, role: body.department }, { new: true });
        if (res1) {
            const res2 = await LocalProfile.findOneAndUpdate({ userId: id }, { name: `${body.firstName} ${body.lastName}` }, { new: true });
            if (res2) {
                const result = await Employee.findOneAndUpdate({ userId: id }, body, { new: true });
                res.json(result);
            }
        }
    } catch (error) {
        next(error);
    }
})

.get(async(req, res, next) => {
    const { id } = req.params;
    const query = { userId: id };
    try {
        const results = await Employee.find(query).exec();
        // console.log(results)
        res.json(results);
    } catch (error) {
        next(error);
    }

})

.delete(async(req, res, next) => {
    const {
        body
    } = req;
    const { id } = req.params;
    try {
        const res1 = await User.findOneAndRemove({ id });
        if (res1) {
            const res2 = await LocalProfile.findOneAndRemove({ userId: id });
            if (res2) {
                const result = await Employee.findOneAndRemove({ userId: id });
                res.json(result);
            }
        }

    } catch (error) {
        next(error);
    }
})




module.exports = router;