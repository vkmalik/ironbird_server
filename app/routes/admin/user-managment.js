'use strict';
const express = require('express');
const router = express.Router();
const { Admin } = require('../../models');
const { UserManagment } = Admin;
const { AuthServ } = require('../../lib');

router.route('/')

.get(async(req, res, next) => {
    const query = { isDeleted: false };
    try {
        const results = await UserManagment.find(query).exec();
        res.json(results);
    } catch (error) {
        next(error);
    }

})

.post(async(req, res, next) => {
    delete req.body.id
    const {
        body
    } = req;
    const query = {};

    try {
        const userManagment = new UserManagment(body);
        const result = await userManagment.save();
        res.json(result);
    } catch (error) {
        next(error);
    }

});


router.route('/:id')

.put(async(req, res, next) => {
    const {
        body
    } = req;
    const { id } = req.params;
    try {
        const result = await UserManagment.findOneAndUpdate({ id }, body, { new: true }).exec();
        res.json(result);
    } catch (error) {
        next(error);
    }
})

.get(async(req, res, next) => {
    const { id } = req.params;
    const query = { id };
    try {
        const results = await UserManagment.find(query).exec();
        res.json(results);
    } catch (error) {
        next(error);
    }

})

.delete(async(req, res, next) => {
    const {
        body
    } = req;
    const { id } = req.params;
    try {
        const result = await UserManagment.findOne({ userId: id });
        res.json(result);
    } catch (error) {
        next(error);
    }
})


module.exports = router;