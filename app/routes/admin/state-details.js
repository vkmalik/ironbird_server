'use strict';
const express = require('express');
const router = express.Router();
const { Admin } = require('../../models');
const { State } = Admin;
const { AuthServ } = require('../../lib');


router.route('/')
    .get(async(req, res, next) => {
        const query = {};

        try {
            const results = await State.find(query).exec();
            res.json(results);
        } catch (error) {
            next(error);
        }

    })

.post(AuthServ.authorize(), async(req, res, next) => {
    delete req.body.id
    const {
        body
    } = req;

    try {
        const state = new State(body);
        const result = await state.save();
        res.json(result);
    } catch (error) {
        next(error);
    }

});

router.route('/:id')

.put(AuthServ.authorize(), async(req, res, next) => {
    const {
        body
    } = req;
    const { id } = req.params;

    try {
        const result = await State.findOneAndUpdate({ id }, body, { new: true }).exec();
        res.json(result);
    } catch (error) {
        next(error);
    }
})


module.exports = router;