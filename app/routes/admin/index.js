'use strict';

const express = require('express');
const router = express.Router({ mergeParams: true });
const companyRouter = require('./company-details');
const statesRouter = require('./state-details');
const elevatorRouter = require('./elevator-details');
const PreparatoryRouter = require('./preparatory-details');
const MaintenanceRouter = require('./maintenance-details');
const TermsAndConditionRouter = require('./terms-conditions');
const PaymentAndConditionRouter = require('./payment-conditions');
const UserManagmentRouter = require('./user-managment');
const EmployeeRouter = require('./employee-details');
const QuotationRouter = require('./quotestion-details');
const InvoiceRouter = require('./invoice-details');
const PurchaseRouter = require('./purchase-details');
const PartsRouter = require('./part-details');
const UserSpecialRouter = require('./specialList');
const PasswordsRouter = require('./password-details');
const AgreementRouter = require('./agreement-jobs');
const RoleRouter = require('./role-details');
const MarketingRouter = require('./marketing');
const Notification = require('./notification');


router.use('/company-details', companyRouter);
router.use('/states', statesRouter);
router.use('/elevator-details', elevatorRouter);
router.use('/preparatory-details', PreparatoryRouter);
router.use('/maintenance-details', MaintenanceRouter);
router.use('/terms-condition', TermsAndConditionRouter);
router.use('/payment-condition', PaymentAndConditionRouter);
router.use('/user-managment', UserManagmentRouter);
router.use('/employee-details', EmployeeRouter);
router.use('/quotation-details', QuotationRouter);
router.use('/invoice-details', InvoiceRouter);
router.use('/purchase-details', PurchaseRouter);
router.use('/part-details', PartsRouter);
router.use('/specialList', UserSpecialRouter);
router.use('/password-details', PasswordsRouter);
router.use('/agreement-jobs', AgreementRouter);
router.use('/roles', RoleRouter);
router.use('/images', MarketingRouter);
router.use('/notifications', Notification);




module.exports = router;