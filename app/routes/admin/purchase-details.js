'use strict';
const express = require('express');
const router = express.Router();
const { Admin } = require('../../models');
const { Purchase } = Admin;
const { AuthServ } = require('../../lib');

router.route('/')

.get(async(req, res, next) => {
    const query = { isDeleted: false };
    try {
        const results = await Purchase.find(query).exec();
        res.json(results);
    } catch (error) {
        next(error);
    }

})

.post(async(req, res, next) => {
    delete req.body.id
    const {
        body
    } = req;
    const query = {};

    try {
        const purchase = new Purchase(body);
        const result = await purchase.save();
        res.json(result);
    } catch (error) {
        next(error);
    }

});


router.route('/:id')

.put(async(req, res, next) => {
    const {
        body
    } = req;
    const { id } = req.params;
    try {
        const result = await Purchase.findOneAndUpdate({ id }, body, { new: true }).exec();
        res.json(result);
    } catch (error) {
        next(error);
    }
})

.get(async(req, res, next) => {
    const { id } = req.params;
    const query = { id };
    try {
        const results = await Purchase.find(query).exec();
        res.json(results);
    } catch (error) {
        next(error);
    }

})

.delete(async(req, res, next) => {
    const {
        body
    } = req;
    const { id } = req.params;
    try {
        const result = await Purchase.findOneAndUpdate({ id }, { isDeleted: true }, { new: true }).exec();
        res.json(result);
    } catch (error) {
        next(error);
    }
})


module.exports = router;