'use strict';
const express = require('express');
const router = express.Router();
const { Admin } = require('../../models');
const { Invoice, Company } = Admin;
const { AuthServ } = require('../../lib');

router.route('/')

.get(async(req, res, next) => {
    const query = { isDeleted: false };
    try {
        const results = await Invoice.find(query).exec();
        res.json(results);
    } catch (error) {
        next(error);
    }

})

// .post(async(req, res, next) => {
//     const {
//         body
//     } = req;
//     const query = {};
//     try {
//         const result1 = await Company.findOneAndUpdate({ id: "9d18edbd-8b16-4ff4-aabd-cb6944971632" }, { $inc: { salesBillGenNo: 1 } }, { new: true}).exec();
//         const invoice = new Invoice(body);
//         const result = await invoice.save();
//         res.json(result);
//     } catch (error) {
//         next(error);
//     }

// });
.post(async(req, res, next) => {
    const {
        body
    } = req;
    const query = {};
    const { id } = req.body.id;
    try {
        await Company.findOneAndUpdate({ id: id }, { $inc: { salesBillGenNo: 1 } }, { new: true }).exec();
        const invoice = new Invoice(body);
        const result = await invoice.save();
        res.json(result);
        // console.log(result);
    } catch (error) {
        next(error);
    }

});


router.route('/:id')

.put(async(req, res, next) => {
    const {
        body
    } = req;
    const { id } = req.params;
    try {
        const result = await Invoice.findOneAndUpdate({ id }, body, { new: true }).exec();
        res.json(result);
    } catch (error) {
        next(error);
    }
})

.get(async(req, res, next) => {
    const { id } = req.params;
    const query = { id };
    try {
        const results = await Invoice.find(query).exec();
        res.json(results);
    } catch (error) {
        next(error);
    }

})

.delete(async(req, res, next) => {
    const {
        body
    } = req;
    const { id } = req.params;
    try {
        const result = await Invoice.findOneAndUpdate({ id }, { isDeleted: true }, { new: true }).exec();
        res.json(result);
    } catch (error) {
        next(error);
    }
})


module.exports = router;