'use strict';
const express = require('express');
const router = express.Router();
const { Admin } = require('../../models');
const { UserSpecialManagment } = Admin;
const { AuthServ } = require('../../lib');

router.route('/')

.get(async(req, res, next) => {
    try {
        const results = await UserSpecialManagment.find({}).sort({ createdAt: 1 }).exec();
        res.json(results);
    } catch (error) {
        next(error);
    }

})

.post(async(req, res, next) => {
    delete req.body.id
    const {
        body
    } = req;
    const query = {};

    try {
        const userSpecialManagment = new UserSpecialManagment(body);
        const result = await userSpecialManagment.save();
        res.json(result);
    } catch (error) {
        next(error);
    }

});


router.route('/:id')

.put(async(req, res, next) => {
    const {
        body
    } = req;
    const { id } = req.params;
    try {
        const result = await UserSpecialManagment.findOneAndUpdate({ id }, body, { new: true }).exec();
        res.json(result);
    } catch (error) {
        next(error);
    }
})

.get(async(req, res, next) => {
    const { id } = req.params;
    const query = { id, isDeleted: false };
    try {
        const results = await UserSpecialManagment.find(query).exec();
        res.json(results);
    } catch (error) {
        next(error);
    }

})

.delete(async(req, res, next) => {
    const { id } = req.params;
    try {
        const result = await UserSpecialManagment.findOneAndRemove({ id }).exec();
        res.json(result);
    } catch (error) {
        next(error);
    }
})


module.exports = router;