'use strict';
const express = require('express');
const router = express.Router();
const { Admin } = require('../../models');
const { Role } = Admin;
const { AuthServ } = require('../../lib');

var multer = require('multer')
var upload = multer({ dest: 'uploads/marketing' })

router.route('/')

.get(async(req, res, next) => {

    try {
        const results = await Role.find({}).sort({ createdAt: 1 }).exec();
        res.json(results);
    } catch (error) {
        next(error);
    }

})

.post(async(req, res, next) => {

    try {
        delete req.body.id
        const creatRoles = new Role(req.body);
        const result = await creatRoles.save()
        res.json(result)
    } catch (error) {
        next(error);
    }

});

router.route('/:id')

.get(async(req, res, next) => {
    const { id } = req.params;
    try {
        const results = await Role.find({ id }).exec();
        res.json(results);
    } catch (error) {
        next(error);
    }

})

.put(async(req, res, next) => {
    const {
        body
    } = req;
    const { id } = req.params;
    try {
        const result = await Role.findOneAndUpdate({ id }, {
            modules: body.original[0],
            modulesOriginal: body.original[1],
            clientdetails: body.clientdetails,
            clientagreement: body.clientagreement,
            clientquotation: body.clientquotation,
            clientinvoices: body.clientinvoices,
            quotdetails: body.quotdetails,
            quotAttachments: body.quotAttachments,
            attachdetails: body.attachdetails,
            attachAttachments: body.attachAttachments,
            attachlist: body.attachlist,
            attachBalance: body.attachBalance,
            attachInvoices: body.attachInvoices,
            jobdetails: body.jobdetails,
            jobAttachments: body.jobAttachments,
            jobDrawings: body.jobDrawings,
            jobApproals: body.jobApproals,
            jobWork: body.jobWork,
            jobStages: body.jobStages,
            jobHand: body.jobHand,
            jobAcceptance: body.jobAcceptance,
            jobMaterial: body.jobMaterial,
            amcdetails: body.amcdetails,
            amcAttachments: body.amcAttachments,
            amcPass: body.amcPass,
            amcReports: body.amcReports,
        }, { new: true }).exec();
        res.json(result);
    } catch (error) {
        next(error);
    }
})

module.exports = router;