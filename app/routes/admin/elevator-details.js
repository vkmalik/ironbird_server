'use strict';
const express = require('express');
const router = express.Router();
const { Admin } = require('../../models');
const { Elevator } = Admin;
const { AuthServ } = require('../../lib');

router.route('/')

.get(async(req, res, next) => {
    const query = { isDeleted: false };
    try {
        const results = await Elevator.find(query).sort({ SNO: 1 }).exec();
        res.json(results);
    } catch (error) {
        next(error);
    }

})

.post(async(req, res, next) => {
    const {
        body
    } = req;
    const query = {};

    try {
        const results = await Elevator.find(query).exec();
        // body.SNO = results.length;
        delete req.body.id
        const elevator = new Elevator(body);
        const result = await elevator.save();
        res.json(result);
    } catch (error) {
        next(error);
    }

});

router.route('/:id')

.put(async(req, res, next) => {
    const {
        body
    } = req;
    const { id } = req.params;
    try {
        const result = await Elevator.findOneAndUpdate({ id }, body, { new: true }).exec();
        res.json(result);
    } catch (error) {
        next(error);
    }
})

.get(async(req, res, next) => {
    const { id } = req.params;
    const query = { id };
    try {
        const results = await Elevator.find(query).exec();
        res.json(results);
    } catch (error) {
        next(error);
    }

})


module.exports = router;