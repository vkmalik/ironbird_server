'use strict';
const express = require('express');
const router = express.Router();
const { Admin } = require('../../models');
const { Passwords } = Admin;
const { AuthServ } = require('../../lib');

router.route('/')

.get(async(req, res, next) => {
    const query = {};
    try {
        const results = await Passwords.find(query).exec();
        res.json(results);
    } catch (error) {
        next(error);
    }

})

.post(async(req, res, next) => {
    delete req.body.id
    const {
        body
    } = req;
    try {
        const passwords = new Passwords(body);
        const result = await passwords.save();
        res.json(result);
    } catch (error) {
        next(error);
    }

});


router.route('/:id')

.put(async(req, res, next) => {
    const {
        body
    } = req;
    const { id } = req.params;
    try {
        const result = await Passwords.findOneAndUpdate({ id }, body, { new: true }).exec();
        res.json(result);
    } catch (error) {
        next(error);
    }
})

.get(async(req, res, next) => {
    const { id } = req.params;
    const query = { id };
    try {
        const results = await Passwords.find(query).exec();
        res.json(results);
    } catch (error) {
        next(error);
    }

})

.delete(async(req, res, next) => {
    const {
        body
    } = req;
    const { id } = req.params;
    try {
        const result = await Passwords.findOneAndUpdate({ id }, { isDeleted: true }, { new: true }).exec();
        res.json(result);
    } catch (error) {
        next(error);
    }
})


// router.route('/get-starge/:stage')

//     .get(async(req, res, next) => {
//         const { stage: stages } = req.params;
//         const query = { stages };
//         try {
//             const results = await Passwords.find(query).exec();

//         } catch (error) {
//             next(error);
//         }

//     })


module.exports = router;