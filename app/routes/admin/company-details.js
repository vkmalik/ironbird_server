'use strict';
const express = require('express');
const fs = require('fs')
const router = express.Router();
const { Admin, UserProfiles } = require('../../models');
const path = require('path');
const { v4: uuid_v4 } = require('uuid');
const { Company } = Admin;
const { LocalProfile, FBProfile, GoogleProfile } = UserProfiles;
const { AuthServ } = require('../../lib');
const path1 = './uploads/company-attachementes';
const multer = require('multer');
const storage = multer.diskStorage({
    // destination: './uploads/company-attachementes',
    destination: (req, res, cb) => {
        fs.exists(path1, function(exists) {
            if (exists) {
                cb(null, path1);
            } else {
                fs.mkdir(path1, function(err) {
                    if (err) {
                        console.log('Error in folder Creation');
                        cb(null, path1);
                    }
                    cb(null, path1);
                })
            }
        })
    },
    filename: (req, file, cb) => {
        const ext = path.extname(file.originalname);
        const fileName = `${uuid_v4()}-${Date.now()}${ext}`;
        cb(null, fileName);
    }
});
const upload = multer({ storage });

const attchFieldNames = [
    'companyLogo',
    'companyRegistrationDigital',
    'companySGSTDigital',
    'companyPANDigital',
    'companyCGSTDigital',
    'companyIGSTDigital',
    'companyHeadPANDigital',
    'companyGSTDigital',
    'companyDigital',
    'companyCEODigitalSignature',
    'companyHrDigitalSignature'
];

const attcFiles = attchFieldNames.map(data => {
    return { name: data, maxCount: 1 }
})

var cpUpload = upload.fields(attcFiles);

/*
 * Get Company Details
 */

router.route('/')
    .get(async(req, res, next) => {
        const query = {};

        try {
            const results = await Company.find(query).exec();
            res.json(results[0]);
        } catch (error) {
            next(error);
        }

    })


.post(cpUpload, async(req, res, next) => {
    try {
        delete req.body.id
        const company = new Company(req.body)
        const results = await company.save();
        res.json(results);
    } catch (error) {
        next(error);
    }

})


/*
 * Get Company Logo
 */

router.route('/logo')

.get(async(req, res, next) => {
    try {
        const result = await Company.findOne({}).exec();
        const { companyLogo } = result;
        res.json(companyLogo);
    } catch (error) {
        next(error);
    }
})


router.route('/:id')

.put(cpUpload, async(req, res, next) => {
    const {
        body
    } = req;

    // const {
    //     companyEmail,
    //     name,
    //     companyAddress,
    //     yearOfEstablished,
    //     contactPersonEmail,
    //     companyPinCode,
    //     designation,
    //     contactNumber,
    //     comapanyState,
    //     comapanyPinCode,
    //     webSideURL,
    //     companyHeadNumber,
    //     noOfEmployess,
    //     companyRegistrationNumber,
    //     companyPANNumber,
    //     companyCGSTNumber,
    //     companyIGSTNumber,
    //     companyHeadPANNumber,
    //     companyDigital,
    //     salesBillGenNo,
    //     purBillGenNo,
    //     quotationNo,
    //     jobNo,
    //     companyISONumber,
    //     companyGSTNumber,
    //     companyBankAccountName,
    //     BankAccountNumber,
    //     IFSCCode,
    //     accountHolderName,
    //     companyBankBranchName,
    //     GST,
    //     SGST,
    //     CGST,
    //     IGST,
    // } = body;

    const { id } = req.params;
    try {
        const findData = await Company.findOne({ id }).exec();
        if (req.files !== undefined)
            attchFieldNames.map(data => {
                if (req.files[data] !== undefined) {
                    findData[data] = req.files[data] === undefined ? findData[data] : req.files[data];
                }

                Object.assign(body, findData);
                // return JSON.stringify(body);
            })

        const result = await Company.findOneAndUpdate({ id }, body, { new: true }).exec();
        await LocalProfile.findOneAndUpdate({ userId: id }, { name: body.name }, { new: true }).exec();
        res.json(result);
    } catch (error) {
        next(error);
    }
})


.get(async(req, res, next) => {
    const { id } = req.params;
    try {
        const result = await Company.findOne({ id }).exec();
        res.json(result);
    } catch (error) {
        next(error);
    }
})

// router.route('/name-search/:name')

// .get(async (req, res, next) => {
//   const { name } = req.params;
//   try {
//     const result = await Company.findOne({ name: { $regex : name, $options: 'i' } }).exec();
//     res.json(result);
//   } catch (error) {
//     next(error);
//   }
// })




module.exports = router;