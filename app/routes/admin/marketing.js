'use strict';
const fs = require('fs')
const express = require('express');
const router = express.Router();
const { Admin } = require('../../models');
const { Marketing } = require('../../models/admin');
const { AuthServ } = require('../../lib');

var multer = require('multer')
var upload = multer({ dest: 'uploads/marketing' })

const path = 'uploads/marketing'

router.route('/:id')
    .delete(async(req, res, next) => {
        const { id } = req.params;
        try {
            let result;
            let updateArr = [];
            let rest = await Marketing.find({});
            let galleryId = rest[0]._id;
            rest[0].listOfImages.forEach((ele, i) => {
                if (ele.filename === id) {
                    fs.unlinkSync(`${path}/${id}`)
                } else {
                    updateArr.push(ele)
                }
            });

            if (updateArr.length > 0) {
                result = await Marketing.findOneAndUpdate({ _id: galleryId }, { listOfImages: updateArr }, { new: true })
            } else {
                result = await Marketing.findOneAndRemove({ _id: galleryId })
            }
            if (result) {
                res.status('200').json({ message: "Deleted successfully" });
            }

        } catch (err) {
            next(err);
        }
    })

router.route('/')

.post(upload.array('photos', 100), async(req, res, next) => {
    let data = { listOfImages: req.files };
    try {
        let countImg = await Marketing.find({}).countDocuments();
        if (countImg == 0) {
            let markeingImag = new Marketing(data);
            let result = await markeingImag.save();
            if (result) {
                res.status('200').json({ message: "uploades successfully" });
            }
        } else {
            let firstData = await Marketing.find({});
            let imgs = [...req.files, ...firstData[0].listOfImages]

            let result = await Marketing.findOneAndUpdate({ _id: firstData[0]._id }, { listOfImages: imgs }, { new: true });
            if (result) {
                res.status('200').json({ message: "uploades successfully" });
            }
        }
    } catch (err) {
        next(err);
    }
})

.get(async(req, res, next) => {
    try {
        let result = await Marketing.find({})
        if (result) {
            res.status('200').json(result);
        }
    } catch (err) {
        next(err);
    }
})

module.exports = router;