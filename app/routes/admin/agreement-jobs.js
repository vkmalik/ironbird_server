'use strict';
const express = require('express');
const router = express.Router();
const { Admin, Creatjobs } = require('../../models');
const { Creatjob } = Creatjobs;
const { AgreementJob } = Admin;
const { AuthServ } = require('../../lib');

function getDateFunction() {
    let date = new Date();
    let day = date.getDate().toString();
    let month = date.getMonth().toString();
    let fullYear = date.getFullYear().toString();
    let hours = date.getHours().toString();
    let mini = date.getMinutes().toString();
    let sec = date.getSeconds().toString();
    let year = fullYear.slice(2);
    return { day, date, month, year, hours, mini, sec }
}

router.route('/name_id')

.get(async(req, res, next) => {
    try {
        const result = await AgreementJob.find({}, {
            id: 1,
            clientId: 1,
            siteId: 1,
            managerEmail: 1,
            quotationnumber: 1,
            agreementId: 1,
            quotationId: 1
        }).exec();
        res.json(result);
    } catch (err) {
        next(err)
    }
})

router.route('/count')

.get(async(req, res, next) => {
    try {
        const results = await AgreementJob.find({}).countDocuments();
        res.json(results);
    } catch (error) {
        next(error);
    }

})

router.route('/')

.get(async(req, res, next) => {

    try {
        const results = await AgreementJob.find({}).sort({ jobNo: 1 }).exec();
        res.json(results);
    } catch (error) {
        next(error);
    }

})

.post(async(req, res, next) => {
    try {
        delete req.body.id
        const creatAgreement = new AgreementJob(req.body);
        const result = await creatAgreement.save()
        res.json(result)
    } catch (error) {
        next(error);
    }

});

router.route('/:id')

.get(async(req, res, next) => {
    const { id } = req.params;
    try {
        const results = await AgreementJob.find({ clientId: id }).sort({ jobNo: 1 }).exec();
        if (results.length > 0) {
            res.json(results);
        } else {
            const result = await AgreementJob.find({ id }).sort({ jobNo: 1 }).exec();
            res.json(result);
        }
    } catch (error) {
        next(error);
    }

})

.put(async(req, res, next) => {
    const {
        body
    } = req;
    const { id } = req.params;
    try {
        const result = await AgreementJob.findOneAndUpdate({ id }, body, { new: true }).exec();
        result.Jobs.forEach(async(ele) => {
            await Creatjob.findOneAndUpdate({ id: ele.uid }, { jobAgreementNumber: result.agreementId }, { new: true }).exec();
        });
        res.json(result);
    } catch (error) {
        next(error);
    }
})

module.exports = router;