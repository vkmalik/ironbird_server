'use strict';
const express = require('express');
const router = express.Router();
const { Customers } = require('../../models');
const { Customer } = Customers;
const { AuthServ } = require('../../lib');

router.route('/name_id')

.get(async(req, res, next) => {
    try {
        const results = await Customer.find({}, { id: 1, address: 1, companyName: 1 }).sort({ companyName: 1 }).exec();
        res.json(results);
    } catch (error) {
        next(error);
    }

})

router.route('/count')

.get(async(req, res, next) => {
    try {
        const results = await Customer.find({}).countDocuments();
        res.json(results);
    } catch (error) {
        next(error);
    }

})

router.route('/')

.get(async(req, res, next) => {
    const query = { isDeleted: false };
    try {
        const results = await Customer.find(query).sort({ companyName: 1 }).exec();
        res.json(results);
    } catch (error) {
        next(error);
    }

})

.post(async(req, res, next) => {
    const {
        body
    } = req;
    const query = {};

    try {
        delete req.body.id
        const customer = new Customer(body);
        const result = await customer.save();
        res.json(result);
    } catch (error) {
        next(error);
    }

});


router.route('/:id')

.put(async(req, res, next) => {
    const {
        body
    } = req;
    const { id } = req.params;
    try {
        const result = await Customer.findOneAndUpdate({ id }, body, { new: true }).exec();
        res.json(result);
    } catch (error) {
        next(error);
    }
})

.get(async(req, res, next) => {
    const { id } = req.params;
    const query = { id };
    try {
        const results = await Customer.find(query).sort({ companyName: 1 }).exec();
        res.json(results);
    } catch (error) {
        next(error);
    }

})

.delete(async(req, res, next) => {
    const {
        body
    } = req;
    const { id } = req.params;
    try {
        const result = await Customer.findOneAndUpdate({ id }, { isDeleted: true }, { new: true }).exec();
        res.json(result);
    } catch (error) {
        next(error);
    }
})




module.exports = router;