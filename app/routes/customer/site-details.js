'use strict';
const express = require('express');
const router = express.Router();
const { Customers } = require('../../models');
const { Site, Customer } = Customers;
const { AuthServ } = require('../../lib');

router.route('/')

.get(async(req, res, next) => {
    const query = { isDeleted: false };
    try {
        const results = await Site.find(query).exec();
        res.json(results);
    } catch (error) {
        next(error);
    }

})

.post(async(req, res, next) => {
    const {
        body
    } = req;
    const query = {};

    try {
        const { clientId } = body;
        await Customer.findOneAndUpdate({ id: clientId }, { $inc: { sites: 1 } })
        delete body.id
        const site = new Site(body);
        const result = await site.save();
        res.json(result);
    } catch (error) {
        next(error);
    }

});

router.route('/:id/name_id')

.get(async(req, res, next) => {
    const { id } = req.params;
    try {
        // const results = await Site.find({ clientId: id }, { id: 1, address: 1, siteName: 1, siteId: 1, personName: 1 }).exec();
        const results = await Site.find({ clientId: id }, { address: 1, siteName: 1, id: 1, personName: 1, mobile: 1 }).exec();
        res.json(results);
    } catch (error) {
        next(error);
    }

})


router.route('/:id')

.put(async(req, res, next) => {
    const {
        body
    } = req;
    const { id } = req.params;
    try {
        const result = await Site.findOneAndUpdate({ id }, body, { new: true }).exec();
        res.json(result);
    } catch (error) {
        next(error);
    }
})

.get(async(req, res, next) => {
    const { id } = req.params;
    const query = { clientId: id };
    try {
        const results = await Site.find(query).exec();
        res.json(results);
    } catch (error) {
        next(error);
    }

})

.delete(async(req, res, next) => {
    const {
        body
    } = req;
    const { id } = req.params;
    try {
        const result = await Site.findOneAndUpdate({ id }, { isDeleted: true }, { new: true }).exec();
        res.json(result);
    } catch (error) {
        next(error);
    }
})


module.exports = router;