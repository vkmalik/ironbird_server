'use strict';
const express = require('express');
const router = express.Router();
const { Customers } = require('../../models');
const { Site, Customer, GeneralInvoice } = Customers;
const { AuthServ } = require('../../lib');

const fs = require('fs')
const { v4: uuid_v4 } = require('uuid');
const path = require('path');

var multer = require('multer')
const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './uploads/invoices')
    },
    filename: function(req, file, cb) {
        const ext = path.extname(file.originalname);
        const fileName = `${uuid_v4()}${ext}`;
        cb(null, fileName);
    }
})

var upload = multer({ storage: storage })

router.route('/')

.get(async(req, res, next) => {
    try {
        const results = await GeneralInvoice.find({}).exec();
        res.json(results);
    } catch (error) {
        next(error);
    }

})

.post(upload.single('general_invoice'), async(req, res, next) => {
    const {
        body
    } = req;
    const attachement = req.file;
    try {
        const { clientId, dispatchBy, dispatchdate } = body;

        if (dispatchBy !== undefined && dispatchdate !== undefined) {
            Object.assign(attachement, { 'dispatchBy': dispatchBy, 'dispatchdate': dispatchdate })
        }

        Object.assign(attachement, { 'totalAmt': body.totalAmt })
        const result = await GeneralInvoice.findOne({ clientId }).exec();
        if (result !== null) {
            if (result.length === 0) {
                let generalInvoices = [];
                generalInvoices.push(attachement)
                Object.assign(body, { generalInvoices })
                const generalInvoice = new GeneralInvoice(body);
                const results = await generalInvoice.save();
                res.json(results);
            } else {
                result.generalInvoices.push(attachement)
                const results = await GeneralInvoice.findOneAndUpdate({ clientId }, { generalInvoices: result.generalInvoices }, { new: true }).exec()
                res.json(results)
            }
        } else {
            let generalInvoices = [];
            generalInvoices.push(attachement)
            Object.assign(body, { generalInvoices })
            const generalInvoice = new GeneralInvoice(body);
            const results = await generalInvoice.save();
            res.json(results);
        }

    } catch (error) {
        next(error);
    }

});


router.route('/:id')

.get(async(req, res, next) => {
    const { id } = req.params || {}
    try {
        const results = await GeneralInvoice.findOne({ clientId: id }).exec()
        res.json(results)
    } catch (err) {
        next(err)
    }
})

module.exports = router;