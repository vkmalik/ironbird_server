'use strict';

const express = require('express');
const router = express.Router({ mergeParams: true });
const CustomerRouter = require('./customer-details');
const SiteRouter = require('./site-details');
const InvoiceRouter = require('./generalInvoices')


router.use('/customer-details', CustomerRouter);
router.use('/site-details', SiteRouter);
router.use('/generalInvoices', InvoiceRouter)


module.exports = router;