'use strict';
const express = require('express');
const router = express.Router();
const { Suppliers } = require('../../models');
const { Supplier } = Suppliers;
const { AuthServ } = require('../../lib');

router.route('/')

.get(async(req, res, next) => {
    const query = { isDeleted: false };
    try {
        const results = await Supplier.find({}).exec();
        res.json(results);
    } catch (error) {
        next(error);
    }

})

.post(async(req, res, next) => {
    try {
        delete req.body.id

        const Supplierdetails = new Supplier(req.body);
        const result = await Supplierdetails.save();
        res.json(result);
    } catch (error) {
        next(error);
    }

});

router.route('/name_id_stage')

.post(async(req, res, next) => {
    const { stages } = req.body || {}
    try {
        const result = await Supplier.aggregate([{
                $match: {
                    "supplierItems.stages": stages,
                }
            },
            {
                "$project": {
                    CompanyName: 1,
                    SupplierName: 1,
                    id: 1,
                    supplierItems: {
                        $filter: {
                            input: "$supplierItems",
                            as: "text",
                            cond: {
                                $in: [
                                    "$$text.stages", [
                                        stages
                                    ]
                                ]
                            }
                        }
                    }
                }
            }
        ]).sort({ CompanyName: 1 }).exec()

        res.json(result)
    } catch (err) {
        next(err)
    }
})

// Vipin Code

router.route('/add/:id')

.put(async(req, res, next) => {
    const {
        body
    } = req;
    const { id } = req.params;
    try {
        const result = await Supplier.findOne({ id }).exec();
        result.supplierItems.push(body);
        const newResult = await Supplier.findOneAndUpdate({ id }, { supplierItems: result.supplierItems }, { new: true }).exec();
        res.json(newResult);
    } catch (error) {
        next(error);
    }
})

router.route('/items/:id')

.put(async(req, res, next) => {
        const {
            body
        } = req;
        const { id } = req.params;
        try {
            // console.log(req.body)
            const result = await Supplier.findOne({ id }).exec();
            result.supplierItems.forEach(ele => {
                // console.log(ele);
                if (ele.id === req.body.id && ele.hSNumber === req.body.hSNumber && ele.stages === req.body.stages) {
                    ele.itemName = req.body.itemName;
                    ele.quantity = req.body.quantity;
                    ele.unit = req.body.unit;
                    ele.total = req.body.total;
                }
            });
            const newResult = await Supplier.findOneAndUpdate({ id }, result, { new: true }).exec();
            // console.log(newResult);
            res.json(newResult);
        } catch (error) {
            // next(error);
        }
    })
    // Vipin Code

router.route('/:id')

.put(async(req, res, next) => {
    const {
        body
    } = req;
    const { id } = req.params;
    try {
        const result = await Supplier.findOneAndUpdate({ id }, body, { new: true }).exec();
        res.json(result);
    } catch (error) {
        next(error);
    }
})

.get(async(req, res, next) => {
    const { id } = req.params;
    const query = { id };
    try {
        const results = await Supplier.find(query).exec();
        res.json(results);
    } catch (error) {
        next(error);
    }

})

.delete(async(req, res, next) => {
    const {
        body
    } = req;
    const { id } = req.params;
    try {
        const result = await Supplier.findOneAndUpdate({ id }, { isDeleted: true }, { new: true }).exec();
        res.json(result);
    } catch (error) {
        next(error);
    }
})

// router.route('/materialupdate/:id')
// .post(async(req, res, next) => {
//     const {
//         body
//     } = req;
//     const query = {};

//     try {
//         const Supplierdetails = new Supplier(body);
//         const result = await Supplierdetails.save();
//         res.json(result);
//     } catch (error) {
//         next(error);
//     }

// });

module.exports = router;