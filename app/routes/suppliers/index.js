'use strict';

const express = require('express');
const router = express.Router({mergeParams: true});
const SupplierRouter = require('./suppliers-details');



router.use('/suppliers-details', SupplierRouter);



module.exports = router;
