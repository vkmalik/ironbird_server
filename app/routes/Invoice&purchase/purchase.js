'use strict';
const express = require('express');
const router = express.Router();

const { InvoicePurchase } = require("../../models");
const { purchase } = InvoicePurchase;

router.route('/')

.post( async (req,res,next) => {
    try{
       const creatpurchase = new purchase(req.body);  
       const results = await creatpurchase.save();
       
       res.json(results);
    }

    catch(error){
        next(error)
    }
})

.get(async(req, res, next) => {
    const query = { isDeleted: false };
    try {
        const results = await purchase.find(query).exec();
        res.json(results);
    } catch (error) {
        next(error);
    }

})




module.exports = router