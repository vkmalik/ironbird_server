'use strict';

const express = require('express');
const router = express.Router({mergeParams: true});
const PurchaseRouter = require('./purchase');


router.use('/purchaseInvoice', PurchaseRouter);

module.exports = router;