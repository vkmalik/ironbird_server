'use strict';

const { v4: uuid_v4 } = require('uuid');
const express = require('express');
const router = express.Router();

const { User, UserProfiles, Admin } = require('../../models');
const { LocalProfile } = UserProfiles;
const { Employee } = Admin;
const { PasswordServ, TokenServ } = require('../../lib');

router.route('/')


/**
 * Login
 * 
 */

.post(async(req, res, next) => {
    const {
        otp: otpToken,
        password: plainPassword
    } = req.body;

    const updateObj = {
        otp: undefined,
        isEmailVerified: true
    };

    try {
        const { otp } = await TokenServ.verify(otpToken);
        const user = await LocalProfile.findOne({ otp }).exec();
        if (!user || !user.otp) {
            const message = 'Invalid Link / Link Expired';
            return inValidLinkError(next, message);
        }

        const password = await PasswordServ.hash(plainPassword);
        Object.assign(user, updateObj, { password });
        await user.save();
        res.json({ message: 'Password Updated Successfully' });
    } catch (error) {
        next(error);
    }
})

router.route('/:id')


.put(async(req, res, next) => {
    const {
        password: plainPassword,
    } = req.body;
    const { id } = req.params;

    try {
        const localProfile = await LocalProfile.findOne({ userId: id }).exec();
        if (!localProfile) {
            const message = 'Invalid Link / Link Expired';
            return inValidLinkError(next, message);
        }

        const password = await PasswordServ.hash(plainPassword);
        Object.assign(localProfile, { password });
        await localProfile.save();
        res.json({ message: 'Password Updated Successfully' });
    } catch (error) {
        next(error);
    }
})



module.exports = router;