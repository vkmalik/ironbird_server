'use strict';
const express = require('express');
const router = express.Router();

const { User, UserProfiles } = require('../../models');
const { LocalProfile } = UserProfiles;
router.route('/')

    /*
     * Delete Account
     * 
     */


    .post(async(req, res, next) => {
        const { email, id } = req.body;
        try {
            const user = await User.findOneAndUpdate({ email, id }, { $set: { isDeleted: true } })
            if (!user) {
                const error = new Error('User Not Exist');
                error.status = 400;
                return next(error);
            }
            res.json({ message: "User Will Deleted Successfully" })
        } catch (error) {
            next(error)
        }
    })


    module.exports = router;