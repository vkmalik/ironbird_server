'use strict';

const { v4: uuid_v4 } = require('uuid');
const express = require('express');
const router = express.Router();
const { User, UserProfiles, Admin } = require('../../models');
const { LocalProfile } = UserProfiles;
const { Company } = Admin;
const { PasswordServ, TokenServ } = require('../../lib');
const { URLSearchParams, URL } = require('url');
const config = require('config');
const HOST_ADDR = config.get('HOST_ADDR');


const {
    NewAccountVerification
} = require('../../helpers/mail');

const inValidLinkError = (next, message = 'Invalid Link') => {
    const error = new Error(message);
    error.status = 400;
    return next(error);
};

const getRedirectURL = (paramsObj = {}, targetURL) => {
    const params = new URLSearchParams(paramsObj);
    const url = new URL(targetURL);
    url.search = params.toString();
    return url;
};

const errorRedirect = (message, res) => {
    const targetURL = `${HOST_ADDR.ui}/login`;
    const url = getRedirectURL({ message }, targetURL);
    res.redirect(url);
};

router.route('/')

.get(async(req, res, next) => {
    const { verify, otp: otpToken } = req.query;
    if (!(verify === 'account') || !otpToken) {
        return inValidLinkError(next);
    }

    const updateObj = {
        otp: undefined,
        isEmailVerified: true
    };

    try {
        const { otp } = await TokenServ.verify(otpToken);
        const user = await LocalProfile.findOne({ otp }).exec();
        if (!user || !user.otp) {
            const message = 'Invalid Link / Link Expired';
            throw new Error(message);
            return;
        }

        Object.assign(user, updateObj);
        await user.save();

        const message = 'Your Account Has Been Verified Successfully.. You Can Login Now..';
        throw new Error(message);
        // res.json({ message: 'Your Account Has Been Verified Successfully.. You Can Login Now..' });
    } catch (error) {
        console.error(error);
        errorRedirect(error.message, res);
    }


})


/**
 * Register New User
 */

.post(async(req, res, next) => {
    const { body } = req;
    const {
        email,
        name,
        role
    } = body;

    const query = {
        email
    };


    try {
        const user = await User.findOne(query).exec();
        if (user) {
            return res.json({ message: "User Already Exit" });
        }
        const newUser = new User({
            email,
            role
        });

        const result = await newUser.save();
        const userId = result.id;
        const password = await PasswordServ.hash(body.password);
        const otp = uuid();
        const profileData = {
            userId,
            name,
            password,
            otp
        };

        const companyData = {
            id: userId,
            name,
            companyEmail: email
        };

        const otpToken = TokenServ.generate({ otp });

        const profile = new LocalProfile(profileData);
        const company = new Company(companyData);

        await profile.save();
        await company.save();

        NewAccountVerification.send(await otpToken, email)
            .then(data => {})
            .catch(error => console.error(error, 'babu'));

        res.json({
            message: 'Verification Email Sent To Your Email Id.. Please Verify Your Email By Clicking The Verification Link'
        });

    } catch (error) {
        next(error);
    }

})



module.exports = router;