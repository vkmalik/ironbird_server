'use strict';

const express = require('express');
const router = express.Router();

const { User, UserProfiles, Admin } = require('../../models');
const { LocalProfile } = UserProfiles;
const { PasswordServ, TokenServ } = require('../../lib');
const { Role } = Admin;

router.route('/')


/**
 * Login
 * 
 */

.post(async(req, res, next) => {
    const {
        email,
        password
    } = req.body;

    try {
        const user = await User.findOne({ email }).exec();

        if (!user) {
            const error = new Error('Check Your Email');
            error.status = 400;
            return next(error);
        }

        const profile = await LocalProfile.findOne({ userId: user.id }).exec();

        if (!profile.isEmailVerified) {
            const error = new Error('You Should Verify Your Email ID To Login');
            error.status = 401;
            return next(error);
        }

        const isCorrectPassword = await PasswordServ.match(password, profile.password);

        if (!isCorrectPassword) {
            const error = new Error('Incorrect Password');
            error.status = 401;
            return next(error);
        }

        let rolesHideAndShow = await Role.findOne({ role: user.role });
        let tokenData = {}
        if (rolesHideAndShow) {
            tokenData = {
                email,
                provider: profile.provider,
                userId: user.id,
                userName: profile.name,
                profileId: profile.id,
                role: user.role,
                rolesHideAndShow
            };
        } else {
            tokenData = {
                email,
                provider: profile.provider,
                userId: user.id,
                userName: profile.name,
                profileId: profile.id,
                role: user.role
            };
        }

        const token = await TokenServ.generate(tokenData);
        res.json({ token });

    } catch (error) {
        next(error);
    }

})



module.exports = router;