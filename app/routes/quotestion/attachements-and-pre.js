'use strict';
const express = require('express');
const router = express.Router();
const { Quotestion, Admin } = require('../../models');
const fs = require('fs')
const path = require('path');
const path1 = './uploads/quotestion-attachementes';
const { v4: uuid_v4 } = require('uuid');
const { QuotestionAttachment } = Quotestion;
const { Quotation } = Admin;
const { AuthServ } = require('../../lib');

const multer = require('multer');
const storage = multer.diskStorage({
    // destination: './uploads/quotestion-attachementes',
    destination: (req, res, cb) => {
        fs.exists(path1, function(exists) {
            if (exists) {
                cb(null, path1);
            } else {
                fs.mkdir(path1, function(err) {
                    if (err) {
                        console.log('Error in folder Creation');
                        cb(null, path1);
                    }
                    cb(null, path1);
                })
            }
        })
    },
    filename: (req, file, cb) => {
        const ext = path.extname(file.originalname);
        const fileName = `${uuid_v4()}${ext}`;
        cb(null, fileName);
    }
});
const upload = multer({ storage });


/*
 * Get QuotestionAttachment Details
 */

router.route('/')
    .get(async(req, res, next) => {
        const query = {};
        try {
            const results = await QuotestionAttachment.find(query).exec();
            res.json(results);
        } catch (error) {
            next(error);
        }

    })

.post(upload.single('quotes-attach'), async(req, res, next) => {
    const {
        body
    } = req;
    const query = {};
    const { quotesId } = body;
    const attachement = req.file;
    Object.assign(body, { attachement })
    try {
        const checkId = await Quotation.findOne({ id: quotesId });
        if (!checkId) {
            return res.json({ message: "No Data Fount Give Id" })
        }

        const attachemente = new QuotestionAttachment(body);
        const result = await attachemente.save();
        res.json(result);
    } catch (error) {
        next(error);
    }
});

router.route('/:id')
    .get(async(req, res, next) => {
        const { id } = req.params;
        // console.log(id)
        const query = { quotesId: id };

        try {
            const results = await QuotestionAttachment.find(query).exec();
            // console.log(results)
            res.json(results);
        } catch (error) {
            next(error);
        }

    })

// Some Problem after change api data

router.route('/remove/:id')

.delete(async(req, res, next) => {
    const {
        body
    } = req;
    const { id } = body;
    try {
        const result = await QuotestionAttachment.findOneAndRemove({ id });
        if (!result) {
            return res.json({ message: "No Data Fount Give Id" })
        }
        res.json(result);
    } catch (error) {
        next(error);
    }
})


.put(async(req, res, next) => {
    const { id } = req.body;
    try {
        const result = await QuotestionAttachment.findOneAndRemove({ id }).exec();
        fs.unlinkSync(`${result.attachement.path}`)
        res.json(result);
    } catch (error) {
        next(error);
    }
})



module.exports = router;