'use strict';
const express = require('express');
const router = express.Router();

const { Creatjobs, Admin } = require("../../models");
const { AgreementJob } = Admin;
const { JobDetails } = Creatjobs;


router.route('/')

.get(async(req, res, next) => {
    try {
        const result = await JobDetails.find().exec();
        res.json(result);
    } catch (error) {
        next(error);
    }
})

.post(async(req, res, next) => {
    try {

        console.log(req.body)
            // const jobDetails = new JobDetails(req.body);
            // console.log(jobDetails, "hello")
            // const result = await jobDetails.save();
            // res.json(result);
    } catch (error) {
        next(error)
    }
})

router.route('/maintenance/:id')
    .put(async(req, res, next) => {
        const {
            body
        } = req;
        // console.log(req.body);
        const { id } = req.params;
        try {
            const result = await JobDetails.findOneAndUpdate({ id: id }, { listOfMaintance: body.listOfMaintance }, { new: true }).exec();
            res.json(result);
        } catch (error) {
            next(error);
        }
    })


router.route('/:id')
    .put(async(req, res, next) => {
        const {
            body
        } = req;
        const { jobid } = body || {};
        const { id } = req.params;
        // console.log(body)
        try {
            const result = await JobDetails.findOneAndUpdate({ id }, body, { new: true }).exec();
            res.json(result);
        } catch (error) {
            next(error);
        }
    })

.get(async(req, res, next) => {
    const { id } = req.params;
    const query = { id };
    try {
        const results = await JobDetails.find(query).exec();
        res.json(results);
    } catch (error) {
        next(error);
    }
})


router.route('/stage/:stage')
    .get(async(req, res, next) => {
        const { stage } = req.params;
        const query = { "jobStage": stage };
        try {
            const results = await JobDetails.find(query).exec();
            res.json(results);
        } catch (error) {
            next(error);
        }
    })

router.route('/jobsDetails/:Details')
    .get(async(req, res, next) => {
        const { Details } = req.params;
        const query = { "agreementId": Details };
        try {
            const results = await JobDetails.find(query).exec();
            res.json(results);
        } catch (error) {
            next(error);
        }
    })

router.route('/client/:id')
    .get(async(req, res, next) => {
        const { id } = req.params;
        const query = { clientId: id };
        try {
            const results = await JobDetails.find(query).exec();
            res.json(results);
        } catch (error) {
            next(error);
        }
    })


module.exports = router