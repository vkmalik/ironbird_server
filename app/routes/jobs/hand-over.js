'use strict';
const express = require('express');
const router = express.Router();

const { Creatjobs } = require("../../models");
const { HandOver } = Creatjobs;


router.route('/')

  .get(async (req, res, next) => {
    try {
      const result2 = await HandOver.find().exec();
      res.json(result2);
    } catch (error) {
      console.log(error);
      next(error);
    }
  })

  .post(async (req, res, next) => {
    try {
      const handOver = new HandOver(req.body);
      const result = await handOver.save();
      res.json(result);
    } catch (error) {
      next(error)
    }
  })

router.route('/:id')
  .put(async (req, res, next) => {
    const {
      body
    } = req;
    const { jobid } = body || {};
    const { id } = req.params;
    try {
      const result = await HandOver.findOneAndUpdate({ id, jobid }, body, { new: true }).exec();
      res.json(result);
    } catch (error) {
      next(error);
    }
  })

  .get(async (req, res, next) => {
    const { id } = req.params;
    const query = { jobId: id  };
    try {
      const results = await HandOver.find(query).exec();
      res.json(results);
    } catch (error) {
      next(error);
    }
  })



module.exports = router
