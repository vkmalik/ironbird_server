'use strict';
const express = require('express');
const router = express.Router();

const { Creatjobs } = require("../../models");
const { JobStageMaintenance, Creatjob } = Creatjobs;

router.route('/exists/:id')
    .get(async(req, res, next) => {
        const { id } = req.params || {}
        try {
            const results = await JobStageMaintenance.find({ jobId: id }).countDocuments().exec();
            res.json(results);
        } catch (err) {
            next(err)
        }
    })

router.route('/')

.get(async(req, res, next) => {
    try {
        const result2 = await JobStageMaintenance.find().exec();
        res.json(result2);
    } catch (error) {
        console.log(error);
        next(error);
    }
})

.post(async(req, res, next) => {
    const { body } = req;
    const { jobId } = body || {};
    try {
        const results = await JobStageMaintenance.findOne({ jobId: jobId }).exec();
        if (results) {
            console.log('already exists')
        } else {
            const maintenance = new JobStageMaintenance(req.body);
            const result = await maintenance.save();
            res.json(result);
        }
    } catch (error) {
        next(error)
    }
})


router.route('/:id')

.put(async(req, res, next) => {
    const { body } = req;
    const { id } = req.params;
    try {
        let query = {}
        if (body.stage == 'Handover') {
            query = { handover: body.data, stage: 'Handover' }
        } else if (body.stage == 'rectification') {
            query = { rectification: body.data }
        } else if (body.stage == 'governor') {
            query = { governor: body.data }
        } else if (body.stage == 'controller') {
            query = { controller: body.data }
        } else if (body.stage == 'hoistway') {
            query = { hoistway: body.data }
        } else if (body.stage == 'landing') {
            query = { landing: body.data }
        } else if (body.stage == 'car') {
            query = { car: body.data }
        } else if (body.stage == 'pit') {
            query = { pit: body.data }
        } else if (body.stage == 'operations') {
            query = { operations: body.data }
        } else {
            query = { owners: body.data }
        }

        const result = await JobStageMaintenance.findOneAndUpdate({ id }, query, { new: true }).exec();
        if (body.stage == 'Handover')
            await Creatjob.findOneAndUpdate({ id: result.jobId }, { jobStage: result.stage });

        res.json(result);
    } catch (error) {
        next(error);
    }
})

.get(async(req, res, next) => {
    const { id } = req.params;
    const query = { jobId: id };
    try {
        const results = await JobStageMaintenance.findOne(query).exec();
        res.json(results);
    } catch (error) {
        next(error);
    }
})

router.route('/signaturesUpdate/:id')

.put(async(req, res, next) => {
    const {
        body
    } = req;
    const { id } = req.params;
    try {
        const result = await JobStageMaintenance.findOneAndUpdate({ jobId: id }, { signatures: body.sign }, { new: true }).exec();
        res.json({ message: 'update' });
    } catch (err) {
        next(err)
    }
})



module.exports = router