'use strict';

const express = require('express');
const router = express.Router({ mergeParams: true });
const CreatejobsRouter = require('./createjobs');
const attachmentJobs = require('./attachments');
const joStagesRouter = require('./stages');
const jobApprovalRouter = require('./approvals');
const workPerformanceRouter = require('./workPerformance');
const jobDrawingsRouter = require('./job-drawings');
const jobstageDetailsRouter = require('./job-stages')
const materialStageRouter = require('./material-stages')
const handOverRouter = require('./hand-over');
const handOverTestRouter = require('./hand-over-test');
const acceptanceRouter = require('./acceptance')
const balanceSheetRouter = require('./balancesheet')

const jobDetailsRouter = require('./jobDetails')

const invoiceChallanRouter = require('./invoicesAndChallans')

router.use('/creat-job', CreatejobsRouter);
router.use('/jobs-attachment', attachmentJobs);
router.use('/jobs-stages', joStagesRouter);
router.use('/jobs-approval', jobApprovalRouter);
router.use('/work-performance', workPerformanceRouter);
router.use('/job-drawings', jobDrawingsRouter);
router.use('/job-stages-details', jobstageDetailsRouter);
router.use('/material-stages-details', materialStageRouter);
router.use('/hand-over', handOverRouter);
router.use('/hand-over-test', handOverTestRouter);
router.use('/acceptance', acceptanceRouter);
router.use('/balancesheet', balanceSheetRouter);
router.use('/invoice-challan', invoiceChallanRouter);

router.use('/job-details', jobDetailsRouter)


module.exports = router;