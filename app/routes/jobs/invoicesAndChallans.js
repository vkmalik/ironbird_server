'use strict';
const fs = require('fs')
const express = require('express');
const router = express.Router();
const path = require('path');
const { v4: uuid_v4 } = require('uuid');
// const fs = require('fs')
const path1 = './uploads/invoices';

const multer = require('multer')
const storage = multer.diskStorage({
    // destination: function(req, file, cb) {
    //     cb(null, './uploads/invoices')
    // },
    destination: (req, res, cb) => {
        fs.exists(path1, function(exists) {
            if (exists) {
                cb(null, path1);
            } else {
                fs.mkdir(path1, function(err) {
                    if (err) {
                        console.log('Error in folder Creation');
                        cb(null, path1);
                    }
                    cb(null, path1);
                })
            }
        })
    },
    filename: function(req, file, cb) {
        const ext = path.extname(file.originalname);
        const fileName = `${uuid_v4()}${ext}`;
        cb(null, fileName);
    }
})

var upload = multer({ storage: storage })

const { Creatjobs } = require("../../models");
const { InvoiceChallan } = Creatjobs;

const cpUpload = upload.fields([{ name: 'challan_invoice' }, { name: 'invoice_challan' }])


router.route('/getTotalInvCount')
    .get(async(req, res, next) => {
        try {
            let invoiceNo = 0
            const result = await InvoiceChallan.find().exec();
            result.forEach(ele => {
                ele.stage1.forEach(element => {
                    if (element.fieldname === 'invoice_challan') {
                        invoiceNo = invoiceNo + 1;
                    }
                });

                ele.stage2.forEach(element => {
                    if (element.fieldname === 'invoice_challan') {
                        invoiceNo = invoiceNo + 1;
                    }
                });

                ele.stage3.forEach(element => {
                    if (element.fieldname === 'invoice_challan') {
                        invoiceNo = invoiceNo + 1;
                    }
                });

                ele.stageCommissioning.forEach(element => {
                    if (element.fieldname === 'invoice_challan') {
                        invoiceNo = invoiceNo + 1;
                    }
                });
            })

            // console.log(invoiceNo)

            invoiceNo = invoiceNo + 1;
            res.json(invoiceNo)
        } catch (err) {
            next(err)
        }
    })

router.route('/getTotalChalanCount')
    .get(async(req, res, next) => {
        try {
            let invoiceNo = 0
            const result = await InvoiceChallan.find().exec();
            result.forEach(ele => {
                ele.stage1.forEach(element => {
                    if (element.fieldname === 'challan_invoice') {
                        invoiceNo = invoiceNo + 1;
                    }
                });

                ele.stage2.forEach(element => {
                    if (element.fieldname === 'challan_invoice') {
                        invoiceNo = invoiceNo + 1;
                    }
                });

                ele.stage3.forEach(element => {
                    if (element.fieldname === 'challan_invoice') {
                        invoiceNo = invoiceNo + 1;
                    }
                });

                ele.stageCommissioning.forEach(element => {
                    if (element.fieldname === 'challan_invoice') {
                        invoiceNo = invoiceNo + 1;
                    }
                });
            })

            // console.log(invoiceNo)

            invoiceNo = invoiceNo + 1;
            res.json(invoiceNo)
        } catch (err) {
            next(err)
        }
    })

router.route('/:id')

.put(cpUpload, async(req, res, next) => {
    let file = {}
    if (req.files.challan_invoice) {
        file = { "file": req.files.challan_invoice[0] }
    } else {
        file = { "file": req.files.invoice_challan[0] }
    }

    const { id } = req.params;
    const { body } = req;
    const { stage, totalAmt, date, invoiceNo, invoiceType } = body || {}
    try {
        const result = await InvoiceChallan.findOne({ jobId: id }).exec();
        if (result) {
            Object.assign(file.file, { 'totalAmt': totalAmt, 'dateCreated': date, 'stage': stage, 'invoiceNo': invoiceNo, 'invoiceType': invoiceType })
            if (stage === 'Stage1') {
                result.stage1.push(file.file);
                const res1 = await InvoiceChallan.findOneAndUpdate({ jobId: result.jobId }, { stage1: result.stage1 }, { new: true })
                res.json(res1)
            } else if (stage === 'Stage2') {
                result.stage2.push(file.file);
                const res1 = await InvoiceChallan.findOneAndUpdate({ jobId: result.jobId }, { stage2: result.stage2 }, { new: true })
                res.json(res1)
            } else if (stage === 'Stage3') {
                result.stage3.push(file.file);
                const res1 = await InvoiceChallan.findOneAndUpdate({ jobId: result.jobId }, { stage3: result.stage3 }, { new: true })
                res.json(res1)
            } else {
                result.stageCommissioning.push(file.file);
                const res1 = await InvoiceChallan.findOneAndUpdate({ jobId: result.jobId }, { stageCommissioning: result.stageCommissioning }, { new: true })
                res.json(res1)
            }
        }
    } catch (err) {
        next(err)
    }
})

.get(async(req, res, next) => {
    const { id } = req.params;
    try {
        const result = await InvoiceChallan.find({ clientId: id }).exec();
        res.json(result);
    } catch (err) {
        console.log(err);
        next(err);
    }
})

router.route('/')

.get(async(req, res, next) => {
    try {
        const result = await InvoiceChallan.find().exec();
        res.json(result);
    } catch (error) {
        console.log(error);
        next(error);
    }
})

.post(async(req, res, next) => {
    const {
        body
    } = req;
    const { jobId, jobNo } = body || {}
    try {
        const result = await InvoiceChallan.find({ jobId, jobNo }).exec();
        if (result.length > 0) {
            res.json("Already Exists");
        } else {
            const invoiceChallan = new InvoiceChallan(req.body);
            const result = await invoiceChallan.save();
            res.json(result);
        }
    } catch (error) {
        next(error)
    }
})

module.exports = router