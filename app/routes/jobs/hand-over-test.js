'use strict';
const express = require('express');
const router = express.Router();

const { Creatjobs } = require("../../models");
const { HandOverTest } = Creatjobs;


router.route('/')
    .get(async(req, res, next) => {
        try {
            const result2 = await HandOverTest.find().exec();
            res.json(result2);
        } catch (error) {
            console.log(error);
            next(error);
        }
    })


.post(async(req, res, next) => {
    const {
        body
    } = req;

    const { stageId, jobId, jobStages, jobNo } = body || {};

    const stageName = body.stageName;

    try {

        const results = await HandOverTest.findOne({ jobNo: jobNo, jobStages: jobStages, jobId: jobId }).exec();
        if (results !== '' && results !== undefined && results !== null) {
            if (stageName === 'rectification') {
                const result = await HandOverTest.findOneAndUpdate({ jobNo: jobNo, jobStages: jobStages, jobId: jobId }, { rectification: body.rectification }, { new: true }).exec();
                res.json(result);
            } else if (stageName === 'car') {
                const result = await HandOverTest.findOneAndUpdate({ jobNo: jobNo, jobStages: jobStages, jobId: jobId }, { car: body.car }, { new: true }).exec();
                res.json(result);
            } else if (stageName === 'controllers') {
                const result = await HandOverTest.findOneAndUpdate({ jobNo: jobNo, jobStages: jobStages, jobId: jobId }, { controllers: body.controllers }, { new: true }).exec();
                res.json(result);
            } else if (stageName === 'governor') {
                const result = await HandOverTest.findOneAndUpdate({ jobNo: jobNo, jobStages: jobStages, jobId: jobId }, { governor: body.governor }, { new: true }).exec();
                res.json(result);
            } else if (stageName === 'hoistway') {
                const result = await HandOverTest.findOneAndUpdate({ jobNo: jobNo, jobStages: jobStages, jobId: jobId }, { hoistway: body.hoistway }, { new: true }).exec();
                res.json(result);
            } else if (stageName === 'landing') {
                const result = await HandOverTest.findOneAndUpdate({ jobNo: jobNo, jobStages: jobStages, jobId: jobId }, { landing: body.landing }, { new: true }).exec();
                res.json(result);
            } else if (stageName === 'operations') {
                const result = await HandOverTest.findOneAndUpdate({ jobNo: jobNo, jobStages: jobStages, jobId: jobId }, { operations: body.operations }, { new: true }).exec();
                res.json(result);
            } else if (stageName === 'owner') {
                const result = await HandOverTest.findOneAndUpdate({ jobNo: jobNo, jobStages: jobStages, jobId: jobId }, { owner: body.owner }, { new: true }).exec();
                res.json(result);
            } else if (stageName === 'pit') {
                const result = await HandOverTest.findOneAndUpdate({ jobNo: jobNo, jobStages: jobStages, jobId: jobId }, { pit: body.pit }, { new: true }).exec();
                res.json(result);
            }

        } else {

            console.log('not found')
            const handOver = new HandOverTest(req.body);
            const result = await handOver.save();
            res.json(result);
        }
    } catch (error) {
        next(error)
    }
})


router.route('/:id')
    .put(async(req, res, next) => {
        const {
            body
        } = req;

        const { id } = req.params;
        const arrayId = body.ele.id;
        try {
            const result = await HandOverTest.findOne({ jobId: id }).exec();
            if (body.arrayName === 'rectification') {
                result.rectification.forEach(ele => {
                    if (ele.id === arrayId) {
                        ele.rectification = body.ele.rectification;
                        ele.Remarks = body.ele.Remarks;
                        ele.status = body.ele.status;
                    }
                });

                const results = await HandOverTest.findOneAndUpdate({ jobId: id }, { rectification: result.rectification }, { new: true }).exec();
                res.json(results);
            } else if (body.arrayName === 'car') {
                result.car.forEach(ele => {
                    if (ele.id === arrayId) {
                        ele.rectification = body.ele.rectification;
                        ele.Remarks = body.ele.Remarks;
                        ele.status = body.ele.status;
                    }
                });

                const results = await HandOverTest.findOneAndUpdate({ jobId: id }, { car: result.car }, { new: true }).exec();
                res.json(results);
            } else if (body.arrayName === 'controllers') {
                result.controllers.forEach(ele => {
                    if (ele.id === arrayId) {
                        ele.rectification = body.ele.rectification;
                        ele.Remarks = body.ele.Remarks;
                        ele.status = body.ele.status;
                    }
                });

                const results = await HandOverTest.findOneAndUpdate({ jobId: id }, { controllers: result.controllers }, { new: true }).exec();
                res.json(results);
            } else if (body.arrayName === 'governor') {
                result.governor.forEach(ele => {
                    if (ele.id === arrayId) {
                        ele.rectification = body.ele.rectification;
                        ele.Remarks = body.ele.Remarks;
                        ele.status = body.ele.status;
                    }
                });

                const results = await HandOverTest.findOneAndUpdate({ jobId: id }, { governor: result.governor }, { new: true }).exec();
                res.json(results);
            } else if (body.arrayName === 'hoistway') {
                result.hoistway.forEach(ele => {
                    if (ele.id === arrayId) {
                        ele.rectification = body.ele.rectification;
                        ele.Remarks = body.ele.Remarks;
                        ele.status = body.ele.status;
                    }
                });

                const results = await HandOverTest.findOneAndUpdate({ jobId: id }, { hoistway: result.hoistway }, { new: true }).exec();
                res.json(results);
            } else if (body.arrayName === 'landing') {
                result.landing.forEach(ele => {
                    if (ele.id === arrayId) {
                        ele.rectification = body.ele.rectification;
                        ele.Remarks = body.ele.Remarks;
                        ele.status = body.ele.status;
                    }
                });

                const results = await HandOverTest.findOneAndUpdate({ jobId: id }, { landing: result.landing }, { new: true }).exec();
                res.json(results);
            } else if (body.arrayName === 'operations') {
                result.operations.forEach(ele => {
                    if (ele.id === arrayId) {
                        ele.rectification = body.ele.rectification;
                        ele.Remarks = body.ele.Remarks;
                        ele.status = body.ele.status;
                    }
                });

                const results = await HandOverTest.findOneAndUpdate({ jobId: id }, { operations: result.operations }, { new: true }).exec();
                res.json(results);
            } else if (body.arrayName === 'owner') {
                result.owner.forEach(ele => {
                    if (ele.id === arrayId) {
                        ele.rectification = body.ele.rectification;
                        ele.Remarks = body.ele.Remarks;
                        ele.status = body.ele.status;
                    }
                });

                const results = await HandOverTest.findOneAndUpdate({ jobId: id }, { owner: result.owner }, { new: true }).exec();
                res.json(results);
            } else if (body.arrayName === 'pit') {
                result.pit.forEach(ele => {
                    if (ele.id === arrayId) {
                        ele.rectification = body.ele.rectification;
                        ele.Remarks = body.ele.Remarks;
                        ele.status = body.ele.status;
                    }
                });

                const results = await HandOverTest.findOneAndUpdate({ jobId: id }, { pit: result.pit }, { new: true }).exec();
                res.json(results);
            }
            // res.json(result);
        } catch (error) {
            next(error);
        }
    })



module.exports = router