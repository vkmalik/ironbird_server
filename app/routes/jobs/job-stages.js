'use strict';
const express = require('express');
const router = express.Router();

const { Creatjobs } = require("../../models");
const { JobStageDetails, Creatjob } = Creatjobs;
const {
    stageMail
} = require('../../helpers/mail');

router.route('/exists/:id')
    .get(async(req, res, next) => {
        const { id } = req.params || {}
        try {
            const results = await JobStageDetails.find({ jobId: id }).countDocuments().exec();
            res.json(results);
        } catch (err) {
            next(err)
        }
    })


router.route('/')

.get(async(req, res, next) => {
    try {
        const result2 = await JobStageDetails.find().exec();
        res.json(result2);
    } catch (error) {
        console.log(error);
        next(error);
    }
})

.post(async(req, res, next) => {
    const { body } = req;
    const { jobId } = body || {};
    try {
        const results = await JobStageDetails.findOne({ jobId: jobId }).exec();
        if (results) {
            console.log('already exists')
        } else {
            const jobStageDetails = new JobStageDetails(req.body);
            const result = await jobStageDetails.save();
            res.json(result);
        }
    } catch (error) {
        next(error)
    }
})

.put(async(req, res, next) => {
    const { body } = req;
    const { jobId, stage, jobNo, dataid } = body || {};
    try {
        const results = await JobStageDetails.findOne({ jobStages: stage, jobId, jobNo }).exec();
        results.stageData.forEach((element, i) => {
            if (element.id === dataid) {
                results.stageData.splice(i, 1)
            }
        });
        const result = await JobStageDetails.findOneAndUpdate({ jobStages: stage, jobId, jobNo }, { stageData: results.stageData }).exec();
        res.json(result)
    } catch (err) {
        next(err)
    }
})

router.route('/:id/:stage')
    .get(async(req, res, next) => {
        const { id, stage } = req.params;
        const query = { jobId: id, jobStages: stage };
        try {
            const results = await JobStageDetails.find(query).exec();
            res.json(results);
        } catch (error) {
            next(error);
        }
    })

router.route('/:id')

.put(async(req, res, next) => {
    const { body } = req;
    const { id } = req.params;
    try {
        let query = {}
        if (body.stage == 'stage1') {
            query = { stage1Data: body.data, jobStages: 'Stage 1' }
        } else if (body.stage == 'stage2') {
            query = { stage2Data: body.data, jobStages: 'Stage 2' }
        } else {
            query = { stage3Data: body.data, jobStages: 'Stage 3' }
        }

        // console.log(id, body)
        const result = await JobStageDetails.findOneAndUpdate({ id }, query, { new: true }).exec();
        const stageUpdate = await Creatjob.findOneAndUpdate({ id: result.jobId }, { jobStage: result.jobStages });
        // const { managerEmail } = stageUpdate
        // stageMail.send(await { email: managerEmail, data: result })
        res.json(result);
    } catch (error) {
        next(error);
    }
})

.get(async(req, res, next) => {
    const { id } = req.params;
    const query = { jobId: id };
    try {
        const results = await JobStageDetails.findOne(query).exec();
        res.json(results);
    } catch (error) {
        next(error);
    }
})

module.exports = router