'use strict';
const express = require('express');
const router = express.Router();
const path = require('path');
const fs = require('fs')
const { v4: uuid_v4 } = require('uuid');
const { Creatjobs } = require("../../models");
const { JobApproval } = Creatjobs;
const path1 = './uploads/job-attachementes';
const multer = require('multer')
const storage = multer.diskStorage({
    // destination: function(req, file, cb) {
    //   cb(null, './uploads/job-attachementes')
    // },
    destination: (req, res, cb) => {
        fs.exists(path1, function(exists) {
            if (exists) {
                cb(null, path1);
            } else {
                fs.mkdir(path1, function(err) {
                    if (err) {
                        console.log('Error in folder Creation');
                        cb(null, path1);
                    }
                    cb(null, path1);
                })
            }
        })
    },
    filename: function(req, file, cb) {
        const ext = path.extname(file.originalname);
        const fileName = `${uuid_v4()}${ext}`;
        cb(null, fileName);
    }
})

var upload = multer({ storage: storage })

router.route('/')

.get(async(req, res, next) => {
    try {
        const result2 = await JobApproval.find().exec();
        res.json(result2);
    } catch (error) {
        console.log(error);
        next(error);
    }
})

.post(upload.single('job-attach'), async(req, res, next) => {
    const {
        body
    } = req;
    const attachement = req.file;
    Object.assign(body, { attachement })
    try {
        const jobApproval = new JobApproval(body);
        const result = await jobApproval.save();

        res.json(result);
    } catch (error) {
        next(error)
    }
})



router.route('/:id')
    .put(upload.single('job-attach'), async(req, res, next) => {
        const {
            body
        } = req;
        const { id } = req.params;
        const attachment = req.file
        Object.assign(req.body, { attachment })
        try {
            const result = await JobApproval.findOneAndUpdate({ id }, body, { new: true }).exec();
            res.json(result);
        } catch (error) {
            next(error);
        }
    })

.get(async(req, res, next) => {
    const { id } = req.params;
    const query = { jobId: id };
    try {
        const results = await JobApproval.find(query).exec();
        res.json(results);
    } catch (error) {
        next(error);
    }
})

.delete(async(req, res, next) => {
    const {
        body
    } = req;
    const { id } = req.params;
    try {
        const result = await JobApproval.findOneAndRemove({ id }, { new: true }).exec();
        fs.unlinkSync(`${result.attachement.path}`)
        res.json(result);
    } catch (error) {
        next(error);
    }
})

module.exports = router