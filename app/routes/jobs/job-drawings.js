'use strict';
const express = require('express');
const router = express.Router();
const path = require('path');
const path1 = './uploads/job-attachementes';
const fs = require('fs')
const { v4: uuid_v4 } = require('uuid');
const { Creatjobs } = require("../../models");
const { JobDrawings } = Creatjobs;
const multer = require('multer')
const storage = multer.diskStorage({
    // destination: './uploads/job-attachementes',
    destination: (req, res, cb) => {
        fs.exists(path1, function(exists) {
            if (exists) {
                cb(null, path1);
            } else {
                fs.mkdir(path1, function(err) {
                    if (err) {
                        console.log('Error in folder Creation');
                        cb(null, path1);
                    }
                    cb(null, path1);
                })
            }
        })
    },
    filename: function(req, file, cb) {
        const ext = path.extname(file.originalname);
        const fileName = `${uuid_v4()}${ext}`;
        cb(null, fileName);
    }
})

const upload = multer({ storage: storage })

const cpUpload = upload.fields([{ name: 'uploadFile' }, { name: 'machineDesign' }, { name: 'drawingfiles2' }, { name: 'qoutes' }, { name: 'designGallery' }])


router.route('/')

.put(async(req, res, next) => {
    const { body } = req || {}
    const { jobId, filenames, description } = body || {}
    try {
        const path = 'uploads/job-attachementes'
        const result = await JobDrawings.findOne({ jobId }).exec();
        result.drawingfiles.forEach((ele, i) => {
            if (ele.filenames === filenames && ele.description === description) {
                fs.unlinkSync(`${path}/${result.drawingfiles[i].attach.filename}`)
                result.drawingfiles.splice(i, 1)
            }
        })

        const results = await JobDrawings.findOneAndUpdate({ jobId }, { drawingfiles: result.drawingfiles }, { new: true }).exec();
        res.json(results);
    } catch (err) {
        next(err)
    }
})

.get(async(req, res, next) => {
    try {
        const result = await JobDrawings.find().exec();
        res.json(result);
    } catch (error) {
        next(error);
    }
})

.post(async(req, res, next) => {
    try {
        delete req.body.id

        const jobDrawings = new JobDrawings(req.body);
        const result = await jobDrawings.save();
        res.json(result);
    } catch (error) {
        next(error)
    }
})

router.route('/individual/:id')
    .put(cpUpload, async(req, res, next) => {
        const { body } = req;
        const { id } = req.params;
        let temp = {};
        // const { drawingfiles2 } = req.files.drawingfiles2[0]
        try {
            Object.assign(temp, { "description": body.description, "filenames": body.filenames, "attach": req.files.drawingfiles2[0] })
            const results = await JobDrawings.findOne({ jobId: body.jobId, id }).exec();
            results.drawingfiles.push(temp)

            const result = await JobDrawings.findOneAndUpdate({ jobId: body.jobId, id }, { drawingfiles: results.drawingfiles }, { new: true }).exec();
            // console.log(body, id, temp)
            res.json(result)
        } catch (err) {
            next(err)
        }
    })


router.route('/:id')
    .put(cpUpload, async(req, res, next) => {
        const {
            body
        } = req;
        const { id } = req.params;
        try {
            const results = await JobDrawings.findOneAndUpdate({ id }, body, { new: true }).exec();
            res.json(results);
        } catch (error) {
            next(error);
        }
    })

.get(async(req, res, next) => {
    const { id } = req.params;
    const query = { jobId: id };
    try {
        const results = await JobDrawings.findOne(query).exec();
        res.json(results);
    } catch (error) {
        next(error);
    }
})





module.exports = router