'use strict';
const express = require('express');
const router = express.Router();

const { Creatjobs } = require("../../models");
const { Creatjob } = require('../../models/jobs');
const { LocalProfile } = require('../../models/profiles');
const { WorkPerformance } = Creatjobs;


router.route('/')

.get(async(req, res, next) => {
    try {
        const result2 = await WorkPerformance.find().exec();
        res.json(result2);
    } catch (error) {
        console.log(error);
        next(error);
    }
})

.post(async(req, res, next) => {
    try {
        const workPerformance = new WorkPerformance(req.body);
        const result = await workPerformance.save();

        res.json(result);
    } catch (error) {
        next(error)
    }
})



router.route('/:id')
    .put(async(req, res, next) => {
        const {
            body
        } = req;
        const { jobid } = body || {};
        const { id } = req.params;
        try {
            const result = await WorkPerformance.findOneAndUpdate({ id, jobid }, body, { new: true }).exec();
            res.json(result);
        } catch (error) {
            next(error);
        }
    })

.get(async(req, res, next) => {
    const { id } = req.params;
    const query = { jobId: id };
    try {
        const results = await WorkPerformance.find(query).exec();
        res.json(results);
    } catch (error) {
        next(error);
    }
})

router.route('/job_id/:id')
    .get(async(req, res, next) => {
        const { id } = req.params;
        try {
            const user = await LocalProfile.find({}).exec()
            const results = await WorkPerformance.find({ jobId: id }, { id: 1, description: 1, userId: 1, dates: 1 }).exec();
            if (results.length > 0) {
                results.forEach(ele => {
                    user.forEach(data => {
                        if (data.userId == ele.userId) {
                            Object.assign(ele, { employee: data.name })
                        }
                    })
                });
            }
            res.json(results);
        } catch (err) {
            next(err)
        }
    })

router.route('/emp_id/:id')
    .post(async(req, res, next) => {
        const { id } = req.params;
        const { fromDate, toDate } = req.body;
        try {
            const results = await WorkPerformance.find({
                userId: id,
                dates: { $gte: fromDate, $lte: toDate }
            }, { description: 1, dates: 1, jobId: 1 }).exec();

            res.json(results);
        } catch (err) {
            next(err)
        }
    })

module.exports = router