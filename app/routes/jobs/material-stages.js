'use strict';
const express = require('express');
const router = express.Router();
const { Inventory, Creatjobs } = require('../../models');
const { InventoryData } = Inventory;
const { JobMaterialStage } = Creatjobs;

function getDateFunction() {
    let date = new Date();
    let day = date.getDate().toString();
    let month = date.getMonth().toString();
    let fullYear = date.getFullYear().toString();
    let hours = date.getHours().toString();
    let mini = date.getMinutes().toString();
    let sec = date.getSeconds().toString();
    let year = fullYear.slice(2);
    return { date }
}


router.route('/based-on-stage')

.post(async(req, res, next) => {
    const { body } = req;
    const { jobStages, jobId } = body || {};
    try {
        // console.log(body)
        const result = await JobMaterialStage.findOne({ jobId, jobStages }).exec();
        res.json(result)
    } catch (err) {
        next(err)
    }
})

router.route('/')

.get(async(req, res, next) => {
    try {
        const result2 = await JobMaterialStage.find().exec();
        res.json(result2);
    } catch (error) {
        console.log(error);
        next(error);
    }
})

.post(async(req, res, next) => {
    const {
        body
    } = req;
    const { jobId, jobNo, jobStages } = body || {};
    try {
        const result = await JobMaterialStage.findOne({ jobId: jobId, jobNo: jobNo, jobStages: jobStages }).exec();
        if (result) {
            body.materialStageData.forEach(async(ele) => {
                if (ele.invoiceStatus === false) {
                    await InventoryData.findOneAndUpdate({ id: ele.id }, {
                        $set: {
                            "available": parseInt(ele.available) - parseInt(ele.Dispatched)
                        }
                    }, { new: true }).exec()
                }
            })
            const result1 = await JobMaterialStage.findOneAndUpdate({ jobId: jobId, jobNo: jobNo, jobStages: jobStages }, body, { new: true }).exec();
            res.json(result1);
        } else {
            req.body.materialStageData.forEach(async(ele) => {
                if (ele.invoiceStatus === false) {
                    await InventoryData.findOneAndUpdate({ id: ele.id }, {
                        $set: {
                            "available": parseInt(ele.available) - parseInt(ele.Dispatched)
                        }
                    }, { new: true }).exec()
                }
            })
            const materialStage = new JobMaterialStage(req.body);
            const result = await materialStage.save();
            res.json(result);
        }

    } catch (error) {
        next(error)
    }
})

router.route('/challan')
    .put(async(req, res, next) => {
        try {
            let count = 0;
            req.body.forEach(async(ele) => {
                count++;
                let available = 0;
                available = ele.available - ele.dispatch;
                await InventoryData.findOneAndUpdate({ id: ele.setid }, {
                    $set: {
                        "PartDetails.0.available": available
                    }
                }, { new: true }).exec()
            })

            if (count === req.body.length) {
                res.json('Updated Successfully')
            }
        } catch (err) {
            next(err)
        }
    })


router.route('/:id')

.post(async(req, res, next) => {
    const {
        body
    } = req;
    const { jobNo, jobStages, jobId } = body || {};
    try {
        // console.log(jobNo, jobStages, jobId)
        const result = await JobMaterialStage.findOne({ jobId, jobNo, jobStages }).exec();
        // console.log(result)
        if (result) {
            result.materialStageData.forEach((ele, i) => {
                if (ele.invoiceStatus === false) {
                    ele.invoiceStatus = true;
                }
            })
        }
        const res2 = await JobMaterialStage.findOneAndUpdate({ jobId, jobNo, jobStages }, { materialStageData: result.materialStageData }).exec();

        res.json(res2)
    } catch (err) {
        next(err)
    }
})

.put(async(req, res, next) => {
    const {
        body
    } = req;

    // console.log(body, 'body')
    const { jobId, jobNo, Dispatched, employeeName, depatchDate, Received, receiveEmployee, receiveDate, stage, available, index } = body || {};
    const { id } = req.params; // this is setid
    try {
        const result = await JobMaterialStage.findOne({ jobId, jobNo, jobStages: stage }).exec();
        result.materialStageData.forEach(async(ele, i) => {
            if (ele.setid === id && i === index) {
                if (Dispatched !== '' && Dispatched !== undefined && Dispatched !== null) {
                    const { date } = getDateFunction();
                    Object.assign(ele, { Dispatched, employeeName, depatchDate, 'date': date });
                    // } else {
                    //     Object.assign(ele, { Received, receiveEmployee, receiveDate, 'dateCreated': date });
                }

                const result1 = await InventoryData.findOne({ id }).exec()
                    // console.log(result1)
                result1.PartDetails[0].available = available;
                // console.log(result1, 'after update')
                await InventoryData.findOneAndUpdate({ id }, { PartDetails: result1.PartDetails }).exec()

            }
        });

        const result2 = await JobMaterialStage.findOneAndUpdate({ jobId, jobNo, jobStages: stage }, { materialStageData: result.materialStageData }, { new: true }).exec();
        res.json(result2);
    } catch (error) {
        next(error);
    }
})

.get(async(req, res, next) => {
    const { id } = req.params;
    const query = { jobId: id };
    try {
        const results = await JobMaterialStage.find(query).exec();
        res.json(results);
    } catch (error) {
        next(error);
    }
})





module.exports = router