'use strict';
const express = require('express');
const router = express.Router();
const { Creatjobs } = require("../../models");
const { Acceptance, Creatjob } = Creatjobs;
const {
    stageMail
} = require('../../helpers/mail');


router.route('/')

.get(async(req, res, next) => {
    try {
        const result = await Acceptance.find().exec();
        res.json(result);
    } catch (error) {
        console.log(error);
        next(error);
    }
})

.post(async(req, res, next) => {
    try {
        const acceptance = new Acceptance(req.body);
        const result = await acceptance.save();
        res.json(result);
    } catch (error) {
        next(error);
    }
})

router.route('/jobId')
    .post(async(req, res, next) => {
        try {
            const results = await Acceptance.find({ jobid: req.body.jobid }).exec();
            res.json(results);
        } catch (error) {
            next(error);
        }
    })

router.route('/:id')
    .put(async(req, res, next) => {
        const {
            body
        } = req;
        const { id } = req.params;
        try {
            const result = await Acceptance.findOneAndUpdate({ id }, body, { new: true }).exec();
            res.json({ message: 'update' });
        } catch (error) {
            next(error);
        }
    })

.get(async(req, res, next) => {
    const { id } = req.params;
    const query = { id: id };
    try {
        const results = await Acceptance.find(query).exec();
        res.json(results);
    } catch (error) {
        next(error);
    }
})



module.exports = router