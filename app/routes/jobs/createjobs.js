'use strict';
const express = require('express');
const router = express.Router();

const { Creatjobs, Admin } = require("../../models");
const { AgreementJob, Maintenance } = Admin;
const { Creatjob } = Creatjobs;

function getDateFunction() {
    let date = new Date();
    let day = date.getDate().toString();
    let month = date.getMonth().toString();
    let fullYear = date.getFullYear().toString();
    let hours = date.getHours().toString();
    let mini = date.getMinutes().toString();
    let sec = date.getSeconds().toString();
    let year = fullYear.slice(2);
    return { day, date, month, year, hours, mini, sec }
}

function convertToInrFormat(amt) {
    var regExpr = /[₹,]/g;
    var price;
    price = amt;
    if (price[0] === '₹') {
        price = price.replace(regExpr, "");
    }

    var currencySymbol = '₹';
    var result = price.toString().split('.');

    var lastThree = result[0].substring(result[0].length - 3);
    var otherNumbers = result[0].substring(0, result[0].length - 3);
    if (otherNumbers != '')
        lastThree = ',' + lastThree;
    var output = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

    if (result.length > 1) {
        output += "." + result[1];
    } else {
        output += ".00";
    }

    price = currencySymbol + output;
    // console.log(price);
    return price;
}

router.route('/updateSpecificationField')
    .put(async(req, res, next) => {
        try {
            const result = await Creatjob.find({}).exec();
            result.forEach(async(ele, i) => {
                if (ele.SpecificationList)
                    if (ele.SpecificationList.length > 0) {
                        let data = ele.SpecificationList[0]
                        await Creatjob.findOneAndUpdate({ id: ele.id }, { SpecificationList: data }).exec();
                    }
            })

            res.json('Complete')
        } catch (err) {
            next(err)
        }
    })

router.route('/updateFloorsField')
    .put(async(req, res, next) => {
        try {
            const result = await Creatjob.find({}).exec();
            result.forEach(async(ele) => {
                if (ele.invoiceItems.length > 0) {
                    ele.invoiceItems.forEach(async(ele0, i) => {
                        if (ele0.selectedItems)

                            if (!ele0.selectedFloorNameString) {
                            let selectedFloorsArray = []
                            let selectedFloorNameString = ''
                            let selectedFloorString = []
                            let length = ele0.selectedItems.length

                            ele0.selectedItems.forEach((ele1, j) => {
                                let obj = {}

                                selectedFloorString.push(ele1.item_text)

                                if (ele1.height == undefined && ele.selectedDoorStatus == undefined) {
                                    obj = {
                                        "item_id": ele1.item_id,
                                        "item_text": ele1.item_text,
                                        "status": 'open',
                                        "height": 0
                                    }
                                } else {
                                    obj = {
                                        "item_id": ele1.item_id,
                                        "item_text": ele1.item_text,
                                        "status": ele1.selectedDoorStatus,
                                        "height": ele1.height
                                    }
                                }

                                if (j == length - 1) {
                                    selectedFloorNameString += ele1.item_text
                                } else {
                                    selectedFloorNameString += ele1.item_text + ', '
                                }

                                selectedFloorsArray.push(obj)


                            });

                            Object.assign(ele0, { selectedFloorsArray, selectedFloorNameString, selectedFloorString })
                        }
                    })
                }
                await Creatjob.findOneAndUpdate({ id: ele.id }, { invoiceItems: ele.invoiceItems }).exec();

            })

            res.json('Complete')
        } catch (err) {
            next(err)
        }
    })

router.route('/name_id')
    .get(async(req, res, next) => {
        try {
            const result = await Creatjob.find({}, { id: 1, jobid: 1, customerName: 1 }).sort({ jobid: 1 }).exec();
            res.json(result);
        } catch (err) {
            next(err)
        }
    })

router.route('/count')

.get(async(req, res, next) => {
    try {
        const results = await Creatjob.find({}).countDocuments();
        res.json(results);
    } catch (error) {
        next(error);
    }

})

router.route('/moveData')
    .post(async(req, res, next) => {
        try {
            const results = await Creatjob.find().exec();
            var temp = {};
            var price;
            var jobs = [];
            var list = results;
            list.forEach(async(ele, i) => {
                jobs = [];
                const { day, date, month, year, hours, mini, sec } = getDateFunction();
                const agreementId = `IBE/BNG/${day}-${year}/${hours}${mini}${sec +i}`;
                const agreementPrefix = `IBE/BNG/${day}-${year}/`;
                const agreementNo = `${hours}${mini}${sec +i}`;

                price = ele.unitprice;
                if (price[0] !== '₹') {
                    price = convertToInrFormat(price)
                }

                jobs.push({ jobid: ele.jobid, unitprice: price, uid: ele.id })

                Object.assign(temp, {
                    agreementId,
                    agreementNo,
                    agreementPrefix,
                    clientId: ele.clientId,
                    quotationId: ele.quotationId,
                    siteId: ele.siteId,
                    location: ele.location,
                    siteAddress: ele.siteAddress,
                    customerName: ele.customerName,
                    siteName: ele.siteName,
                    agreementdate: ele.agreementdate,
                    quotationnumber: ele.quotationnumber,
                    totalJobQuantity: 1,
                    totalJobPrice: price,
                    payment_conditions: ele.payment_conditions,
                    TermsAndConditions: ele.TermsAndConditions,
                    PoNumber: 0,
                    JobsList: 1,
                    Jobs: jobs,
                    notePoints: ele.notePoints,
                    BankAccountNumber: ele.BankAccountNumber,
                    IFSCCode: ele.IFSCCode,
                    accountHolderName: ele.accountHolderName,
                    companyBankAccountName: ele.companyBankAccountName,
                    companyBankBranchName: ele.companyBankBranchName,
                    companyPANNumber: ele.companyPANNumber,
                    companyGSTNumber: ele.companyGSTNumber,
                    companyAddress: ele.companyAddress,
                    MarketingStaffcontactNumber: ele.MarketingStaffcontactNumber,
                    MarketingStaffPerson: ele.MarketingStaffPerson,
                    listOfMaintance: ele.listOfMaintance,
                    listOfPrepartoryWork: ele.listOfPrepartoryWork,
                    SpecificationList: ele.SpecificationList
                })
                const agree = new AgreementJob(temp)
                const result = await agree.save();

                await Creatjob.findOneAndUpdate({ id: ele.id }, { jobAgreementId: result.id, jobAgreementNumber: result.agreementId }, { new: true }).exec();
                temp = {};
            })
        } catch (error) {
            next(error);
        }
    })

router.route('/')

.get(async(req, res, next) => {
    try {
        const result = await Creatjob.find().sort({ jobNo: 1 }).exec();
        res.json(result);
    } catch (error) {
        next(error);
    }
})

.post(async(req, res, next) => {
    const {
        body
    } = req;
    try {
        var price, total, qty;
        delete req.body.id;
        delete req.body._id;

        const results = await AgreementJob.findOne({ id: req.body.jobAgreementId });

        Object.assign(req.body, {
            location: results.location,
            companyAddress: results.companyAddress,
            siteAddress: results.siteAddress,
            customerName: results.customerName,
            PersonName: results.PersonName,
            siteName: results.siteName,
            mobile: results.mobile,
            agreementdate: new Date().toISOString().substr(0, 10),
            MarketingStaffcontactNumber: results.MarketingStaffcontactNumber,
            MarketingStaffPerson: results.MarketingStaffPerson,
        })

        const resultM = await Maintenance.find({}).exec();
        Object.assign(req.body, { listOfMaintance: resultM })

        const creatjob = new Creatjob(req.body);
        const result = await creatjob.save();

        // console.log(results)
        var Jobs = [];
        var regExpr = /[₹,]/g;
        Jobs = results.Jobs;
        qty = parseInt(req.body.quantity) + parseInt(results.totalJobQuantity)
        price = req.body.totalPrice;
        total = results.totalJobPrice;
        if (price[0] === '₹' || total[0] === '₹') {
            price = price.replace(regExpr, "");
            total = total.replace(regExpr, "");
        }

        // if (total[0] === '₹') {
        //     total = total.replace(regExpr, "");
        // }

        total = parseInt(total) + parseInt(price);
        total = convertToInrFormat(total);

        Jobs.push({ jobid: result.jobid, unitprice: result.unitprice, uid: result.id })

        await AgreementJob.findOneAndUpdate({ id: result.jobAgreementId }, { Jobs: Jobs, totalJobPrice: total, totalJobQuantity: qty, JobsList: qty });

        res.json(result);
    } catch (error) {
        next(error)
    }
})

router.route('/maintenance/:id')
    .put(async(req, res, next) => {
        const {
            body
        } = req;
        const { id } = req.params;
        try {
            const result = await Creatjob.findOneAndUpdate({ id: id }, { listOfMaintance: body.listOfMaintance }, { new: true }).exec();
            res.json(result);
        } catch (error) {
            next(error);
        }
    })


.get(async(req, res, next) => {
    const { id } = req.params;
    try {
        const result = await Creatjob.findOne({ id: id }).exec();
        res.json(result);
    } catch (error) {
        next(error);
    }

})


router.route('/:id')
    .put(async(req, res, next) => {
        const {
            body
        } = req;
        const { jobid } = body || {};
        const { id } = req.params;
        // console.log(body)
        try {
            const result = await Creatjob.findOneAndUpdate({ id }, body, { new: true }).exec();

            const results = await AgreementJob.find({ id: result.jobAgreementId }).exec();
            // console.log(result)
            var Jobs = [];
            Jobs = results[0].Jobs
            var total = 0;
            var regExpr = /[₹,]/g;
            var price;
            Jobs.forEach(async(ele) => {
                if (ele.uid === id) {
                    ele.jobid = result.jobid;
                    if (result.unitprice[0] !== '₹') {
                        ele.unitprice = convertToInrFormat(result.unitprice);
                    } else {
                        ele.unitprice = result.unitprice;
                    }

                    price = ele.unitprice;
                    if (price[0] === '₹') {
                        price = price.replace(regExpr, "");
                        if (total !== 0) {
                            if (total[0] === '₹') {
                                total = total.replace(regExpr, "");
                            }
                        }
                    }
                    total = parseInt(total) + parseInt(price);
                } else {
                    price = ele.unitprice;
                    if (price[0] === '₹') {
                        price = price.replace(regExpr, "");
                        if (total !== 0) {
                            if (total[0] === '₹') {
                                total = total.replace(regExpr, "");
                            }
                        }
                    }
                    total = parseInt(total) + parseInt(price);
                }
            });
            total = convertToInrFormat(total);
            await AgreementJob.findOneAndUpdate({ id: result.jobAgreementId }, { Jobs: Jobs, totalJobPrice: total }, { new: true }).exec();

            res.json(result);
        } catch (error) {
            next(error);
        }
    })

.get(async(req, res, next) => {

    const { id } = req.params;
    const query = { "jobAgreementId": id };

    try {
        const results = await Creatjob.find(query).sort({ jobNo: 1 }).exec();
        res.json(results);
    } catch (error) {
        next(error);
    }
})


router.route('/stage/:stage')
    .get(async(req, res, next) => {
        const { stage } = req.params;
        const query = { "jobStage": stage };
        try {
            const results = await Creatjob.find(query).exec();
            res.json(results);
        } catch (error) {
            next(error);
        }
    })

router.route('/client/:id')
    .get(async(req, res, next) => {
        const { id } = req.params;
        const query = { clientId: id };
        try {
            const results = await Creatjob.find(query).exec();
            res.json(results);
        } catch (error) {
            next(error);
        }
    })

router.route('/agreement/:id')
    .get(async(req, res, next) => {
        const { id } = req.params;
        const query = { id: id };
        try {
            const results = await Creatjob.find(query).exec();
            res.json(results);
        } catch (error) {
            next(error);
        }
    })

module.exports = router