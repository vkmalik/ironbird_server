'use strict';
const express = require('express');
const router = express.Router();
const { Creatjobs } = require("../../models");
const { BalanceSheet } = Creatjobs;

function getDateFunction() {
    let date = new Date();
    let day = date.getDate().toString();
    let month = date.getMonth().toString();
    let fullYear = date.getFullYear().toString();
    let hours = date.getHours().toString();
    let mini = date.getMinutes().toString();
    let sec = date.getSeconds().toString();
    let year = fullYear.slice(2);
    return { day, date, month, year, hours, mini, sec }
}

router.route('/')

.get(async(req, res, next) => {
    try {
        const result = await BalanceSheet.find().exec();
        res.json(result);
    } catch (error) {
        console.log(error);
        next(error);
    }
})

.post(async(req, res, next) => {
    const {
        body
    } = req;
    const query = {};
    try {
        const { userId, userName } = body || {};
        const users = { userName, userId, time: new Date() };
        Object.assign(body, { users });
        const balanceSheet = new BalanceSheet(req.body);
        const result = await balanceSheet.save();
        res.json(result);
    } catch (error) {
        next(error);
    }
})

router.route('/:id')

.put(async(req, res, next) => {
    const {
        body
    } = req;
    const { id } = req.params;
    try {
        const { userName, userId } = body || {};
        const users = { userName, userId, time: new Date() };
        const result = await BalanceSheet.findOne({ id: id }).exec();
        result.users.push(users);
        Object.assign(body, { users: result.users })
        await BalanceSheet.findOneAndUpdate({ id: id }, body, { new: true }).exec();
        res.json({ message: 'update' });
    } catch (error) {
        next(error);
    }
})

.get(async(req, res, next) => {
    const { id } = req.params;
    const query = { agreement_id: id };
    try {
        const results = await BalanceSheet.find(query).exec();
        res.json(results);
    } catch (error) {
        next(error);
    }
})

.delete(async(req, res, next) => {
    const {
        body
    } = req;
    const { id } = req.params;
    try {
        const result = await BalanceSheet.findOneAndDelete({ id: id }).exec();
        // res.json(result);
        res.json({ message: 'Deleted Successfully' });
    } catch (error) {
        next(error);
    }
})

router.route('/client/:id')
    .get(async(req, res, next) => {
        const { id } = req.params;
        const query = { clientId: id };
        try {
            const results = await BalanceSheet.find(query).exec();
            res.json(results);
        } catch (error) {
            next(error);
        }
    })


module.exports = router