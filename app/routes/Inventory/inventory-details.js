'use strict';
const express = require('express');
const router = express.Router();
const { Inventory } = require('../../models');
const { InventoryData } = Inventory;
const { AuthServ } = require('../../lib');

router.route('/')

.get(async(req, res, next) => {
    const query = {};
    try {
        const results = await InventoryData.find(query).exec();
        res.json(results);
    } catch (error) {
        next(error);
    }

})

.post(async(req, res, next) => {
    const {
        body
    } = req;
    const query = {};
    try {
        delete body.id

        const results = await InventoryData.find(query).exec();
        body.SNO = results.length;
        const customer = new InventoryData(body);
        const result = await customer.save();
        // result.PartDetails.forEach((data, i) => {
        //     // Object.assign(req.body, {
        //     //     PartDetail: data, PartDetail: data.PartDetail, partNumber: data.partNumber, unitPrice: data.unitPrice, unit: data.unit, available: data.available, reorder: data.reorder
        //     // })
        // })
        res.json(result);
    } catch (error) {
        next(error);
    }

});

router.route('/:id')

.put(async(req, res, next) => {
    const {
        body
    } = req;
    const { id } = req.params;
    try {
        // const result = await InventoryData.findOne({ id }).exec();
        // result.PartDetails[0] = body;
        const result1 = await InventoryData.findOneAndUpdate({ id }, body, { new: true }).exec();
        res.json(result1);
    } catch (error) {
        next(error);
    }
})

.get(async(req, res, next) => {
    const { id } = req.params;
    const query = { id };
    try {
        const results = await InventoryData.find(query).exec();
        res.json(results);
    } catch (error) {
        next(error);
    }

})

.delete(async(req, res, next) => {
    const { id } = req.params;
    try {
        const result = await InventoryData.findOneAndRemove({ id }, { new: true }).exec();
        res.json(result);
    } catch (error) {
        next(error);
    }
})

router.route('/byStage')
    .post(async(req, res, next) => {
        try {
            // console.log(req.body)
            const results = await InventoryData.find({ selectedStage: req.body.stages }).exec();
            res.json(results);
        } catch (err) {
            next(err)
        }
    })


module.exports = router;