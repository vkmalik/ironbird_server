'use strict';

const express = require('express');
const router = express.Router({mergeParams: true});
const inventoryRouter = require('./inventory-details');


router.use('/inventory-details', inventoryRouter);




module.exports = router;