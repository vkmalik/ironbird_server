'use strict';

const express = require('express');
const router = express.Router({ mergeParams: true });
const AmcRouter = require('./create-amc');
const AmcServicesRouter = require('./amc-services');
const AmcMaintanaceRouter = require('./maintanance');
const AmcPasswordRouter = require('./passwordsAmc');
const AmcMaintancePointROuter = require('./amcMaintance-services')
const AmcUploadsRouter = require('./amc-uploads');

router.use('/amc-details', AmcRouter);
router.use('/amc-services', AmcServicesRouter);
router.use('/amc-maintanace', AmcMaintanaceRouter);
router.use('/amc-passwords', AmcPasswordRouter);
router.use('/amc-Maintainapoints', AmcMaintancePointROuter);
router.use('/amc-uploads', AmcUploadsRouter)




module.exports = router;