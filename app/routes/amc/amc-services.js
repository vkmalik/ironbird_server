'use strict';
const express = require('express');
const router = express.Router();

const { Amc } = require("../../models");
const { AmcService } = Amc;


router.route('/')

.get(async(req, res, next) => {
    try {
        const result2 = await AmcService.find().exec();
        res.json(result2);
    } catch (error) {
        next(error);
    }
})

.post(async(req, res, next) => {
    // console.log(req.body)
    try {
        delete req.body.id
        const amcService = new AmcService(req.body);
        const result = await amcService.save();

        res.json(result);
    } catch (error) {
        next(error)
    }
})


router.route('/:id')
    .put(async(req, res, next) => {
        const {
            body
        } = req;
        const { id } = req.params;
        // const { service, selected } = body;
        try {
            // if (selected === 'IRONBIRD ELEVATORS PVT LTD.') {
            //     const result = await AmcService.findOneAndUpdate({ id }, { serviesPoint: service }, { new: true }).exec();
            //     res.json(result);
            // } else if (selected === 'The customer shall') {
            //     const result = await AmcService.findOneAndUpdate({ id }, { serviesPoint0: service }, { new: true }).exec();
            //     res.json(result);
            // } else {
            //     const result = await AmcService.findOneAndUpdate({ id }, { serviesPoint1: service }, { new: true }).exec();
            //     res.json(result);
            // }

            const result = await AmcService.findOneAndUpdate({ id }, body, { new: true }).exec();
            res.json(result);

        } catch (error) {
            next(error);
        }
    })

.get(async(req, res, next) => {
    const { id } = req.params;
    const query = { id };
    try {
        const results = await AmcService.find(query).exec();
        res.json(results);
    } catch (error) {
        next(error);
    }
})


module.exports = router