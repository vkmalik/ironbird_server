'use strict';
const express = require('express');
const router = express.Router();

const { Amc } = require("../../models");
const { AmcSilverService } = Amc;


router.route('/')

.get(async(req, res, next) => {
    try {
        const result2 = await AmcSilverService.find().exec();
        res.json(result2);
    } catch (error) {
        next(error);
    }
})

.post(async(req, res, next) => {
    try {
        delete req.body.id
        const amcSilverService = new AmcSilverService(req.body);
        const result = await amcSilverService.save();

        res.json(result);
    } catch (error) {
        next(error)
    }
})



router.route('/:id')
    .put(async(req, res, next) => {
        const {
            body
        } = req;
        const { id } = req.params;
        try {
            const result = await AmcSilverService.findOneAndUpdate({ id }, body, { new: true }).exec();
            res.json(result);
        } catch (error) {
            next(error);
        }
    })

.get(async(req, res, next) => {
    const { id } = req.params;
    const query = { id };
    try {
        const results = await AmcSilverService.find(query).exec();
        res.json(results);
    } catch (error) {
        next(error);
    }
})


module.exports = router