'use strict';
const express = require('express');
const router = express.Router();

const { Amc } = require("../../models");
const { AmcMaintanancesService } = Amc;


router.route('/')

.get(async(req, res, next) => {
    try {
        const result = await AmcMaintanancesService.find().sort({ createdAt: 1 }).sort({ updatedAt: 1 }).exec();
        res.json(result);
    } catch (error) {
        next(error);
    }
})

.post(async(req, res, next) => {
    try {
        delete req.body.id
        const amcMaintanancesService = new AmcMaintanancesService(req.body);
        const result = await amcMaintanancesService.save();
        res.json(result);
    } catch (error) {
        next(error)
    }
})


router.route('/:id')
    .put(async(req, res, next) => {
        const {
            body
        } = req;
        const { id } = req.params;
        try {
            const result = await AmcMaintanancesService.findOneAndUpdate({ id }, body, { new: true }).exec();
            res.json(result);
        } catch (error) {
            next(error);
        }
    })

.get(async(req, res, next) => {
    const { id } = req.params;
    const query = { amcId: id };
    try {
        const results = await AmcMaintanancesService.find(query).sort({ createdAt: 1 }).sort({ updatedAt: 1 }).exec();
        res.json(results);
    } catch (error) {
        next(error);
    }
})


module.exports = router