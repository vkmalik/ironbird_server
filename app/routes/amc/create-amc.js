'use strict';
const express = require('express');
const router = express.Router();

const { Amc } = require("../../models");
const { AnnualMaintenanceContract } = Amc;

function getDateFunction() {
    let date = new Date();
    let day = date.getDate().toString();
    let month = date.getMonth().toString();
    let fullYear = date.getFullYear().toString();
    let hours = date.getHours().toString();
    let mini = date.getMinutes().toString();
    let sec = date.getSeconds().toString();
    let year = fullYear.slice(2);
    return { day, date, month, year, hours, mini, sec }
}


router.route('/jobId')
    .post(async(req, res, next) => {
        try {
            const results = await AnnualMaintenanceContract.find({ jobid: req.body.jobid }).exec();
            res.json(results);
        } catch (err) {
            next(err)
        }
    })

router.route('/count')

.get(async(req, res, next) => {
    try {
        const results = await AnnualMaintenanceContract.find({}).countDocuments();
        res.json(results);
    } catch (error) {
        next(error);
    }

})

router.route('/new')
    .post(async(req, res, next) => {
        try {
            const annualMaintenanceContract = new AnnualMaintenanceContract(req.body);
            const result = await annualMaintenanceContract.save();
            res.json(result)
        } catch (err) {
            next(err)
        }
    })

router.route('/renewal/:id')
    .put(async(req, res, next) => {
        const { id } = req.params;
        // console.log(req.body, id)
        try {
            const result = await AnnualMaintenanceContract.findOneAndUpdate({ id }, req.body, { new: true }).exec();
            res.json(result);
        } catch (err) {
            next(err)
        }
    })

router.route('/')

.get(async(req, res, next) => {
    try {
        const result = await AnnualMaintenanceContract.find().sort({ amcid: 1 }).exec();
        res.json(result);
    } catch (error) {
        next(error);
    }
})

.post(async(req, res, next) => {
    try {
        delete req.body.id
            // const { day, date, month, year, hours, mini, sec } = getDateFunction();
            // const amcid = `IBE${day}${hours}${mini}${sec}`;
            // const amcPrefix = `IBE`;
            // const amcNo = `${day}${hours}${mini}${sec}`;
            // Object.assign(req.body, { amcNo, amcPrefix, amcid });
        const annualMaintenanceContract = new AnnualMaintenanceContract(req.body);
        const result = await annualMaintenanceContract.save();
        res.json(result);
    } catch (error) {
        next(error)
    }
})

router.route('/updateAMCS')
    .put(async(req, res, next) => {
        try {
            const result = await AnnualMaintenanceContract.updateMany({}, { $set: { 'servies': '', 'serviesPoint': [], 'serviesPoint0': [], 'serviesPoint1': [] } }, { upsert: false, multi: true }).exec();
            res.json({ msg: 'success' })
        } catch (err) {
            next(err)
        }
    })

router.route('/:id')
    .put(async(req, res, next) => {
        const {
            body
        } = req;
        const { id } = req.params;
        try {
            const result = await AnnualMaintenanceContract.findOneAndUpdate({ id }, body, { new: true }).exec();
            res.json(result);
        } catch (error) {
            next(error);
        }
    })

.get(async(req, res, next) => {
    const { id } = req.params;
    const query = { id };
    try {
        const results = await AnnualMaintenanceContract.find(query).sort({ amcid: 1 }).exec();
        res.json(results);
    } catch (error) {
        next(error);
    }
})

router.route('/renewalAMC')

.post(async(req, res, next) => {
    const { expiry } = req.body
    try {
        const results = await AnnualMaintenanceContract.find({
            toData: { $lte: expiry }
        }).sort({ amcNo: 1 }).exec();

        results.forEach(ele => {
            Object.assign(ele, { 'renewal': false, 'reminder': false })
        });

        res.json(results);
    } catch (err) {
        next(err)
    }
})


module.exports = router