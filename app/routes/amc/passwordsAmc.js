'use strict';
const express = require('express');
const router = express.Router();

const { Amc } = require("../../models");
const { AmcPasswordService } = Amc;


router.route('/')

  .get(async (req, res, next) => {
    try {
      const result = await AmcPasswordService.find().exec();
      res.json(result);
    } catch (error) {
      next(error);
    }
  })

  .post(async (req, res, next) => {
    try {
      const AmcPasswordservice = new AmcPasswordService(req.body);
      const result = await AmcPasswordservice.save();
      res.json(result);
    } catch (error) {
      next(error)
    }
  })



router.route('/:siteId')
  .put(async (req, res, next) => {
    const {
      body
    } = req;
    const { siteId } = req.params;
    try {
      const result = await AmcPasswordService.findOneAndUpdate({ siteId }, body, { new: true }).exec();
      res.json(result);
    } catch (error) {
      next(error);
    }
  })

  .get(async (req, res, next) => {
    const { siteId } = req.params;
    const query = { siteId };
    try {
      const results = await AmcPasswordService.find(query).exec();
      res.json(results);
    } catch (error) {
      next(error);
    }
  })


module.exports = router
