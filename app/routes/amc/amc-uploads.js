'use strict';
const express = require('express');
const router = express.Router();
const fs = require('fs')

const { Amc } = require("../../models");
const { AmcUpload, AnnualMaintenanceContract } = Amc;

const path = require('path');
const path1 = './uploads/amc-attachementes';
const { v4: uuid_v4 } = require('uuid');

const multer = require('multer');
const storage = multer.diskStorage({
    // destination: './uploads/amc-attachementes',
    destination: (req, res, cb) => {
        fs.exists(path1, function(exists) {
            if (exists) {
                cb(null, path1);
            } else {
                fs.mkdir(path1, function(err) {
                    if (err) {
                        console.log('Error in folder Creation');
                        cb(null, path1);
                    }
                    cb(null, path1);
                })
            }
        })
    },
    filename: (req, file, cb) => {
        const ext = path.extname(file.originalname);
        const fileName = `${uuid_v4()}${ext}`;
        cb(null, fileName);
    }
});
const upload = multer({ storage });


/*
 * Get AmcUpload Details
 */

router.route('/')
    .get(async(req, res, next) => {
        const query = {};
        try {
            const results = await AmcUpload.find(query).exec();
            res.json(results);
        } catch (error) {
            next(error);
        }

    })

.post(upload.single('amc-attach'), async(req, res, next) => {
    const {
        body
    } = req;
    const { id } = body;
    const attachement = req.file;
    Object.assign(body, { attachement, amcId: id })
    try {
        const upload = new AmcUpload(body);
        const result = await upload.save();
        res.json(result);
    } catch (error) {
        next(error);
    }
});


router.route('/:id')
    .get(async(req, res, next) => {
        const { id } = req.params;
        const query = { amcId: id };
        try {
            const results = await AmcUpload.find(query).exec();
            // console.log(results)
            res.json(results);
        } catch (error) {
            next(error);
        }

    })

// Some Problem after change api data

router.route('/remove/attach')
    .put(async(req, res, next) => {
        const { uid } = req.body;
        try {
            const result = await AmcUpload.findOneAndRemove({ uid }).exec();
            fs.unlinkSync(`${result.attachement.path}`)
            res.json(result);
        } catch (error) {
            next(error);
        }
    })

router.route('/remove')

.delete(async(req, res, next) => {
    const {
        body
    } = req;
    const { id } = body;
    try {
        const result = await AmcUpload.findOneAndRemove({ id });
        if (!result) {
            return res.json({ message: "No Data Fount Give Id" })
        }
        res.json(result);
    } catch (error) {
        next(error);
    }
})


.put(async(req, res, next) => {
    const { id } = req.body;
    try {
        const result = await AmcUpload.find({ amcId: id }).exec();
        res.json(result);
    } catch (error) {
        next(error);
    }
})



module.exports = router;