'use strict';
const {
	errorHandlingMiddleware,
	AuthServ
 } = require('../lib');
const usersRouter = require('./users');
const authRouter = require('./auth');
const blogsRouter = require('./blogs');
const contactRouter = require('./contact-us');
const adminRouter = require('./admin');
const customersRouter = require('./customer');
const jobsRouter = require('./jobs');
const suppliersRouter = require('./suppliers');
const inventoryRouter = require('./Inventory');
const quotestionAttRouter = require('./quotestion');
const PurchaseOrderRouter = require('./Invoice&purchase');
const AMCOrderRouter = require('./amc');


module.exports = app => {
    app.use('/api/auth', authRouter);
    app.use('/api/users', usersRouter);
    app.use('/api/blogs', blogsRouter);
    app.use('/api/contact', contactRouter);
    app.use('/api/admin', adminRouter);
    app.use('/api/customer', customersRouter);
    app.use('/api/jobs', jobsRouter);
    app.use('/api/suppliers' , suppliersRouter);
    app.use('/api/inventory', inventoryRouter);
    app.use('/api/quotestion', quotestionAttRouter);
    app.use('/api/Purchase', PurchaseOrderRouter);
    app.use('/api/amc', AMCOrderRouter);

    app.use(errorHandlingMiddleware);
};



 
