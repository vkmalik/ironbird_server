'use strict';
const express = require('express');
const router = express.Router();
const { AuthServ } = require('../../lib');


const usersAccountRouter = require('./account');
const profilesRouter = require('./profiles');
// const usersLocalProfileRouter = require('./local-profile');
// const usersFaceBookProfileRouter = require('./fb-profile');
// const usersGoogleProfileRouter = require('./google-profile');

router.use('/', usersAccountRouter);
router.use('/:userId/profiles', profilesRouter);

// router.use('/:userId/profile/local', usersLocalProfileRouter);
// router.use('/:userId/profile/fb', usersFaceBookProfileRouter);
// router.use(usersGoogleProfileRouter);

module.exports = router;