'use strict';
const express = require('express');
const router = express.Router({mergeParams: true});

const localProfileRouter = require('./local.js');



router.use('/local', localProfileRouter);


module.exports = router;
