'use strict';

const express = require('express');
const router = express.Router({ mergeParams: true });
const fs = require('fs')
const path1 = './uploads/profile-pictures';
const path = require('path');
const { UserProfiles } = require('../../../models');
const { LocalProfile } = UserProfiles;
const multer = require('multer')

const storage = multer.diskStorage({
    // destination: './uploads/profile-pictures',
    destination: (req, res, cb) => {
        fs.exists(path1, function(exists) {
            if (exists) {
                cb(null, path1);
            } else {
                fs.mkdir(path1, function(err) {
                    if (err) {
                        console.log('Error in folder Creation');
                        cb(null, path1);
                    }
                    cb(null, path1);
                })
            }
        })
    },
    filename: (req, file, cb) => {
        const ext = path.extname(file.originalname);
        const fileName = `${file.fieldname}-${Date.now()}${ext}`;
        cb(null, fileName);
    }
});
const upload = multer({ storage: storage });


/**
 * Update Profile Pic  User
 */

router.route('/')

.put(upload.single('photo'), async(req, res, next) => {

    const fieldLevelPermissions = {
        name: 1,
        email: 1,
        password: 2,
        isEmailVerified: 2,
        provider: 3,
        address: 1,
        otp: 2
    }

    const fields = Object.keys(req.body);
    const userRole = 3;
    const isPermitted = fields.every(field => userRole >= fieldLevelPermissions[field]);

    if (!isPermitted) {
        const error = new Error('You Don\'t Have Permission To Update Requested Fields');
        error.status = 401;
        return next(error);
    }

    const updatedData = {};
    fields.forEach(field => updatedData[field] = req.body[field]);

    if (req.file) {
        const { filename, path } = req.file || {};
        const [basePath, ...imagePath] = path.split('/');
        const profilePic = imagePath.join('/');
        updatedData['profilePic'] = `/${profilePic}`;
    }

    const { userId } = req.params;

    try {
        const result = await LocalProfile.findOneAndUpdate({ userId }, updatedData, { new: true });

        if (!result) {
            const error = new Error('User Not Exist');
            error.status = 400;
            return next(error);
        }

        res.json({ message: "Updated Successfully " });
    } catch (error) {
        next(error);
    }
})


module.exports = router;