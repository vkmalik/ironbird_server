'use strict';
const express = require('express');
const router = express.Router();
const { User, UserProfiles } = require('../../models');
const { LocalProfile, FBProfile, GoogleProfile } = UserProfiles;


function getRequiredProfiles(userId, profiles = '') {
  if (!profiles) {
    return [];
  }

  const ProfileTypes = {
    local: LocalProfile,
    fb: FBProfile,
    google: GoogleProfile
  };
  const requiredProfiles = profiles.split(',')
    .map(profileKey => ProfileTypes[profileKey])
    .map(profileModel => profileModel.findOne({ userId }).exec());
  return requiredProfiles;

}


router.route('/')
  .get(async (req, res, next) => {
    try {
      const user = await User.find({}).exec();
      res.json(user);
    } catch (error) {
      next(error);
    }
  });


router.route('/:id')
  .get(async (req, res, next) => {

    const { id } = req.params;
    const { profiles } = req.query;
    try {
      const userPromise = User.findOne({ id }).lean().exec();
      const requiredProfiles = getRequiredProfiles(id, profiles);
      const profileList = await Promise.all(requiredProfiles);
      const user = await userPromise;
      const resData = Object.assign({}, user, { profiles: profileList });
      res.json(resData);
    } catch (error) {
      next(error);
    }
  });

  router.route('/managers/data')
  .get(async (req, res, next) => {

    try {
      const userPromise =await User.find({ role: "Admin"}).exec();
      res.json(userPromise);
    } catch (error) {
      next(error);
    }
  });


module.exports = router;
