'use strict';
const { SendMail } = require('../../lib');
const { URLSearchParams, URL } = require('url');
const config = require('config');
const HOST_ADDR = config.get('HOST_ADDR');


function getTemplate(url) {
    const template = `
    <p> Click The Following Link To Verify Your Account.. 
      <a href="${url}">Click To Verify Your Account</a>

      <br/>
      or Just Copy Paste the below text...
      <b>${url}</b>
    </p>
`;

    return template;
}



async function send(otpToken, email) {
    const otp = otpToken;
    const params = new URLSearchParams({
        otp,
        verify: 'account'
    });

    const host = HOST_ADDR.server;
    const url = new URL(`${host}/api/auth/signup`);
    url.search = params.toString();
    const html = getTemplate(url.href);
    return SendMail.send(email, {
        html,
        subject: 'Email Verification'
    });
}



module.exports = {
    send
};