'use strict';


const { SendMail } = require('../../lib');

// const defultEmail = 'prakash@veniteck.com';


async function send({ email, data }) {
  
  const html1 = data.stageData.map(ele => {
    if (!ele.description) {
      return;
    }
    return `<tr>
    <td>${ele.description || ''}</td>
    <td style='padding: 20px'>${ele.Completion || ''}</td>
    <td>${ele.isActive || ''}</td>
  </tr>`
  })
  let jobStages = `<p>Dear </p><p> Job Stages: ${data.jobStages}</p><br>${html1.join('')}`
  return SendMail.send(email, {
    html: jobStages,
    subject: 'Contact Enquiry'
  });

}


module.exports = {
  send,
};
