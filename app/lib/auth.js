'use strict';
const TokenServ = require('./token');

const ROLES = {
    user: 1,
    admin: 2,
    super_admin: 3,
    FrontOffice:4,
};

function authorize(role) {
    return async(req, res, next) => {

        const token = (req.headers.authorization || req.headers.Authorization || '').split('Bearer ').pop();
        // console.log(token);

        /*
         * If Token Not Exist Unauthorized Error;
         */
        if (!token) {
            const error = new Error('Token Not Exist');
            error.status = 401;
            return next(error);
        }

        try {
            const decodedData = await TokenServ.verify(token);
            req.tokenData = decodedData;
            const requiredRole = ROLES[role];
            const actualRole = ROLES[decodedData.role];

            if (requiredRole > actualRole) {
                const error = new Error('You Don\'t Have Permission To Proceed');
                error.status = 401;
                return next(error);
            }

            next();
        } catch (error) {
            next(error);
        }
    };
}


async function authorizeAsAdmin() {

}


module.exports = {
    authorize,
};