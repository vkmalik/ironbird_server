'use strict';

const config = {
    db: {
        uri: process.env.MONGO_DB_URI
    },

    // JWT Secret
    jwt: {
        secret: process.env.JWT_SECRET,
        tokenExpirePeriod: (60 * 60 * 1) // 1 day
    },

    // Node MAiler
    nodemailer: {
        user: 'example@gmail.com',
        password: 'example'
    },

    // NODE ENV VARIABLES

    PORT: process.env.PORT || 3500,

    IP: '0.0.0.0',

    // Redirection Host Addresses
    HOST_ADDR: {
        ui: 'http://localhost:4200',
        server: 'http://localhost:3500'
    },

};

module.exports = config;