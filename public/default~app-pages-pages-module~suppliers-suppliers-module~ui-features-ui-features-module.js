(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~app-pages-pages-module~suppliers-suppliers-module~ui-features-ui-features-module"],{

/***/ "./src/app/pages/ui-features/suppliers/purchase-order/purchase-order.component.html":
/*!******************************************************************************************!*\
  !*** ./src/app/pages/ui-features/suppliers/purchase-order/purchase-order.component.html ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n  <div class=\"col-xl-5\">\n      <nb-card >\n          <nb-card-header>\n          Suppliers List\n           <!-- <p>Click on the customer name to view full details  <button class=\"btn btn-success btn-rounded \" (click)=\"showFun('create')\" >Create New Suppliers</button></p> -->\n          </nb-card-header>\n          <nb-card-body>\n            <div class=\"container\">\n              <ng2-smart-table [settings]=\"settings\" [source]=\"source\" (deleteConfirm)=\"onDeleteConfirm($event)\" (userRowSelect)=\"onUserRowSelect($event)\"  >\n                  </ng2-smart-table>\n            </div>\n             \n          </nb-card-body>\n    </nb-card>\n    </div>\n  <div class=\"col-xl-7\" *ngIf=\"tableShow\">\n<ngx-purchases></ngx-purchases>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/pages/ui-features/suppliers/purchase-order/purchase-order.component.scss":
/*!******************************************************************************************!*\
  !*** ./src/app/pages/ui-features/suppliers/purchase-order/purchase-order.component.scss ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "nb-card-body {\n  background-color: #E1F5FE;\n  color: white; }\n\ncol-xl-6, nb-card-header {\n  background: #1D8ECE;\n  color: white; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3ByYWthc2gvVmlwaW4vaXJvbmJpcmQvMjBGRUIyMDIwL2lyb25iaXJkLWNsaWVudHMvc3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9zdXBwbGllcnMvcHVyY2hhc2Utb3JkZXIvcHVyY2hhc2Utb3JkZXIuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSx5QkFBd0I7RUFDeEIsWUFBVyxFQUFBOztBQUViO0VBQ0UsbUJBQW1CO0VBQ25CLFlBQVksRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VpLWZlYXR1cmVzL3N1cHBsaWVycy9wdXJjaGFzZS1vcmRlci9wdXJjaGFzZS1vcmRlci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIm5iLWNhcmQtYm9keXtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiNFMUY1RkU7XG4gICAgY29sb3I6d2hpdGU7XG4gIH1cbiAgY29sLXhsLTYsIG5iLWNhcmQtaGVhZGVye1xuICAgIGJhY2tncm91bmQ6ICMxRDhFQ0U7XG4gICAgY29sb3I6IHdoaXRlO1xuICB9Il19 */"

/***/ }),

/***/ "./src/app/pages/ui-features/suppliers/purchase-order/purchase-order.component.ts":
/*!****************************************************************************************!*\
  !*** ./src/app/pages/ui-features/suppliers/purchase-order/purchase-order.component.ts ***!
  \****************************************************************************************/
/*! exports provided: PurchaseOrderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PurchaseOrderComponent", function() { return PurchaseOrderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var _services_suppliers_supplier_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/suppliers/supplier.service */ "./src/app/services/suppliers/supplier.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var PurchaseOrderComponent = /** @class */ (function () {
    function PurchaseOrderComponent(SupplierServ) {
        this.SupplierServ = SupplierServ;
        this.creatShow = false;
        this.tableShow = false;
        this.settings = {
            add: {
                addButtonContent: '<i class="nb-plus"></i>',
                createButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
            },
            edit: {
                editButtonContent: '<i class="nb-edit"></i>',
                saveButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
            },
            delete: {
                deleteButtonContent: '<i class="nb-trash"></i>',
                confirmDelete: true,
            },
            actions: {
                edit: false,
                add: false,
                delete: false,
                position: 'right'
            },
            columns: {
                SupplierName: {
                    title: 'Supplier Name',
                    type: 'string',
                },
                ContactNo: {
                    title: 'Contact Number',
                    type: 'string',
                },
                // person: {
                //   title: 'Contact Person Name',
                //   type: 'string',
                // },
                orders: {
                    title: 'No.of orders',
                    type: 'string',
                },
            },
        };
        this.source = new ng2_smart_table__WEBPACK_IMPORTED_MODULE_1__["LocalDataSource"]();
        this.Supplier = {};
        this.id = '';
        this.listOfSuppliers = [];
    }
    PurchaseOrderComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.SupplierServ.suppliers
            .subscribe(function (data) {
            _this.getSupplierInfo();
        }, function (error) {
            console.log(error);
        });
        this.getSupplierInfo();
    };
    /**
     *  Create New Elevator Form Open
     */
    PurchaseOrderComponent.prototype.showFun = function () {
        this.creatShow = true;
        this.tableShow = false;
        this.SupplierServ.suppliers.next({ creatShow: true });
    };
    /**
      * Get Supplier Details
      */
    PurchaseOrderComponent.prototype.getSupplierInfo = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, error_1;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        _a = this;
                        return [4 /*yield*/, this.SupplierServ.getSupplier()];
                    case 1:
                        _a.listOfSuppliers = _b.sent();
                        this.listOfSuppliers.reverse();
                        this.listOfSuppliers.map(function (data, index) { return data.S_NO = index + 1; });
                        this.source.load(this.listOfSuppliers);
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _b.sent();
                        console.error(error_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * User Selected Row
     */
    PurchaseOrderComponent.prototype.onUserRowSelect = function (event) {
        event.data.creatShow = false;
        this.SupplierServ.suppliers.next(event.data);
        //  console.log(event.data)
        if (event.data) {
            this.creatShow = false;
            return this.tableShow = true;
        }
        this.creatShow = true;
        this.tableShow = false;
    };
    /**
     *  Delete Row Data
     */
    PurchaseOrderComponent.prototype.onDeleteConfirm = function (event) {
    };
    PurchaseOrderComponent.prototype.createData = function (data) {
        this.getSupplierInfo();
        this.listOfSuppliers.push(data);
        this.listOfSuppliers.reverse();
        this.listOfSuppliers.map(function (data, index) { return data.S_NO = index + 1; });
        this.source.load(this.listOfSuppliers);
    };
    PurchaseOrderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-purchase-order',
            template: __webpack_require__(/*! ./purchase-order.component.html */ "./src/app/pages/ui-features/suppliers/purchase-order/purchase-order.component.html"),
            styles: [__webpack_require__(/*! ./purchase-order.component.scss */ "./src/app/pages/ui-features/suppliers/purchase-order/purchase-order.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_suppliers_supplier_service__WEBPACK_IMPORTED_MODULE_2__["SupplierService"]])
    ], PurchaseOrderComponent);
    return PurchaseOrderComponent;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/suppliers/purchase-order/purchases/purchases.component.html":
/*!***********************************************************************************************!*\
  !*** ./src/app/pages/ui-features/suppliers/purchase-order/purchases/purchases.component.html ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nb-card id=\"contentToConvert\">\n    <nb-card-header>\n     Purchase Order\n    </nb-card-header>\n    <nb-card-body>\n       <ngx-poorder></ngx-poorder>\n    </nb-card-body>\n </nb-card>"

/***/ }),

/***/ "./src/app/pages/ui-features/suppliers/purchase-order/purchases/purchases.component.scss":
/*!***********************************************************************************************!*\
  !*** ./src/app/pages/ui-features/suppliers/purchase-order/purchases/purchases.component.scss ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".odd_row {\n  background: #f9f9f9; }\n\n.right_placeholder {\n  text-align: right; }\n\n.heading {\n  background-color: #357EBD;\n  color: #FFF;\n  margin-bottom: 3em;\n  text-align: center;\n  line-height: 1.2em; }\n\na {\n  cursor: pointer; }\n\na:hover {\n  color: lightgreen; }\n\ninput[type=text], input[type=number], input[type=date], select {\n  font-family: 'Source Sans Pro', Sans-serif;\n  font-size: 1em;\n  border: none;\n  border-bottom: 1px solid #d7d7d7;\n  padding: 4px;\n  outline: none; }\n\n.items-table {\n  width: 100%; }\n\n.items-table th {\n  text-align: left;\n  font-weight: 300;\n  padding: 10px 5px 3px;\n  white-space: nowrap; }\n\n.items-table input[type=text], .items-table input[type=number], .items-table input[type=date], .items-table select {\n  width: 100%; }\n\n#items td {\n  padding: 5px;\n  background: #eee;\n  border-bottom: 1px solid #fff; }\n\n#items .desc {\n  min-width: 200px; }\n\n#items .total-price {\n  text-align: right;\n  font-weight: 600; }\n\n.invoice-totals {\n  width: 50%;\n  float: right; }\n\n.invoice-totals th {\n  font-weight: 300; }\n\n.invoice-totals th, .invoice-totals td {\n  text-align: right;\n  padding: 5px;\n  border-bottom: 1px solid #d7d7d7; }\n\n.invoice-totals .tax-container td {\n  border-bottom: none;\n  padding: 0; }\n\n#taxs {\n  width: 100%; }\n\n#taxs thead th {\n  font-weight: 300;\n  background-color: #eee; }\n\n#taxs th, #taxs td {\n  text-align: right;\n  padding: 5px;\n  border-bottom: 1px solid #d7d7d7; }\n\nimg {\n  width: 303px !important; }\n\n@media (max-width: 1280px) {\n  img {\n    width: 167px !important; } }\n\nnb-card-body {\n  background-color: #E1F5FE;\n  color: white; }\n\nnb-card-header {\n  background: #1D8ECE;\n  color: white; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3ByYWthc2gvVmlwaW4vaXJvbmJpcmQvMjBGRUIyMDIwL2lyb25iaXJkLWNsaWVudHMvc3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9zdXBwbGllcnMvcHVyY2hhc2Utb3JkZXIvcHVyY2hhc2VzL3B1cmNoYXNlcy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLG1CQUFrQixFQUFBOztBQUd0QjtFQUNJLGlCQUFnQixFQUFBOztBQUdwQjtFQUNJLHlCQUF5QjtFQUN6QixXQUFXO0VBQ1gsa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixrQkFBa0IsRUFBQTs7QUFFcEI7RUFDSSxlQUFlLEVBQUE7O0FBRW5CO0VBQ0ksaUJBQWdCLEVBQUE7O0FBRXBCO0VBQ0UsMENBQTBDO0VBQzFDLGNBQWM7RUFDZCxZQUFZO0VBQ1osZ0NBQWdDO0VBQ2hDLFlBQVk7RUFDWixhQUFhLEVBQUE7O0FBRWY7RUFDRSxXQUFXLEVBQUE7O0FBRWI7RUFDRSxnQkFBZ0I7RUFDaEIsZ0JBQWdCO0VBQ2hCLHFCQUFxQjtFQUNyQixtQkFBbUIsRUFBQTs7QUFFckI7RUFDRSxXQUFXLEVBQUE7O0FBRWI7RUFDRSxZQUFZO0VBQ1osZ0JBQWdCO0VBQ2hCLDZCQUE2QixFQUFBOztBQUUvQjtFQUNFLGdCQUFnQixFQUFBOztBQUVsQjtFQUNFLGlCQUFpQjtFQUNqQixnQkFBZ0IsRUFBQTs7QUFFbEI7RUFDRSxVQUFVO0VBQ1YsWUFBWSxFQUFBOztBQUVkO0VBQ0UsZ0JBQWdCLEVBQUE7O0FBRWxCO0VBQ0UsaUJBQWlCO0VBQ2pCLFlBQVk7RUFDWixnQ0FBZ0MsRUFBQTs7QUFFbEM7RUFDRSxtQkFBbUI7RUFDbkIsVUFBVSxFQUFBOztBQUVaO0VBQ0UsV0FBVyxFQUFBOztBQUViO0VBQ0UsZ0JBQWdCO0VBQ2hCLHNCQUFzQixFQUFBOztBQUV4QjtFQUNFLGlCQUFpQjtFQUNqQixZQUFZO0VBQ1osZ0NBQWdDLEVBQUE7O0FBRWxDO0VBQ0UsdUJBQXVCLEVBQUE7O0FBRTNCO0VBQ0U7SUFDRSx1QkFBdUIsRUFBQSxFQUMxQjs7QUFFRDtFQUNFLHlCQUF3QjtFQUN4QixZQUFXLEVBQUE7O0FBRWI7RUFDRSxtQkFBbUI7RUFDbkIsWUFBWSxFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvdWktZmVhdHVyZXMvc3VwcGxpZXJzL3B1cmNoYXNlLW9yZGVyL3B1cmNoYXNlcy9wdXJjaGFzZXMuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIub2RkX3Jvd3tcbiAgICBiYWNrZ3JvdW5kOiNmOWY5Zjk7XG59XG5cbi5yaWdodF9wbGFjZWhvbGRlcntcbiAgICB0ZXh0LWFsaWduOnJpZ2h0O1xufVxuXG4uaGVhZGluZyB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzM1N0VCRDtcbiAgICBjb2xvcjogI0ZGRjtcbiAgICBtYXJnaW4tYm90dG9tOiAzZW07XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGxpbmUtaGVpZ2h0OiAxLjJlbTtcbiAgfVxuICBhe1xuICAgICAgY3Vyc29yOiBwb2ludGVyO1xuICB9XG4gIGE6aG92ZXJ7XG4gICAgICBjb2xvcjpsaWdodGdyZWVuO1xuICB9XG4gIGlucHV0W3R5cGU9dGV4dF0sIGlucHV0W3R5cGU9bnVtYmVyXSwgaW5wdXRbdHlwZT1kYXRlXSwgc2VsZWN0IHtcbiAgICBmb250LWZhbWlseTogJ1NvdXJjZSBTYW5zIFBybycsIFNhbnMtc2VyaWY7XG4gICAgZm9udC1zaXplOiAxZW07XG4gICAgYm9yZGVyOiBub25lO1xuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZDdkN2Q3O1xuICAgIHBhZGRpbmc6IDRweDtcbiAgICBvdXRsaW5lOiBub25lO1xuICB9XG4gIC5pdGVtcy10YWJsZSB7XG4gICAgd2lkdGg6IDEwMCU7XG4gIH1cbiAgLml0ZW1zLXRhYmxlIHRoIHtcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgIGZvbnQtd2VpZ2h0OiAzMDA7XG4gICAgcGFkZGluZzogMTBweCA1cHggM3B4O1xuICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gIH1cbiAgLml0ZW1zLXRhYmxlIGlucHV0W3R5cGU9dGV4dF0sIC5pdGVtcy10YWJsZSBpbnB1dFt0eXBlPW51bWJlcl0sIC5pdGVtcy10YWJsZSBpbnB1dFt0eXBlPWRhdGVdLCAuaXRlbXMtdGFibGUgc2VsZWN0IHtcbiAgICB3aWR0aDogMTAwJTtcbiAgfVxuICAjaXRlbXMgdGQge1xuICAgIHBhZGRpbmc6IDVweDtcbiAgICBiYWNrZ3JvdW5kOiAjZWVlO1xuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZmZmO1xuICB9XG4gICNpdGVtcyAuZGVzYyB7XG4gICAgbWluLXdpZHRoOiAyMDBweDtcbiAgfVxuICAjaXRlbXMgLnRvdGFsLXByaWNlIHtcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICB9XG4gIC5pbnZvaWNlLXRvdGFscyB7XG4gICAgd2lkdGg6IDUwJTtcbiAgICBmbG9hdDogcmlnaHQ7XG4gIH1cbiAgLmludm9pY2UtdG90YWxzIHRoIHtcbiAgICBmb250LXdlaWdodDogMzAwO1xuICB9XG4gIC5pbnZvaWNlLXRvdGFscyB0aCwgLmludm9pY2UtdG90YWxzIHRkIHtcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcbiAgICBwYWRkaW5nOiA1cHg7XG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNkN2Q3ZDc7XG4gIH1cbiAgLmludm9pY2UtdG90YWxzIC50YXgtY29udGFpbmVyIHRkIHtcbiAgICBib3JkZXItYm90dG9tOiBub25lO1xuICAgIHBhZGRpbmc6IDA7XG4gIH1cbiAgI3RheHMge1xuICAgIHdpZHRoOiAxMDAlO1xuICB9XG4gICN0YXhzIHRoZWFkIHRoIHtcbiAgICBmb250LXdlaWdodDogMzAwO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNlZWU7XG4gIH1cbiAgI3RheHMgdGgsICN0YXhzIHRkIHtcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcbiAgICBwYWRkaW5nOiA1cHg7XG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNkN2Q3ZDc7XG4gIH1cbiAgaW1ne1xuICAgIHdpZHRoOiAzMDNweCAhaW1wb3J0YW50O1xufVxuQG1lZGlhKG1heC13aWR0aDoxMjgwcHgpe1xuICBpbWcge1xuICAgIHdpZHRoOiAxNjdweCAhaW1wb3J0YW50O1xufVxufVxubmItY2FyZC1ib2R5e1xuICBiYWNrZ3JvdW5kLWNvbG9yOiNFMUY1RkU7XG4gIGNvbG9yOndoaXRlO1xufVxubmItY2FyZC1oZWFkZXJ7XG4gIGJhY2tncm91bmQ6ICMxRDhFQ0U7XG4gIGNvbG9yOiB3aGl0ZTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/ui-features/suppliers/purchase-order/purchases/purchases.component.ts":
/*!*********************************************************************************************!*\
  !*** ./src/app/pages/ui-features/suppliers/purchase-order/purchases/purchases.component.ts ***!
  \*********************************************************************************************/
/*! exports provided: PurchasesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PurchasesComponent", function() { return PurchasesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var PurchasesComponent = /** @class */ (function () {
    function PurchasesComponent() {
    }
    PurchasesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-purchases',
            template: __webpack_require__(/*! ./purchases.component.html */ "./src/app/pages/ui-features/suppliers/purchase-order/purchases/purchases.component.html"),
            styles: [__webpack_require__(/*! ./purchases.component.scss */ "./src/app/pages/ui-features/suppliers/purchase-order/purchases/purchases.component.scss")]
        })
    ], PurchasesComponent);
    return PurchasesComponent;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/suppliers/supplier/new-suppliers/new-suppliers.component.html":
/*!*************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/suppliers/supplier/new-suppliers/new-suppliers.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nb-card>\n    <nb-card-header class=\"text-center\">\n        New Suppliers\n    </nb-card-header>\n    <nb-card-body>\n        <form [formGroup]=\"form\" (ngSubmit)=\"craeteSupplier(form);\" novalidate>\n            <div>\n                <!-- <h4 class=\"text-center\">New Suppliers</h4> -->\n                <div class=\"row\">\n\n                    <div class=\"col-6\">\n                        <div class=\"form-group input-group-sm\">\n                            <label for=\"Maintenance\">Supplier Name</label>\n                            <input type=\"text\" class=\"form-control\" id=\"Maintenance\" formControlName=\"SupplierName\" placeholder=\"Supplier Name\" [ngClass]=\"{ 'is-invalid': submitted && f.SupplierName.errors }\">\n                            <div *ngIf=\" submitted && f.SupplierName.errors\" class=\"invalid-feedback\">\n                                <div *ngIf=\"f.SupplierName.errors.required\">Supplier Name is required</div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"col-6\">\n                        <div class=\"form-group input-group-sm\">\n                            <label for=\"Date\"> Company Name</label>\n                            <input type=\"text\" class=\"form-control\" id=\"Date\" formControlName=\"CompanyName\" placeholder=\"Company Name\" [ngClass]=\"{ 'is-invalid': submitted && f.CompanyName.errors }\">\n                            <div *ngIf=\" submitted && f.CompanyName.errors\" class=\"invalid-feedback\">\n                                <div *ngIf=\"f.CompanyName.errors.required\">Company Name is required</div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"row\">\n                    <div class=\"col-6 \">\n                        <label for=\"Maintenance\">Supplier Address</label>\n                        <textarea class=\"form-control\" type=\"text\" formControlName=\"SupplierAddress\" placeholder=\"Supplier Address\"></textarea>\n                    </div>\n                    <div class=\"col-6 \">\n                        <label for=\"Maintenance\">Supplier Pickup Address</label>\n                        <textarea class=\"form-control\" type=\"text\" formControlName=\"PickupAddress\" placeholder=\"Supplier Pickup Address\"></textarea>\n                    </div>\n                </div>&nbsp;\n                <div class=\"row\">\n                    <div class=\"col-3\">\n                        <div class=\"form-group input-group-sm\">\n                            <label for=\"Maintenance\">Contact No</label>\n                            <input type=\"text\" class=\"form-control\" id=\"Maintenance\" formControlName=\"ContactNo\" placeholder=\"Contact No\" [ngClass]=\"{ 'is-invalid': submitted && f.ContactNo.errors }\">\n                            <div *ngIf=\" submitted && f.ContactNo.errors\" class=\"invalid-feedback\">\n                                <div *ngIf=\"f.ContactNo.errors.required\">Contact No is required</div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"col-3\">\n                        <div class=\"form-group input-group-sm\">\n                            <label for=\"Date\"> Email id</label>\n                            <input type=\"email\" class=\"form-control\" id=\"Date\" formControlName=\"EmailId\" placeholder=\"Email id\" [ngClass]=\"{ 'is-invalid': submitted && f.EmailId.errors }\">\n                            <div *ngIf=\" submitted && f.EmailId.errors\" class=\"invalid-feedback\">\n                                <div *ngIf=\"f.EmailId.errors.required\">Email id is required</div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"col-3\">\n                        <div class=\"form-group input-group-sm\">\n                            <label for=\"Delivary\">GST</label>\n                            <input type=\"text\" class=\"form-control\" id=\"Delivary\" formControlName=\"Gst\" placeholder=\"GST\" [ngClass]=\"{ 'is-invalid': submitted && f.Gst.errors }\">\n                            <div *ngIf=\" submitted && f.Gst.errors\" class=\"invalid-feedback\">\n                                <div *ngIf=\"f.Gst.errors.required\">GST No is required</div>\n                            </div>\n                        </div>\n                    </div>\n                    <!-- <div class=\"col-3\">\n                        <div class=\"form-group input-group-sm\">\n                            <label for=\"Delivarys\">E Sugam No</label>\n                            <input type=\"text\" class=\"form-control\" id=\"Delivarys\" formControlName=\"ESugamNo\" placeholder=\"E Sugam No\" [ngClass]=\"{ 'is-invalid': submitted && f.ESugamNo.errors }\">\n                            <div *ngIf=\" submitted && f.ESugamNo.errors\" class=\"invalid-feedback\">\n                                <div *ngIf=\"f.ESugamNo.errors.required\">E Sugam No is required</div>\n                            </div>\n                        </div>\n                    </div> -->\n                </div>&nbsp;\n                <div class=\"row\" style=\"margin-left: 1px; margin-right: 1px;\" formArrayName=\"supplierItems\">&nbsp;\n                    <div id=\"item-1\" *ngFor=\"let item of supplierItems.controls; let i=index; let odd=odd;\" [formGroupName]=\"i\" [ngClass]=\"{odd_row: odd}\">\n                        <div class=\"row\">\n                            <div class=\"col-12\">\n                                <div class=\"form-group input-group-sm\">\n                                    <label for=\"description\">Description</label>\n                                    <textarea class=\"form-control\" type=\"text\" formControlName=\"description\" placeholder=\"Description\"></textarea>\n                                </div>\n                            </div>\n\n                            <div class=\"col-3\">\n                                <div class=\"form-group input-group-sm\">\n                                    <label for=\"Delivary\">Item Name</label>\n                                    <!-- <input type=\"text\" class=\"form-control\" id=\"Delivary\" formControlName=\"itemName\" placeholder=\"Item Name\"> -->\n                                    <select class=\"form-control\" id=\"Delivary\" size=\"small\" formControlName=\"itemName\" placeholder=\"Item Name\">\n                    <option value=\"\" disabled>--Select Item Name--</option>\n                  <option *ngFor=\"let list of listOfMaterials;let i = index\" [value]='list.partName'>{{list.partName}}</option>\n                </select>\n                                </div>\n                            </div>\n\n                            <div class=\"col-3\">\n                                <div class=\"form-group input-group-sm\">\n                                    <label for=\"Delivary\">Quantity</label>\n                                    <input type=\"text\" class=\"form-control\" (blur)='changeQty($event.target.value, i)' id=\"Delivary\" formControlName=\"quantity\" placeholder=\"Quantity\">\n                                </div>\n                            </div>\n                            <div class=\"col-3\">\n                                <div class=\"form-group input-group-sm\">\n                                    <label for=\"unit\">Unit Price</label>\n                                    <input type=\"text\" class=\"form-control\" value=\"{{totalObj[i].price}}\" (blur)='changeAmt($event.target.value, i)' id=\"unit\" formControlName=\"unit\" placeholder=\"Unit Price\">\n                                </div>\n                            </div>\n                            <div class=\"col-3\">\n                                <div class=\"form-group input-group-sm\">\n                                    <label for=\"Total\">Total Price</label>\n                                    <!-- value=\"{{totalPrice}}\" -->\n                                    <input type=\"text\" class=\"form-control\" [(ngModel)]=\"total\" value=\"{{totalObj[i].total}}\" id=\"Total\" formControlName=\"total\" placeholder=\"Total price\" disabled>\n                                </div>\n\n                                <i *ngIf=\"supplierItems.controls.length > 1\" style=\"float:right\" class=\"far fa-trash-alt\" (click)=\"removeItem(item)\"></i>\n                            </div>\n                        </div>\n\n\n                    </div>&nbsp;\n                    <!-- <div class=\"col-12\" style=\"margin-left: 1px;\">\n            <button class=\"btn btn-outline-primary \" type=\"button\" shape=\"semi-round\" (click)=\"addItem()\" id=\"addItemButton\">Add</button><br>\n          </div>&nbsp; -->\n\n                </div>\n                <div class=\"row foot\">\n                    <div class=\"col-sm-12\" style=\"margin-top: 3px;\">\n                        <button type=\"submit\" class=\"btn btn-outline-primary \" shape=\"semi-round\" style=\"float: right;\">Save</button>\n                        <!-- <button type=\"submit\" class=\"btn btn-outline-primary \" shape=\"semi-round\" style=\"float: right;\">Print</button> -->\n                        <br>\n                        <br>\n                    </div>\n                </div>\n            </div>\n        </form>\n\n    </nb-card-body>\n</nb-card>"

/***/ }),

/***/ "./src/app/pages/ui-features/suppliers/supplier/new-suppliers/new-suppliers.component.scss":
/*!*************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/suppliers/supplier/new-suppliers/new-suppliers.component.scss ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table {\n  width: -webkit-fill-available; }\n\na {\n  cursor: pointer; }\n\ntextarea {\n  border: none; }\n\nnb-card-body {\n  background-color: #E1F5FE; }\n\nnb-card-header {\n  background: #1D8ECE;\n  color: white; }\n\n.nb-close-circled {\n  font-size: 2.73em;\n  color: red; }\n\n.far {\n  font-size: 1.5em;\n  color: red; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3ByYWthc2gvVmlwaW4vaXJvbmJpcmQvMjBGRUIyMDIwL2lyb25iaXJkLWNsaWVudHMvc3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9zdXBwbGllcnMvc3VwcGxpZXIvbmV3LXN1cHBsaWVycy9uZXctc3VwcGxpZXJzLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksNkJBQTZCLEVBQUE7O0FBRWpDO0VBQ0ksZUFBZSxFQUFBOztBQUVuQjtFQUNJLFlBQVksRUFBQTs7QUFFaEI7RUFDSSx5QkFBd0IsRUFBQTs7QUFHMUI7RUFDRSxtQkFBbUI7RUFDbkIsWUFBWSxFQUFBOztBQUVkO0VBQ0UsaUJBQWlCO0VBQ2pCLFVBQVUsRUFBQTs7QUFFWjtFQUNFLGdCQUFnQjtFQUNoQixVQUFVLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9zdXBwbGllcnMvc3VwcGxpZXIvbmV3LXN1cHBsaWVycy9uZXctc3VwcGxpZXJzLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsidGFibGV7XG4gICAgd2lkdGg6IC13ZWJraXQtZmlsbC1hdmFpbGFibGU7XG59XG5he1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbn1cbnRleHRhcmVhe1xuICAgIGJvcmRlcjogbm9uZTtcbn1cbm5iLWNhcmQtYm9keXtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiNFMUY1RkU7XG4gICAgLy8gY29sb3I6d2hpdGU7XG4gIH1cbiAgbmItY2FyZC1oZWFkZXJ7XG4gICAgYmFja2dyb3VuZDogIzFEOEVDRTtcbiAgICBjb2xvcjogd2hpdGU7XG4gIH1cbiAgLm5iLWNsb3NlLWNpcmNsZWQge1xuICAgIGZvbnQtc2l6ZTogMi43M2VtO1xuICAgIGNvbG9yOiByZWQ7XG4gIH1cbiAgLmZhciB7XG4gICAgZm9udC1zaXplOiAxLjVlbTtcbiAgICBjb2xvcjogcmVkO1xuICAgIC8vIG1hcmdpbi10b3A6IDE1cHg7XG4gIH0iXX0= */"

/***/ }),

/***/ "./src/app/pages/ui-features/suppliers/supplier/new-suppliers/new-suppliers.component.ts":
/*!***********************************************************************************************!*\
  !*** ./src/app/pages/ui-features/suppliers/supplier/new-suppliers/new-suppliers.component.ts ***!
  \***********************************************************************************************/
/*! exports provided: TotalDetails, NewSuppliersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TotalDetails", function() { return TotalDetails; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewSuppliersComponent", function() { return NewSuppliersComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _confirmation_dialog_confirmation_dialog_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../confirmation-dialog/confirmation-dialog.service */ "./src/app/confirmation-dialog/confirmation-dialog.service.ts");
/* harmony import */ var _services_currency_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../services/currency.service */ "./src/app/services/currency.service.ts");
/* harmony import */ var _services_admin_isertParts_insert_parts_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../services/admin/isertParts/insert-parts.service */ "./src/app/services/admin/isertParts/insert-parts.service.ts");
/* harmony import */ var _services_suppliers_supplier_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../../services/suppliers/supplier.service */ "./src/app/services/suppliers/supplier.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




 // VIPIN CODE FOR CURRENCY

var TotalDetails = /** @class */ (function () {
    function TotalDetails() {
        this.itemName = '';
        this.quantity = '';
        this.unit = '';
        this.description = '';
        this.total = '';
        this.hSNumber = ''; // vipin code
        this.stages = ''; // vipin code
    }
    return TotalDetails;
}());

var NewSuppliersComponent = /** @class */ (function () {
    function NewSuppliersComponent(fb, supplierserv, parts, // vipin code
    confirmationDialogService, currency // VIPIN CODE FOR CURRENCY
    ) {
        this.fb = fb;
        this.supplierserv = supplierserv;
        this.parts = parts;
        this.confirmationDialogService = confirmationDialogService;
        this.currency = currency;
        this.regExpr = /[₹,]/g;
        this.submitted = false;
        this.data2 = {};
        this.CompanyName = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]();
        this.supplierChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.totalObj = [];
        this.obj1 = {};
        this.createForm();
        this.addItem();
    }
    NewSuppliersComponent.prototype.ngOnInit = function () {
        this.getPartNames(); // vipin code
    };
    // vipin code
    NewSuppliersComponent.prototype.getPartNames = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, error_1;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        _a = this;
                        return [4 /*yield*/, this.parts.getPartDetails()];
                    case 1:
                        _a.listOfMaterials = _b.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _b.sent();
                        console.error(error_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    NewSuppliersComponent.prototype.createForm = function () {
        this.form = this.fb.group({
            SupplierName: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            CompanyName: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            SupplierAddress: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            PickupAddress: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            ContactNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            EmailId: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            Gst: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            // ESugamNo: new FormControl('', [Validators.required]),
            supplierItems: this.fb.array([]),
        });
    };
    Object.defineProperty(NewSuppliersComponent.prototype, "supplierItems", {
        get: function () {
            return this.form.get('supplierItems');
        },
        enumerable: true,
        configurable: true
    });
    ;
    Object.defineProperty(NewSuppliersComponent.prototype, "f", {
        get: function () { return this.form.controls; },
        enumerable: true,
        configurable: true
    });
    /**
     * Add And Remove for Supplier in form
     */
    NewSuppliersComponent.prototype.addItem = function () {
        this.supplierItems.push(this.fb.group(new TotalDetails()));
    };
    NewSuppliersComponent.prototype.removeItem = function (item) {
        var i = this.supplierItems.controls.indexOf(item);
        if (i != -1) {
            this.supplierItems.controls.splice(i, 1);
            var items = this.form.get('supplierItems');
            var data = { invoiceItems: items };
        }
    };
    /**
    * Create New Suplier
    */
    NewSuppliersComponent.prototype.craeteSupplier = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            var resultAleart, value, result;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        // vipin code starts
                        data.value.supplierItems.forEach(function (ele, index) {
                            Object.assign(ele, { 'id': index + 1 });
                            _this.listOfMaterials.forEach(function (ele1) {
                                if (ele1.partName === ele.itemName) {
                                    Object.assign(ele, { 'hSNumber': ele1.HSN_Code, 'stages': ele1.stages });
                                }
                            });
                        });
                        // vipin code ends
                        this.submitted = true;
                        if (this.form.invalid) {
                            return [2 /*return*/];
                        }
                        return [4 /*yield*/, this.confirmationDialogService.confirm('Alert Message', 'Are you sure you want to Create?')];
                    case 1:
                        resultAleart = _a.sent();
                        if (!resultAleart) return [3 /*break*/, 3];
                        value = data.value;
                        return [4 /*yield*/, this.supplierserv.createSupplier(value)];
                    case 2:
                        result = _a.sent();
                        this.supplierChange.emit(result);
                        return [2 /*return*/, this.form.reset()];
                    case 3:
                        data.confirm.reject();
                        _a.label = 4;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    NewSuppliersComponent.prototype.changeQty = function (qty, i) {
        this.quantity = qty;
        if (this.price !== undefined) {
        }
        else {
            this.price = 0;
        }
        this.calculationPrice = this.quantity * this.price;
        this.total = '';
        this.total = this.currency.convertToInrFormat(this.calculationPrice);
    };
    NewSuppliersComponent.prototype.changeAmt = function (amt, i) {
        if (amt[0] === '₹') {
            amt = amt.replace(this.regExpr, "");
        }
        this.price = amt;
        if (this.quantity !== undefined) {
        }
        else {
            this.quantity = 0;
        }
        this.calculationPrice = this.quantity * this.price;
        this.unitPrice = this.currency.convertToInrFormat(this.price);
        this.total = '';
        this.total = this.currency.convertToInrFormat(this.calculationPrice);
        this.obj1 = {};
        Object.assign(this.obj1, { 'price': this.unitPrice, 'total': this.total });
        this.totalObj[i] = this.obj1;
        // console.log(this.totalObj);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], NewSuppliersComponent.prototype, "supplierChange", void 0);
    NewSuppliersComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-new-suppliers',
            template: __webpack_require__(/*! ./new-suppliers.component.html */ "./src/app/pages/ui-features/suppliers/supplier/new-suppliers/new-suppliers.component.html"),
            styles: [__webpack_require__(/*! ./new-suppliers.component.scss */ "./src/app/pages/ui-features/suppliers/supplier/new-suppliers/new-suppliers.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            _services_suppliers_supplier_service__WEBPACK_IMPORTED_MODULE_5__["SupplierService"],
            _services_admin_isertParts_insert_parts_service__WEBPACK_IMPORTED_MODULE_4__["InsertPartsService"],
            _confirmation_dialog_confirmation_dialog_service__WEBPACK_IMPORTED_MODULE_2__["ConfirmationDialogService"],
            _services_currency_service__WEBPACK_IMPORTED_MODULE_3__["CurrencyService"] // VIPIN CODE FOR CURRENCY
        ])
    ], NewSuppliersComponent);
    return NewSuppliersComponent;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/suppliers/supplier/supplier.component.html":
/*!******************************************************************************!*\
  !*** ./src/app/pages/ui-features/suppliers/supplier/supplier.component.html ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n    <div class=\"col-xl-5\">\n        <nb-card>\n            <nb-card-header>\n                Suppliers List\n                <button *ngIf='moduleCreate' class=\"btn btn-outline-primary \" shape=\"semi-round\" (click)=\"showFun('create')\">Create New\n          Suppliers</button>\n            </nb-card-header>\n            <nb-card-body style=\"background-color:#E1F5FE;\">\n                <div class=\"container\">\n                    <ng2-smart-table [settings]=\"settings\" [source]=\"source\" (deleteConfirm)=\"onDeleteConfirm($event)\" (userRowSelect)=\"onUserRowSelect($event)\">\n                    </ng2-smart-table>\n\n                </div>\n\n            </nb-card-body>\n        </nb-card>\n    </div>\n    <div class=\"col-xl-7\" *ngIf=\"tableShow\">\n        <ngx-suppliers-details></ngx-suppliers-details>\n    </div>\n    <div class=\"col-xl-7\" *ngIf=\"creatShow\">\n        <!-- Nav tabs -->\n        <ngx-new-suppliers (supplierChange)=\"createData($event)\"></ngx-new-suppliers>\n        <!-- Nav tabs -->\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/pages/ui-features/suppliers/supplier/supplier.component.scss":
/*!******************************************************************************!*\
  !*** ./src/app/pages/ui-features/suppliers/supplier/supplier.component.scss ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "button {\n  float: right; }\n\nnb-card-body {\n  color: white; }\n\ncol-xl-6, nb-card-header {\n  background: #1D8ECE;\n  color: white; }\n\n/deep/ ng2-smart-table .ng2-smart-title {\n  color: white !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3ByYWthc2gvVmlwaW4vaXJvbmJpcmQvMjBGRUIyMDIwL2lyb25iaXJkLWNsaWVudHMvc3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9zdXBwbGllcnMvc3VwcGxpZXIvc3VwcGxpZXIuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxZQUFZLEVBQUE7O0FBRWhCO0VBRUksWUFBVyxFQUFBOztBQUViO0VBQ0UsbUJBQW1CO0VBQ25CLFlBQVksRUFBQTs7QUFFZDtFQUNFLHVCQUNBLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9zdXBwbGllcnMvc3VwcGxpZXIvc3VwcGxpZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJidXR0b257XG4gICAgZmxvYXQ6IHJpZ2h0O1xufVxubmItY2FyZC1ib2R5e1xuXG4gICAgY29sb3I6d2hpdGU7XG4gIH1cbiAgY29sLXhsLTYsIG5iLWNhcmQtaGVhZGVye1xuICAgIGJhY2tncm91bmQ6ICMxRDhFQ0U7XG4gICAgY29sb3I6IHdoaXRlO1xuICB9XG4gIC9kZWVwLyBuZzItc21hcnQtdGFibGUgLm5nMi1zbWFydC10aXRsZXtcbiAgICBjb2xvcjp3aGl0ZSAhaW1wb3J0YW50XG4gICAgfSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/ui-features/suppliers/supplier/supplier.component.ts":
/*!****************************************************************************!*\
  !*** ./src/app/pages/ui-features/suppliers/supplier/supplier.component.ts ***!
  \****************************************************************************/
/*! exports provided: SupplierComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SupplierComponent", function() { return SupplierComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var _services_suppliers_supplier_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/suppliers/supplier.service */ "./src/app/services/suppliers/supplier.service.ts");
/* harmony import */ var _services_token_token_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/token/token.service */ "./src/app/services/token/token.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var SupplierComponent = /** @class */ (function () {
    function SupplierComponent(userServ, SupplierServ) {
        this.userServ = userServ;
        this.SupplierServ = SupplierServ;
        this.creatShow = false;
        this.tableShow = false;
        this.settings = {
            add: {
                addButtonContent: '<i class="nb-plus"></i>',
                createButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
            },
            edit: {
                editButtonContent: '<i class="nb-edit"></i>',
                saveButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
            },
            delete: {
                deleteButtonContent: '<i class="nb-trash"></i>',
                confirmDelete: true,
            },
            actions: {
                edit: false,
                add: false,
                delete: false,
                position: 'right'
            },
            columns: {
                SupplierName: {
                    title: 'Supplier Name',
                    type: 'string',
                },
                SupplierAddress: {
                    title: 'Address',
                    type: 'string',
                },
                ContactNo: {
                    title: 'Mobile',
                    type: 'string',
                },
            },
        };
        this.source = new ng2_smart_table__WEBPACK_IMPORTED_MODULE_1__["LocalDataSource"]();
        this.Supplier = {};
        this.id = '';
        this.listOfSuppliers = [];
        this.moduleCreate = true;
        this.moduleEdit = true;
        this.moduleView = true;
    }
    SupplierComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.userServ.userAccess !== undefined) {
            this.userServ.userAccess
                .subscribe(function (data) {
                data.forEach(function (ele) {
                    if (ele.name === 'Suppliers') {
                        _this.moduleCreate = ele.create;
                        _this.moduleEdit = ele.edit;
                        _this.moduleView = ele.view;
                    }
                });
            }, function (error) {
                console.log(error);
            });
        }
        this.SupplierServ.suppliers
            .subscribe(function (data) {
            _this.getSupplierInfo();
        }, function (error) {
            console.log(error);
        });
        this.getSupplierInfo();
    };
    /**
    *  Create New Elevator Form Open
    */
    SupplierComponent.prototype.showFun = function () {
        this.creatShow = true;
        this.tableShow = false;
        this.SupplierServ.suppliers.next({ creatShow: true });
    };
    /**
      * Get Supplier Details
      */
    SupplierComponent.prototype.getSupplierInfo = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, error_1;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        _a = this;
                        return [4 /*yield*/, this.SupplierServ.getSupplier()];
                    case 1:
                        _a.listOfSuppliers = _b.sent();
                        this.listOfSuppliers.reverse();
                        this.listOfSuppliers.map(function (data, index) { return data.S_NO = index + 1; });
                        this.source.load(this.listOfSuppliers);
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _b.sent();
                        console.error(error_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * User Selected Row
     */
    SupplierComponent.prototype.onUserRowSelect = function (event) {
        event.data.creatShow = false;
        this.SupplierServ.suppliers.next(event.data);
        if (event) {
            this.creatShow = false;
            return this.tableShow = true;
        }
        this.creatShow = true;
        this.tableShow = false;
    };
    /**
     *  Delete Row Data
     */
    SupplierComponent.prototype.onDeleteConfirm = function (event) {
    };
    SupplierComponent.prototype.createData = function (data) {
        this.getSupplierInfo();
        this.listOfSuppliers.push(data);
        this.listOfSuppliers.reverse();
        this.listOfSuppliers.map(function (data, index) { return data.S_NO = index + 1; });
        this.source.load(this.listOfSuppliers);
    };
    SupplierComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-supplier',
            template: __webpack_require__(/*! ./supplier.component.html */ "./src/app/pages/ui-features/suppliers/supplier/supplier.component.html"),
            styles: [__webpack_require__(/*! ./supplier.component.scss */ "./src/app/pages/ui-features/suppliers/supplier/supplier.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_token_token_service__WEBPACK_IMPORTED_MODULE_3__["TokenService"],
            _services_suppliers_supplier_service__WEBPACK_IMPORTED_MODULE_2__["SupplierService"]])
    ], SupplierComponent);
    return SupplierComponent;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/suppliers/supplier/suppliers-details/po/po.component.html":
/*!*********************************************************************************************!*\
  !*** ./src/app/pages/ui-features/suppliers/supplier/suppliers-details/po/po.component.html ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <div class=\"row\">\n        <div class=\"col-5\">\n            <p class=\"back\">Company Name & Supplier Name : {{suppliers.CompanyName}} & {{suppliers.SupplierName}}</p>\n        </div>\n    </div>\n    <nb-card-body *ngIf=\"!show\">\n\n        <button *ngIf='moduleEdit || moduleCreate' (click)=\"show = !show\" class=\"btn btn-outline-primary \" shape=\"semi-round\" style=\"float:right; margin-bottom: 15px;\">New Purchase Order</button>\n        <ng2-smart-table [settings]=\"settings\" [source]=\"source\" (deleteConfirm)=\"onDeleteConfirm($event)\" (userRowSelect)=\"onUserRowSelect()\">\n        </ng2-smart-table>\n    </nb-card-body>\n</div>\n\n<div class=\"container-fluid\">\n    <div *ngIf=\"show\">\n        <ngx-purchase></ngx-purchase>\n        <button (click)=\"show = !show\" class=\"btn btn-outline-primary \" shape=\"semi-round\">Go Back</button>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/pages/ui-features/suppliers/supplier/suppliers-details/po/po.component.scss":
/*!*********************************************************************************************!*\
  !*** ./src/app/pages/ui-features/suppliers/supplier/suppliers-details/po/po.component.scss ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ngx-invoices-templates .noPrint {\n  display: none; }\n\n.back {\n  margin-top: 18px; }\n\n/deep/ ng2-smart-table i {\n  color: #212529 !important; }\n\n/deep/ ng2-smart-table .ng2-smart-title {\n  color: white !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3ByYWthc2gvVmlwaW4vaXJvbmJpcmQvMjBGRUIyMDIwL2lyb25iaXJkLWNsaWVudHMvc3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9zdXBwbGllcnMvc3VwcGxpZXIvc3VwcGxpZXJzLWRldGFpbHMvcG8vcG8uY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxhQUFhLEVBQUE7O0FBRWpCO0VBQ0ksZ0JBQWdCLEVBQUE7O0FBR3BCO0VBQ0kseUJBQ0EsRUFBQTs7QUFDQTtFQUNJLHVCQUNBLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9zdXBwbGllcnMvc3VwcGxpZXIvc3VwcGxpZXJzLWRldGFpbHMvcG8vcG8uY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJuZ3gtaW52b2ljZXMtdGVtcGxhdGVzIC5ub1ByaW50e1xuICAgIGRpc3BsYXk6IG5vbmU7XG59XG4uYmFja3tcbiAgICBtYXJnaW4tdG9wOiAxOHB4O1xufVxuXG4vZGVlcC8gbmcyLXNtYXJ0LXRhYmxlICBpe1xuICAgIGNvbG9yOiMyMTI1MjkgIWltcG9ydGFudFxuICAgIH1cbiAgICAvZGVlcC8gbmcyLXNtYXJ0LXRhYmxlIC5uZzItc21hcnQtdGl0bGV7XG4gICAgICAgIGNvbG9yOndoaXRlICFpbXBvcnRhbnRcbiAgICAgICAgfSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/ui-features/suppliers/supplier/suppliers-details/po/po.component.ts":
/*!*******************************************************************************************!*\
  !*** ./src/app/pages/ui-features/suppliers/supplier/suppliers-details/po/po.component.ts ***!
  \*******************************************************************************************/
/*! exports provided: PoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PoComponent", function() { return PoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var _core_data_invoices_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../@core/data/invoices.service */ "./src/app/@core/data/invoices.service.ts");
/* harmony import */ var _services_suppliers_supplier_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../services/suppliers/supplier.service */ "./src/app/services/suppliers/supplier.service.ts");
/* harmony import */ var _services_token_token_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../../services/token/token.service */ "./src/app/services/token/token.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var PoComponent = /** @class */ (function () {
    function PoComponent(service, userServ, supplierServiceServ) {
        this.service = service;
        this.userServ = userServ;
        this.supplierServiceServ = supplierServiceServ;
        this.suppliers = {};
        this.supplierChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.source = new ng2_smart_table__WEBPACK_IMPORTED_MODULE_1__["LocalDataSource"]();
        this.moduleCreate = true;
        this.moduleEdit = true;
        this.moduleView = true;
        var data = this.service.getData();
        this.source.load(data);
        this.getSupplierDetail();
    }
    PoComponent.prototype.onSearch = function (query) {
        if (query === void 0) { query = ''; }
        this.source.setFilter([
            // fields we want to include in the search
            {
                field: 'id',
                search: query
            },
            {
                field: 'customerName',
                search: query
            },
            {
                field: 'siteName',
                search: query
            },
            {
                field: 'agreement',
                search: query
            },
            {
                field: 'payment',
                search: query
            },
            {
                field: 'status',
                search: query
            }
        ], false);
        // second parameter specifying whether to perform 'AND' or 'OR' search 
        // (meaning all columns should contain search query or at least one)
        // 'AND' by default, so changing to 'OR' by setting false here
    };
    PoComponent.prototype.onDeleteConfirm = function (event) {
        if (window.confirm('Are you sure you want to delete?')) {
            event.confirm.resolve();
        }
        else {
            event.confirm.reject();
        }
    };
    PoComponent.prototype.onUserRowSelect = function () {
        if (this.moduleCreate || this.moduleEdit) {
            this.show = !this.show;
        }
    };
    /**
   * Get Supplier Detail
   */
    PoComponent.prototype.getSupplierDetail = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.supplierServiceServ.suppliers
                    .subscribe(function (data) {
                    _this.suppliers = data;
                    _this.id = data.id;
                }, function (error) {
                    console.log(error);
                });
                return [2 /*return*/];
            });
        });
    };
    PoComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.userServ.userAccess !== undefined) {
            this.userServ.userAccess
                .subscribe(function (data) {
                data.forEach(function (ele) {
                    if (ele.name === 'Suppliers') {
                        _this.moduleCreate = ele.create;
                        _this.moduleEdit = ele.edit;
                        _this.moduleView = ele.view;
                    }
                });
            }, function (error) {
                console.log(error);
            });
        }
        if (this.moduleCreate || this.moduleEdit) {
            this.settings = {
                add: {
                    addButtonContent: '<i class="nb-plus"></i>',
                    createButtonContent: '<i class="nb-checkmark"></i>',
                    cancelButtonContent: '<i class="nb-close"></i>',
                },
                edit: {
                    editButtonContent: '<i class="nb-edit"></i>',
                    saveButtonContent: '<i class="nb-checkmark"></i>',
                    cancelButtonContent: '<i class="nb-close"></i>',
                },
                delete: {
                    deleteButtonContent: '<i class="nb-trash"></i>',
                    confirmDelete: true,
                },
                view: {
                    viewButtonContent: '<i  class="nb-edit"></i>'
                },
                actions: {
                    edit: false,
                    add: false,
                    position: 'right'
                },
                columns: {
                    id: {
                        title: 'Invoice id',
                        type: 'number',
                    },
                    date: {
                        title: 'Date',
                        type: 'string',
                    },
                    amount: {
                        title: 'Total Amount',
                        type: 'string',
                    },
                    paid: {
                        title: ' Amount Paid',
                        type: 'string',
                    },
                    balance: {
                        title: 'Remaining Amount',
                        type: 'string',
                    },
                },
            };
        }
        else {
            this.settings = {
                add: {
                    addButtonContent: '<i class="nb-plus"></i>',
                    createButtonContent: '<i class="nb-checkmark"></i>',
                    cancelButtonContent: '<i class="nb-close"></i>',
                },
                edit: {
                    editButtonContent: '<i class="nb-edit"></i>',
                    saveButtonContent: '<i class="nb-checkmark"></i>',
                    cancelButtonContent: '<i class="nb-close"></i>',
                },
                delete: {
                    deleteButtonContent: '<i class="nb-trash"></i>',
                    confirmDelete: true,
                },
                view: {
                    viewButtonContent: '<i  class="nb-edit"></i>'
                },
                actions: {
                    edit: false,
                    add: false,
                    delete: false,
                    position: 'right'
                },
                columns: {
                    id: {
                        title: 'Invoice id',
                        type: 'number',
                    },
                    date: {
                        title: 'Date',
                        type: 'string',
                    },
                    amount: {
                        title: 'Total Amount',
                        type: 'string',
                    },
                    paid: {
                        title: ' Amount Paid',
                        type: 'string',
                    },
                    balance: {
                        title: 'Remaining Amount',
                        type: 'string',
                    },
                },
            };
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], PoComponent.prototype, "supplierChange", void 0);
    PoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-po',
            template: __webpack_require__(/*! ./po.component.html */ "./src/app/pages/ui-features/suppliers/supplier/suppliers-details/po/po.component.html"),
            styles: [__webpack_require__(/*! ./po.component.scss */ "./src/app/pages/ui-features/suppliers/supplier/suppliers-details/po/po.component.scss")]
        }),
        __metadata("design:paramtypes", [_core_data_invoices_service__WEBPACK_IMPORTED_MODULE_2__["InvoicesService"],
            _services_token_token_service__WEBPACK_IMPORTED_MODULE_4__["TokenService"],
            _services_suppliers_supplier_service__WEBPACK_IMPORTED_MODULE_3__["SupplierService"]])
    ], PoComponent);
    return PoComponent;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/suppliers/supplier/suppliers-details/supplier-details/supplier-details.component.html":
/*!*************************************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/suppliers/supplier/suppliers-details/supplier-details/supplier-details.component.html ***!
  \*************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <br>\n    <nb-card-header class=\"text-center\">\n        <div class=\"text-center\"> Supplier Details</div>\n    </nb-card-header>\n    <br>\n    <nb-card-body>\n        <form #SupplierFortm=\"ngForm\" (ngSubmit)=\"editSupplier(SupplierFortm)\">\n            <div>\n                <div class=\"row\">\n\n                    <div class=\"col-6\">\n                        <div class=\"form-group input-group-sm\">\n                            <label for=\"Maintenance\">Supplier Name</label>\n                            <input type=\"text\" class=\"form-control\" id=\"Maintenance\" name=\"SupplierName\" [(ngModel)]=\"suppliers.SupplierName\" placeholder=\"Supplier Name\">\n                        </div>\n                    </div>\n\n                    <div class=\"col-6\">\n                        <div class=\"form-group input-group-sm\">\n                            <label for=\"Maintenance\">Company Name</label>\n                            <input type=\"text\" class=\"form-control\" id=\"Maintenance\" name=\"CompanyName\" [(ngModel)]=\"suppliers.CompanyName\" placeholder=\"Supplier Name\">\n                        </div>\n                    </div>\n\n                </div>\n                <div class=\"row\">\n                    <div class=\"col-6 \">\n                        <label for=\"Maintenance\">Supplier Address</label>\n                        <textarea class=\"form-control\" type=\"text\" name=\"SupplierAddress\" [(ngModel)]=\"suppliers.SupplierAddress\" placeholder=\"Supplier Address\"></textarea>\n                    </div>\n                    <div class=\"col-6 \">\n                        <label for=\"Maintenance\">Supplier Pickup Address</label>\n                        <textarea class=\"form-control\" type=\"text\" name=\"PickupAddress\" [(ngModel)]=\"suppliers.PickupAddress\" placeholder=\"Supplier Pickup Address\"></textarea>\n                    </div>\n                </div><br>\n                <div class=\"row\">\n                    <div class=\"col-3\">\n                        <div class=\"form-group input-group-sm\">\n                            <label for=\"Maintenance\">Contact No</label>\n                            <input type=\"text\" class=\"form-control\" id=\"Maintenance\" name=\"ContactNo\" [(ngModel)]=\"suppliers.ContactNo\" placeholder=\"Contact No\">\n                        </div>\n                    </div>\n                    <div class=\"col-3\">\n                        <div class=\"form-group input-group-sm\">\n                            <label for=\"Date\"> Email id</label>\n                            <input type=\"email\" class=\"form-control\" id=\"Date\" name=\"EmailId\" [(ngModel)]=\"suppliers.EmailId\" placeholder=\"Email id\">\n                        </div>\n                    </div>\n                    <div class=\"col-3\">\n                        <div class=\"form-group input-group-sm\">\n                            <label for=\"Delivary\">GST</label>\n                            <input type=\"text\" class=\"form-control\" id=\"Delivary\" name=\"Gst\" [(ngModel)]=\"suppliers.Gst\" placeholder=\"GST\">\n                        </div>\n                    </div>\n                    <!-- <div class=\"col-3\">\n                        <div class=\"form-group input-group-sm\">\n                            <label for=\"Delivary\">E Sugam No</label>\n                            <input type=\"text\" class=\"form-control\" id=\"Delivary\" name=\"ESugamNo\" [(ngModel)]=\"suppliers.ESugamNo\" placeholder=\"E Sugam No\">\n                        </div>\n                    </div> -->\n                </div>\n                <button *ngIf='moduleCreate' class=\"btn btn-outline-primary \" type=\"button\" shape=\"semi-round\" style=\"float:right;margin-top: 3px;\" (click)=\"componentInsideModalSuppliers.open()\">Add Material</button>\n                <ng2-smart-table [settings]=\"settings\" [source]=\"source\" (userRowSelect)=\"onUserRowSelect($event)\" (createConfirm)=\"onCreateConfirm($event)\">\n                </ng2-smart-table>\n                <br>\n                <div class=\"row foot\" *ngIf='moduleCreate || moduleEdit'>\n                    <div class=\"col-sm-12\" style=\"margin-top: 3px;\">\n                        <button class=\"btn btn-outline-primary \" shape=\"semi-round\" style=\"float: right;\">Save</button>\n                    </div>\n                </div>\n\n            </div>\n        </form>\n    </nb-card-body>\n\n</div>\n\n\n\n\n<modal class=\"modalfirst\" #componentInsideModalSuppliers>\n    <ng-template #modalHeader>\n        <h4>Enter Material Details</h4>\n    </ng-template>\n    <ng-template #modalBody>\n        <form #customerForm=\"ngForm\" (ngSubmit)=\"onCreateConfirm(customerForm);customerForm.reset()\">\n            <div class=\"container\">\n                <div class=\"row\">\n                    <div class=\"col-6\">\n                        <div class=\"form-group input-group-sm\">\n                            <label for=\"description\">Description</label>\n                            <textarea class=\"form-control\" type=\"text\" [(ngModel)]=\"item.description\" name=\"description\" placeholder=\"Description\"></textarea>\n                        </div>\n                    </div>\n                    <div class=\"col-6\">\n                        <div class=\"form-control-group input-group-sm\">\n                            <label class=\"label\" for=\"input-email\">Item Name</label>\n                            <!-- <input type=\"text\" class=\"form-control\" [(ngModel)]=\"item.itemName\" name=\"itemName\" placeholder=\"Item Name\" > -->\n                            <select class=\"form-control\" id=\"Delivary\" size=\"small\" [(ngModel)]=\"item.itemName\" name=\"itemName\" placeholder=\"Item Name\">\n                <option value=\"\" disabled>--Select Item Name--</option>\n                <option *ngFor=\"let list of listOfMaterials;let i = index\" [value]='list.partName'>{{list.partName}}\n                </option>\n              </select>\n                        </div>\n                    </div>\n                    <!-- <div class=\"col-6\">\n            <div class=\"form-control-group input-group-sm\">\n              <label class=\"label\" for=\"input-email\">unit Cost</label>\n              <input type=\"text\" class=\"form-control\" [(ngModel)]=\"item.unit\" name=\"unit\" placeholder=\"unit Cost\">\n            </div>\n          </div> -->\n                </div><br>\n                <div class=\"row\">\n                    <div class=\"col\">\n                        <div class=\"form-control-group input-group-sm\">\n                            <label class=\"label\" for=\"input-email\"> Quantity</label>\n                            <input type=\"number\" class=\"form-control\" (blur)='changeQty($event.target.value)' [(ngModel)]=\"item.quantity\" name=\"quantity\" placeholder=\"Quantity\">\n                        </div>\n                    </div>\n                    <div class=\"col\">\n                        <div class=\"form-control-group input-group-sm\">\n                            <label class=\"label\" for=\"input-email\">Unit Cost</label>\n                            <input type=\"text\" class=\"form-control\" (blur)='changeUnit($event.target.value)' value=\"{{unit}}\" [(ngModel)]=\"item.unit\" name=\"unit\" placeholder=\"unit Cost\">\n                        </div>\n                    </div>\n                    <div class=\"col\">\n                        <div class=\"form-control-group input-group-sm\">\n                            <label class=\"label\" for=\"input-email\">Total Cost</label>\n                            <input type=\"text\" class=\"form-control\" [(ngModel)]=\"total\" value=\"{{total}}\" name=\"total\" placeholder=\"Total Cost\">\n                        </div>\n                    </div>\n\n                </div>\n                <button class=\"btn btn-outline-primary \" type=\"submit\" shape=\"semi-round\" style=\"float:right;margin-top: 3px;\">Save</button>\n            </div>\n        </form>\n\n    </ng-template>\n</modal>\n\n\n\n<modal class=\"modalfirst\" #componentInsideModalSuppliersUpdate>\n    <ng-template #modalHeader>\n        <h4>Update Material Details</h4>\n    </ng-template>\n    <ng-template #modalBody>\n        <form #updateForm=\"ngForm\" (ngSubmit)=\"onUpdateConfirm(updateForm);updateForm.reset()\">\n            <div class=\"container\">\n                <div class=\"row\">\n                    <div class=\"col-6\">\n                        <div class=\"form-control-group input-group-sm\">\n                            <label class=\"label\" for=\"input-email\">Item Name</label>\n                            <input type=\"text\" class=\"form-control\" [(ngModel)]=\"itemName\" value=\"{{itemName}}\" name=\"itemName\" placeholder=\"Item Name\">\n                        </div>\n                    </div>\n                </div><br>\n                <div class=\"row\">\n                    <div class=\"col\">\n                        <div class=\"form-control-group input-group-sm\">\n                            <label class=\"label\" for=\"input-email\"> Quantity</label>\n                            <input type=\"number\" class=\"form-control\" (blur)='changeQty($event.target.value)' [(ngModel)]=\"quantity\" value=\"{{quantity}}\" name=\"quantity\" placeholder=\"Quantity\">\n                        </div>\n                    </div>\n                    <div class=\"col\">\n                        <div class=\"form-control-group input-group-sm\">\n                            <label class=\"label\" for=\"input-email\">Unit Cost</label>\n                            <input type=\"text\" class=\"form-control\" (blur)='changeUnit($event.target.value)' [(ngModel)]=\"unit\" value=\"{{unit}}\" name=\"unit\" placeholder=\"Unit Cost\">\n                        </div>\n                    </div>\n                    <div class=\"col\">\n                        <div class=\"form-control-group input-group-sm\">\n                            <label class=\"label\" for=\"input-email\">Total Cost</label>\n                            <input type=\"text\" class=\"form-control\" [(ngModel)]=\"total\" value=\"{{total}}\" name=\"total\" placeholder=\"Total Cost\">\n                        </div>\n                    </div>\n\n                </div>\n                <button class=\"btn btn-outline-primary \" type=\"submit\" shape=\"semi-round\" style=\"float:right;margin-top: 3px;\">Save</button>\n            </div>\n        </form>\n\n    </ng-template>\n</modal>"

/***/ }),

/***/ "./src/app/pages/ui-features/suppliers/supplier/suppliers-details/supplier-details/supplier-details.component.scss":
/*!*************************************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/suppliers/supplier/suppliers-details/supplier-details/supplier-details.component.scss ***!
  \*************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * This is a starting point where we declare the maps of themes and globally available functions/mixins\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * This mixin generates keyfames.\n * Because of all keyframes can't be scoped,\n * we need to puts unique name in each btn-pulse call.\n */\n/*\n\nAccording to the specification (https://www.w3.org/TR/css-scoping-1/#host-selector)\n:host and :host-context are pseudo-classes. So we assume they could be combined,\nlike other pseudo-classes, even same ones.\nFor example: ':nth-of-type(2n):nth-of-type(even)'.\n\nIdeal solution would be to prepend any selector with :host-context([dir=rtl]).\nThen nebular components will behave as an html element and respond to [dir] attribute on any level,\nso direction could be overridden on any component level.\n\nImplementation code:\n\n@mixin nb-rtl() {\n  // add # to scss interpolation statement.\n  // it works in comments and we can't use it here\n  @at-root {selector-append(':host-context([dir=rtl])', &)} {\n    @content;\n  }\n}\n\nAnd when we call it somewhere:\n\n:host {\n  .some-class {\n    @include nb-rtl() {\n      ...\n    }\n  }\n}\n:host-context(...) {\n  .some-class {\n    @include nb-rtl() {\n      ...\n    }\n  }\n}\n\nResult will look like:\n\n:host-context([dir=rtl]):host .some-class {\n  ...\n}\n:host-context([dir=rtl]):host-context(...) .some-class {\n  ...\n}\n\n*\n  Side note:\n  :host-context():host selector are valid. https://lists.w3.org/Archives/Public/www-style/2015Feb/0305.html\n\n  :host-context([dir=rtl]):host-context(...) should match any permutation,\n  so order is not important.\n*\n\n\nCurrently, there're two problems with this approach:\n\nFirst, is that we can't combine :host, :host-context. Angular bugs #14349, #19199.\nFor the moment of writing, the only possible way is:\n:host {\n  :host-context(...) {\n    ...\n  }\n}\nIt doesn't work for us because mixin could be called somewhere deeper, like:\n:host {\n  p {\n    @include nb-rtl() { ... }\n  }\n}\nWe are not able to go up to :host level to place content passed to mixin.\n\nThe second problem is that we only can be sure that we appending :host-context([dir=rtl]) to another\n:host/:host-context pseudo-class when called in theme files (*.theme.scss).\n  *\n    Side note:\n    Currently, nb-install-component uses another approach where :host prepended with the theme name\n    (https://github.com/angular/angular/blob/5b96078624b0a4760f2dbcf6fdf0bd62791be5bb/packages/compiler/src/shadow_css.ts#L441),\n    but it was made to be able to use current realization of rtl and it can be rewritten back to\n    :host-context($theme) once we will be able to use multiple shadow selectors.\n  *\nBut when it's called in *.component.scss we can't be sure, that selector starts with :host/:host-context,\nbecause angular allows omitting pseudo-classes if we don't need to style :host component itself.\nWe can break such selectors, by just appending :host-context([dir=rtl]) to them.\n  ***\n    Possible solution\n    check if we in theme by some theme variables and if so append, otherwise nest like\n    @at-root :host-context([dir=rtl]) {\n      // add # to scss interpolation statement.\n      // it works in comments and we can't use it here\n      {&} {\n        @content;\n      }\n    }\n    What if :host specified? Can we add space in :host-context(...) :host?\n    Or maybe add :host selector anyway? If multiple :host selectors are allowed\n  ***\n\n\nProblems with the current approach.\n\n1. Direction can be applied only on document level, because mixin prepends theme class,\nwhich placed on the body.\n2. *.component.scss styles should be in :host selector. Otherwise angular will add host\nattribute to [dir=rtl] attribute as well.\n\n\nGeneral problems.\n\nLtr is default document direction, but for proper work of nb-ltr (means ltr only),\n[dir=ltr] should be specified at least somewhere. ':not([dir=rtl]' not applicable here,\nbecause it's satisfy any parent, that don't have [dir=rtl] attribute.\nPrevious approach was to use single rtl mixin and reset ltr properties to initial value.\nBut sometimes it's hard to find, what the previous value should be. And such mixin call looks too verbose.\n*/\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * This mixin generates keyfames.\n * Because of all keyframes can't be scoped,\n * we need to puts unique name in each btn-pulse call.\n */\n/*\n\nAccording to the specification (https://www.w3.org/TR/css-scoping-1/#host-selector)\n:host and :host-context are pseudo-classes. So we assume they could be combined,\nlike other pseudo-classes, even same ones.\nFor example: ':nth-of-type(2n):nth-of-type(even)'.\n\nIdeal solution would be to prepend any selector with :host-context([dir=rtl]).\nThen nebular components will behave as an html element and respond to [dir] attribute on any level,\nso direction could be overridden on any component level.\n\nImplementation code:\n\n@mixin nb-rtl() {\n  // add # to scss interpolation statement.\n  // it works in comments and we can't use it here\n  @at-root {selector-append(':host-context([dir=rtl])', &)} {\n    @content;\n  }\n}\n\nAnd when we call it somewhere:\n\n:host {\n  .some-class {\n    @include nb-rtl() {\n      ...\n    }\n  }\n}\n:host-context(...) {\n  .some-class {\n    @include nb-rtl() {\n      ...\n    }\n  }\n}\n\nResult will look like:\n\n:host-context([dir=rtl]):host .some-class {\n  ...\n}\n:host-context([dir=rtl]):host-context(...) .some-class {\n  ...\n}\n\n*\n  Side note:\n  :host-context():host selector are valid. https://lists.w3.org/Archives/Public/www-style/2015Feb/0305.html\n\n  :host-context([dir=rtl]):host-context(...) should match any permutation,\n  so order is not important.\n*\n\n\nCurrently, there're two problems with this approach:\n\nFirst, is that we can't combine :host, :host-context. Angular bugs #14349, #19199.\nFor the moment of writing, the only possible way is:\n:host {\n  :host-context(...) {\n    ...\n  }\n}\nIt doesn't work for us because mixin could be called somewhere deeper, like:\n:host {\n  p {\n    @include nb-rtl() { ... }\n  }\n}\nWe are not able to go up to :host level to place content passed to mixin.\n\nThe second problem is that we only can be sure that we appending :host-context([dir=rtl]) to another\n:host/:host-context pseudo-class when called in theme files (*.theme.scss).\n  *\n    Side note:\n    Currently, nb-install-component uses another approach where :host prepended with the theme name\n    (https://github.com/angular/angular/blob/5b96078624b0a4760f2dbcf6fdf0bd62791be5bb/packages/compiler/src/shadow_css.ts#L441),\n    but it was made to be able to use current realization of rtl and it can be rewritten back to\n    :host-context($theme) once we will be able to use multiple shadow selectors.\n  *\nBut when it's called in *.component.scss we can't be sure, that selector starts with :host/:host-context,\nbecause angular allows omitting pseudo-classes if we don't need to style :host component itself.\nWe can break such selectors, by just appending :host-context([dir=rtl]) to them.\n  ***\n    Possible solution\n    check if we in theme by some theme variables and if so append, otherwise nest like\n    @at-root :host-context([dir=rtl]) {\n      // add # to scss interpolation statement.\n      // it works in comments and we can't use it here\n      {&} {\n        @content;\n      }\n    }\n    What if :host specified? Can we add space in :host-context(...) :host?\n    Or maybe add :host selector anyway? If multiple :host selectors are allowed\n  ***\n\n\nProblems with the current approach.\n\n1. Direction can be applied only on document level, because mixin prepends theme class,\nwhich placed on the body.\n2. *.component.scss styles should be in :host selector. Otherwise angular will add host\nattribute to [dir=rtl] attribute as well.\n\n\nGeneral problems.\n\nLtr is default document direction, but for proper work of nb-ltr (means ltr only),\n[dir=ltr] should be specified at least somewhere. ':not([dir=rtl]' not applicable here,\nbecause it's satisfy any parent, that don't have [dir=rtl] attribute.\nPrevious approach was to use single rtl mixin and reset ltr properties to initial value.\nBut sometimes it's hard to find, what the previous value should be. And such mixin call looks too verbose.\n*/\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * This mixin generates keyfames.\n * Because of all keyframes can't be scoped,\n * we need to puts unique name in each btn-pulse call.\n */\n/*\n\nAccording to the specification (https://www.w3.org/TR/css-scoping-1/#host-selector)\n:host and :host-context are pseudo-classes. So we assume they could be combined,\nlike other pseudo-classes, even same ones.\nFor example: ':nth-of-type(2n):nth-of-type(even)'.\n\nIdeal solution would be to prepend any selector with :host-context([dir=rtl]).\nThen nebular components will behave as an html element and respond to [dir] attribute on any level,\nso direction could be overridden on any component level.\n\nImplementation code:\n\n@mixin nb-rtl() {\n  // add # to scss interpolation statement.\n  // it works in comments and we can't use it here\n  @at-root {selector-append(':host-context([dir=rtl])', &)} {\n    @content;\n  }\n}\n\nAnd when we call it somewhere:\n\n:host {\n  .some-class {\n    @include nb-rtl() {\n      ...\n    }\n  }\n}\n:host-context(...) {\n  .some-class {\n    @include nb-rtl() {\n      ...\n    }\n  }\n}\n\nResult will look like:\n\n:host-context([dir=rtl]):host .some-class {\n  ...\n}\n:host-context([dir=rtl]):host-context(...) .some-class {\n  ...\n}\n\n*\n  Side note:\n  :host-context():host selector are valid. https://lists.w3.org/Archives/Public/www-style/2015Feb/0305.html\n\n  :host-context([dir=rtl]):host-context(...) should match any permutation,\n  so order is not important.\n*\n\n\nCurrently, there're two problems with this approach:\n\nFirst, is that we can't combine :host, :host-context. Angular bugs #14349, #19199.\nFor the moment of writing, the only possible way is:\n:host {\n  :host-context(...) {\n    ...\n  }\n}\nIt doesn't work for us because mixin could be called somewhere deeper, like:\n:host {\n  p {\n    @include nb-rtl() { ... }\n  }\n}\nWe are not able to go up to :host level to place content passed to mixin.\n\nThe second problem is that we only can be sure that we appending :host-context([dir=rtl]) to another\n:host/:host-context pseudo-class when called in theme files (*.theme.scss).\n  *\n    Side note:\n    Currently, nb-install-component uses another approach where :host prepended with the theme name\n    (https://github.com/angular/angular/blob/5b96078624b0a4760f2dbcf6fdf0bd62791be5bb/packages/compiler/src/shadow_css.ts#L441),\n    but it was made to be able to use current realization of rtl and it can be rewritten back to\n    :host-context($theme) once we will be able to use multiple shadow selectors.\n  *\nBut when it's called in *.component.scss we can't be sure, that selector starts with :host/:host-context,\nbecause angular allows omitting pseudo-classes if we don't need to style :host component itself.\nWe can break such selectors, by just appending :host-context([dir=rtl]) to them.\n  ***\n    Possible solution\n    check if we in theme by some theme variables and if so append, otherwise nest like\n    @at-root :host-context([dir=rtl]) {\n      // add # to scss interpolation statement.\n      // it works in comments and we can't use it here\n      {&} {\n        @content;\n      }\n    }\n    What if :host specified? Can we add space in :host-context(...) :host?\n    Or maybe add :host selector anyway? If multiple :host selectors are allowed\n  ***\n\n\nProblems with the current approach.\n\n1. Direction can be applied only on document level, because mixin prepends theme class,\nwhich placed on the body.\n2. *.component.scss styles should be in :host selector. Otherwise angular will add host\nattribute to [dir=rtl] attribute as well.\n\n\nGeneral problems.\n\nLtr is default document direction, but for proper work of nb-ltr (means ltr only),\n[dir=ltr] should be specified at least somewhere. ':not([dir=rtl]' not applicable here,\nbecause it's satisfy any parent, that don't have [dir=rtl] attribute.\nPrevious approach was to use single rtl mixin and reset ltr properties to initial value.\nBut sometimes it's hard to find, what the previous value should be. And such mixin call looks too verbose.\n*/\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * This mixin generates keyfames.\n * Because of all keyframes can't be scoped,\n * we need to puts unique name in each btn-pulse call.\n */\n/*\n\nAccording to the specification (https://www.w3.org/TR/css-scoping-1/#host-selector)\n:host and :host-context are pseudo-classes. So we assume they could be combined,\nlike other pseudo-classes, even same ones.\nFor example: ':nth-of-type(2n):nth-of-type(even)'.\n\nIdeal solution would be to prepend any selector with :host-context([dir=rtl]).\nThen nebular components will behave as an html element and respond to [dir] attribute on any level,\nso direction could be overridden on any component level.\n\nImplementation code:\n\n@mixin nb-rtl() {\n  // add # to scss interpolation statement.\n  // it works in comments and we can't use it here\n  @at-root {selector-append(':host-context([dir=rtl])', &)} {\n    @content;\n  }\n}\n\nAnd when we call it somewhere:\n\n:host {\n  .some-class {\n    @include nb-rtl() {\n      ...\n    }\n  }\n}\n:host-context(...) {\n  .some-class {\n    @include nb-rtl() {\n      ...\n    }\n  }\n}\n\nResult will look like:\n\n:host-context([dir=rtl]):host .some-class {\n  ...\n}\n:host-context([dir=rtl]):host-context(...) .some-class {\n  ...\n}\n\n*\n  Side note:\n  :host-context():host selector are valid. https://lists.w3.org/Archives/Public/www-style/2015Feb/0305.html\n\n  :host-context([dir=rtl]):host-context(...) should match any permutation,\n  so order is not important.\n*\n\n\nCurrently, there're two problems with this approach:\n\nFirst, is that we can't combine :host, :host-context. Angular bugs #14349, #19199.\nFor the moment of writing, the only possible way is:\n:host {\n  :host-context(...) {\n    ...\n  }\n}\nIt doesn't work for us because mixin could be called somewhere deeper, like:\n:host {\n  p {\n    @include nb-rtl() { ... }\n  }\n}\nWe are not able to go up to :host level to place content passed to mixin.\n\nThe second problem is that we only can be sure that we appending :host-context([dir=rtl]) to another\n:host/:host-context pseudo-class when called in theme files (*.theme.scss).\n  *\n    Side note:\n    Currently, nb-install-component uses another approach where :host prepended with the theme name\n    (https://github.com/angular/angular/blob/5b96078624b0a4760f2dbcf6fdf0bd62791be5bb/packages/compiler/src/shadow_css.ts#L441),\n    but it was made to be able to use current realization of rtl and it can be rewritten back to\n    :host-context($theme) once we will be able to use multiple shadow selectors.\n  *\nBut when it's called in *.component.scss we can't be sure, that selector starts with :host/:host-context,\nbecause angular allows omitting pseudo-classes if we don't need to style :host component itself.\nWe can break such selectors, by just appending :host-context([dir=rtl]) to them.\n  ***\n    Possible solution\n    check if we in theme by some theme variables and if so append, otherwise nest like\n    @at-root :host-context([dir=rtl]) {\n      // add # to scss interpolation statement.\n      // it works in comments and we can't use it here\n      {&} {\n        @content;\n      }\n    }\n    What if :host specified? Can we add space in :host-context(...) :host?\n    Or maybe add :host selector anyway? If multiple :host selectors are allowed\n  ***\n\n\nProblems with the current approach.\n\n1. Direction can be applied only on document level, because mixin prepends theme class,\nwhich placed on the body.\n2. *.component.scss styles should be in :host selector. Otherwise angular will add host\nattribute to [dir=rtl] attribute as well.\n\n\nGeneral problems.\n\nLtr is default document direction, but for proper work of nb-ltr (means ltr only),\n[dir=ltr] should be specified at least somewhere. ':not([dir=rtl]' not applicable here,\nbecause it's satisfy any parent, that don't have [dir=rtl] attribute.\nPrevious approach was to use single rtl mixin and reset ltr properties to initial value.\nBut sometimes it's hard to find, what the previous value should be. And such mixin call looks too verbose.\n*/\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * This mixin generates keyfames.\n * Because of all keyframes can't be scoped,\n * we need to puts unique name in each btn-pulse call.\n */\n/*\n\nAccording to the specification (https://www.w3.org/TR/css-scoping-1/#host-selector)\n:host and :host-context are pseudo-classes. So we assume they could be combined,\nlike other pseudo-classes, even same ones.\nFor example: ':nth-of-type(2n):nth-of-type(even)'.\n\nIdeal solution would be to prepend any selector with :host-context([dir=rtl]).\nThen nebular components will behave as an html element and respond to [dir] attribute on any level,\nso direction could be overridden on any component level.\n\nImplementation code:\n\n@mixin nb-rtl() {\n  // add # to scss interpolation statement.\n  // it works in comments and we can't use it here\n  @at-root {selector-append(':host-context([dir=rtl])', &)} {\n    @content;\n  }\n}\n\nAnd when we call it somewhere:\n\n:host {\n  .some-class {\n    @include nb-rtl() {\n      ...\n    }\n  }\n}\n:host-context(...) {\n  .some-class {\n    @include nb-rtl() {\n      ...\n    }\n  }\n}\n\nResult will look like:\n\n:host-context([dir=rtl]):host .some-class {\n  ...\n}\n:host-context([dir=rtl]):host-context(...) .some-class {\n  ...\n}\n\n*\n  Side note:\n  :host-context():host selector are valid. https://lists.w3.org/Archives/Public/www-style/2015Feb/0305.html\n\n  :host-context([dir=rtl]):host-context(...) should match any permutation,\n  so order is not important.\n*\n\n\nCurrently, there're two problems with this approach:\n\nFirst, is that we can't combine :host, :host-context. Angular bugs #14349, #19199.\nFor the moment of writing, the only possible way is:\n:host {\n  :host-context(...) {\n    ...\n  }\n}\nIt doesn't work for us because mixin could be called somewhere deeper, like:\n:host {\n  p {\n    @include nb-rtl() { ... }\n  }\n}\nWe are not able to go up to :host level to place content passed to mixin.\n\nThe second problem is that we only can be sure that we appending :host-context([dir=rtl]) to another\n:host/:host-context pseudo-class when called in theme files (*.theme.scss).\n  *\n    Side note:\n    Currently, nb-install-component uses another approach where :host prepended with the theme name\n    (https://github.com/angular/angular/blob/5b96078624b0a4760f2dbcf6fdf0bd62791be5bb/packages/compiler/src/shadow_css.ts#L441),\n    but it was made to be able to use current realization of rtl and it can be rewritten back to\n    :host-context($theme) once we will be able to use multiple shadow selectors.\n  *\nBut when it's called in *.component.scss we can't be sure, that selector starts with :host/:host-context,\nbecause angular allows omitting pseudo-classes if we don't need to style :host component itself.\nWe can break such selectors, by just appending :host-context([dir=rtl]) to them.\n  ***\n    Possible solution\n    check if we in theme by some theme variables and if so append, otherwise nest like\n    @at-root :host-context([dir=rtl]) {\n      // add # to scss interpolation statement.\n      // it works in comments and we can't use it here\n      {&} {\n        @content;\n      }\n    }\n    What if :host specified? Can we add space in :host-context(...) :host?\n    Or maybe add :host selector anyway? If multiple :host selectors are allowed\n  ***\n\n\nProblems with the current approach.\n\n1. Direction can be applied only on document level, because mixin prepends theme class,\nwhich placed on the body.\n2. *.component.scss styles should be in :host selector. Otherwise angular will add host\nattribute to [dir=rtl] attribute as well.\n\n\nGeneral problems.\n\nLtr is default document direction, but for proper work of nb-ltr (means ltr only),\n[dir=ltr] should be specified at least somewhere. ':not([dir=rtl]' not applicable here,\nbecause it's satisfy any parent, that don't have [dir=rtl] attribute.\nPrevious approach was to use single rtl mixin and reset ltr properties to initial value.\nBut sometimes it's hard to find, what the previous value should be. And such mixin call looks too verbose.\n*/\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * This mixin generates keyfames.\n * Because of all keyframes can't be scoped,\n * we need to puts unique name in each btn-pulse call.\n */\n/*\n\nAccording to the specification (https://www.w3.org/TR/css-scoping-1/#host-selector)\n:host and :host-context are pseudo-classes. So we assume they could be combined,\nlike other pseudo-classes, even same ones.\nFor example: ':nth-of-type(2n):nth-of-type(even)'.\n\nIdeal solution would be to prepend any selector with :host-context([dir=rtl]).\nThen nebular components will behave as an html element and respond to [dir] attribute on any level,\nso direction could be overridden on any component level.\n\nImplementation code:\n\n@mixin nb-rtl() {\n  // add # to scss interpolation statement.\n  // it works in comments and we can't use it here\n  @at-root {selector-append(':host-context([dir=rtl])', &)} {\n    @content;\n  }\n}\n\nAnd when we call it somewhere:\n\n:host {\n  .some-class {\n    @include nb-rtl() {\n      ...\n    }\n  }\n}\n:host-context(...) {\n  .some-class {\n    @include nb-rtl() {\n      ...\n    }\n  }\n}\n\nResult will look like:\n\n:host-context([dir=rtl]):host .some-class {\n  ...\n}\n:host-context([dir=rtl]):host-context(...) .some-class {\n  ...\n}\n\n*\n  Side note:\n  :host-context():host selector are valid. https://lists.w3.org/Archives/Public/www-style/2015Feb/0305.html\n\n  :host-context([dir=rtl]):host-context(...) should match any permutation,\n  so order is not important.\n*\n\n\nCurrently, there're two problems with this approach:\n\nFirst, is that we can't combine :host, :host-context. Angular bugs #14349, #19199.\nFor the moment of writing, the only possible way is:\n:host {\n  :host-context(...) {\n    ...\n  }\n}\nIt doesn't work for us because mixin could be called somewhere deeper, like:\n:host {\n  p {\n    @include nb-rtl() { ... }\n  }\n}\nWe are not able to go up to :host level to place content passed to mixin.\n\nThe second problem is that we only can be sure that we appending :host-context([dir=rtl]) to another\n:host/:host-context pseudo-class when called in theme files (*.theme.scss).\n  *\n    Side note:\n    Currently, nb-install-component uses another approach where :host prepended with the theme name\n    (https://github.com/angular/angular/blob/5b96078624b0a4760f2dbcf6fdf0bd62791be5bb/packages/compiler/src/shadow_css.ts#L441),\n    but it was made to be able to use current realization of rtl and it can be rewritten back to\n    :host-context($theme) once we will be able to use multiple shadow selectors.\n  *\nBut when it's called in *.component.scss we can't be sure, that selector starts with :host/:host-context,\nbecause angular allows omitting pseudo-classes if we don't need to style :host component itself.\nWe can break such selectors, by just appending :host-context([dir=rtl]) to them.\n  ***\n    Possible solution\n    check if we in theme by some theme variables and if so append, otherwise nest like\n    @at-root :host-context([dir=rtl]) {\n      // add # to scss interpolation statement.\n      // it works in comments and we can't use it here\n      {&} {\n        @content;\n      }\n    }\n    What if :host specified? Can we add space in :host-context(...) :host?\n    Or maybe add :host selector anyway? If multiple :host selectors are allowed\n  ***\n\n\nProblems with the current approach.\n\n1. Direction can be applied only on document level, because mixin prepends theme class,\nwhich placed on the body.\n2. *.component.scss styles should be in :host selector. Otherwise angular will add host\nattribute to [dir=rtl] attribute as well.\n\n\nGeneral problems.\n\nLtr is default document direction, but for proper work of nb-ltr (means ltr only),\n[dir=ltr] should be specified at least somewhere. ':not([dir=rtl]' not applicable here,\nbecause it's satisfy any parent, that don't have [dir=rtl] attribute.\nPrevious approach was to use single rtl mixin and reset ltr properties to initial value.\nBut sometimes it's hard to find, what the previous value should be. And such mixin call looks too verbose.\n*/\n/*\n      :host can be prefixed\n      https://github.com/angular/angular/blob/8d0ee34939f14c07876d222c25b405ed458a34d3/packages/compiler/src/shadow_css.ts#L441\n\n      We have to use :host insted of :host-context($theme), to be able to prefix theme class\n      with something defined inside of @content, by prefixing &.\n      For example this scss code:\n        .nb-theme-default {\n          .some-selector & {\n            ...\n          }\n        }\n      Will result in next css:\n        .some-selector .nb-theme-default {\n          ...\n        }\n\n      It doesn't work with :host-context because angular splitting it in two selectors and removes\n      prefix in one of the selectors.\n    */\n.nb-theme-default :host .full-width {\n  -webkit-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1;\n  min-width: 220px; }\n.nb-theme-default :host nb-checkbox {\n  margin-bottom: 1rem; }\n[dir=ltr] .nb-theme-default :host .form-inline > * {\n  margin: 0 1.5rem 1.5rem 0; }\n[dir=rtl] .nb-theme-default :host .form-inline > * {\n  margin: 0 0 1.5rem 1.5rem; }\n.nb-theme-default :host nb-card.inline-form-card nb-card-body {\n  padding-bottom: 0; }\n/*\n      :host can be prefixed\n      https://github.com/angular/angular/blob/8d0ee34939f14c07876d222c25b405ed458a34d3/packages/compiler/src/shadow_css.ts#L441\n\n      We have to use :host insted of :host-context($theme), to be able to prefix theme class\n      with something defined inside of @content, by prefixing &.\n      For example this scss code:\n        .nb-theme-default {\n          .some-selector & {\n            ...\n          }\n        }\n      Will result in next css:\n        .some-selector .nb-theme-default {\n          ...\n        }\n\n      It doesn't work with :host-context because angular splitting it in two selectors and removes\n      prefix in one of the selectors.\n    */\n.nb-theme-cosmic :host .full-width {\n  -webkit-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1;\n  min-width: 220px; }\n.nb-theme-cosmic :host nb-checkbox {\n  margin-bottom: 1rem; }\n[dir=ltr] .nb-theme-cosmic :host .form-inline > * {\n  margin: 0 1.5rem 1.5rem 0; }\n[dir=rtl] .nb-theme-cosmic :host .form-inline > * {\n  margin: 0 0 1.5rem 1.5rem; }\n.nb-theme-cosmic :host nb-card.inline-form-card nb-card-body {\n  padding-bottom: 0; }\n/*\n      :host can be prefixed\n      https://github.com/angular/angular/blob/8d0ee34939f14c07876d222c25b405ed458a34d3/packages/compiler/src/shadow_css.ts#L441\n\n      We have to use :host insted of :host-context($theme), to be able to prefix theme class\n      with something defined inside of @content, by prefixing &.\n      For example this scss code:\n        .nb-theme-default {\n          .some-selector & {\n            ...\n          }\n        }\n      Will result in next css:\n        .some-selector .nb-theme-default {\n          ...\n        }\n\n      It doesn't work with :host-context because angular splitting it in two selectors and removes\n      prefix in one of the selectors.\n    */\n.nb-theme-corporate :host .full-width {\n  -webkit-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1;\n  min-width: 220px; }\n.nb-theme-corporate :host nb-checkbox {\n  margin-bottom: 1rem; }\n[dir=ltr] .nb-theme-corporate :host .form-inline > * {\n  margin: 0 1.5rem 1.5rem 0; }\n[dir=rtl] .nb-theme-corporate :host .form-inline > * {\n  margin: 0 0 1.5rem 1.5rem; }\n.nb-theme-corporate :host nb-card.inline-form-card nb-card-body {\n  padding-bottom: 0; }\n.hi {\n  border: 1px solid;\n  /* border-bottom-left-radius: 16px; */\n  /* border-bottom-right-radius: 16px; */\n  /* border-top-left-radius: 16px; */\n  /* border-top-right-radius: 16px; */ }\n.border-left-right {\n  border: 1px solid;\n  border-bottom-left-radius: 16px; }\n.border-top-right {\n  border: 1px solid;\n  border-top-left-radius: 16px; }\n.border-top-left {\n  border: 1px solid;\n  border-top-right-radius: 16px; }\n.border-bottom-left {\n  border: 1px solid;\n  border-bottom-right-radius: 16px; }\ntable {\n  width: -webkit-fill-available; }\na {\n  cursor: pointer; }\ntextarea {\n  border: none; }\n/deep/ ng2-smart-table .ng2-smart-title {\n  color: white !important; }\n/deep/ ng2-smart-table i {\n  color: #212529 !important; }\n/deep/ modal .modal-dialog {\n  max-width: 1000px !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3ByYWthc2gvVmlwaW4vaXJvbmJpcmQvMjBGRUIyMDIwL2lyb25iaXJkLWNsaWVudHMvbm9kZV9tb2R1bGVzL0BuZWJ1bGFyL3RoZW1lL3N0eWxlcy9fdGhlbWluZy5zY3NzIiwic3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9zdXBwbGllcnMvc3VwcGxpZXIvc3VwcGxpZXJzLWRldGFpbHMvc3VwcGxpZXItZGV0YWlscy9zdXBwbGllci1kZXRhaWxzLmNvbXBvbmVudC5zY3NzIiwiL2hvbWUvcHJha2FzaC9WaXBpbi9pcm9uYmlyZC8yMEZFQjIwMjAvaXJvbmJpcmQtY2xpZW50cy9ub2RlX21vZHVsZXMvQG5lYnVsYXIvdGhlbWUvc3R5bGVzL2NvcmUvX21peGlucy5zY3NzIiwiL2hvbWUvcHJha2FzaC9WaXBpbi9pcm9uYmlyZC8yMEZFQjIwMjAvaXJvbmJpcmQtY2xpZW50cy9ub2RlX21vZHVsZXMvQG5lYnVsYXIvdGhlbWUvc3R5bGVzL2NvcmUvX2Z1bmN0aW9ucy5zY3NzIiwiL2hvbWUvcHJha2FzaC9WaXBpbi9pcm9uYmlyZC8yMEZFQjIwMjAvaXJvbmJpcmQtY2xpZW50cy9ub2RlX21vZHVsZXMvQG5lYnVsYXIvdGhlbWUvc3R5bGVzL3RoZW1lcy9fZGVmYXVsdC5zY3NzIiwiL2hvbWUvcHJha2FzaC9WaXBpbi9pcm9uYmlyZC8yMEZFQjIwMjAvaXJvbmJpcmQtY2xpZW50cy9ub2RlX21vZHVsZXMvQG5lYnVsYXIvdGhlbWUvc3R5bGVzL3RoZW1lcy9fY29zbWljLnNjc3MiLCIvaG9tZS9wcmFrYXNoL1ZpcGluL2lyb25iaXJkLzIwRkVCMjAyMC9pcm9uYmlyZC1jbGllbnRzL25vZGVfbW9kdWxlcy9AbmVidWxhci90aGVtZS9zdHlsZXMvdGhlbWVzL19jb3Jwb3JhdGUuc2NzcyIsIi9ob21lL3ByYWthc2gvVmlwaW4vaXJvbmJpcmQvMjBGRUIyMDIwL2lyb25iaXJkLWNsaWVudHMvc3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9zdXBwbGllcnMvc3VwcGxpZXIvc3VwcGxpZXJzLWRldGFpbHMvc3VwcGxpZXItZGV0YWlscy9zdXBwbGllci1kZXRhaWxzLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOzs7O0VDSUU7QURHRjs7RUNBRTtBQ1BGOzs7O0VEWUU7QUM4SkY7Ozs7RUR6SkU7QUNtTEY7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Q0QvREM7QUVySUQ7Ozs7RUYwSUU7QUcxSUY7Ozs7RUgrSUU7QUUvSUY7Ozs7RUZvSkU7QUNwSkY7Ozs7RUR5SkU7QUNpQkY7Ozs7RURaRTtBQ3NDRjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztDRDhFQztBSWxSRDs7OztFSnVSRTtBRXZSRjs7OztFRjRSRTtBQzVSRjs7OztFRGlTRTtBQ3ZIRjs7OztFRDRIRTtBQ2xHRjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztDRHNOQztBRzFaRDs7OztFSCtaRTtBRS9aRjs7OztFRm9hRTtBQ3BhRjs7OztFRHlhRTtBQy9QRjs7OztFRG9RRTtBQzFPRjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztDRDhWQztBS2xpQkQ7Ozs7RUx1aUJFO0FFdmlCRjs7OztFRjRpQkU7QUM1aUJGOzs7O0VEaWpCRTtBQ3ZZRjs7OztFRDRZRTtBQ2xYRjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztDRHNlQztBRzFxQkQ7Ozs7RUgrcUJFO0FFL3FCRjs7OztFRm9yQkU7QUNwckJGOzs7O0VEeXJCRTtBQy9nQkY7Ozs7RURvaEJFO0FDMWZGOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0NEOG1CQztBRGhzQkc7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7S0NvdEJDO0FNbjBCSDtFQUNFLG1CQUFPO01BQVAsV0FBTztVQUFQLE9BQU87RUFDUCxnQkFBZ0IsRUFBQTtBQUdsQjtFQUNFLG1CQUFtQixFQUFBO0FOcTBCdkI7RUNsaEJNLHlCSy9TdUMsRUFBQTtBTm8wQjdDO0VDcmhCTSx5Qks5U3VDLEVBQUE7QUFHM0M7RUFDRSxpQkFBaUIsRUFBQTtBUGdHakI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7S0N3dkJDO0FNdjJCSDtFQUNFLG1CQUFPO01BQVAsV0FBTztVQUFQLE9BQU87RUFDUCxnQkFBZ0IsRUFBQTtBQUdsQjtFQUNFLG1CQUFtQixFQUFBO0FOeTJCdkI7RUN0akJNLHlCSy9TdUMsRUFBQTtBTncyQjdDO0VDempCTSx5Qks5U3VDLEVBQUE7QUFHM0M7RUFDRSxpQkFBaUIsRUFBQTtBUGdHakI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7S0M0eEJDO0FNMzRCSDtFQUNFLG1CQUFPO01BQVAsV0FBTztVQUFQLE9BQU87RUFDUCxnQkFBZ0IsRUFBQTtBQUdsQjtFQUNFLG1CQUFtQixFQUFBO0FONjRCdkI7RUMxbEJNLHlCSy9TdUMsRUFBQTtBTjQ0QjdDO0VDN2xCTSx5Qks5U3VDLEVBQUE7QUFHM0M7RUFDRSxpQkFBaUIsRUFBQTtBQUlyQjtFQUNFLGlCQUFpQjtFQUNqQixxQ0FBQTtFQUNBLHNDQUFBO0VBQ0Esa0NBQUE7RUFDQSxtQ0FBQSxFQUFvQztBQUd0QztFQUNFLGlCQUFpQjtFQUNqQiwrQkFBK0IsRUFBQTtBQUdqQztFQUNFLGlCQUFpQjtFQUNqQiw0QkFBNEIsRUFBQTtBQUc5QjtFQUNFLGlCQUFpQjtFQUNqQiw2QkFBNkIsRUFBQTtBQUcvQjtFQUNFLGlCQUFpQjtFQUNqQixnQ0FBZ0MsRUFBQTtBQUdsQztFQUNFLDZCQUE2QixFQUFBO0FBRy9CO0VBQ0UsZUFBZSxFQUFBO0FBR2pCO0VBQ0UsWUFBWSxFQUFBO0FBR2Q7RUFDRSx1QkFDRixFQUFBO0FBRUE7RUFDRSx5QkFDRixFQUFBO0FBRUE7RUFDRSw0QkFBNEIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VpLWZlYXR1cmVzL3N1cHBsaWVycy9zdXBwbGllci9zdXBwbGllcnMtZGV0YWlscy9zdXBwbGllci1kZXRhaWxzL3N1cHBsaWVyLWRldGFpbHMuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG5cblxuLyoqXG4gKiBUaGlzIGlzIGEgc3RhcnRpbmcgcG9pbnQgd2hlcmUgd2UgZGVjbGFyZSB0aGUgbWFwcyBvZiB0aGVtZXMgYW5kIGdsb2JhbGx5IGF2YWlsYWJsZSBmdW5jdGlvbnMvbWl4aW5zXG4gKi9cblxuQGltcG9ydCAnY29yZS9taXhpbnMnO1xuQGltcG9ydCAnY29yZS9mdW5jdGlvbnMnO1xuXG4kbmItZW5hYmxlZC10aGVtZXM6ICgpICFnbG9iYWw7XG4kbmItZW5hYmxlLWNzcy12YXJpYWJsZXM6IGZhbHNlICFnbG9iYWw7XG5cbiRuYi10aGVtZXM6ICgpICFnbG9iYWw7XG4kbmItdGhlbWVzLW5vbi1wcm9jZXNzZWQ6ICgpICFnbG9iYWw7XG4kbmItdGhlbWVzLWV4cG9ydDogKCkgIWdsb2JhbDtcblxuQGZ1bmN0aW9uIG5iLXRoZW1lKCRrZXkpIHtcbiAgQHJldHVybiBtYXAtZ2V0KCR0aGVtZSwgJGtleSk7XG59XG5cbkBmdW5jdGlvbiBuYi1nZXQtdmFsdWUoJHRoZW1lLCAka2V5LCAkdmFsdWUpIHtcbiAgQGlmICh0eXBlLW9mKCR2YWx1ZSkgPT0gJ3N0cmluZycpIHtcbiAgICAkdG1wOiBtYXAtZ2V0KCR0aGVtZSwgJHZhbHVlKTtcblxuICAgIEBpZiAoJHRtcCAhPSBudWxsKSB7XG4gICAgICBAcmV0dXJuIG5iLWdldC12YWx1ZSgkdGhlbWUsICR2YWx1ZSwgJHRtcCk7XG4gICAgfVxuICB9XG5cbiAgQHJldHVybiBtYXAtZ2V0KCR0aGVtZSwgJGtleSk7XG59XG5cbkBmdW5jdGlvbiBjb252ZXJ0LXRvLWNzcy12YXJpYWJsZXMoJHZhcmlhYmxlcykge1xuICAkcmVzdWx0OiAoKTtcbiAgQGVhY2ggJHZhciwgJHZhbHVlIGluICR2YXJpYWJsZXMge1xuICAgICRyZXN1bHQ6IG1hcC1zZXQoJHJlc3VsdCwgJHZhciwgJy0tdmFyKCN7JHZhcn0pJyk7XG4gIH1cblxuICBAZGVidWcgJHJlc3VsdDtcbiAgQHJldHVybiAkcmVzdWx0O1xufVxuXG5AZnVuY3Rpb24gc2V0LWdsb2JhbC10aGVtZS12YXJzKCR0aGVtZSwgJHRoZW1lLW5hbWUpIHtcbiAgJHRoZW1lOiAkdGhlbWUgIWdsb2JhbDtcbiAgJHRoZW1lLW5hbWU6ICR0aGVtZS1uYW1lICFnbG9iYWw7XG4gIEBpZiAoJG5iLWVuYWJsZS1jc3MtdmFyaWFibGVzKSB7XG4gICAgJHRoZW1lOiBjb252ZXJ0LXRvLWNzcy12YXJpYWJsZXMoJHRoZW1lKSAhZ2xvYmFsO1xuICB9XG4gIEByZXR1cm4gJHRoZW1lO1xufVxuXG5AZnVuY3Rpb24gbmItcmVnaXN0ZXItdGhlbWUoJHRoZW1lLCAkbmFtZSwgJGRlZmF1bHQ6IG51bGwpIHtcblxuICAkdGhlbWUtZGF0YTogKCk7XG5cblxuICBAaWYgKCRkZWZhdWx0ICE9IG51bGwpIHtcblxuICAgICR0aGVtZTogbWFwLW1lcmdlKG1hcC1nZXQoJG5iLXRoZW1lcy1ub24tcHJvY2Vzc2VkLCAkZGVmYXVsdCksICR0aGVtZSk7XG4gICAgJG5iLXRoZW1lcy1ub24tcHJvY2Vzc2VkOiBtYXAtc2V0KCRuYi10aGVtZXMtbm9uLXByb2Nlc3NlZCwgJG5hbWUsICR0aGVtZSkgIWdsb2JhbDtcblxuICAgICR0aGVtZS1kYXRhOiBtYXAtc2V0KCR0aGVtZS1kYXRhLCBkYXRhLCAkdGhlbWUpO1xuICAgICRuYi10aGVtZXMtZXhwb3J0OiBtYXAtc2V0KCRuYi10aGVtZXMtZXhwb3J0LCAkbmFtZSwgbWFwLXNldCgkdGhlbWUtZGF0YSwgcGFyZW50LCAkZGVmYXVsdCkpICFnbG9iYWw7XG5cbiAgfSBAZWxzZSB7XG4gICAgJG5iLXRoZW1lcy1ub24tcHJvY2Vzc2VkOiBtYXAtc2V0KCRuYi10aGVtZXMtbm9uLXByb2Nlc3NlZCwgJG5hbWUsICR0aGVtZSkgIWdsb2JhbDtcblxuICAgICR0aGVtZS1kYXRhOiBtYXAtc2V0KCR0aGVtZS1kYXRhLCBkYXRhLCAkdGhlbWUpO1xuICAgICRuYi10aGVtZXMtZXhwb3J0OiBtYXAtc2V0KCRuYi10aGVtZXMtZXhwb3J0LCAkbmFtZSwgbWFwLXNldCgkdGhlbWUtZGF0YSwgcGFyZW50LCBudWxsKSkgIWdsb2JhbDtcbiAgfVxuXG4gICR0aGVtZS1wYXJzZWQ6ICgpO1xuICBAZWFjaCAka2V5LCAkdmFsdWUgaW4gJHRoZW1lIHtcbiAgICAkdGhlbWUtcGFyc2VkOiBtYXAtc2V0KCR0aGVtZS1wYXJzZWQsICRrZXksIG5iLWdldC12YWx1ZSgkdGhlbWUsICRrZXksICR2YWx1ZSkpO1xuICB9XG5cbiAgLy8gZW5hYmxlIHJpZ2h0IGF3YXkgd2hlbiBpbnN0YWxsZWRcbiAgJHRoZW1lLXBhcnNlZDogc2V0LWdsb2JhbC10aGVtZS12YXJzKCR0aGVtZS1wYXJzZWQsICRuYW1lKTtcbiAgQHJldHVybiBtYXAtc2V0KCRuYi10aGVtZXMsICRuYW1lLCAkdGhlbWUtcGFyc2VkKTtcbn1cblxuQGZ1bmN0aW9uIGdldC1lbmFibGVkLXRoZW1lcygpIHtcbiAgJHRoZW1lcy10by1pbnN0YWxsOiAoKTtcblxuICBAaWYgKGxlbmd0aCgkbmItZW5hYmxlZC10aGVtZXMpID4gMCkge1xuICAgIEBlYWNoICR0aGVtZS1uYW1lIGluICRuYi1lbmFibGVkLXRoZW1lcyB7XG4gICAgICAkdGhlbWVzLXRvLWluc3RhbGw6IG1hcC1zZXQoJHRoZW1lcy10by1pbnN0YWxsLCAkdGhlbWUtbmFtZSwgbWFwLWdldCgkbmItdGhlbWVzLCAkdGhlbWUtbmFtZSkpO1xuICAgIH1cbiAgfSBAZWxzZSB7XG4gICAgJHRoZW1lcy10by1pbnN0YWxsOiAkbmItdGhlbWVzO1xuICB9XG5cbiAgQHJldHVybiAkdGhlbWVzLXRvLWluc3RhbGw7XG59XG5cbkBtaXhpbiBpbnN0YWxsLWNzcy12YXJpYWJsZXMoJHRoZW1lLW5hbWUsICR2YXJpYWJsZXMpIHtcbiAgLm5iLXRoZW1lLSN7JHRoZW1lLW5hbWV9IHtcbiAgICBAZWFjaCAkdmFyLCAkdmFsdWUgaW4gJHZhcmlhYmxlcyB7XG4gICAgICAtLSN7JHZhcn06ICR2YWx1ZTtcbiAgICB9XG4gIH1cbn1cblxuLy8gVE9ETzogd2UgaGlkZSA6aG9zdCBpbnNpZGUgb2YgaXQgd2hpY2ggaXMgbm90IG9idmlvdXNcbkBtaXhpbiBuYi1pbnN0YWxsLWNvbXBvbmVudCgpIHtcblxuICAkdGhlbWVzLXRvLWluc3RhbGw6IGdldC1lbmFibGVkLXRoZW1lcygpO1xuXG4gIEBlYWNoICR0aGVtZS1uYW1lLCAkdGhlbWUgaW4gJHRoZW1lcy10by1pbnN0YWxsIHtcbiAgICAvKlxuICAgICAgOmhvc3QgY2FuIGJlIHByZWZpeGVkXG4gICAgICBodHRwczovL2dpdGh1Yi5jb20vYW5ndWxhci9hbmd1bGFyL2Jsb2IvOGQwZWUzNDkzOWYxNGMwNzg3NmQyMjJjMjViNDA1ZWQ0NThhMzRkMy9wYWNrYWdlcy9jb21waWxlci9zcmMvc2hhZG93X2Nzcy50cyNMNDQxXG5cbiAgICAgIFdlIGhhdmUgdG8gdXNlIDpob3N0IGluc3RlZCBvZiA6aG9zdC1jb250ZXh0KCR0aGVtZSksIHRvIGJlIGFibGUgdG8gcHJlZml4IHRoZW1lIGNsYXNzXG4gICAgICB3aXRoIHNvbWV0aGluZyBkZWZpbmVkIGluc2lkZSBvZiBAY29udGVudCwgYnkgcHJlZml4aW5nICYuXG4gICAgICBGb3IgZXhhbXBsZSB0aGlzIHNjc3MgY29kZTpcbiAgICAgICAgLm5iLXRoZW1lLWRlZmF1bHQge1xuICAgICAgICAgIC5zb21lLXNlbGVjdG9yICYge1xuICAgICAgICAgICAgLi4uXG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICBXaWxsIHJlc3VsdCBpbiBuZXh0IGNzczpcbiAgICAgICAgLnNvbWUtc2VsZWN0b3IgLm5iLXRoZW1lLWRlZmF1bHQge1xuICAgICAgICAgIC4uLlxuICAgICAgICB9XG5cbiAgICAgIEl0IGRvZXNuJ3Qgd29yayB3aXRoIDpob3N0LWNvbnRleHQgYmVjYXVzZSBhbmd1bGFyIHNwbGl0dGluZyBpdCBpbiB0d28gc2VsZWN0b3JzIGFuZCByZW1vdmVzXG4gICAgICBwcmVmaXggaW4gb25lIG9mIHRoZSBzZWxlY3RvcnMuXG4gICAgKi9cbiAgICAubmItdGhlbWUtI3skdGhlbWUtbmFtZX0gOmhvc3Qge1xuICAgICAgJHRoZW1lOiBzZXQtZ2xvYmFsLXRoZW1lLXZhcnMoJHRoZW1lLCAkdGhlbWUtbmFtZSk7XG4gICAgICBAY29udGVudDtcbiAgICB9XG4gIH1cbn1cblxuQG1peGluIG5iLWZvci10aGVtZSgkbmFtZSkge1xuICBAaWYgKCR0aGVtZS1uYW1lID09ICRuYW1lKSB7XG4gICAgQGNvbnRlbnQ7XG4gIH1cbn1cblxuQG1peGluIG5iLWV4Y2VwdC10aGVtZSgkbmFtZSkge1xuICBAaWYgKCR0aGVtZS1uYW1lICE9ICRuYW1lKSB7XG4gICAgQGNvbnRlbnQ7XG4gIH1cbn1cblxuLy8gVE9ETzogYW5vdGhlciBtaXhpbmcgZm9yIHRoZSBhbG1vc3Qgc2FtZSB0aGluZ1xuQG1peGluIG5iLWluc3RhbGwtcm9vdC1jb21wb25lbnQoKSB7XG4gIEB3YXJuICdgbmItaW5zdGFsbC1yb290LWNvbXBvbmVudGAgaXMgZGVwcmljYXRlZCwgcmVwbGFjZSB3aXRoIGBuYi1pbnN0YWxsLWNvbXBvbmVudGAsIGFzIGBib2R5YCBpcyByb290IGVsZW1lbnQgbm93JztcblxuICBAaW5jbHVkZSBuYi1pbnN0YWxsLWNvbXBvbmVudCgpIHtcbiAgICBAY29udGVudDtcbiAgfVxufVxuXG5AbWl4aW4gbmItaW5zdGFsbC1nbG9iYWwoKSB7XG4gICR0aGVtZXMtdG8taW5zdGFsbDogZ2V0LWVuYWJsZWQtdGhlbWVzKCk7XG5cbiAgQGVhY2ggJHRoZW1lLW5hbWUsICR0aGVtZSBpbiAkdGhlbWVzLXRvLWluc3RhbGwge1xuICAgIC5uYi10aGVtZS0jeyR0aGVtZS1uYW1lfSB7XG4gICAgICAkdGhlbWU6IHNldC1nbG9iYWwtdGhlbWUtdmFycygkdGhlbWUsICR0aGVtZS1uYW1lKTtcbiAgICAgIEBjb250ZW50O1xuICAgIH1cbiAgfVxufVxuIiwiLyoqXG4gKiBAbGljZW5zZVxuICogQ29weXJpZ2h0IEFrdmVvLiBBbGwgUmlnaHRzIFJlc2VydmVkLlxuICogTGljZW5zZWQgdW5kZXIgdGhlIE1JVCBMaWNlbnNlLiBTZWUgTGljZW5zZS50eHQgaW4gdGhlIHByb2plY3Qgcm9vdCBmb3IgbGljZW5zZSBpbmZvcm1hdGlvbi5cbiAqL1xuLyoqXG4gKiBUaGlzIGlzIGEgc3RhcnRpbmcgcG9pbnQgd2hlcmUgd2UgZGVjbGFyZSB0aGUgbWFwcyBvZiB0aGVtZXMgYW5kIGdsb2JhbGx5IGF2YWlsYWJsZSBmdW5jdGlvbnMvbWl4aW5zXG4gKi9cbi8qKlxuICogQGxpY2Vuc2VcbiAqIENvcHlyaWdodCBBa3Zlby4gQWxsIFJpZ2h0cyBSZXNlcnZlZC5cbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgTGljZW5zZS4gU2VlIExpY2Vuc2UudHh0IGluIHRoZSBwcm9qZWN0IHJvb3QgZm9yIGxpY2Vuc2UgaW5mb3JtYXRpb24uXG4gKi9cbi8qKlxuICogVGhpcyBtaXhpbiBnZW5lcmF0ZXMga2V5ZmFtZXMuXG4gKiBCZWNhdXNlIG9mIGFsbCBrZXlmcmFtZXMgY2FuJ3QgYmUgc2NvcGVkLFxuICogd2UgbmVlZCB0byBwdXRzIHVuaXF1ZSBuYW1lIGluIGVhY2ggYnRuLXB1bHNlIGNhbGwuXG4gKi9cbi8qXG5cbkFjY29yZGluZyB0byB0aGUgc3BlY2lmaWNhdGlvbiAoaHR0cHM6Ly93d3cudzMub3JnL1RSL2Nzcy1zY29waW5nLTEvI2hvc3Qtc2VsZWN0b3IpXG46aG9zdCBhbmQgOmhvc3QtY29udGV4dCBhcmUgcHNldWRvLWNsYXNzZXMuIFNvIHdlIGFzc3VtZSB0aGV5IGNvdWxkIGJlIGNvbWJpbmVkLFxubGlrZSBvdGhlciBwc2V1ZG8tY2xhc3NlcywgZXZlbiBzYW1lIG9uZXMuXG5Gb3IgZXhhbXBsZTogJzpudGgtb2YtdHlwZSgybik6bnRoLW9mLXR5cGUoZXZlbiknLlxuXG5JZGVhbCBzb2x1dGlvbiB3b3VsZCBiZSB0byBwcmVwZW5kIGFueSBzZWxlY3RvciB3aXRoIDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKS5cblRoZW4gbmVidWxhciBjb21wb25lbnRzIHdpbGwgYmVoYXZlIGFzIGFuIGh0bWwgZWxlbWVudCBhbmQgcmVzcG9uZCB0byBbZGlyXSBhdHRyaWJ1dGUgb24gYW55IGxldmVsLFxuc28gZGlyZWN0aW9uIGNvdWxkIGJlIG92ZXJyaWRkZW4gb24gYW55IGNvbXBvbmVudCBsZXZlbC5cblxuSW1wbGVtZW50YXRpb24gY29kZTpcblxuQG1peGluIG5iLXJ0bCgpIHtcbiAgLy8gYWRkICMgdG8gc2NzcyBpbnRlcnBvbGF0aW9uIHN0YXRlbWVudC5cbiAgLy8gaXQgd29ya3MgaW4gY29tbWVudHMgYW5kIHdlIGNhbid0IHVzZSBpdCBoZXJlXG4gIEBhdC1yb290IHtzZWxlY3Rvci1hcHBlbmQoJzpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKScsICYpfSB7XG4gICAgQGNvbnRlbnQ7XG4gIH1cbn1cblxuQW5kIHdoZW4gd2UgY2FsbCBpdCBzb21ld2hlcmU6XG5cbjpob3N0IHtcbiAgLnNvbWUtY2xhc3Mge1xuICAgIEBpbmNsdWRlIG5iLXJ0bCgpIHtcbiAgICAgIC4uLlxuICAgIH1cbiAgfVxufVxuOmhvc3QtY29udGV4dCguLi4pIHtcbiAgLnNvbWUtY2xhc3Mge1xuICAgIEBpbmNsdWRlIG5iLXJ0bCgpIHtcbiAgICAgIC4uLlxuICAgIH1cbiAgfVxufVxuXG5SZXN1bHQgd2lsbCBsb29rIGxpa2U6XG5cbjpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKTpob3N0IC5zb21lLWNsYXNzIHtcbiAgLi4uXG59XG46aG9zdC1jb250ZXh0KFtkaXI9cnRsXSk6aG9zdC1jb250ZXh0KC4uLikgLnNvbWUtY2xhc3Mge1xuICAuLi5cbn1cblxuKlxuICBTaWRlIG5vdGU6XG4gIDpob3N0LWNvbnRleHQoKTpob3N0IHNlbGVjdG9yIGFyZSB2YWxpZC4gaHR0cHM6Ly9saXN0cy53My5vcmcvQXJjaGl2ZXMvUHVibGljL3d3dy1zdHlsZS8yMDE1RmViLzAzMDUuaHRtbFxuXG4gIDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKTpob3N0LWNvbnRleHQoLi4uKSBzaG91bGQgbWF0Y2ggYW55IHBlcm11dGF0aW9uLFxuICBzbyBvcmRlciBpcyBub3QgaW1wb3J0YW50LlxuKlxuXG5cbkN1cnJlbnRseSwgdGhlcmUncmUgdHdvIHByb2JsZW1zIHdpdGggdGhpcyBhcHByb2FjaDpcblxuRmlyc3QsIGlzIHRoYXQgd2UgY2FuJ3QgY29tYmluZSA6aG9zdCwgOmhvc3QtY29udGV4dC4gQW5ndWxhciBidWdzICMxNDM0OSwgIzE5MTk5LlxuRm9yIHRoZSBtb21lbnQgb2Ygd3JpdGluZywgdGhlIG9ubHkgcG9zc2libGUgd2F5IGlzOlxuOmhvc3Qge1xuICA6aG9zdC1jb250ZXh0KC4uLikge1xuICAgIC4uLlxuICB9XG59XG5JdCBkb2Vzbid0IHdvcmsgZm9yIHVzIGJlY2F1c2UgbWl4aW4gY291bGQgYmUgY2FsbGVkIHNvbWV3aGVyZSBkZWVwZXIsIGxpa2U6XG46aG9zdCB7XG4gIHAge1xuICAgIEBpbmNsdWRlIG5iLXJ0bCgpIHsgLi4uIH1cbiAgfVxufVxuV2UgYXJlIG5vdCBhYmxlIHRvIGdvIHVwIHRvIDpob3N0IGxldmVsIHRvIHBsYWNlIGNvbnRlbnQgcGFzc2VkIHRvIG1peGluLlxuXG5UaGUgc2Vjb25kIHByb2JsZW0gaXMgdGhhdCB3ZSBvbmx5IGNhbiBiZSBzdXJlIHRoYXQgd2UgYXBwZW5kaW5nIDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKSB0byBhbm90aGVyXG46aG9zdC86aG9zdC1jb250ZXh0IHBzZXVkby1jbGFzcyB3aGVuIGNhbGxlZCBpbiB0aGVtZSBmaWxlcyAoKi50aGVtZS5zY3NzKS5cbiAgKlxuICAgIFNpZGUgbm90ZTpcbiAgICBDdXJyZW50bHksIG5iLWluc3RhbGwtY29tcG9uZW50IHVzZXMgYW5vdGhlciBhcHByb2FjaCB3aGVyZSA6aG9zdCBwcmVwZW5kZWQgd2l0aCB0aGUgdGhlbWUgbmFtZVxuICAgIChodHRwczovL2dpdGh1Yi5jb20vYW5ndWxhci9hbmd1bGFyL2Jsb2IvNWI5NjA3ODYyNGIwYTQ3NjBmMmRiY2Y2ZmRmMGJkNjI3OTFiZTViYi9wYWNrYWdlcy9jb21waWxlci9zcmMvc2hhZG93X2Nzcy50cyNMNDQxKSxcbiAgICBidXQgaXQgd2FzIG1hZGUgdG8gYmUgYWJsZSB0byB1c2UgY3VycmVudCByZWFsaXphdGlvbiBvZiBydGwgYW5kIGl0IGNhbiBiZSByZXdyaXR0ZW4gYmFjayB0b1xuICAgIDpob3N0LWNvbnRleHQoJHRoZW1lKSBvbmNlIHdlIHdpbGwgYmUgYWJsZSB0byB1c2UgbXVsdGlwbGUgc2hhZG93IHNlbGVjdG9ycy5cbiAgKlxuQnV0IHdoZW4gaXQncyBjYWxsZWQgaW4gKi5jb21wb25lbnQuc2NzcyB3ZSBjYW4ndCBiZSBzdXJlLCB0aGF0IHNlbGVjdG9yIHN0YXJ0cyB3aXRoIDpob3N0Lzpob3N0LWNvbnRleHQsXG5iZWNhdXNlIGFuZ3VsYXIgYWxsb3dzIG9taXR0aW5nIHBzZXVkby1jbGFzc2VzIGlmIHdlIGRvbid0IG5lZWQgdG8gc3R5bGUgOmhvc3QgY29tcG9uZW50IGl0c2VsZi5cbldlIGNhbiBicmVhayBzdWNoIHNlbGVjdG9ycywgYnkganVzdCBhcHBlbmRpbmcgOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pIHRvIHRoZW0uXG4gICoqKlxuICAgIFBvc3NpYmxlIHNvbHV0aW9uXG4gICAgY2hlY2sgaWYgd2UgaW4gdGhlbWUgYnkgc29tZSB0aGVtZSB2YXJpYWJsZXMgYW5kIGlmIHNvIGFwcGVuZCwgb3RoZXJ3aXNlIG5lc3QgbGlrZVxuICAgIEBhdC1yb290IDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKSB7XG4gICAgICAvLyBhZGQgIyB0byBzY3NzIGludGVycG9sYXRpb24gc3RhdGVtZW50LlxuICAgICAgLy8gaXQgd29ya3MgaW4gY29tbWVudHMgYW5kIHdlIGNhbid0IHVzZSBpdCBoZXJlXG4gICAgICB7Jn0ge1xuICAgICAgICBAY29udGVudDtcbiAgICAgIH1cbiAgICB9XG4gICAgV2hhdCBpZiA6aG9zdCBzcGVjaWZpZWQ/IENhbiB3ZSBhZGQgc3BhY2UgaW4gOmhvc3QtY29udGV4dCguLi4pIDpob3N0P1xuICAgIE9yIG1heWJlIGFkZCA6aG9zdCBzZWxlY3RvciBhbnl3YXk/IElmIG11bHRpcGxlIDpob3N0IHNlbGVjdG9ycyBhcmUgYWxsb3dlZFxuICAqKipcblxuXG5Qcm9ibGVtcyB3aXRoIHRoZSBjdXJyZW50IGFwcHJvYWNoLlxuXG4xLiBEaXJlY3Rpb24gY2FuIGJlIGFwcGxpZWQgb25seSBvbiBkb2N1bWVudCBsZXZlbCwgYmVjYXVzZSBtaXhpbiBwcmVwZW5kcyB0aGVtZSBjbGFzcyxcbndoaWNoIHBsYWNlZCBvbiB0aGUgYm9keS5cbjIuICouY29tcG9uZW50LnNjc3Mgc3R5bGVzIHNob3VsZCBiZSBpbiA6aG9zdCBzZWxlY3Rvci4gT3RoZXJ3aXNlIGFuZ3VsYXIgd2lsbCBhZGQgaG9zdFxuYXR0cmlidXRlIHRvIFtkaXI9cnRsXSBhdHRyaWJ1dGUgYXMgd2VsbC5cblxuXG5HZW5lcmFsIHByb2JsZW1zLlxuXG5MdHIgaXMgZGVmYXVsdCBkb2N1bWVudCBkaXJlY3Rpb24sIGJ1dCBmb3IgcHJvcGVyIHdvcmsgb2YgbmItbHRyIChtZWFucyBsdHIgb25seSksXG5bZGlyPWx0cl0gc2hvdWxkIGJlIHNwZWNpZmllZCBhdCBsZWFzdCBzb21ld2hlcmUuICc6bm90KFtkaXI9cnRsXScgbm90IGFwcGxpY2FibGUgaGVyZSxcbmJlY2F1c2UgaXQncyBzYXRpc2Z5IGFueSBwYXJlbnQsIHRoYXQgZG9uJ3QgaGF2ZSBbZGlyPXJ0bF0gYXR0cmlidXRlLlxuUHJldmlvdXMgYXBwcm9hY2ggd2FzIHRvIHVzZSBzaW5nbGUgcnRsIG1peGluIGFuZCByZXNldCBsdHIgcHJvcGVydGllcyB0byBpbml0aWFsIHZhbHVlLlxuQnV0IHNvbWV0aW1lcyBpdCdzIGhhcmQgdG8gZmluZCwgd2hhdCB0aGUgcHJldmlvdXMgdmFsdWUgc2hvdWxkIGJlLiBBbmQgc3VjaCBtaXhpbiBjYWxsIGxvb2tzIHRvbyB2ZXJib3NlLlxuKi9cbi8qKlxuICogQGxpY2Vuc2VcbiAqIENvcHlyaWdodCBBa3Zlby4gQWxsIFJpZ2h0cyBSZXNlcnZlZC5cbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgTGljZW5zZS4gU2VlIExpY2Vuc2UudHh0IGluIHRoZSBwcm9qZWN0IHJvb3QgZm9yIGxpY2Vuc2UgaW5mb3JtYXRpb24uXG4gKi9cbi8qKlxuICogQGxpY2Vuc2VcbiAqIENvcHlyaWdodCBBa3Zlby4gQWxsIFJpZ2h0cyBSZXNlcnZlZC5cbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgTGljZW5zZS4gU2VlIExpY2Vuc2UudHh0IGluIHRoZSBwcm9qZWN0IHJvb3QgZm9yIGxpY2Vuc2UgaW5mb3JtYXRpb24uXG4gKi9cbi8qKlxuICogQGxpY2Vuc2VcbiAqIENvcHlyaWdodCBBa3Zlby4gQWxsIFJpZ2h0cyBSZXNlcnZlZC5cbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgTGljZW5zZS4gU2VlIExpY2Vuc2UudHh0IGluIHRoZSBwcm9qZWN0IHJvb3QgZm9yIGxpY2Vuc2UgaW5mb3JtYXRpb24uXG4gKi9cbi8qKlxuICogQGxpY2Vuc2VcbiAqIENvcHlyaWdodCBBa3Zlby4gQWxsIFJpZ2h0cyBSZXNlcnZlZC5cbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgTGljZW5zZS4gU2VlIExpY2Vuc2UudHh0IGluIHRoZSBwcm9qZWN0IHJvb3QgZm9yIGxpY2Vuc2UgaW5mb3JtYXRpb24uXG4gKi9cbi8qKlxuICogVGhpcyBtaXhpbiBnZW5lcmF0ZXMga2V5ZmFtZXMuXG4gKiBCZWNhdXNlIG9mIGFsbCBrZXlmcmFtZXMgY2FuJ3QgYmUgc2NvcGVkLFxuICogd2UgbmVlZCB0byBwdXRzIHVuaXF1ZSBuYW1lIGluIGVhY2ggYnRuLXB1bHNlIGNhbGwuXG4gKi9cbi8qXG5cbkFjY29yZGluZyB0byB0aGUgc3BlY2lmaWNhdGlvbiAoaHR0cHM6Ly93d3cudzMub3JnL1RSL2Nzcy1zY29waW5nLTEvI2hvc3Qtc2VsZWN0b3IpXG46aG9zdCBhbmQgOmhvc3QtY29udGV4dCBhcmUgcHNldWRvLWNsYXNzZXMuIFNvIHdlIGFzc3VtZSB0aGV5IGNvdWxkIGJlIGNvbWJpbmVkLFxubGlrZSBvdGhlciBwc2V1ZG8tY2xhc3NlcywgZXZlbiBzYW1lIG9uZXMuXG5Gb3IgZXhhbXBsZTogJzpudGgtb2YtdHlwZSgybik6bnRoLW9mLXR5cGUoZXZlbiknLlxuXG5JZGVhbCBzb2x1dGlvbiB3b3VsZCBiZSB0byBwcmVwZW5kIGFueSBzZWxlY3RvciB3aXRoIDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKS5cblRoZW4gbmVidWxhciBjb21wb25lbnRzIHdpbGwgYmVoYXZlIGFzIGFuIGh0bWwgZWxlbWVudCBhbmQgcmVzcG9uZCB0byBbZGlyXSBhdHRyaWJ1dGUgb24gYW55IGxldmVsLFxuc28gZGlyZWN0aW9uIGNvdWxkIGJlIG92ZXJyaWRkZW4gb24gYW55IGNvbXBvbmVudCBsZXZlbC5cblxuSW1wbGVtZW50YXRpb24gY29kZTpcblxuQG1peGluIG5iLXJ0bCgpIHtcbiAgLy8gYWRkICMgdG8gc2NzcyBpbnRlcnBvbGF0aW9uIHN0YXRlbWVudC5cbiAgLy8gaXQgd29ya3MgaW4gY29tbWVudHMgYW5kIHdlIGNhbid0IHVzZSBpdCBoZXJlXG4gIEBhdC1yb290IHtzZWxlY3Rvci1hcHBlbmQoJzpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKScsICYpfSB7XG4gICAgQGNvbnRlbnQ7XG4gIH1cbn1cblxuQW5kIHdoZW4gd2UgY2FsbCBpdCBzb21ld2hlcmU6XG5cbjpob3N0IHtcbiAgLnNvbWUtY2xhc3Mge1xuICAgIEBpbmNsdWRlIG5iLXJ0bCgpIHtcbiAgICAgIC4uLlxuICAgIH1cbiAgfVxufVxuOmhvc3QtY29udGV4dCguLi4pIHtcbiAgLnNvbWUtY2xhc3Mge1xuICAgIEBpbmNsdWRlIG5iLXJ0bCgpIHtcbiAgICAgIC4uLlxuICAgIH1cbiAgfVxufVxuXG5SZXN1bHQgd2lsbCBsb29rIGxpa2U6XG5cbjpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKTpob3N0IC5zb21lLWNsYXNzIHtcbiAgLi4uXG59XG46aG9zdC1jb250ZXh0KFtkaXI9cnRsXSk6aG9zdC1jb250ZXh0KC4uLikgLnNvbWUtY2xhc3Mge1xuICAuLi5cbn1cblxuKlxuICBTaWRlIG5vdGU6XG4gIDpob3N0LWNvbnRleHQoKTpob3N0IHNlbGVjdG9yIGFyZSB2YWxpZC4gaHR0cHM6Ly9saXN0cy53My5vcmcvQXJjaGl2ZXMvUHVibGljL3d3dy1zdHlsZS8yMDE1RmViLzAzMDUuaHRtbFxuXG4gIDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKTpob3N0LWNvbnRleHQoLi4uKSBzaG91bGQgbWF0Y2ggYW55IHBlcm11dGF0aW9uLFxuICBzbyBvcmRlciBpcyBub3QgaW1wb3J0YW50LlxuKlxuXG5cbkN1cnJlbnRseSwgdGhlcmUncmUgdHdvIHByb2JsZW1zIHdpdGggdGhpcyBhcHByb2FjaDpcblxuRmlyc3QsIGlzIHRoYXQgd2UgY2FuJ3QgY29tYmluZSA6aG9zdCwgOmhvc3QtY29udGV4dC4gQW5ndWxhciBidWdzICMxNDM0OSwgIzE5MTk5LlxuRm9yIHRoZSBtb21lbnQgb2Ygd3JpdGluZywgdGhlIG9ubHkgcG9zc2libGUgd2F5IGlzOlxuOmhvc3Qge1xuICA6aG9zdC1jb250ZXh0KC4uLikge1xuICAgIC4uLlxuICB9XG59XG5JdCBkb2Vzbid0IHdvcmsgZm9yIHVzIGJlY2F1c2UgbWl4aW4gY291bGQgYmUgY2FsbGVkIHNvbWV3aGVyZSBkZWVwZXIsIGxpa2U6XG46aG9zdCB7XG4gIHAge1xuICAgIEBpbmNsdWRlIG5iLXJ0bCgpIHsgLi4uIH1cbiAgfVxufVxuV2UgYXJlIG5vdCBhYmxlIHRvIGdvIHVwIHRvIDpob3N0IGxldmVsIHRvIHBsYWNlIGNvbnRlbnQgcGFzc2VkIHRvIG1peGluLlxuXG5UaGUgc2Vjb25kIHByb2JsZW0gaXMgdGhhdCB3ZSBvbmx5IGNhbiBiZSBzdXJlIHRoYXQgd2UgYXBwZW5kaW5nIDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKSB0byBhbm90aGVyXG46aG9zdC86aG9zdC1jb250ZXh0IHBzZXVkby1jbGFzcyB3aGVuIGNhbGxlZCBpbiB0aGVtZSBmaWxlcyAoKi50aGVtZS5zY3NzKS5cbiAgKlxuICAgIFNpZGUgbm90ZTpcbiAgICBDdXJyZW50bHksIG5iLWluc3RhbGwtY29tcG9uZW50IHVzZXMgYW5vdGhlciBhcHByb2FjaCB3aGVyZSA6aG9zdCBwcmVwZW5kZWQgd2l0aCB0aGUgdGhlbWUgbmFtZVxuICAgIChodHRwczovL2dpdGh1Yi5jb20vYW5ndWxhci9hbmd1bGFyL2Jsb2IvNWI5NjA3ODYyNGIwYTQ3NjBmMmRiY2Y2ZmRmMGJkNjI3OTFiZTViYi9wYWNrYWdlcy9jb21waWxlci9zcmMvc2hhZG93X2Nzcy50cyNMNDQxKSxcbiAgICBidXQgaXQgd2FzIG1hZGUgdG8gYmUgYWJsZSB0byB1c2UgY3VycmVudCByZWFsaXphdGlvbiBvZiBydGwgYW5kIGl0IGNhbiBiZSByZXdyaXR0ZW4gYmFjayB0b1xuICAgIDpob3N0LWNvbnRleHQoJHRoZW1lKSBvbmNlIHdlIHdpbGwgYmUgYWJsZSB0byB1c2UgbXVsdGlwbGUgc2hhZG93IHNlbGVjdG9ycy5cbiAgKlxuQnV0IHdoZW4gaXQncyBjYWxsZWQgaW4gKi5jb21wb25lbnQuc2NzcyB3ZSBjYW4ndCBiZSBzdXJlLCB0aGF0IHNlbGVjdG9yIHN0YXJ0cyB3aXRoIDpob3N0Lzpob3N0LWNvbnRleHQsXG5iZWNhdXNlIGFuZ3VsYXIgYWxsb3dzIG9taXR0aW5nIHBzZXVkby1jbGFzc2VzIGlmIHdlIGRvbid0IG5lZWQgdG8gc3R5bGUgOmhvc3QgY29tcG9uZW50IGl0c2VsZi5cbldlIGNhbiBicmVhayBzdWNoIHNlbGVjdG9ycywgYnkganVzdCBhcHBlbmRpbmcgOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pIHRvIHRoZW0uXG4gICoqKlxuICAgIFBvc3NpYmxlIHNvbHV0aW9uXG4gICAgY2hlY2sgaWYgd2UgaW4gdGhlbWUgYnkgc29tZSB0aGVtZSB2YXJpYWJsZXMgYW5kIGlmIHNvIGFwcGVuZCwgb3RoZXJ3aXNlIG5lc3QgbGlrZVxuICAgIEBhdC1yb290IDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKSB7XG4gICAgICAvLyBhZGQgIyB0byBzY3NzIGludGVycG9sYXRpb24gc3RhdGVtZW50LlxuICAgICAgLy8gaXQgd29ya3MgaW4gY29tbWVudHMgYW5kIHdlIGNhbid0IHVzZSBpdCBoZXJlXG4gICAgICB7Jn0ge1xuICAgICAgICBAY29udGVudDtcbiAgICAgIH1cbiAgICB9XG4gICAgV2hhdCBpZiA6aG9zdCBzcGVjaWZpZWQ/IENhbiB3ZSBhZGQgc3BhY2UgaW4gOmhvc3QtY29udGV4dCguLi4pIDpob3N0P1xuICAgIE9yIG1heWJlIGFkZCA6aG9zdCBzZWxlY3RvciBhbnl3YXk/IElmIG11bHRpcGxlIDpob3N0IHNlbGVjdG9ycyBhcmUgYWxsb3dlZFxuICAqKipcblxuXG5Qcm9ibGVtcyB3aXRoIHRoZSBjdXJyZW50IGFwcHJvYWNoLlxuXG4xLiBEaXJlY3Rpb24gY2FuIGJlIGFwcGxpZWQgb25seSBvbiBkb2N1bWVudCBsZXZlbCwgYmVjYXVzZSBtaXhpbiBwcmVwZW5kcyB0aGVtZSBjbGFzcyxcbndoaWNoIHBsYWNlZCBvbiB0aGUgYm9keS5cbjIuICouY29tcG9uZW50LnNjc3Mgc3R5bGVzIHNob3VsZCBiZSBpbiA6aG9zdCBzZWxlY3Rvci4gT3RoZXJ3aXNlIGFuZ3VsYXIgd2lsbCBhZGQgaG9zdFxuYXR0cmlidXRlIHRvIFtkaXI9cnRsXSBhdHRyaWJ1dGUgYXMgd2VsbC5cblxuXG5HZW5lcmFsIHByb2JsZW1zLlxuXG5MdHIgaXMgZGVmYXVsdCBkb2N1bWVudCBkaXJlY3Rpb24sIGJ1dCBmb3IgcHJvcGVyIHdvcmsgb2YgbmItbHRyIChtZWFucyBsdHIgb25seSksXG5bZGlyPWx0cl0gc2hvdWxkIGJlIHNwZWNpZmllZCBhdCBsZWFzdCBzb21ld2hlcmUuICc6bm90KFtkaXI9cnRsXScgbm90IGFwcGxpY2FibGUgaGVyZSxcbmJlY2F1c2UgaXQncyBzYXRpc2Z5IGFueSBwYXJlbnQsIHRoYXQgZG9uJ3QgaGF2ZSBbZGlyPXJ0bF0gYXR0cmlidXRlLlxuUHJldmlvdXMgYXBwcm9hY2ggd2FzIHRvIHVzZSBzaW5nbGUgcnRsIG1peGluIGFuZCByZXNldCBsdHIgcHJvcGVydGllcyB0byBpbml0aWFsIHZhbHVlLlxuQnV0IHNvbWV0aW1lcyBpdCdzIGhhcmQgdG8gZmluZCwgd2hhdCB0aGUgcHJldmlvdXMgdmFsdWUgc2hvdWxkIGJlLiBBbmQgc3VjaCBtaXhpbiBjYWxsIGxvb2tzIHRvbyB2ZXJib3NlLlxuKi9cbi8qKlxuICogQGxpY2Vuc2VcbiAqIENvcHlyaWdodCBBa3Zlby4gQWxsIFJpZ2h0cyBSZXNlcnZlZC5cbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgTGljZW5zZS4gU2VlIExpY2Vuc2UudHh0IGluIHRoZSBwcm9qZWN0IHJvb3QgZm9yIGxpY2Vuc2UgaW5mb3JtYXRpb24uXG4gKi9cbi8qKlxuICogQGxpY2Vuc2VcbiAqIENvcHlyaWdodCBBa3Zlby4gQWxsIFJpZ2h0cyBSZXNlcnZlZC5cbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgTGljZW5zZS4gU2VlIExpY2Vuc2UudHh0IGluIHRoZSBwcm9qZWN0IHJvb3QgZm9yIGxpY2Vuc2UgaW5mb3JtYXRpb24uXG4gKi9cbi8qKlxuICogQGxpY2Vuc2VcbiAqIENvcHlyaWdodCBBa3Zlby4gQWxsIFJpZ2h0cyBSZXNlcnZlZC5cbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgTGljZW5zZS4gU2VlIExpY2Vuc2UudHh0IGluIHRoZSBwcm9qZWN0IHJvb3QgZm9yIGxpY2Vuc2UgaW5mb3JtYXRpb24uXG4gKi9cbi8qKlxuICogVGhpcyBtaXhpbiBnZW5lcmF0ZXMga2V5ZmFtZXMuXG4gKiBCZWNhdXNlIG9mIGFsbCBrZXlmcmFtZXMgY2FuJ3QgYmUgc2NvcGVkLFxuICogd2UgbmVlZCB0byBwdXRzIHVuaXF1ZSBuYW1lIGluIGVhY2ggYnRuLXB1bHNlIGNhbGwuXG4gKi9cbi8qXG5cbkFjY29yZGluZyB0byB0aGUgc3BlY2lmaWNhdGlvbiAoaHR0cHM6Ly93d3cudzMub3JnL1RSL2Nzcy1zY29waW5nLTEvI2hvc3Qtc2VsZWN0b3IpXG46aG9zdCBhbmQgOmhvc3QtY29udGV4dCBhcmUgcHNldWRvLWNsYXNzZXMuIFNvIHdlIGFzc3VtZSB0aGV5IGNvdWxkIGJlIGNvbWJpbmVkLFxubGlrZSBvdGhlciBwc2V1ZG8tY2xhc3NlcywgZXZlbiBzYW1lIG9uZXMuXG5Gb3IgZXhhbXBsZTogJzpudGgtb2YtdHlwZSgybik6bnRoLW9mLXR5cGUoZXZlbiknLlxuXG5JZGVhbCBzb2x1dGlvbiB3b3VsZCBiZSB0byBwcmVwZW5kIGFueSBzZWxlY3RvciB3aXRoIDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKS5cblRoZW4gbmVidWxhciBjb21wb25lbnRzIHdpbGwgYmVoYXZlIGFzIGFuIGh0bWwgZWxlbWVudCBhbmQgcmVzcG9uZCB0byBbZGlyXSBhdHRyaWJ1dGUgb24gYW55IGxldmVsLFxuc28gZGlyZWN0aW9uIGNvdWxkIGJlIG92ZXJyaWRkZW4gb24gYW55IGNvbXBvbmVudCBsZXZlbC5cblxuSW1wbGVtZW50YXRpb24gY29kZTpcblxuQG1peGluIG5iLXJ0bCgpIHtcbiAgLy8gYWRkICMgdG8gc2NzcyBpbnRlcnBvbGF0aW9uIHN0YXRlbWVudC5cbiAgLy8gaXQgd29ya3MgaW4gY29tbWVudHMgYW5kIHdlIGNhbid0IHVzZSBpdCBoZXJlXG4gIEBhdC1yb290IHtzZWxlY3Rvci1hcHBlbmQoJzpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKScsICYpfSB7XG4gICAgQGNvbnRlbnQ7XG4gIH1cbn1cblxuQW5kIHdoZW4gd2UgY2FsbCBpdCBzb21ld2hlcmU6XG5cbjpob3N0IHtcbiAgLnNvbWUtY2xhc3Mge1xuICAgIEBpbmNsdWRlIG5iLXJ0bCgpIHtcbiAgICAgIC4uLlxuICAgIH1cbiAgfVxufVxuOmhvc3QtY29udGV4dCguLi4pIHtcbiAgLnNvbWUtY2xhc3Mge1xuICAgIEBpbmNsdWRlIG5iLXJ0bCgpIHtcbiAgICAgIC4uLlxuICAgIH1cbiAgfVxufVxuXG5SZXN1bHQgd2lsbCBsb29rIGxpa2U6XG5cbjpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKTpob3N0IC5zb21lLWNsYXNzIHtcbiAgLi4uXG59XG46aG9zdC1jb250ZXh0KFtkaXI9cnRsXSk6aG9zdC1jb250ZXh0KC4uLikgLnNvbWUtY2xhc3Mge1xuICAuLi5cbn1cblxuKlxuICBTaWRlIG5vdGU6XG4gIDpob3N0LWNvbnRleHQoKTpob3N0IHNlbGVjdG9yIGFyZSB2YWxpZC4gaHR0cHM6Ly9saXN0cy53My5vcmcvQXJjaGl2ZXMvUHVibGljL3d3dy1zdHlsZS8yMDE1RmViLzAzMDUuaHRtbFxuXG4gIDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKTpob3N0LWNvbnRleHQoLi4uKSBzaG91bGQgbWF0Y2ggYW55IHBlcm11dGF0aW9uLFxuICBzbyBvcmRlciBpcyBub3QgaW1wb3J0YW50LlxuKlxuXG5cbkN1cnJlbnRseSwgdGhlcmUncmUgdHdvIHByb2JsZW1zIHdpdGggdGhpcyBhcHByb2FjaDpcblxuRmlyc3QsIGlzIHRoYXQgd2UgY2FuJ3QgY29tYmluZSA6aG9zdCwgOmhvc3QtY29udGV4dC4gQW5ndWxhciBidWdzICMxNDM0OSwgIzE5MTk5LlxuRm9yIHRoZSBtb21lbnQgb2Ygd3JpdGluZywgdGhlIG9ubHkgcG9zc2libGUgd2F5IGlzOlxuOmhvc3Qge1xuICA6aG9zdC1jb250ZXh0KC4uLikge1xuICAgIC4uLlxuICB9XG59XG5JdCBkb2Vzbid0IHdvcmsgZm9yIHVzIGJlY2F1c2UgbWl4aW4gY291bGQgYmUgY2FsbGVkIHNvbWV3aGVyZSBkZWVwZXIsIGxpa2U6XG46aG9zdCB7XG4gIHAge1xuICAgIEBpbmNsdWRlIG5iLXJ0bCgpIHsgLi4uIH1cbiAgfVxufVxuV2UgYXJlIG5vdCBhYmxlIHRvIGdvIHVwIHRvIDpob3N0IGxldmVsIHRvIHBsYWNlIGNvbnRlbnQgcGFzc2VkIHRvIG1peGluLlxuXG5UaGUgc2Vjb25kIHByb2JsZW0gaXMgdGhhdCB3ZSBvbmx5IGNhbiBiZSBzdXJlIHRoYXQgd2UgYXBwZW5kaW5nIDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKSB0byBhbm90aGVyXG46aG9zdC86aG9zdC1jb250ZXh0IHBzZXVkby1jbGFzcyB3aGVuIGNhbGxlZCBpbiB0aGVtZSBmaWxlcyAoKi50aGVtZS5zY3NzKS5cbiAgKlxuICAgIFNpZGUgbm90ZTpcbiAgICBDdXJyZW50bHksIG5iLWluc3RhbGwtY29tcG9uZW50IHVzZXMgYW5vdGhlciBhcHByb2FjaCB3aGVyZSA6aG9zdCBwcmVwZW5kZWQgd2l0aCB0aGUgdGhlbWUgbmFtZVxuICAgIChodHRwczovL2dpdGh1Yi5jb20vYW5ndWxhci9hbmd1bGFyL2Jsb2IvNWI5NjA3ODYyNGIwYTQ3NjBmMmRiY2Y2ZmRmMGJkNjI3OTFiZTViYi9wYWNrYWdlcy9jb21waWxlci9zcmMvc2hhZG93X2Nzcy50cyNMNDQxKSxcbiAgICBidXQgaXQgd2FzIG1hZGUgdG8gYmUgYWJsZSB0byB1c2UgY3VycmVudCByZWFsaXphdGlvbiBvZiBydGwgYW5kIGl0IGNhbiBiZSByZXdyaXR0ZW4gYmFjayB0b1xuICAgIDpob3N0LWNvbnRleHQoJHRoZW1lKSBvbmNlIHdlIHdpbGwgYmUgYWJsZSB0byB1c2UgbXVsdGlwbGUgc2hhZG93IHNlbGVjdG9ycy5cbiAgKlxuQnV0IHdoZW4gaXQncyBjYWxsZWQgaW4gKi5jb21wb25lbnQuc2NzcyB3ZSBjYW4ndCBiZSBzdXJlLCB0aGF0IHNlbGVjdG9yIHN0YXJ0cyB3aXRoIDpob3N0Lzpob3N0LWNvbnRleHQsXG5iZWNhdXNlIGFuZ3VsYXIgYWxsb3dzIG9taXR0aW5nIHBzZXVkby1jbGFzc2VzIGlmIHdlIGRvbid0IG5lZWQgdG8gc3R5bGUgOmhvc3QgY29tcG9uZW50IGl0c2VsZi5cbldlIGNhbiBicmVhayBzdWNoIHNlbGVjdG9ycywgYnkganVzdCBhcHBlbmRpbmcgOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pIHRvIHRoZW0uXG4gICoqKlxuICAgIFBvc3NpYmxlIHNvbHV0aW9uXG4gICAgY2hlY2sgaWYgd2UgaW4gdGhlbWUgYnkgc29tZSB0aGVtZSB2YXJpYWJsZXMgYW5kIGlmIHNvIGFwcGVuZCwgb3RoZXJ3aXNlIG5lc3QgbGlrZVxuICAgIEBhdC1yb290IDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKSB7XG4gICAgICAvLyBhZGQgIyB0byBzY3NzIGludGVycG9sYXRpb24gc3RhdGVtZW50LlxuICAgICAgLy8gaXQgd29ya3MgaW4gY29tbWVudHMgYW5kIHdlIGNhbid0IHVzZSBpdCBoZXJlXG4gICAgICB7Jn0ge1xuICAgICAgICBAY29udGVudDtcbiAgICAgIH1cbiAgICB9XG4gICAgV2hhdCBpZiA6aG9zdCBzcGVjaWZpZWQ/IENhbiB3ZSBhZGQgc3BhY2UgaW4gOmhvc3QtY29udGV4dCguLi4pIDpob3N0P1xuICAgIE9yIG1heWJlIGFkZCA6aG9zdCBzZWxlY3RvciBhbnl3YXk/IElmIG11bHRpcGxlIDpob3N0IHNlbGVjdG9ycyBhcmUgYWxsb3dlZFxuICAqKipcblxuXG5Qcm9ibGVtcyB3aXRoIHRoZSBjdXJyZW50IGFwcHJvYWNoLlxuXG4xLiBEaXJlY3Rpb24gY2FuIGJlIGFwcGxpZWQgb25seSBvbiBkb2N1bWVudCBsZXZlbCwgYmVjYXVzZSBtaXhpbiBwcmVwZW5kcyB0aGVtZSBjbGFzcyxcbndoaWNoIHBsYWNlZCBvbiB0aGUgYm9keS5cbjIuICouY29tcG9uZW50LnNjc3Mgc3R5bGVzIHNob3VsZCBiZSBpbiA6aG9zdCBzZWxlY3Rvci4gT3RoZXJ3aXNlIGFuZ3VsYXIgd2lsbCBhZGQgaG9zdFxuYXR0cmlidXRlIHRvIFtkaXI9cnRsXSBhdHRyaWJ1dGUgYXMgd2VsbC5cblxuXG5HZW5lcmFsIHByb2JsZW1zLlxuXG5MdHIgaXMgZGVmYXVsdCBkb2N1bWVudCBkaXJlY3Rpb24sIGJ1dCBmb3IgcHJvcGVyIHdvcmsgb2YgbmItbHRyIChtZWFucyBsdHIgb25seSksXG5bZGlyPWx0cl0gc2hvdWxkIGJlIHNwZWNpZmllZCBhdCBsZWFzdCBzb21ld2hlcmUuICc6bm90KFtkaXI9cnRsXScgbm90IGFwcGxpY2FibGUgaGVyZSxcbmJlY2F1c2UgaXQncyBzYXRpc2Z5IGFueSBwYXJlbnQsIHRoYXQgZG9uJ3QgaGF2ZSBbZGlyPXJ0bF0gYXR0cmlidXRlLlxuUHJldmlvdXMgYXBwcm9hY2ggd2FzIHRvIHVzZSBzaW5nbGUgcnRsIG1peGluIGFuZCByZXNldCBsdHIgcHJvcGVydGllcyB0byBpbml0aWFsIHZhbHVlLlxuQnV0IHNvbWV0aW1lcyBpdCdzIGhhcmQgdG8gZmluZCwgd2hhdCB0aGUgcHJldmlvdXMgdmFsdWUgc2hvdWxkIGJlLiBBbmQgc3VjaCBtaXhpbiBjYWxsIGxvb2tzIHRvbyB2ZXJib3NlLlxuKi9cbi8qKlxuICogQGxpY2Vuc2VcbiAqIENvcHlyaWdodCBBa3Zlby4gQWxsIFJpZ2h0cyBSZXNlcnZlZC5cbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgTGljZW5zZS4gU2VlIExpY2Vuc2UudHh0IGluIHRoZSBwcm9qZWN0IHJvb3QgZm9yIGxpY2Vuc2UgaW5mb3JtYXRpb24uXG4gKi9cbi8qKlxuICogQGxpY2Vuc2VcbiAqIENvcHlyaWdodCBBa3Zlby4gQWxsIFJpZ2h0cyBSZXNlcnZlZC5cbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgTGljZW5zZS4gU2VlIExpY2Vuc2UudHh0IGluIHRoZSBwcm9qZWN0IHJvb3QgZm9yIGxpY2Vuc2UgaW5mb3JtYXRpb24uXG4gKi9cbi8qKlxuICogQGxpY2Vuc2VcbiAqIENvcHlyaWdodCBBa3Zlby4gQWxsIFJpZ2h0cyBSZXNlcnZlZC5cbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgTGljZW5zZS4gU2VlIExpY2Vuc2UudHh0IGluIHRoZSBwcm9qZWN0IHJvb3QgZm9yIGxpY2Vuc2UgaW5mb3JtYXRpb24uXG4gKi9cbi8qKlxuICogVGhpcyBtaXhpbiBnZW5lcmF0ZXMga2V5ZmFtZXMuXG4gKiBCZWNhdXNlIG9mIGFsbCBrZXlmcmFtZXMgY2FuJ3QgYmUgc2NvcGVkLFxuICogd2UgbmVlZCB0byBwdXRzIHVuaXF1ZSBuYW1lIGluIGVhY2ggYnRuLXB1bHNlIGNhbGwuXG4gKi9cbi8qXG5cbkFjY29yZGluZyB0byB0aGUgc3BlY2lmaWNhdGlvbiAoaHR0cHM6Ly93d3cudzMub3JnL1RSL2Nzcy1zY29waW5nLTEvI2hvc3Qtc2VsZWN0b3IpXG46aG9zdCBhbmQgOmhvc3QtY29udGV4dCBhcmUgcHNldWRvLWNsYXNzZXMuIFNvIHdlIGFzc3VtZSB0aGV5IGNvdWxkIGJlIGNvbWJpbmVkLFxubGlrZSBvdGhlciBwc2V1ZG8tY2xhc3NlcywgZXZlbiBzYW1lIG9uZXMuXG5Gb3IgZXhhbXBsZTogJzpudGgtb2YtdHlwZSgybik6bnRoLW9mLXR5cGUoZXZlbiknLlxuXG5JZGVhbCBzb2x1dGlvbiB3b3VsZCBiZSB0byBwcmVwZW5kIGFueSBzZWxlY3RvciB3aXRoIDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKS5cblRoZW4gbmVidWxhciBjb21wb25lbnRzIHdpbGwgYmVoYXZlIGFzIGFuIGh0bWwgZWxlbWVudCBhbmQgcmVzcG9uZCB0byBbZGlyXSBhdHRyaWJ1dGUgb24gYW55IGxldmVsLFxuc28gZGlyZWN0aW9uIGNvdWxkIGJlIG92ZXJyaWRkZW4gb24gYW55IGNvbXBvbmVudCBsZXZlbC5cblxuSW1wbGVtZW50YXRpb24gY29kZTpcblxuQG1peGluIG5iLXJ0bCgpIHtcbiAgLy8gYWRkICMgdG8gc2NzcyBpbnRlcnBvbGF0aW9uIHN0YXRlbWVudC5cbiAgLy8gaXQgd29ya3MgaW4gY29tbWVudHMgYW5kIHdlIGNhbid0IHVzZSBpdCBoZXJlXG4gIEBhdC1yb290IHtzZWxlY3Rvci1hcHBlbmQoJzpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKScsICYpfSB7XG4gICAgQGNvbnRlbnQ7XG4gIH1cbn1cblxuQW5kIHdoZW4gd2UgY2FsbCBpdCBzb21ld2hlcmU6XG5cbjpob3N0IHtcbiAgLnNvbWUtY2xhc3Mge1xuICAgIEBpbmNsdWRlIG5iLXJ0bCgpIHtcbiAgICAgIC4uLlxuICAgIH1cbiAgfVxufVxuOmhvc3QtY29udGV4dCguLi4pIHtcbiAgLnNvbWUtY2xhc3Mge1xuICAgIEBpbmNsdWRlIG5iLXJ0bCgpIHtcbiAgICAgIC4uLlxuICAgIH1cbiAgfVxufVxuXG5SZXN1bHQgd2lsbCBsb29rIGxpa2U6XG5cbjpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKTpob3N0IC5zb21lLWNsYXNzIHtcbiAgLi4uXG59XG46aG9zdC1jb250ZXh0KFtkaXI9cnRsXSk6aG9zdC1jb250ZXh0KC4uLikgLnNvbWUtY2xhc3Mge1xuICAuLi5cbn1cblxuKlxuICBTaWRlIG5vdGU6XG4gIDpob3N0LWNvbnRleHQoKTpob3N0IHNlbGVjdG9yIGFyZSB2YWxpZC4gaHR0cHM6Ly9saXN0cy53My5vcmcvQXJjaGl2ZXMvUHVibGljL3d3dy1zdHlsZS8yMDE1RmViLzAzMDUuaHRtbFxuXG4gIDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKTpob3N0LWNvbnRleHQoLi4uKSBzaG91bGQgbWF0Y2ggYW55IHBlcm11dGF0aW9uLFxuICBzbyBvcmRlciBpcyBub3QgaW1wb3J0YW50LlxuKlxuXG5cbkN1cnJlbnRseSwgdGhlcmUncmUgdHdvIHByb2JsZW1zIHdpdGggdGhpcyBhcHByb2FjaDpcblxuRmlyc3QsIGlzIHRoYXQgd2UgY2FuJ3QgY29tYmluZSA6aG9zdCwgOmhvc3QtY29udGV4dC4gQW5ndWxhciBidWdzICMxNDM0OSwgIzE5MTk5LlxuRm9yIHRoZSBtb21lbnQgb2Ygd3JpdGluZywgdGhlIG9ubHkgcG9zc2libGUgd2F5IGlzOlxuOmhvc3Qge1xuICA6aG9zdC1jb250ZXh0KC4uLikge1xuICAgIC4uLlxuICB9XG59XG5JdCBkb2Vzbid0IHdvcmsgZm9yIHVzIGJlY2F1c2UgbWl4aW4gY291bGQgYmUgY2FsbGVkIHNvbWV3aGVyZSBkZWVwZXIsIGxpa2U6XG46aG9zdCB7XG4gIHAge1xuICAgIEBpbmNsdWRlIG5iLXJ0bCgpIHsgLi4uIH1cbiAgfVxufVxuV2UgYXJlIG5vdCBhYmxlIHRvIGdvIHVwIHRvIDpob3N0IGxldmVsIHRvIHBsYWNlIGNvbnRlbnQgcGFzc2VkIHRvIG1peGluLlxuXG5UaGUgc2Vjb25kIHByb2JsZW0gaXMgdGhhdCB3ZSBvbmx5IGNhbiBiZSBzdXJlIHRoYXQgd2UgYXBwZW5kaW5nIDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKSB0byBhbm90aGVyXG46aG9zdC86aG9zdC1jb250ZXh0IHBzZXVkby1jbGFzcyB3aGVuIGNhbGxlZCBpbiB0aGVtZSBmaWxlcyAoKi50aGVtZS5zY3NzKS5cbiAgKlxuICAgIFNpZGUgbm90ZTpcbiAgICBDdXJyZW50bHksIG5iLWluc3RhbGwtY29tcG9uZW50IHVzZXMgYW5vdGhlciBhcHByb2FjaCB3aGVyZSA6aG9zdCBwcmVwZW5kZWQgd2l0aCB0aGUgdGhlbWUgbmFtZVxuICAgIChodHRwczovL2dpdGh1Yi5jb20vYW5ndWxhci9hbmd1bGFyL2Jsb2IvNWI5NjA3ODYyNGIwYTQ3NjBmMmRiY2Y2ZmRmMGJkNjI3OTFiZTViYi9wYWNrYWdlcy9jb21waWxlci9zcmMvc2hhZG93X2Nzcy50cyNMNDQxKSxcbiAgICBidXQgaXQgd2FzIG1hZGUgdG8gYmUgYWJsZSB0byB1c2UgY3VycmVudCByZWFsaXphdGlvbiBvZiBydGwgYW5kIGl0IGNhbiBiZSByZXdyaXR0ZW4gYmFjayB0b1xuICAgIDpob3N0LWNvbnRleHQoJHRoZW1lKSBvbmNlIHdlIHdpbGwgYmUgYWJsZSB0byB1c2UgbXVsdGlwbGUgc2hhZG93IHNlbGVjdG9ycy5cbiAgKlxuQnV0IHdoZW4gaXQncyBjYWxsZWQgaW4gKi5jb21wb25lbnQuc2NzcyB3ZSBjYW4ndCBiZSBzdXJlLCB0aGF0IHNlbGVjdG9yIHN0YXJ0cyB3aXRoIDpob3N0Lzpob3N0LWNvbnRleHQsXG5iZWNhdXNlIGFuZ3VsYXIgYWxsb3dzIG9taXR0aW5nIHBzZXVkby1jbGFzc2VzIGlmIHdlIGRvbid0IG5lZWQgdG8gc3R5bGUgOmhvc3QgY29tcG9uZW50IGl0c2VsZi5cbldlIGNhbiBicmVhayBzdWNoIHNlbGVjdG9ycywgYnkganVzdCBhcHBlbmRpbmcgOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pIHRvIHRoZW0uXG4gICoqKlxuICAgIFBvc3NpYmxlIHNvbHV0aW9uXG4gICAgY2hlY2sgaWYgd2UgaW4gdGhlbWUgYnkgc29tZSB0aGVtZSB2YXJpYWJsZXMgYW5kIGlmIHNvIGFwcGVuZCwgb3RoZXJ3aXNlIG5lc3QgbGlrZVxuICAgIEBhdC1yb290IDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKSB7XG4gICAgICAvLyBhZGQgIyB0byBzY3NzIGludGVycG9sYXRpb24gc3RhdGVtZW50LlxuICAgICAgLy8gaXQgd29ya3MgaW4gY29tbWVudHMgYW5kIHdlIGNhbid0IHVzZSBpdCBoZXJlXG4gICAgICB7Jn0ge1xuICAgICAgICBAY29udGVudDtcbiAgICAgIH1cbiAgICB9XG4gICAgV2hhdCBpZiA6aG9zdCBzcGVjaWZpZWQ/IENhbiB3ZSBhZGQgc3BhY2UgaW4gOmhvc3QtY29udGV4dCguLi4pIDpob3N0P1xuICAgIE9yIG1heWJlIGFkZCA6aG9zdCBzZWxlY3RvciBhbnl3YXk/IElmIG11bHRpcGxlIDpob3N0IHNlbGVjdG9ycyBhcmUgYWxsb3dlZFxuICAqKipcblxuXG5Qcm9ibGVtcyB3aXRoIHRoZSBjdXJyZW50IGFwcHJvYWNoLlxuXG4xLiBEaXJlY3Rpb24gY2FuIGJlIGFwcGxpZWQgb25seSBvbiBkb2N1bWVudCBsZXZlbCwgYmVjYXVzZSBtaXhpbiBwcmVwZW5kcyB0aGVtZSBjbGFzcyxcbndoaWNoIHBsYWNlZCBvbiB0aGUgYm9keS5cbjIuICouY29tcG9uZW50LnNjc3Mgc3R5bGVzIHNob3VsZCBiZSBpbiA6aG9zdCBzZWxlY3Rvci4gT3RoZXJ3aXNlIGFuZ3VsYXIgd2lsbCBhZGQgaG9zdFxuYXR0cmlidXRlIHRvIFtkaXI9cnRsXSBhdHRyaWJ1dGUgYXMgd2VsbC5cblxuXG5HZW5lcmFsIHByb2JsZW1zLlxuXG5MdHIgaXMgZGVmYXVsdCBkb2N1bWVudCBkaXJlY3Rpb24sIGJ1dCBmb3IgcHJvcGVyIHdvcmsgb2YgbmItbHRyIChtZWFucyBsdHIgb25seSksXG5bZGlyPWx0cl0gc2hvdWxkIGJlIHNwZWNpZmllZCBhdCBsZWFzdCBzb21ld2hlcmUuICc6bm90KFtkaXI9cnRsXScgbm90IGFwcGxpY2FibGUgaGVyZSxcbmJlY2F1c2UgaXQncyBzYXRpc2Z5IGFueSBwYXJlbnQsIHRoYXQgZG9uJ3QgaGF2ZSBbZGlyPXJ0bF0gYXR0cmlidXRlLlxuUHJldmlvdXMgYXBwcm9hY2ggd2FzIHRvIHVzZSBzaW5nbGUgcnRsIG1peGluIGFuZCByZXNldCBsdHIgcHJvcGVydGllcyB0byBpbml0aWFsIHZhbHVlLlxuQnV0IHNvbWV0aW1lcyBpdCdzIGhhcmQgdG8gZmluZCwgd2hhdCB0aGUgcHJldmlvdXMgdmFsdWUgc2hvdWxkIGJlLiBBbmQgc3VjaCBtaXhpbiBjYWxsIGxvb2tzIHRvbyB2ZXJib3NlLlxuKi9cbi8qKlxuICogQGxpY2Vuc2VcbiAqIENvcHlyaWdodCBBa3Zlby4gQWxsIFJpZ2h0cyBSZXNlcnZlZC5cbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgTGljZW5zZS4gU2VlIExpY2Vuc2UudHh0IGluIHRoZSBwcm9qZWN0IHJvb3QgZm9yIGxpY2Vuc2UgaW5mb3JtYXRpb24uXG4gKi9cbi8qKlxuICogQGxpY2Vuc2VcbiAqIENvcHlyaWdodCBBa3Zlby4gQWxsIFJpZ2h0cyBSZXNlcnZlZC5cbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgTGljZW5zZS4gU2VlIExpY2Vuc2UudHh0IGluIHRoZSBwcm9qZWN0IHJvb3QgZm9yIGxpY2Vuc2UgaW5mb3JtYXRpb24uXG4gKi9cbi8qKlxuICogQGxpY2Vuc2VcbiAqIENvcHlyaWdodCBBa3Zlby4gQWxsIFJpZ2h0cyBSZXNlcnZlZC5cbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgTGljZW5zZS4gU2VlIExpY2Vuc2UudHh0IGluIHRoZSBwcm9qZWN0IHJvb3QgZm9yIGxpY2Vuc2UgaW5mb3JtYXRpb24uXG4gKi9cbi8qKlxuICogVGhpcyBtaXhpbiBnZW5lcmF0ZXMga2V5ZmFtZXMuXG4gKiBCZWNhdXNlIG9mIGFsbCBrZXlmcmFtZXMgY2FuJ3QgYmUgc2NvcGVkLFxuICogd2UgbmVlZCB0byBwdXRzIHVuaXF1ZSBuYW1lIGluIGVhY2ggYnRuLXB1bHNlIGNhbGwuXG4gKi9cbi8qXG5cbkFjY29yZGluZyB0byB0aGUgc3BlY2lmaWNhdGlvbiAoaHR0cHM6Ly93d3cudzMub3JnL1RSL2Nzcy1zY29waW5nLTEvI2hvc3Qtc2VsZWN0b3IpXG46aG9zdCBhbmQgOmhvc3QtY29udGV4dCBhcmUgcHNldWRvLWNsYXNzZXMuIFNvIHdlIGFzc3VtZSB0aGV5IGNvdWxkIGJlIGNvbWJpbmVkLFxubGlrZSBvdGhlciBwc2V1ZG8tY2xhc3NlcywgZXZlbiBzYW1lIG9uZXMuXG5Gb3IgZXhhbXBsZTogJzpudGgtb2YtdHlwZSgybik6bnRoLW9mLXR5cGUoZXZlbiknLlxuXG5JZGVhbCBzb2x1dGlvbiB3b3VsZCBiZSB0byBwcmVwZW5kIGFueSBzZWxlY3RvciB3aXRoIDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKS5cblRoZW4gbmVidWxhciBjb21wb25lbnRzIHdpbGwgYmVoYXZlIGFzIGFuIGh0bWwgZWxlbWVudCBhbmQgcmVzcG9uZCB0byBbZGlyXSBhdHRyaWJ1dGUgb24gYW55IGxldmVsLFxuc28gZGlyZWN0aW9uIGNvdWxkIGJlIG92ZXJyaWRkZW4gb24gYW55IGNvbXBvbmVudCBsZXZlbC5cblxuSW1wbGVtZW50YXRpb24gY29kZTpcblxuQG1peGluIG5iLXJ0bCgpIHtcbiAgLy8gYWRkICMgdG8gc2NzcyBpbnRlcnBvbGF0aW9uIHN0YXRlbWVudC5cbiAgLy8gaXQgd29ya3MgaW4gY29tbWVudHMgYW5kIHdlIGNhbid0IHVzZSBpdCBoZXJlXG4gIEBhdC1yb290IHtzZWxlY3Rvci1hcHBlbmQoJzpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKScsICYpfSB7XG4gICAgQGNvbnRlbnQ7XG4gIH1cbn1cblxuQW5kIHdoZW4gd2UgY2FsbCBpdCBzb21ld2hlcmU6XG5cbjpob3N0IHtcbiAgLnNvbWUtY2xhc3Mge1xuICAgIEBpbmNsdWRlIG5iLXJ0bCgpIHtcbiAgICAgIC4uLlxuICAgIH1cbiAgfVxufVxuOmhvc3QtY29udGV4dCguLi4pIHtcbiAgLnNvbWUtY2xhc3Mge1xuICAgIEBpbmNsdWRlIG5iLXJ0bCgpIHtcbiAgICAgIC4uLlxuICAgIH1cbiAgfVxufVxuXG5SZXN1bHQgd2lsbCBsb29rIGxpa2U6XG5cbjpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKTpob3N0IC5zb21lLWNsYXNzIHtcbiAgLi4uXG59XG46aG9zdC1jb250ZXh0KFtkaXI9cnRsXSk6aG9zdC1jb250ZXh0KC4uLikgLnNvbWUtY2xhc3Mge1xuICAuLi5cbn1cblxuKlxuICBTaWRlIG5vdGU6XG4gIDpob3N0LWNvbnRleHQoKTpob3N0IHNlbGVjdG9yIGFyZSB2YWxpZC4gaHR0cHM6Ly9saXN0cy53My5vcmcvQXJjaGl2ZXMvUHVibGljL3d3dy1zdHlsZS8yMDE1RmViLzAzMDUuaHRtbFxuXG4gIDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKTpob3N0LWNvbnRleHQoLi4uKSBzaG91bGQgbWF0Y2ggYW55IHBlcm11dGF0aW9uLFxuICBzbyBvcmRlciBpcyBub3QgaW1wb3J0YW50LlxuKlxuXG5cbkN1cnJlbnRseSwgdGhlcmUncmUgdHdvIHByb2JsZW1zIHdpdGggdGhpcyBhcHByb2FjaDpcblxuRmlyc3QsIGlzIHRoYXQgd2UgY2FuJ3QgY29tYmluZSA6aG9zdCwgOmhvc3QtY29udGV4dC4gQW5ndWxhciBidWdzICMxNDM0OSwgIzE5MTk5LlxuRm9yIHRoZSBtb21lbnQgb2Ygd3JpdGluZywgdGhlIG9ubHkgcG9zc2libGUgd2F5IGlzOlxuOmhvc3Qge1xuICA6aG9zdC1jb250ZXh0KC4uLikge1xuICAgIC4uLlxuICB9XG59XG5JdCBkb2Vzbid0IHdvcmsgZm9yIHVzIGJlY2F1c2UgbWl4aW4gY291bGQgYmUgY2FsbGVkIHNvbWV3aGVyZSBkZWVwZXIsIGxpa2U6XG46aG9zdCB7XG4gIHAge1xuICAgIEBpbmNsdWRlIG5iLXJ0bCgpIHsgLi4uIH1cbiAgfVxufVxuV2UgYXJlIG5vdCBhYmxlIHRvIGdvIHVwIHRvIDpob3N0IGxldmVsIHRvIHBsYWNlIGNvbnRlbnQgcGFzc2VkIHRvIG1peGluLlxuXG5UaGUgc2Vjb25kIHByb2JsZW0gaXMgdGhhdCB3ZSBvbmx5IGNhbiBiZSBzdXJlIHRoYXQgd2UgYXBwZW5kaW5nIDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKSB0byBhbm90aGVyXG46aG9zdC86aG9zdC1jb250ZXh0IHBzZXVkby1jbGFzcyB3aGVuIGNhbGxlZCBpbiB0aGVtZSBmaWxlcyAoKi50aGVtZS5zY3NzKS5cbiAgKlxuICAgIFNpZGUgbm90ZTpcbiAgICBDdXJyZW50bHksIG5iLWluc3RhbGwtY29tcG9uZW50IHVzZXMgYW5vdGhlciBhcHByb2FjaCB3aGVyZSA6aG9zdCBwcmVwZW5kZWQgd2l0aCB0aGUgdGhlbWUgbmFtZVxuICAgIChodHRwczovL2dpdGh1Yi5jb20vYW5ndWxhci9hbmd1bGFyL2Jsb2IvNWI5NjA3ODYyNGIwYTQ3NjBmMmRiY2Y2ZmRmMGJkNjI3OTFiZTViYi9wYWNrYWdlcy9jb21waWxlci9zcmMvc2hhZG93X2Nzcy50cyNMNDQxKSxcbiAgICBidXQgaXQgd2FzIG1hZGUgdG8gYmUgYWJsZSB0byB1c2UgY3VycmVudCByZWFsaXphdGlvbiBvZiBydGwgYW5kIGl0IGNhbiBiZSByZXdyaXR0ZW4gYmFjayB0b1xuICAgIDpob3N0LWNvbnRleHQoJHRoZW1lKSBvbmNlIHdlIHdpbGwgYmUgYWJsZSB0byB1c2UgbXVsdGlwbGUgc2hhZG93IHNlbGVjdG9ycy5cbiAgKlxuQnV0IHdoZW4gaXQncyBjYWxsZWQgaW4gKi5jb21wb25lbnQuc2NzcyB3ZSBjYW4ndCBiZSBzdXJlLCB0aGF0IHNlbGVjdG9yIHN0YXJ0cyB3aXRoIDpob3N0Lzpob3N0LWNvbnRleHQsXG5iZWNhdXNlIGFuZ3VsYXIgYWxsb3dzIG9taXR0aW5nIHBzZXVkby1jbGFzc2VzIGlmIHdlIGRvbid0IG5lZWQgdG8gc3R5bGUgOmhvc3QgY29tcG9uZW50IGl0c2VsZi5cbldlIGNhbiBicmVhayBzdWNoIHNlbGVjdG9ycywgYnkganVzdCBhcHBlbmRpbmcgOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pIHRvIHRoZW0uXG4gICoqKlxuICAgIFBvc3NpYmxlIHNvbHV0aW9uXG4gICAgY2hlY2sgaWYgd2UgaW4gdGhlbWUgYnkgc29tZSB0aGVtZSB2YXJpYWJsZXMgYW5kIGlmIHNvIGFwcGVuZCwgb3RoZXJ3aXNlIG5lc3QgbGlrZVxuICAgIEBhdC1yb290IDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKSB7XG4gICAgICAvLyBhZGQgIyB0byBzY3NzIGludGVycG9sYXRpb24gc3RhdGVtZW50LlxuICAgICAgLy8gaXQgd29ya3MgaW4gY29tbWVudHMgYW5kIHdlIGNhbid0IHVzZSBpdCBoZXJlXG4gICAgICB7Jn0ge1xuICAgICAgICBAY29udGVudDtcbiAgICAgIH1cbiAgICB9XG4gICAgV2hhdCBpZiA6aG9zdCBzcGVjaWZpZWQ/IENhbiB3ZSBhZGQgc3BhY2UgaW4gOmhvc3QtY29udGV4dCguLi4pIDpob3N0P1xuICAgIE9yIG1heWJlIGFkZCA6aG9zdCBzZWxlY3RvciBhbnl3YXk/IElmIG11bHRpcGxlIDpob3N0IHNlbGVjdG9ycyBhcmUgYWxsb3dlZFxuICAqKipcblxuXG5Qcm9ibGVtcyB3aXRoIHRoZSBjdXJyZW50IGFwcHJvYWNoLlxuXG4xLiBEaXJlY3Rpb24gY2FuIGJlIGFwcGxpZWQgb25seSBvbiBkb2N1bWVudCBsZXZlbCwgYmVjYXVzZSBtaXhpbiBwcmVwZW5kcyB0aGVtZSBjbGFzcyxcbndoaWNoIHBsYWNlZCBvbiB0aGUgYm9keS5cbjIuICouY29tcG9uZW50LnNjc3Mgc3R5bGVzIHNob3VsZCBiZSBpbiA6aG9zdCBzZWxlY3Rvci4gT3RoZXJ3aXNlIGFuZ3VsYXIgd2lsbCBhZGQgaG9zdFxuYXR0cmlidXRlIHRvIFtkaXI9cnRsXSBhdHRyaWJ1dGUgYXMgd2VsbC5cblxuXG5HZW5lcmFsIHByb2JsZW1zLlxuXG5MdHIgaXMgZGVmYXVsdCBkb2N1bWVudCBkaXJlY3Rpb24sIGJ1dCBmb3IgcHJvcGVyIHdvcmsgb2YgbmItbHRyIChtZWFucyBsdHIgb25seSksXG5bZGlyPWx0cl0gc2hvdWxkIGJlIHNwZWNpZmllZCBhdCBsZWFzdCBzb21ld2hlcmUuICc6bm90KFtkaXI9cnRsXScgbm90IGFwcGxpY2FibGUgaGVyZSxcbmJlY2F1c2UgaXQncyBzYXRpc2Z5IGFueSBwYXJlbnQsIHRoYXQgZG9uJ3QgaGF2ZSBbZGlyPXJ0bF0gYXR0cmlidXRlLlxuUHJldmlvdXMgYXBwcm9hY2ggd2FzIHRvIHVzZSBzaW5nbGUgcnRsIG1peGluIGFuZCByZXNldCBsdHIgcHJvcGVydGllcyB0byBpbml0aWFsIHZhbHVlLlxuQnV0IHNvbWV0aW1lcyBpdCdzIGhhcmQgdG8gZmluZCwgd2hhdCB0aGUgcHJldmlvdXMgdmFsdWUgc2hvdWxkIGJlLiBBbmQgc3VjaCBtaXhpbiBjYWxsIGxvb2tzIHRvbyB2ZXJib3NlLlxuKi9cbi8qKlxuICogQGxpY2Vuc2VcbiAqIENvcHlyaWdodCBBa3Zlby4gQWxsIFJpZ2h0cyBSZXNlcnZlZC5cbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgTGljZW5zZS4gU2VlIExpY2Vuc2UudHh0IGluIHRoZSBwcm9qZWN0IHJvb3QgZm9yIGxpY2Vuc2UgaW5mb3JtYXRpb24uXG4gKi9cbi8qKlxuICogQGxpY2Vuc2VcbiAqIENvcHlyaWdodCBBa3Zlby4gQWxsIFJpZ2h0cyBSZXNlcnZlZC5cbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgTGljZW5zZS4gU2VlIExpY2Vuc2UudHh0IGluIHRoZSBwcm9qZWN0IHJvb3QgZm9yIGxpY2Vuc2UgaW5mb3JtYXRpb24uXG4gKi9cbi8qKlxuICogQGxpY2Vuc2VcbiAqIENvcHlyaWdodCBBa3Zlby4gQWxsIFJpZ2h0cyBSZXNlcnZlZC5cbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgTGljZW5zZS4gU2VlIExpY2Vuc2UudHh0IGluIHRoZSBwcm9qZWN0IHJvb3QgZm9yIGxpY2Vuc2UgaW5mb3JtYXRpb24uXG4gKi9cbi8qKlxuICogVGhpcyBtaXhpbiBnZW5lcmF0ZXMga2V5ZmFtZXMuXG4gKiBCZWNhdXNlIG9mIGFsbCBrZXlmcmFtZXMgY2FuJ3QgYmUgc2NvcGVkLFxuICogd2UgbmVlZCB0byBwdXRzIHVuaXF1ZSBuYW1lIGluIGVhY2ggYnRuLXB1bHNlIGNhbGwuXG4gKi9cbi8qXG5cbkFjY29yZGluZyB0byB0aGUgc3BlY2lmaWNhdGlvbiAoaHR0cHM6Ly93d3cudzMub3JnL1RSL2Nzcy1zY29waW5nLTEvI2hvc3Qtc2VsZWN0b3IpXG46aG9zdCBhbmQgOmhvc3QtY29udGV4dCBhcmUgcHNldWRvLWNsYXNzZXMuIFNvIHdlIGFzc3VtZSB0aGV5IGNvdWxkIGJlIGNvbWJpbmVkLFxubGlrZSBvdGhlciBwc2V1ZG8tY2xhc3NlcywgZXZlbiBzYW1lIG9uZXMuXG5Gb3IgZXhhbXBsZTogJzpudGgtb2YtdHlwZSgybik6bnRoLW9mLXR5cGUoZXZlbiknLlxuXG5JZGVhbCBzb2x1dGlvbiB3b3VsZCBiZSB0byBwcmVwZW5kIGFueSBzZWxlY3RvciB3aXRoIDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKS5cblRoZW4gbmVidWxhciBjb21wb25lbnRzIHdpbGwgYmVoYXZlIGFzIGFuIGh0bWwgZWxlbWVudCBhbmQgcmVzcG9uZCB0byBbZGlyXSBhdHRyaWJ1dGUgb24gYW55IGxldmVsLFxuc28gZGlyZWN0aW9uIGNvdWxkIGJlIG92ZXJyaWRkZW4gb24gYW55IGNvbXBvbmVudCBsZXZlbC5cblxuSW1wbGVtZW50YXRpb24gY29kZTpcblxuQG1peGluIG5iLXJ0bCgpIHtcbiAgLy8gYWRkICMgdG8gc2NzcyBpbnRlcnBvbGF0aW9uIHN0YXRlbWVudC5cbiAgLy8gaXQgd29ya3MgaW4gY29tbWVudHMgYW5kIHdlIGNhbid0IHVzZSBpdCBoZXJlXG4gIEBhdC1yb290IHtzZWxlY3Rvci1hcHBlbmQoJzpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKScsICYpfSB7XG4gICAgQGNvbnRlbnQ7XG4gIH1cbn1cblxuQW5kIHdoZW4gd2UgY2FsbCBpdCBzb21ld2hlcmU6XG5cbjpob3N0IHtcbiAgLnNvbWUtY2xhc3Mge1xuICAgIEBpbmNsdWRlIG5iLXJ0bCgpIHtcbiAgICAgIC4uLlxuICAgIH1cbiAgfVxufVxuOmhvc3QtY29udGV4dCguLi4pIHtcbiAgLnNvbWUtY2xhc3Mge1xuICAgIEBpbmNsdWRlIG5iLXJ0bCgpIHtcbiAgICAgIC4uLlxuICAgIH1cbiAgfVxufVxuXG5SZXN1bHQgd2lsbCBsb29rIGxpa2U6XG5cbjpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKTpob3N0IC5zb21lLWNsYXNzIHtcbiAgLi4uXG59XG46aG9zdC1jb250ZXh0KFtkaXI9cnRsXSk6aG9zdC1jb250ZXh0KC4uLikgLnNvbWUtY2xhc3Mge1xuICAuLi5cbn1cblxuKlxuICBTaWRlIG5vdGU6XG4gIDpob3N0LWNvbnRleHQoKTpob3N0IHNlbGVjdG9yIGFyZSB2YWxpZC4gaHR0cHM6Ly9saXN0cy53My5vcmcvQXJjaGl2ZXMvUHVibGljL3d3dy1zdHlsZS8yMDE1RmViLzAzMDUuaHRtbFxuXG4gIDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKTpob3N0LWNvbnRleHQoLi4uKSBzaG91bGQgbWF0Y2ggYW55IHBlcm11dGF0aW9uLFxuICBzbyBvcmRlciBpcyBub3QgaW1wb3J0YW50LlxuKlxuXG5cbkN1cnJlbnRseSwgdGhlcmUncmUgdHdvIHByb2JsZW1zIHdpdGggdGhpcyBhcHByb2FjaDpcblxuRmlyc3QsIGlzIHRoYXQgd2UgY2FuJ3QgY29tYmluZSA6aG9zdCwgOmhvc3QtY29udGV4dC4gQW5ndWxhciBidWdzICMxNDM0OSwgIzE5MTk5LlxuRm9yIHRoZSBtb21lbnQgb2Ygd3JpdGluZywgdGhlIG9ubHkgcG9zc2libGUgd2F5IGlzOlxuOmhvc3Qge1xuICA6aG9zdC1jb250ZXh0KC4uLikge1xuICAgIC4uLlxuICB9XG59XG5JdCBkb2Vzbid0IHdvcmsgZm9yIHVzIGJlY2F1c2UgbWl4aW4gY291bGQgYmUgY2FsbGVkIHNvbWV3aGVyZSBkZWVwZXIsIGxpa2U6XG46aG9zdCB7XG4gIHAge1xuICAgIEBpbmNsdWRlIG5iLXJ0bCgpIHsgLi4uIH1cbiAgfVxufVxuV2UgYXJlIG5vdCBhYmxlIHRvIGdvIHVwIHRvIDpob3N0IGxldmVsIHRvIHBsYWNlIGNvbnRlbnQgcGFzc2VkIHRvIG1peGluLlxuXG5UaGUgc2Vjb25kIHByb2JsZW0gaXMgdGhhdCB3ZSBvbmx5IGNhbiBiZSBzdXJlIHRoYXQgd2UgYXBwZW5kaW5nIDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKSB0byBhbm90aGVyXG46aG9zdC86aG9zdC1jb250ZXh0IHBzZXVkby1jbGFzcyB3aGVuIGNhbGxlZCBpbiB0aGVtZSBmaWxlcyAoKi50aGVtZS5zY3NzKS5cbiAgKlxuICAgIFNpZGUgbm90ZTpcbiAgICBDdXJyZW50bHksIG5iLWluc3RhbGwtY29tcG9uZW50IHVzZXMgYW5vdGhlciBhcHByb2FjaCB3aGVyZSA6aG9zdCBwcmVwZW5kZWQgd2l0aCB0aGUgdGhlbWUgbmFtZVxuICAgIChodHRwczovL2dpdGh1Yi5jb20vYW5ndWxhci9hbmd1bGFyL2Jsb2IvNWI5NjA3ODYyNGIwYTQ3NjBmMmRiY2Y2ZmRmMGJkNjI3OTFiZTViYi9wYWNrYWdlcy9jb21waWxlci9zcmMvc2hhZG93X2Nzcy50cyNMNDQxKSxcbiAgICBidXQgaXQgd2FzIG1hZGUgdG8gYmUgYWJsZSB0byB1c2UgY3VycmVudCByZWFsaXphdGlvbiBvZiBydGwgYW5kIGl0IGNhbiBiZSByZXdyaXR0ZW4gYmFjayB0b1xuICAgIDpob3N0LWNvbnRleHQoJHRoZW1lKSBvbmNlIHdlIHdpbGwgYmUgYWJsZSB0byB1c2UgbXVsdGlwbGUgc2hhZG93IHNlbGVjdG9ycy5cbiAgKlxuQnV0IHdoZW4gaXQncyBjYWxsZWQgaW4gKi5jb21wb25lbnQuc2NzcyB3ZSBjYW4ndCBiZSBzdXJlLCB0aGF0IHNlbGVjdG9yIHN0YXJ0cyB3aXRoIDpob3N0Lzpob3N0LWNvbnRleHQsXG5iZWNhdXNlIGFuZ3VsYXIgYWxsb3dzIG9taXR0aW5nIHBzZXVkby1jbGFzc2VzIGlmIHdlIGRvbid0IG5lZWQgdG8gc3R5bGUgOmhvc3QgY29tcG9uZW50IGl0c2VsZi5cbldlIGNhbiBicmVhayBzdWNoIHNlbGVjdG9ycywgYnkganVzdCBhcHBlbmRpbmcgOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pIHRvIHRoZW0uXG4gICoqKlxuICAgIFBvc3NpYmxlIHNvbHV0aW9uXG4gICAgY2hlY2sgaWYgd2UgaW4gdGhlbWUgYnkgc29tZSB0aGVtZSB2YXJpYWJsZXMgYW5kIGlmIHNvIGFwcGVuZCwgb3RoZXJ3aXNlIG5lc3QgbGlrZVxuICAgIEBhdC1yb290IDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKSB7XG4gICAgICAvLyBhZGQgIyB0byBzY3NzIGludGVycG9sYXRpb24gc3RhdGVtZW50LlxuICAgICAgLy8gaXQgd29ya3MgaW4gY29tbWVudHMgYW5kIHdlIGNhbid0IHVzZSBpdCBoZXJlXG4gICAgICB7Jn0ge1xuICAgICAgICBAY29udGVudDtcbiAgICAgIH1cbiAgICB9XG4gICAgV2hhdCBpZiA6aG9zdCBzcGVjaWZpZWQ/IENhbiB3ZSBhZGQgc3BhY2UgaW4gOmhvc3QtY29udGV4dCguLi4pIDpob3N0P1xuICAgIE9yIG1heWJlIGFkZCA6aG9zdCBzZWxlY3RvciBhbnl3YXk/IElmIG11bHRpcGxlIDpob3N0IHNlbGVjdG9ycyBhcmUgYWxsb3dlZFxuICAqKipcblxuXG5Qcm9ibGVtcyB3aXRoIHRoZSBjdXJyZW50IGFwcHJvYWNoLlxuXG4xLiBEaXJlY3Rpb24gY2FuIGJlIGFwcGxpZWQgb25seSBvbiBkb2N1bWVudCBsZXZlbCwgYmVjYXVzZSBtaXhpbiBwcmVwZW5kcyB0aGVtZSBjbGFzcyxcbndoaWNoIHBsYWNlZCBvbiB0aGUgYm9keS5cbjIuICouY29tcG9uZW50LnNjc3Mgc3R5bGVzIHNob3VsZCBiZSBpbiA6aG9zdCBzZWxlY3Rvci4gT3RoZXJ3aXNlIGFuZ3VsYXIgd2lsbCBhZGQgaG9zdFxuYXR0cmlidXRlIHRvIFtkaXI9cnRsXSBhdHRyaWJ1dGUgYXMgd2VsbC5cblxuXG5HZW5lcmFsIHByb2JsZW1zLlxuXG5MdHIgaXMgZGVmYXVsdCBkb2N1bWVudCBkaXJlY3Rpb24sIGJ1dCBmb3IgcHJvcGVyIHdvcmsgb2YgbmItbHRyIChtZWFucyBsdHIgb25seSksXG5bZGlyPWx0cl0gc2hvdWxkIGJlIHNwZWNpZmllZCBhdCBsZWFzdCBzb21ld2hlcmUuICc6bm90KFtkaXI9cnRsXScgbm90IGFwcGxpY2FibGUgaGVyZSxcbmJlY2F1c2UgaXQncyBzYXRpc2Z5IGFueSBwYXJlbnQsIHRoYXQgZG9uJ3QgaGF2ZSBbZGlyPXJ0bF0gYXR0cmlidXRlLlxuUHJldmlvdXMgYXBwcm9hY2ggd2FzIHRvIHVzZSBzaW5nbGUgcnRsIG1peGluIGFuZCByZXNldCBsdHIgcHJvcGVydGllcyB0byBpbml0aWFsIHZhbHVlLlxuQnV0IHNvbWV0aW1lcyBpdCdzIGhhcmQgdG8gZmluZCwgd2hhdCB0aGUgcHJldmlvdXMgdmFsdWUgc2hvdWxkIGJlLiBBbmQgc3VjaCBtaXhpbiBjYWxsIGxvb2tzIHRvbyB2ZXJib3NlLlxuKi9cbi8qXG4gICAgICA6aG9zdCBjYW4gYmUgcHJlZml4ZWRcbiAgICAgIGh0dHBzOi8vZ2l0aHViLmNvbS9hbmd1bGFyL2FuZ3VsYXIvYmxvYi84ZDBlZTM0OTM5ZjE0YzA3ODc2ZDIyMmMyNWI0MDVlZDQ1OGEzNGQzL3BhY2thZ2VzL2NvbXBpbGVyL3NyYy9zaGFkb3dfY3NzLnRzI0w0NDFcblxuICAgICAgV2UgaGF2ZSB0byB1c2UgOmhvc3QgaW5zdGVkIG9mIDpob3N0LWNvbnRleHQoJHRoZW1lKSwgdG8gYmUgYWJsZSB0byBwcmVmaXggdGhlbWUgY2xhc3NcbiAgICAgIHdpdGggc29tZXRoaW5nIGRlZmluZWQgaW5zaWRlIG9mIEBjb250ZW50LCBieSBwcmVmaXhpbmcgJi5cbiAgICAgIEZvciBleGFtcGxlIHRoaXMgc2NzcyBjb2RlOlxuICAgICAgICAubmItdGhlbWUtZGVmYXVsdCB7XG4gICAgICAgICAgLnNvbWUtc2VsZWN0b3IgJiB7XG4gICAgICAgICAgICAuLi5cbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIFdpbGwgcmVzdWx0IGluIG5leHQgY3NzOlxuICAgICAgICAuc29tZS1zZWxlY3RvciAubmItdGhlbWUtZGVmYXVsdCB7XG4gICAgICAgICAgLi4uXG4gICAgICAgIH1cblxuICAgICAgSXQgZG9lc24ndCB3b3JrIHdpdGggOmhvc3QtY29udGV4dCBiZWNhdXNlIGFuZ3VsYXIgc3BsaXR0aW5nIGl0IGluIHR3byBzZWxlY3RvcnMgYW5kIHJlbW92ZXNcbiAgICAgIHByZWZpeCBpbiBvbmUgb2YgdGhlIHNlbGVjdG9ycy5cbiAgICAqL1xuLm5iLXRoZW1lLWRlZmF1bHQgOmhvc3QgLmZ1bGwtd2lkdGgge1xuICBmbGV4OiAxO1xuICBtaW4td2lkdGg6IDIyMHB4OyB9XG5cbi5uYi10aGVtZS1kZWZhdWx0IDpob3N0IG5iLWNoZWNrYm94IHtcbiAgbWFyZ2luLWJvdHRvbTogMXJlbTsgfVxuXG5bZGlyPWx0cl0gLm5iLXRoZW1lLWRlZmF1bHQgOmhvc3QgLmZvcm0taW5saW5lID4gKiB7XG4gIG1hcmdpbjogMCAxLjVyZW0gMS41cmVtIDA7IH1cblxuW2Rpcj1ydGxdIC5uYi10aGVtZS1kZWZhdWx0IDpob3N0IC5mb3JtLWlubGluZSA+ICoge1xuICBtYXJnaW46IDAgMCAxLjVyZW0gMS41cmVtOyB9XG5cbi5uYi10aGVtZS1kZWZhdWx0IDpob3N0IG5iLWNhcmQuaW5saW5lLWZvcm0tY2FyZCBuYi1jYXJkLWJvZHkge1xuICBwYWRkaW5nLWJvdHRvbTogMDsgfVxuXG4vKlxuICAgICAgOmhvc3QgY2FuIGJlIHByZWZpeGVkXG4gICAgICBodHRwczovL2dpdGh1Yi5jb20vYW5ndWxhci9hbmd1bGFyL2Jsb2IvOGQwZWUzNDkzOWYxNGMwNzg3NmQyMjJjMjViNDA1ZWQ0NThhMzRkMy9wYWNrYWdlcy9jb21waWxlci9zcmMvc2hhZG93X2Nzcy50cyNMNDQxXG5cbiAgICAgIFdlIGhhdmUgdG8gdXNlIDpob3N0IGluc3RlZCBvZiA6aG9zdC1jb250ZXh0KCR0aGVtZSksIHRvIGJlIGFibGUgdG8gcHJlZml4IHRoZW1lIGNsYXNzXG4gICAgICB3aXRoIHNvbWV0aGluZyBkZWZpbmVkIGluc2lkZSBvZiBAY29udGVudCwgYnkgcHJlZml4aW5nICYuXG4gICAgICBGb3IgZXhhbXBsZSB0aGlzIHNjc3MgY29kZTpcbiAgICAgICAgLm5iLXRoZW1lLWRlZmF1bHQge1xuICAgICAgICAgIC5zb21lLXNlbGVjdG9yICYge1xuICAgICAgICAgICAgLi4uXG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICBXaWxsIHJlc3VsdCBpbiBuZXh0IGNzczpcbiAgICAgICAgLnNvbWUtc2VsZWN0b3IgLm5iLXRoZW1lLWRlZmF1bHQge1xuICAgICAgICAgIC4uLlxuICAgICAgICB9XG5cbiAgICAgIEl0IGRvZXNuJ3Qgd29yayB3aXRoIDpob3N0LWNvbnRleHQgYmVjYXVzZSBhbmd1bGFyIHNwbGl0dGluZyBpdCBpbiB0d28gc2VsZWN0b3JzIGFuZCByZW1vdmVzXG4gICAgICBwcmVmaXggaW4gb25lIG9mIHRoZSBzZWxlY3RvcnMuXG4gICAgKi9cbi5uYi10aGVtZS1jb3NtaWMgOmhvc3QgLmZ1bGwtd2lkdGgge1xuICBmbGV4OiAxO1xuICBtaW4td2lkdGg6IDIyMHB4OyB9XG5cbi5uYi10aGVtZS1jb3NtaWMgOmhvc3QgbmItY2hlY2tib3gge1xuICBtYXJnaW4tYm90dG9tOiAxcmVtOyB9XG5cbltkaXI9bHRyXSAubmItdGhlbWUtY29zbWljIDpob3N0IC5mb3JtLWlubGluZSA+ICoge1xuICBtYXJnaW46IDAgMS41cmVtIDEuNXJlbSAwOyB9XG5cbltkaXI9cnRsXSAubmItdGhlbWUtY29zbWljIDpob3N0IC5mb3JtLWlubGluZSA+ICoge1xuICBtYXJnaW46IDAgMCAxLjVyZW0gMS41cmVtOyB9XG5cbi5uYi10aGVtZS1jb3NtaWMgOmhvc3QgbmItY2FyZC5pbmxpbmUtZm9ybS1jYXJkIG5iLWNhcmQtYm9keSB7XG4gIHBhZGRpbmctYm90dG9tOiAwOyB9XG5cbi8qXG4gICAgICA6aG9zdCBjYW4gYmUgcHJlZml4ZWRcbiAgICAgIGh0dHBzOi8vZ2l0aHViLmNvbS9hbmd1bGFyL2FuZ3VsYXIvYmxvYi84ZDBlZTM0OTM5ZjE0YzA3ODc2ZDIyMmMyNWI0MDVlZDQ1OGEzNGQzL3BhY2thZ2VzL2NvbXBpbGVyL3NyYy9zaGFkb3dfY3NzLnRzI0w0NDFcblxuICAgICAgV2UgaGF2ZSB0byB1c2UgOmhvc3QgaW5zdGVkIG9mIDpob3N0LWNvbnRleHQoJHRoZW1lKSwgdG8gYmUgYWJsZSB0byBwcmVmaXggdGhlbWUgY2xhc3NcbiAgICAgIHdpdGggc29tZXRoaW5nIGRlZmluZWQgaW5zaWRlIG9mIEBjb250ZW50LCBieSBwcmVmaXhpbmcgJi5cbiAgICAgIEZvciBleGFtcGxlIHRoaXMgc2NzcyBjb2RlOlxuICAgICAgICAubmItdGhlbWUtZGVmYXVsdCB7XG4gICAgICAgICAgLnNvbWUtc2VsZWN0b3IgJiB7XG4gICAgICAgICAgICAuLi5cbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIFdpbGwgcmVzdWx0IGluIG5leHQgY3NzOlxuICAgICAgICAuc29tZS1zZWxlY3RvciAubmItdGhlbWUtZGVmYXVsdCB7XG4gICAgICAgICAgLi4uXG4gICAgICAgIH1cblxuICAgICAgSXQgZG9lc24ndCB3b3JrIHdpdGggOmhvc3QtY29udGV4dCBiZWNhdXNlIGFuZ3VsYXIgc3BsaXR0aW5nIGl0IGluIHR3byBzZWxlY3RvcnMgYW5kIHJlbW92ZXNcbiAgICAgIHByZWZpeCBpbiBvbmUgb2YgdGhlIHNlbGVjdG9ycy5cbiAgICAqL1xuLm5iLXRoZW1lLWNvcnBvcmF0ZSA6aG9zdCAuZnVsbC13aWR0aCB7XG4gIGZsZXg6IDE7XG4gIG1pbi13aWR0aDogMjIwcHg7IH1cblxuLm5iLXRoZW1lLWNvcnBvcmF0ZSA6aG9zdCBuYi1jaGVja2JveCB7XG4gIG1hcmdpbi1ib3R0b206IDFyZW07IH1cblxuW2Rpcj1sdHJdIC5uYi10aGVtZS1jb3Jwb3JhdGUgOmhvc3QgLmZvcm0taW5saW5lID4gKiB7XG4gIG1hcmdpbjogMCAxLjVyZW0gMS41cmVtIDA7IH1cblxuW2Rpcj1ydGxdIC5uYi10aGVtZS1jb3Jwb3JhdGUgOmhvc3QgLmZvcm0taW5saW5lID4gKiB7XG4gIG1hcmdpbjogMCAwIDEuNXJlbSAxLjVyZW07IH1cblxuLm5iLXRoZW1lLWNvcnBvcmF0ZSA6aG9zdCBuYi1jYXJkLmlubGluZS1mb3JtLWNhcmQgbmItY2FyZC1ib2R5IHtcbiAgcGFkZGluZy1ib3R0b206IDA7IH1cblxuLmhpIHtcbiAgYm9yZGVyOiAxcHggc29saWQ7XG4gIC8qIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDE2cHg7ICovXG4gIC8qIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiAxNnB4OyAqL1xuICAvKiBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAxNnB4OyAqL1xuICAvKiBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogMTZweDsgKi8gfVxuXG4uYm9yZGVyLWxlZnQtcmlnaHQge1xuICBib3JkZXI6IDFweCBzb2xpZDtcbiAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogMTZweDsgfVxuXG4uYm9yZGVyLXRvcC1yaWdodCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkO1xuICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAxNnB4OyB9XG5cbi5ib3JkZXItdG9wLWxlZnQge1xuICBib3JkZXI6IDFweCBzb2xpZDtcbiAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDE2cHg7IH1cblxuLmJvcmRlci1ib3R0b20tbGVmdCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkO1xuICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogMTZweDsgfVxuXG50YWJsZSB7XG4gIHdpZHRoOiAtd2Via2l0LWZpbGwtYXZhaWxhYmxlOyB9XG5cbmEge1xuICBjdXJzb3I6IHBvaW50ZXI7IH1cblxudGV4dGFyZWEge1xuICBib3JkZXI6IG5vbmU7IH1cblxuL2RlZXAvIG5nMi1zbWFydC10YWJsZSAubmcyLXNtYXJ0LXRpdGxlIHtcbiAgY29sb3I6IHdoaXRlICFpbXBvcnRhbnQ7IH1cblxuL2RlZXAvIG5nMi1zbWFydC10YWJsZSBpIHtcbiAgY29sb3I6ICMyMTI1MjkgIWltcG9ydGFudDsgfVxuXG4vZGVlcC8gbW9kYWwgLm1vZGFsLWRpYWxvZyB7XG4gIG1heC13aWR0aDogMTAwMHB4ICFpbXBvcnRhbnQ7IH1cbiIsIi8qKlxuICogQGxpY2Vuc2VcbiAqIENvcHlyaWdodCBBa3Zlby4gQWxsIFJpZ2h0cyBSZXNlcnZlZC5cbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgTGljZW5zZS4gU2VlIExpY2Vuc2UudHh0IGluIHRoZSBwcm9qZWN0IHJvb3QgZm9yIGxpY2Vuc2UgaW5mb3JtYXRpb24uXG4gKi9cblxuQG1peGluIG5iLXNjcm9sbGJhcnMoJGZnLCAkYmcsICRzaXplLCAkYm9yZGVyLXJhZGl1czogJHNpemUgLyAyKSB7XG4gIDo6LXdlYmtpdC1zY3JvbGxiYXIge1xuICAgIHdpZHRoOiAkc2l6ZTtcbiAgICBoZWlnaHQ6ICRzaXplO1xuICB9XG5cbiAgOjotd2Via2l0LXNjcm9sbGJhci10aHVtYiB7XG4gICAgYmFja2dyb3VuZDogJGZnO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICBib3JkZXItcmFkaXVzOiAkYm9yZGVyLXJhZGl1cztcbiAgfVxuXG4gIDo6LXdlYmtpdC1zY3JvbGxiYXItdHJhY2sge1xuICAgIGJhY2tncm91bmQ6ICRiZztcbiAgfVxuXG4gIC8vIFRPRE86IHJlbW92ZVxuICAvLyBGb3IgSW50ZXJuZXQgRXhwbG9yZXJcbiAgc2Nyb2xsYmFyLWZhY2UtY29sb3I6ICRmZztcbiAgc2Nyb2xsYmFyLXRyYWNrLWNvbG9yOiAkYmc7XG59XG5cbkBtaXhpbiBuYi1yYWRpYWwtZ3JhZGllbnQoJGNvbG9yLTEsICRjb2xvci0yLCAkY29sb3ItMykge1xuICBiYWNrZ3JvdW5kOiAkY29sb3ItMjsgLyogT2xkIGJyb3dzZXJzICovXG4gIGJhY2tncm91bmQ6IC1tb3otcmFkaWFsLWdyYWRpZW50KGJvdHRvbSwgZWxsaXBzZSBjb3ZlciwgJGNvbG9yLTEgMCUsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJGNvbG9yLTIgNDUlLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRjb2xvci0zIDEwMCUpOyAvKiBGRjMuNi0xNSAqL1xuICBiYWNrZ3JvdW5kOiAtd2Via2l0LXJhZGlhbC1ncmFkaWVudChib3R0b20sIGVsbGlwc2UgY292ZXIsICRjb2xvci0xIDAlLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRjb2xvci0yIDQ1JSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkY29sb3ItMyAxMDAlKTsgLyogQ2hyb21lMTAtMjUsU2FmYXJpNS4xLTYgKi9cbiAgYmFja2dyb3VuZDogcmFkaWFsLWdyYWRpZW50KGVsbGlwc2UgYXQgYm90dG9tLCAkY29sb3ItMSAwJSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkY29sb3ItMiA0NSUsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJGNvbG9yLTMgMTAwJSk7IC8qIFczQywgSUUxMCssIEZGMTYrLCBDaHJvbWUyNissIE9wZXJhMTIrLCBTYWZhcmk3KyAqL1xuICBmaWx0ZXI6IHByb2dpZDpkeGltYWdldHJhbnNmb3JtLm1pY3Jvc29mdC5ncmFkaWVudChzdGFydENvbG9yc3RyPSckY29sb3ItMScsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGVuZENvbG9yc3RyPSckY29sb3ItMycsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIEdyYWRpZW50VHlwZT0xKTsgLyogSUU2LTkgZmFsbGJhY2sgb24gaG9yaXpvbnRhbCBncmFkaWVudCAqL1xufVxuXG5AbWl4aW4gbmItcmlnaHQtZ3JhZGllbnQoJGxlZnQtY29sb3IsICRyaWdodC1jb2xvcikge1xuICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICRsZWZ0LWNvbG9yLCAkcmlnaHQtY29sb3IpO1xufVxuXG5AbWl4aW4gbmItaGVhZGluZ3MoJGZyb206IDEsICR0bzogNikge1xuICBAZm9yICRpIGZyb20gJGZyb20gdGhyb3VnaCAkdG8ge1xuICAgIGgjeyRpfSB7XG4gICAgICBtYXJnaW46IDA7XG4gICAgfVxuICB9XG59XG5cbkBtaXhpbiBob3Zlci1mb2N1cy1hY3RpdmUge1xuICAmOmZvY3VzLFxuICAmOmFjdGl2ZSxcbiAgJjpob3ZlciB7XG4gICAgQGNvbnRlbnQ7XG4gIH1cbn1cblxuQG1peGluIGNlbnRlci1ob3Jpem9udGFsLWFic29sdXRlIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAwKTtcbiAgbGVmdDogNTAlO1xufVxuXG5AbWl4aW4gaW5zdGFsbC10aHVtYigpIHtcbiAgJHRodW1iLXNlbGVjdG9yczogKFxuICAgICc6Oi13ZWJraXQtc2xpZGVyLXRodW1iJ1xuICAgICc6Oi1tb3otcmFuZ2UtdGh1bWInXG4gICAgJzo6LW1zLXRodW1iJ1xuICApO1xuXG4gIEBlYWNoICRzZWxlY3RvciBpbiAkdGh1bWItc2VsZWN0b3JzIHtcbiAgICAmI3skc2VsZWN0b3J9IHtcbiAgICAgIC13ZWJraXQtYXBwZWFyYW5jZTogbm9uZTtcbiAgICAgIC1tb3otYXBwZWFyYW5jZTogbm9uZTtcbiAgICAgIEBjb250ZW50O1xuICAgIH1cbiAgfVxufVxuXG5AbWl4aW4gaW5zdGFsbC10cmFjaygpIHtcbiAgJHRodW1iLXNlbGVjdG9yczogKFxuICAgICc6Oi13ZWJraXQtc2xpZGVyLXJ1bm5hYmxlLXRyYWNrJ1xuICAgICc6Oi1tb3otcmFuZ2UtdHJhY2snXG4gICAgJzo6LW1zLXRyYWNrJ1xuICApO1xuXG4gIEBlYWNoICRzZWxlY3RvciBpbiAkdGh1bWItc2VsZWN0b3JzIHtcbiAgICAmI3skc2VsZWN0b3J9IHtcbiAgICAgIC13ZWJraXQtYXBwZWFyYW5jZTogbm9uZTtcbiAgICAgIC1tb3otYXBwZWFyYW5jZTogbm9uZTtcbiAgICAgIEBjb250ZW50O1xuICAgIH1cbiAgfVxufVxuXG5AbWl4aW4gaW5zdGFsbC1wbGFjZWhvbGRlcigkY29sb3IsICRmb250LXNpemUpIHtcbiAgJHBsYWNlaG9sZGVyLXNlbGVjdG9yczogKFxuICAgICc6Oi13ZWJraXQtaW5wdXQtcGxhY2Vob2xkZXInXG4gICAgJzo6LW1vei1wbGFjZWhvbGRlcidcbiAgICAnOi1tb3otcGxhY2Vob2xkZXInXG4gICAgJzotbXMtaW5wdXQtcGxhY2Vob2xkZXInXG4gICk7XG5cbiAgJjo6cGxhY2Vob2xkZXIge1xuICAgIEBpbmNsdWRlIHBsYWNlaG9sZGVyKCRjb2xvciwgJGZvbnQtc2l6ZSk7XG4gIH1cblxuICBAZWFjaCAkc2VsZWN0b3IgaW4gJHBsYWNlaG9sZGVyLXNlbGVjdG9ycyB7XG4gICAgJiN7JHNlbGVjdG9yfSB7XG4gICAgICBAaW5jbHVkZSBwbGFjZWhvbGRlcigkY29sb3IsICRmb250LXNpemUpO1xuICAgIH1cblxuICAgICY6Zm9jdXMjeyRzZWxlY3Rvcn0ge1xuICAgICAgQGluY2x1ZGUgcGxhY2Vob2xkZXItZm9jdXMoKTtcbiAgICB9XG4gIH1cbn1cblxuQG1peGluIHBsYWNlaG9sZGVyKCRjb2xvciwgJGZvbnQtc2l6ZSkge1xuICBjb2xvcjogJGNvbG9yO1xuICBmb250LXNpemU6ICRmb250LXNpemU7XG4gIG9wYWNpdHk6IDE7XG4gIHRyYW5zaXRpb246IG9wYWNpdHkgMC4zcyBlYXNlO1xuICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcbn1cblxuQG1peGluIHBsYWNlaG9sZGVyLWZvY3VzKCkge1xuICBvcGFjaXR5OiAwO1xuICB0cmFuc2l0aW9uOiBvcGFjaXR5IDAuM3MgZWFzZTtcbn1cblxuQG1peGluIGFuaW1hdGlvbigkYW5pbWF0ZS4uLikge1xuICAkbWF4OiBsZW5ndGgoJGFuaW1hdGUpO1xuICAkYW5pbWF0aW9uczogJyc7XG5cbiAgQGZvciAkaSBmcm9tIDEgdGhyb3VnaCAkbWF4IHtcbiAgICAkYW5pbWF0aW9uczogI3skYW5pbWF0aW9ucyArIG50aCgkYW5pbWF0ZSwgJGkpfTtcblxuICAgIEBpZiAkaSA8ICRtYXgge1xuICAgICAgJGFuaW1hdGlvbnM6ICN7JGFuaW1hdGlvbnMgKyAnLCAnfTtcbiAgICB9XG4gIH1cbiAgLXdlYmtpdC1hbmltYXRpb246ICRhbmltYXRpb25zO1xuICAtbW96LWFuaW1hdGlvbjogICAgJGFuaW1hdGlvbnM7XG4gIC1vLWFuaW1hdGlvbjogICAgICAkYW5pbWF0aW9ucztcbiAgYW5pbWF0aW9uOiAgICAgICAgICRhbmltYXRpb25zO1xufVxuXG5AbWl4aW4ga2V5ZnJhbWVzKCRhbmltYXRpb25OYW1lKSB7XG4gIEAtd2Via2l0LWtleWZyYW1lcyAjeyRhbmltYXRpb25OYW1lfSB7XG4gICAgQGNvbnRlbnQ7XG4gIH1cbiAgQC1tb3ota2V5ZnJhbWVzICN7JGFuaW1hdGlvbk5hbWV9IHtcbiAgICBAY29udGVudDtcbiAgfVxuICBALW8ta2V5ZnJhbWVzICN7JGFuaW1hdGlvbk5hbWV9IHtcbiAgICBAY29udGVudDtcbiAgfVxuICBAa2V5ZnJhbWVzICN7JGFuaW1hdGlvbk5hbWV9IHtcbiAgICBAY29udGVudDtcbiAgfVxufVxuXG4vKipcbiAqIFRoaXMgbWl4aW4gZ2VuZXJhdGVzIGtleWZhbWVzLlxuICogQmVjYXVzZSBvZiBhbGwga2V5ZnJhbWVzIGNhbid0IGJlIHNjb3BlZCxcbiAqIHdlIG5lZWQgdG8gcHV0cyB1bmlxdWUgbmFtZSBpbiBlYWNoIGJ0bi1wdWxzZSBjYWxsLlxuICovXG5AbWl4aW4gYnRuLXB1bHNlKCRuYW1lLCAkY29sb3IpIHtcbiAgJi5idG4tcHVsc2Uge1xuICAgIEBpbmNsdWRlIGFuaW1hdGlvbihidG4tI3skbmFtZX0tcHVsc2UgMS41cyBpbmZpbml0ZSk7XG4gIH1cblxuICBAaW5jbHVkZSBrZXlmcmFtZXMoYnRuLSN7JG5hbWV9LXB1bHNlKSB7XG4gICAgMCUge1xuICAgICAgYm94LXNoYWRvdzogbm9uZTtcbiAgICAgIG9wYWNpdHk6IG5iLXRoZW1lKGJ0bi1kaXNhYmxlZC1vcGFjaXR5KTtcbiAgICB9XG4gICAgNTAlIHtcbiAgICAgIGJveC1zaGFkb3c6IDAgMCAxcmVtIDAgJGNvbG9yO1xuICAgICAgb3BhY2l0eTogMC44O1xuICAgIH1cbiAgICAxMDAlIHtcbiAgICAgIGJveC1zaGFkb3c6IG5vbmU7XG4gICAgICBvcGFjaXR5OiBuYi10aGVtZShidG4tZGlzYWJsZWQtb3BhY2l0eSk7XG4gICAgfVxuICB9XG59XG5cbi8qXG5cbkFjY29yZGluZyB0byB0aGUgc3BlY2lmaWNhdGlvbiAoaHR0cHM6Ly93d3cudzMub3JnL1RSL2Nzcy1zY29waW5nLTEvI2hvc3Qtc2VsZWN0b3IpXG46aG9zdCBhbmQgOmhvc3QtY29udGV4dCBhcmUgcHNldWRvLWNsYXNzZXMuIFNvIHdlIGFzc3VtZSB0aGV5IGNvdWxkIGJlIGNvbWJpbmVkLFxubGlrZSBvdGhlciBwc2V1ZG8tY2xhc3NlcywgZXZlbiBzYW1lIG9uZXMuXG5Gb3IgZXhhbXBsZTogJzpudGgtb2YtdHlwZSgybik6bnRoLW9mLXR5cGUoZXZlbiknLlxuXG5JZGVhbCBzb2x1dGlvbiB3b3VsZCBiZSB0byBwcmVwZW5kIGFueSBzZWxlY3RvciB3aXRoIDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKS5cblRoZW4gbmVidWxhciBjb21wb25lbnRzIHdpbGwgYmVoYXZlIGFzIGFuIGh0bWwgZWxlbWVudCBhbmQgcmVzcG9uZCB0byBbZGlyXSBhdHRyaWJ1dGUgb24gYW55IGxldmVsLFxuc28gZGlyZWN0aW9uIGNvdWxkIGJlIG92ZXJyaWRkZW4gb24gYW55IGNvbXBvbmVudCBsZXZlbC5cblxuSW1wbGVtZW50YXRpb24gY29kZTpcblxuQG1peGluIG5iLXJ0bCgpIHtcbiAgLy8gYWRkICMgdG8gc2NzcyBpbnRlcnBvbGF0aW9uIHN0YXRlbWVudC5cbiAgLy8gaXQgd29ya3MgaW4gY29tbWVudHMgYW5kIHdlIGNhbid0IHVzZSBpdCBoZXJlXG4gIEBhdC1yb290IHtzZWxlY3Rvci1hcHBlbmQoJzpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKScsICYpfSB7XG4gICAgQGNvbnRlbnQ7XG4gIH1cbn1cblxuQW5kIHdoZW4gd2UgY2FsbCBpdCBzb21ld2hlcmU6XG5cbjpob3N0IHtcbiAgLnNvbWUtY2xhc3Mge1xuICAgIEBpbmNsdWRlIG5iLXJ0bCgpIHtcbiAgICAgIC4uLlxuICAgIH1cbiAgfVxufVxuOmhvc3QtY29udGV4dCguLi4pIHtcbiAgLnNvbWUtY2xhc3Mge1xuICAgIEBpbmNsdWRlIG5iLXJ0bCgpIHtcbiAgICAgIC4uLlxuICAgIH1cbiAgfVxufVxuXG5SZXN1bHQgd2lsbCBsb29rIGxpa2U6XG5cbjpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKTpob3N0IC5zb21lLWNsYXNzIHtcbiAgLi4uXG59XG46aG9zdC1jb250ZXh0KFtkaXI9cnRsXSk6aG9zdC1jb250ZXh0KC4uLikgLnNvbWUtY2xhc3Mge1xuICAuLi5cbn1cblxuKlxuICBTaWRlIG5vdGU6XG4gIDpob3N0LWNvbnRleHQoKTpob3N0IHNlbGVjdG9yIGFyZSB2YWxpZC4gaHR0cHM6Ly9saXN0cy53My5vcmcvQXJjaGl2ZXMvUHVibGljL3d3dy1zdHlsZS8yMDE1RmViLzAzMDUuaHRtbFxuXG4gIDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKTpob3N0LWNvbnRleHQoLi4uKSBzaG91bGQgbWF0Y2ggYW55IHBlcm11dGF0aW9uLFxuICBzbyBvcmRlciBpcyBub3QgaW1wb3J0YW50LlxuKlxuXG5cbkN1cnJlbnRseSwgdGhlcmUncmUgdHdvIHByb2JsZW1zIHdpdGggdGhpcyBhcHByb2FjaDpcblxuRmlyc3QsIGlzIHRoYXQgd2UgY2FuJ3QgY29tYmluZSA6aG9zdCwgOmhvc3QtY29udGV4dC4gQW5ndWxhciBidWdzICMxNDM0OSwgIzE5MTk5LlxuRm9yIHRoZSBtb21lbnQgb2Ygd3JpdGluZywgdGhlIG9ubHkgcG9zc2libGUgd2F5IGlzOlxuOmhvc3Qge1xuICA6aG9zdC1jb250ZXh0KC4uLikge1xuICAgIC4uLlxuICB9XG59XG5JdCBkb2Vzbid0IHdvcmsgZm9yIHVzIGJlY2F1c2UgbWl4aW4gY291bGQgYmUgY2FsbGVkIHNvbWV3aGVyZSBkZWVwZXIsIGxpa2U6XG46aG9zdCB7XG4gIHAge1xuICAgIEBpbmNsdWRlIG5iLXJ0bCgpIHsgLi4uIH1cbiAgfVxufVxuV2UgYXJlIG5vdCBhYmxlIHRvIGdvIHVwIHRvIDpob3N0IGxldmVsIHRvIHBsYWNlIGNvbnRlbnQgcGFzc2VkIHRvIG1peGluLlxuXG5UaGUgc2Vjb25kIHByb2JsZW0gaXMgdGhhdCB3ZSBvbmx5IGNhbiBiZSBzdXJlIHRoYXQgd2UgYXBwZW5kaW5nIDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKSB0byBhbm90aGVyXG46aG9zdC86aG9zdC1jb250ZXh0IHBzZXVkby1jbGFzcyB3aGVuIGNhbGxlZCBpbiB0aGVtZSBmaWxlcyAoKi50aGVtZS5zY3NzKS5cbiAgKlxuICAgIFNpZGUgbm90ZTpcbiAgICBDdXJyZW50bHksIG5iLWluc3RhbGwtY29tcG9uZW50IHVzZXMgYW5vdGhlciBhcHByb2FjaCB3aGVyZSA6aG9zdCBwcmVwZW5kZWQgd2l0aCB0aGUgdGhlbWUgbmFtZVxuICAgIChodHRwczovL2dpdGh1Yi5jb20vYW5ndWxhci9hbmd1bGFyL2Jsb2IvNWI5NjA3ODYyNGIwYTQ3NjBmMmRiY2Y2ZmRmMGJkNjI3OTFiZTViYi9wYWNrYWdlcy9jb21waWxlci9zcmMvc2hhZG93X2Nzcy50cyNMNDQxKSxcbiAgICBidXQgaXQgd2FzIG1hZGUgdG8gYmUgYWJsZSB0byB1c2UgY3VycmVudCByZWFsaXphdGlvbiBvZiBydGwgYW5kIGl0IGNhbiBiZSByZXdyaXR0ZW4gYmFjayB0b1xuICAgIDpob3N0LWNvbnRleHQoJHRoZW1lKSBvbmNlIHdlIHdpbGwgYmUgYWJsZSB0byB1c2UgbXVsdGlwbGUgc2hhZG93IHNlbGVjdG9ycy5cbiAgKlxuQnV0IHdoZW4gaXQncyBjYWxsZWQgaW4gKi5jb21wb25lbnQuc2NzcyB3ZSBjYW4ndCBiZSBzdXJlLCB0aGF0IHNlbGVjdG9yIHN0YXJ0cyB3aXRoIDpob3N0Lzpob3N0LWNvbnRleHQsXG5iZWNhdXNlIGFuZ3VsYXIgYWxsb3dzIG9taXR0aW5nIHBzZXVkby1jbGFzc2VzIGlmIHdlIGRvbid0IG5lZWQgdG8gc3R5bGUgOmhvc3QgY29tcG9uZW50IGl0c2VsZi5cbldlIGNhbiBicmVhayBzdWNoIHNlbGVjdG9ycywgYnkganVzdCBhcHBlbmRpbmcgOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pIHRvIHRoZW0uXG4gICoqKlxuICAgIFBvc3NpYmxlIHNvbHV0aW9uXG4gICAgY2hlY2sgaWYgd2UgaW4gdGhlbWUgYnkgc29tZSB0aGVtZSB2YXJpYWJsZXMgYW5kIGlmIHNvIGFwcGVuZCwgb3RoZXJ3aXNlIG5lc3QgbGlrZVxuICAgIEBhdC1yb290IDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKSB7XG4gICAgICAvLyBhZGQgIyB0byBzY3NzIGludGVycG9sYXRpb24gc3RhdGVtZW50LlxuICAgICAgLy8gaXQgd29ya3MgaW4gY29tbWVudHMgYW5kIHdlIGNhbid0IHVzZSBpdCBoZXJlXG4gICAgICB7Jn0ge1xuICAgICAgICBAY29udGVudDtcbiAgICAgIH1cbiAgICB9XG4gICAgV2hhdCBpZiA6aG9zdCBzcGVjaWZpZWQ/IENhbiB3ZSBhZGQgc3BhY2UgaW4gOmhvc3QtY29udGV4dCguLi4pIDpob3N0P1xuICAgIE9yIG1heWJlIGFkZCA6aG9zdCBzZWxlY3RvciBhbnl3YXk/IElmIG11bHRpcGxlIDpob3N0IHNlbGVjdG9ycyBhcmUgYWxsb3dlZFxuICAqKipcblxuXG5Qcm9ibGVtcyB3aXRoIHRoZSBjdXJyZW50IGFwcHJvYWNoLlxuXG4xLiBEaXJlY3Rpb24gY2FuIGJlIGFwcGxpZWQgb25seSBvbiBkb2N1bWVudCBsZXZlbCwgYmVjYXVzZSBtaXhpbiBwcmVwZW5kcyB0aGVtZSBjbGFzcyxcbndoaWNoIHBsYWNlZCBvbiB0aGUgYm9keS5cbjIuICouY29tcG9uZW50LnNjc3Mgc3R5bGVzIHNob3VsZCBiZSBpbiA6aG9zdCBzZWxlY3Rvci4gT3RoZXJ3aXNlIGFuZ3VsYXIgd2lsbCBhZGQgaG9zdFxuYXR0cmlidXRlIHRvIFtkaXI9cnRsXSBhdHRyaWJ1dGUgYXMgd2VsbC5cblxuXG5HZW5lcmFsIHByb2JsZW1zLlxuXG5MdHIgaXMgZGVmYXVsdCBkb2N1bWVudCBkaXJlY3Rpb24sIGJ1dCBmb3IgcHJvcGVyIHdvcmsgb2YgbmItbHRyIChtZWFucyBsdHIgb25seSksXG5bZGlyPWx0cl0gc2hvdWxkIGJlIHNwZWNpZmllZCBhdCBsZWFzdCBzb21ld2hlcmUuICc6bm90KFtkaXI9cnRsXScgbm90IGFwcGxpY2FibGUgaGVyZSxcbmJlY2F1c2UgaXQncyBzYXRpc2Z5IGFueSBwYXJlbnQsIHRoYXQgZG9uJ3QgaGF2ZSBbZGlyPXJ0bF0gYXR0cmlidXRlLlxuUHJldmlvdXMgYXBwcm9hY2ggd2FzIHRvIHVzZSBzaW5nbGUgcnRsIG1peGluIGFuZCByZXNldCBsdHIgcHJvcGVydGllcyB0byBpbml0aWFsIHZhbHVlLlxuQnV0IHNvbWV0aW1lcyBpdCdzIGhhcmQgdG8gZmluZCwgd2hhdCB0aGUgcHJldmlvdXMgdmFsdWUgc2hvdWxkIGJlLiBBbmQgc3VjaCBtaXhpbiBjYWxsIGxvb2tzIHRvbyB2ZXJib3NlLlxuKi9cblxuQG1peGluIF9wcmVwZW5kLXdpdGgtc2VsZWN0b3IoJHNlbGVjdG9yLCAkcHJvcDogbnVsbCwgJHZhbHVlOiBudWxsKSB7XG4gICN7JHNlbGVjdG9yfSAmIHtcbiAgICBAaWYgJHByb3AgIT0gbnVsbCB7XG4gICAgICAjeyRwcm9wfTogJHZhbHVlO1xuICAgIH1cblxuICAgIEBjb250ZW50O1xuICB9XG59XG5cbkBtaXhpbiBuYi1sdHIoJHByb3A6IG51bGwsICR2YWx1ZTogbnVsbCkge1xuICBAaW5jbHVkZSBfcHJlcGVuZC13aXRoLXNlbGVjdG9yKCdbZGlyPWx0cl0nLCAkcHJvcCwgJHZhbHVlKSB7XG4gICAgQGNvbnRlbnQ7XG4gIH1cbn1cblxuQG1peGluIG5iLXJ0bCgkcHJvcDogbnVsbCwgJHZhbHVlOiBudWxsKSB7XG4gIEBpbmNsdWRlIF9wcmVwZW5kLXdpdGgtc2VsZWN0b3IoJ1tkaXI9cnRsXScsICRwcm9wLCAkdmFsdWUpIHtcbiAgICBAY29udGVudDtcbiAgfTtcbn1cbiIsIi8qKlxuICogQGxpY2Vuc2VcbiAqIENvcHlyaWdodCBBa3Zlby4gQWxsIFJpZ2h0cyBSZXNlcnZlZC5cbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgTGljZW5zZS4gU2VlIExpY2Vuc2UudHh0IGluIHRoZSBwcm9qZWN0IHJvb3QgZm9yIGxpY2Vuc2UgaW5mb3JtYXRpb24uXG4gKi9cblxuLy8vIFNsaWdodGx5IGxpZ2h0ZW4gYSBjb2xvclxuLy8vIEBhY2Nlc3MgcHVibGljXG4vLy8gQHBhcmFtIHtDb2xvcn0gJGNvbG9yIC0gY29sb3IgdG8gdGludFxuLy8vIEBwYXJhbSB7TnVtYmVyfSAkcGVyY2VudGFnZSAtIHBlcmNlbnRhZ2Ugb2YgYCRjb2xvcmAgaW4gcmV0dXJuZWQgY29sb3Jcbi8vLyBAcmV0dXJuIHtDb2xvcn1cbkBmdW5jdGlvbiB0aW50KCRjb2xvciwgJHBlcmNlbnRhZ2UpIHtcbiAgQHJldHVybiBtaXgod2hpdGUsICRjb2xvciwgJHBlcmNlbnRhZ2UpO1xufVxuXG4vLy8gU2xpZ2h0bHkgZGFya2VuIGEgY29sb3Jcbi8vLyBAYWNjZXNzIHB1YmxpY1xuLy8vIEBwYXJhbSB7Q29sb3J9ICRjb2xvciAtIGNvbG9yIHRvIHNoYWRlXG4vLy8gQHBhcmFtIHtOdW1iZXJ9ICRwZXJjZW50YWdlIC0gcGVyY2VudGFnZSBvZiBgJGNvbG9yYCBpbiByZXR1cm5lZCBjb2xvclxuLy8vIEByZXR1cm4ge0NvbG9yfVxuQGZ1bmN0aW9uIHNoYWRlKCRjb2xvciwgJHBlcmNlbnRhZ2UpIHtcbiAgQHJldHVybiBtaXgoYmxhY2ssICRjb2xvciwgJHBlcmNlbnRhZ2UpO1xufVxuXG5AZnVuY3Rpb24gbWFwLXNldCgkbWFwLCAka2V5LCAkdmFsdWU6IG51bGwpIHtcbiAgJG5ldzogKCRrZXk6ICR2YWx1ZSk7XG4gIEByZXR1cm4gbWFwLW1lcmdlKCRtYXAsICRuZXcpO1xufVxuIiwiLyoqXG4gKiBAbGljZW5zZVxuICogQ29weXJpZ2h0IEFrdmVvLiBBbGwgUmlnaHRzIFJlc2VydmVkLlxuICogTGljZW5zZWQgdW5kZXIgdGhlIE1JVCBMaWNlbnNlLiBTZWUgTGljZW5zZS50eHQgaW4gdGhlIHByb2plY3Qgcm9vdCBmb3IgbGljZW5zZSBpbmZvcm1hdGlvbi5cbiAqL1xuXG5AaW1wb3J0ICcuLi9jb3JlL2Z1bmN0aW9ucyc7XG5AaW1wb3J0ICcuLi9jb3JlL21peGlucyc7XG5cbiR0aGVtZTogKFxuICBmb250LW1haW46IHVucXVvdGUoJ1wiU2Vnb2UgVUlcIiwgUm9ib3RvLCBcIkhlbHZldGljYSBOZXVlXCIsIEFyaWFsLCBzYW5zLXNlcmlmJyksXG4gIGZvbnQtc2Vjb25kYXJ5OiBmb250LW1haW4sXG5cbiAgZm9udC13ZWlnaHQtdGhpbjogMjAwLFxuICBmb250LXdlaWdodC1saWdodDogMzAwLFxuICBmb250LXdlaWdodC1ub3JtYWw6IDQwMCxcbiAgZm9udC13ZWlnaHQtYm9sZGVyOiA1MDAsXG4gIGZvbnQtd2VpZ2h0LWJvbGQ6IDYwMCxcbiAgZm9udC13ZWlnaHQtdWx0cmEtYm9sZDogODAwLFxuXG4gIC8vIFRPRE86IHVzZSBpdCBhcyBhIGRlZmF1bHQgZm9udC1zaXplXG4gIGJhc2UtZm9udC1zaXplOiAxNnB4LFxuXG4gIGZvbnQtc2l6ZS14bGc6IDEuMjVyZW0sXG4gIGZvbnQtc2l6ZS1sZzogMS4xMjVyZW0sXG4gIGZvbnQtc2l6ZTogMXJlbSxcbiAgZm9udC1zaXplLXNtOiAwLjg3NXJlbSxcbiAgZm9udC1zaXplLXhzOiAwLjc1cmVtLFxuXG4gIHJhZGl1czogMC4zNzVyZW0sXG4gIHBhZGRpbmc6IDEuMjVyZW0sXG4gIG1hcmdpbjogMS41cmVtLFxuICBsaW5lLWhlaWdodDogMS4yNSxcblxuICBjb2xvci1iZzogI2ZmZmZmZixcbiAgY29sb3ItYmctYWN0aXZlOiAjZTllZGYyLFxuICBjb2xvci1mZzogI2E0YWJiMyxcbiAgY29sb3ItZmctaGVhZGluZzogIzJhMmEyYSxcbiAgY29sb3ItZmctdGV4dDogIzRiNGI0YixcbiAgY29sb3ItZmctaGlnaGxpZ2h0OiAjNDBkYzdlLFxuXG4gIHNlcGFyYXRvcjogI2ViZWVmMixcblxuICBjb2xvci1ncmF5OiByZ2JhKDgxLCAxMTMsIDE2NSwgMC4xNSksXG4gIGNvbG9yLW5ldXRyYWw6IHRyYW5zcGFyZW50LFxuICBjb2xvci13aGl0ZTogI2ZmZmZmZixcbiAgY29sb3ItZGlzYWJsZWQ6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC40KSxcblxuICBjb2xvci1wcmltYXJ5OiAjOGE3ZmZmLFxuICBjb2xvci1zdWNjZXNzOiAjNDBkYzdlLFxuICBjb2xvci1pbmZvOiAjNGNhNmZmLFxuICBjb2xvci13YXJuaW5nOiAjZmZhMTAwLFxuICBjb2xvci1kYW5nZXI6ICNmZjRjNmEsXG5cbiAgLy8gVE9ETzogbW92ZSB0byBjb25zdGFudHNcbiAgc29jaWFsLWNvbG9yLWZhY2Vib29rOiAjM2I1OTk4LFxuICBzb2NpYWwtY29sb3ItdHdpdHRlcjogIzU1YWNlZSxcbiAgc29jaWFsLWNvbG9yLWdvb2dsZTogI2RkNGIzOSxcbiAgc29jaWFsLWNvbG9yLWxpbmtlZGluOiAjMDE3N2I1LFxuICBzb2NpYWwtY29sb3ItZ2l0aHViOiAjNmI2YjZiLFxuICBzb2NpYWwtY29sb3Itc3RhY2tvdmVyZmxvdzogIzJmOTZlOCxcbiAgc29jaWFsLWNvbG9yLWRyaWJibGU6ICNmMjY3OTgsXG4gIHNvY2lhbC1jb2xvci1iZWhhbmNlOiAjMDA5M2ZhLFxuXG4gIGJvcmRlci1jb2xvcjogY29sb3ItZ3JheSxcbiAgc2hhZG93OiAwIDJweCAxMnB4IDAgI2RmZTNlYixcblxuICBsaW5rLWNvbG9yOiAjM2RjYzZkLFxuICBsaW5rLWNvbG9yLWhvdmVyOiAjMmVlNTZiLFxuICBsaW5rLWNvbG9yLXZpc2l0ZWQ6IGxpbmstY29sb3IsXG5cbiAgc2Nyb2xsYmFyLWZnOiAjZGFkYWRhLFxuICBzY3JvbGxiYXItYmc6ICNmMmYyZjIsXG4gIHNjcm9sbGJhci13aWR0aDogNXB4LFxuICBzY3JvbGxiYXItdGh1bWItcmFkaXVzOiAyLjVweCxcblxuICByYWRpYWwtZ3JhZGllbnQ6IG5vbmUsXG4gIGxpbmVhci1ncmFkaWVudDogbm9uZSxcblxuICBjYXJkLWZvbnQtc2l6ZTogZm9udC1zaXplLFxuICBjYXJkLWxpbmUtaGVpZ2h0OiBsaW5lLWhlaWdodCxcbiAgY2FyZC1mb250LXdlaWdodDogZm9udC13ZWlnaHQtbm9ybWFsLFxuICBjYXJkLWZnOiBjb2xvci1mZywgLy8gVE9ETzogbm90IHVzZWRcbiAgY2FyZC1mZy10ZXh0OiBjb2xvci1mZy10ZXh0LFxuICBjYXJkLWZnLWhlYWRpbmc6IGNvbG9yLWZnLWhlYWRpbmcsIC8vIFRPRE86IG5vdCB1c2VkXG4gIGNhcmQtYmc6IGNvbG9yLWJnLFxuICBjYXJkLWhlaWdodC14eHNtYWxsOiA5NnB4LFxuICBjYXJkLWhlaWdodC14c21hbGw6IDIxNnB4LFxuICBjYXJkLWhlaWdodC1zbWFsbDogMzM2cHgsXG4gIGNhcmQtaGVpZ2h0LW1lZGl1bTogNDU2cHgsXG4gIGNhcmQtaGVpZ2h0LWxhcmdlOiA1NzZweCxcbiAgY2FyZC1oZWlnaHQteGxhcmdlOiA2OTZweCxcbiAgY2FyZC1oZWlnaHQteHhsYXJnZTogODE2cHgsXG4gIGNhcmQtc2hhZG93OiBzaGFkb3csXG4gIGNhcmQtYm9yZGVyLXdpZHRoOiAwLFxuICBjYXJkLWJvcmRlci10eXBlOiBzb2xpZCxcbiAgY2FyZC1ib3JkZXItY29sb3I6IGNvbG9yLWJnLFxuICBjYXJkLWJvcmRlci1yYWRpdXM6IHJhZGl1cyxcbiAgY2FyZC1wYWRkaW5nOiBwYWRkaW5nLFxuICBjYXJkLW1hcmdpbjogbWFyZ2luLFxuICBjYXJkLWhlYWRlci1mb250LWZhbWlseTogZm9udC1zZWNvbmRhcnksXG4gIGNhcmQtaGVhZGVyLWZvbnQtc2l6ZTogZm9udC1zaXplLWxnLFxuICBjYXJkLWhlYWRlci1mb250LXdlaWdodDogZm9udC13ZWlnaHQtYm9sZCxcbiAgY2FyZC1zZXBhcmF0b3I6IHNlcGFyYXRvcixcbiAgY2FyZC1oZWFkZXItZmc6IGNvbG9yLWZnLCAvLyBUT0RPOiBub3QgdXNlZFxuICBjYXJkLWhlYWRlci1mZy1oZWFkaW5nOiBjb2xvci1mZy1oZWFkaW5nLFxuICBjYXJkLWhlYWRlci1hY3RpdmUtYmc6IGNvbG9yLWZnLFxuICBjYXJkLWhlYWRlci1hY3RpdmUtZmc6IGNvbG9yLWJnLFxuICBjYXJkLWhlYWRlci1kaXNhYmxlZC1iZzogY29sb3ItZGlzYWJsZWQsXG4gIGNhcmQtaGVhZGVyLXByaW1hcnktYmc6IGNvbG9yLXByaW1hcnksXG4gIGNhcmQtaGVhZGVyLWluZm8tYmc6IGNvbG9yLWluZm8sXG4gIGNhcmQtaGVhZGVyLXN1Y2Nlc3MtYmc6IGNvbG9yLXN1Y2Nlc3MsXG4gIGNhcmQtaGVhZGVyLXdhcm5pbmctYmc6IGNvbG9yLXdhcm5pbmcsXG4gIGNhcmQtaGVhZGVyLWRhbmdlci1iZzogY29sb3ItZGFuZ2VyLFxuICBjYXJkLWhlYWRlci1ib3JkZXItd2lkdGg6IDFweCxcbiAgY2FyZC1oZWFkZXItYm9yZGVyLXR5cGU6IHNvbGlkLFxuICBjYXJkLWhlYWRlci1ib3JkZXItY29sb3I6IGNhcmQtc2VwYXJhdG9yLFxuXG4gIGhlYWRlci1mb250LWZhbWlseTogZm9udC1zZWNvbmRhcnksXG4gIGhlYWRlci1mb250LXNpemU6IGZvbnQtc2l6ZSxcbiAgaGVhZGVyLWxpbmUtaGVpZ2h0OiBsaW5lLWhlaWdodCxcbiAgaGVhZGVyLWZnOiBjb2xvci1mZy1oZWFkaW5nLFxuICBoZWFkZXItYmc6IGNvbG9yLWJnLFxuICBoZWFkZXItaGVpZ2h0OiA0Ljc1cmVtLFxuICBoZWFkZXItcGFkZGluZzogMS4yNXJlbSxcbiAgaGVhZGVyLXNoYWRvdzogc2hhZG93LFxuXG4gIGZvb3Rlci1oZWlnaHQ6IDQuNzI1cmVtLFxuICBmb290ZXItcGFkZGluZzogMS4yNXJlbSxcbiAgZm9vdGVyLWZnOiBjb2xvci1mZy1oZWFkaW5nLFxuICBmb290ZXItZmctaGlnaGxpZ2h0OiBjb2xvci1mZy1oZWFkaW5nLFxuICBmb290ZXItYmc6IGNvbG9yLWJnLFxuICBmb290ZXItc2VwYXJhdG9yOiBzZXBhcmF0b3IsXG4gIGZvb3Rlci1zaGFkb3c6IHNoYWRvdyxcblxuICBsYXlvdXQtZm9udC1mYW1pbHk6IGZvbnQtbWFpbixcbiAgbGF5b3V0LWZvbnQtc2l6ZTogZm9udC1zaXplLFxuICBsYXlvdXQtbGluZS1oZWlnaHQ6IGxpbmUtaGVpZ2h0LFxuICBsYXlvdXQtZmc6IGNvbG9yLWZnLFxuICBsYXlvdXQtYmc6ICNlYmVmZjUsXG4gIGxheW91dC1taW4taGVpZ2h0OiAxMDB2aCxcbiAgbGF5b3V0LWNvbnRlbnQtd2lkdGg6IDkwMHB4LFxuICBsYXlvdXQtd2luZG93LW1vZGUtbWluLXdpZHRoOiAzMDBweCxcbiAgbGF5b3V0LXdpbmRvdy1tb2RlLW1heC13aWR0aDogMTkyMHB4LFxuICBsYXlvdXQtd2luZG93LW1vZGUtYmc6IGxheW91dC1iZyxcbiAgbGF5b3V0LXdpbmRvdy1tb2RlLXBhZGRpbmctdG9wOiA0Ljc1cmVtLFxuICBsYXlvdXQtd2luZG93LXNoYWRvdzogc2hhZG93LFxuICBsYXlvdXQtcGFkZGluZzogMi4yNXJlbSAyLjI1cmVtIDAuNzVyZW0sXG4gIGxheW91dC1tZWRpdW0tcGFkZGluZzogMS41cmVtIDEuNXJlbSAwLjVyZW0sXG4gIGxheW91dC1zbWFsbC1wYWRkaW5nOiAxcmVtIDFyZW0gMCxcblxuICBzaWRlYmFyLWZvbnQtc2l6ZTogZm9udC1zaXplLFxuICBzaWRlYmFyLWxpbmUtaGVpZ2h0OiBsaW5lLWhlaWdodCxcbiAgc2lkZWJhci1mZzogY29sb3ItZmctaGVhZGluZyxcbiAgc2lkZWJhci1iZzogY29sb3ItYmcsXG4gIHNpZGViYXItaGVpZ2h0OiAxMDB2aCxcbiAgc2lkZWJhci13aWR0aDogMTZyZW0sXG4gIHNpZGViYXItd2lkdGgtY29tcGFjdDogMy41cmVtLFxuICBzaWRlYmFyLXBhZGRpbmc6IHBhZGRpbmcsXG4gIHNpZGViYXItaGVhZGVyLWhlaWdodDogMy41cmVtLFxuICBzaWRlYmFyLWZvb3Rlci1oZWlnaHQ6IDMuNXJlbSxcbiAgc2lkZWJhci1zaGFkb3c6IHNoYWRvdyxcblxuICBtZW51LWZvbnQtZmFtaWx5OiBmb250LXNlY29uZGFyeSxcbiAgbWVudS1mb250LXNpemU6IGZvbnQtc2l6ZSxcbiAgbWVudS1mb250LXdlaWdodDogZm9udC13ZWlnaHQtYm9sZGVyLFxuICBtZW51LWZnOiBjb2xvci1mZy10ZXh0LFxuICBtZW51LWJnOiBjb2xvci1iZyxcbiAgbWVudS1hY3RpdmUtZmc6IGNvbG9yLWZnLWhlYWRpbmcsXG4gIG1lbnUtYWN0aXZlLWJnOiBjb2xvci1iZyxcbiAgbWVudS1hY3RpdmUtZm9udC13ZWlnaHQ6IGZvbnQtd2VpZ2h0LWJvbGQsXG5cbiAgbWVudS1zdWJtZW51LWJnOiBjb2xvci1iZyxcbiAgbWVudS1zdWJtZW51LWZnOiBjb2xvci1mZy10ZXh0LFxuICBtZW51LXN1Ym1lbnUtYWN0aXZlLWZnOiBjb2xvci1mZy1oZWFkaW5nLFxuICBtZW51LXN1Ym1lbnUtYWN0aXZlLWJnOiBjb2xvci1iZyxcbiAgbWVudS1zdWJtZW51LWFjdGl2ZS1ib3JkZXItY29sb3I6IGNvbG9yLWZnLWhpZ2hsaWdodCxcbiAgbWVudS1zdWJtZW51LWFjdGl2ZS1zaGFkb3c6IG5vbmUsXG4gIG1lbnUtc3VibWVudS1ob3Zlci1mZzogbWVudS1zdWJtZW51LWFjdGl2ZS1mZyxcbiAgbWVudS1zdWJtZW51LWhvdmVyLWJnOiBtZW51LXN1Ym1lbnUtYmcsXG4gIG1lbnUtc3VibWVudS1pdGVtLWJvcmRlci13aWR0aDogMC4xMjVyZW0sXG4gIG1lbnUtc3VibWVudS1pdGVtLWJvcmRlci1yYWRpdXM6IHJhZGl1cyxcbiAgbWVudS1zdWJtZW51LWl0ZW0tcGFkZGluZzogMC41cmVtIDFyZW0sXG4gIG1lbnUtc3VibWVudS1pdGVtLWNvbnRhaW5lci1wYWRkaW5nOiAwIDEuMjVyZW0sXG4gIG1lbnUtc3VibWVudS1wYWRkaW5nOiAwLjVyZW0sXG5cbiAgbWVudS1ncm91cC1mb250LXdlaWdodDogZm9udC13ZWlnaHQtYm9sZGVyLFxuICBtZW51LWdyb3VwLWZvbnQtc2l6ZTogMC44NzVyZW0sXG4gIG1lbnUtZ3JvdXAtZmc6IGNvbG9yLWZnLFxuICBtZW51LWdyb3VwLXBhZGRpbmc6IDFyZW0gMS4yNXJlbSxcbiAgbWVudS1pdGVtLXBhZGRpbmc6IDAuNjc1cmVtIDAuNzVyZW0sXG4gIG1lbnUtaXRlbS1zZXBhcmF0b3I6IHNlcGFyYXRvcixcbiAgbWVudS1pY29uLWZvbnQtc2l6ZTogMi41cmVtLFxuICBtZW51LWljb24tbWFyZ2luOiAwIDAuMjVyZW0gMCxcbiAgbWVudS1pY29uLWNvbG9yOiBjb2xvci1mZyxcbiAgbWVudS1pY29uLWFjdGl2ZS1jb2xvcjogY29sb3ItZmctaGVhZGluZyxcblxuICB0YWJzLWZvbnQtZmFtaWx5OiBmb250LXNlY29uZGFyeSxcbiAgdGFicy1mb250LXNpemU6IGZvbnQtc2l6ZS1sZyxcbiAgdGFicy1jb250ZW50LWZvbnQtZmFtaWx5OiBmb250LW1haW4sXG4gIHRhYnMtY29udGVudC1mb250LXNpemU6IGZvbnQtc2l6ZSxcbiAgdGFicy1hY3RpdmUtYmc6IHRyYW5zcGFyZW50LFxuICB0YWJzLWFjdGl2ZS1mb250LXdlaWdodDogY2FyZC1oZWFkZXItZm9udC13ZWlnaHQsXG4gIHRhYnMtcGFkZGluZzogcGFkZGluZyxcbiAgdGFicy1jb250ZW50LXBhZGRpbmc6IDAsXG4gIHRhYnMtaGVhZGVyLWJnOiB0cmFuc3BhcmVudCxcbiAgdGFicy1zZXBhcmF0b3I6IHNlcGFyYXRvcixcbiAgdGFicy1mZzogY29sb3ItZmcsXG4gIHRhYnMtZmctdGV4dDogY29sb3ItZmctdGV4dCxcbiAgdGFicy1mZy1oZWFkaW5nOiBjb2xvci1mZy1oZWFkaW5nLFxuICB0YWJzLWJnOiB0cmFuc3BhcmVudCxcbiAgdGFicy1zZWxlY3RlZDogY29sb3Itc3VjY2VzcyxcbiAgdGFicy1pY29uLW9ubHktbWF4LXdpZHRoOiA1NzZweCxcblxuICByb3V0ZS10YWJzLWZvbnQtZmFtaWx5OiBmb250LXNlY29uZGFyeSxcbiAgcm91dGUtdGFicy1mb250LXNpemU6IGZvbnQtc2l6ZS1sZyxcbiAgcm91dGUtdGFicy1hY3RpdmUtYmc6IHRyYW5zcGFyZW50LFxuICByb3V0ZS10YWJzLWFjdGl2ZS1mb250LXdlaWdodDogY2FyZC1oZWFkZXItZm9udC13ZWlnaHQsXG4gIHJvdXRlLXRhYnMtcGFkZGluZzogcGFkZGluZyxcbiAgcm91dGUtdGFicy1oZWFkZXItYmc6IHRyYW5zcGFyZW50LFxuICByb3V0ZS10YWJzLXNlcGFyYXRvcjogc2VwYXJhdG9yLFxuICByb3V0ZS10YWJzLWZnOiBjb2xvci1mZyxcbiAgcm91dGUtdGFicy1mZy1oZWFkaW5nOiBjb2xvci1mZy1oZWFkaW5nLFxuICByb3V0ZS10YWJzLWJnOiB0cmFuc3BhcmVudCxcbiAgcm91dGUtdGFicy1zZWxlY3RlZDogY29sb3Itc3VjY2VzcyxcbiAgcm91dGUtdGFicy1pY29uLW9ubHktbWF4LXdpZHRoOiA1NzZweCxcblxuICB1c2VyLWZvbnQtc2l6ZTogZm9udC1zaXplLFxuICB1c2VyLWxpbmUtaGVpZ2h0OiBsaW5lLWhlaWdodCxcbiAgdXNlci1iZzogY29sb3ItYmcsXG4gIHVzZXItZmc6IGNvbG9yLWZnLFxuICB1c2VyLWZnLWhpZ2hsaWdodDogI2JjYzNjYyxcbiAgdXNlci1mb250LWZhbWlseS1zZWNvbmRhcnk6IGZvbnQtc2Vjb25kYXJ5LFxuICB1c2VyLXNpemUtc21hbGw6IDEuNXJlbSxcbiAgdXNlci1zaXplLW1lZGl1bTogMi41cmVtLFxuICB1c2VyLXNpemUtbGFyZ2U6IDMuMjVyZW0sXG4gIHVzZXItc2l6ZS14bGFyZ2U6IDRyZW0sXG5cbiAgcG9wb3Zlci1mZzogY29sb3ItZmctaGVhZGluZyxcbiAgcG9wb3Zlci1iZzogY29sb3ItYmcsXG4gIHBvcG92ZXItYm9yZGVyOiBjb2xvci1zdWNjZXNzLFxuICBwb3BvdmVyLXNoYWRvdzogbm9uZSxcblxuICBjb250ZXh0LW1lbnUtZmc6IGNvbG9yLWZnLWhlYWRpbmcsXG4gIGNvbnRleHQtbWVudS1hY3RpdmUtZmc6IGNvbG9yLXdoaXRlLFxuICBjb250ZXh0LW1lbnUtYWN0aXZlLWJnOiBjb2xvci1zdWNjZXNzLFxuXG4gIGFjdGlvbnMtZm9udC1zaXplOiBmb250LXNpemUsXG4gIGFjdGlvbnMtZm9udC1mYW1pbHk6IGZvbnQtc2Vjb25kYXJ5LFxuICBhY3Rpb25zLWxpbmUtaGVpZ2h0OiBsaW5lLWhlaWdodCxcbiAgYWN0aW9ucy1mZzogY29sb3ItZmcsXG4gIGFjdGlvbnMtYmc6IGNvbG9yLWJnLFxuICBhY3Rpb25zLXNlcGFyYXRvcjogc2VwYXJhdG9yLFxuICBhY3Rpb25zLXBhZGRpbmc6IHBhZGRpbmcsXG4gIGFjdGlvbnMtc2l6ZS1zbWFsbDogMS41cmVtLFxuICBhY3Rpb25zLXNpemUtbWVkaXVtOiAyLjI1cmVtLFxuICBhY3Rpb25zLXNpemUtbGFyZ2U6IDMuNXJlbSxcblxuICBzZWFyY2gtYnRuLW9wZW4tZmc6IGNvbG9yLWZnLFxuICBzZWFyY2gtYnRuLWNsb3NlLWZnOlx0Y29sb3ItZmcsXG4gIHNlYXJjaC1iZzogbGF5b3V0LWJnLFxuICBzZWFyY2gtYmctc2Vjb25kYXJ5OiBjb2xvci1mZyxcbiAgc2VhcmNoLXRleHQ6IGNvbG9yLWZnLWhlYWRpbmcsXG4gIHNlYXJjaC1pbmZvOiBjb2xvci1mZyxcbiAgc2VhcmNoLWRhc2g6IGNvbG9yLWZnLFxuICBzZWFyY2gtcGxhY2Vob2xkZXI6IGNvbG9yLWZnLFxuXG4gIHNtYXJ0LXRhYmxlLWhlYWRlci1mb250LWZhbWlseTogZm9udC1zZWNvbmRhcnksXG4gIHNtYXJ0LXRhYmxlLWhlYWRlci1mb250LXNpemU6IGZvbnQtc2l6ZSxcbiAgc21hcnQtdGFibGUtaGVhZGVyLWZvbnQtd2VpZ2h0OiBmb250LXdlaWdodC1ib2xkLFxuICBzbWFydC10YWJsZS1oZWFkZXItbGluZS1oZWlnaHQ6IGxpbmUtaGVpZ2h0LFxuICBzbWFydC10YWJsZS1oZWFkZXItZmc6IGNvbG9yLWZnLWhlYWRpbmcsXG4gIHNtYXJ0LXRhYmxlLWhlYWRlci1iZzogY29sb3ItYmcsXG5cbiAgc21hcnQtdGFibGUtZm9udC1mYW1pbHk6IGZvbnQtbWFpbixcbiAgc21hcnQtdGFibGUtZm9udC1zaXplOiBmb250LXNpemUsXG4gIHNtYXJ0LXRhYmxlLWZvbnQtd2VpZ2h0OiBmb250LXdlaWdodC1ub3JtYWwsXG4gIHNtYXJ0LXRhYmxlLWxpbmUtaGVpZ2h0OiBsaW5lLWhlaWdodCxcbiAgc21hcnQtdGFibGUtZmc6IGNvbG9yLWZnLWhlYWRpbmcsXG4gIHNtYXJ0LXRhYmxlLWJnOiBjb2xvci1iZyxcblxuICBzbWFydC10YWJsZS1iZy1ldmVuOiAjZjVmN2ZjLFxuICBzbWFydC10YWJsZS1mZy1zZWNvbmRhcnk6IGNvbG9yLWZnLFxuICBzbWFydC10YWJsZS1iZy1hY3RpdmU6ICNlNmYzZmYsXG4gIHNtYXJ0LXRhYmxlLXBhZGRpbmc6IDAuODc1cmVtIDEuMjVyZW0sXG4gIHNtYXJ0LXRhYmxlLWZpbHRlci1wYWRkaW5nOiAwLjM3NXJlbSAwLjVyZW0sXG4gIHNtYXJ0LXRhYmxlLXNlcGFyYXRvcjogc2VwYXJhdG9yLFxuICBzbWFydC10YWJsZS1ib3JkZXItcmFkaXVzOiByYWRpdXMsXG5cbiAgc21hcnQtdGFibGUtcGFnaW5nLWJvcmRlci1jb2xvcjogc2VwYXJhdG9yLFxuICBzbWFydC10YWJsZS1wYWdpbmctYm9yZGVyLXdpZHRoOiAxcHgsXG4gIHNtYXJ0LXRhYmxlLXBhZ2luZy1mZy1hY3RpdmU6ICNmZmZmZmYsXG4gIHNtYXJ0LXRhYmxlLXBhZ2luZy1iZy1hY3RpdmU6IGNvbG9yLXN1Y2Nlc3MsXG4gIHNtYXJ0LXRhYmxlLXBhZ2luZy1ob3ZlcjogcmdiYSgwLCAwLCAwLCAwLjA1KSxcblxuICB0b2FzdGVyLWJnOiBjb2xvci1wcmltYXJ5LFxuICB0b2FzdGVyLWZnLWRlZmF1bHQ6IGNvbG9yLWludmVyc2UsXG4gIHRvYXN0ZXItYnRuLWNsb3NlLWJnOiB0cmFuc3BhcmVudCxcbiAgdG9hc3Rlci1idG4tY2xvc2UtZmc6IHRvYXN0ZXItZmctZGVmYXVsdCxcbiAgdG9hc3Rlci1zaGFkb3c6IHNoYWRvdyxcblxuICB0b2FzdGVyLWZnOiBjb2xvci13aGl0ZSxcbiAgdG9hc3Rlci1zdWNjZXNzOiBjb2xvci1zdWNjZXNzLFxuICB0b2FzdGVyLWluZm86IGNvbG9yLWluZm8sXG4gIHRvYXN0ZXItd2FybmluZzogY29sb3Itd2FybmluZyxcbiAgdG9hc3Rlci13YWl0OiBjb2xvci1wcmltYXJ5LFxuICB0b2FzdGVyLWVycm9yOiBjb2xvci1kYW5nZXIsXG5cbiAgYnRuLWZnOiBjb2xvci13aGl0ZSxcbiAgYnRuLWZvbnQtZmFtaWx5OiBmb250LXNlY29uZGFyeSxcbiAgYnRuLWxpbmUtaGVpZ2h0OiBsaW5lLWhlaWdodCxcbiAgYnRuLWRpc2FibGVkLW9wYWNpdHk6IDAuMyxcbiAgYnRuLWN1cnNvcjogZGVmYXVsdCxcblxuICBidG4tcHJpbWFyeS1iZzogY29sb3ItcHJpbWFyeSxcbiAgYnRuLXNlY29uZGFyeS1iZzogdHJhbnNwYXJlbnQsXG4gIGJ0bi1pbmZvLWJnOiBjb2xvci1pbmZvLFxuICBidG4tc3VjY2Vzcy1iZzogY29sb3Itc3VjY2VzcyxcbiAgYnRuLXdhcm5pbmctYmc6IGNvbG9yLXdhcm5pbmcsXG4gIGJ0bi1kYW5nZXItYmc6IGNvbG9yLWRhbmdlcixcblxuICBidG4tc2Vjb25kYXJ5LWJvcmRlcjogI2RhZGZlNixcbiAgYnRuLXNlY29uZGFyeS1ib3JkZXItd2lkdGg6IDJweCxcblxuICBidG4tcGFkZGluZy15LWxnOiAwLjg3NXJlbSxcbiAgYnRuLXBhZGRpbmcteC1sZzogMS43NXJlbSxcbiAgYnRuLWZvbnQtc2l6ZS1sZzogZm9udC1zaXplLWxnLFxuXG4gIC8vIGRlZmF1bHQgc2l6ZVxuICBidG4tcGFkZGluZy15LW1kOiAwLjc1cmVtLFxuICBidG4tcGFkZGluZy14LW1kOiAxLjVyZW0sXG4gIGJ0bi1mb250LXNpemUtbWQ6IDFyZW0sXG5cbiAgYnRuLXBhZGRpbmcteS1zbTogMC42MjVyZW0sXG4gIGJ0bi1wYWRkaW5nLXgtc206IDEuNXJlbSxcbiAgYnRuLWZvbnQtc2l6ZS1zbTogMC44NzVyZW0sXG5cbiAgYnRuLXBhZGRpbmcteS14czogMC41cmVtLFxuICBidG4tcGFkZGluZy14LXhzOiAxLjI1cmVtLFxuICBidG4tZm9udC1zaXplLXhzOiAwLjc1cmVtLFxuXG4gIGJ0bi1ib3JkZXItcmFkaXVzOiByYWRpdXMsXG4gIGJ0bi1yZWN0YW5nbGUtYm9yZGVyLXJhZGl1czogMC4yNXJlbSxcbiAgYnRuLXNlbWktcm91bmQtYm9yZGVyLXJhZGl1czogMC43NXJlbSxcbiAgYnRuLXJvdW5kLWJvcmRlci1yYWRpdXM6IDEuNXJlbSxcblxuICBidG4taGVyby1zaGFkb3c6IG5vbmUsXG4gIGJ0bi1oZXJvLXRleHQtc2hhZG93OiBub25lLFxuICBidG4taGVyby1iZXZlbC1zaXplOiAwIDAgMCAwLFxuICBidG4taGVyby1nbG93LXNpemU6IDAgMCAwIDAsXG4gIGJ0bi1oZXJvLXByaW1hcnktZ2xvdy1zaXplOiBidG4taGVyby1nbG93LXNpemUsXG4gIGJ0bi1oZXJvLXN1Y2Nlc3MtZ2xvdy1zaXplOiBidG4taGVyby1nbG93LXNpemUsXG4gIGJ0bi1oZXJvLXdhcm5pbmctZ2xvdy1zaXplOiBidG4taGVyby1nbG93LXNpemUsXG4gIGJ0bi1oZXJvLWluZm8tZ2xvdy1zaXplOiBidG4taGVyby1nbG93LXNpemUsXG4gIGJ0bi1oZXJvLWRhbmdlci1nbG93LXNpemU6IGJ0bi1oZXJvLWdsb3ctc2l6ZSxcbiAgYnRuLWhlcm8tc2Vjb25kYXJ5LWdsb3ctc2l6ZTogYnRuLWhlcm8tZ2xvdy1zaXplLFxuICBidG4taGVyby1kZWdyZWU6IDIwZGVnLFxuICBidG4taGVyby1wcmltYXJ5LWRlZ3JlZTogYnRuLWhlcm8tZGVncmVlLFxuICBidG4taGVyby1zdWNjZXNzLWRlZ3JlZTogYnRuLWhlcm8tZGVncmVlLFxuICBidG4taGVyby13YXJuaW5nLWRlZ3JlZTogMTBkZWcsXG4gIGJ0bi1oZXJvLWluZm8tZGVncmVlOiAtMTBkZWcsXG4gIGJ0bi1oZXJvLWRhbmdlci1kZWdyZWU6IC0yMGRlZyxcbiAgYnRuLWhlcm8tc2Vjb25kYXJ5LWRlZ3JlZTogYnRuLWhlcm8tZGVncmVlLFxuICBidG4taGVyby1ib3JkZXItcmFkaXVzOiByYWRpdXMsXG5cbiAgYnRuLW91dGxpbmUtZmc6IGNvbG9yLWZnLWhlYWRpbmcsXG4gIGJ0bi1vdXRsaW5lLWhvdmVyLWZnOiAjZmZmZmZmLFxuICBidG4tb3V0bGluZS1mb2N1cy1mZzogY29sb3ItZmctaGVhZGluZyxcblxuICBidG4tZ3JvdXAtYmc6IGxheW91dC1iZyxcbiAgYnRuLWdyb3VwLWZnOiBjb2xvci1mZy1oZWFkaW5nLFxuICBidG4tZ3JvdXAtc2VwYXJhdG9yOiAjZGFkZmU2LFxuXG4gIGZvcm0tY29udHJvbC10ZXh0LXByaW1hcnktY29sb3I6IGNvbG9yLWZnLWhlYWRpbmcsXG4gIGZvcm0tY29udHJvbC1iZzogY29sb3ItYmcsXG4gIGZvcm0tY29udHJvbC1mb2N1cy1iZzogY29sb3ItYmcsXG5cbiAgZm9ybS1jb250cm9sLWJvcmRlci13aWR0aDogMnB4LFxuICBmb3JtLWNvbnRyb2wtYm9yZGVyLXR5cGU6IHNvbGlkLFxuICBmb3JtLWNvbnRyb2wtYm9yZGVyLXJhZGl1czogcmFkaXVzLFxuICBmb3JtLWNvbnRyb2wtc2VtaS1yb3VuZC1ib3JkZXItcmFkaXVzOiAwLjc1cmVtLFxuICBmb3JtLWNvbnRyb2wtcm91bmQtYm9yZGVyLXJhZGl1czogMS41cmVtLFxuICBmb3JtLWNvbnRyb2wtYm9yZGVyLWNvbG9yOiAjZGFkZmU2LFxuICBmb3JtLWNvbnRyb2wtc2VsZWN0ZWQtYm9yZGVyLWNvbG9yOiBjb2xvci1zdWNjZXNzLFxuXG4gIGZvcm0tY29udHJvbC1pbmZvLWJvcmRlci1jb2xvcjogY29sb3ItaW5mbyxcbiAgZm9ybS1jb250cm9sLXN1Y2Nlc3MtYm9yZGVyLWNvbG9yOiBjb2xvci1zdWNjZXNzLFxuICBmb3JtLWNvbnRyb2wtZGFuZ2VyLWJvcmRlci1jb2xvcjogY29sb3ItZGFuZ2VyLFxuICBmb3JtLWNvbnRyb2wtd2FybmluZy1ib3JkZXItY29sb3I6IGNvbG9yLXdhcm5pbmcsXG5cbiAgZm9ybS1jb250cm9sLXBsYWNlaG9sZGVyLWNvbG9yOiBjb2xvci1mZyxcbiAgZm9ybS1jb250cm9sLXBsYWNlaG9sZGVyLWZvbnQtc2l6ZTogMXJlbSxcblxuICBmb3JtLWNvbnRyb2wtZm9udC1zaXplOiAxcmVtLFxuICBmb3JtLWNvbnRyb2wtcGFkZGluZzogMC43NXJlbSAxLjEyNXJlbSxcbiAgZm9ybS1jb250cm9sLWZvbnQtc2l6ZS1zbTogZm9udC1zaXplLXNtLFxuICBmb3JtLWNvbnRyb2wtcGFkZGluZy1zbTogMC4zNzVyZW0gMS4xMjVyZW0sXG4gIGZvcm0tY29udHJvbC1mb250LXNpemUtbGc6IGZvbnQtc2l6ZS1sZyxcbiAgZm9ybS1jb250cm9sLXBhZGRpbmctbGc6IDEuMTI1cmVtLFxuXG4gIGZvcm0tY29udHJvbC1sYWJlbC1mb250LXdlaWdodDogNDAwLFxuXG4gIGZvcm0tY29udHJvbC1mZWVkYmFjay1mb250LXNpemU6IDAuODc1cmVtLFxuICBmb3JtLWNvbnRyb2wtZmVlZGJhY2stZm9udC13ZWlnaHQ6IGZvbnQtd2VpZ2h0LW5vcm1hbCxcblxuICBjaGVja2JveC1iZzogdHJhbnNwYXJlbnQsXG4gIGNoZWNrYm94LXNpemU6IDEuMjVyZW0sXG4gIGNoZWNrYm94LWJvcmRlci1zaXplOiAycHgsXG4gIGNoZWNrYm94LWJvcmRlci1jb2xvcjogZm9ybS1jb250cm9sLWJvcmRlci1jb2xvcixcbiAgY2hlY2tib3gtY2hlY2ttYXJrOiB0cmFuc3BhcmVudCxcblxuICBjaGVja2JveC1jaGVja2VkLWJnOiB0cmFuc3BhcmVudCxcbiAgY2hlY2tib3gtY2hlY2tlZC1zaXplOiAxLjI1cmVtLFxuICBjaGVja2JveC1jaGVja2VkLWJvcmRlci1zaXplOiAycHgsXG4gIGNoZWNrYm94LWNoZWNrZWQtYm9yZGVyLWNvbG9yOiBjb2xvci1zdWNjZXNzLFxuICBjaGVja2JveC1jaGVja2VkLWNoZWNrbWFyazogY29sb3ItZmctaGVhZGluZyxcblxuICBjaGVja2JveC1kaXNhYmxlZC1iZzogdHJhbnNwYXJlbnQsXG4gIGNoZWNrYm94LWRpc2FibGVkLXNpemU6IDEuMjVyZW0sXG4gIGNoZWNrYm94LWRpc2FibGVkLWJvcmRlci1zaXplOiAycHgsXG4gIGNoZWNrYm94LWRpc2FibGVkLWJvcmRlci1jb2xvcjogY29sb3ItZmctaGVhZGluZyxcbiAgY2hlY2tib3gtZGlzYWJsZWQtY2hlY2ttYXJrOiBjb2xvci1mZy1oZWFkaW5nLFxuXG4gIHJhZGlvLWZnOiBjb2xvci1zdWNjZXNzLFxuXG4gIG1vZGFsLWZvbnQtc2l6ZTogZm9udC1zaXplLFxuICBtb2RhbC1saW5lLWhlaWdodDogbGluZS1oZWlnaHQsXG4gIG1vZGFsLWZvbnQtd2VpZ2h0OiBmb250LXdlaWdodC1ub3JtYWwsXG4gIG1vZGFsLWZnOiBjb2xvci1mZy10ZXh0LFxuICBtb2RhbC1mZy1oZWFkaW5nOiBjb2xvci1mZy1oZWFkaW5nLFxuICBtb2RhbC1iZzogY29sb3ItYmcsXG4gIG1vZGFsLWJvcmRlcjogdHJhbnNwYXJlbnQsXG4gIG1vZGFsLWJvcmRlci1yYWRpdXM6IHJhZGl1cyxcbiAgbW9kYWwtcGFkZGluZzogcGFkZGluZyxcbiAgbW9kYWwtaGVhZGVyLWZvbnQtZmFtaWx5OiBmb250LXNlY29uZGFyeSxcbiAgbW9kYWwtaGVhZGVyLWZvbnQtd2VpZ2h0OiBmb250LXdlaWdodC1ib2xkZXIsXG4gIG1vZGFsLWhlYWRlci1mb250LXNpemU6IGZvbnQtc2l6ZS1sZyxcbiAgbW9kYWwtYm9keS1mb250LWZhbWlseTogZm9udC1tYWluLFxuICBtb2RhbC1ib2R5LWZvbnQtd2VpZ2h0OiBmb250LXdlaWdodC1ub3JtYWwsXG4gIG1vZGFsLWJvZHktZm9udC1zaXplOiBmb250LXNpemUsXG4gIG1vZGFsLXNlcGFyYXRvcjogc2VwYXJhdG9yLFxuXG4gIGJhZGdlLWZnLXRleHQ6IGNvbG9yLXdoaXRlLFxuICBiYWRnZS1wcmltYXJ5LWJnLWNvbG9yOiBjb2xvci1wcmltYXJ5LFxuICBiYWRnZS1zdWNjZXNzLWJnLWNvbG9yOiBjb2xvci1zdWNjZXNzLFxuICBiYWRnZS1pbmZvLWJnLWNvbG9yOiBjb2xvci1pbmZvLFxuICBiYWRnZS13YXJuaW5nLWJnLWNvbG9yOiBjb2xvci13YXJuaW5nLFxuICBiYWRnZS1kYW5nZXItYmctY29sb3I6IGNvbG9yLWRhbmdlcixcblxuICBwcm9ncmVzcy1iYXItaGVpZ2h0LXhsZzogMS43NXJlbSxcbiAgcHJvZ3Jlc3MtYmFyLWhlaWdodC1sZzogMS41cmVtLFxuICBwcm9ncmVzcy1iYXItaGVpZ2h0OiAxLjM3NXJlbSxcbiAgcHJvZ3Jlc3MtYmFyLWhlaWdodC1zbTogMS4yNXJlbSxcbiAgcHJvZ3Jlc3MtYmFyLWhlaWdodC14czogMXJlbSxcbiAgcHJvZ3Jlc3MtYmFyLWFuaW1hdGlvbi1kdXJhdGlvbjogNDAwbXMsXG4gIHByb2dyZXNzLWJhci1mb250LXNpemUteGxnOiBmb250LXNpemUteGxnLFxuICBwcm9ncmVzcy1iYXItZm9udC1zaXplLWxnOiBmb250LXNpemUtbGcsXG4gIHByb2dyZXNzLWJhci1mb250LXNpemU6IGZvbnQtc2l6ZSxcbiAgcHJvZ3Jlc3MtYmFyLWZvbnQtc2l6ZS1zbTogZm9udC1zaXplLXNtLFxuICBwcm9ncmVzcy1iYXItZm9udC1zaXplLXhzOiBmb250LXNpemUteHMsXG4gIHByb2dyZXNzLWJhci1yYWRpdXM6IHJhZGl1cyxcbiAgcHJvZ3Jlc3MtYmFyLWJnOiBsYXlvdXQtYmcsXG4gIHByb2dyZXNzLWJhci1mb250LWNvbG9yOiBjb2xvci13aGl0ZSxcbiAgcHJvZ3Jlc3MtYmFyLWZvbnQtd2VpZ2h0OiBmb250LXdlaWdodC1ib2xkLFxuICBwcm9ncmVzcy1iYXItZGVmYXVsdC1iZzogY29sb3ItaW5mbyxcbiAgcHJvZ3Jlc3MtYmFyLXByaW1hcnktYmc6IGNvbG9yLXByaW1hcnksXG4gIHByb2dyZXNzLWJhci1zdWNjZXNzLWJnOiBjb2xvci1zdWNjZXNzLFxuICBwcm9ncmVzcy1iYXItaW5mby1iZzogY29sb3ItaW5mbyxcbiAgcHJvZ3Jlc3MtYmFyLXdhcm5pbmctYmc6IGNvbG9yLXdhcm5pbmcsXG4gIHByb2dyZXNzLWJhci1kYW5nZXItYmc6IGNvbG9yLWRhbmdlcixcblxuICBhbGVydC1mb250LXNpemU6IGZvbnQtc2l6ZSxcbiAgYWxlcnQtbGluZS1oZWlnaHQ6IGxpbmUtaGVpZ2h0LFxuICBhbGVydC1mb250LXdlaWdodDogZm9udC13ZWlnaHQtbm9ybWFsLFxuICBhbGVydC1mZzogY29sb3Itd2hpdGUsXG4gIGFsZXJ0LW91dGxpbmUtZmc6IGNvbG9yLWZnLFxuICBhbGVydC1iZzogY29sb3ItYmcsXG4gIGFsZXJ0LWFjdGl2ZS1iZzogY29sb3ItZmcsXG4gIGFsZXJ0LWRpc2FibGVkLWJnOiBjb2xvci1kaXNhYmxlZCxcbiAgYWxlcnQtZGlzYWJsZWQtZmc6IGNvbG9yLWZnLFxuICBhbGVydC1wcmltYXJ5LWJnOiBjb2xvci1wcmltYXJ5LFxuICBhbGVydC1pbmZvLWJnOiBjb2xvci1pbmZvLFxuICBhbGVydC1zdWNjZXNzLWJnOiBjb2xvci1zdWNjZXNzLFxuICBhbGVydC13YXJuaW5nLWJnOiBjb2xvci13YXJuaW5nLFxuICBhbGVydC1kYW5nZXItYmc6IGNvbG9yLWRhbmdlcixcbiAgYWxlcnQtaGVpZ2h0LXh4c21hbGw6IDUycHgsXG4gIGFsZXJ0LWhlaWdodC14c21hbGw6IDcycHgsXG4gIGFsZXJ0LWhlaWdodC1zbWFsbDogOTJweCxcbiAgYWxlcnQtaGVpZ2h0LW1lZGl1bTogMTEycHgsXG4gIGFsZXJ0LWhlaWdodC1sYXJnZTogMTMycHgsXG4gIGFsZXJ0LWhlaWdodC14bGFyZ2U6IDE1MnB4LFxuICBhbGVydC1oZWlnaHQteHhsYXJnZTogMTcycHgsXG4gIGFsZXJ0LXNoYWRvdzogbm9uZSxcbiAgYWxlcnQtYm9yZGVyLXJhZGl1czogcmFkaXVzLFxuICBhbGVydC1wYWRkaW5nOiAxcmVtIDEuMTI1cmVtLFxuICBhbGVydC1jbG9zYWJsZS1wYWRkaW5nOiAzcmVtLFxuICBhbGVydC1idXR0b24tcGFkZGluZzogM3JlbSxcbiAgYWxlcnQtbWFyZ2luOiBtYXJnaW4sXG5cbiAgY2hhdC1mb250LXNpemU6IGZvbnQtc2l6ZSxcbiAgY2hhdC1mZzogY29sb3Itd2hpdGUsXG4gIGNoYXQtYmc6IGNvbG9yLWJnLFxuICBjaGF0LWJvcmRlci1yYWRpdXM6IHJhZGl1cyxcbiAgY2hhdC1mZy10ZXh0OiBjb2xvci1mZy10ZXh0LFxuICBjaGF0LWhlaWdodC14eHNtYWxsOiA5NnB4LFxuICBjaGF0LWhlaWdodC14c21hbGw6IDIxNnB4LFxuICBjaGF0LWhlaWdodC1zbWFsbDogMzM2cHgsXG4gIGNoYXQtaGVpZ2h0LW1lZGl1bTogNDU2cHgsXG4gIGNoYXQtaGVpZ2h0LWxhcmdlOiA1NzZweCxcbiAgY2hhdC1oZWlnaHQteGxhcmdlOiA2OTZweCxcbiAgY2hhdC1oZWlnaHQteHhsYXJnZTogODE2cHgsXG4gIGNoYXQtYm9yZGVyOiBib3JkZXIsXG4gIGNoYXQtcGFkZGluZzogcGFkZGluZyxcbiAgY2hhdC1zaGFkb3c6IHNoYWRvdyxcbiAgY2hhdC1zZXBhcmF0b3I6IHNlcGFyYXRvcixcbiAgY2hhdC1tZXNzYWdlLWZnOiBjb2xvci13aGl0ZSxcbiAgY2hhdC1tZXNzYWdlLWJnOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICM0Y2E2ZmYsICM1OWJmZmYpLFxuICBjaGF0LW1lc3NhZ2UtcmVwbHktYmc6IGNvbG9yLWJnLWFjdGl2ZSxcbiAgY2hhdC1tZXNzYWdlLXJlcGx5LWZnOiBjb2xvci1mZy10ZXh0LFxuICBjaGF0LW1lc3NhZ2UtYXZhdGFyLWJnOiBjb2xvci1mZyxcbiAgY2hhdC1tZXNzYWdlLXNlbmRlci1mZzogY29sb3ItZmcsXG4gIGNoYXQtbWVzc2FnZS1xdW90ZS1mZzogY29sb3ItZmcsXG4gIGNoYXQtbWVzc2FnZS1xdW90ZS1iZzogY29sb3ItYmctYWN0aXZlLFxuICBjaGF0LW1lc3NhZ2UtZmlsZS1mZzogY29sb3ItZmcsXG4gIGNoYXQtbWVzc2FnZS1maWxlLWJnOiB0cmFuc3BhcmVudCxcbiAgY2hhdC1mb3JtLWJnOiB0cmFuc3BhcmVudCxcbiAgY2hhdC1mb3JtLWZnOiBjb2xvci1mZy1oZWFkaW5nLFxuICBjaGF0LWZvcm0tYm9yZGVyOiBzZXBhcmF0b3IsXG4gIGNoYXQtZm9ybS1wbGFjZWhvbGRlci1mZzogY29sb3ItZmcsXG4gIGNoYXQtZm9ybS1hY3RpdmUtYm9yZGVyOiBjb2xvci1mZyxcbiAgY2hhdC1hY3RpdmUtYmc6IGNvbG9yLWZnLFxuICBjaGF0LWRpc2FibGVkLWJnOiBjb2xvci1kaXNhYmxlZCxcbiAgY2hhdC1kaXNhYmxlZC1mZzogY29sb3ItZmcsXG4gIGNoYXQtcHJpbWFyeS1iZzogY29sb3ItcHJpbWFyeSxcbiAgY2hhdC1pbmZvLWJnOiBjb2xvci1pbmZvLFxuICBjaGF0LXN1Y2Nlc3MtYmc6IGNvbG9yLXN1Y2Nlc3MsXG4gIGNoYXQtd2FybmluZy1iZzogY29sb3Itd2FybmluZyxcbiAgY2hhdC1kYW5nZXItYmc6IGNvbG9yLWRhbmdlcixcblxuICBzcGlubmVyLWJnOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuODMpLFxuICBzcGlubmVyLWNpcmNsZS1iZzogY29sb3ItYmctYWN0aXZlLFxuICBzcGlubmVyLWZnOiBjb2xvci1mZy10ZXh0LFxuICBzcGlubmVyLWFjdGl2ZS1iZzogY29sb3ItZmcsXG4gIHNwaW5uZXItZGlzYWJsZWQtYmc6IGNvbG9yLWRpc2FibGVkLFxuICBzcGlubmVyLWRpc2FibGVkLWZnOiBjb2xvci1mZyxcbiAgc3Bpbm5lci1wcmltYXJ5LWJnOiBjb2xvci1wcmltYXJ5LFxuICBzcGlubmVyLWluZm8tYmc6IGNvbG9yLWluZm8sXG4gIHNwaW5uZXItc3VjY2Vzcy1iZzogY29sb3Itc3VjY2VzcyxcbiAgc3Bpbm5lci13YXJuaW5nLWJnOiBjb2xvci13YXJuaW5nLFxuICBzcGlubmVyLWRhbmdlci1iZzogY29sb3ItZGFuZ2VyLFxuICBzcGlubmVyLXh4c21hbGw6IDEuMjVyZW0sXG4gIHNwaW5uZXIteHNtYWxsOiAxLjVyZW0sXG4gIHNwaW5uZXItc21hbGw6IDEuNzVyZW0sXG4gIHNwaW5uZXItbWVkaXVtOiAycmVtLFxuICBzcGlubmVyLWxhcmdlOiAyLjI1cmVtLFxuICBzcGlubmVyLXhsYXJnZTogMi41cmVtLFxuICBzcGlubmVyLXh4bGFyZ2U6IDNyZW0sXG5cbiAgc3RlcHBlci1pbmRleC1zaXplOiAycmVtLFxuICBzdGVwcGVyLWxhYmVsLWZvbnQtc2l6ZTogZm9udC1zaXplLXNtLFxuICBzdGVwcGVyLWxhYmVsLWZvbnQtd2VpZ2h0OiBmb250LXdlaWdodC1ib2xkZXIsXG4gIHN0ZXBwZXItYWNjZW50LWNvbG9yOiBjb2xvci1wcmltYXJ5LFxuICBzdGVwcGVyLWNvbXBsZXRlZC1mZzogY29sb3Itd2hpdGUsXG4gIHN0ZXBwZXItZmc6IGNvbG9yLWZnLFxuICBzdGVwcGVyLWNvbXBsZXRlZC1pY29uLXNpemU6IDEuNXJlbSxcbiAgc3RlcHBlci1jb21wbGV0ZWQtaWNvbi13ZWlnaHQ6IGZvbnQtd2VpZ2h0LXVsdHJhLWJvbGQsXG4gIHN0ZXBwZXItc3RlcC1wYWRkaW5nOiBwYWRkaW5nLFxuXG4gIGFjY29yZGlvbi1wYWRkaW5nOiBwYWRkaW5nLFxuICBhY2NvcmRpb24tc2VwYXJhdG9yOiBzZXBhcmF0b3IsXG4gIGFjY29yZGlvbi1oZWFkZXItZm9udC1mYW1pbHk6IGZvbnQtc2Vjb25kYXJ5LFxuICBhY2NvcmRpb24taGVhZGVyLWZvbnQtc2l6ZTogZm9udC1zaXplLWxnLFxuICBhY2NvcmRpb24taGVhZGVyLWZvbnQtd2VpZ2h0OiBmb250LXdlaWdodC1ub3JtYWwsXG4gIGFjY29yZGlvbi1oZWFkZXItZmctaGVhZGluZzogY29sb3ItZmctaGVhZGluZyxcbiAgYWNjb3JkaW9uLWhlYWRlci1kaXNhYmxlZC1mZzogY29sb3ItZmcsXG4gIGFjY29yZGlvbi1oZWFkZXItYm9yZGVyLXdpZHRoOiAxcHgsXG4gIGFjY29yZGlvbi1oZWFkZXItYm9yZGVyLXR5cGU6IHNvbGlkLFxuICBhY2NvcmRpb24taGVhZGVyLWJvcmRlci1jb2xvcjogYWNjb3JkaW9uLXNlcGFyYXRvcixcbiAgYWNjb3JkaW9uLWJvcmRlci1yYWRpdXM6IHJhZGl1cyxcbiAgYWNjb3JkaW9uLWl0ZW0tYmc6IGNvbG9yLWJnLFxuICBhY2NvcmRpb24taXRlbS1mb250LXNpemU6IGZvbnQtc2l6ZSxcbiAgYWNjb3JkaW9uLWl0ZW0tZm9udC13ZWlnaHQ6IGZvbnQtd2VpZ2h0LW5vcm1hbCxcbiAgYWNjb3JkaW9uLWl0ZW0tZm9udC1mYW1pbHk6IGZvbnQtbWFpbixcbiAgYWNjb3JkaW9uLWl0ZW0tZmctdGV4dDogY29sb3ItZmctdGV4dCxcbiAgYWNjb3JkaW9uLWl0ZW0tc2hhZG93OiBzaGFkb3csXG5cbiAgbGlzdC1pdGVtLWJvcmRlci1jb2xvcjogdGFicy1zZXBhcmF0b3IsXG4gIGxpc3QtaXRlbS1wYWRkaW5nOiAxcmVtLFxuXG4gIGNhbGVuZGFyLXdpZHRoOiAyMS44NzVyZW0sXG4gIGNhbGVuZGFyLWhlaWdodDogMzFyZW0sXG4gIGNhbGVuZGFyLWhlYWRlci10aXRsZS1mb250LXNpemU6IGZvbnQtc2l6ZS14bGcsXG4gIGNhbGVuZGFyLWhlYWRlci10aXRsZS1mb250LXdlaWdodDogZm9udC13ZWlnaHQtYm9sZCxcbiAgY2FsZW5kYXItaGVhZGVyLXN1Yi10aXRsZS1mb250LXNpemU6IGZvbnQtc2l6ZSxcbiAgY2FsZW5kYXItaGVhZGVyLXN1Yi10aXRsZS1mb250LXdlaWdodDogZm9udC13ZWlnaHQtdGhpbixcbiAgY2FsZW5kYXItbmF2aWdhdGlvbi1idXR0b24td2lkdGg6IDEwcmVtLFxuICBjYWxlbmRhci1zZWxlY3RlZC1pdGVtLWJnOiBjb2xvci1zdWNjZXNzLFxuICBjYWxlbmRhci1ob3Zlci1pdGVtLWJnOiBjYWxlbmRhci1zZWxlY3RlZC1pdGVtLWJnLFxuICBjYWxlbmRhci10b2RheS1pdGVtLWJnOiBjb2xvci1iZy1hY3RpdmUsXG4gIGNhbGVuZGFyLWFjdGl2ZS1pdGVtLWJnOiBjb2xvci1zdWNjZXNzLFxuICBjYWxlbmRhci1mZzogY29sb3ItZmctdGV4dCxcbiAgY2FsZW5kYXItc2VsZWN0ZWQtZmc6IGNvbG9yLXdoaXRlLFxuICBjYWxlbmRhci10b2RheS1mZzogY2FsZW5kYXItZmcsXG4gIGNhbGVuZGFyLWRheS1jZWxsLXdpZHRoOiAyLjYyNXJlbSxcbiAgY2FsZW5kYXItZGF5LWNlbGwtaGVpZ2h0OiAyLjYyNXJlbSxcbiAgY2FsZW5kYXItbW9udGgtY2VsbC13aWR0aDogNC4yNXJlbSxcbiAgY2FsZW5kYXItbW9udGgtY2VsbC1oZWlnaHQ6IDIuMzc1cmVtLFxuICBjYWxlbmRhci15ZWFyLWNlbGwtd2lkdGg6IGNhbGVuZGFyLW1vbnRoLWNlbGwtd2lkdGgsXG4gIGNhbGVuZGFyLXllYXItY2VsbC1oZWlnaHQ6IGNhbGVuZGFyLW1vbnRoLWNlbGwtaGVpZ2h0LFxuICBjYWxlbmRhci1pbmFjdGl2ZS1vcGFjaXR5OiAwLjUsXG4gIGNhbGVuZGFyLWRpc2FibGVkLW9wYWNpdHk6IDAuMyxcbiAgY2FsZW5kYXItYm9yZGVyLXJhZGl1czogcmFkaXVzLFxuICBjYWxlbmRhci13ZWVrZGF5LXdpZHRoOiBjYWxlbmRhci1kYXktY2VsbC13aWR0aCxcbiAgY2FsZW5kYXItd2Vla2RheS1oZWlnaHQ6IDEuNzVyZW0sXG4gIGNhbGVuZGFyLXdlZWtkYXktZm9udC1zaXplOiBmb250LXNpemUteHMsXG4gIGNhbGVuZGFyLXdlZWtkYXktZm9udC13ZWlnaHQ6IGZvbnQtd2VpZ2h0LW5vcm1hbCxcbiAgY2FsZW5kYXItd2Vla2RheS1mZzogY29sb3ItZmcsXG4gIGNhbGVuZGFyLXdlZWtkYXktaG9saWRheS1mZzogY29sb3ItZGFuZ2VyLFxuICBjYWxlbmRhci1yYW5nZS1iZy1pbi1yYW5nZTogI2ViZmJmMixcblxuICBjYWxlbmRhci1sYXJnZS13aWR0aDogMjQuMzc1cmVtLFxuICBjYWxlbmRhci1sYXJnZS1oZWlnaHQ6IDMzLjEyNXJlbSxcbiAgY2FsZW5kYXItZGF5LWNlbGwtbGFyZ2Utd2lkdGg6IDNyZW0sXG4gIGNhbGVuZGFyLWRheS1jZWxsLWxhcmdlLWhlaWdodDogM3JlbSxcbiAgY2FsZW5kYXItbW9udGgtY2VsbC1sYXJnZS13aWR0aDogNC4yNXJlbSxcbiAgY2FsZW5kYXItbW9udGgtY2VsbC1sYXJnZS1oZWlnaHQ6IDIuMzc1cmVtLFxuICBjYWxlbmRhci15ZWFyLWNlbGwtbGFyZ2Utd2lkdGg6IGNhbGVuZGFyLW1vbnRoLWNlbGwtd2lkdGgsXG4gIGNhbGVuZGFyLXllYXItY2VsbC1sYXJnZS1oZWlnaHQ6IGNhbGVuZGFyLW1vbnRoLWNlbGwtaGVpZ2h0LFxuKTtcblxuLy8gcmVnaXN0ZXIgdGhlIHRoZW1lXG4kbmItdGhlbWVzOiBuYi1yZWdpc3Rlci10aGVtZSgkdGhlbWUsIGRlZmF1bHQpO1xuIiwiLyoqXG4gKiBAbGljZW5zZVxuICogQ29weXJpZ2h0IEFrdmVvLiBBbGwgUmlnaHRzIFJlc2VydmVkLlxuICogTGljZW5zZWQgdW5kZXIgdGhlIE1JVCBMaWNlbnNlLiBTZWUgTGljZW5zZS50eHQgaW4gdGhlIHByb2plY3Qgcm9vdCBmb3IgbGljZW5zZSBpbmZvcm1hdGlvbi5cbiAqL1xuXG5AaW1wb3J0ICcuLi9jb3JlL2Z1bmN0aW9ucyc7XG5AaW1wb3J0ICcuLi9jb3JlL21peGlucyc7XG5AaW1wb3J0ICdkZWZhdWx0JztcblxuLy8gZGVmYXVsdCB0aGUgYmFzZSB0aGVtZVxuJHRoZW1lOiAoXG4gIHJhZGl1czogMC41cmVtLFxuXG4gIGNvbG9yLWJnOiAjM2QzNzgwLFxuICBjb2xvci1iZy1hY3RpdmU6ICM0OTQyOTksXG4gIGNvbG9yLWZnOiAjYTFhMWU1LFxuICBjb2xvci1mZy1oZWFkaW5nOiAjZmZmZmZmLFxuICBjb2xvci1mZy10ZXh0OiAjZDFkMWZmLFxuICBjb2xvci1mZy1oaWdobGlnaHQ6ICMwMGY5YTYsXG5cbiAgY29sb3ItZ3JheTogcmdiYSg4MSwgMTEzLCAxNjUsIDAuMTUpLFxuICBjb2xvci1uZXV0cmFsOiB0cmFuc3BhcmVudCxcbiAgY29sb3Itd2hpdGU6ICNmZmZmZmYsXG4gIGNvbG9yLWRpc2FibGVkOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuNCksXG5cbiAgY29sb3ItcHJpbWFyeTogIzc2NTlmZixcbiAgY29sb3Itc3VjY2VzczogIzAwZDk3NyxcbiAgY29sb3ItaW5mbzogIzAwODhmZixcbiAgY29sb3Itd2FybmluZzogI2ZmYTEwMCxcbiAgY29sb3ItZGFuZ2VyOiAjZmYzODZhLFxuXG4gIGxpbmstY29sb3I6ICMwMGY5YTYsXG4gIGxpbmstY29sb3ItaG92ZXI6ICMxNGZmYmUsXG5cbiAgc2VwYXJhdG9yOiAjMzQyZTczLFxuICBzaGFkb3c6IDAgOHB4IDIwcHggMCByZ2JhKDQwLCAzNywgODksIDAuNiksXG5cbiAgY2FyZC1oZWFkZXItZm9udC13ZWlnaHQ6IGZvbnQtd2VpZ2h0LWJvbGRlcixcblxuICBsYXlvdXQtYmc6ICMyZjI5NmIsXG5cbiAgc2Nyb2xsYmFyLWZnOiAjNTU0ZGIzLFxuICBzY3JvbGxiYXItYmc6ICMzMzJlNzMsXG5cbiAgcmFkaWFsLWdyYWRpZW50OiByYWRpYWwtZ3JhZGllbnQoY2lyY2xlIGF0IDUwJSA1MCUsICM0MjNmOGMsICMzMDJjNmUpLFxuICBsaW5lYXItZ3JhZGllbnQ6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgIzE3MTc0OSwgIzQxMzc4OSksXG5cbiAgc2lkZWJhci1mZzogY29sb3Itc2Vjb25kYXJ5LFxuICBzaWRlYmFyLWJnOiBjb2xvci1iZyxcblxuICBoZWFkZXItZmc6IGNvbG9yLXdoaXRlLFxuICBoZWFkZXItYmc6IGNvbG9yLWJnLFxuXG4gIGZvb3Rlci1mZzogY29sb3ItZmcsXG4gIGZvb3Rlci1iZzogY29sb3ItYmcsXG5cbiAgYWN0aW9ucy1mZzogY29sb3ItZmcsXG4gIGFjdGlvbnMtYmc6IGNvbG9yLWJnLFxuXG4gIHVzZXItZmc6IGNvbG9yLWJnLFxuICB1c2VyLWJnOiBjb2xvci1mZyxcbiAgdXNlci1mZy1oaWdobGlnaHQ6IGNvbG9yLWZnLWhpZ2hsaWdodCxcblxuICBwb3BvdmVyLWJvcmRlcjogY29sb3ItcHJpbWFyeSxcbiAgcG9wb3Zlci1zaGFkb3c6IHNoYWRvdyxcblxuICBjb250ZXh0LW1lbnUtYWN0aXZlLWJnOiBjb2xvci1wcmltYXJ5LFxuXG4gIGZvb3Rlci1oZWlnaHQ6IGhlYWRlci1oZWlnaHQsXG5cbiAgc2lkZWJhci13aWR0aDogMTYuMjVyZW0sXG4gIHNpZGViYXItd2lkdGgtY29tcGFjdDogMy40NXJlbSxcblxuICBtZW51LWZnOiBjb2xvci1mZyxcbiAgbWVudS1iZzogY29sb3ItYmcsXG4gIG1lbnUtYWN0aXZlLWZnOiBjb2xvci13aGl0ZSxcbiAgbWVudS1ncm91cC1mZzogY29sb3Itd2hpdGUsXG4gIG1lbnUtZm9udC13ZWlnaHQ6IGZvbnQtd2VpZ2h0LW5vcm1hbCxcbiAgbWVudS1hY3RpdmUtZm9udC13ZWlnaHQ6IGZvbnQtd2VpZ2h0LWJvbGRlcixcbiAgbWVudS1zdWJtZW51LWJnOiBsYXlvdXQtYmcsXG4gIG1lbnUtc3VibWVudS1mZzogY29sb3ItZmcsXG4gIG1lbnUtc3VibWVudS1hY3RpdmUtZmc6IGNvbG9yLWZnLWhlYWRpbmcsXG4gIG1lbnUtc3VibWVudS1hY3RpdmUtYmc6IHJnYmEoMCwgMjU1LCAxNzAsIDAuMjUpLFxuICBtZW51LXN1Ym1lbnUtYWN0aXZlLWJvcmRlci1jb2xvcjogY29sb3ItZmctaGlnaGxpZ2h0LFxuICBtZW51LXN1Ym1lbnUtYWN0aXZlLXNoYWRvdzogMCAycHggMTJweCAwIHJnYmEoMCwgMjU1LCAxNzAsIDAuMjUpLFxuICBtZW51LWl0ZW0tcGFkZGluZzogMC4yNXJlbSAwLjc1cmVtLFxuICBtZW51LWl0ZW0tc2VwYXJhdG9yOiB0cmFuc3BhcmVudCxcblxuICBidG4taGVyby1zaGFkb3c6IDAgNHB4IDEwcHggMCByZ2JhKDMzLCA3LCA3NywgMC41KSxcbiAgYnRuLWhlcm8tdGV4dC1zaGFkb3c6IDAgMXB4IDNweCByZ2JhKDAsIDAsIDAsIDAuMyksXG4gIGJ0bi1oZXJvLWJldmVsLXNpemU6IDAgM3B4IDAgMCxcbiAgYnRuLWhlcm8tZ2xvdy1zaXplOiAwIDJweCA4cHggMCxcbiAgYnRuLWhlcm8tcHJpbWFyeS1nbG93LXNpemU6IGJ0bi1oZXJvLWdsb3ctc2l6ZSxcbiAgYnRuLWhlcm8tc3VjY2Vzcy1nbG93LXNpemU6IGJ0bi1oZXJvLWdsb3ctc2l6ZSxcbiAgYnRuLWhlcm8td2FybmluZy1nbG93LXNpemU6IGJ0bi1oZXJvLWdsb3ctc2l6ZSxcbiAgYnRuLWhlcm8taW5mby1nbG93LXNpemU6IGJ0bi1oZXJvLWdsb3ctc2l6ZSxcbiAgYnRuLWhlcm8tZGFuZ2VyLWdsb3ctc2l6ZTogYnRuLWhlcm8tZ2xvdy1zaXplLFxuICBidG4taGVyby1zZWNvbmRhcnktZ2xvdy1zaXplOiBidG4taGVyby1nbG93LXNpemUsXG4gIGJ0bi1zZWNvbmRhcnktYm9yZGVyOiBjb2xvci1wcmltYXJ5LFxuICBidG4tb3V0bGluZS1mZzogY29sb3ItZmctaGVhZGluZyxcbiAgYnRuLW91dGxpbmUtaG92ZXItZmc6IGNvbG9yLWZnLWhlYWRpbmcsXG4gIGJ0bi1vdXRsaW5lLWZvY3VzLWZnOiBjb2xvci1mZy1oZWFkaW5nLFxuICBidG4tZ3JvdXAtYmc6ICMzNzMyNzMsXG4gIGJ0bi1ncm91cC1zZXBhcmF0b3I6ICMzMTJjNjYsXG5cbiAgZm9ybS1jb250cm9sLWJnOiAjMzczMTdhLFxuICBmb3JtLWNvbnRyb2wtZm9jdXMtYmc6IHNlcGFyYXRvcixcbiAgZm9ybS1jb250cm9sLWJvcmRlci1jb2xvcjogc2VwYXJhdG9yLFxuICBmb3JtLWNvbnRyb2wtc2VsZWN0ZWQtYm9yZGVyLWNvbG9yOiBjb2xvci1wcmltYXJ5LFxuXG4gIGNoZWNrYm94LWJnOiB0cmFuc3BhcmVudCxcbiAgY2hlY2tib3gtc2l6ZTogMS4yNXJlbSxcbiAgY2hlY2tib3gtYm9yZGVyLXNpemU6IDJweCxcbiAgY2hlY2tib3gtYm9yZGVyLWNvbG9yOiBjb2xvci1mZyxcbiAgY2hlY2tib3gtY2hlY2ttYXJrOiB0cmFuc3BhcmVudCxcblxuICBjaGVja2JveC1jaGVja2VkLWJnOiB0cmFuc3BhcmVudCxcbiAgY2hlY2tib3gtY2hlY2tlZC1zaXplOiAxLjI1cmVtLFxuICBjaGVja2JveC1jaGVja2VkLWJvcmRlci1zaXplOiAycHgsXG4gIGNoZWNrYm94LWNoZWNrZWQtYm9yZGVyLWNvbG9yOiBjb2xvci1zdWNjZXNzLFxuICBjaGVja2JveC1jaGVja2VkLWNoZWNrbWFyazogY29sb3ItZmctaGVhZGluZyxcblxuICBjaGVja2JveC1kaXNhYmxlZC1iZzogdHJhbnNwYXJlbnQsXG4gIGNoZWNrYm94LWRpc2FibGVkLXNpemU6IDEuMjVyZW0sXG4gIGNoZWNrYm94LWRpc2FibGVkLWJvcmRlci1zaXplOiAycHgsXG4gIGNoZWNrYm94LWRpc2FibGVkLWJvcmRlci1jb2xvcjogY29sb3ItZmctaGVhZGluZyxcbiAgY2hlY2tib3gtZGlzYWJsZWQtY2hlY2ttYXJrOiBjb2xvci1mZy1oZWFkaW5nLFxuXG4gIHNlYXJjaC1iZzogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjMTcxNzQ5LCAjNDEzNzg5KSxcblxuICBzbWFydC10YWJsZS1oZWFkZXItZm9udC13ZWlnaHQ6IGZvbnQtd2VpZ2h0LW5vcm1hbCxcbiAgc21hcnQtdGFibGUtaGVhZGVyLWJnOiBjb2xvci1iZy1hY3RpdmUsXG4gIHNtYXJ0LXRhYmxlLWJnLWV2ZW46ICMzYTM0N2EsXG4gIHNtYXJ0LXRhYmxlLWJnLWFjdGl2ZTogY29sb3ItYmctYWN0aXZlLFxuXG4gIHNtYXJ0LXRhYmxlLXBhZ2luZy1ib3JkZXItY29sb3I6IGNvbG9yLXByaW1hcnksXG4gIHNtYXJ0LXRhYmxlLXBhZ2luZy1ib3JkZXItd2lkdGg6IDJweCxcbiAgc21hcnQtdGFibGUtcGFnaW5nLWZnLWFjdGl2ZTogY29sb3ItZmctaGVhZGluZyxcbiAgc21hcnQtdGFibGUtcGFnaW5nLWJnLWFjdGl2ZTogY29sb3ItcHJpbWFyeSxcbiAgc21hcnQtdGFibGUtcGFnaW5nLWhvdmVyOiByZ2JhKDAsIDAsIDAsIDAuMiksXG5cbiAgYmFkZ2UtZmctdGV4dDogY29sb3Itd2hpdGUsXG4gIGJhZGdlLXByaW1hcnktYmctY29sb3I6IGNvbG9yLXByaW1hcnksXG4gIGJhZGdlLXN1Y2Nlc3MtYmctY29sb3I6IGNvbG9yLXN1Y2Nlc3MsXG4gIGJhZGdlLWluZm8tYmctY29sb3I6IGNvbG9yLWluZm8sXG4gIGJhZGdlLXdhcm5pbmctYmctY29sb3I6IGNvbG9yLXdhcm5pbmcsXG4gIGJhZGdlLWRhbmdlci1iZy1jb2xvcjogY29sb3ItZGFuZ2VyLFxuXG4gIHNwaW5uZXItYmc6IHJnYmEoNjEsIDU1LCAxMjgsIDAuOSksXG4gIHN0ZXBwZXItYWNjZW50LWNvbG9yOiBjb2xvci1zdWNjZXNzLFxuXG4gIGNhbGVuZGFyLWFjdGl2ZS1pdGVtLWJnOiBjb2xvci1wcmltYXJ5LFxuICBjYWxlbmRhci1zZWxlY3RlZC1pdGVtLWJnOiBjb2xvci1wcmltYXJ5LFxuICBjYWxlbmRhci1yYW5nZS1iZy1pbi1yYW5nZTogIzRlNDA5NSxcbiAgY2FsZW5kYXItdG9kYXktaXRlbS1iZzogIzM1MmY2ZSxcbik7XG5cbi8vIHJlZ2lzdGVyIHRoZSB0aGVtZVxuJG5iLXRoZW1lczogbmItcmVnaXN0ZXItdGhlbWUoJHRoZW1lLCBjb3NtaWMsIGRlZmF1bHQpO1xuIiwiLyoqXG4gKiBAbGljZW5zZVxuICogQ29weXJpZ2h0IEFrdmVvLiBBbGwgUmlnaHRzIFJlc2VydmVkLlxuICogTGljZW5zZWQgdW5kZXIgdGhlIE1JVCBMaWNlbnNlLiBTZWUgTGljZW5zZS50eHQgaW4gdGhlIHByb2plY3Qgcm9vdCBmb3IgbGljZW5zZSBpbmZvcm1hdGlvbi5cbiAqL1xuXG5AaW1wb3J0ICcuLi9jb3JlL2Z1bmN0aW9ucyc7XG5AaW1wb3J0ICcuLi9jb3JlL21peGlucyc7XG5AaW1wb3J0ICdkZWZhdWx0JztcblxuLy8gZGVmYXVsdCB0aGUgYmFzZSB0aGVtZVxuJHRoZW1lOiAoXG4gIGhlYWRlci1mZzogI2Y3ZmFmYixcbiAgaGVhZGVyLWJnOiAjMTExMjE4LFxuXG4gIGxheW91dC1iZzogI2YxZjVmOCxcblxuICBjb2xvci1mZy1oZWFkaW5nOiAjMTgxODE4LFxuICBjb2xvci1mZy10ZXh0OiAjNGI0YjRiLFxuICBjb2xvci1mZy1oaWdobGlnaHQ6IGNvbG9yLWZnLFxuXG4gIHNlcGFyYXRvcjogI2NkZDVkYyxcblxuICByYWRpdXM6IDAuMTdyZW0sXG5cbiAgc2Nyb2xsYmFyLWJnOiAjZTNlOWVlLFxuXG4gIGNvbG9yLXByaW1hcnk6ICM3M2ExZmYsXG4gIGNvbG9yLXN1Y2Nlc3M6ICM1ZGNmZTMsXG4gIGNvbG9yLWluZm86ICNiYTdmZWMsXG4gIGNvbG9yLXdhcm5pbmc6ICNmZmEzNmIsXG4gIGNvbG9yLWRhbmdlcjogI2ZmNmI4MyxcblxuICBidG4tc2Vjb25kYXJ5LWJnOiAjZWRmMmY1LFxuICBidG4tc2Vjb25kYXJ5LWJvcmRlcjogI2VkZjJmNSxcblxuICBhY3Rpb25zLWZnOiAjZDNkYmU1LFxuICBhY3Rpb25zLWJnOiBjb2xvci1iZyxcblxuICBzaWRlYmFyLWJnOiAjZTNlOWVlLFxuXG4gIGJvcmRlci1jb2xvcjogI2Q1ZGJlMCxcblxuICBtZW51LWZvbnQtd2VpZ2h0OiBmb250LXdlaWdodC1ib2xkZXIsXG4gIG1lbnUtZmc6IGNvbG9yLWZnLXRleHQsXG4gIG1lbnUtYmc6ICNlM2U5ZWUsXG4gIG1lbnUtYWN0aXZlLWZnOiBjb2xvci1mZy1oZWFkaW5nLFxuICBtZW51LWFjdGl2ZS1iZzogbWVudS1iZyxcblxuICBtZW51LXN1Ym1lbnUtYmc6IG1lbnUtYmcsXG4gIG1lbnUtc3VibWVudS1mZzogY29sb3ItZmctdGV4dCxcbiAgbWVudS1zdWJtZW51LWFjdGl2ZS1mZzogY29sb3ItZmctaGVhZGluZyxcbiAgbWVudS1zdWJtZW51LWFjdGl2ZS1iZzogI2NkZDVkYyxcbiAgbWVudS1zdWJtZW51LWFjdGl2ZS1ib3JkZXItY29sb3I6IG1lbnUtc3VibWVudS1hY3RpdmUtYmcsXG4gIG1lbnUtc3VibWVudS1hY3RpdmUtc2hhZG93OiBub25lLFxuICBtZW51LXN1Ym1lbnUtaG92ZXItZmc6IG1lbnUtc3VibWVudS1hY3RpdmUtZmcsXG4gIG1lbnUtc3VibWVudS1ob3Zlci1iZzogbWVudS1iZyxcbiAgbWVudS1zdWJtZW51LWl0ZW0tYm9yZGVyLXdpZHRoOiAwLjEyNXJlbSxcbiAgbWVudS1zdWJtZW51LWl0ZW0tYm9yZGVyLXJhZGl1czogcmFkaXVzLFxuICBtZW51LXN1Ym1lbnUtaXRlbS1wYWRkaW5nOiAwLjVyZW0gMXJlbSxcbiAgbWVudS1zdWJtZW51LWl0ZW0tY29udGFpbmVyLXBhZGRpbmc6IDAgMS4yNXJlbSxcbiAgbWVudS1zdWJtZW51LXBhZGRpbmc6IDAuNXJlbSxcblxuICBidG4tYm9yZGVyLXJhZGl1czogYnRuLXNlbWktcm91bmQtYm9yZGVyLXJhZGl1cyxcblxuICBidG4taGVyby1kZWdyZWU6IDBkZWcsXG4gIGJ0bi1oZXJvLXByaW1hcnktZGVncmVlOiBidG4taGVyby1kZWdyZWUsXG4gIGJ0bi1oZXJvLXN1Y2Nlc3MtZGVncmVlOiBidG4taGVyby1kZWdyZWUsXG4gIGJ0bi1oZXJvLXdhcm5pbmctZGVncmVlOiBidG4taGVyby1kZWdyZWUsXG4gIGJ0bi1oZXJvLWluZm8tZGVncmVlOiBidG4taGVyby1kZWdyZWUsXG4gIGJ0bi1oZXJvLWRhbmdlci1kZWdyZWU6IGJ0bi1oZXJvLWRlZ3JlZSxcbiAgYnRuLWhlcm8tc2Vjb25kYXJ5LWRlZ3JlZTogYnRuLWhlcm8tZGVncmVlLFxuICBidG4taGVyby1nbG93LXNpemU6IDAgMCAyMHB4IDAsXG4gIGJ0bi1oZXJvLXByaW1hcnktZ2xvdy1zaXplOiBidG4taGVyby1nbG93LXNpemUsXG4gIGJ0bi1oZXJvLXN1Y2Nlc3MtZ2xvdy1zaXplOiBidG4taGVyby1nbG93LXNpemUsXG4gIGJ0bi1oZXJvLXdhcm5pbmctZ2xvdy1zaXplOiBidG4taGVyby1nbG93LXNpemUsXG4gIGJ0bi1oZXJvLWluZm8tZ2xvdy1zaXplOiBidG4taGVyby1nbG93LXNpemUsXG4gIGJ0bi1oZXJvLWRhbmdlci1nbG93LXNpemU6IGJ0bi1oZXJvLWdsb3ctc2l6ZSxcbiAgYnRuLWhlcm8tc2Vjb25kYXJ5LWdsb3ctc2l6ZTogMCAwIDAgMCxcbiAgYnRuLWhlcm8tYm9yZGVyLXJhZGl1czogYnRuLWJvcmRlci1yYWRpdXMsXG5cbiAgY2FyZC1zaGFkb3c6IG5vbmUsXG4gIGNhcmQtYm9yZGVyLXdpZHRoOiAxcHgsXG4gIGNhcmQtYm9yZGVyLWNvbG9yOiBib3JkZXItY29sb3IsXG4gIGNhcmQtaGVhZGVyLWJvcmRlci13aWR0aDogMCxcblxuICBsaW5rLWNvbG9yOiAjNWRjZmUzLFxuICBsaW5rLWNvbG9yLWhvdmVyOiAjN2RjZmUzLFxuICBsaW5rLWNvbG9yLXZpc2l0ZWQ6IGxpbmstY29sb3IsXG5cbiAgYWN0aW9ucy1zZXBhcmF0b3I6ICNmMWY0ZjUsXG5cbiAgbW9kYWwtc2VwYXJhdG9yOiBib3JkZXItY29sb3IsXG5cbiAgdGFicy1zZWxlY3RlZDogY29sb3ItcHJpbWFyeSxcbiAgdGFicy1zZXBhcmF0b3I6ICNlYmVjZWUsXG5cbiAgc21hcnQtdGFibGUtcGFnaW5nLWJnLWFjdGl2ZTogY29sb3ItcHJpbWFyeSxcblxuICByb3V0ZS10YWJzLXNlbGVjdGVkOiBjb2xvci1wcmltYXJ5LFxuXG4gIHBvcG92ZXItYm9yZGVyOiBjb2xvci1wcmltYXJ5LFxuXG4gIGZvb3Rlci1zaGFkb3c6IG5vbmUsXG4gIGZvb3Rlci1zZXBhcmF0b3I6IGJvcmRlci1jb2xvcixcbiAgZm9vdGVyLWZnLWhpZ2hsaWdodDogIzJhMmEyYSxcblxuICBjYWxlbmRhci10b2RheS1pdGVtLWJnOiAjYTJiMmM3LFxuICBjYWxlbmRhci1hY3RpdmUtaXRlbS1iZzogY29sb3ItcHJpbWFyeSxcbiAgY2FsZW5kYXItcmFuZ2UtYmctaW4tcmFuZ2U6ICNlM2VjZmUsXG4gIGNhbGVuZGFyLXRvZGF5LWZnOiBjb2xvci13aGl0ZSxcbik7XG5cbi8vIHJlZ2lzdGVyIHRoZSB0aGVtZVxuJG5iLXRoZW1lczogbmItcmVnaXN0ZXItdGhlbWUoJHRoZW1lLCBjb3Jwb3JhdGUsIGRlZmF1bHQpO1xuIiwiQGltcG9ydCAnLi4vLi4vLi4vLi4vLi4vLi4vQHRoZW1lL3N0eWxlcy90aGVtZXMnO1xuXG5AaW5jbHVkZSBuYi1pbnN0YWxsLWNvbXBvbmVudCgpIHtcbiAgLmZ1bGwtd2lkdGgge1xuICAgIGZsZXg6IDE7XG4gICAgbWluLXdpZHRoOiAyMjBweDtcbiAgfVxuXG4gIG5iLWNoZWNrYm94IHtcbiAgICBtYXJnaW4tYm90dG9tOiAxcmVtO1xuICB9XG5cbiAgLmZvcm0taW5saW5lPioge1xuICAgIEBpbmNsdWRlIG5iLWx0cihtYXJnaW4sIDAgMS41cmVtIDEuNXJlbSAwKTtcbiAgICBAaW5jbHVkZSBuYi1ydGwobWFyZ2luLCAwIDAgMS41cmVtIDEuNXJlbSk7XG4gIH1cblxuICBuYi1jYXJkLmlubGluZS1mb3JtLWNhcmQgbmItY2FyZC1ib2R5IHtcbiAgICBwYWRkaW5nLWJvdHRvbTogMDtcbiAgfVxufVxuXG4uaGkge1xuICBib3JkZXI6IDFweCBzb2xpZDtcbiAgLyogYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogMTZweDsgKi9cbiAgLyogYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDE2cHg7ICovXG4gIC8qIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDE2cHg7ICovXG4gIC8qIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAxNnB4OyAqL1xufVxuXG4uYm9yZGVyLWxlZnQtcmlnaHQge1xuICBib3JkZXI6IDFweCBzb2xpZDtcbiAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogMTZweDtcbn1cblxuLmJvcmRlci10b3AtcmlnaHQge1xuICBib3JkZXI6IDFweCBzb2xpZDtcbiAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogMTZweDtcbn1cblxuLmJvcmRlci10b3AtbGVmdCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkO1xuICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogMTZweDtcbn1cblxuLmJvcmRlci1ib3R0b20tbGVmdCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkO1xuICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogMTZweDtcbn1cblxudGFibGUge1xuICB3aWR0aDogLXdlYmtpdC1maWxsLWF2YWlsYWJsZTtcbn1cblxuYSB7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cblxudGV4dGFyZWEge1xuICBib3JkZXI6IG5vbmU7XG59XG5cbi9kZWVwLyBuZzItc21hcnQtdGFibGUgLm5nMi1zbWFydC10aXRsZSB7XG4gIGNvbG9yOiB3aGl0ZSAhaW1wb3J0YW50XG59XG5cbi9kZWVwLyBuZzItc21hcnQtdGFibGUgaSB7XG4gIGNvbG9yOiAjMjEyNTI5ICFpbXBvcnRhbnRcbn1cblxuL2RlZXAvIG1vZGFsIC5tb2RhbC1kaWFsb2cge1xuICBtYXgtd2lkdGg6IDEwMDBweCAhaW1wb3J0YW50O1xufVxuIl19 */"

/***/ }),

/***/ "./src/app/pages/ui-features/suppliers/supplier/suppliers-details/supplier-details/supplier-details.component.ts":
/*!***********************************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/suppliers/supplier/suppliers-details/supplier-details/supplier-details.component.ts ***!
  \***********************************************************************************************************************/
/*! exports provided: SupplierDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SupplierDetailsComponent", function() { return SupplierDetailsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var angular_custom_modal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! angular-custom-modal */ "./node_modules/angular-custom-modal/angular-custom-modal.es5.js");
/* harmony import */ var _confirmation_dialog_confirmation_dialog_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../confirmation-dialog/confirmation-dialog.service */ "./src/app/confirmation-dialog/confirmation-dialog.service.ts");
/* harmony import */ var _services_currency_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../../services/currency.service */ "./src/app/services/currency.service.ts");
/* harmony import */ var _services_admin_isertParts_insert_parts_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../../../services/admin/isertParts/insert-parts.service */ "./src/app/services/admin/isertParts/insert-parts.service.ts");
/* harmony import */ var _services_suppliers_supplier_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../../../services/suppliers/supplier.service */ "./src/app/services/suppliers/supplier.service.ts");
/* harmony import */ var _services_token_token_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../../../services/token/token.service */ "./src/app/services/token/token.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




 // VIPIN CODE FOR CURRENCY



var SupplierDetailsComponent = /** @class */ (function () {
    function SupplierDetailsComponent(userServ, supplierServiceServ, confirmationDialogService, currency, // VIPIN CODE FOR CURRENCY
    parts // VIPIN CODE FOR CURRENCY
    ) {
        var _this = this;
        this.userServ = userServ;
        this.supplierServiceServ = supplierServiceServ;
        this.confirmationDialogService = confirmationDialogService;
        this.currency = currency;
        this.parts = parts;
        this.itemsLength = 0;
        this.suppliers = {};
        this.regExpr = /[₹,]/g;
        this.item = {};
        this.source = new ng2_smart_table__WEBPACK_IMPORTED_MODULE_1__["LocalDataSource"]();
        /**
           Inventory Items Details
           **/
        this.settings = {
            add: {
                addButtonContent: '<i class="nb-plus"></i>',
                createButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
            },
            edit: {
                editButtonContent: '<i class="nb-edit"></i>',
                saveButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
            },
            delete: {
                deleteButtonContent: '<i class="nb-trash"></i>',
                confirmDelete: true,
            },
            actions: {
                delete: false,
                add: false,
                edit: false,
            },
            columns: {
                itemName: {
                    title: 'Item Name',
                    type: 'string',
                    filter: false,
                },
                quantity: {
                    title: 'Quantity',
                    type: 'string',
                    filter: false,
                },
                unit: {
                    title: 'Unit Cost',
                    type: 'string',
                    valuePrepareFunction: function (value) { return value === 'Total' ? value : _this.currency.convertToInrFormat(value); },
                    filter: false,
                },
                total: {
                    title: 'Total Cost',
                    type: 'string',
                    // valuePrepareFunction: (value) => { return value === 'Total' ? value : this.currency.convertToInrFormat(value) },           // VIPIN CODE FOR CURRENCY
                    filter: false,
                },
            },
        };
        // VIPIN CODE FOR CURRENCY
        this.moduleCreate = true;
        this.moduleEdit = true;
        this.moduleView = true;
        this.getSupplierDetail();
        this.getPartNames();
    }
    /**
      * Get Inventory Detail
      */
    SupplierDetailsComponent.prototype.getSupplierDetail = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.supplierServiceServ.suppliers
                    .subscribe(function (data) {
                    _this.suppliers = data;
                    _this.id = data.id;
                    _this.items = data.supplierItems;
                    _this.source.load(_this.items);
                    // this.onCreateConfirm(data);
                }, function (error) {
                    console.log(error);
                });
                return [2 /*return*/];
            });
        });
    };
    /**
         Edit  Inventory
       **/
    SupplierDetailsComponent.prototype.editSupplier = function (Data) {
        return __awaiter(this, void 0, void 0, function () {
            var resultAlert, value, result, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.confirmationDialogService.confirm('Alert Message', 'Are you sure you want to Update?')];
                    case 1:
                        resultAlert = _a.sent();
                        if (!resultAlert) return [3 /*break*/, 6];
                        value = Data.value;
                        _a.label = 2;
                    case 2:
                        _a.trys.push([2, 4, , 5]);
                        value.supplierItems = this.items;
                        return [4 /*yield*/, this.supplierServiceServ.UpdateSupplierDetails(value, this.id)];
                    case 3:
                        result = _a.sent();
                        this.supplierServiceServ.suppliers.next(result);
                        return [3 /*break*/, 5];
                    case 4:
                        error_1 = _a.sent();
                        console.log(error_1);
                        return [3 /*break*/, 5];
                    case 5:
                        Data.confirm.resolve();
                        return [3 /*break*/, 7];
                    case 6:
                        Data.confirm.reject();
                        _a.label = 7;
                    case 7: return [2 /*return*/];
                }
            });
        });
    };
    SupplierDetailsComponent.prototype.onDeleteConfirm = function (event) {
        if (window.confirm('Are you sure you want to delete?')) {
            event.confirm.resolve();
        }
        else {
            event.confirm.reject();
        }
    };
    SupplierDetailsComponent.prototype.onUserRowSelect = function (event) {
        if (this.moduleCreate || this.moduleEdit) {
            this.rowCount = event.data.id;
            this.itemName = event.data.itemName;
            this.quantity = event.data.quantity;
            this.un = event.data.unit;
            this.unit = this.currency.convertToInrFormat(this.un);
            this.total = event.data.total;
            this.componentInsideModalSuppliersUpdate.open();
        }
    };
    SupplierDetailsComponent.prototype.changeQty = function (qty) {
        this.qt = qty;
        if (this.price !== undefined) {
        }
        else {
            this.price = this.un;
        }
        this.calculationPrice = this.qt * this.price;
        this.total = this.currency.convertToInrFormat(this.calculationPrice);
    };
    SupplierDetailsComponent.prototype.changeUnit = function (amt) {
        if (amt[0] === '₹') {
            amt = amt.replace(this.regExpr, "");
        }
        this.price = amt;
        if (this.qt !== undefined) {
        }
        else {
            this.qt = this.quantity;
        }
        this.calculationPrice = this.qt * this.price;
        this.unit = this.currency.convertToInrFormat(this.price);
        this.total = this.currency.convertToInrFormat(this.calculationPrice);
    };
    SupplierDetailsComponent.prototype.onUpdateConfirm = function (data) {
        var _this = this;
        this.updateUnit = data.value.amountCredit.slice(1, data.value.amountCredit.length);
        this.updateUnit = this.updateUnit.replace(this.regExpr, "");
        // this.updateUnitNew = Number(this.updateUnit);
        delete data.value.unit;
        this.listOfMaterials.forEach(function (ele) {
            if (data.value.itemName === ele.partName) {
                _this.stage = ele.stages;
                _this.hsnCode = ele.HSN_Code;
            }
        });
        Object.assign(data.value, { 'unit': this.updateUnit, 'id': this.rowCount, 'hSNumber': this.hsnCode, 'stages': this.stage });
        this.updateFunction(data.value, this.id);
    };
    SupplierDetailsComponent.prototype.updateFunction = function (a, b) {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.supplierServiceServ.updateSupplierItems(a, b)];
                    case 1:
                        result = _a.sent();
                        this.source.load(result.supplierItems);
                        this.componentInsideModalSuppliersUpdate.close();
                        return [2 /*return*/];
                }
            });
        });
    };
    SupplierDetailsComponent.prototype.getPartNames = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, error_2;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        _a = this;
                        return [4 /*yield*/, this.parts.getPartDetails()];
                    case 1:
                        _a.listOfMaterials = _b.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        error_2 = _b.sent();
                        console.error(error_2);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    SupplierDetailsComponent.prototype.onCreateConfirm = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            var value, result;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        value = data.value;
                        this.items.forEach(function (ele) {
                            if (ele !== undefined) {
                                _this.itemsLength = _this.itemsLength + 1;
                            }
                        });
                        this.listOfMaterials.forEach(function (ele) {
                            if (value.itemName === ele.partName) {
                                _this.stage = ele.stages;
                                _this.hsnCode = ele.HSN_Code;
                            }
                        });
                        Object.assign(value, { 'id': this.itemsLength + 1, 'hSNumber': this.hsnCode, 'stages': this.stage });
                        return [4 /*yield*/, this.supplierServiceServ.addSupplierItems(value, this.id)];
                    case 1:
                        result = _a.sent();
                        this.items.push(value);
                        this.source.load(this.items);
                        this.componentInsideModalSuppliers.close();
                        return [2 /*return*/];
                }
            });
        });
    };
    SupplierDetailsComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.userServ.userAccess !== undefined) {
            this.userServ.userAccess
                .subscribe(function (data) {
                data.forEach(function (ele) {
                    if (ele.name === 'Suppliers') {
                        _this.moduleCreate = ele.create;
                        _this.moduleEdit = ele.edit;
                        _this.moduleView = ele.view;
                    }
                });
            }, function (error) {
                console.log(error);
            });
        }
        this.supplierServiceServ.suppliers
            .subscribe(function (data) {
            _this.getSupplierDetail();
        }, function (error) {
            console.log(error);
        });
        this.getSupplierDetail();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('componentInsideModal'),
        __metadata("design:type", angular_custom_modal__WEBPACK_IMPORTED_MODULE_2__["ModalComponent"])
    ], SupplierDetailsComponent.prototype, "componentInsideModal", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('componentInsideModalSuppliers'),
        __metadata("design:type", angular_custom_modal__WEBPACK_IMPORTED_MODULE_2__["ModalComponent"])
    ], SupplierDetailsComponent.prototype, "componentInsideModalSuppliers", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('componentInsideModalSuppliersUpdate'),
        __metadata("design:type", angular_custom_modal__WEBPACK_IMPORTED_MODULE_2__["ModalComponent"])
    ], SupplierDetailsComponent.prototype, "componentInsideModalSuppliersUpdate", void 0);
    SupplierDetailsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-supplier-details',
            template: __webpack_require__(/*! ./supplier-details.component.html */ "./src/app/pages/ui-features/suppliers/supplier/suppliers-details/supplier-details/supplier-details.component.html"),
            styles: [__webpack_require__(/*! ./supplier-details.component.scss */ "./src/app/pages/ui-features/suppliers/supplier/suppliers-details/supplier-details/supplier-details.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_token_token_service__WEBPACK_IMPORTED_MODULE_7__["TokenService"],
            _services_suppliers_supplier_service__WEBPACK_IMPORTED_MODULE_6__["SupplierService"],
            _confirmation_dialog_confirmation_dialog_service__WEBPACK_IMPORTED_MODULE_3__["ConfirmationDialogService"],
            _services_currency_service__WEBPACK_IMPORTED_MODULE_4__["CurrencyService"],
            _services_admin_isertParts_insert_parts_service__WEBPACK_IMPORTED_MODULE_5__["InsertPartsService"] // VIPIN CODE FOR CURRENCY
        ])
    ], SupplierDetailsComponent);
    return SupplierDetailsComponent;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/suppliers/supplier/suppliers-details/supplier-invoices/supplier-invoices.component.html":
/*!***************************************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/suppliers/supplier/suppliers-details/supplier-invoices/supplier-invoices.component.html ***!
  \***************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <div class=\"row\">\n        <div class=\"col-5\">\n            <p class=\"back\">Company Name & Supplier Name : {{suppliers.CompanyName}} & {{suppliers.SupplierName}}</p>\n        </div>\n    </div>\n    <nb-card-body *ngIf=\"!show\">\n        <button *ngIf='moduleCreate || moduleEdit' (click)=\"show = !show\" class=\"btn btn-outline-primary \" shape=\"semi-round\" style=\"float:right; margin-bottom: 15px;\">New Invoice</button>\n\n        <ng2-smart-table [settings]=\"settings\" [source]=\"source\" (deleteConfirm)=\"onDeleteConfirm($event)\" (userRowSelect)=\"onUserRowSelect()\">\n        </ng2-smart-table>\n    </nb-card-body>\n</div>\n\n<div class=\"container-fluid\">\n    <div *ngIf=\"show\">\n        <ngx-invoices-templates></ngx-invoices-templates>\n        <button (click)=\"show = !show\" class=\"btn btn-outline-primary \" shape=\"semi-round\">Go Back</button>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/pages/ui-features/suppliers/supplier/suppliers-details/supplier-invoices/supplier-invoices.component.scss":
/*!***************************************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/suppliers/supplier/suppliers-details/supplier-invoices/supplier-invoices.component.scss ***!
  \***************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ngx-invoices-templates .noPrint {\n  display: none; }\n\n.back {\n  margin-top: 18px; }\n\n/deep/ ng2-smart-table .ng2-smart-title {\n  color: white !important; }\n\n/deep/ ng2-smart-table i {\n  color: #212529 !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3ByYWthc2gvVmlwaW4vaXJvbmJpcmQvMjBGRUIyMDIwL2lyb25iaXJkLWNsaWVudHMvc3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9zdXBwbGllcnMvc3VwcGxpZXIvc3VwcGxpZXJzLWRldGFpbHMvc3VwcGxpZXItaW52b2ljZXMvc3VwcGxpZXItaW52b2ljZXMuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0E7RUFDSSxhQUFhLEVBQUE7O0FBR2pCO0VBQ0ksZ0JBQWdCLEVBQUE7O0FBRXBCO0VBQ0ksdUJBQ0EsRUFBQTs7QUFFQTtFQUNJLHlCQUNBLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9zdXBwbGllcnMvc3VwcGxpZXIvc3VwcGxpZXJzLWRldGFpbHMvc3VwcGxpZXItaW52b2ljZXMvc3VwcGxpZXItaW52b2ljZXMuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcbm5neC1pbnZvaWNlcy10ZW1wbGF0ZXMgLm5vUHJpbnR7XG4gICAgZGlzcGxheTogbm9uZTtcbn1cblxuLmJhY2t7XG4gICAgbWFyZ2luLXRvcDogMThweDtcbn1cbi9kZWVwLyBuZzItc21hcnQtdGFibGUgLm5nMi1zbWFydC10aXRsZXtcbiAgICBjb2xvcjp3aGl0ZSAhaW1wb3J0YW50XG4gICAgfVxuXG4gICAgL2RlZXAvIG5nMi1zbWFydC10YWJsZSAgaXtcbiAgICAgICAgY29sb3I6IzIxMjUyOSAhaW1wb3J0YW50XG4gICAgICAgIH0iXX0= */"

/***/ }),

/***/ "./src/app/pages/ui-features/suppliers/supplier/suppliers-details/supplier-invoices/supplier-invoices.component.ts":
/*!*************************************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/suppliers/supplier/suppliers-details/supplier-invoices/supplier-invoices.component.ts ***!
  \*************************************************************************************************************************/
/*! exports provided: SupplierInvoicesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SupplierInvoicesComponent", function() { return SupplierInvoicesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var _core_data_invoices_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../@core/data/invoices.service */ "./src/app/@core/data/invoices.service.ts");
/* harmony import */ var _services_suppliers_supplier_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../services/suppliers/supplier.service */ "./src/app/services/suppliers/supplier.service.ts");
/* harmony import */ var _services_token_token_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../../services/token/token.service */ "./src/app/services/token/token.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var SupplierInvoicesComponent = /** @class */ (function () {
    function SupplierInvoicesComponent(service, userServ, supplierServiceServ) {
        this.service = service;
        this.userServ = userServ;
        this.supplierServiceServ = supplierServiceServ;
        this.suppliers = {};
        this.supplierChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.source = new ng2_smart_table__WEBPACK_IMPORTED_MODULE_1__["LocalDataSource"]();
        this.moduleCreate = true;
        this.moduleEdit = true;
        this.moduleView = true;
        var data = this.service.getData();
        this.source.load(data);
        this.getSupplierDetail();
        // console.log(this.getSupplierDetail())
    }
    SupplierInvoicesComponent.prototype.onSearch = function (query) {
        if (query === void 0) { query = ''; }
        this.source.setFilter([
            // fields we want to include in the search
            {
                field: 'id',
                search: query
            },
            {
                field: 'customerName',
                search: query
            },
            {
                field: 'siteName',
                search: query
            },
            {
                field: 'agreement',
                search: query
            },
            {
                field: 'payment',
                search: query
            },
            {
                field: 'status',
                search: query
            }
        ], false);
        // second parameter specifying whether to perform 'AND' or 'OR' search 
        // (meaning all columns should contain search query or at least one)
        // 'AND' by default, so changing to 'OR' by setting false here
    };
    SupplierInvoicesComponent.prototype.onDeleteConfirm = function (event) {
        if (window.confirm('Are you sure you want to delete?')) {
            event.confirm.resolve();
        }
        else {
            event.confirm.reject();
        }
    };
    SupplierInvoicesComponent.prototype.onUserRowSelect = function () {
        if (this.moduleCreate || this.moduleEdit) {
            this.show = !this.show;
        }
    };
    /**
    * Get Suppliers Detail
    */
    SupplierInvoicesComponent.prototype.getSupplierDetail = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.supplierServiceServ.suppliers
                    .subscribe(function (data) {
                    _this.suppliers = data;
                    _this.id = data.id;
                }, function (error) {
                    console.log(error);
                });
                return [2 /*return*/];
            });
        });
    };
    SupplierInvoicesComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.userServ.userAccess !== undefined) {
            this.userServ.userAccess
                .subscribe(function (data) {
                data.forEach(function (ele) {
                    if (ele.name === 'Suppliers') {
                        _this.moduleCreate = ele.create;
                        _this.moduleEdit = ele.edit;
                        _this.moduleView = ele.view;
                    }
                });
            }, function (error) {
                console.log(error);
            });
        }
        if (this.moduleCreate || this.moduleEdit) {
            this.settings = {
                add: {
                    addButtonContent: '<i class="nb-plus"></i>',
                    createButtonContent: '<i class="nb-checkmark"></i>',
                    cancelButtonContent: '<i class="nb-close"></i>',
                },
                edit: {
                    editButtonContent: '<i class="nb-edit"></i>',
                    saveButtonContent: '<i class="nb-checkmark"></i>',
                    cancelButtonContent: '<i class="nb-close"></i>',
                },
                delete: {
                    deleteButtonContent: '<i class="nb-trash"></i>',
                    confirmDelete: true,
                },
                view: {
                    viewButtonContent: '<i  class="nb-edit"></i>'
                },
                actions: {
                    edit: false,
                    add: false,
                    position: 'right'
                },
                columns: {
                    id: {
                        title: 'Invoice id',
                        type: 'number',
                    },
                    date: {
                        title: 'Date',
                        type: 'string',
                    },
                    amount: {
                        title: 'Total Amount',
                        type: 'string',
                    },
                    paid: {
                        title: ' Amount Paid',
                        type: 'string',
                    },
                    balance: {
                        title: 'Remaining Amount',
                        type: 'string',
                    },
                },
            };
        }
        else {
            this.settings = {
                add: {
                    addButtonContent: '<i class="nb-plus"></i>',
                    createButtonContent: '<i class="nb-checkmark"></i>',
                    cancelButtonContent: '<i class="nb-close"></i>',
                },
                edit: {
                    editButtonContent: '<i class="nb-edit"></i>',
                    saveButtonContent: '<i class="nb-checkmark"></i>',
                    cancelButtonContent: '<i class="nb-close"></i>',
                },
                delete: {
                    deleteButtonContent: '<i class="nb-trash"></i>',
                    confirmDelete: true,
                },
                view: {
                    viewButtonContent: '<i  class="nb-edit"></i>'
                },
                actions: {
                    edit: false,
                    add: false,
                    delete: false,
                    position: 'right'
                },
                columns: {
                    id: {
                        title: 'Invoice id',
                        type: 'number',
                    },
                    date: {
                        title: 'Date',
                        type: 'string',
                    },
                    amount: {
                        title: 'Total Amount',
                        type: 'string',
                    },
                    paid: {
                        title: ' Amount Paid',
                        type: 'string',
                    },
                    balance: {
                        title: 'Remaining Amount',
                        type: 'string',
                    },
                },
            };
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], SupplierInvoicesComponent.prototype, "supplierChange", void 0);
    SupplierInvoicesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-supplier-invoices',
            template: __webpack_require__(/*! ./supplier-invoices.component.html */ "./src/app/pages/ui-features/suppliers/supplier/suppliers-details/supplier-invoices/supplier-invoices.component.html"),
            styles: [__webpack_require__(/*! ./supplier-invoices.component.scss */ "./src/app/pages/ui-features/suppliers/supplier/suppliers-details/supplier-invoices/supplier-invoices.component.scss")]
        }),
        __metadata("design:paramtypes", [_core_data_invoices_service__WEBPACK_IMPORTED_MODULE_2__["InvoicesService"],
            _services_token_token_service__WEBPACK_IMPORTED_MODULE_4__["TokenService"],
            _services_suppliers_supplier_service__WEBPACK_IMPORTED_MODULE_3__["SupplierService"]])
    ], SupplierInvoicesComponent);
    return SupplierInvoicesComponent;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/suppliers/supplier/suppliers-details/suppliers-details.component.html":
/*!*********************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/suppliers/supplier/suppliers-details/suppliers-details.component.html ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nb-card>\n  <ngb-tabset>\n    <ngb-tab title=\"Details\">\n      <ng-template ngbTabContent>\n        <ngx-supplier-details></ngx-supplier-details>\n      </ng-template>\n    </ngb-tab>\n    <ngb-tab title=\"Purchase Order\">\n      <ng-template ngbTabContent>\n        <ngx-po></ngx-po>\n      </ng-template>\n    </ngb-tab>\n    <ngb-tab title=\"Invoices\">\n      <ng-template ngbTabContent>\n        <ngx-supplier-invoices></ngx-supplier-invoices>\n      </ng-template>\n    </ngb-tab>\n  </ngb-tabset>\n</nb-card>"

/***/ }),

/***/ "./src/app/pages/ui-features/suppliers/supplier/suppliers-details/suppliers-details.component.scss":
/*!*********************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/suppliers/supplier/suppliers-details/suppliers-details.component.scss ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "nb-card {\n  background-color: #E1F5FE; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3ByYWthc2gvVmlwaW4vaXJvbmJpcmQvMjBGRUIyMDIwL2lyb25iaXJkLWNsaWVudHMvc3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9zdXBwbGllcnMvc3VwcGxpZXIvc3VwcGxpZXJzLWRldGFpbHMvc3VwcGxpZXJzLWRldGFpbHMuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSx5QkFBd0IsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VpLWZlYXR1cmVzL3N1cHBsaWVycy9zdXBwbGllci9zdXBwbGllcnMtZGV0YWlscy9zdXBwbGllcnMtZGV0YWlscy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIm5iLWNhcmR7XG4gICAgYmFja2dyb3VuZC1jb2xvcjojRTFGNUZFO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/ui-features/suppliers/supplier/suppliers-details/suppliers-details.component.ts":
/*!*******************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/suppliers/supplier/suppliers-details/suppliers-details.component.ts ***!
  \*******************************************************************************************************/
/*! exports provided: SuppliersDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SuppliersDetailsComponent", function() { return SuppliersDetailsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SuppliersDetailsComponent = /** @class */ (function () {
    function SuppliersDetailsComponent() {
    }
    SuppliersDetailsComponent.prototype.ngOnInit = function () {
    };
    SuppliersDetailsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-suppliers-details',
            template: __webpack_require__(/*! ./suppliers-details.component.html */ "./src/app/pages/ui-features/suppliers/supplier/suppliers-details/suppliers-details.component.html"),
            styles: [__webpack_require__(/*! ./suppliers-details.component.scss */ "./src/app/pages/ui-features/suppliers/supplier/suppliers-details/suppliers-details.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], SuppliersDetailsComponent);
    return SuppliersDetailsComponent;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/suppliers/suppliers-routing.module.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/ui-features/suppliers/suppliers-routing.module.ts ***!
  \*************************************************************************/
/*! exports provided: SuppliersRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SuppliersRoutingModule", function() { return SuppliersRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _suppliers_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./suppliers.component */ "./src/app/pages/ui-features/suppliers/suppliers.component.ts");
/* harmony import */ var _supplier_supplier_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./supplier/supplier.component */ "./src/app/pages/ui-features/suppliers/supplier/supplier.component.ts");
/* harmony import */ var _purchase_order_purchase_order_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./purchase-order/purchase-order.component */ "./src/app/pages/ui-features/suppliers/purchase-order/purchase-order.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var routes = [{
        path: '',
        component: _suppliers_component__WEBPACK_IMPORTED_MODULE_2__["SuppliersComponent"],
        children: [{
                path: 'Suppliers',
                component: _supplier_supplier_component__WEBPACK_IMPORTED_MODULE_3__["SupplierComponent"],
            },
            {
                path: 'PurchaseOrder',
                component: _purchase_order_purchase_order_component__WEBPACK_IMPORTED_MODULE_4__["PurchaseOrderComponent"],
            }
        ]
    }];
var SuppliersRoutingModule = /** @class */ (function () {
    function SuppliersRoutingModule() {
    }
    SuppliersRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], SuppliersRoutingModule);
    return SuppliersRoutingModule;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/suppliers/suppliers.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/pages/ui-features/suppliers/suppliers.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./src/app/pages/ui-features/suppliers/suppliers.component.scss":
/*!**********************************************************************!*\
  !*** ./src/app/pages/ui-features/suppliers/suppliers.component.scss ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VpLWZlYXR1cmVzL3N1cHBsaWVycy9zdXBwbGllcnMuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/pages/ui-features/suppliers/suppliers.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/pages/ui-features/suppliers/suppliers.component.ts ***!
  \********************************************************************/
/*! exports provided: SuppliersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SuppliersComponent", function() { return SuppliersComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SuppliersComponent = /** @class */ (function () {
    function SuppliersComponent() {
    }
    SuppliersComponent.prototype.ngOnInit = function () {
    };
    SuppliersComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-suppliers',
            template: __webpack_require__(/*! ./suppliers.component.html */ "./src/app/pages/ui-features/suppliers/suppliers.component.html"),
            styles: [__webpack_require__(/*! ./suppliers.component.scss */ "./src/app/pages/ui-features/suppliers/suppliers.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], SuppliersComponent);
    return SuppliersComponent;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/suppliers/suppliers.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/pages/ui-features/suppliers/suppliers.module.ts ***!
  \*****************************************************************/
/*! exports provided: SuppliersModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SuppliersModule", function() { return SuppliersModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _core_data_smart_table_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../@core/data/smart-table.service */ "./src/app/@core/data/smart-table.service.ts");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var ngx_echarts__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-echarts */ "./node_modules/ngx-echarts/ngx-echarts.es5.js");
/* harmony import */ var _swimlane_ngx_charts__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @swimlane/ngx-charts */ "./node_modules/@swimlane/ngx-charts/release/index.js");
/* harmony import */ var _swimlane_ngx_charts__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_swimlane_ngx_charts__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var _nebular_theme__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @nebular/theme */ "./node_modules/@nebular/theme/index.js");
/* harmony import */ var angular_custom_modal__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! angular-custom-modal */ "./node_modules/angular-custom-modal/angular-custom-modal.es5.js");
/* harmony import */ var _theme_theme_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../@theme/theme.module */ "./src/app/@theme/theme.module.ts");
/* harmony import */ var _asymmetrik_ngx_leaflet__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @asymmetrik/ngx-leaflet */ "./node_modules/@asymmetrik/ngx-leaflet/dist/index.js");
/* harmony import */ var _suppliers_routing_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./suppliers-routing.module */ "./src/app/pages/ui-features/suppliers/suppliers-routing.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _admin_admin_module__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../admin/admin.module */ "./src/app/pages/ui-features/admin/admin.module.ts");
/* harmony import */ var _suppliers_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./suppliers.component */ "./src/app/pages/ui-features/suppliers/suppliers.component.ts");
/* harmony import */ var _supplier_supplier_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./supplier/supplier.component */ "./src/app/pages/ui-features/suppliers/supplier/supplier.component.ts");
/* harmony import */ var _supplier_suppliers_details_suppliers_details_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./supplier/suppliers-details/suppliers-details.component */ "./src/app/pages/ui-features/suppliers/supplier/suppliers-details/suppliers-details.component.ts");
/* harmony import */ var _supplier_new_suppliers_new_suppliers_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./supplier/new-suppliers/new-suppliers.component */ "./src/app/pages/ui-features/suppliers/supplier/new-suppliers/new-suppliers.component.ts");
/* harmony import */ var _supplier_suppliers_details_supplier_details_supplier_details_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./supplier/suppliers-details/supplier-details/supplier-details.component */ "./src/app/pages/ui-features/suppliers/supplier/suppliers-details/supplier-details/supplier-details.component.ts");
/* harmony import */ var _supplier_suppliers_details_supplier_invoices_supplier_invoices_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./supplier/suppliers-details/supplier-invoices/supplier-invoices.component */ "./src/app/pages/ui-features/suppliers/supplier/suppliers-details/supplier-invoices/supplier-invoices.component.ts");
/* harmony import */ var _supplier_suppliers_details_po_po_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./supplier/suppliers-details/po/po.component */ "./src/app/pages/ui-features/suppliers/supplier/suppliers-details/po/po.component.ts");
/* harmony import */ var _purchase_order_purchase_order_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./purchase-order/purchase-order.component */ "./src/app/pages/ui-features/suppliers/purchase-order/purchase-order.component.ts");
/* harmony import */ var _purchase_order_purchases_purchases_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./purchase-order/purchases/purchases.component */ "./src/app/pages/ui-features/suppliers/purchase-order/purchases/purchases.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};























var SuppliersModule = /** @class */ (function () {
    function SuppliersModule() {
    }
    SuppliersModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                angular_custom_modal__WEBPACK_IMPORTED_MODULE_8__["ModalModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _suppliers_routing_module__WEBPACK_IMPORTED_MODULE_11__["SuppliersRoutingModule"],
                ng2_smart_table__WEBPACK_IMPORTED_MODULE_3__["Ng2SmartTableModule"],
                ngx_echarts__WEBPACK_IMPORTED_MODULE_4__["NgxEchartsModule"],
                _swimlane_ngx_charts__WEBPACK_IMPORTED_MODULE_5__["NgxChartsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_6__["NgbModule"],
                _theme_theme_module__WEBPACK_IMPORTED_MODULE_9__["ThemeModule"],
                _asymmetrik_ngx_leaflet__WEBPACK_IMPORTED_MODULE_10__["LeafletModule"],
                _nebular_theme__WEBPACK_IMPORTED_MODULE_7__["NbInputModule"],
                _admin_admin_module__WEBPACK_IMPORTED_MODULE_13__["AdminModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_12__["ReactiveFormsModule"]
            ],
            declarations: [
                _suppliers_component__WEBPACK_IMPORTED_MODULE_14__["SuppliersComponent"],
                _supplier_supplier_component__WEBPACK_IMPORTED_MODULE_15__["SupplierComponent"],
                _supplier_suppliers_details_suppliers_details_component__WEBPACK_IMPORTED_MODULE_16__["SuppliersDetailsComponent"],
                _supplier_new_suppliers_new_suppliers_component__WEBPACK_IMPORTED_MODULE_17__["NewSuppliersComponent"],
                _supplier_suppliers_details_supplier_details_supplier_details_component__WEBPACK_IMPORTED_MODULE_18__["SupplierDetailsComponent"],
                _supplier_suppliers_details_supplier_invoices_supplier_invoices_component__WEBPACK_IMPORTED_MODULE_19__["SupplierInvoicesComponent"],
                _supplier_suppliers_details_po_po_component__WEBPACK_IMPORTED_MODULE_20__["PoComponent"],
                _purchase_order_purchase_order_component__WEBPACK_IMPORTED_MODULE_21__["PurchaseOrderComponent"],
                _purchase_order_purchases_purchases_component__WEBPACK_IMPORTED_MODULE_22__["PurchasesComponent"],
            ],
            providers: [
                _core_data_smart_table_service__WEBPACK_IMPORTED_MODULE_2__["SmartTableService"],
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["DatePipe"]
            ],
        })
    ], SuppliersModule);
    return SuppliersModule;
}());



/***/ }),

/***/ "./src/app/services/suppliers/supplier.service.ts":
/*!********************************************************!*\
  !*** ./src/app/services/suppliers/supplier.service.ts ***!
  \********************************************************/
/*! exports provided: SupplierService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SupplierService", function() { return SupplierService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _settings_settings_services_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../settings/settings-services.service */ "./src/app/services/settings/settings-services.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SupplierService = /** @class */ (function () {
    function SupplierService(http, settings) {
        this.http = http;
        this.settings = settings;
        this.suppliers = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]({});
        this.API_URL = this.settings.BASE_API_URL;
    }
    SupplierService.prototype.createSupplier = function (data) {
        var _this = this;
        var url = this.API_URL + "/suppliers/suppliers-details";
        return new Promise(function (resolve, reject) {
            _this.http.post(url, data)
                .subscribe(resolve, reject);
        });
    };
    SupplierService.prototype.getSupplier = function () {
        var _this = this;
        var url = this.API_URL + "/suppliers/suppliers-details";
        return new Promise(function (resolve, reject) {
            _this.http.get(url)
                .subscribe(resolve, reject);
        });
    };
    // Vipin Code
    SupplierService.prototype.updateSupplierItems = function (value, id) {
        var _this = this;
        var url = this.API_URL + "/suppliers/suppliers-details/items/" + id;
        return new Promise(function (resolve, reject) {
            _this.http.put(url, value)
                .subscribe(resolve, reject);
        });
    };
    SupplierService.prototype.addSupplierItems = function (value, id) {
        var _this = this;
        var url = this.API_URL + "/suppliers/suppliers-details/add/" + id;
        return new Promise(function (resolve, reject) {
            _this.http.put(url, value)
                .subscribe(resolve, reject);
        });
    };
    // Vipin Code
    SupplierService.prototype.UpdateSupplierDetails = function (value, id) {
        var _this = this;
        var url = this.API_URL + "/suppliers/suppliers-details/" + id;
        return new Promise(function (resolve, reject) {
            _this.http.put(url, value)
                .subscribe(resolve, reject);
        });
    };
    SupplierService.prototype.getSupplierByID = function (id) {
        var _this = this;
        var url = this.API_URL + "/suppliers/suppliers-details/" + id;
        return new Promise(function (resolve, reject) {
            _this.http.get(url)
                .subscribe(resolve, reject);
        });
    };
    SupplierService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _settings_settings_services_service__WEBPACK_IMPORTED_MODULE_3__["SettingsServicesService"]])
    ], SupplierService);
    return SupplierService;
}());



/***/ })

}]);
//# sourceMappingURL=default~app-pages-pages-module~suppliers-suppliers-module~ui-features-ui-features-module.js.map