(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["app-pages-pages-module"],{

/***/ "./src/app/pages/e-commerce/e-commerce.component.html":
/*!************************************************************!*\
  !*** ./src/app/pages/e-commerce/e-commerce.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <div class=\"row\">\n  <div class=\"col-xxl-5\">\n    <div class=\"row\">\n      <div class=\"col-md-6\">\n        <ngx-profit-card></ngx-profit-card>\n      </div>\n      <div class=\"col-md-6\">\n        <ngx-earning-card></ngx-earning-card>\n      </div>\n    </div>\n\n    <ngx-traffic-reveal-card></ngx-traffic-reveal-card>\n  </div>\n\n  <div class=\"col-xxl-7\">\n    <ngx-ecommerce-charts></ngx-ecommerce-charts>\n  </div>\n</div> -->\n<router-outlet></router-outlet>\n<div class=\"row\">\n    <div class=\"col-md-3\">\n        <div class=\"stat-card \">\n            <a routerLink=\"/pages/customers\">\n                <!-- <div style=\"font-size: 70px;\"><i class=\"nb-person\"></i></div> -->\n                <div class=\"stat-card__stat\">\n                    <div class=\"myicons\"><i class=\"nb-person\"></i></div>\n                    {{LengthOfClient}}\n                </div>\n                <div class=\"stat-card__desc\">Clients</div>\n            </a>\n        </div>\n    </div>\n    <div class=\"col-md-3\">\n        <div class=\"stat-card stat-card--pink\">\n            <a routerLink=\"/pages/QuotationDashboard\">\n                <div class=\"stat-card__stat\">\n                    <div class=\"myicons\"><i class=\"nb-compose\"></i></div>\n                    {{lengthsOfQuotation}}\n                </div>\n                <div class=\"stat-card__desc\">Quotations</div>\n            </a>\n        </div>\n    </div>\n    <div class=\"col-md-3\">\n        <div class=\"stat-card stat-card--blue\">\n            <a routerLink=\"/pages/Jobs_Dashboard\">\n                <div class=\"stat-card__stat\">\n                    <div class=\"myicons\"><i class=\"nb-gear\"></i></div>\n                    {{lengthsOfJobs}}\n                </div>\n                <div class=\"stat-card__desc\">Agreements</div>\n            </a>\n        </div>\n    </div>\n    <div class=\"col-md-3\">\n        <div class=\"stat-card stat-card--green \">\n            <a routerLink=\"/pages/Main_AMC\">\n                <div class=\"stat-card__stat\">\n                    <div class=\"myicons\"><i class=\"nb-layout-sidebar-right\"></i></div>\n                    {{LengthOfAMC}}\n                </div>\n                <div class=\"stat-card__desc\">AMC</div>\n            </a>\n        </div>\n    </div>\n</div>\n\n\n\n<div class=\"row\">\n    <div class=\"col-xxl-6\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <nb-card>\n\n                    <nb-card-header>\n                        Agreements List\n                    </nb-card-header>\n                    <nb-card-body>\n                        <ng2-smart-table [settings]=\"settingsJobs\" [source]=\"sourcejobs\" (userRowSelect)=\"onUserRowSelects($event)\">\n                        </ng2-smart-table>\n                    </nb-card-body>\n                </nb-card>\n            </div>\n        </div>\n    </div>\n    <div class=\"col-xxl-6\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n\n                <nb-card>\n                    <nb-card-header>\n                        Quotation List\n                        <!-- <button class=\"btn btn-outline-primary \" shape=\"semi-round\" (click)=\"showFun('create')\" mdbWavesEffect >Create New Quotation</button> -->\n                        <!-- <br> <br> -->\n\n                        <!-- <button class=\"btn btn-outline-primary \" shape=\"semi-round\"  [routerLink]=\"['/pages/MainQuotation']\" style=\"margin-right: 0px;\">Clear</button> -->\n\n                    </nb-card-header>\n                    <nb-card-body>\n\n\n                        <ng2-smart-table [settings]=\"settings\" [source]=\"source\" (userRowSelect)=\"onUserRowSelect($event)\">\n                        </ng2-smart-table>\n\n                    </nb-card-body>\n                </nb-card>\n\n            </div>\n\n\n        </div>\n    </div>\n\n</div>"

/***/ }),

/***/ "./src/app/pages/e-commerce/e-commerce.component.scss":
/*!************************************************************!*\
  !*** ./src/app/pages/e-commerce/e-commerce.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "button {\n  float: right; }\n\na {\n  cursor: pointer; }\n\n@media (max-width: 600px) {\n  button {\n    float: none;\n    margin-top: 1px; } }\n\n:host /deep/ td.ng2-smart-actions a {\n  cursor: pointer;\n  color: #009ECE; }\n\n:host /deep/ ng2-smart-table thead > tr > th > select {\n  font-size: 0.875rem;\n  padding: 0.375rem 1.125rem; }\n\n:host /deep/ tr th .nb-theme-corporate select.form-control:not([size]):not([multiple]) {\n  height: calc(2rem + 4px) !important; }\n\n:host /deep/ tr, th .nb-theme-corporate .form-control {\n  font-size: 1rem !important;\n  font-size: 0.875rem !important;\n  padding: 0.375rem 1.125rem !important; }\n\nnb-card-body {\n  background-color: #E1F5FE; }\n\nnb-card-header {\n  background: #1D8ECE;\n  color: white; }\n\n:host /deep/ table tbody tr:nth-child(even) {\n  background: #e9eaea !important; }\n\n:host /deep/ table tbody tr:nth-child(odd) {\n  background: white !important; }\n\n/deep/ ng2-smart-table-pager nav ul {\n  background-color: black; }\n\n:host {\n  font-size: 1rem; }\n\n:host /deep/ * {\n    -webkit-box-sizing: border-box;\n            box-sizing: border-box; }\n\n:host /deep/ /deep/ .modal-dialog {\n    max-width: 22%; }\n\n:host /deep/ select {\n    height: 2.4rem !important;\n    padding-top: 5px !important; }\n\n.stat-card, .stat-card__sub-stat, .stat-card__stat, .stat-card__desc {\n  -webkit-transition: all .5s ease;\n  transition: all .5s ease; }\n\n.myicons {\n  float: left;\n  font-size: 81px; }\n\n.stat-card {\n  width: 100%;\n  border-radius: 3px;\n  height: 135px;\n  padding: 0 15px 0 15px;\n  margin: 15px;\n  background-size: 100% 200%;\n  background-image: -webkit-gradient(linear, left top, left bottom, from(#e4dede), color-stop(50%, #9B9B9B));\n  background-image: linear-gradient(to bottom, #e4dede 0%, #9B9B9B 50%);\n  border-top: 5px solid #9B9B9B;\n  display: inline-block;\n  -webkit-box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);\n          box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);\n  vertical-align: top; }\n\n.stat-card:hover {\n    background-position: 0 -100%;\n    cursor: pointer; }\n\n.stat-card:hover .stat-card__stat, .stat-card:hover .stat-card__desc, .stat-card:hover .stat-card__sub-stat {\n      color: white; }\n\n.stat-card__stat, .stat-card__desc {\n    text-align: center;\n    font-weight: 500; }\n\n.stat-card__sub-stat {\n    display: inline;\n    font-size: 26px;\n    color: #F5437B;\n    margin-left: -5px; }\n\n.stat-card__stat {\n    color: #9B9B9B;\n    font-size: 40px;\n    margin: 5px 0; }\n\n.stat-card__desc {\n    color: #4D5365;\n    font-size: 17px; }\n\n.stat-card--pink {\n    border-top: 5px solid #F5437B;\n    background-image: -webkit-gradient(linear, left top, left bottom, from(white), color-stop(50%, #F5437B));\n    background-image: linear-gradient(to bottom, white 0%, #F5437B 50%); }\n\n.stat-card--pink .stat-card__stat {\n      color: #F5437B; }\n\n.stat-card--blue {\n    border-top: 5px solid #0072BB;\n    background-image: -webkit-gradient(linear, left top, left bottom, from(white), color-stop(50%, #0072BB));\n    background-image: linear-gradient(to bottom, white 0%, #0072BB 50%); }\n\n.stat-card--blue .stat-card__stat {\n      color: #0072BB; }\n\n.stat-card--green {\n    border-top: 5px solid #67C97C;\n    background-image: -webkit-gradient(linear, left top, left bottom, from(white), color-stop(50%, #67C97C));\n    background-image: linear-gradient(to bottom, white 0%, #67C97C 50%); }\n\n.stat-card--green .stat-card__stat {\n      color: #67C97C; }\n\n.stat-card--yellow {\n    border-top: 5px solid #FFA630;\n    background-image: -webkit-gradient(linear, left top, left bottom, from(white), color-stop(50%, #FFA630));\n    background-image: linear-gradient(to bottom, white 0%, #FFA630 50%); }\n\n.stat-card--yellow .stat-card__stat {\n      color: #FFA630; }\n\n.stat-card--red {\n    border-top: 5px solid #D0021B;\n    background-image: -webkit-gradient(linear, left top, left bottom, from(white), color-stop(50%, #D0021B));\n    background-image: linear-gradient(to bottom, white 0%, #D0021B 50%); }\n\n.stat-card--red .stat-card__stat {\n      color: #D0021B; }\n\n.stat-card--light-blue {\n    border-top: 5px solid #3C91E6;\n    background-image: -webkit-gradient(linear, left top, left bottom, from(white), color-stop(50%, #3C91E6));\n    background-image: linear-gradient(to bottom, white 0%, #3C91E6 50%); }\n\n.stat-card--light-blue .stat-card__stat {\n      color: #3C91E6; }\n\n.stat-card--light-default {\n    border-top: 5px solid #e4dede;\n    background-image: -webkit-gradient(linear, left top, left bottom, from(white), color-stop(50%, #e4dede));\n    background-image: linear-gradient(to bottom, white 0%, #e4dede 50%); }\n\n.stat-card--light-default .stat-card__stat {\n      color: #e4dede; }\n\n.stat-card--light-pink {\n    border-top: 5px solid #fb457e57;\n    background-image: -webkit-gradient(linear, left top, left bottom, from(white), color-stop(50%, #fb457e57));\n    background-image: linear-gradient(to bottom, white 0%, #fb457e57 50%); }\n\n.stat-card--light-pink .stat-card__stat {\n      color: #fb457e57; }\n\n.stat-card--light-blue-light {\n    border-top: 5px solid #0072bb57;\n    background-image: -webkit-gradient(linear, left top, left bottom, from(white), color-stop(50%, #0072bb57));\n    background-image: linear-gradient(to bottom, white 0%, #0072bb57 50%); }\n\n.stat-card--light-blue-light .stat-card__stat {\n      color: #0072bb57; }\n\n.stat-card--light-green {\n    border-top: 5px solid #67c97c85;\n    background-image: -webkit-gradient(linear, left top, left bottom, from(white), color-stop(50%, #67c97c85));\n    background-image: linear-gradient(to bottom, white 0%, #67c97c85 50%); }\n\n.stat-card--light-green .stat-card__stat {\n      color: #67c97c85; }\n\n@media (max-width: 800px) {\n  .stat-card__stat {\n    color: #9B9B9B;\n    font-size: 36px;\n    margin: 0px 0; } }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3ByYWthc2gvVmlwaW4vaXJvbmJpcmQvMjBGRUIyMDIwL2lyb25iaXJkLWNsaWVudHMvc3JjL2FwcC9wYWdlcy9lLWNvbW1lcmNlL2UtY29tbWVyY2UuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxZQUFZLEVBQUE7O0FBRWhCO0VBQ0UsZUFBZSxFQUFBOztBQUVqQjtFQU5BO0lBUVEsV0FBVztJQUNYLGVBQWUsRUFBQSxFQUNsQjs7QUFTTDtFQUFxQyxlQUFlO0VBQUMsY0FBYyxFQUFBOztBQUVuRTtFQUNJLG1CQUFtQjtFQUNuQiwwQkFBMEIsRUFBQTs7QUFHOUI7RUFDSSxtQ0FBbUMsRUFBQTs7QUFRdkM7RUFDSSwwQkFBMEI7RUFDMUIsOEJBQThCO0VBQzlCLHFDQUFxQyxFQUFBOztBQUV6QztFQUNJLHlCQUF3QixFQUFBOztBQUcxQjtFQUNFLG1CQUFtQjtFQUNuQixZQUFZLEVBQUE7O0FBRWQ7RUFDRSw4QkFBNEIsRUFBQTs7QUFFNUI7RUFDQSw0QkFBNEIsRUFBQTs7QUFFNUI7RUFDSSx1QkFBdUIsRUFBQTs7QUFFekI7RUFDRSxlQUFlLEVBQUE7O0FBRGpCO0lBS00sOEJBQXNCO1lBQXRCLHNCQUFzQixFQUFBOztBQUw1QjtJQVE0QixjQUFnQixFQUFBOztBQVI1QztJQVVNLHlCQUF5QjtJQUN6QiwyQkFBMkIsRUFBQTs7QUF1QnZDO0VBQ0ksZ0NBQXdCO0VBQXhCLHdCQUF3QixFQUFBOztBQUU1QjtFQUNFLFdBQVc7RUFDWCxlQUFlLEVBQUE7O0FBYWpCO0VBRUksV0FBVztFQUNYLGtCQUFrQjtFQUNsQixhQUFhO0VBQ2Isc0JBQXNCO0VBQ3RCLFlBQVk7RUFDWiwwQkFBMEI7RUFDMUIsMEdBQXNGO0VBQXRGLHFFQUFzRjtFQUN0Riw2QkEzQzZCO0VBNEM3QixxQkFBcUI7RUFDckIscUZBL0JrRTtVQStCbEUsNkVBL0JrRTtFQWdDbEUsbUJBQW1CLEVBQUE7O0FBWnZCO0lBZVEsNEJBQTRCO0lBQzVCLGVBQWUsRUFBQTs7QUFoQnZCO01BbUJZLFlBQVksRUFBQTs7QUFJcEI7SUFDSSxrQkFBa0I7SUFDbEIsZ0JBQWdCLEVBQUE7O0FBR3BCO0lBRUksZUFBZTtJQUNmLGVBQWU7SUFDZixjQWpFc0I7SUFrRXRCLGlCQUFpQixFQUFBOztBQUdyQjtJQUVJLGNBeEV5QjtJQXlFekIsZUFBZTtJQUNmLGFBQWEsRUFBQTs7QUFHakI7SUFFSSxjQXhFc0I7SUF5RXRCLGVBQWUsRUFBQTs7QUF4RG5CO0lBQ0ksNkJBeEJzQjtJQXlCdEIsd0dBQW1FO0lBQW5FLG1FQUFtRSxFQUFBOztBQUZ0RTtNQUlPLGNBM0JrQixFQUFBOztBQXVCMUI7SUFDSSw2QkF2QnNCO0lBd0J0Qix3R0FBbUU7SUFBbkUsbUVBQW1FLEVBQUE7O0FBRnRFO01BSU8sY0ExQmtCLEVBQUE7O0FBc0IxQjtJQUNJLDZCQXJCdUI7SUFzQnZCLHdHQUFtRTtJQUFuRSxtRUFBbUUsRUFBQTs7QUFGdEU7TUFJTyxjQXhCbUIsRUFBQTs7QUFvQjNCO0lBQ0ksNkJBcEJ3QjtJQXFCeEIsd0dBQW1FO0lBQW5FLG1FQUFtRSxFQUFBOztBQUZ0RTtNQUlPLGNBdkJvQixFQUFBOztBQW1CNUI7SUFDSSw2QkFuQnFCO0lBb0JyQix3R0FBbUU7SUFBbkUsbUVBQW1FLEVBQUE7O0FBRnRFO01BSU8sY0F0QmlCLEVBQUE7O0FBa0J6QjtJQUNJLDZCQXRCMkI7SUF1QjNCLHdHQUFtRTtJQUFuRSxtRUFBbUUsRUFBQTs7QUFGdEU7TUFJTyxjQXpCdUIsRUFBQTs7QUFxQi9CO0lBQ0ksNkJBakI4QjtJQWtCOUIsd0dBQW1FO0lBQW5FLG1FQUFtRSxFQUFBOztBQUZ0RTtNQUlPLGNBcEIwQixFQUFBOztBQWdCbEM7SUFDSSwrQkFoQjZCO0lBaUI3QiwwR0FBbUU7SUFBbkUscUVBQW1FLEVBQUE7O0FBRnRFO01BSU8sZ0JBbkJ5QixFQUFBOztBQWVqQztJQUNJLCtCQWY2QjtJQWdCN0IsMEdBQW1FO0lBQW5FLHFFQUFtRSxFQUFBOztBQUZ0RTtNQUlPLGdCQWxCeUIsRUFBQTs7QUFjakM7SUFDSSwrQkFkOEI7SUFlOUIsMEdBQW1FO0lBQW5FLHFFQUFtRSxFQUFBOztBQUZ0RTtNQUlPLGdCQWpCMEIsRUFBQTs7QUFrR3RDO0VBdkNJO0lBeUNJLGNBQWM7SUFDZCxlQUFlO0lBQ2YsYUFBYSxFQUFBLEVBQ2hCIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvZS1jb21tZXJjZS9lLWNvbW1lcmNlLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiYnV0dG9ue1xuICAgIGZsb2F0OiByaWdodDtcbn1cbmF7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cbkBtZWRpYSAobWF4LXdpZHRoOjYwMHB4KXtcbiAgICBidXR0b257XG4gICAgICAgIGZsb2F0OiBub25lO1xuICAgICAgICBtYXJnaW4tdG9wOiAxcHg7XG4gICAgfVxufVxuLy8gOmhvc3QgL2RlZXAvIG5nMi1zbWFydC10YWJsZS10aXRsZSBhOmZvY3VzIHsgY29sb3I6IHJnYigxNTgsIDIwNiwgMCkgIWltcG9ydGFudDsgfVxuXG4vLyA6aG9zdCAvZGVlcC8gbmcyLXNtYXJ0LXRhYmxlIHRoZWFkID4gdHIgPiB0aCA+IGRpdiB7IGNvbG9yOiByZ2IoNjIsIDAsIDIwNik7IH1cbi8vIDpob3N0IC9kZWVwL25nMi1zbWFydC10YWJsZSB0YWJsZXtcbi8vICAgICB3b3JkLWJyZWFrOiBicmVhay13b3JkO1xuLy8gfVxuXG46aG9zdCAvZGVlcC8gdGQubmcyLXNtYXJ0LWFjdGlvbnMgYSB7Y3Vyc29yOiBwb2ludGVyO2NvbG9yOiAjMDA5RUNFOyB9XG5cbjpob3N0IC9kZWVwLyBuZzItc21hcnQtdGFibGUgdGhlYWQgPnRyID4gdGggPiBzZWxlY3QgeyAgICBcbiAgICBmb250LXNpemU6IDAuODc1cmVtO1xuICAgIHBhZGRpbmc6IDAuMzc1cmVtIDEuMTI1cmVtOyBcbn1cblxuOmhvc3QgL2RlZXAvIHRyIHRoIC5uYi10aGVtZS1jb3Jwb3JhdGUgc2VsZWN0LmZvcm0tY29udHJvbDpub3QoW3NpemVdKTpub3QoW211bHRpcGxlXSkge1xuICAgIGhlaWdodDogY2FsYygycmVtICsgNHB4KSAhaW1wb3J0YW50O1xufVxuXG5cbi8vIHNlbGVjdC5mb3JtLWNvbnRyb2w6bm90KFtzaXplXSk6bm90KFttdWx0aXBsZV0pIHtcbi8vICAgICBoZWlnaHQ6IGNhbGMoMi4yNXJlbSArIDJweCk7XG4vLyB9XG5cbjpob3N0IC9kZWVwLyB0cix0aCAubmItdGhlbWUtY29ycG9yYXRlIC5mb3JtLWNvbnRyb2wge1xuICAgIGZvbnQtc2l6ZTogMXJlbSAhaW1wb3J0YW50O1xuICAgIGZvbnQtc2l6ZTogMC44NzVyZW0gIWltcG9ydGFudDtcbiAgICBwYWRkaW5nOiAwLjM3NXJlbSAxLjEyNXJlbSAhaW1wb3J0YW50O1xufVxubmItY2FyZC1ib2R5e1xuICAgIGJhY2tncm91bmQtY29sb3I6I0UxRjVGRTtcbiAgICAvLyBjb2xvcjp3aGl0ZTtcbiAgfVxuICBuYi1jYXJkLWhlYWRlcntcbiAgICBiYWNrZ3JvdW5kOiAjMUQ4RUNFO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgfVxuICA6aG9zdCAvZGVlcC8gdGFibGUgdGJvZHkgdHI6bnRoLWNoaWxkKGV2ZW4pIHtcbiAgICBiYWNrZ3JvdW5kOiNlOWVhZWEhaW1wb3J0YW50O1xuICAgIH1cbiAgICA6aG9zdCAvZGVlcC8gdGFibGUgdGJvZHkgdHI6bnRoLWNoaWxkKG9kZCkge1xuICAgIGJhY2tncm91bmQ6ICB3aGl0ZSFpbXBvcnRhbnQ7XG4gICAgfVxuICAgIC9kZWVwLyBuZzItc21hcnQtdGFibGUtcGFnZXIgbmF2IHVse1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcbiAgICAgIH1cbiAgICAgIDpob3N0IHtcbiAgICAgICAgZm9udC1zaXplOiAxcmVtO1xuICAgICAgXG4gICAgICAgIC9kZWVwLyB7XG4gICAgICAgICAgKiB7XG4gICAgICAgICAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAgICAgICAgIH1cbiAgICAgIFxuICAgICAgICAgIC9kZWVwLyAubW9kYWwtZGlhbG9nIHsgIG1heC13aWR0aDogMjIlICB9XG4gICAgICAgICAgc2VsZWN0IHtcbiAgICAgICAgICAgIGhlaWdodDogMi40cmVtICFpbXBvcnRhbnQ7XG4gICAgICAgICAgICBwYWRkaW5nLXRvcDogNXB4ICFpbXBvcnRhbnQ7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgICRibG9jay1zdGF0LWNhcmQ6IFwic3RhdC1jYXJkXCI7XG4vL1N0YXQgY2FyZFxuJGNvbG9yLXN0YXQtY2FyZC1kZWZhdWx0OiAjOUI5QjlCO1xuJGNvbG9yLXN0YXQtY2FyZC1waW5rOiAjRjU0MzdCO1xuJGNvbG9yLXN0YXQtY2FyZC1ibHVlOiAjMDA3MkJCO1xuJGNvbG9yLXN0YXQtY2FyZC1saWdodGJsdWU6ICMzQzkxRTY7XG4kY29sb3Itc3RhdC1jYXJkLWdyZWVuOiAjNjdDOTdDO1xuJGNvbG9yLXN0YXQtY2FyZC15ZWxsb3c6ICNGRkE2MzA7XG4kY29sb3Itc3RhdC1jYXJkLXJlZDogI0QwMDIxQjtcbiRjb2xvci1zdGF0LWNhcmQtZGVzYzogIzRENTM2NTtcbiRjb2xvci1zdGF0LWNhcmQtZGVmYXVsdC1saWdodDojZTRkZWRlO1xuJGNvbG9yLXN0YXQtY2FyZC1waW5rLWxpZ2h0OiNmYjQ1N2U1NztcbiRjb2xvci1zdGF0LWNhcmQtYmx1ZS1saWdodDojMDA3MmJiNTc7XG4kY29sb3Itc3RhdC1jYXJkLWdyZWVuLWxpZ2h0OiM2N2M5N2M4NTtcblxuXG4kYm94LXNoYWRvdzogMCAycHggNXB4IDAgcmdiYSgwLDAsMCwuMTYpLCAwIDJweCAxMHB4IDAgcmdiYSgwLDAsMCwuMTIpO1xuXG4ldHJhbnNpdGlvbiB7XG4gICAgdHJhbnNpdGlvbjogYWxsIC41cyBlYXNlO1xufVxuLm15aWNvbnN7XG4gIGZsb2F0OiBsZWZ0O1xuICBmb250LXNpemU6IDgxcHg7XG59XG5AbWl4aW4gc3RhdC1jb2xvciAoJG5hbWUsJGNvbG9yKSB7XG4gICAgJi0tI3skbmFtZX0ge1xuICAgICAgICBib3JkZXItdG9wOiA1cHggc29saWQgJGNvbG9yO1xuICAgICAgICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQodG8gYm90dG9tLCB3aGl0ZSAgMCUsICRjb2xvciA1MCUpO1xuICAgICAgICAuI3skYmxvY2stc3RhdC1jYXJkfV9fc3RhdCB7XG4gICAgICAgICAgICBjb2xvcjogJGNvbG9yO1xuICAgICAgICB9XG4gICAgICAgIFxuICAgIH1cbn1cblxuLiN7JGJsb2NrLXN0YXQtY2FyZH0ge1xuICAgIEBleHRlbmQgJXRyYW5zaXRpb247XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgYm9yZGVyLXJhZGl1czogM3B4O1xuICAgIGhlaWdodDogMTM1cHg7XG4gICAgcGFkZGluZzogMCAxNXB4IDAgMTVweDtcbiAgICBtYXJnaW46IDE1cHg7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDIwMCU7XG4gICAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KHRvIGJvdHRvbSwgI2U0ZGVkZSAwJSwgJGNvbG9yLXN0YXQtY2FyZC1kZWZhdWx0IDUwJSk7XG4gICAgYm9yZGVyLXRvcDogNXB4IHNvbGlkICRjb2xvci1zdGF0LWNhcmQtZGVmYXVsdDtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgYm94LXNoYWRvdzogJGJveC1zaGFkb3c7XG4gICAgdmVydGljYWwtYWxpZ246IHRvcDtcblxuICAgICY6aG92ZXIge1xuICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiAwIC0xMDAlO1xuICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XG5cbiAgICAgICAgLiN7JGJsb2NrLXN0YXQtY2FyZH1fX3N0YXQsIC4jeyRibG9jay1zdGF0LWNhcmR9X19kZXNjLCAuI3skYmxvY2stc3RhdC1jYXJkfV9fc3ViLXN0YXQge1xuICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xuICAgICAgICB9XG4gICAgfVxuICAgIC8vRWxlbWVudHNcbiAgICAmX19zdGF0LCAmX19kZXNjIHtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICBmb250LXdlaWdodDogNTAwO1xuICAgIH1cblxuICAgICZfX3N1Yi1zdGF0IHtcbiAgICAgICAgQGV4dGVuZCAldHJhbnNpdGlvbjtcbiAgICAgICAgZGlzcGxheTogaW5saW5lO1xuICAgICAgICBmb250LXNpemU6IDI2cHg7XG4gICAgICAgIGNvbG9yOiAkY29sb3Itc3RhdC1jYXJkLXBpbms7XG4gICAgICAgIG1hcmdpbi1sZWZ0OiAtNXB4O1xuICAgIH1cblxuICAgICZfX3N0YXQge1xuICAgICAgICBAZXh0ZW5kICV0cmFuc2l0aW9uO1xuICAgICAgICBjb2xvcjogJGNvbG9yLXN0YXQtY2FyZC1kZWZhdWx0O1xuICAgICAgICBmb250LXNpemU6IDQwcHg7XG4gICAgICAgIG1hcmdpbjogNXB4IDA7XG4gICAgfVxuXG4gICAgJl9fZGVzYyB7XG4gICAgICAgIEBleHRlbmQgJXRyYW5zaXRpb247XG4gICAgICAgIGNvbG9yOiAkY29sb3Itc3RhdC1jYXJkLWRlc2M7XG4gICAgICAgIGZvbnQtc2l6ZTogMTdweDtcbiAgICB9XG4gICAgLy9Nb2RpZmllcnNcbiAgICBAaW5jbHVkZSBzdGF0LWNvbG9yKCdwaW5rJywkY29sb3Itc3RhdC1jYXJkLXBpbmspO1xuICAgIEBpbmNsdWRlIHN0YXQtY29sb3IoJ2JsdWUnLCRjb2xvci1zdGF0LWNhcmQtYmx1ZSk7XG4gICAgQGluY2x1ZGUgc3RhdC1jb2xvcignZ3JlZW4nLCRjb2xvci1zdGF0LWNhcmQtZ3JlZW4pO1xuICAgIEBpbmNsdWRlIHN0YXQtY29sb3IoJ3llbGxvdycsJGNvbG9yLXN0YXQtY2FyZC15ZWxsb3cpO1xuICAgIEBpbmNsdWRlIHN0YXQtY29sb3IoJ3JlZCcsJGNvbG9yLXN0YXQtY2FyZC1yZWQpO1xuICAgIEBpbmNsdWRlIHN0YXQtY29sb3IoJ2xpZ2h0LWJsdWUnLCRjb2xvci1zdGF0LWNhcmQtbGlnaHRibHVlKTtcbiAgICBAaW5jbHVkZSBzdGF0LWNvbG9yKCdsaWdodC1kZWZhdWx0JywkY29sb3Itc3RhdC1jYXJkLWRlZmF1bHQtbGlnaHQpO1xuICAgIEBpbmNsdWRlIHN0YXQtY29sb3IoJ2xpZ2h0LXBpbmsnLCRjb2xvci1zdGF0LWNhcmQtcGluay1saWdodCk7XG4gICAgQGluY2x1ZGUgc3RhdC1jb2xvcignbGlnaHQtYmx1ZS1saWdodCcsJGNvbG9yLXN0YXQtY2FyZC1ibHVlLWxpZ2h0KTtcbiAgICBAaW5jbHVkZSBzdGF0LWNvbG9yKCdsaWdodC1ncmVlbicsJGNvbG9yLXN0YXQtY2FyZC1ncmVlbi1saWdodCk7XG5cblxuICAgIC8vICYtLXNob3J0IHtcbiAgICAvLyAgICAgaGVpZ2h0OiA5MHB4O1xuICAgIC8vICAgICBwYWRkaW5nOiAwIDVweCAwIDVweDtcblxuICAgIC8vICAgICAuI3skYmxvY2stc3RhdC1jYXJkfV9fc3RhdCB7XG4gICAgLy8gICAgICAgICBtYXJnaW4tYm90dG9tOiAtMTBweDtcbiAgICAvLyAgICAgICAgIG1hcmdpbi10b3A6IDA7XG4gICAgLy8gICAgIH1cbiAgICAvLyB9XG59XG5cblxuXG5cbkBtZWRpYSAobWF4LXdpZHRoOjgwMHB4KXtcbiAgICAuc3RhdC1jYXJkX19zdGF0e1xuICAgICAgICBjb2xvcjogIzlCOUI5QjtcbiAgICAgICAgZm9udC1zaXplOiAzNnB4O1xuICAgICAgICBtYXJnaW46IDBweCAwO1xuICAgIH1cbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/e-commerce/e-commerce.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/pages/e-commerce/e-commerce.component.ts ***!
  \**********************************************************/
/*! exports provided: ECommerceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ECommerceComponent", function() { return ECommerceComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var _services_AMC_amc_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/AMC/amc.service */ "./src/app/services/AMC/amc.service.ts");
/* harmony import */ var _services_customer_customer_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/customer/customer.service */ "./src/app/services/customer/customer.service.ts");
/* harmony import */ var _services_jobs_total_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/jobs/total.service */ "./src/app/services/jobs/total.service.ts");
/* harmony import */ var _services_admin_quotation_quotation_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/admin/quotation/quotation.service */ "./src/app/services/admin/quotation/quotation.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};







var ECommerceComponent = /** @class */ (function () {
    function ECommerceComponent(route, router, QuotationServ, JobsServ, CustomerServ, AmcServ) {
        this.route = route;
        this.router = router;
        this.QuotationServ = QuotationServ;
        this.JobsServ = JobsServ;
        this.CustomerServ = CustomerServ;
        this.AmcServ = AmcServ;
        this.listOfJobs = [];
        this.settings = {
            pager: {
                display: true,
                perPage: 6
            },
            add: {
                addButtonContent: '<i class="nb-plus"></i>',
                createButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
            },
            edit: {
                editButtonContent: '<i class="nb-edit"></i>',
                saveButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
            },
            delete: {
                deleteButtonContent: '<i class="nb-trash"></i>',
                confirmDelete: true,
            },
            actions: {
                edit: false,
                add: false,
                delete: false,
            },
            columns: {
                quoteNo: {
                    title: 'Quotation No',
                    type: 'string',
                },
                companyName: {
                    title: 'Client Name',
                    type: 'html',
                },
                siteName: {
                    title: 'Site Name',
                    type: 'string',
                },
                statuses: {
                    title: 'Status',
                    type: 'string',
                    filter: {
                        type: 'list',
                        config: {
                            selectText: 'All',
                            list: [
                                { value: 'Created & Not Sent', title: 'Created & Not Sent' },
                                { value: 'Sent & Waiting For Approval', title: 'Sent & Waiting For Approval' },
                                { value: 'Approved.', title: 'Approved.' },
                                { value: 'Approved & Job Created', title: 'Approved & Job Created' },
                                { value: 'Rejected', title: 'Rejected' },
                                { value: 'Preparatory Work Changed', title: 'Preparatory Work Changed' },
                            ],
                        },
                    },
                }
            },
        };
        this.settingsJobs = {
            add: {
                addButtonContent: '<i class="nb-plus"></i>',
                createButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
            },
            edit: {
                editButtonContent: '<i class="nb-edit"></i>',
                saveButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
            },
            delete: {
                deleteButtonContent: '<i class="nb-trash"></i>',
                confirmDelete: true,
            },
            pager: {
                display: true,
                perPage: 6
            },
            actions: {
                edit: false,
                add: false,
                delete: false,
            },
            columns: {
                agreementId: {
                    title: 'Agreement No',
                    type: 'number',
                },
                agreementdate: {
                    title: ' Agreement Date',
                    type: 'string',
                    valuePrepareFunction: function (value) {
                        return value.split('-').reverse().join('/');
                    },
                },
                customerName: {
                    title: 'Client Name',
                    type: 'string',
                },
                JobsList: {
                    title: 'No.of Jobs Created',
                    type: 'string'
                }
            },
        };
        this.source = new ng2_smart_table__WEBPACK_IMPORTED_MODULE_2__["LocalDataSource"]();
        this.sourcejobs = new ng2_smart_table__WEBPACK_IMPORTED_MODULE_2__["LocalDataSource"]();
        // this.getJobsInfo();
        // this.getQuotationDetails();
        // this.getAMCInfo();
        // this.getClients();
    }
    /**
     * user row selected started from here
     */
    /**
     *  it is for quotations
     */
    ECommerceComponent.prototype.onUserRowSelect = function (event) {
        var id = event.data.id;
        if (this.router.url === "/pages/dashboard") {
            this.router.navigate(["/pages/MainQuotation/" + id]); //for the case 'the user logout I want him to be redirected to home.'
        }
        else {
            // Stay here or do something
            // for the case 'However if he is on a safe page I don't want to redirected him.'
        }
    };
    /**
     *  it is for agreements
     */
    ECommerceComponent.prototype.onUserRowSelects = function (event) {
        var id = event.data.id;
        if (this.router.url === "/pages/dashboard") {
            this.router.navigate(["/pages/Main_Jobs/" + id]);
            return;
        }
    };
    ECommerceComponent.prototype.getQuotationDetails = function () {
        return __awaiter(this, void 0, void 0, function () {
            var quotaionId, _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        quotaionId = this.route.snapshot.paramMap.get('id');
                        _a = this;
                        return [4 /*yield*/, this.QuotationServ.getQuotation(quotaionId)];
                    case 1:
                        _a.finalData = _b.sent();
                        this.lengthsOfQuotation = this.finalData.length;
                        this.finalData.forEach(function (invAmount) {
                            invAmount.totalAmount = 0;
                            return invAmount.invoiceItems.map(function (data) {
                                return invAmount.totalAmount += data.totalAmount;
                            });
                        });
                        // sort by name
                        // console.log(this.finalData)
                        this.finalData.sort(function (a, b) {
                            var nameA = a.quoteNum; // ignore upper and lowercase
                            var nameB = b.quoteNum; // ignore upper and lowercase
                            if (nameA < nameB) {
                                return -1;
                            }
                            if (nameA > nameB) {
                                return 1;
                            }
                            return 0;
                        });
                        this.source.load(this.finalData);
                        this.getAMCInfo();
                        return [2 /*return*/];
                }
            });
        });
    };
    ECommerceComponent.prototype.getJobsInfo = function () {
        return __awaiter(this, void 0, void 0, function () {
            var jobId, _a, error_1;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        jobId = this.route.snapshot.paramMap.get('id');
                        console.log(jobId);
                        _a = this;
                        return [4 /*yield*/, this.JobsServ.getJobs(jobId)];
                    case 1:
                        _a.listOfJobs = _b.sent();
                        this.lengthsOfJobs = this.listOfJobs.length;
                        this.listOfJobs.sort(function (a, b) {
                            var nameA = a.customerName.toUpperCase(); // ignore upper and lowercase
                            var nameB = b.customerName.toUpperCase(); // ignore upper and lowercase
                            if (nameA < nameB) {
                                return -1;
                            }
                            if (nameA > nameB) {
                                return 1;
                            }
                            return 0;
                        });
                        this.sourcejobs.load(this.listOfJobs);
                        this.getQuotationDetails();
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _b.sent();
                        console.error(error_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ECommerceComponent.prototype.getAMCInfo = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.AmcServ.getAllAmc()];
                    case 1:
                        _a.Amcs = _b.sent();
                        this.LengthOfAMC = this.Amcs.length;
                        this.getClients();
                        return [2 /*return*/];
                }
            });
        });
    };
    ECommerceComponent.prototype.getClients = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.CustomerServ.getCustomer()];
                    case 1:
                        _a.clientsDetails = _b.sent();
                        this.LengthOfClient = this.clientsDetails.length;
                        return [2 /*return*/];
                }
            });
        });
    };
    ECommerceComponent.prototype.ngOnInit = function () {
        this.getJobsInfo();
    };
    ECommerceComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-ecommerce',
            template: __webpack_require__(/*! ./e-commerce.component.html */ "./src/app/pages/e-commerce/e-commerce.component.html"),
            styles: [__webpack_require__(/*! ./e-commerce.component.scss */ "./src/app/pages/e-commerce/e-commerce.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _services_admin_quotation_quotation_service__WEBPACK_IMPORTED_MODULE_6__["QuotationService"],
            _services_jobs_total_service__WEBPACK_IMPORTED_MODULE_5__["TotalJobsService"],
            _services_customer_customer_service__WEBPACK_IMPORTED_MODULE_4__["CustomerService"],
            _services_AMC_amc_service__WEBPACK_IMPORTED_MODULE_3__["AmcService"]])
    ], ECommerceComponent);
    return ECommerceComponent;
}());



/***/ }),

/***/ "./src/app/pages/e-commerce/e-commerce.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/e-commerce/e-commerce.module.ts ***!
  \*******************************************************/
/*! exports provided: ECommerceModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ECommerceModule", function() { return ECommerceModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ui_features_ui_features_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../ui-features/ui-features.module */ "./src/app/pages/ui-features/ui-features.module.ts");
/* harmony import */ var _theme_theme_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../@theme/theme.module */ "./src/app/@theme/theme.module.ts");
/* harmony import */ var _e_commerce_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./e-commerce.component */ "./src/app/pages/e-commerce/e-commerce.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ECommerceModule = /** @class */ (function () {
    function ECommerceModule() {
    }
    ECommerceModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _theme_theme_module__WEBPACK_IMPORTED_MODULE_2__["ThemeModule"],
                _ui_features_ui_features_module__WEBPACK_IMPORTED_MODULE_1__["UiFeaturesModule"]
            ],
            declarations: [
                _e_commerce_component__WEBPACK_IMPORTED_MODULE_3__["ECommerceComponent"],
            ],
            providers: [],
        })
    ], ECommerceModule);
    return ECommerceModule;
}());



/***/ }),

/***/ "./src/app/pages/pages-routing.module.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/pages-routing.module.ts ***!
  \***********************************************/
/*! exports provided: routes, PagesRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routes", function() { return routes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PagesRoutingModule", function() { return PagesRoutingModule; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _pages_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./pages.component */ "./src/app/pages/pages.component.ts");
/* harmony import */ var _e_commerce_e_commerce_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./e-commerce/e-commerce.component */ "./src/app/pages/e-commerce/e-commerce.component.ts");
/* harmony import */ var _miscellaneous_not_found_not_found_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./miscellaneous/not-found/not-found.component */ "./src/app/pages/miscellaneous/not-found/not-found.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var routes = [{
        path: '',
        component: _pages_component__WEBPACK_IMPORTED_MODULE_2__["PagesComponent"],
        children: [
            {
                path: 'dashboard',
                component: _e_commerce_e_commerce_component__WEBPACK_IMPORTED_MODULE_3__["ECommerceComponent"],
            },
            {
                path: '',
                loadChildren: './ui-features/ui-features.module#UiFeaturesModule',
            },
            {
                path: 'miscellaneous',
                loadChildren: './miscellaneous/miscellaneous.module#MiscellaneousModule',
            },
            {
                path: '',
                redirectTo: 'dashboard',
                pathMatch: 'full',
            },
            {
                path: '**',
                component: _miscellaneous_not_found_not_found_component__WEBPACK_IMPORTED_MODULE_4__["NotFoundComponent"],
            }
        ],
    }];
var PagesRoutingModule = /** @class */ (function () {
    function PagesRoutingModule() {
    }
    PagesRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]],
            entryComponents: [
                _e_commerce_e_commerce_component__WEBPACK_IMPORTED_MODULE_3__["ECommerceComponent"]
            ]
        })
    ], PagesRoutingModule);
    return PagesRoutingModule;
}());



/***/ }),

/***/ "./src/app/pages/pages.component.ts":
/*!******************************************!*\
  !*** ./src/app/pages/pages.component.ts ***!
  \******************************************/
/*! exports provided: PagesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PagesComponent", function() { return PagesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_token_token_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/token/token.service */ "./src/app/services/token/token.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PagesComponent = /** @class */ (function () {
    function PagesComponent(userServ) {
        this.userServ = userServ;
        this.getTokenVAlues();
    }
    PagesComponent.prototype.getTokenVAlues = function () {
        var tokenValue = this.userServ.getDecodedToken();
        var MENU_ITEMS = [
            {
                // title: 'VS-DashBoard',
                title: 'IB-DashBoard',
                icon: 'nb-home',
                link: '/pages/dashboard',
                home: true,
            },
            {
                title: 'MODULES',
                group: true,
            },
            {
                title: 'Client',
                icon: 'nb-person',
                link: '/pages/customers',
            },
            {
                title: 'Quotations',
                icon: 'nb-compose',
                link: '/pages/QuotationDashboard',
                children: [
                    {
                        title: 'Q-Dashboard',
                        link: '/pages/QuotationDashboard',
                    },
                    {
                        title: 'Quotation Details',
                        link: '/pages/MainQuotation'
                    },
                ],
            },
            {
                title: 'Agreement',
                icon: 'nb-gear',
                link: '/pages/Jobs_Dashboard',
                children: [
                    {
                        title: 'A-Dashboard',
                        link: '/pages/Jobs_Dashboard',
                    },
                    {
                        title: 'Agreement Details',
                        link: '/pages/Main_Jobs',
                    },
                    {
                        title: 'Job Details',
                        link: '/pages/JobsList',
                    },
                    {
                        title: 'Work Performance',
                        link: '/pages/workPerformance',
                    },
                ],
            },
            {
                title: 'AMC',
                icon: 'nb-layout-sidebar-right',
                link: '/pages/Main_AMC',
                children: [
                    {
                        title: 'AMC Details',
                        link: '/pages/Main_AMC',
                    },
                    {
                        title: 'AMC Renewal/Reminder',
                        link: '/pages/renewalAMC',
                    },
                ],
            },
            {
                title: 'Invoice',
                icon: 'nb-list',
                link: '/pages/Invoices',
            },
            {
                title: 'Suppliers',
                icon: 'nb-shuffle',
                link: '/pages/Suppliers',
            },
            {
                title: 'Inventory',
                icon: 'nb-bar-chart',
                link: '/pages/Inventory_Dashboard',
                children: [
                    {
                        title: 'I-Dashboard',
                        link: '/pages/Inventory_Dashboard',
                    },
                    {
                        title: 'Inventory Details',
                        link: '/pages/Main_Inventory',
                    },
                ],
            },
            {
                title: 'HR Payroll',
                icon: 'nb-loop-circled',
                link: '/pages/EmployeeDetails',
                children: [
                    {
                        title: 'Employee Details',
                        link: '/pages/EmployeeDetails',
                    },
                    {
                        title: 'Employee Status',
                        link: '/pages/EmployeeStatus',
                    },
                    {
                        title: 'Attendance',
                        link: '/pages/Attendence',
                    },
                ],
            },
            {
                title: 'Marketing',
                icon: 'nb-bar-chart',
                link: '/pages/Gallary',
            },
            {
                title: 'Admin',
                icon: 'nb-grid-a-outline',
                link: '/pages/admin/admin',
                children: [
                    {
                        title: 'Company Settings',
                        link: '/pages/admin/admin',
                    },
                    {
                        title: 'Elevator Types',
                        link: '/pages/admin/Elevator_Types',
                    },
                    {
                        title: 'Preparatory Works',
                        link: '/pages/admin/Preparatory',
                    },
                    {
                        title: 'Data Maintenance',
                        link: '/pages/admin/Maintances',
                    },
                    {
                        title: 'Terms & Conditions',
                        link: '/pages/admin/Terms_Coditions',
                    },
                    {
                        title: 'Materials',
                        link: '/pages/admin/Materials',
                    },
                    {
                        title: 'Employee',
                        link: '/pages/admin/AddEmployee',
                    },
                    {
                        title: 'Department List',
                        link: '/pages/admin/DepartmentsList',
                    },
                    {
                        title: 'User Management',
                        link: '/pages/admin/UserManagement',
                    },
                    {
                        title: 'AMC',
                        link: '/pages/admin/AMC',
                    },
                    {
                        title: 'Technical Data',
                        link: '/pages/admin/Technical-Data'
                    },
                ],
            },
            {
                title: 'Notification',
                icon: 'nb-notifications',
                link: '/pages/Notifications',
            },
            // {
            //   title: 'Accounting',
            //   icon: 'nb-bar-chart',
            //   link: '/pages/accounts',
            // },
            {
                title: 'Log Out',
                icon: 'nb-power',
                link: '/auth/logout',
            }
        ];
        var userBasedMenu = [];
        var child = [];
        if (tokenValue.rolesHideAndShow !== undefined) {
            MENU_ITEMS.forEach(function (ele) {
                // if (ele.title === 'VS-DashBoard' || ele.title === 'MODULES' || ele.title === 'Log Out') {
                if (ele.title === 'IB-DashBoard' || ele.title === 'MODULES' || ele.title === 'Log Out') {
                    userBasedMenu.push(ele);
                }
                tokenValue.rolesHideAndShow.modules.forEach(function (ele1) {
                    if (ele.title === ele1.name) {
                        child = [];
                        if (ele1.subMenu.length !== 0 && (ele.children.length !== 0 || ele.children === undefined)) {
                            ele.children.forEach(function (element) {
                                ele1.subMenu.forEach(function (element1) {
                                    // if (element.title === element1.name && (element1.create === true || element1.edit === true || element1.view === true)) {
                                    if (element.title === element1.name && element1.view === true) {
                                        child.push(element);
                                    }
                                });
                            });
                            ele.children = child;
                            if (ele.children.length !== 0) {
                                userBasedMenu.push(ele);
                            }
                        }
                        else {
                            // if (ele1.create === true || ele1.edit === true || ele1.view === true) {
                            if (ele1.view === true) {
                                userBasedMenu.push(ele);
                            }
                        }
                    }
                });
            });
            this.menu = userBasedMenu;
        }
        else {
            this.menu = MENU_ITEMS;
        }
    };
    PagesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-pages',
            template: "\n    <ngx-sample-layout>\n      <nb-menu [items]=\"menu\" ></nb-menu>\n      <router-outlet></router-outlet>\n    </ngx-sample-layout>\n  ",
        }),
        __metadata("design:paramtypes", [_services_token_token_service__WEBPACK_IMPORTED_MODULE_1__["TokenService"]])
    ], PagesComponent);
    return PagesComponent;
}());



/***/ }),

/***/ "./src/app/pages/pages.module.ts":
/*!***************************************!*\
  !*** ./src/app/pages/pages.module.ts ***!
  \***************************************/
/*! exports provided: PagesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PagesModule", function() { return PagesModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _theme_theme_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../@theme/theme.module */ "./src/app/@theme/theme.module.ts");
/* harmony import */ var _pages_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./pages-routing.module */ "./src/app/pages/pages-routing.module.ts");
/* harmony import */ var _pages_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./pages.component */ "./src/app/pages/pages.component.ts");
/* harmony import */ var _e_commerce_e_commerce_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./e-commerce/e-commerce.module */ "./src/app/pages/e-commerce/e-commerce.module.ts");
/* harmony import */ var _miscellaneous_miscellaneous_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./miscellaneous/miscellaneous.module */ "./src/app/pages/miscellaneous/miscellaneous.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var PAGES_COMPONENTS = [
    _pages_component__WEBPACK_IMPORTED_MODULE_3__["PagesComponent"],
];
var PagesModule = /** @class */ (function () {
    function PagesModule() {
    }
    PagesModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _pages_routing_module__WEBPACK_IMPORTED_MODULE_2__["PagesRoutingModule"],
                _theme_theme_module__WEBPACK_IMPORTED_MODULE_1__["ThemeModule"],
                _e_commerce_e_commerce_module__WEBPACK_IMPORTED_MODULE_4__["ECommerceModule"],
                _miscellaneous_miscellaneous_module__WEBPACK_IMPORTED_MODULE_5__["MiscellaneousModule"],
            ],
            declarations: PAGES_COMPONENTS.slice()
        })
    ], PagesModule);
    return PagesModule;
}());



/***/ })

}]);
//# sourceMappingURL=app-pages-pages-module.js.map