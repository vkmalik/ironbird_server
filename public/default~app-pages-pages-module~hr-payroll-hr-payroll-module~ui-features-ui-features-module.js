(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~app-pages-pages-module~hr-payroll-hr-payroll-module~ui-features-ui-features-module"],{

/***/ "./src/app/pages/ui-features/hr-payroll/attendence/attendence.component.html":
/*!***********************************************************************************!*\
  !*** ./src/app/pages/ui-features/hr-payroll/attendence/attendence.component.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  Attendance works!\n</p>\n"

/***/ }),

/***/ "./src/app/pages/ui-features/hr-payroll/attendence/attendence.component.scss":
/*!***********************************************************************************!*\
  !*** ./src/app/pages/ui-features/hr-payroll/attendence/attendence.component.scss ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VpLWZlYXR1cmVzL2hyLXBheXJvbGwvYXR0ZW5kZW5jZS9hdHRlbmRlbmNlLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/pages/ui-features/hr-payroll/attendence/attendence.component.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/pages/ui-features/hr-payroll/attendence/attendence.component.ts ***!
  \*********************************************************************************/
/*! exports provided: AttendenceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AttendenceComponent", function() { return AttendenceComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AttendenceComponent = /** @class */ (function () {
    function AttendenceComponent() {
    }
    AttendenceComponent.prototype.ngOnInit = function () {
    };
    AttendenceComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-attendence',
            template: __webpack_require__(/*! ./attendence.component.html */ "./src/app/pages/ui-features/hr-payroll/attendence/attendence.component.html"),
            styles: [__webpack_require__(/*! ./attendence.component.scss */ "./src/app/pages/ui-features/hr-payroll/attendence/attendence.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], AttendenceComponent);
    return AttendenceComponent;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/hr-payroll/employee-details/employee-details.component.html":
/*!***********************************************************************************************!*\
  !*** ./src/app/pages/ui-features/hr-payroll/employee-details/employee-details.component.html ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nb-card>\n    <nb-card-header>\n        <h4 class=\"text-center\" style=\"color:white\"> Employee Status </h4>\n    </nb-card-header>\n\n    <nb-card-body>\n        <ng2-smart-table [settings]=\"settings\" [source]=\"source\" (createConfirm)=\"onCreateConfirm($event)\" (editConfirm)=\"onSaveConfirm($event)\" (deleteConfirm)=\"onDeleteConfirm($event)\">\n        </ng2-smart-table>\n    </nb-card-body>\n</nb-card>"

/***/ }),

/***/ "./src/app/pages/ui-features/hr-payroll/employee-details/employee-details.component.scss":
/*!***********************************************************************************************!*\
  !*** ./src/app/pages/ui-features/hr-payroll/employee-details/employee-details.component.scss ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "nb-card-body {\n  background-color: #E1F5FE;\n  color: white; }\n\nnb-card-header {\n  background: #1D8ECE;\n  color: white; }\n\nng2-smart-table-cell ng2-smart-table-cell table-cell-edit-modediv input-editor .nb-theme-corporate .input-group-addon:disabled, .nb-theme-corporate .form-control:disabled {\n  border-color: none !important; }\n\n:host {\n  font-size: 1rem; }\n\n:host /deep/ * {\n    -webkit-box-sizing: border-box;\n            box-sizing: border-box; }\n\n:host /deep/ select {\n    height: 2.4rem !important;\n    padding-top: 5px !important; }\n\n:host /deep/ /deep/ .modal-dialog {\n    max-width: 22%; }\n\n:host /deep/ table tbody tr:nth-child(even) {\n  background: #e9eaea !important; }\n\n:host /deep/ table tbody tr:nth-child(odd) {\n  background: white !important; }\n\n/deep/ng2-smart-table .ng2-smart-title {\n  color: white; }\n\n/deep/ng2-smart-table i {\n  color: black; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3ByYWthc2gvVmlwaW4vaXJvbmJpcmQvMjBGRUIyMDIwL2lyb25iaXJkLWNsaWVudHMvc3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9oci1wYXlyb2xsL2VtcGxveWVlLWRldGFpbHMvZW1wbG95ZWUtZGV0YWlscy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHlCQUF3QjtFQUN4QixZQUFXLEVBQUE7O0FBRWQ7RUFDRyxtQkFBbUI7RUFDbkIsWUFBWSxFQUFBOztBQUdkO0VBQ0UsNkJBQTZCLEVBQUE7O0FBR2pDO0VBQ0UsZUFBZSxFQUFBOztBQURqQjtJQUtNLDhCQUFzQjtZQUF0QixzQkFBc0IsRUFBQTs7QUFMNUI7SUFVTSx5QkFBeUI7SUFDekIsMkJBQTJCLEVBQUE7O0FBWGpDO0lBYTRCLGNBQWdCLEVBQUE7O0FBRzVDO0VBQ0UsOEJBQTRCLEVBQUE7O0FBRTVCO0VBQ0EsNEJBQTRCLEVBQUE7O0FBRzVCO0VBRUYsWUFBVyxFQUFBOztBQUdUO0VBQ0UsWUFBVyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvdWktZmVhdHVyZXMvaHItcGF5cm9sbC9lbXBsb3llZS1kZXRhaWxzL2VtcGxveWVlLWRldGFpbHMuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJuYi1jYXJkLWJvZHl7XG4gICAgYmFja2dyb3VuZC1jb2xvcjojRTFGNUZFO1xuICAgIGNvbG9yOndoaXRlO1xuICB9XG4gbmItY2FyZC1oZWFkZXJ7XG4gICAgYmFja2dyb3VuZDogIzFEOEVDRTtcbiAgICBjb2xvcjogd2hpdGU7XG4gIH1cblxuICBuZzItc21hcnQtdGFibGUtY2VsbCBuZzItc21hcnQtdGFibGUtY2VsbCB0YWJsZS1jZWxsLWVkaXQtbW9kZWRpdiBpbnB1dC1lZGl0b3IgLm5iLXRoZW1lLWNvcnBvcmF0ZSAuaW5wdXQtZ3JvdXAtYWRkb246ZGlzYWJsZWQsIC5uYi10aGVtZS1jb3Jwb3JhdGUgLmZvcm0tY29udHJvbDpkaXNhYmxlZCB7XG4gICAgYm9yZGVyLWNvbG9yOiBub25lICFpbXBvcnRhbnQ7XG59XG5cbjpob3N0IHtcbiAgZm9udC1zaXplOiAxcmVtO1xuXG4gIC9kZWVwLyB7XG4gICAgKiB7XG4gICAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAgIH1cblxuICAgXG4gICAgc2VsZWN0IHtcbiAgICAgIGhlaWdodDogMi40cmVtICFpbXBvcnRhbnQ7XG4gICAgICBwYWRkaW5nLXRvcDogNXB4ICFpbXBvcnRhbnQ7XG4gICAgfVxuICAgIC9kZWVwLyAubW9kYWwtZGlhbG9nIHsgIG1heC13aWR0aDogMjIlICB9XG4gIH1cbn1cbjpob3N0IC9kZWVwLyB0YWJsZSB0Ym9keSB0cjpudGgtY2hpbGQoZXZlbikge1xuICBiYWNrZ3JvdW5kOiNlOWVhZWEhaW1wb3J0YW50O1xuICB9XG4gIDpob3N0IC9kZWVwLyB0YWJsZSB0Ym9keSB0cjpudGgtY2hpbGQob2RkKSB7XG4gIGJhY2tncm91bmQ6ICB3aGl0ZSFpbXBvcnRhbnQ7XG4gIH1cblxuICAvZGVlcC9uZzItc21hcnQtdGFibGUgLm5nMi1zbWFydC10aXRsZVxuICB7XG5jb2xvcjp3aGl0ZTtcbiAgfVxuXG4gIC9kZWVwL25nMi1zbWFydC10YWJsZSBpe1xuICAgIGNvbG9yOmJsYWNrO1xuXG4gIH0iXX0= */"

/***/ }),

/***/ "./src/app/pages/ui-features/hr-payroll/employee-details/employee-details.component.ts":
/*!*********************************************************************************************!*\
  !*** ./src/app/pages/ui-features/hr-payroll/employee-details/employee-details.component.ts ***!
  \*********************************************************************************************/
/*! exports provided: EmployeeDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeeDetailsComponent", function() { return EmployeeDetailsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var _confirmation_dialog_confirmation_dialog_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../confirmation-dialog/confirmation-dialog.service */ "./src/app/confirmation-dialog/confirmation-dialog.service.ts");
/* harmony import */ var _services_token_token_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/token/token.service */ "./src/app/services/token/token.service.ts");
/* harmony import */ var _services_admin_employee_details_employee_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/admin/employee-details/employee.service */ "./src/app/services/admin/employee-details/employee.service.ts");
/* harmony import */ var _services_admin_user_management_user_management_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../services/admin/user-management/user-management.service */ "./src/app/services/admin/user-management/user-management.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






var EmployeeDetailsComponent = /** @class */ (function () {
    function EmployeeDetailsComponent(userManagementServ, confirmationDialogService, userServ, employeeServ) {
        this.userManagementServ = userManagementServ;
        this.confirmationDialogService = confirmationDialogService;
        this.userServ = userServ;
        this.employeeServ = employeeServ;
        this.source = new ng2_smart_table__WEBPACK_IMPORTED_MODULE_1__["LocalDataSource"]();
        this.moduleCreate = true;
        this.moduleEdit = true;
        this.moduleView = true;
    }
    EmployeeDetailsComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.userServ.userAccess !== undefined) {
            this.userServ.userAccess
                .subscribe(function (data) {
                data.forEach(function (ele) {
                    if (ele.name === 'HR Payroll') {
                        ele.subMenu.forEach(function (element) {
                            if (element.name === 'Employee Status') {
                                _this.moduleCreate = element.create;
                                _this.moduleEdit = element.edit;
                                _this.moduleView = element.view;
                            }
                        });
                    }
                });
            }, function (error) {
                console.log(error);
            });
        }
        if (this.moduleCreate || this.moduleEdit) {
            this.settings = {
                add: {
                    addButtonContent: '<i class="nb-plus"></i>',
                    createButtonContent: '<i class="nb-checkmark"></i>',
                    cancelButtonContent: '<i class="nb-close"></i>',
                    confirmCreate: true,
                },
                edit: {
                    editButtonContent: '<i class="nb-edit"></i>',
                    saveButtonContent: '<i class="nb-checkmark"></i>',
                    cancelButtonContent: '<i class="nb-close"></i>',
                    confirmSave: true,
                },
                delete: {
                    deleteButtonContent: '<i class="nb-trash"></i>',
                    confirmDelete: true,
                },
                actions: {
                    delete: false,
                    add: false,
                },
                columns: {
                    S_NO: {
                        title: 'Employee Number',
                        type: 'number',
                        editable: false,
                    },
                    name: {
                        title: 'Employee Name',
                        type: 'string',
                        editable: false,
                    },
                    companyEmail: {
                        title: 'Email',
                        type: 'string',
                        editable: false,
                    },
                    position: {
                        title: 'Designation',
                        type: 'string',
                        editable: false,
                    },
                    isActive: {
                        title: 'Status',
                        type: 'html',
                        editor: {
                            type: 'list',
                            config: {
                                list: [
                                    { value: 'true', title: 'Active' },
                                    { value: 'false', title: 'InActive' }
                                ],
                            },
                        }
                    },
                },
            };
        }
        else {
            this.settings = {
                add: {
                    addButtonContent: '<i class="nb-plus"></i>',
                    createButtonContent: '<i class="nb-checkmark"></i>',
                    cancelButtonContent: '<i class="nb-close"></i>',
                    confirmCreate: true,
                },
                edit: {
                    editButtonContent: '<i class="nb-edit"></i>',
                    saveButtonContent: '<i class="nb-checkmark"></i>',
                    cancelButtonContent: '<i class="nb-close"></i>',
                    confirmSave: true,
                },
                delete: {
                    deleteButtonContent: '<i class="nb-trash"></i>',
                    confirmDelete: true,
                },
                actions: {
                    delete: false,
                    add: false,
                    edit: false,
                },
                columns: {
                    S_NO: {
                        title: 'Employee Number',
                        type: 'number',
                        editable: false,
                    },
                    name: {
                        title: 'Employee Name',
                        type: 'string',
                        editable: false,
                    },
                    companyEmail: {
                        title: 'Email',
                        type: 'string',
                        editable: false,
                    },
                    position: {
                        title: 'Designation',
                        type: 'string',
                        editable: false,
                    },
                    isActive: {
                        title: 'Status',
                        type: 'html',
                        editor: {
                            type: 'list',
                            config: {
                                list: [
                                    { value: 'true', title: 'Active' },
                                    { value: 'false', title: 'InActive' }
                                ],
                            },
                        }
                    },
                },
            };
        }
        this.getuserManagementInfo();
    };
    /**
     * Get  Details
     */
    EmployeeDetailsComponent.prototype.getuserManagementInfo = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, error_1;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        _a = this;
                        return [4 /*yield*/, this.employeeServ.getEmployee()];
                    case 1:
                        _a.listOfUserManagement = _b.sent();
                        // console.log(this.listOfUserManagement)
                        this.listOfUserManagement.reverse();
                        this.listOfUserManagement.map(function (data, index) { return data.S_NO = 'IBE/BNG/EMP' + index + 1; });
                        this.listOfUserManagement.map(function (data) { return data.name = data.firstName + "  " + data.lastName; });
                        this.source.load(this.listOfUserManagement);
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _b.sent();
                        console.error(error_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Create Details
     */
    EmployeeDetailsComponent.prototype.onCreateConfirm = function (event) {
        return __awaiter(this, void 0, void 0, function () {
            var resultAleart, newData;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.confirmationDialogService.confirm('Alert Message', 'Are you sure you want to Create?')];
                    case 1:
                        resultAleart = _a.sent();
                        if (!resultAleart) return [3 /*break*/, 3];
                        newData = event.newData;
                        //  console.log(event.S_NO)
                        newData.isActive = true;
                        return [4 /*yield*/, this.userManagementServ.createUserManagement(newData)];
                    case 2:
                        _a.sent();
                        this.getuserManagementInfo();
                        event.confirm.resolve();
                        return [3 /*break*/, 4];
                    case 3:
                        event.confirm.reject();
                        _a.label = 4;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Selected Row Edit Terms And Condition
     */
    EmployeeDetailsComponent.prototype.onSaveConfirm = function (event) {
        return __awaiter(this, void 0, void 0, function () {
            var resultAleart, editData, result, error_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.confirmationDialogService.confirm('Alert Message', 'Are you sure you want to Save?')];
                    case 1:
                        resultAleart = _a.sent();
                        if (!resultAleart) return [3 /*break*/, 6];
                        _a.label = 2;
                    case 2:
                        _a.trys.push([2, 4, , 5]);
                        editData = event.newData;
                        return [4 /*yield*/, this.userManagementServ.updateUserManagement(editData)];
                    case 3:
                        result = _a.sent();
                        // result.S_NO = S_NO;
                        // this.listOfUserManagement.splice((S_NO - 1), 1, result);
                        this.source.load(this.listOfUserManagement);
                        event.confirm.resolve();
                        return [3 /*break*/, 5];
                    case 4:
                        error_2 = _a.sent();
                        console.log(error_2);
                        return [3 /*break*/, 5];
                    case 5: return [3 /*break*/, 7];
                    case 6:
                        event.confirm.reject();
                        _a.label = 7;
                    case 7: return [2 /*return*/];
                }
            });
        });
    };
    /**
    * Delete Terms And Condition
    */
    EmployeeDetailsComponent.prototype.onDeleteConfirm = function (event) {
        return __awaiter(this, void 0, void 0, function () {
            var resultAleart, editData;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.confirmationDialogService.confirm('Alert Message', 'Are you sure you want to Delete?')];
                    case 1:
                        resultAleart = _a.sent();
                        if (!resultAleart) return [3 /*break*/, 3];
                        editData = event.data;
                        return [4 /*yield*/, this.userManagementServ.removeUserManagement(editData)];
                    case 2:
                        _a.sent();
                        this.listOfUserManagement.splice((event.data.S_NO - 1), 1);
                        this.listOfUserManagement.reverse();
                        this.listOfUserManagement.map(function (data, index) { return data.S_NO = 'IBE/BNG/EMP' + index + 1; });
                        this.listOfUserManagement.map(function (data) { return data.name = data.firstName + "  " + data.lastName; });
                        this.source.load(this.listOfUserManagement);
                        event.confirm.resolve();
                        return [3 /*break*/, 4];
                    case 3:
                        event.confirm.reject();
                        _a.label = 4;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    EmployeeDetailsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-employee-details',
            template: __webpack_require__(/*! ./employee-details.component.html */ "./src/app/pages/ui-features/hr-payroll/employee-details/employee-details.component.html"),
            styles: [__webpack_require__(/*! ./employee-details.component.scss */ "./src/app/pages/ui-features/hr-payroll/employee-details/employee-details.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_admin_user_management_user_management_service__WEBPACK_IMPORTED_MODULE_5__["UserManagementService"],
            _confirmation_dialog_confirmation_dialog_service__WEBPACK_IMPORTED_MODULE_2__["ConfirmationDialogService"],
            _services_token_token_service__WEBPACK_IMPORTED_MODULE_3__["TokenService"],
            _services_admin_employee_details_employee_service__WEBPACK_IMPORTED_MODULE_4__["EmployeeService"]])
    ], EmployeeDetailsComponent);
    return EmployeeDetailsComponent;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/hr-payroll/employee-total-details/employee-total-details.component.html":
/*!***********************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/hr-payroll/employee-total-details/employee-total-details.component.html ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n    <div class=\"col-xl-5\">\n        <nb-card>\n            <nb-card-header>\n             Employee Details \n            </nb-card-header>\n          \n            <nb-card-body>\n              <ng2-smart-table [settings]=\"settings\" [source]=\"source\" (deleteConfirm)=\"onDeleteConfirm($event)\"  (userRowSelect)=\"onUserRowSelect($event)\">\n              </ng2-smart-table>\n          </nb-card-body>\n          </nb-card>\n       <!-- <button class=\"btn btn-success btn-rounded \" (click)=\"showFun('create')\" >Create New Jobs</button> -->\n      </div>\n    <div class=\"col-xl-7\" *ngIf=\"tableShow\">\n<ngx-update-employee></ngx-update-employee>\n    </div>\n    \n  </div>\n "

/***/ }),

/***/ "./src/app/pages/ui-features/hr-payroll/employee-total-details/employee-total-details.component.scss":
/*!***********************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/hr-payroll/employee-total-details/employee-total-details.component.scss ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "nb-card-body {\n  background-color: #E1F5FE;\n  color: white; }\n\ncol-xl-6, nb-card-header {\n  background: #1D8ECE;\n  color: white; }\n\n:host /deep/ table tbody tr:nth-child(even) {\n  background: #e9eaea !important; }\n\n:host /deep/ table tbody tr:nth-child(odd) {\n  background: white !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3ByYWthc2gvVmlwaW4vaXJvbmJpcmQvMjBGRUIyMDIwL2lyb25iaXJkLWNsaWVudHMvc3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9oci1wYXlyb2xsL2VtcGxveWVlLXRvdGFsLWRldGFpbHMvZW1wbG95ZWUtdG90YWwtZGV0YWlscy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHlCQUF3QjtFQUN4QixZQUFXLEVBQUE7O0FBRWI7RUFDRSxtQkFBbUI7RUFDbkIsWUFBWSxFQUFBOztBQUVkO0VBQ0UsOEJBQTRCLEVBQUE7O0FBRTVCO0VBQ0EsNEJBQTRCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9oci1wYXlyb2xsL2VtcGxveWVlLXRvdGFsLWRldGFpbHMvZW1wbG95ZWUtdG90YWwtZGV0YWlscy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIm5iLWNhcmQtYm9keXtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiNFMUY1RkU7XG4gICAgY29sb3I6d2hpdGU7XG4gIH1cbiAgY29sLXhsLTYsIG5iLWNhcmQtaGVhZGVye1xuICAgIGJhY2tncm91bmQ6ICMxRDhFQ0U7XG4gICAgY29sb3I6IHdoaXRlO1xuICB9XG4gIDpob3N0IC9kZWVwLyB0YWJsZSB0Ym9keSB0cjpudGgtY2hpbGQoZXZlbikge1xuICAgIGJhY2tncm91bmQ6I2U5ZWFlYSFpbXBvcnRhbnQ7XG4gICAgfVxuICAgIDpob3N0IC9kZWVwLyB0YWJsZSB0Ym9keSB0cjpudGgtY2hpbGQob2RkKSB7XG4gICAgYmFja2dyb3VuZDogIHdoaXRlIWltcG9ydGFudDtcbiAgICB9Il19 */"

/***/ }),

/***/ "./src/app/pages/ui-features/hr-payroll/employee-total-details/employee-total-details.component.ts":
/*!*********************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/hr-payroll/employee-total-details/employee-total-details.component.ts ***!
  \*********************************************************************************************************/
/*! exports provided: EmployeeTotalDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeeTotalDetailsComponent", function() { return EmployeeTotalDetailsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var _services_admin_employee_details_employee_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/admin/employee-details/employee.service */ "./src/app/services/admin/employee-details/employee.service.ts");
/* harmony import */ var _services_admin_user_management_user_management_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/admin/user-management/user-management.service */ "./src/app/services/admin/user-management/user-management.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var EmployeeTotalDetailsComponent = /** @class */ (function () {
    function EmployeeTotalDetailsComponent(userManagementServ, employeeServ) {
        this.userManagementServ = userManagementServ;
        this.employeeServ = employeeServ;
        this.creatShow = false;
        this.tableShow = false;
        this.settings = {
            add: {
                addButtonContent: '<i class="nb-plus"></i>',
                createButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
            },
            edit: {
                editButtonContent: '<i class="nb-edit"></i>',
                saveButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
            },
            delete: {
                deleteButtonContent: '<i class="nb-trash"></i>',
                confirmDelete: true,
            },
            actions: {
                delete: false,
                add: false,
                edit: false,
            },
            columns: {
                S_NO: {
                    title: 'Employee Number',
                    type: 'number',
                    editable: false,
                },
                name: {
                    title: 'Employee Name',
                    type: 'string',
                },
                deportment: {
                    title: 'Department',
                    type: 'string',
                },
                position: {
                    title: 'Designation',
                    type: 'string',
                },
            },
        };
        this.source = new ng2_smart_table__WEBPACK_IMPORTED_MODULE_1__["LocalDataSource"]();
    }
    EmployeeTotalDetailsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.employeeServ.userEmployee
            .subscribe(function (data) {
            _this.getuserManagementInfo();
        }, function (error) {
            console.log(error);
        });
        this.getuserManagementInfo();
    };
    EmployeeTotalDetailsComponent.prototype.showFun = function () {
        this.creatShow = true;
        this.tableShow = false;
        this.employeeServ.userEmployee.next({ creatShow: true });
    };
    /**
     * Get Details of Employee
     */
    EmployeeTotalDetailsComponent.prototype.getuserManagementInfo = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, error_1;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        _a = this;
                        return [4 /*yield*/, this.employeeServ.getEmployee()];
                    case 1:
                        _a.listOfUserManagement = _b.sent();
                        // console.log(this.listOfUserManagement)
                        this.listOfUserManagement.reverse();
                        this.listOfUserManagement.map(function (data, index) { return data.S_NO = 'IBE/BNG/EMP' + index + 1; });
                        this.listOfUserManagement.map(function (data) { return data.name = data.firstName + "  " + data.lastName; });
                        this.source.load(this.listOfUserManagement);
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _b.sent();
                        console.error(error_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Create New Details of Employee
     */
    EmployeeTotalDetailsComponent.prototype.onCreateConfirm = function (event) {
        return __awaiter(this, void 0, void 0, function () {
            var newData;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!window.confirm('Are you sure you want to create?')) return [3 /*break*/, 2];
                        newData = event.newData;
                        //  console.log(event.S_NO)
                        newData.isActive = true;
                        return [4 /*yield*/, this.userManagementServ.createUserManagement(newData)];
                    case 1:
                        _a.sent();
                        this.getuserManagementInfo();
                        event.confirm.resolve();
                        return [3 /*break*/, 3];
                    case 2:
                        event.confirm.reject();
                        _a.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Selected Row Edit Details of Employee
     */
    EmployeeTotalDetailsComponent.prototype.onSaveConfirm = function (event) {
        return __awaiter(this, void 0, void 0, function () {
            var editData, result, error_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!window.confirm('Are you sure you want to save?')) return [3 /*break*/, 5];
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        editData = event.newData;
                        return [4 /*yield*/, this.userManagementServ.updateUserManagement(editData)];
                    case 2:
                        result = _a.sent();
                        // result.S_NO = S_NO;
                        // this.listOfUserManagement.splice((S_NO - 1), 1, result);
                        this.source.load(this.listOfUserManagement);
                        event.confirm.resolve();
                        return [3 /*break*/, 4];
                    case 3:
                        error_2 = _a.sent();
                        console.log(error_2);
                        return [3 /*break*/, 4];
                    case 4: return [3 /*break*/, 6];
                    case 5:
                        event.confirm.reject();
                        _a.label = 6;
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * User Selected Row
     */
    EmployeeTotalDetailsComponent.prototype.onUserRowSelect = function (event) {
        event.data.creatShow = false;
        this.employeeServ.userEmployee.next(event.data);
        if (event) {
            this.creatShow = false;
            return this.tableShow = true;
        }
        this.creatShow = true;
        this.tableShow = false;
    };
    /**
    * Delete Details of Employee
    */
    EmployeeTotalDetailsComponent.prototype.onDeleteConfirm = function (event) {
        return __awaiter(this, void 0, void 0, function () {
            var editData;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!window.confirm('Are you sure you want to delete?')) return [3 /*break*/, 2];
                        editData = event.data;
                        return [4 /*yield*/, this.userManagementServ.removeUserManagement(editData)];
                    case 1:
                        _a.sent();
                        this.listOfUserManagement.splice((event.data.S_NO - 1), 1);
                        this.listOfUserManagement.reverse();
                        this.listOfUserManagement.map(function (data, index) { return data.S_NO = 'IBE/BNG/EMP' + index + 1; });
                        this.listOfUserManagement.map(function (data) { return data.name = data.firstName + "  " + data.lastName; });
                        this.source.load(this.listOfUserManagement);
                        event.confirm.resolve();
                        return [3 /*break*/, 3];
                    case 2:
                        event.confirm.reject();
                        _a.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    EmployeeTotalDetailsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-employee-total-details',
            template: __webpack_require__(/*! ./employee-total-details.component.html */ "./src/app/pages/ui-features/hr-payroll/employee-total-details/employee-total-details.component.html"),
            styles: [__webpack_require__(/*! ./employee-total-details.component.scss */ "./src/app/pages/ui-features/hr-payroll/employee-total-details/employee-total-details.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_admin_user_management_user_management_service__WEBPACK_IMPORTED_MODULE_3__["UserManagementService"],
            _services_admin_employee_details_employee_service__WEBPACK_IMPORTED_MODULE_2__["EmployeeService"]])
    ], EmployeeTotalDetailsComponent);
    return EmployeeTotalDetailsComponent;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/hr-payroll/employee-total-details/update-employee/main-details/main-details.component.html":
/*!******************************************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/hr-payroll/employee-total-details/update-employee/main-details/main-details.component.html ***!
  \******************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nb-card>\n    <nb-card-body>\n        <div class=\"row\">\n\n            <div class=\"col-lg-12\">\n                <br>\n                <form style=\"color: #757575;\" #addEmployee=\"ngForm\" (ngSubmit)=\"editEmployee(addEmployee)\">\n                    <div class=\"form-row\">\n                        <div class=\"col-lg-12\">\n                            <h4 class=\"text-center\">Employee Details</h4>\n\n                        </div>\n                    </div>\n\n                    <div class=\"form-row\">\n                        <div class=\"col-lg-6\">\n\n                            <div class=\"form-group input-group-sm\">\n                                <label for=\"materialRegisterFormFirstName\">First name</label>\n                                <input type=\"text\" id=\"materialRegisterFormFirstName\" name=\"firstName\" [(ngModel)]=\"employee.firstName\" class=\"form-control\">\n                            </div>\n                        </div>\n                        <div class=\"col-lg-6\">\n\n                            <div class=\"form-group input-group-sm\">\n                                <label for=\"materialRegisterFormLastName\">Last name</label>\n                                <input type=\"Text\" id=\"materialRegisterFormLastName\" name=\"lastName\" [(ngModel)]=\"employee.lastName\" class=\"form-control\">\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"form-row\">\n                        <div class=\"col-lg-6\">\n\n                            <div class=\"form-group input-group-sm\">\n                                <label for=\"form7\">Permanent Address</label>\n                                <textarea type=\"text\" id=\"form7\" class=\"md-textarea form-control\" name=\"permanentAddr\" [(ngModel)]=\"employee.permanentAddr\" rows=\"3\"></textarea>\n                            </div>\n                        </div>\n                        <div class=\"col-lg-6\">\n\n                            <div class=\"form-group input-group-sm\">\n                                <label for=\"form76\">Temporary Address</label>\n                                <textarea type=\"text\" id=\"form76\" class=\"md-textarea form-control\" name=\"presentAddr\" [(ngModel)]=\"employee.presentAddr\" rows=\"3\"></textarea>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"form-row\">\n                        <div class=\"col-lg-6\">\n                            <div class=\"form-row\">\n                                <div class=\"col-lg-6\">\n\n                                    <div class=\"form-group input-group-sm\">\n                                        <label for=\"city\">Permanent City</label>\n                                        <input type=\"text\" id=\"city\" name=\"city\" [(ngModel)]=\"employee.city\" class=\"form-control\">\n                                    </div>\n                                </div>\n                                <div class=\"col-lg-6\">\n\n                                    <div class=\"form-group input-group-sm\">\n                                        <label for=\"State\">Permanent State</label>\n                                        <input type=\"email\" id=\"State\" name=\"state\" [(ngModel)]=\"employee.state\" class=\"form-control\">\n                                    </div>\n                                </div>\n                                <div class=\"col-lg-6\">\n\n                                    <div class=\"form-group input-group-sm\">\n                                        <label for=\"Pin\">Permanent PinCode</label>\n                                        <input type=\"email\" id=\"Pin\" name=\"pin\" [(ngModel)]=\"employee.pin\" class=\"form-control\">\n                                    </div>\n                                </div>\n                                <div class=\"col-lg-6\">\n\n                                    <div class=\"form-group input-group-sm\">\n                                        <label for=\"materialRegisterFormLastName\">Permanent Country</label>\n                                        <input type=\"email\" id=\"materialRegisterFormLastName\" name=\"country\" [(ngModel)]=\"employee.country\" class=\"form-control\">\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                        <div class=\"col-lg-6\">\n                            <div class=\"form-row\">\n                                <div class=\"col-lg-6\">\n\n                                    <div class=\"form-group input-group-sm\">\n                                        <label for=\"city\">Temporary City</label>\n                                        <input type=\"text\" id=\"city\" name=\"Tmcity\" [(ngModel)]=\"employee.Tmcity\" class=\"form-control\">\n                                    </div>\n                                </div>\n                                <div class=\"col-lg-6\">\n\n                                    <div class=\"form-group input-group-sm\">\n                                        <label for=\"State\">Temporary State</label>\n                                        <input type=\"email\" id=\"State\" name=\"Tmstate\" [(ngModel)]=\"employee.Tmstate\" class=\"form-control\">\n                                    </div>\n                                </div>\n                                <div class=\"col-lg-6\">\n\n                                    <div class=\"form-group input-group-sm\">\n                                        <label for=\"Pin\">Temporary PinCode</label>\n                                        <input type=\"email\" id=\"Pin\" name=\"Tmpin\" [(ngModel)]=\"employee.Tmpin\" class=\"form-control\">\n                                    </div>\n                                </div>\n                                <div class=\"col-lg-6\">\n\n                                    <div class=\"form-group input-group-sm\">\n                                        <label for=\"materialRegisterFormLastName\">Temporary Country</label>\n                                        <input type=\"email\" id=\"materialRegisterFormLastName\" name=\"Tmcountry\" [(ngModel)]=\"employee.Tmcountry\" class=\"form-control\">\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n\n\n                    <!-- Phone number -->\n                    <div class=\"form-row\">\n                        <div class=\"col-lg-6\">\n                            <div class=\"form-group input-group-sm\">\n                                <label for=\"Phone\">Mobile Number</label>\n                                <input type=\"text\" id=\"Phone\" class=\"form-control\" name=\"mobNumber\" [(ngModel)]=\"employee.mobNumber\" aria-describedby=\"materialRegisterFormPhoneHelpBlock\">\n                            </div>\n                        </div>\n                        <div class=\"col-lg-6\">\n\n                            <div class=\"form-group input-group-sm\">\n                                <label for=\"materialRegisterFormPhone\">Alternate Mobile Number</label>\n                                <input type=\"text\" id=\"materialRegisterFormPhone\" name=\"mobAlNumber\" [(ngModel)]=\"employee.mobAlNumber\" class=\"form-control\" aria-describedby=\"materialRegisterFormPhoneHelpBlock\">\n                                <small id=\"materialRegisterFormPhoneHelpBlock\" class=\"form-text text-muted mb-4\">\n                  Optional\n                </small>\n                            </div>\n                        </div>\n                    </div>\n\n\n                    <div class=\"form-row\">\n                        <div class=\"col-lg-6\">\n                            <div class=\"form-group input-group-sm\">\n                                <label for=\"Email\">Email Id</label>\n                                <input type=\"email\" id=\"Email\" class=\"form-control\" name=\"email\" [(ngModel)]=\"employee.email\" aria-describedby=\"materialRegisterFormPhoneHelpBlock\">\n\n                            </div>\n                        </div>\n                        <div class=\"col-lg-6\">\n\n                            <div class=\"form-group input-group-sm\">\n                                <label for=\"Alternate\">Alternate Email Id</label>\n                                <input type=\"email\" id=\"Alternate\" class=\"form-control\" name=\"altEmail\" [(ngModel)]=\"employee.altEmail\" aria-describedby=\"materialRegisterFormPhoneHelpBlock\">\n                                <small id=\"materialRegisterFormPhoneHelpBlock\" class=\"form-text text-muted mb-4\">\n                  Optional\n                </small>\n                            </div>\n                        </div>\n                    </div>\n\n\n                    <div class=\"form-row\">\n                        <div class=\"col-lg-6\">\n                            <label style=\"font-weight: 700;color: black;\">Select Gender</label>\n                            <div class=\"row\">\n                                <div class=\"col-lg-3\">\n                                    <label class=\"custom-control custom-radio\">\n                        <input type=\"radio\" class=\"custom-control-input\" name=\"gender\" value=\"male\" ngModel>\n                        <span class=\"custom-control-indicator\"></span>\n                        <span class=\"custom-control-description\">Male</span>\n                      </label>\n                                </div>\n                                <div class=\"col-lg-3\">\n                                    <label class=\"custom-control custom-radio\">\n                        <input type=\"radio\" class=\"custom-control-input\" name=\"gender\" value=\"female\" ngModel >\n                        <span class=\"custom-control-indicator\"></span>\n                        <span class=\"custom-control-description\">Female</span>\n                      </label>\n                                </div>\n                            </div>\n                            <label style=\"font-weight: 700;color: black;\">Select Marital Status</label>\n                            <div class=\"row\">\n                                <div class=\"col-lg-3\">\n                                    <label class=\"custom-control custom-radio\">\n                        <input type=\"radio\" class=\"custom-control-input\" name=\"marriedStatus\" value=\"single\" ngModel>\n                        <span class=\"custom-control-indicator\"></span>\n                        <span class=\"custom-control-description\">Single</span>\n                      </label>\n                                </div>\n                                <div class=\"col-lg-3\">\n                                    <label class=\"custom-control custom-radio\">\n                        <input type=\"radio\" class=\"custom-control-input\" name=\"marriedStatus\" value=\"married\" ngModel\n                          >\n                        <span class=\"custom-control-indicator\"></span>\n                        <span class=\"custom-control-description\">Married</span>\n                      </label>\n                                </div>\n                            </div>\n                            <div class=\"row\">\n\n                                <div class=\"form-group input-group-sm col-lg-12\">\n                                    <label for=\"Alternates\"> Upload Photo</label>\n                                    <input type=\"file\" id=\"Alternates\" class=\"form-control\" name=\"altEmail\" ngModel aria-describedby=\"materialRegisterFormPhoneHelpBlock\" placeholder=\"Upload Photo\">\n                                </div>\n                                <div class=\"form-group input-group-sm col-lg-12\">\n                                    <label for=\"Alternate\">Upload Any Government Id</label>\n                                    <input type=\"file\" id=\"Alternate\" class=\"form-control\" name=\"altEmail\" ngModel aria-describedby=\"materialRegisterFormPhoneHelpBlocks\" placeholder=\"Upload Photo\">\n                                </div>\n                                <div class=\"form-group input-group-sm col-lg-12\">\n                                    <label for=\"Alternates\">Upload Resume Attachement</label>\n                                    <input type=\"file\" id=\"Alternates\" class=\"form-control\" name=\"altEmail\" ngModel aria-describedby=\"materialRegisterFormPhoneHelpBlock\" placeholder=\"Upload Photo\">\n                                </div>\n\n                            </div>\n                        </div>\n\n                        <div class=\"col-lg-3\">\n                            <div>\n                                <img [src]=\"imageSrc6\" [(ngModel)]=\"employee.altEmail\" class=\"rounded mb-3\" width=\"220px\" height=\"220px\" style=\"object-fit: scale-down\">\n                            </div>\n                        </div>\n                        <div class=\"col-lg-3\">\n                            <div>\n                                <img [src]=\"imageSrc6\" class=\"rounded mb-3\" width=\"220px\" height=\"220px\" style=\"object-fit: scale-down\">\n                            </div>\n                        </div>\n\n                    </div>\n                    <div class=\"form-row\">\n                        <div class=\"col-lg-6\">\n                            <div class=\"form-group input-group-sm\">\n                                <label for=\"Emails\">Company Email Id</label>\n                                <input type=\"email\" id=\"Emails\" class=\"form-control\" name=\"companyEmail\" [(ngModel)]=\"employee.companyEmail\" aria-describedby=\"materialRegisterFormPhoneHelpBlock\">\n\n                            </div>\n                        </div>\n                        <div class=\"col-lg-6\">\n                            <div class=\"form-group input-group-sm\">\n                                <label for=\"mobiless\">Company Mobile No:</label>\n                                <input type=\"text\" id=\"mobiless\" class=\"form-control\" name=\"companyMob\" [(ngModel)]=\"employee.companyMob\" aria-describedby=\"materialRegisterFormPhoneHelpBlock\">\n\n                            </div>\n                        </div>\n                    </div>\n\n\n\n\n\n\n                    <!-- Newsletter -->\n                    <div class=\"form-row\">\n                        <div class=\"col-lg-6\">\n                            <div class=\"form-group input-group-sm\">\n\n                                <label><b>Date Of Birth</b></label>\n                                <input type=\"date\" name=\"DOB\" class=\"form-control\" [(ngModel)]=\"employee.DOB\" required>\n                            </div>\n\n                        </div>\n                        <div class=\"col-lg-6\">\n\n                            <div class=\"form-group input-group-sm\">\n                                <label><b>Joining Date</b></label>\n                                <input type=\"date\" name=\"joinDate\" class=\"form-control\" [(ngModel)]=\"employee.joinDate\" required>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"form-row\">\n                        <div class=\"col-lg-6\">\n                            <div class=\"form-group input-group-sm\">\n\n                                <label><b>Deportment</b></label>\n                                <input type=\"text\" name=\"deportment\" class=\"form-control\" [(ngModel)]=\"employee.deportment\" required>\n                            </div>\n\n                        </div>\n                        <div class=\"col-lg-6\">\n\n                            <div class=\"form-group input-group-sm\">\n                                <label><b>Position</b></label>\n                                <input type=\"text\" name=\"position\" class=\"form-control\" [(ngModel)]=\"employee.position\" required>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"form-row\">\n                        <div class=\"col-lg-6\">\n                            <div class=\"form-group input-group-sm\">\n                                <label><b>Probation Period end Date</b></label>\n                                <input type=\"date\" name=\"probationDate\" [(ngModel)]=\"employee.probationDate\" class=\"form-control\" fieldSize=\"small\" placeholder=\"Probation Period end Date\" required>\n                            </div>\n                        </div>\n                        <div class=\"col-lg-6\">\n                            <div class=\"form-group input-group-sm\">\n                                <label><b>Employee Termination Date</b></label>\n                                <input type=\"date\" name=\"terminationDate\" [(ngModel)]=\"employee.terminationDate\" class=\"form-control\" fieldSize=\"small\" placeholder=\"Employee Termination Date\" required>\n                            </div>\n                        </div>\n                    </div>\n\n                    <div class=\"row\">\n                        <div class=\"col-lg-3\">\n                            <div class=\"form-group input-group-sm\">\n                                <label for=\"AccountName\">Bank Account Name</label>\n                                <input type=\"email\" id=\"AccountName\" class=\"form-control\" name=\"bankAccountName\" [(ngModel)]=\"employee.bankAccountName\" aria-describedby=\"materialRegisterFormPhoneHelpBlock\">\n\n                            </div>\n                        </div>\n                        <div class=\"col-lg-3\">\n                            <div class=\"form-group input-group-sm\">\n                                <label for=\"Account\">Bank Account Number</label>\n                                <input type=\"text\" id=\"Account\" class=\"form-control\" name=\"bankAccountNumber\" [(ngModel)]=\"employee.bankAccountNumber\" aria-describedby=\"materialRegisterFormPhoneHelpBlock\">\n                            </div>\n                        </div>\n                        <div class=\"col-lg-3\">\n                            <div class=\"form-group input-group-sm\">\n\n                                <label for=\"IFSC\">Bank IFSC Number</label>\n                                <input type=\"text\" id=\"IFSC\" class=\"form-control\" name=\"ifscCode\" [(ngModel)]=\"employee.ifscCode\" aria-describedby=\"materialRegisterFormPhoneHelpBlock\">\n\n                            </div>\n                        </div>\n                        <div class=\"col-lg-3\">\n                            <div class=\"form-group input-group-sm\">\n                                <label for=\"BankName\">Bank Name</label>\n                                <input type=\"text\" id=\"BankName\" class=\"form-control\" name=\"bankName\" [(ngModel)]=\"employee.bankName\" aria-describedby=\"materialRegisterFormPhoneHelpBlock\">\n\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"row foot\" *ngIf='modulesCreate || modulesEdit'>\n                        <div class=\"col-sm-12\" style=\"margin-top: 3px;\">\n                            <button class=\"btn btn-outline-primary \" shape=\"semi-round\" type=\"submit\" style=\"float:right\">Save</button>\n                        </div>\n                    </div>\n\n                    <!-- Sign up button -->\n                    <!-- <button class=\"btn btn-outline-primary \" shape=\"semi-round\" style=\"margin: 48px;\"  (click)=\"show = !show\" >Submit</button> -->\n\n\n                </form>\n                <!-- Form -->\n\n            </div>\n        </div>\n    </nb-card-body>\n</nb-card>"

/***/ }),

/***/ "./src/app/pages/ui-features/hr-payroll/employee-total-details/update-employee/main-details/main-details.component.scss":
/*!******************************************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/hr-payroll/employee-total-details/update-employee/main-details/main-details.component.scss ***!
  \******************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "nb-card-body {\n  background-color: #E1F5FE;\n  color: white; }\n\ncol-xl-6, nb-card-header {\n  background: #1D8ECE;\n  color: white; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3ByYWthc2gvVmlwaW4vaXJvbmJpcmQvMjBGRUIyMDIwL2lyb25iaXJkLWNsaWVudHMvc3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9oci1wYXlyb2xsL2VtcGxveWVlLXRvdGFsLWRldGFpbHMvdXBkYXRlLWVtcGxveWVlL21haW4tZGV0YWlscy9tYWluLWRldGFpbHMuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSx5QkFBd0I7RUFDeEIsWUFBVyxFQUFBOztBQUViO0VBQ0UsbUJBQW1CO0VBQ25CLFlBQVksRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VpLWZlYXR1cmVzL2hyLXBheXJvbGwvZW1wbG95ZWUtdG90YWwtZGV0YWlscy91cGRhdGUtZW1wbG95ZWUvbWFpbi1kZXRhaWxzL21haW4tZGV0YWlscy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIm5iLWNhcmQtYm9keXtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiNFMUY1RkU7XG4gICAgY29sb3I6d2hpdGU7XG4gIH1cbiAgY29sLXhsLTYsIG5iLWNhcmQtaGVhZGVye1xuICAgIGJhY2tncm91bmQ6ICMxRDhFQ0U7XG4gICAgY29sb3I6IHdoaXRlO1xuICB9Il19 */"

/***/ }),

/***/ "./src/app/pages/ui-features/hr-payroll/employee-total-details/update-employee/main-details/main-details.component.ts":
/*!****************************************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/hr-payroll/employee-total-details/update-employee/main-details/main-details.component.ts ***!
  \****************************************************************************************************************************/
/*! exports provided: MainDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainDetailsComponent", function() { return MainDetailsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _confirmation_dialog_confirmation_dialog_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../../confirmation-dialog/confirmation-dialog.service */ "./src/app/confirmation-dialog/confirmation-dialog.service.ts");
/* harmony import */ var _services_token_token_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../services/token/token.service */ "./src/app/services/token/token.service.ts");
/* harmony import */ var _services_admin_employee_details_employee_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../services/admin/employee-details/employee.service */ "./src/app/services/admin/employee-details/employee.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var MainDetailsComponent = /** @class */ (function () {
    // listOfSite: any;
    function MainDetailsComponent(EmployeeServ, userServ, confirmationDialogService) {
        this.EmployeeServ = EmployeeServ;
        this.userServ = userServ;
        this.confirmationDialogService = confirmationDialogService;
        this.employee = {};
        this.employeeChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.moduleCreate = true;
        this.moduleEdit = true;
        this.moduleView = true;
        this.getEmployeerDetail();
    }
    /**
      * Get Customer Detail
      */
    MainDetailsComponent.prototype.getEmployeerDetail = function () {
        var _this = this;
        this.EmployeeServ.userEmployee
            .subscribe(function (data) {
            _this.employee = data;
            _this.id = data.id;
        }, function (error) {
            console.log(error);
        });
    };
    /**
         Edit  Employee
       **/
    MainDetailsComponent.prototype.editEmployee = function (employeeData) {
        return __awaiter(this, void 0, void 0, function () {
            var value, resultAleart, result, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        value = employeeData.value;
                        return [4 /*yield*/, this.confirmationDialogService.confirm('Alert Message', 'Are you sure you want to Update?')];
                    case 1:
                        resultAleart = _a.sent();
                        if (!resultAleart) return [3 /*break*/, 6];
                        _a.label = 2;
                    case 2:
                        _a.trys.push([2, 4, , 5]);
                        return [4 /*yield*/, this.EmployeeServ.updateEmployee({ id: this.id }, value)];
                    case 3:
                        result = _a.sent();
                        this.EmployeeServ.userEmployee.next(result);
                        this.employeeChange.emit(result);
                        return [3 /*break*/, 5];
                    case 4:
                        error_1 = _a.sent();
                        console.log(error_1);
                        return [3 /*break*/, 5];
                    case 5: return [3 /*break*/, 6];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    MainDetailsComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.userServ.userAccess !== undefined) {
            this.userServ.userAccess
                .subscribe(function (data) {
                data.forEach(function (ele) {
                    if (ele.name === 'HR Payroll') {
                        ele.subMenu.forEach(function (element) {
                            if (element.name === 'Employee Details') {
                                _this.moduleCreate = element.create;
                                _this.moduleEdit = element.edit;
                                _this.moduleView = element.view;
                            }
                        });
                    }
                });
            }, function (error) {
                console.log(error);
            });
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], MainDetailsComponent.prototype, "employeeChange", void 0);
    MainDetailsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-main-details',
            template: __webpack_require__(/*! ./main-details.component.html */ "./src/app/pages/ui-features/hr-payroll/employee-total-details/update-employee/main-details/main-details.component.html"),
            styles: [__webpack_require__(/*! ./main-details.component.scss */ "./src/app/pages/ui-features/hr-payroll/employee-total-details/update-employee/main-details/main-details.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_admin_employee_details_employee_service__WEBPACK_IMPORTED_MODULE_3__["EmployeeService"],
            _services_token_token_service__WEBPACK_IMPORTED_MODULE_2__["TokenService"],
            _confirmation_dialog_confirmation_dialog_service__WEBPACK_IMPORTED_MODULE_1__["ConfirmationDialogService"]])
    ], MainDetailsComponent);
    return MainDetailsComponent;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/hr-payroll/employee-total-details/update-employee/performance-review/performance-review.component.html":
/*!******************************************************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/hr-payroll/employee-total-details/update-employee/performance-review/performance-review.component.html ***!
  \******************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nb-card-body>\n\n    <form #addEmployee=\"ngForm\" (ngSubmit)=\"editEmployee(addEmployee)\">\n        <!-- <button class=\"btn btn-outline-primary \" shape=\"semi-round\"   (click)=\"show = !show\" >Submit</button> -->\n        <div class=\"container\">\n            <div class=\"row\">\n                <div class=\"col-5\">\n                    <p class=\"back\">Employee Name: {{employee.firstName}}&nbsp;&nbsp;{{employee.lastName}}</p>\n                </div>\n            </div>\n            <h4 class=\"text-center\"> Performance </h4>\n            <!-- <div class=\"form-row\">\n        <div class=\"col-6\">\n           \n            <div class=\"md-form\">\n                <label for=\"materialRegisterFormFirstName\">First name</label>\n                <input   type=\"text\" id=\"materialRegisterFormFirstName\" class=\"form-control\"  name=\"firstName\" [(ngModel)]=\"employee.firstName\">\n            </div>\n        </div>\n        <div class=\"col-6\">\n           \n            <div class=\"md-form\">\n                <label for=\"materialRegisterFormLastName\">Last name</label>\n                <input   type=\"text\" id=\"materialRegisterFormLastName\" class=\"form-control\" name=\"lastName\" [(ngModel)]=\"employee.lastName\">\n            </div>\n        </div>\n    </div><br> -->\n            <div class=\"form-row\">\n                <div class=\"col\">\n\n                    <div class=\"md-form\">\n                        <label for=\"form7\">Employee Performance description</label>\n                        <textarea type=\"text\" id=\"form7\" class=\"md-textarea form-control\" rows=\"3\" name=\"Performance\" [(ngModel)]=\"employee.Performance\"></textarea>\n                    </div>\n                </div>\n                <div class=\"col\">\n\n                    <div class=\"md-form\">\n                        <label for=\"form76\">Manager feedback</label>\n                        <textarea type=\"text\" id=\"form76\" class=\"md-textarea form-control\" rows=\"3\" name=\"feedback\" [(ngModel)]=\"employee.feedback\"></textarea>\n                    </div>\n                </div>\n            </div><br>\n            <div class=\"form-row\">\n                <div class=\"col-6\">\n                    <label style=\"font-weight: 700;color: black;\">Promoted(Y/N)</label>\n                    <div class=\"row\">\n                        <div class=\"col-6\">\n                            <label class=\"radio-inline\"><input type=\"radio\" name=\"optradio\" >Yes</label>\n                        </div>\n\n                        <div class=\"col-6\">\n                            <label class=\"radio-inline\"><input type=\"radio\" name=\"optradio\" checked>No</label>\n                        </div>\n                    </div>\n                </div>\n            </div><br>\n            <div class=\"form-row\">\n                <div class=\"col-12\">\n\n                    <div class=\"md-form\">\n                        <label for=\"form17\">Promoted Designation</label>\n                        <input type=\"text\" name=\"position\" class=\"form-control\" [(ngModel)]=\"employee.position\" required>\n                    </div>\n                </div>\n\n            </div>\n            <br><br>\n            <div class=\"form-row foot\" *ngIf='moduleCreate || moduleEdit'>\n\n                <div class=\"col-12\">\n                    <!-- Sign up button -->\n                    <button class=\"btn btn-outline-primary \" shape=\"semi-round\" style=\"margin: 3px;float:right;\" mdbWavesEffect>Save</button>\n                </div>\n\n            </div>\n        </div>\n\n    </form>\n</nb-card-body>"

/***/ }),

/***/ "./src/app/pages/ui-features/hr-payroll/employee-total-details/update-employee/performance-review/performance-review.component.scss":
/*!******************************************************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/hr-payroll/employee-total-details/update-employee/performance-review/performance-review.component.scss ***!
  \******************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/deep/ .modal-dialog {\n  max-width: 22%; }\n\n.back {\n  text-align: left; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3ByYWthc2gvVmlwaW4vaXJvbmJpcmQvMjBGRUIyMDIwL2lyb25iaXJkLWNsaWVudHMvc3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9oci1wYXlyb2xsL2VtcGxveWVlLXRvdGFsLWRldGFpbHMvdXBkYXRlLWVtcGxveWVlL3BlcmZvcm1hbmNlLXJldmlldy9wZXJmb3JtYW5jZS1yZXZpZXcuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxjQUFlLEVBQUE7O0FBRWpCO0VBQ0UsZ0JBQWdCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9oci1wYXlyb2xsL2VtcGxveWVlLXRvdGFsLWRldGFpbHMvdXBkYXRlLWVtcGxveWVlL3BlcmZvcm1hbmNlLXJldmlldy9wZXJmb3JtYW5jZS1yZXZpZXcuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvZGVlcC8gLm1vZGFsLWRpYWxvZyB7XG4gICAgbWF4LXdpZHRoOiAyMiUgO1xuICB9XG4gIC5iYWNrIHtcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/ui-features/hr-payroll/employee-total-details/update-employee/performance-review/performance-review.component.ts":
/*!****************************************************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/hr-payroll/employee-total-details/update-employee/performance-review/performance-review.component.ts ***!
  \****************************************************************************************************************************************/
/*! exports provided: PerformanceReviewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PerformanceReviewComponent", function() { return PerformanceReviewComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _confirmation_dialog_confirmation_dialog_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../../confirmation-dialog/confirmation-dialog.service */ "./src/app/confirmation-dialog/confirmation-dialog.service.ts");
/* harmony import */ var _services_token_token_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../services/token/token.service */ "./src/app/services/token/token.service.ts");
/* harmony import */ var _services_admin_employee_details_employee_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../services/admin/employee-details/employee.service */ "./src/app/services/admin/employee-details/employee.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var PerformanceReviewComponent = /** @class */ (function () {
    function PerformanceReviewComponent(EmployeeServ, userServ, confirmationDialogService) {
        this.EmployeeServ = EmployeeServ;
        this.userServ = userServ;
        this.confirmationDialogService = confirmationDialogService;
        this.employee = {};
        this.employeeChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.moduleCreate = true;
        this.moduleEdit = true;
        this.moduleView = true;
        this.getEmployeerDetail();
    }
    /**
         Get  Employee Details
       **/
    PerformanceReviewComponent.prototype.getEmployeerDetail = function () {
        var _this = this;
        this.EmployeeServ.userEmployee
            .subscribe(function (data) {
            _this.employee = data;
            _this.id = data.id;
        }, function (error) {
            console.log(error);
        });
    };
    /**
        Edit  Employee Performance
      **/
    PerformanceReviewComponent.prototype.editEmployee = function (employeeData) {
        return __awaiter(this, void 0, void 0, function () {
            var value, resultAleart, result, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        value = employeeData.value;
                        return [4 /*yield*/, this.confirmationDialogService.confirm('Alert Message', 'Are you sure you want to Update?')];
                    case 1:
                        resultAleart = _a.sent();
                        if (!resultAleart) return [3 /*break*/, 6];
                        _a.label = 2;
                    case 2:
                        _a.trys.push([2, 4, , 5]);
                        return [4 /*yield*/, this.EmployeeServ.updateEmployee({ id: this.id }, value)];
                    case 3:
                        result = _a.sent();
                        this.EmployeeServ.userEmployee.next(result);
                        this.employeeChange.emit(result);
                        return [3 /*break*/, 5];
                    case 4:
                        error_1 = _a.sent();
                        console.log(error_1);
                        return [3 /*break*/, 5];
                    case 5: return [3 /*break*/, 6];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    PerformanceReviewComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.userServ.userAccess !== undefined) {
            this.userServ.userAccess
                .subscribe(function (data) {
                data.forEach(function (ele) {
                    if (ele.name === 'HR Payroll') {
                        ele.subMenu.forEach(function (element) {
                            if (element.name === 'Employee Status') {
                                _this.moduleCreate = element.create;
                                _this.moduleEdit = element.edit;
                                _this.moduleView = element.view;
                            }
                        });
                    }
                });
            }, function (error) {
                console.log(error);
            });
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], PerformanceReviewComponent.prototype, "employeeChange", void 0);
    PerformanceReviewComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-performance-review',
            template: __webpack_require__(/*! ./performance-review.component.html */ "./src/app/pages/ui-features/hr-payroll/employee-total-details/update-employee/performance-review/performance-review.component.html"),
            styles: [__webpack_require__(/*! ./performance-review.component.scss */ "./src/app/pages/ui-features/hr-payroll/employee-total-details/update-employee/performance-review/performance-review.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_admin_employee_details_employee_service__WEBPACK_IMPORTED_MODULE_3__["EmployeeService"],
            _services_token_token_service__WEBPACK_IMPORTED_MODULE_2__["TokenService"],
            _confirmation_dialog_confirmation_dialog_service__WEBPACK_IMPORTED_MODULE_1__["ConfirmationDialogService"]])
    ], PerformanceReviewComponent);
    return PerformanceReviewComponent;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/hr-payroll/employee-total-details/update-employee/update-employee.component.html":
/*!********************************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/hr-payroll/employee-total-details/update-employee/update-employee.component.html ***!
  \********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n      <nb-card style=\" background-color:#E1F5FE;\">\n          <ngb-tabset>\n            <ngb-tab title=\"Employee Details\">\n              <ng-template ngbTabContent>\n                  <ngx-main-details></ngx-main-details>\n              </ng-template>\n            </ngb-tab>\n            <ngb-tab title=\"Performance Review\">\n              <ng-template ngbTabContent>\n                  <ngx-performance-review></ngx-performance-review>\n              </ng-template>\n            </ngb-tab>\n            <ngb-tab title=\"Salaries\">\n              <ng-template ngbTabContent>\n                  <ngx-monthly-salary-processing></ngx-monthly-salary-processing>\n              </ng-template>\n            </ngb-tab>\n          </ngb-tabset>\n        </nb-card>"

/***/ }),

/***/ "./src/app/pages/ui-features/hr-payroll/employee-total-details/update-employee/update-employee.component.scss":
/*!********************************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/hr-payroll/employee-total-details/update-employee/update-employee.component.scss ***!
  \********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "nd-card, ngb-tabset {\n  background-color: #E1F5FE; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3ByYWthc2gvVmlwaW4vaXJvbmJpcmQvMjBGRUIyMDIwL2lyb25iaXJkLWNsaWVudHMvc3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9oci1wYXlyb2xsL2VtcGxveWVlLXRvdGFsLWRldGFpbHMvdXBkYXRlLWVtcGxveWVlL3VwZGF0ZS1lbXBsb3llZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHlCQUF3QixFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvdWktZmVhdHVyZXMvaHItcGF5cm9sbC9lbXBsb3llZS10b3RhbC1kZXRhaWxzL3VwZGF0ZS1lbXBsb3llZS91cGRhdGUtZW1wbG95ZWUuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJuZC1jYXJkLCBuZ2ItdGFic2V0e1xuICAgIGJhY2tncm91bmQtY29sb3I6I0UxRjVGRTtcbn1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/pages/ui-features/hr-payroll/employee-total-details/update-employee/update-employee.component.ts":
/*!******************************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/hr-payroll/employee-total-details/update-employee/update-employee.component.ts ***!
  \******************************************************************************************************************/
/*! exports provided: UpdateEmployeeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateEmployeeComponent", function() { return UpdateEmployeeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var UpdateEmployeeComponent = /** @class */ (function () {
    function UpdateEmployeeComponent() {
    }
    UpdateEmployeeComponent.prototype.ngOnInit = function () {
    };
    UpdateEmployeeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-update-employee',
            template: __webpack_require__(/*! ./update-employee.component.html */ "./src/app/pages/ui-features/hr-payroll/employee-total-details/update-employee/update-employee.component.html"),
            styles: [__webpack_require__(/*! ./update-employee.component.scss */ "./src/app/pages/ui-features/hr-payroll/employee-total-details/update-employee/update-employee.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], UpdateEmployeeComponent);
    return UpdateEmployeeComponent;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/hr-payroll/hr-payroll-routing.module.ts":
/*!***************************************************************************!*\
  !*** ./src/app/pages/ui-features/hr-payroll/hr-payroll-routing.module.ts ***!
  \***************************************************************************/
/*! exports provided: HrPayrollRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HrPayrollRoutingModule", function() { return HrPayrollRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _hr_payroll_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./hr-payroll.component */ "./src/app/pages/ui-features/hr-payroll/hr-payroll.component.ts");
/* harmony import */ var _employee_details_employee_details_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./employee-details/employee-details.component */ "./src/app/pages/ui-features/hr-payroll/employee-details/employee-details.component.ts");
/* harmony import */ var _employee_total_details_employee_total_details_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./employee-total-details/employee-total-details.component */ "./src/app/pages/ui-features/hr-payroll/employee-total-details/employee-total-details.component.ts");
/* harmony import */ var _salary_salary_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./salary/salary.component */ "./src/app/pages/ui-features/hr-payroll/salary/salary.component.ts");
/* harmony import */ var _attendence_attendence_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./attendence/attendence.component */ "./src/app/pages/ui-features/hr-payroll/attendence/attendence.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var routes = [{
        path: '',
        component: _hr_payroll_component__WEBPACK_IMPORTED_MODULE_2__["HrPayrollComponent"],
        children: [
            {
                path: 'EmployeeDetails',
                component: _employee_total_details_employee_total_details_component__WEBPACK_IMPORTED_MODULE_4__["EmployeeTotalDetailsComponent"],
            },
            {
                path: 'EmployeeStatus',
                component: _employee_details_employee_details_component__WEBPACK_IMPORTED_MODULE_3__["EmployeeDetailsComponent"],
            },
            {
                path: 'Salary',
                component: _salary_salary_component__WEBPACK_IMPORTED_MODULE_5__["SalaryComponent"],
            },
            {
                path: 'Attendence',
                component: _attendence_attendence_component__WEBPACK_IMPORTED_MODULE_6__["AttendenceComponent"],
            }
        ]
    }];
var HrPayrollRoutingModule = /** @class */ (function () {
    function HrPayrollRoutingModule() {
    }
    HrPayrollRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], HrPayrollRoutingModule);
    return HrPayrollRoutingModule;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/hr-payroll/hr-payroll.component.html":
/*!************************************************************************!*\
  !*** ./src/app/pages/ui-features/hr-payroll/hr-payroll.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./src/app/pages/ui-features/hr-payroll/hr-payroll.component.scss":
/*!************************************************************************!*\
  !*** ./src/app/pages/ui-features/hr-payroll/hr-payroll.component.scss ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VpLWZlYXR1cmVzL2hyLXBheXJvbGwvaHItcGF5cm9sbC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/pages/ui-features/hr-payroll/hr-payroll.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/pages/ui-features/hr-payroll/hr-payroll.component.ts ***!
  \**********************************************************************/
/*! exports provided: HrPayrollComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HrPayrollComponent", function() { return HrPayrollComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HrPayrollComponent = /** @class */ (function () {
    function HrPayrollComponent() {
    }
    HrPayrollComponent.prototype.ngOnInit = function () {
    };
    HrPayrollComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-hr-payroll',
            template: __webpack_require__(/*! ./hr-payroll.component.html */ "./src/app/pages/ui-features/hr-payroll/hr-payroll.component.html"),
            styles: [__webpack_require__(/*! ./hr-payroll.component.scss */ "./src/app/pages/ui-features/hr-payroll/hr-payroll.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], HrPayrollComponent);
    return HrPayrollComponent;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/hr-payroll/hr-payroll.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/pages/ui-features/hr-payroll/hr-payroll.module.ts ***!
  \*******************************************************************/
/*! exports provided: HrPayrollModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HrPayrollModule", function() { return HrPayrollModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ngx_echarts__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-echarts */ "./node_modules/ngx-echarts/ngx-echarts.es5.js");
/* harmony import */ var _swimlane_ngx_charts__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @swimlane/ngx-charts */ "./node_modules/@swimlane/ngx-charts/release/index.js");
/* harmony import */ var _swimlane_ngx_charts__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_swimlane_ngx_charts__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var _theme_theme_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../@theme/theme.module */ "./src/app/@theme/theme.module.ts");
/* harmony import */ var _asymmetrik_ngx_leaflet__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @asymmetrik/ngx-leaflet */ "./node_modules/@asymmetrik/ngx-leaflet/dist/index.js");
/* harmony import */ var _core_data_profit_bar_animation_chart_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../@core/data/profit-bar-animation-chart.service */ "./src/app/@core/data/profit-bar-animation-chart.service.ts");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var _node_modules_angular_custom_modal__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../../../node_modules/angular-custom-modal */ "./node_modules/angular-custom-modal/angular-custom-modal.es5.js");
/* harmony import */ var _nebular_theme__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @nebular/theme */ "./node_modules/@nebular/theme/index.js");
/* harmony import */ var _core_data_smart_table_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../../@core/data/smart-table.service */ "./src/app/@core/data/smart-table.service.ts");
/* harmony import */ var _core_data_jobslist_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../../@core/data/jobslist.service */ "./src/app/@core/data/jobslist.service.ts");
/* harmony import */ var _hr_payroll_routing_module__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./hr-payroll-routing.module */ "./src/app/pages/ui-features/hr-payroll/hr-payroll-routing.module.ts");
/* harmony import */ var _hr_payroll_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./hr-payroll.component */ "./src/app/pages/ui-features/hr-payroll/hr-payroll.component.ts");
/* harmony import */ var _employee_details_employee_details_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./employee-details/employee-details.component */ "./src/app/pages/ui-features/hr-payroll/employee-details/employee-details.component.ts");
/* harmony import */ var _employee_total_details_employee_total_details_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./employee-total-details/employee-total-details.component */ "./src/app/pages/ui-features/hr-payroll/employee-total-details/employee-total-details.component.ts");
/* harmony import */ var _employee_total_details_update_employee_update_employee_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./employee-total-details/update-employee/update-employee.component */ "./src/app/pages/ui-features/hr-payroll/employee-total-details/update-employee/update-employee.component.ts");
/* harmony import */ var _employee_total_details_update_employee_main_details_main_details_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./employee-total-details/update-employee/main-details/main-details.component */ "./src/app/pages/ui-features/hr-payroll/employee-total-details/update-employee/main-details/main-details.component.ts");
/* harmony import */ var _employee_total_details_update_employee_performance_review_performance_review_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./employee-total-details/update-employee/performance-review/performance-review.component */ "./src/app/pages/ui-features/hr-payroll/employee-total-details/update-employee/performance-review/performance-review.component.ts");
/* harmony import */ var _salary_salary_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./salary/salary.component */ "./src/app/pages/ui-features/hr-payroll/salary/salary.component.ts");
/* harmony import */ var _salary_salary_master_salary_master_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./salary/salary-master/salary-master.component */ "./src/app/pages/ui-features/hr-payroll/salary/salary-master/salary-master.component.ts");
/* harmony import */ var _salary_monthly_salary_processing_monthly_salary_processing_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./salary/monthly-salary-processing/monthly-salary-processing.component */ "./src/app/pages/ui-features/hr-payroll/salary/monthly-salary-processing/monthly-salary-processing.component.ts");
/* harmony import */ var _attendence_attendence_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./attendence/attendence.component */ "./src/app/pages/ui-features/hr-payroll/attendence/attendence.component.ts");
/* harmony import */ var _salary_monthly_salary_processing_yearly_salary_yearly_salary_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./salary/monthly-salary-processing/yearly-salary/yearly-salary.component */ "./src/app/pages/ui-features/hr-payroll/salary/monthly-salary-processing/yearly-salary/yearly-salary.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


// import { CommonModule } from '@angular/common';




// import { ChartModule } from 'angular2-chartjs';




// import { CollapsibleModule } from 'angular2-collapsible';
















var HrPayrollModule = /** @class */ (function () {
    function HrPayrollModule() {
    }
    HrPayrollModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _hr_payroll_routing_module__WEBPACK_IMPORTED_MODULE_14__["HrPayrollRoutingModule"],
                ng2_smart_table__WEBPACK_IMPORTED_MODULE_9__["Ng2SmartTableModule"],
                _theme_theme_module__WEBPACK_IMPORTED_MODULE_6__["ThemeModule"],
                _nebular_theme__WEBPACK_IMPORTED_MODULE_11__["NbStepperModule"],
                // ChartModule,
                ngx_echarts__WEBPACK_IMPORTED_MODULE_3__["NgxEchartsModule"],
                _swimlane_ngx_charts__WEBPACK_IMPORTED_MODULE_4__["NgxChartsModule"],
                _asymmetrik_ngx_leaflet__WEBPACK_IMPORTED_MODULE_7__["LeafletModule"],
                _nebular_theme__WEBPACK_IMPORTED_MODULE_11__["NbListModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _nebular_theme__WEBPACK_IMPORTED_MODULE_11__["NbProgressBarModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__["NgbModule"],
                _nebular_theme__WEBPACK_IMPORTED_MODULE_11__["NbAccordionModule"],
                // CollapsibleModule,
                _node_modules_angular_custom_modal__WEBPACK_IMPORTED_MODULE_10__["ModalModule"],
            ],
            declarations: [
                _hr_payroll_component__WEBPACK_IMPORTED_MODULE_15__["HrPayrollComponent"],
                _employee_details_employee_details_component__WEBPACK_IMPORTED_MODULE_16__["EmployeeDetailsComponent"],
                _employee_total_details_employee_total_details_component__WEBPACK_IMPORTED_MODULE_17__["EmployeeTotalDetailsComponent"],
                _employee_total_details_update_employee_update_employee_component__WEBPACK_IMPORTED_MODULE_18__["UpdateEmployeeComponent"],
                _employee_total_details_update_employee_main_details_main_details_component__WEBPACK_IMPORTED_MODULE_19__["MainDetailsComponent"],
                _employee_total_details_update_employee_performance_review_performance_review_component__WEBPACK_IMPORTED_MODULE_20__["PerformanceReviewComponent"],
                _salary_salary_component__WEBPACK_IMPORTED_MODULE_21__["SalaryComponent"],
                _salary_salary_master_salary_master_component__WEBPACK_IMPORTED_MODULE_22__["SalaryMasterComponent"],
                _salary_monthly_salary_processing_monthly_salary_processing_component__WEBPACK_IMPORTED_MODULE_23__["MonthlySalaryProcessingComponent"],
                _attendence_attendence_component__WEBPACK_IMPORTED_MODULE_24__["AttendenceComponent"], _salary_monthly_salary_processing_yearly_salary_yearly_salary_component__WEBPACK_IMPORTED_MODULE_25__["YearlySalaryComponent"],
            ],
            providers: [
                _core_data_smart_table_service__WEBPACK_IMPORTED_MODULE_12__["SmartTableService"],
                _core_data_jobslist_service__WEBPACK_IMPORTED_MODULE_13__["JobsTableService"],
                _core_data_profit_bar_animation_chart_service__WEBPACK_IMPORTED_MODULE_8__["ProfitBarAnimationChartService"],
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["DatePipe"]
            ],
        })
    ], HrPayrollModule);
    return HrPayrollModule;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/hr-payroll/salary/monthly-salary-processing/monthly-salary-processing.component.html":
/*!************************************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/hr-payroll/salary/monthly-salary-processing/monthly-salary-processing.component.html ***!
  \************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nb-card>\n  <nb-card-body>\n      <div class=\"row\">\n          <div class=\"col-5\">\n            <p class=\"back\">Employee Name: {{employee.firstName}}&nbsp;&nbsp;{{employee.lastName}}</p>\n          </div>\n        </div>\n        <h5 class=\"text-center\" >Salary Details </h5>\n      <ngx-yearly-salary></ngx-yearly-salary>\n  </nb-card-body>\n</nb-card>\n"

/***/ }),

/***/ "./src/app/pages/ui-features/hr-payroll/salary/monthly-salary-processing/monthly-salary-processing.component.scss":
/*!************************************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/hr-payroll/salary/monthly-salary-processing/monthly-salary-processing.component.scss ***!
  \************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "nb-card-body {\n  background-color: #E1F5FE;\n  color: black; }\n\ncol-xl-6, nb-card-header {\n  background: #E1F5FE;\n  color: black; }\n\n.back {\n  text-align: left; }\n\nnb-card {\n  border: 0px solid #d5dbe0; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3ByYWthc2gvVmlwaW4vaXJvbmJpcmQvMjBGRUIyMDIwL2lyb25iaXJkLWNsaWVudHMvc3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9oci1wYXlyb2xsL3NhbGFyeS9tb250aGx5LXNhbGFyeS1wcm9jZXNzaW5nL21vbnRobHktc2FsYXJ5LXByb2Nlc3NpbmcuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSx5QkFBd0I7RUFDeEIsWUFBWSxFQUFBOztBQUVkO0VBQ0csbUJBQW1CO0VBQ3JCLFlBQVksRUFBQTs7QUFLYjtFQUNFLGdCQUFnQixFQUFBOztBQUVwQjtFQUNFLHlCQUF5QixFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvdWktZmVhdHVyZXMvaHItcGF5cm9sbC9zYWxhcnkvbW9udGhseS1zYWxhcnktcHJvY2Vzc2luZy9tb250aGx5LXNhbGFyeS1wcm9jZXNzaW5nLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsibmItY2FyZC1ib2R5e1xuICAgIGJhY2tncm91bmQtY29sb3I6I0UxRjVGRTtcbiAgICBjb2xvcjogYmxhY2s7XG4gIH1cbiAgY29sLXhsLTYsIG5iLWNhcmQtaGVhZGVye1xuICAgICBiYWNrZ3JvdW5kOiAjRTFGNUZFO1xuICAgY29sb3I6IGJsYWNrO1xuICB9XG4gIC8vIC5ibGFja3tcbiAgLy8gICAgZm9udC1zaXplOiAxcmVtICFpbXBvcnRhbnQ7XG4gIC8vIH1cbiAgLmJhY2sge1xuICAgIHRleHQtYWxpZ246IGxlZnQ7XG59XG5uYi1jYXJke1xuICBib3JkZXI6IDBweCBzb2xpZCAjZDVkYmUwO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/ui-features/hr-payroll/salary/monthly-salary-processing/monthly-salary-processing.component.ts":
/*!**********************************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/hr-payroll/salary/monthly-salary-processing/monthly-salary-processing.component.ts ***!
  \**********************************************************************************************************************/
/*! exports provided: MonthlySalaryProcessingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MonthlySalaryProcessingComponent", function() { return MonthlySalaryProcessingComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _confirmation_dialog_confirmation_dialog_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../confirmation-dialog/confirmation-dialog.service */ "./src/app/confirmation-dialog/confirmation-dialog.service.ts");
/* harmony import */ var _services_admin_employee_details_employee_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../services/admin/employee-details/employee.service */ "./src/app/services/admin/employee-details/employee.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var MonthlySalaryProcessingComponent = /** @class */ (function () {
    function MonthlySalaryProcessingComponent(EmployeeServ, confirmationDialogService) {
        this.EmployeeServ = EmployeeServ;
        this.confirmationDialogService = confirmationDialogService;
        this.employee = {};
        this.employeeChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.getEmployeerDetail();
    }
    /**
         Get  Employee Performance
       **/
    MonthlySalaryProcessingComponent.prototype.getEmployeerDetail = function () {
        var _this = this;
        this.EmployeeServ.userEmployee
            .subscribe(function (data) {
            _this.employee = data;
            _this.id = data.id;
        }, function (error) {
            console.log(error);
        });
    };
    MonthlySalaryProcessingComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], MonthlySalaryProcessingComponent.prototype, "employeeChange", void 0);
    MonthlySalaryProcessingComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-monthly-salary-processing',
            template: __webpack_require__(/*! ./monthly-salary-processing.component.html */ "./src/app/pages/ui-features/hr-payroll/salary/monthly-salary-processing/monthly-salary-processing.component.html"),
            styles: [__webpack_require__(/*! ./monthly-salary-processing.component.scss */ "./src/app/pages/ui-features/hr-payroll/salary/monthly-salary-processing/monthly-salary-processing.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_admin_employee_details_employee_service__WEBPACK_IMPORTED_MODULE_2__["EmployeeService"],
            _confirmation_dialog_confirmation_dialog_service__WEBPACK_IMPORTED_MODULE_1__["ConfirmationDialogService"]])
    ], MonthlySalaryProcessingComponent);
    return MonthlySalaryProcessingComponent;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/hr-payroll/salary/monthly-salary-processing/yearly-salary/yearly-salary.component.html":
/*!**************************************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/hr-payroll/salary/monthly-salary-processing/yearly-salary/yearly-salary.component.html ***!
  \**************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nb-accordion multi>\n  <nb-accordion-item>\n    <nb-accordion-item-header>\n      2016\n    </nb-accordion-item-header>\n    <nb-accordion-item-body>\n      \n      <ng2-smart-table [settings]=\"settings\" [source]=\"source\" (deleteConfirm)=\"onDeleteConfirm($event)\"\n        (userRowSelect)=\"componentInsideModal.open()\">\n      </ng2-smart-table>\n     \n    </nb-accordion-item-body>\n  </nb-accordion-item>\n\n  <nb-accordion-item>\n    <nb-accordion-item-header>\n      2017\n    </nb-accordion-item-header>\n    <nb-accordion-item-body>\n      <ng2-smart-table [settings]=\"settings\" [source]=\"source\" (deleteConfirm)=\"onDeleteConfirm($event)\"\n        (userRowSelect)=\"componentInsideModal.open()\">\n      </ng2-smart-table>\n    </nb-accordion-item-body>\n  </nb-accordion-item>\n\n  <nb-accordion-item>\n    <nb-accordion-item-header>\n      2018\n    </nb-accordion-item-header>\n    <nb-accordion-item-body>\n      <ng2-smart-table [settings]=\"settings\" [source]=\"source\" (deleteConfirm)=\"onDeleteConfirm($event)\"\n        (userRowSelect)=\"componentInsideModal.open()\">\n      </ng2-smart-table>\n    </nb-accordion-item-body>\n  </nb-accordion-item>\n\n  <nb-accordion-item>\n    <nb-accordion-item-header>\n      2019\n    </nb-accordion-item-header>\n    <nb-accordion-item-body>\n      <p>Coming Soon....</p>\n    </nb-accordion-item-body>\n  </nb-accordion-item>\n\n</nb-accordion>\n<modal #componentInsideModal>\n  <ng-template #modalHeader>\n  </ng-template>\n  <ng-template #modalBody>\n    <div class=\"row\">\n\n      <div class=\"col-lg-12\">\n        <br>\n        <form style=\"color: #757575;\" #addEmployee=\"ngForm\" (ngSubmit)=\"editEmployee(addEmployee)\">\n          <h3>Salary Slip</h3>\n          <div class=\"form-row\">\n            <div class=\"col-sm-4\">\n              <div class=\"form-group input-group-sm\">\n                <label for=\"materialRegisterFormFirstName\">Employee Name:</label>\n                <input type=\"text\" id=\"materialRegisterFormFirstName\" name=\"firstName\" class=\"form-control\">\n                <label for=\"materialRegisterFormLastName\">Designation:</label>\n                <input type=\"Text\" id=\"materialRegisterFormLastName\" name=\"lastName\" class=\"form-control\">\n                <label for=\"materialRegisterFormLastName\">Month & Year:</label>\n                <input type=\"date\" id=\"materialRegisterFormLastName\" name=\"lastName\" class=\"form-control\">\n              </div>\n            </div>\n            <div class=\"col-sm-8\">\n              <!-- <div class=\"form-group input-group-sm\">\n                <label for=\"materialRegisterFormLastName\">Last name</label>\n                <input type=\"Text\" id=\"materialRegisterFormLastName\" name=\"lastName\" [(ngModel)]=\"employee.lastName\"\n                  class=\"form-control\">\n              </div> -->\n            </div>\n          </div>\n          <div class=\"form-row\">\n            <div class=\"col-sm-6 \">\n              <div class=\"form-group input-group-sm \">\n                <h3>Earnings</h3>\n                <label for=\"materialRegisterFormFirstName\">Basic & DA:</label>\n                <input type=\"text\" id=\"materialRegisterFormFirstName\" name=\"firstName\" class=\"form-control\">\n                <label for=\"materialRegisterFormLastName\">HRA:</label>\n                <input type=\"Text\" id=\"materialRegisterFormLastName\" name=\"lastName\" class=\"form-control\">\n                <label for=\"materialRegisterFormLastName\">Conveyance:</label>\n                <input type=\"Text\" id=\"materialRegisterFormLastName\" name=\"lastName\" class=\"form-control\">\n                <br><br><br><br><br><br>\n                <label for=\"materialRegisterFormLastName\" style=\"margin-top: 5px;\">Total Addition:</label>\n                <input type=\"Text\" id=\"materialRegisterFormLastName\" name=\"lastName\" class=\"form-control\" style=\"margin-top: 1px;\">\n              </div>\n            </div>\n\n            <div class=\"col-sm-6\">\n              <div class=\"form-group input-group-sm vl\">\n                <h3>Deductions</h3>\n                <label for=\"materialRegisterFormFirstName\">Provident Fund:</label>\n                <input type=\"text\" id=\"materialRegisterFormFirstName\" name=\"firstName\" class=\"form-control\">\n                <label for=\"materialRegisterFormLastName\">E.S.I.:</label>\n                <input type=\"Text\" id=\"materialRegisterFormLastName\" name=\"lastName\" class=\"form-control\">\n                <label for=\"materialRegisterFormLastName\">Loan:</label>\n                <input type=\"Text\" id=\"materialRegisterFormLastName\" name=\"lastName\" class=\"form-control\">\n                <label for=\"materialRegisterFormLastName\">Profession Tax:</label>\n                <input type=\"Text\" id=\"materialRegisterFormLastName\" name=\"lastName\" class=\"form-control\">\n                <label for=\"materialRegisterFormLastName\">TSD/IT:</label>\n                <input type=\"Text\" id=\"materialRegisterFormLastName\" name=\"lastName\" class=\"form-control\">\n                <label for=\"materialRegisterFormLastName\">Total Deduction:</label>\n                <input type=\"Text\" id=\"materialRegisterFormLastName\" name=\"lastName\" class=\"form-control\">\n              </div>\n            </div>\n          </div>\n          <div class=\"form-row\">\n            <div class=\"col-sm-12\">\n              <label for=\"materialRegisterFormFirstName\">Net Salary:</label>\n              <input type=\"text\" id=\"materialRegisterFormFirstName\" name=\"firstName\" class=\"form-control\">\n            </div>\n          </div>\n          <br>\n          <div class=\"form-row\">\n            <div class=\"col-sm-6\">\n              <div class=\"form-group input-group-sm \">\n                <label for=\"materialRegisterFormFirstName\">Transition Id:</label>\n                <input type=\"text\" id=\"materialRegisterFormFirstName\" name=\"firstName\" class=\"form-control\">\n                <label for=\"materialRegisterFormFirstName\">Date:</label>\n                <input type=\"text\" id=\"materialRegisterFormFirstName\" name=\"firstName\" class=\"form-control\">\n              </div>\n            </div>\n            <div class=\"col-sm-6\">\n              <div class=\"form-group input-group-sm \">\n                <label for=\"materialRegisterFormFirstName\">Name of Bank:</label>\n                <input type=\"text\" id=\"materialRegisterFormFirstName\" name=\"firstName\" class=\"form-control\">\n              </div>\n            </div>\n          </div>\n          <button class=\"btn btn-outline-primary \" shape=\"semi-round\" type=\"submit\" style=\"float:right\">Save</button>\n          <!-- Sign up button -->\n          <!-- <button class=\"btn btn-outline-primary \" shape=\"semi-round\" style=\"margin: 48px;\"  (click)=\"show = !show\" >Submit</button> -->\n\n\n        </form>\n        <!-- Form -->\n\n      </div>\n    </div>\n    <br>\n  </ng-template>\n  <ng-template #modalFooter></ng-template>\n</modal>\n"

/***/ }),

/***/ "./src/app/pages/ui-features/hr-payroll/salary/monthly-salary-processing/yearly-salary/yearly-salary.component.scss":
/*!**************************************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/hr-payroll/salary/monthly-salary-processing/yearly-salary/yearly-salary.component.scss ***!
  \**************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "h3 {\n  text-align: center; }\n\n.vl {\n  border-left: 1px solid;\n  height: auto;\n  position: initial;\n  left: 50%;\n  margin-left: 5px;\n  top: 0;\n  padding-left: 15px; }\n\ninput {\n  border: none !important;\n  border-bottom: 1px solid black !important;\n  padding-bottom: 0px !important; }\n\nlabel {\n  padding-top: 5px !important; }\n\n/deep/ nb-accordion-item.expanded nb-accordion-item-body .ng-trigger-accordionItemBody {\n  height: auto !important; }\n\n/deep/ nb-accordion-item nb-accordion-item-header {\n  background-color: black !important;\n  color: white !important; }\n\n:host /deep/ table tbody tr:nth-child(even) {\n  background: #e9eaea !important; }\n\n:host /deep/ table tbody tr:nth-child(odd) {\n  background: white !important; }\n\nnb-card {\n  border: 0px solid #d5dbe0; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3ByYWthc2gvVmlwaW4vaXJvbmJpcmQvMjBGRUIyMDIwL2lyb25iaXJkLWNsaWVudHMvc3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9oci1wYXlyb2xsL3NhbGFyeS9tb250aGx5LXNhbGFyeS1wcm9jZXNzaW5nL3llYXJseS1zYWxhcnkveWVhcmx5LXNhbGFyeS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFjQTtFQUNFLGtCQUFrQixFQUFBOztBQUdwQjtFQUNFLHNCQUFzQjtFQUN0QixZQUFZO0VBQ1osaUJBQWlCO0VBQ2pCLFNBQVM7RUFDVCxnQkFBZ0I7RUFDaEIsTUFBTTtFQUNOLGtCQUFrQixFQUFBOztBQUdwQjtFQUNFLHVCQUF1QjtFQUN2Qix5Q0FBeUM7RUFDekMsOEJBQThCLEVBQUE7O0FBR2hDO0VBQ0UsMkJBQTJCLEVBQUE7O0FBRzdCO0VBRUksdUJBQXVCLEVBQUE7O0FBSTNCO0VBQ0Usa0NBQWtDO0VBQ2xDLHVCQUNGLEVBQUE7O0FBR0E7RUFDRSw4QkFBNEIsRUFBQTs7QUFFNUI7RUFDQSw0QkFBNEIsRUFBQTs7QUFFNUI7RUFDRSx5QkFBeUIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VpLWZlYXR1cmVzL2hyLXBheXJvbGwvc2FsYXJ5L21vbnRobHktc2FsYXJ5LXByb2Nlc3NpbmcveWVhcmx5LXNhbGFyeS95ZWFybHktc2FsYXJ5LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gIGNvbGxhcHNpYmxlLWhlYWRlcntcbi8vICAgICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFja1xuLy8gfVxuXG4vLyBjb2xsYXBzaWJsZS1oZWFkZXJ7XG4vLyAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4vLyAgICBmb250LWRpc3BsYXk6IGJvbGQ7XG4vLyAgICBmb250LXdlaWdodDogOTAwO1xuLy8gICAgZm9udC1zaXplOiAycmVtO1xuLy8gICAgY29sb3I6d2hpdGU7XG4vLyB9XG4vLyBjb2xsYXBzaWJsZS1saXN0e1xuLy8gICAgIGNvbG9yOmJsYWNrO1xuLy8gfVxuaDMge1xuICB0ZXh0LWFsaWduOiBjZW50ZXIsXG59XG5cbi52bCB7XG4gIGJvcmRlci1sZWZ0OiAxcHggc29saWQ7XG4gIGhlaWdodDogYXV0bztcbiAgcG9zaXRpb246IGluaXRpYWw7XG4gIGxlZnQ6IDUwJTtcbiAgbWFyZ2luLWxlZnQ6IDVweDtcbiAgdG9wOiAwO1xuICBwYWRkaW5nLWxlZnQ6IDE1cHg7XG59XG5cbmlucHV0IHtcbiAgYm9yZGVyOiBub25lICFpbXBvcnRhbnQ7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBibGFjayAhaW1wb3J0YW50O1xuICBwYWRkaW5nLWJvdHRvbTogMHB4ICFpbXBvcnRhbnQ7XG59XG5cbmxhYmVsIHtcbiAgcGFkZGluZy10b3A6IDVweCAhaW1wb3J0YW50O1xufVxuXG4vZGVlcC8gbmItYWNjb3JkaW9uLWl0ZW0uZXhwYW5kZWQgbmItYWNjb3JkaW9uLWl0ZW0tYm9keSB7XG4gIC5uZy10cmlnZ2VyLWFjY29yZGlvbkl0ZW1Cb2R5IHtcbiAgICBoZWlnaHQ6IGF1dG8gIWltcG9ydGFudDtcbiAgfVxufVxuXG4vZGVlcC8gbmItYWNjb3JkaW9uLWl0ZW0gbmItYWNjb3JkaW9uLWl0ZW0taGVhZGVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2sgIWltcG9ydGFudDtcbiAgY29sb3I6IHdoaXRlICFpbXBvcnRhbnRcbn1cblxuXG46aG9zdCAvZGVlcC8gdGFibGUgdGJvZHkgdHI6bnRoLWNoaWxkKGV2ZW4pIHtcbiAgYmFja2dyb3VuZDojZTllYWVhIWltcG9ydGFudDtcbiAgfVxuICA6aG9zdCAvZGVlcC8gdGFibGUgdGJvZHkgdHI6bnRoLWNoaWxkKG9kZCkge1xuICBiYWNrZ3JvdW5kOiAgd2hpdGUhaW1wb3J0YW50O1xuICB9XG4gIG5iLWNhcmR7XG4gICAgYm9yZGVyOiAwcHggc29saWQgI2Q1ZGJlMDtcbiAgfSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/ui-features/hr-payroll/salary/monthly-salary-processing/yearly-salary/yearly-salary.component.ts":
/*!************************************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/hr-payroll/salary/monthly-salary-processing/yearly-salary/yearly-salary.component.ts ***!
  \************************************************************************************************************************/
/*! exports provided: YearlySalaryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "YearlySalaryComponent", function() { return YearlySalaryComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var _core_data_salaries__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../@core/data/salaries */ "./src/app/@core/data/salaries.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var YearlySalaryComponent = /** @class */ (function () {
    function YearlySalaryComponent(service) {
        this.service = service;
        this.settings = {
            add: {
                addButtonContent: '<i class="nb-plus"></i>',
                createButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
            },
            edit: {
                editButtonContent: '<i class="nb-edit"></i>',
                saveButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
            },
            delete: {
                deleteButtonContent: '<i class="nb-trash"></i>',
                confirmDelete: true,
            },
            actions: {
                delete: false,
                add: false,
                edit: false,
            },
            columns: {
                id: {
                    title: 'SI NO',
                    type: 'number',
                },
                month: {
                    title: 'Month',
                    type: 'string',
                },
                salary: {
                    title: 'Salary',
                    type: 'string',
                },
            },
        };
        this.source = new ng2_smart_table__WEBPACK_IMPORTED_MODULE_1__["LocalDataSource"]();
        var data = this.service.getData();
        this.source.load(data);
    }
    YearlySalaryComponent.prototype.onDeleteConfirm = function (event) {
        if (window.confirm('Are you sure you want to delete?')) {
            event.confirm.resolve();
        }
        else {
            event.confirm.reject();
        }
    };
    YearlySalaryComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-yearly-salary',
            template: __webpack_require__(/*! ./yearly-salary.component.html */ "./src/app/pages/ui-features/hr-payroll/salary/monthly-salary-processing/yearly-salary/yearly-salary.component.html"),
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectionStrategy"].OnPush,
            styles: [__webpack_require__(/*! ./yearly-salary.component.scss */ "./src/app/pages/ui-features/hr-payroll/salary/monthly-salary-processing/yearly-salary/yearly-salary.component.scss")]
        }),
        __metadata("design:paramtypes", [_core_data_salaries__WEBPACK_IMPORTED_MODULE_2__["salariesService"]])
    ], YearlySalaryComponent);
    return YearlySalaryComponent;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/hr-payroll/salary/salary-master/salary-master.component.html":
/*!************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/hr-payroll/salary/salary-master/salary-master.component.html ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nb-card>\n    <nb-card-header>\n    Employee Salary Details \n    </nb-card-header>\n  \n    <nb-card-body>\n      <ng2-smart-table [settings]=\"settings\" [source]=\"source\" (deleteConfirm)=\"onDeleteConfirm($event)\">\n      </ng2-smart-table>\n  </nb-card-body>\n  </nb-card>"

/***/ }),

/***/ "./src/app/pages/ui-features/hr-payroll/salary/salary-master/salary-master.component.scss":
/*!************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/hr-payroll/salary/salary-master/salary-master.component.scss ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ng2-smart-table ui li {\n  cursor: pointer; }\n\nnb-card-body {\n  background-color: #E1F5FE;\n  color: white; }\n\ncol-xl-6, nb-card-header {\n  background: #1D8ECE;\n  color: white; }\n\n:host /deep/ table tbody tr:nth-child(even) {\n  background: #e9eaea !important; }\n\n:host /deep/ table tbody tr:nth-child(odd) {\n  background: white !important; }\n\nnb-card {\n  border: 0px solid #d5dbe0; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3ByYWthc2gvVmlwaW4vaXJvbmJpcmQvMjBGRUIyMDIwL2lyb25iaXJkLWNsaWVudHMvc3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9oci1wYXlyb2xsL3NhbGFyeS9zYWxhcnktbWFzdGVyL3NhbGFyeS1tYXN0ZXIuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxlQUFlLEVBQUE7O0FBRW5CO0VBQ0kseUJBQXdCO0VBQ3hCLFlBQVcsRUFBQTs7QUFFYjtFQUNFLG1CQUFtQjtFQUNuQixZQUFZLEVBQUE7O0FBRWQ7RUFDRSw4QkFBNEIsRUFBQTs7QUFFNUI7RUFDQSw0QkFBNEIsRUFBQTs7QUFFNUI7RUFDRSx5QkFBeUIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VpLWZlYXR1cmVzL2hyLXBheXJvbGwvc2FsYXJ5L3NhbGFyeS1tYXN0ZXIvc2FsYXJ5LW1hc3Rlci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIm5nMi1zbWFydC10YWJsZSB1aSBsaXtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG59XG5uYi1jYXJkLWJvZHl7XG4gICAgYmFja2dyb3VuZC1jb2xvcjojRTFGNUZFO1xuICAgIGNvbG9yOndoaXRlO1xuICB9XG4gIGNvbC14bC02LCBuYi1jYXJkLWhlYWRlcntcbiAgICBiYWNrZ3JvdW5kOiAjMUQ4RUNFO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgfVxuICA6aG9zdCAvZGVlcC8gdGFibGUgdGJvZHkgdHI6bnRoLWNoaWxkKGV2ZW4pIHtcbiAgICBiYWNrZ3JvdW5kOiNlOWVhZWEhaW1wb3J0YW50O1xuICAgIH1cbiAgICA6aG9zdCAvZGVlcC8gdGFibGUgdGJvZHkgdHI6bnRoLWNoaWxkKG9kZCkge1xuICAgIGJhY2tncm91bmQ6ICB3aGl0ZSFpbXBvcnRhbnQ7XG4gICAgfVxuICAgIG5iLWNhcmR7XG4gICAgICBib3JkZXI6IDBweCBzb2xpZCAjZDVkYmUwO1xuICAgIH0iXX0= */"

/***/ }),

/***/ "./src/app/pages/ui-features/hr-payroll/salary/salary-master/salary-master.component.ts":
/*!**********************************************************************************************!*\
  !*** ./src/app/pages/ui-features/hr-payroll/salary/salary-master/salary-master.component.ts ***!
  \**********************************************************************************************/
/*! exports provided: SalaryMasterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SalaryMasterComponent", function() { return SalaryMasterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var _services_admin_user_management_user_management_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../services/admin/user-management/user-management.service */ "./src/app/services/admin/user-management/user-management.service.ts");
/* harmony import */ var _services_admin_employee_details_employee_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../services/admin/employee-details/employee.service */ "./src/app/services/admin/employee-details/employee.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var SalaryMasterComponent = /** @class */ (function () {
    function SalaryMasterComponent(userManagementServ, employeeServ) {
        this.userManagementServ = userManagementServ;
        this.employeeServ = employeeServ;
        this.creatShow = false;
        this.tableShow = false;
        this.settings = {
            add: {
                addButtonContent: '<i class="nb-plus"></i>',
                createButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
            },
            edit: {
                editButtonContent: '<i class="nb-edit"></i>',
                saveButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
            },
            delete: {
                deleteButtonContent: '<i class="nb-trash"></i>',
                confirmDelete: true,
            },
            actions: {
                delete: false,
                add: false,
                edit: false,
            },
            columns: {
                S_NO: {
                    title: 'Employee Number',
                    type: 'number',
                    editable: false,
                },
                name: {
                    title: 'Employee Name',
                    type: 'string',
                },
                deportment: {
                    title: 'Department',
                    type: 'string',
                },
                position: {
                    title: 'Designation',
                    type: 'string',
                },
            },
        };
        this.source = new ng2_smart_table__WEBPACK_IMPORTED_MODULE_1__["LocalDataSource"]();
    }
    SalaryMasterComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.employeeServ.userEmployee
            .subscribe(function (data) {
            _this.getuserManagementInfo();
        }, function (error) {
            console.log(error);
        });
        this.getuserManagementInfo();
    };
    SalaryMasterComponent.prototype.showFun = function () {
        this.creatShow = true;
        this.tableShow = false;
        this.employeeServ.userEmployee.next({ creatShow: true });
    };
    /**
     * Get Details of Employee
     */
    SalaryMasterComponent.prototype.getuserManagementInfo = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, error_1;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        _a = this;
                        return [4 /*yield*/, this.employeeServ.getEmployee()];
                    case 1:
                        _a.listOfUserManagement = _b.sent();
                        // console.log(this.listOfUserManagement)
                        this.listOfUserManagement.reverse();
                        this.listOfUserManagement.map(function (data, index) { return data.S_NO = 'IBE/BNG/EMP' + index + 1; });
                        this.listOfUserManagement.map(function (data) { return data.name = data.firstName + "  " + data.lastName; });
                        this.source.load(this.listOfUserManagement);
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _b.sent();
                        console.error(error_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Create New Details of Employee
     */
    SalaryMasterComponent.prototype.onCreateConfirm = function (event) {
        return __awaiter(this, void 0, void 0, function () {
            var newData;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!window.confirm('Are you sure you want to create?')) return [3 /*break*/, 2];
                        newData = event.newData;
                        //  console.log(event.S_NO)
                        newData.isActive = true;
                        return [4 /*yield*/, this.userManagementServ.createUserManagement(newData)];
                    case 1:
                        _a.sent();
                        this.getuserManagementInfo();
                        event.confirm.resolve();
                        return [3 /*break*/, 3];
                    case 2:
                        event.confirm.reject();
                        _a.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Selected Row Edit Details of Employee
     */
    SalaryMasterComponent.prototype.onSaveConfirm = function (event) {
        return __awaiter(this, void 0, void 0, function () {
            var editData, result, error_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!window.confirm('Are you sure you want to save?')) return [3 /*break*/, 5];
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        editData = event.newData;
                        return [4 /*yield*/, this.userManagementServ.updateUserManagement(editData)];
                    case 2:
                        result = _a.sent();
                        // result.S_NO = S_NO;
                        // this.listOfUserManagement.splice((S_NO - 1), 1, result);
                        this.source.load(this.listOfUserManagement);
                        event.confirm.resolve();
                        return [3 /*break*/, 4];
                    case 3:
                        error_2 = _a.sent();
                        console.log(error_2);
                        return [3 /*break*/, 4];
                    case 4: return [3 /*break*/, 6];
                    case 5:
                        event.confirm.reject();
                        _a.label = 6;
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * User Selected Row
     */
    SalaryMasterComponent.prototype.onUserRowSelect = function (event) {
        event.data.creatShow = false;
        this.employeeServ.userEmployee.next(event.data);
        if (event) {
            this.creatShow = false;
            return this.tableShow = true;
        }
        this.creatShow = true;
        this.tableShow = false;
    };
    /**
    * Delete Details of Employee
    */
    SalaryMasterComponent.prototype.onDeleteConfirm = function (event) {
        return __awaiter(this, void 0, void 0, function () {
            var editData;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!window.confirm('Are you sure you want to delete?')) return [3 /*break*/, 2];
                        editData = event.data;
                        return [4 /*yield*/, this.userManagementServ.removeUserManagement(editData)];
                    case 1:
                        _a.sent();
                        this.listOfUserManagement.splice((event.data.S_NO - 1), 1);
                        this.listOfUserManagement.reverse();
                        this.listOfUserManagement.map(function (data, index) { return data.S_NO = 'IBE/BNG/EMP' + index + 1; });
                        this.listOfUserManagement.map(function (data) { return data.name = data.firstName + "  " + data.lastName; });
                        this.source.load(this.listOfUserManagement);
                        event.confirm.resolve();
                        return [3 /*break*/, 3];
                    case 2:
                        event.confirm.reject();
                        _a.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    SalaryMasterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-salary-master',
            template: __webpack_require__(/*! ./salary-master.component.html */ "./src/app/pages/ui-features/hr-payroll/salary/salary-master/salary-master.component.html"),
            styles: [__webpack_require__(/*! ./salary-master.component.scss */ "./src/app/pages/ui-features/hr-payroll/salary/salary-master/salary-master.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_admin_user_management_user_management_service__WEBPACK_IMPORTED_MODULE_2__["UserManagementService"],
            _services_admin_employee_details_employee_service__WEBPACK_IMPORTED_MODULE_3__["EmployeeService"]])
    ], SalaryMasterComponent);
    return SalaryMasterComponent;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/hr-payroll/salary/salary.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/pages/ui-features/hr-payroll/salary/salary.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n    <div class=\"col-xl-5\">\n       <ngx-salary-master (click)=\"showFun('data')\"></ngx-salary-master>\n       <!-- <button class=\"btn btn-success btn-rounded \" (click)=\"showFun('create')\" >Create New Jobs</button> -->\n      </div>\n    <div class=\"col-xl-7\" *ngIf=\"tableShow\">\n<ngx-monthly-salary-processing></ngx-monthly-salary-processing>\n    </div>\n    \n  </div>\n "

/***/ }),

/***/ "./src/app/pages/ui-features/hr-payroll/salary/salary.component.scss":
/*!***************************************************************************!*\
  !*** ./src/app/pages/ui-features/hr-payroll/salary/salary.component.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VpLWZlYXR1cmVzL2hyLXBheXJvbGwvc2FsYXJ5L3NhbGFyeS5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/pages/ui-features/hr-payroll/salary/salary.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/ui-features/hr-payroll/salary/salary.component.ts ***!
  \*************************************************************************/
/*! exports provided: SalaryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SalaryComponent", function() { return SalaryComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var SalaryComponent = /** @class */ (function () {
    function SalaryComponent() {
    }
    SalaryComponent.prototype.showFun = function (data) {
        if (data === 'data') {
            this.tableShow = true;
            return this.creatShow = false;
        }
        this.tableShow = false;
        this.creatShow = true;
    };
    SalaryComponent.prototype.ngOnInit = function () {
    };
    SalaryComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-salary',
            template: __webpack_require__(/*! ./salary.component.html */ "./src/app/pages/ui-features/hr-payroll/salary/salary.component.html"),
            styles: [__webpack_require__(/*! ./salary.component.scss */ "./src/app/pages/ui-features/hr-payroll/salary/salary.component.scss")]
        })
    ], SalaryComponent);
    return SalaryComponent;
}());



/***/ })

}]);
//# sourceMappingURL=default~app-pages-pages-module~hr-payroll-hr-payroll-module~ui-features-ui-features-module.js.map