(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["marketing-leads-marketing-leads-module"],{

/***/ "./src/app/services/admin/roles.service.ts":
/*!*************************************************!*\
  !*** ./src/app/services/admin/roles.service.ts ***!
  \*************************************************/
/*! exports provided: RolesService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RolesService", function() { return RolesService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _settings_settings_services_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../settings/settings-services.service */ "./src/app/services/settings/settings-services.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var RolesService = /** @class */ (function () {
    function RolesService(http, settings) {
        this.http = http;
        this.settings = settings;
        this.userRoles = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]({});
        this.API_URL = this.settings.BASE_API_URL;
    }
    RolesService.prototype.createRoles = function (data) {
        var _this = this;
        var url = this.API_URL + "/admin/roles";
        return new Promise(function (resolve, reject) {
            _this.http.post(url, data)
                .subscribe(resolve, reject);
        });
    };
    RolesService.prototype.getRoles = function () {
        var _this = this;
        var url = this.API_URL + "/admin/roles";
        return new Promise(function (resolve, reject) {
            _this.http.get(url)
                .subscribe(resolve, reject);
        });
    };
    RolesService.prototype.updateRoles = function (value, data) {
        var _this = this;
        var url = this.API_URL + "/admin/roles/" + value;
        return new Promise(function (resolve, reject) {
            _this.http.put(url, data)
                .subscribe(resolve, reject);
        });
    };
    RolesService.prototype.getRolesDetails = function (id) {
        var _this = this;
        var url = this.API_URL + "/admin/roles/" + id;
        //  console.log(url,"link")
        return new Promise(function (resolve, reject) {
            _this.http.get(url)
                .subscribe(resolve, reject);
        });
    };
    RolesService.prototype.removeRoles = function (value) {
        var _this = this;
        var id = value.id;
        var url = this.API_URL + "/admin/roles/" + id;
        return new Promise(function (resolve, reject) {
            _this.http.delete(url)
                .subscribe(resolve, reject);
        });
    };
    RolesService.prototype.eventFiles = function (data) {
        var _this = this;
        var url = this.API_URL + "/admin/images";
        return new Promise(function (resolve, reject) {
            _this.http.post(url, data)
                .subscribe(resolve, reject);
        });
    };
    RolesService.prototype.geteventFiles = function () {
        var _this = this;
        var url = this.API_URL + "/admin/images";
        return new Promise(function (resolve, reject) {
            _this.http.get(url)
                .subscribe(resolve, reject);
        });
    };
    RolesService.prototype.deletefiles = function (data) {
        var _this = this;
        var url = this.API_URL + "/admin/images/" + data;
        return new Promise(function (resolve, reject) {
            _this.http.delete(url)
                .subscribe(resolve, reject);
        });
    };
    RolesService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _settings_settings_services_service__WEBPACK_IMPORTED_MODULE_3__["SettingsServicesService"]])
    ], RolesService);
    return RolesService;
}());



/***/ })

}]);
//# sourceMappingURL=marketing-leads-marketing-leads-module.js.map