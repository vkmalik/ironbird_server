(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~amc-amc-module~app-pages-pages-module~ui-features-ui-features-module"],{

/***/ "./node_modules/ng2-order-pipe/dist/index.js":
/*!***************************************************!*\
  !*** ./node_modules/ng2-order-pipe/dist/index.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
/**
 * Created by vadimdez on 20/01/2017.
 */
__export(__webpack_require__(/*! ./src/ng2-order.module */ "./node_modules/ng2-order-pipe/dist/src/ng2-order.module.js"));
__export(__webpack_require__(/*! ./src/ng2-order.pipe */ "./node_modules/ng2-order-pipe/dist/src/ng2-order.pipe.js"));
//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/ng2-order-pipe/dist/src/ng2-order.module.js":
/*!******************************************************************!*\
  !*** ./node_modules/ng2-order-pipe/dist/src/ng2-order.module.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
/**
 * Created by vadimdez on 20/01/2017.
 */
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var ng2_order_pipe_1 = __webpack_require__(/*! ./ng2-order.pipe */ "./node_modules/ng2-order-pipe/dist/src/ng2-order.pipe.js");
var Ng2OrderModule = (function () {
    function Ng2OrderModule() {
    }
    return Ng2OrderModule;
}());
Ng2OrderModule = __decorate([
    core_1.NgModule({
        declarations: [ng2_order_pipe_1.Ng2OrderPipe],
        exports: [ng2_order_pipe_1.Ng2OrderPipe]
    })
], Ng2OrderModule);
exports.Ng2OrderModule = Ng2OrderModule;
//# sourceMappingURL=ng2-order.module.js.map

/***/ }),

/***/ "./node_modules/ng2-order-pipe/dist/src/ng2-order.pipe.js":
/*!****************************************************************!*\
  !*** ./node_modules/ng2-order-pipe/dist/src/ng2-order.pipe.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var Ng2OrderPipe = Ng2OrderPipe_1 = (function () {
    function Ng2OrderPipe() {
    }
    Ng2OrderPipe.prototype.transform = function (value, expression, reverse) {
        if (!value) {
            return value;
        }
        var isArray = value instanceof Array;
        if (isArray) {
            return this.sortArray(value, expression, reverse);
        }
        if (typeof value === 'object') {
            return this.transformObject(value, expression, reverse);
        }
        return value;
    };
    /**
     * Sort array
     *
     * @param value
     * @param expression
     * @param reverse
     * @returns {any[]}
     */
    Ng2OrderPipe.prototype.sortArray = function (value, expression, reverse) {
        var array = value.sort(function (a, b) {
            if (!expression) {
                return a > b ? 1 : -1;
            }
            return a[expression] > b[expression] ? 1 : -1;
        });
        if (reverse) {
            return array.reverse();
        }
        return array;
    };
    /**
     * Transform Object
     *
     * @param value
     * @param expression
     * @param reverse
     * @returns {any[]}
     */
    Ng2OrderPipe.prototype.transformObject = function (value, expression, reverse) {
        var parsedExpression = Ng2OrderPipe_1.parseExpression(expression);
        var lastPredicate = parsedExpression.pop();
        var oldValue = Ng2OrderPipe_1.getValue(value, parsedExpression);
        if (!(oldValue instanceof Array)) {
            parsedExpression.push(lastPredicate);
            lastPredicate = null;
            oldValue = Ng2OrderPipe_1.getValue(value, parsedExpression);
        }
        if (!oldValue) {
            return value;
        }
        var newValue = this.transform(oldValue, lastPredicate, reverse);
        Ng2OrderPipe_1.setValue(value, newValue, parsedExpression);
        return value;
    };
    /**
     * Parse expression, split into items
     * @param expression
     * @returns {string[]}
     */
    Ng2OrderPipe.parseExpression = function (expression) {
        expression = expression.replace(/\[(\w+)\]/g, '.$1');
        expression = expression.replace(/^\./, '');
        return expression.split('.');
    };
    /**
     * Get value by expression
     *
     * @param object
     * @param expression
     * @returns {any}
     */
    Ng2OrderPipe.getValue = function (object, expression) {
        for (var i = 0, n = expression.length; i < n; ++i) {
            var k = expression[i];
            if (!(k in object)) {
                return;
            }
            object = object[k];
        }
        return object;
    };
    /**
     * Set value by expression
     *
     * @param object
     * @param value
     * @param expression
     */
    Ng2OrderPipe.setValue = function (object, value, expression) {
        var i;
        for (i = 0; i < expression.length - 1; i++) {
            object = object[expression[i]];
        }
        object[expression[i]] = value;
    };
    return Ng2OrderPipe;
}());
Ng2OrderPipe = Ng2OrderPipe_1 = __decorate([
    core_1.Pipe({
        name: 'orderBy'
    })
], Ng2OrderPipe);
exports.Ng2OrderPipe = Ng2OrderPipe;
var Ng2OrderPipe_1;
//# sourceMappingURL=ng2-order.pipe.js.map

/***/ }),

/***/ "./node_modules/ng2-search-filter/ng2-search-filter.es5.js":
/*!*****************************************************************!*\
  !*** ./node_modules/ng2-search-filter/ng2-search-filter.es5.js ***!
  \*****************************************************************/
/*! exports provided: Ng2SearchPipeModule, Ng2SearchPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Ng2SearchPipeModule", function() { return Ng2SearchPipeModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Ng2SearchPipe", function() { return Ng2SearchPipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");

var Ng2SearchPipe = (function () {
    function Ng2SearchPipe() {
    }
    /**
     * @param {?} items object from array
     * @param {?} term term's search
     * @return {?}
     */
    Ng2SearchPipe.prototype.transform = function (items, term) {
        if (!term || !items)
            return items;
        return Ng2SearchPipe.filter(items, term);
    };
    /**
     *
     * @param {?} items List of items to filter
     * @param {?} term  a string term to compare with every property of the list
     *
     * @return {?}
     */
    Ng2SearchPipe.filter = function (items, term) {
        var /** @type {?} */ toCompare = term.toLowerCase();
        /**
         * @param {?} item
         * @param {?} term
         * @return {?}
         */
        function checkInside(item, term) {
            for (var /** @type {?} */ property in item) {
                if (item[property] === null || item[property] == undefined) {
                    continue;
                }
                if (typeof item[property] === 'object') {
                    if (checkInside(item[property], term)) {
                        return true;
                    }
                }
                if (item[property].toString().toLowerCase().includes(toCompare)) {
                    return true;
                }
            }
            return false;
        }
        return items.filter(function (item) {
            return checkInside(item, term);
        });
    };
    return Ng2SearchPipe;
}());
Ng2SearchPipe.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"], args: [{
                name: 'filter',
                pure: false
            },] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"] },
];
/**
 * @nocollapse
 */
Ng2SearchPipe.ctorParameters = function () { return []; };
var Ng2SearchPipeModule = (function () {
    function Ng2SearchPipeModule() {
    }
    return Ng2SearchPipeModule;
}());
Ng2SearchPipeModule.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"], args: [{
                declarations: [Ng2SearchPipe],
                exports: [Ng2SearchPipe]
            },] },
];
/**
 * @nocollapse
 */
Ng2SearchPipeModule.ctorParameters = function () { return []; };
/**
 * Generated bundle index. Do not edit.
 */

//# sourceMappingURL=ng2-search-filter.es5.js.map


/***/ }),

/***/ "./node_modules/ngx-pagination/dist/ngx-pagination.js":
/*!************************************************************!*\
  !*** ./node_modules/ngx-pagination/dist/ngx-pagination.js ***!
  \************************************************************/
/*! exports provided: ɵb, ɵa, NgxPaginationModule, PaginationService, PaginationControlsComponent, PaginationControlsDirective, PaginatePipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵb", function() { return DEFAULT_STYLES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵa", function() { return DEFAULT_TEMPLATE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgxPaginationModule", function() { return NgxPaginationModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaginationService", function() { return PaginationService; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaginationControlsComponent", function() { return PaginationControlsComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaginationControlsDirective", function() { return PaginationControlsDirective; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaginatePipe", function() { return PaginatePipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");



var PaginationService = /** @class */ (function () {
    function PaginationService() {
        this.change = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.instances = {};
        this.DEFAULT_ID = 'DEFAULT_PAGINATION_ID';
    }
    PaginationService.prototype.defaultId = function () { return this.DEFAULT_ID; };
    /**
     * Register a PaginationInstance with this service. Returns a
     * boolean value signifying whether the instance is new or
     * updated (true = new or updated, false = unchanged).
     */
    PaginationService.prototype.register = function (instance) {
        if (instance.id == null) {
            instance.id = this.DEFAULT_ID;
        }
        if (!this.instances[instance.id]) {
            this.instances[instance.id] = instance;
            return true;
        }
        else {
            return this.updateInstance(instance);
        }
    };
    /**
     * Check each property of the instance and update any that have changed. Return
     * true if any changes were made, else return false.
     */
    PaginationService.prototype.updateInstance = function (instance) {
        var changed = false;
        for (var prop in this.instances[instance.id]) {
            if (instance[prop] !== this.instances[instance.id][prop]) {
                this.instances[instance.id][prop] = instance[prop];
                changed = true;
            }
        }
        return changed;
    };
    /**
     * Returns the current page number.
     */
    PaginationService.prototype.getCurrentPage = function (id) {
        if (this.instances[id]) {
            return this.instances[id].currentPage;
        }
    };
    /**
     * Sets the current page number.
     */
    PaginationService.prototype.setCurrentPage = function (id, page) {
        if (this.instances[id]) {
            var instance = this.instances[id];
            var maxPage = Math.ceil(instance.totalItems / instance.itemsPerPage);
            if (page <= maxPage && 1 <= page) {
                this.instances[id].currentPage = page;
                this.change.emit(id);
            }
        }
    };
    /**
     * Sets the value of instance.totalItems
     */
    PaginationService.prototype.setTotalItems = function (id, totalItems) {
        if (this.instances[id] && 0 <= totalItems) {
            this.instances[id].totalItems = totalItems;
            this.change.emit(id);
        }
    };
    /**
     * Sets the value of instance.itemsPerPage.
     */
    PaginationService.prototype.setItemsPerPage = function (id, itemsPerPage) {
        if (this.instances[id]) {
            this.instances[id].itemsPerPage = itemsPerPage;
            this.change.emit(id);
        }
    };
    /**
     * Returns a clone of the pagination instance object matching the id. If no
     * id specified, returns the instance corresponding to the default id.
     */
    PaginationService.prototype.getInstance = function (id) {
        if (id === void 0) { id = this.DEFAULT_ID; }
        if (this.instances[id]) {
            return this.clone(this.instances[id]);
        }
        return {};
    };
    /**
     * Perform a shallow clone of an object.
     */
    PaginationService.prototype.clone = function (obj) {
        var target = {};
        for (var i in obj) {
            if (obj.hasOwnProperty(i)) {
                target[i] = obj[i];
            }
        }
        return target;
    };
    return PaginationService;
}());

var __decorate$1 = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var LARGE_NUMBER = Number.MAX_SAFE_INTEGER;
var PaginatePipe = /** @class */ (function () {
    function PaginatePipe(service) {
        this.service = service;
        // store the values from the last time the pipe was invoked
        this.state = {};
    }
    PaginatePipe.prototype.transform = function (collection, args) {
        // When an observable is passed through the AsyncPipe, it will output
        // `null` until the subscription resolves. In this case, we want to
        // use the cached data from the `state` object to prevent the NgFor
        // from flashing empty until the real values arrive.
        if (!(collection instanceof Array)) {
            var _id = args.id || this.service.defaultId();
            if (this.state[_id]) {
                return this.state[_id].slice;
            }
            else {
                return collection;
            }
        }
        var serverSideMode = args.totalItems && args.totalItems !== collection.length;
        var instance = this.createInstance(collection, args);
        var id = instance.id;
        var start, end;
        var perPage = instance.itemsPerPage;
        var emitChange = this.service.register(instance);
        if (!serverSideMode && collection instanceof Array) {
            perPage = +perPage || LARGE_NUMBER;
            start = (instance.currentPage - 1) * perPage;
            end = start + perPage;
            var isIdentical = this.stateIsIdentical(id, collection, start, end);
            if (isIdentical) {
                return this.state[id].slice;
            }
            else {
                var slice = collection.slice(start, end);
                this.saveState(id, collection, slice, start, end);
                this.service.change.emit(id);
                return slice;
            }
        }
        else {
            if (emitChange) {
                this.service.change.emit(id);
            }
            // save the state for server-side collection to avoid null
            // flash as new data loads.
            this.saveState(id, collection, collection, start, end);
            return collection;
        }
    };
    /**
     * Create an PaginationInstance object, using defaults for any optional properties not supplied.
     */
    PaginatePipe.prototype.createInstance = function (collection, config) {
        this.checkConfig(config);
        return {
            id: config.id != null ? config.id : this.service.defaultId(),
            itemsPerPage: +config.itemsPerPage || 0,
            currentPage: +config.currentPage || 1,
            totalItems: +config.totalItems || collection.length
        };
    };
    /**
     * Ensure the argument passed to the filter contains the required properties.
     */
    PaginatePipe.prototype.checkConfig = function (config) {
        var required = ['itemsPerPage', 'currentPage'];
        var missing = required.filter(function (prop) { return !(prop in config); });
        if (0 < missing.length) {
            throw new Error("PaginatePipe: Argument is missing the following required properties: " + missing.join(', '));
        }
    };
    /**
     * To avoid returning a brand new array each time the pipe is run, we store the state of the sliced
     * array for a given id. This means that the next time the pipe is run on this collection & id, we just
     * need to check that the collection, start and end points are all identical, and if so, return the
     * last sliced array.
     */
    PaginatePipe.prototype.saveState = function (id, collection, slice, start, end) {
        this.state[id] = {
            collection: collection,
            size: collection.length,
            slice: slice,
            start: start,
            end: end
        };
    };
    /**
     * For a given id, returns true if the collection, size, start and end values are identical.
     */
    PaginatePipe.prototype.stateIsIdentical = function (id, collection, start, end) {
        var state = this.state[id];
        if (!state) {
            return false;
        }
        var isMetaDataIdentical = state.size === collection.length &&
            state.start === start &&
            state.end === end;
        if (!isMetaDataIdentical) {
            return false;
        }
        return state.slice.every(function (element, index) { return element === collection[start + index]; });
    };
    PaginatePipe = __decorate$1([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({
            name: 'paginate',
            pure: false
        }),
        __metadata("design:paramtypes", [PaginationService])
    ], PaginatePipe);
    return PaginatePipe;
}());

/**
 * The default template and styles for the pagination links are borrowed directly
 * from Zurb Foundation 6: http://foundation.zurb.com/sites/docs/pagination.html
 */
var DEFAULT_TEMPLATE = "\n    <pagination-template  #p=\"paginationApi\"\n                         [id]=\"id\"\n                         [maxSize]=\"maxSize\"\n                         (pageChange)=\"pageChange.emit($event)\"\n                         (pageBoundsCorrection)=\"pageBoundsCorrection.emit($event)\">\n    <ul class=\"ngx-pagination\" \n        role=\"navigation\" \n        [attr.aria-label]=\"screenReaderPaginationLabel\" \n        [class.responsive]=\"responsive\"\n        *ngIf=\"!(autoHide && p.pages.length <= 1)\">\n\n        <li class=\"pagination-previous\" [class.disabled]=\"p.isFirstPage()\" *ngIf=\"directionLinks\"> \n            <a tabindex=\"0\" *ngIf=\"1 < p.getCurrent()\" (keyup.enter)=\"p.previous()\" (click)=\"p.previous()\" [attr.aria-label]=\"previousLabel + ' ' + screenReaderPageLabel\">\n                {{ previousLabel }} <span class=\"show-for-sr\">{{ screenReaderPageLabel }}</span>\n            </a>\n            <span *ngIf=\"p.isFirstPage()\">\n                {{ previousLabel }} <span class=\"show-for-sr\">{{ screenReaderPageLabel }}</span>\n            </span>\n        </li> \n\n        <li class=\"small-screen\">\n            {{ p.getCurrent() }} / {{ p.getLastPage() }}\n        </li>\n\n        <li [class.current]=\"p.getCurrent() === page.value\" \n            [class.ellipsis]=\"page.label === '...'\"\n            *ngFor=\"let page of p.pages\">\n            <a tabindex=\"0\" (keyup.enter)=\"p.setCurrent(page.value)\" (click)=\"p.setCurrent(page.value)\" *ngIf=\"p.getCurrent() !== page.value\">\n                <span class=\"show-for-sr\">{{ screenReaderPageLabel }} </span>\n                <span>{{ (page.label === '...') ? page.label : (page.label | number:'') }}</span>\n            </a>\n            <ng-container *ngIf=\"p.getCurrent() === page.value\">\n                <span class=\"show-for-sr\">{{ screenReaderCurrentLabel }} </span>\n                <span>{{ (page.label === '...') ? page.label : (page.label | number:'') }}</span> \n            </ng-container>\n        </li>\n\n        <li class=\"pagination-next\" [class.disabled]=\"p.isLastPage()\" *ngIf=\"directionLinks\">\n            <a tabindex=\"0\" *ngIf=\"!p.isLastPage()\" (keyup.enter)=\"p.next()\" (click)=\"p.next()\" [attr.aria-label]=\"nextLabel + ' ' + screenReaderPageLabel\">\n                 {{ nextLabel }} <span class=\"show-for-sr\">{{ screenReaderPageLabel }}</span>\n            </a>\n            <span *ngIf=\"p.isLastPage()\">\n                 {{ nextLabel }} <span class=\"show-for-sr\">{{ screenReaderPageLabel }}</span>\n            </span>\n        </li>\n\n    </ul>\n    </pagination-template>\n    ";
var DEFAULT_STYLES = "\n.ngx-pagination {\n  margin-left: 0;\n  margin-bottom: 1rem; }\n  .ngx-pagination::before, .ngx-pagination::after {\n    content: ' ';\n    display: table; }\n  .ngx-pagination::after {\n    clear: both; }\n  .ngx-pagination li {\n    -moz-user-select: none;\n    -webkit-user-select: none;\n    -ms-user-select: none;\n    margin-right: 0.0625rem;\n    border-radius: 0; }\n  .ngx-pagination li {\n    display: inline-block; }\n  .ngx-pagination a,\n  .ngx-pagination button {\n    color: #0a0a0a; \n    display: block;\n    padding: 0.1875rem 0.625rem;\n    border-radius: 0; }\n    .ngx-pagination a:hover,\n    .ngx-pagination button:hover {\n      background: #e6e6e6; }\n  .ngx-pagination .current {\n    padding: 0.1875rem 0.625rem;\n    background: #2199e8;\n    color: #fefefe;\n    cursor: default; }\n  .ngx-pagination .disabled {\n    padding: 0.1875rem 0.625rem;\n    color: #cacaca;\n    cursor: default; } \n    .ngx-pagination .disabled:hover {\n      background: transparent; }\n  .ngx-pagination a, .ngx-pagination button {\n    cursor: pointer; }\n\n.ngx-pagination .pagination-previous a::before,\n.ngx-pagination .pagination-previous.disabled::before { \n  content: '\u00AB';\n  display: inline-block;\n  margin-right: 0.5rem; }\n\n.ngx-pagination .pagination-next a::after,\n.ngx-pagination .pagination-next.disabled::after {\n  content: '\u00BB';\n  display: inline-block;\n  margin-left: 0.5rem; }\n\n.ngx-pagination .show-for-sr {\n  position: absolute !important;\n  width: 1px;\n  height: 1px;\n  overflow: hidden;\n  clip: rect(0, 0, 0, 0); }\n.ngx-pagination .small-screen {\n  display: none; }\n@media screen and (max-width: 601px) {\n  .ngx-pagination.responsive .small-screen {\n    display: inline-block; } \n  .ngx-pagination.responsive li:not(.small-screen):not(.pagination-previous):not(.pagination-next) {\n    display: none; }\n}\n  ";

var __decorate$2 = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata$1 = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
function coerceToBoolean(input) {
    return !!input && input !== 'false';
}
/**
 * The default pagination controls component. Actually just a default implementation of a custom template.
 */
var PaginationControlsComponent = /** @class */ (function () {
    function PaginationControlsComponent() {
        this.maxSize = 7;
        this.previousLabel = 'Previous';
        this.nextLabel = 'Next';
        this.screenReaderPaginationLabel = 'Pagination';
        this.screenReaderPageLabel = 'page';
        this.screenReaderCurrentLabel = "You're on page";
        this.pageChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.pageBoundsCorrection = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this._directionLinks = true;
        this._autoHide = false;
        this._responsive = false;
    }
    Object.defineProperty(PaginationControlsComponent.prototype, "directionLinks", {
        get: function () {
            return this._directionLinks;
        },
        set: function (value) {
            this._directionLinks = coerceToBoolean(value);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PaginationControlsComponent.prototype, "autoHide", {
        get: function () {
            return this._autoHide;
        },
        set: function (value) {
            this._autoHide = coerceToBoolean(value);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PaginationControlsComponent.prototype, "responsive", {
        get: function () {
            return this._responsive;
        },
        set: function (value) {
            this._responsive = coerceToBoolean(value);
        },
        enumerable: true,
        configurable: true
    });
    __decorate$2([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata$1("design:type", String)
    ], PaginationControlsComponent.prototype, "id", void 0);
    __decorate$2([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata$1("design:type", Number)
    ], PaginationControlsComponent.prototype, "maxSize", void 0);
    __decorate$2([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata$1("design:type", Boolean),
        __metadata$1("design:paramtypes", [Boolean])
    ], PaginationControlsComponent.prototype, "directionLinks", null);
    __decorate$2([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata$1("design:type", Boolean),
        __metadata$1("design:paramtypes", [Boolean])
    ], PaginationControlsComponent.prototype, "autoHide", null);
    __decorate$2([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata$1("design:type", Boolean),
        __metadata$1("design:paramtypes", [Boolean])
    ], PaginationControlsComponent.prototype, "responsive", null);
    __decorate$2([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata$1("design:type", String)
    ], PaginationControlsComponent.prototype, "previousLabel", void 0);
    __decorate$2([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata$1("design:type", String)
    ], PaginationControlsComponent.prototype, "nextLabel", void 0);
    __decorate$2([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata$1("design:type", String)
    ], PaginationControlsComponent.prototype, "screenReaderPaginationLabel", void 0);
    __decorate$2([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata$1("design:type", String)
    ], PaginationControlsComponent.prototype, "screenReaderPageLabel", void 0);
    __decorate$2([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata$1("design:type", String)
    ], PaginationControlsComponent.prototype, "screenReaderCurrentLabel", void 0);
    __decorate$2([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata$1("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"])
    ], PaginationControlsComponent.prototype, "pageChange", void 0);
    __decorate$2([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata$1("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"])
    ], PaginationControlsComponent.prototype, "pageBoundsCorrection", void 0);
    PaginationControlsComponent = __decorate$2([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'pagination-controls',
            template: DEFAULT_TEMPLATE,
            styles: [DEFAULT_STYLES],
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectionStrategy"].OnPush,
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewEncapsulation"].None
        })
    ], PaginationControlsComponent);
    return PaginationControlsComponent;
}());

var __decorate$3 = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata$2 = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * This directive is what powers all pagination controls components, including the default one.
 * It exposes an API which is hooked up to the PaginationService to keep the PaginatePipe in sync
 * with the pagination controls.
 */
var PaginationControlsDirective = /** @class */ (function () {
    function PaginationControlsDirective(service, changeDetectorRef) {
        var _this = this;
        this.service = service;
        this.changeDetectorRef = changeDetectorRef;
        this.maxSize = 7;
        this.pageChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.pageBoundsCorrection = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.pages = [];
        this.changeSub = this.service.change
            .subscribe(function (id) {
            if (_this.id === id) {
                _this.updatePageLinks();
                _this.changeDetectorRef.markForCheck();
                _this.changeDetectorRef.detectChanges();
            }
        });
    }
    PaginationControlsDirective.prototype.ngOnInit = function () {
        if (this.id === undefined) {
            this.id = this.service.defaultId();
        }
        this.updatePageLinks();
    };
    PaginationControlsDirective.prototype.ngOnChanges = function (changes) {
        this.updatePageLinks();
    };
    PaginationControlsDirective.prototype.ngOnDestroy = function () {
        this.changeSub.unsubscribe();
    };
    /**
     * Go to the previous page
     */
    PaginationControlsDirective.prototype.previous = function () {
        this.checkValidId();
        this.setCurrent(this.getCurrent() - 1);
    };
    /**
     * Go to the next page
     */
    PaginationControlsDirective.prototype.next = function () {
        this.checkValidId();
        this.setCurrent(this.getCurrent() + 1);
    };
    /**
     * Returns true if current page is first page
     */
    PaginationControlsDirective.prototype.isFirstPage = function () {
        return this.getCurrent() === 1;
    };
    /**
     * Returns true if current page is last page
     */
    PaginationControlsDirective.prototype.isLastPage = function () {
        return this.getLastPage() === this.getCurrent();
    };
    /**
     * Set the current page number.
     */
    PaginationControlsDirective.prototype.setCurrent = function (page) {
        this.pageChange.emit(page);
    };
    /**
     * Get the current page number.
     */
    PaginationControlsDirective.prototype.getCurrent = function () {
        return this.service.getCurrentPage(this.id);
    };
    /**
     * Returns the last page number
     */
    PaginationControlsDirective.prototype.getLastPage = function () {
        var inst = this.service.getInstance(this.id);
        if (inst.totalItems < 1) {
            // when there are 0 or fewer (an error case) items, there are no "pages" as such,
            // but it makes sense to consider a single, empty page as the last page.
            return 1;
        }
        return Math.ceil(inst.totalItems / inst.itemsPerPage);
    };
    PaginationControlsDirective.prototype.getTotalItems = function () {
        return this.service.getInstance(this.id).totalItems;
    };
    PaginationControlsDirective.prototype.checkValidId = function () {
        if (this.service.getInstance(this.id).id == null) {
            console.warn("PaginationControlsDirective: the specified id \"" + this.id + "\" does not match any registered PaginationInstance");
        }
    };
    /**
     * Updates the page links and checks that the current page is valid. Should run whenever the
     * PaginationService.change stream emits a value matching the current ID, or when any of the
     * input values changes.
     */
    PaginationControlsDirective.prototype.updatePageLinks = function () {
        var _this = this;
        var inst = this.service.getInstance(this.id);
        var correctedCurrentPage = this.outOfBoundCorrection(inst);
        if (correctedCurrentPage !== inst.currentPage) {
            setTimeout(function () {
                _this.pageBoundsCorrection.emit(correctedCurrentPage);
                _this.pages = _this.createPageArray(inst.currentPage, inst.itemsPerPage, inst.totalItems, _this.maxSize);
            });
        }
        else {
            this.pages = this.createPageArray(inst.currentPage, inst.itemsPerPage, inst.totalItems, this.maxSize);
        }
    };
    /**
     * Checks that the instance.currentPage property is within bounds for the current page range.
     * If not, return a correct value for currentPage, or the current value if OK.
     */
    PaginationControlsDirective.prototype.outOfBoundCorrection = function (instance) {
        var totalPages = Math.ceil(instance.totalItems / instance.itemsPerPage);
        if (totalPages < instance.currentPage && 0 < totalPages) {
            return totalPages;
        }
        else if (instance.currentPage < 1) {
            return 1;
        }
        return instance.currentPage;
    };
    /**
     * Returns an array of Page objects to use in the pagination controls.
     */
    PaginationControlsDirective.prototype.createPageArray = function (currentPage, itemsPerPage, totalItems, paginationRange) {
        // paginationRange could be a string if passed from attribute, so cast to number.
        paginationRange = +paginationRange;
        var pages = [];
        var totalPages = Math.ceil(totalItems / itemsPerPage);
        var halfWay = Math.ceil(paginationRange / 2);
        var isStart = currentPage <= halfWay;
        var isEnd = totalPages - halfWay < currentPage;
        var isMiddle = !isStart && !isEnd;
        var ellipsesNeeded = paginationRange < totalPages;
        var i = 1;
        while (i <= totalPages && i <= paginationRange) {
            var label = void 0;
            var pageNumber = this.calculatePageNumber(i, currentPage, paginationRange, totalPages);
            var openingEllipsesNeeded = (i === 2 && (isMiddle || isEnd));
            var closingEllipsesNeeded = (i === paginationRange - 1 && (isMiddle || isStart));
            if (ellipsesNeeded && (openingEllipsesNeeded || closingEllipsesNeeded)) {
                label = '...';
            }
            else {
                label = pageNumber;
            }
            pages.push({
                label: label,
                value: pageNumber
            });
            i++;
        }
        return pages;
    };
    /**
     * Given the position in the sequence of pagination links [i],
     * figure out what page number corresponds to that position.
     */
    PaginationControlsDirective.prototype.calculatePageNumber = function (i, currentPage, paginationRange, totalPages) {
        var halfWay = Math.ceil(paginationRange / 2);
        if (i === paginationRange) {
            return totalPages;
        }
        else if (i === 1) {
            return i;
        }
        else if (paginationRange < totalPages) {
            if (totalPages - halfWay < currentPage) {
                return totalPages - paginationRange + i;
            }
            else if (halfWay < currentPage) {
                return currentPage - halfWay + i;
            }
            else {
                return i;
            }
        }
        else {
            return i;
        }
    };
    __decorate$3([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata$2("design:type", String)
    ], PaginationControlsDirective.prototype, "id", void 0);
    __decorate$3([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata$2("design:type", Number)
    ], PaginationControlsDirective.prototype, "maxSize", void 0);
    __decorate$3([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata$2("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"])
    ], PaginationControlsDirective.prototype, "pageChange", void 0);
    __decorate$3([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata$2("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"])
    ], PaginationControlsDirective.prototype, "pageBoundsCorrection", void 0);
    PaginationControlsDirective = __decorate$3([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
            selector: 'pagination-template,[pagination-template]',
            exportAs: 'paginationApi'
        }),
        __metadata$2("design:paramtypes", [PaginationService,
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]])
    ], PaginationControlsDirective);
    return PaginationControlsDirective;
}());

var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var NgxPaginationModule = /** @class */ (function () {
    function NgxPaginationModule() {
    }
    NgxPaginationModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"]],
            declarations: [
                PaginatePipe,
                PaginationControlsComponent,
                PaginationControlsDirective
            ],
            providers: [PaginationService],
            exports: [PaginatePipe, PaginationControlsComponent, PaginationControlsDirective]
        })
    ], NgxPaginationModule);
    return NgxPaginationModule;
}());

/**
 * Generated bundle index. Do not edit.
 */




/***/ }),

/***/ "./src/app/pages/ui-features/amc/amc-maintenance-report/amc-maintenance-report.component.html":
/*!****************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/amc/amc-maintenance-report/amc-maintenance-report.component.html ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  amc-maintenance-report works!\n</p>\n"

/***/ }),

/***/ "./src/app/pages/ui-features/amc/amc-maintenance-report/amc-maintenance-report.component.scss":
/*!****************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/amc/amc-maintenance-report/amc-maintenance-report.component.scss ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VpLWZlYXR1cmVzL2FtYy9hbWMtbWFpbnRlbmFuY2UtcmVwb3J0L2FtYy1tYWludGVuYW5jZS1yZXBvcnQuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/pages/ui-features/amc/amc-maintenance-report/amc-maintenance-report.component.ts":
/*!**************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/amc/amc-maintenance-report/amc-maintenance-report.component.ts ***!
  \**************************************************************************************************/
/*! exports provided: AmcMaintenanceReportComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AmcMaintenanceReportComponent", function() { return AmcMaintenanceReportComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AmcMaintenanceReportComponent = /** @class */ (function () {
    function AmcMaintenanceReportComponent() {
    }
    AmcMaintenanceReportComponent.prototype.ngOnInit = function () {
    };
    AmcMaintenanceReportComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-amc-maintenance-report',
            template: __webpack_require__(/*! ./amc-maintenance-report.component.html */ "./src/app/pages/ui-features/amc/amc-maintenance-report/amc-maintenance-report.component.html"),
            styles: [__webpack_require__(/*! ./amc-maintenance-report.component.scss */ "./src/app/pages/ui-features/amc/amc-maintenance-report/amc-maintenance-report.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], AmcMaintenanceReportComponent);
    return AmcMaintenanceReportComponent;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/amc/amc-renewal/amc-renewal.component.html":
/*!******************************************************************************!*\
  !*** ./src/app/pages/ui-features/amc/amc-renewal/amc-renewal.component.html ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nb-card>\n    <nb-card-header>\n        AMC Renewal/Reminder\n    </nb-card-header>\n    <nb-card-body>\n        <div class=\"container\">\n            <div class=\"row\">\n                <div class=\"col-3\" style=\"margin-bottom: 2%;\" *ngIf='listOfAmc.length !== 0'>\n                    <input placeholder=\"Search\" style=\"float: right;\" class=\"form-control\" type=\"text\" name=\"search\" [(ngModel)]=\"filter\">\n                </div>\n                <div class=\"col-4\" style=\"margin-bottom: 2%;display: flex;\">\n                    <label style=\"width: 100%;margin-left: 25%;font-weight: 600;\">Expiry Within</label><input class=\"form-control\" type=\"date\" (change)='updateExpiry($event.target.value)' [(ngModel)]=\"expiry\">\n                </div>\n                <div class=\"col-5\" style=\"margin-bottom: 2%;\" *ngIf='listOfAmc.length !== 0'>\n                    <button style=\"float: right;\" class=\"btn btn-outline-primary\" (click)='print()' shape=\"semi-round\">Send Reminder</button>\n                    <button style=\"float: right;margin-right: 2%;\" class=\"btn btn-outline-primary\" (click)='print()' shape=\"semi-round\">Send Renewal</button>\n                </div>\n                <div class=\"col-12\" style=\"margin-bottom: 2%;margin-bottom: 2%;text-align: center;\" *ngIf='listOfAmc.length === 0'>\n                    <label style=\"font-weight: 600;font-size: xx-large;\">No Data Found</label>\n                </div>\n                <div class=\"col-12\" style=\"margin-bottom: 2%;margin-bottom: 2%;\" *ngIf='listOfAmc.length !== 0'>\n                    <table class=\"table table-bordered\">\n                        <thead>\n                            <tr style=\"text-align: center;\">\n                                <th>Amc No</th>\n                                <th>Site</th>\n                                <th>Amc End Date</th>\n                                <th>Amount</th>\n                                <th>Renewal From</th>\n                                <th>Renewal To</th>\n                                <th>Renewal</th>\n                                <th>Reminder</th>\n                                <th>Edit</th>\n                            </tr>\n                        </thead>\n                        <tbody>\n                            <tr *ngFor=\"let list of listOfAmc | filter:filter  | paginate: { itemsPerPage: 10, currentPage: p }; let i = index\">\n                                <td>{{list.amcid}}</td>\n                                <td>{{list.siteName}}</td>\n                                <td style=\"text-align: center;\">{{list.toData.split('-').reverse().join('/')}}</td>\n                                <td style=\"text-align: right;\">{{list.amcAmount}}</td>\n                                <td style=\"text-align: center;\">{{list.fromData.split('-').reverse().join('/')}}</td>\n                                <td style=\"text-align: center;\">{{list.toData.split('-').reverse().join('/')}}</td>\n                                <td style=\"text-align: center;\"><input type=\"checkbox\" [(ngModel)]=\"list.renewal\"></td>\n                                <td style=\"text-align: center;\"><input type=\"checkbox\" [(ngModel)]=\"list.reminder\"></td>\n                                <td style=\"text-align: center;\"><i class=\"fa fa-edit\" style=\"cursor: pointer;\" (click)='edit(list.id,amcDetails)' title=\"Edit Details\"></i></td>\n                            </tr>\n                        </tbody>\n                    </table>\n                </div>\n                <div class=\"col-12\" *ngIf='listOfAmc.length !== 0'>\n                    <pagination-controls style=\"float: right;\" (pageChange)=\"p = $event\"></pagination-controls>\n                </div>\n            </div>\n        </div>\n    </nb-card-body>\n</nb-card>\n\n<ng-template #amcDetails let-modal>\n    <div class=\"modal-header\">\n        <h4 class=\"modal-title\">Edit AMC Details</h4>\n    </div>\n    <div class=\"modal-body\">\n        <div class=\"row\">\n            <div class=\"col-3\">\n                <label style=\"font-weight: 600;\">AMC No</label><br>\n                <label style=\"background-color: #c7c5c5;font-size: x-large;\">{{amNO}}</label>\n            </div>\n            <div class=\"col-3\">\n                <label style=\"font-weight: 600;\">AMC Amount</label>\n                <input type=\"text\" class=\"form-control\" [(ngModel)]='amAmt' (blur)='changeAmt($event.target.value)'>\n            </div>\n            <div class=\"col-3\">\n                <label style=\"font-weight: 600;\">AMC From Date</label>\n                <input type=\"date\" class=\"form-control\" [(ngModel)]='amFrom'>\n            </div>\n            <div class=\"col-3\">\n                <label style=\"font-weight: 600;\">AMC To Date</label>\n                <input type=\"date\" class=\"form-control\" [(ngModel)]='amTo'>\n            </div>\n        </div>\n    </div>\n    <div class=\"modal-footer\">\n        <div class=\"d-flex flex-row-reverse\">\n            <div class=\"p-3\">\n                <button class=\"btn btn-outline-primary\" type=\"button\" (click)='updateDetails()'>Update</button>\n            </div>\n            <div class=\"p-3\">\n                <button class=\"btn btn-outline-primary\" type=\"button\" (click)='close()'>Cancel</button>\n            </div>\n        </div>\n    </div>\n</ng-template>"

/***/ }),

/***/ "./src/app/pages/ui-features/amc/amc-renewal/amc-renewal.component.scss":
/*!******************************************************************************!*\
  !*** ./src/app/pages/ui-features/amc/amc-renewal/amc-renewal.component.scss ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "nb-card-header {\n  background: #1D8ECE;\n  color: white; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3ByYWthc2gvVmlwaW4vaXJvbmJpcmQvMjBGRUIyMDIwL2lyb25iaXJkLWNsaWVudHMvc3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9hbWMvYW1jLXJlbmV3YWwvYW1jLXJlbmV3YWwuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxtQkFBbUI7RUFDbkIsWUFBWSxFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvdWktZmVhdHVyZXMvYW1jL2FtYy1yZW5ld2FsL2FtYy1yZW5ld2FsLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsibmItY2FyZC1oZWFkZXIge1xuICAgIGJhY2tncm91bmQ6ICMxRDhFQ0U7XG4gICAgY29sb3I6IHdoaXRlO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/ui-features/amc/amc-renewal/amc-renewal.component.ts":
/*!****************************************************************************!*\
  !*** ./src/app/pages/ui-features/amc/amc-renewal/amc-renewal.component.ts ***!
  \****************************************************************************/
/*! exports provided: AmcRenewalComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AmcRenewalComponent", function() { return AmcRenewalComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! pdfmake/build/pdfmake */ "./node_modules/pdfmake/build/pdfmake.js");
/* harmony import */ var pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! pdfmake/build/vfs_fonts */ "./node_modules/pdfmake/build/vfs_fonts.js");
/* harmony import */ var pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _confirmation_dialog_confirmation_dialog_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../confirmation-dialog/confirmation-dialog.service */ "./src/app/confirmation-dialog/confirmation-dialog.service.ts");
/* harmony import */ var _services_currency_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../services/currency.service */ "./src/app/services/currency.service.ts");
/* harmony import */ var _services_image_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../services/image.service */ "./src/app/services/image.service.ts");
/* harmony import */ var _services_AMC_amc_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../services/AMC/amc.service */ "./src/app/services/AMC/amc.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_2___default.a.vfs = pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_3___default.a.pdfMake.vfs;
var AmcRenewalComponent = /** @class */ (function () {
    function AmcRenewalComponent(confirmationDialogService, img, modalService, AmcServ, curr) {
        this.confirmationDialogService = confirmationDialogService;
        this.img = img;
        this.modalService = modalService;
        this.AmcServ = AmcServ;
        this.curr = curr;
        this.listOfAmc = [];
        this.list = [];
        this.p = 1;
        this.options = false;
    }
    AmcRenewalComponent.prototype.ngOnInit = function () {
        this.currentDate = new Date().toISOString().substring(0, 10);
        var now = new Date(); //Current date
        this.currentYear = now.getFullYear(); //Current year
        this.nextYear = now.getFullYear() + 1; //next year
        var nextWeek = this.addDays(now, 7); // Add 7 days
        this.expiry = nextWeek.toISOString().substring(0, 10);
        this.getAMCDetails();
    };
    AmcRenewalComponent.prototype.addYear = function (e) {
        var data = e;
        var day1 = data.getDate();
        if (day1 < 10) {
            day1 = '0' + day1;
        }
        var day = day1.toString();
        var month1 = data.getMonth() + 1;
        if (month1 < 10) {
            month1 = '0' + month1;
        }
        var month = month1.toString();
        var fullYear = data.getFullYear() + 1;
        var fullYear1 = fullYear.toString();
        return fullYear + "-" + month + "-" + day;
    };
    AmcRenewalComponent.prototype.addDays = function (dateObj, numDays) {
        dateObj.setDate(dateObj.getDate() + numDays);
        return dateObj;
    };
    AmcRenewalComponent.prototype.updateExpiry = function (e) {
        var _this = this;
        this.listOfAmc = [];
        this.expiry = e;
        this.list.forEach(function (ele) {
            if (ele.toData < _this.expiry) {
                Object.assign(ele, { 'renewal': false, 'reminder': false });
                _this.listOfAmc.push(ele);
            }
        });
    };
    AmcRenewalComponent.prototype.getAMCDetails = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, error_1;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        _a = this;
                        return [4 /*yield*/, this.AmcServ.getAllAmc()];
                    case 1:
                        _a.list = _b.sent();
                        this.list.reverse();
                        this.list.forEach(function (ele) {
                            if (ele.toData < _this.expiry) {
                                Object.assign(ele, { 'renewal': false, 'reminder': false });
                                _this.listOfAmc.push(ele);
                            }
                        });
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _b.sent();
                        console.error(error_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AmcRenewalComponent.prototype.print = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, resultAleart;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.img.image];
                    case 1:
                        _a.image = _b.sent();
                        return [4 /*yield*/, this.confirmationDialogService.confirm('Alert Message', 'Do You Want To Download With Logo')];
                    case 2:
                        resultAleart = _b.sent();
                        if (resultAleart) {
                            this.options = true;
                        }
                        else {
                            this.options = false;
                        }
                        this.capturefullScreen();
                        return [2 /*return*/];
                }
            });
        });
    };
    AmcRenewalComponent.prototype.capturefullScreen = function () {
        var _this = this;
        this.listOfAmc.forEach(function (ele) {
            if (ele.renewal) {
                var documentDefinition = _this.getRenewal(ele, _this.options);
                pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_2___default.a.createPdf(documentDefinition).download(ele.customerName + "_Renewal.pdf");
                Object.assign(ele, { 'renewal': false });
            }
            else if (ele.reminder) {
                var documentDefinition = _this.getReminder(ele, _this.options);
                pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_2___default.a.createPdf(documentDefinition).download(ele.customerName + "_Reminder.pdf");
                Object.assign(ele, { 'reminder': false });
            }
        });
    };
    AmcRenewalComponent.prototype.getReminder = function (e, options) {
        var testImageDataUrl = this.image;
        var nextTo = this.addYear(new Date(e.toData));
        var nextFrom = this.addDays(new Date(e.toData), 1).toISOString().substring(0, 10);
        return {
            pageMargins: [20, 115, 20, 70],
            pageSize: 'A4',
            footer: function () {
                if (options) {
                    return [
                        [
                            { canvas: [{ type: 'line', x1: 0, y1: 0, x2: 595, y2: 0, lineWidth: 0.5 }] },
                            {
                                stack: [
                                    {
                                        columns: [
                                            {
                                                text: '#21, 4th MAIN, BALAJI LAYOUT, NEAR BADRAPPA LAYOUT, SANJAY NAGAR POST, BANGALORE-560094.',
                                                width: '*',
                                                alignment: 'center',
                                                fontSize: 8,
                                                margin: [0, 15, 0, 0]
                                            }
                                        ]
                                    },
                                    {
                                        columns: [
                                            {
                                                text: 'Mail: ironbirdelevators@gmail.com, Landline: 080-23511445.',
                                                width: '*',
                                                alignment: 'center',
                                                fontSize: 8
                                            }
                                        ]
                                    }
                                ]
                            }
                        ],
                    ];
                }
                else {
                    return;
                }
            },
            header: function () {
                // you can apply any logic and return any valid pdfmake element
                if (options) {
                    return [
                        {
                            image: testImageDataUrl,
                            width: 600,
                            height: 100
                        }
                    ];
                }
                else {
                    return;
                }
            },
            content: [
                // Header
                {
                    columns: [
                        [
                            {
                                text: 'REMINDER LETTER',
                                style: 'invoiceTitle',
                                width: '*'
                            },
                            {
                                stack: [
                                    {
                                        columns: [
                                            {
                                                text: '',
                                                width: 400,
                                            },
                                            {
                                                text: 'Date',
                                                width: '*',
                                                fontSize: 10
                                            },
                                            {
                                                text: "" + this.currentDate.split('-').reverse().join('/'),
                                                width: '*',
                                                fontSize: 10
                                            },
                                        ]
                                    },
                                    {
                                        columns: [
                                            {
                                                text: '',
                                                width: 400,
                                            },
                                            {
                                                text: 'Job No',
                                                fontSize: 10,
                                                width: '*',
                                            },
                                            {
                                                text: "" + e.amcid,
                                                fontSize: 10,
                                                width: '*',
                                            },
                                        ]
                                    }
                                ]
                            }
                        ],
                    ],
                },
                {
                    text: 'To,',
                    fontSize: 10
                },
                {
                    text: "" + e.customerName,
                    bold: true,
                    width: 150,
                    fontSize: 10
                },
                {
                    text: "" + e.clientAddress,
                    bold: true,
                    fontSize: 10,
                    width: 110
                },
                {
                    text: 'Dear Sir/Madam,',
                    bold: true,
                    margin: [0, 15, 0, 5],
                    fontSize: 10
                },
                {
                    text: "Sub: Renewal of Maintenance contract " + this.currentYear + "-" + this.nextYear,
                    bold: true,
                    margin: [0, 5, 0, 5],
                    fontSize: 10
                },
                {
                    text: "The maintenance period for your elevator is getting over next month (i.e. " + e.fromData.split('-').reverse().join('/') + " to " + e.toData.split('-').reverse().join('/') + ")",
                    bold: true,
                    margin: [0, 5, 0, 5],
                    fontSize: 10,
                    alignment: 'justify'
                },
                {
                    text: [
                        "Now that the maintenance contract has to be renewed for the further continuation of uninterrupted service from our end.",
                        { text: "The maintenance period for the lift will start from " + nextFrom.split('-').reverse().join('/') + " to " + nextTo.split('-').reverse().join('/') + "\n\n", bold: true },
                    ],
                    margin: [0, 5, 0, 5],
                    fontSize: 10,
                    alignment: 'justify'
                },
                {
                    text: 'Kindly renew the AMC agreement as per the date mentioned for our continuous service at all times.',
                    fontSize: 10,
                    alignment: 'justify'
                },
                {
                    text: 'Thanks,',
                    fontSize: 10,
                    margin: [0, 20, 0, 0]
                },
                {
                    text: 'For  M/s. IRONBIRD ELEVATORS',
                    margin: [0, 10, 0, 25],
                    fontSize: 10
                },
                {
                    text: 'Authorized Signatory',
                    fontSize: 10
                }
            ],
            styles: {
                // Invoice Title
                invoiceTitle: {
                    fontSize: 15,
                    bold: true,
                    alignment: 'center',
                    margin: [0, 20, 0, 0],
                },
            },
            defaultStyle: {
                columnGap: 20,
            }
        };
    };
    AmcRenewalComponent.prototype.getRenewal = function (e, options) {
        var amt;
        if (e.amcAmount.includes('₹')) {
            amt = e.amcAmount.replace('₹', '').split('.')[0];
        }
        else {
            if (e.amcAmount.includes('.')) {
                amt = e.amcAmount.split('.')[0];
            }
            else {
                amt = e.amcAmount;
            }
        }
        var testImageDataUrl = this.image;
        var nextTo = this.addYear(new Date(e.toData));
        var nextFrom = this.addDays(new Date(e.toData), 1).toISOString().substring(0, 10);
        return {
            pageMargins: [20, 115, 20, 70],
            pageSize: 'A4',
            footer: function () {
                if (options) {
                    return [
                        [
                            { canvas: [{ type: 'line', x1: 0, y1: 0, x2: 595, y2: 0, lineWidth: 0.5 }] },
                            {
                                stack: [
                                    {
                                        columns: [
                                            {
                                                text: '#21, 4th MAIN, BALAJI LAYOUT, NEAR BADRAPPA LAYOUT, SANJAY NAGAR POST, BANGALORE-560094.',
                                                width: '*',
                                                alignment: 'center',
                                                fontSize: 8,
                                                margin: [0, 15, 0, 0]
                                            }
                                        ]
                                    },
                                    {
                                        columns: [
                                            {
                                                text: 'Mail: ironbirdelevators@gmail.com, Landline: 080-23511445.',
                                                width: '*',
                                                alignment: 'center',
                                                fontSize: 8
                                            }
                                        ]
                                    }
                                ]
                            }
                        ],
                    ];
                }
                else {
                    return;
                }
            },
            header: function () {
                // you can apply any logic and return any valid pdfmake element
                if (options) {
                    return [
                        {
                            image: testImageDataUrl,
                            width: 600,
                            height: 100
                        }
                    ];
                }
                else {
                    return;
                }
            },
            content: [
                // Header
                {
                    columns: [
                        [
                            {
                                text: 'RENEWAL LETTER',
                                style: 'invoiceTitle',
                                width: '*'
                            },
                            {
                                stack: [
                                    {
                                        columns: [
                                            {
                                                text: '',
                                                width: 400,
                                            },
                                            {
                                                text: 'Date',
                                                width: '*',
                                                fontSize: 10
                                            },
                                            {
                                                text: "" + this.currentDate.split('-').reverse().join('/'),
                                                width: '*',
                                                fontSize: 10
                                            },
                                        ]
                                    },
                                    {
                                        columns: [
                                            {
                                                text: '',
                                                width: 400,
                                            },
                                            {
                                                text: 'Job No',
                                                fontSize: 10,
                                                width: '*',
                                            },
                                            {
                                                text: "" + e.amcid,
                                                fontSize: 10,
                                                width: '*',
                                            },
                                        ]
                                    }
                                ]
                            }
                        ],
                    ],
                },
                {
                    text: 'To,',
                    fontSize: 10
                },
                {
                    text: "" + e.customerName,
                    bold: true,
                    width: 150,
                    fontSize: 10
                },
                {
                    text: "" + e.clientAddress,
                    bold: true,
                    fontSize: 10,
                    width: 110
                },
                {
                    text: 'Dear Sir/Madam,',
                    bold: true,
                    margin: [0, 15, 0, 5],
                    fontSize: 10
                },
                {
                    text: "Sub: Renewal of AMC for the year " + this.currentYear + "-" + this.nextYear,
                    bold: true,
                    margin: [0, 5, 0, 5],
                    fontSize: 10
                },
                {
                    text: [
                        'Thank you very for your support in installing our elevator for your Project. ',
                        'At the outset we are proud to inform you that we have successfully completed our maintenance contract which had started from ',
                        { text: e.fromData.split('-').reverse().join('/') + " to " + e.toData.split('-').reverse().join('/') + ", now that the maintenance period is getting over ", bold: true },
                        { text: "and you have to renew the maintenance contract which will start from " + nextFrom.split('-').reverse().join('/') + " to " + nextTo.split('-').reverse().join('/') + ".", bold: true },
                    ],
                    margin: [0, 5, 0, 5],
                    fontSize: 10,
                    alignment: 'justify'
                },
                {
                    text: [
                        { text: "The AMC amount will be Rs. " + amt + "/- per annum (Exclusive of GST 18 %), without spares. ", bold: true },
                        "We request your good selves to renew the AMC at the earliest for our continued service at all times.",
                    ],
                    margin: [0, 5, 0, 5],
                    fontSize: 10,
                    alignment: 'justify'
                },
                {
                    text: 'Thanks,',
                    fontSize: 10,
                    margin: [0, 20, 0, 0]
                },
                {
                    text: 'For  M/s. IRONBIRD ELEVATORS',
                    margin: [0, 10, 0, 25],
                    fontSize: 10
                },
                {
                    text: 'Authorized Signatory',
                    fontSize: 10
                }
            ],
            styles: {
                // Invoice Title
                invoiceTitle: {
                    fontSize: 15,
                    bold: true,
                    alignment: 'center',
                    margin: [0, 20, 0, 0],
                },
            },
            defaultStyle: {
                columnGap: 20,
            }
        };
    };
    AmcRenewalComponent.prototype.edit = function (i, content) {
        var _this = this;
        this.selectedAMC = '';
        this.selectedAMC = i;
        this.amNO = '';
        this.amFrom = '';
        this.amTo = '';
        this.amAmt = '';
        this.listOfAmc.forEach(function (ele) {
            if (ele.id === i) {
                _this.amNO = ele.amcid;
                _this.amFrom = ele.fromData;
                _this.amTo = ele.toData;
                _this.amAmt = ele.amcAmount;
            }
        });
        this.modalRef = this.modalService.open(content, {
            size: 'lg',
            backdrop: 'static',
            keyboard: false
        });
    };
    AmcRenewalComponent.prototype.changeAmt = function (e) {
        this.amAmt = this.curr.convertToInrFormat(e);
    };
    AmcRenewalComponent.prototype.updateDetails = function () {
        var _this = this;
        this.listOfAmc.forEach(function (ele) { return __awaiter(_this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!(ele.id === this.selectedAMC)) return [3 /*break*/, 2];
                        ele.fromData = this.amFrom;
                        ele.toData = this.amTo;
                        ele.amcAmount = this.amAmt;
                        return [4 /*yield*/, this.AmcServ.updateAmcDetails(ele, this.selectedAMC)];
                    case 1:
                        result = _a.sent();
                        _a.label = 2;
                    case 2: return [2 /*return*/];
                }
            });
        }); });
        this.modalRef.close();
    };
    AmcRenewalComponent.prototype.close = function () {
        this.modalRef.close();
    };
    AmcRenewalComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-amc-renewal',
            template: __webpack_require__(/*! ./amc-renewal.component.html */ "./src/app/pages/ui-features/amc/amc-renewal/amc-renewal.component.html"),
            styles: [__webpack_require__(/*! ./amc-renewal.component.scss */ "./src/app/pages/ui-features/amc/amc-renewal/amc-renewal.component.scss")]
        }),
        __metadata("design:paramtypes", [_confirmation_dialog_confirmation_dialog_service__WEBPACK_IMPORTED_MODULE_4__["ConfirmationDialogService"],
            _services_image_service__WEBPACK_IMPORTED_MODULE_6__["ImageService"],
            _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NgbModal"],
            _services_AMC_amc_service__WEBPACK_IMPORTED_MODULE_7__["AmcService"],
            _services_currency_service__WEBPACK_IMPORTED_MODULE_5__["CurrencyService"]])
    ], AmcRenewalComponent);
    return AmcRenewalComponent;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/amc/amc-routing.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/ui-features/amc/amc-routing.module.ts ***!
  \*************************************************************/
/*! exports provided: AMCRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AMCRoutingModule", function() { return AMCRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _amc_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./amc.component */ "./src/app/pages/ui-features/amc/amc.component.ts");
/* harmony import */ var _amc_maintenance_report_amc_maintenance_report_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./amc-maintenance-report/amc-maintenance-report.component */ "./src/app/pages/ui-features/amc/amc-maintenance-report/amc-maintenance-report.component.ts");
/* harmony import */ var _amc_renewal_amc_renewal_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./amc-renewal/amc-renewal.component */ "./src/app/pages/ui-features/amc/amc-renewal/amc-renewal.component.ts");
/* harmony import */ var _total_amc_total_amc_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./total-amc/total-amc.component */ "./src/app/pages/ui-features/amc/total-amc/total-amc.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [{
        path: '',
        component: _amc_component__WEBPACK_IMPORTED_MODULE_2__["AmcComponent"],
        children: [
            //   path: 'Jobs_Dashboard',
            //   component: DashboardComponent,
            //  }, 
            {
                path: 'Main_AMC',
                component: _total_amc_total_amc_component__WEBPACK_IMPORTED_MODULE_5__["TotalAMCComponent"],
            },
            {
                path: 'renewalAMC',
                component: _amc_renewal_amc_renewal_component__WEBPACK_IMPORTED_MODULE_4__["AmcRenewalComponent"]
            },
            {
                path: 'reportAMC',
                component: _amc_maintenance_report_amc_maintenance_report_component__WEBPACK_IMPORTED_MODULE_3__["AmcMaintenanceReportComponent"]
            }
        ]
    }
];
var AMCRoutingModule = /** @class */ (function () {
    function AMCRoutingModule() {
    }
    AMCRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AMCRoutingModule);
    return AMCRoutingModule;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/amc/amc.component.html":
/*!**********************************************************!*\
  !*** ./src/app/pages/ui-features/amc/amc.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/pages/ui-features/amc/amc.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/pages/ui-features/amc/amc.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VpLWZlYXR1cmVzL2FtYy9hbWMuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/pages/ui-features/amc/amc.component.ts":
/*!********************************************************!*\
  !*** ./src/app/pages/ui-features/amc/amc.component.ts ***!
  \********************************************************/
/*! exports provided: AmcComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AmcComponent", function() { return AmcComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AmcComponent = /** @class */ (function () {
    function AmcComponent() {
    }
    AmcComponent.prototype.ngOnInit = function () {
    };
    AmcComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-amc',
            template: __webpack_require__(/*! ./amc.component.html */ "./src/app/pages/ui-features/amc/amc.component.html"),
            styles: [__webpack_require__(/*! ./amc.component.scss */ "./src/app/pages/ui-features/amc/amc.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], AmcComponent);
    return AmcComponent;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/amc/amc.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/ui-features/amc/amc.module.ts ***!
  \*****************************************************/
/*! exports provided: AMCModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AMCModule", function() { return AMCModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var _nebular_theme__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @nebular/theme */ "./node_modules/@nebular/theme/index.js");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var angular2_signaturepad__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! angular2-signaturepad */ "./node_modules/angular2-signaturepad/index.js");
/* harmony import */ var angular2_signaturepad__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(angular2_signaturepad__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var angular_tree_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! angular-tree-component */ "./node_modules/angular-tree-component/dist/angular-tree-component.js");
/* harmony import */ var ng2_search_filter__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ng2-search-filter */ "./node_modules/ng2-search-filter/ng2-search-filter.es5.js");
/* harmony import */ var ng2_order_pipe__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ng2-order-pipe */ "./node_modules/ng2-order-pipe/dist/index.js");
/* harmony import */ var ng2_order_pipe__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(ng2_order_pipe__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var ngx_pagination__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-pagination */ "./node_modules/ngx-pagination/dist/ngx-pagination.js");
/* harmony import */ var _node_modules_angular_custom_modal__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../../../node_modules/angular-custom-modal */ "./node_modules/angular-custom-modal/angular-custom-modal.es5.js");
/* harmony import */ var _amc_routing_module__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./amc-routing.module */ "./src/app/pages/ui-features/amc/amc-routing.module.ts");
/* harmony import */ var _amc_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./amc.component */ "./src/app/pages/ui-features/amc/amc.component.ts");
/* harmony import */ var _amc_maintenance_report_amc_maintenance_report_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./amc-maintenance-report/amc-maintenance-report.component */ "./src/app/pages/ui-features/amc/amc-maintenance-report/amc-maintenance-report.component.ts");
/* harmony import */ var _amc_renewal_amc_renewal_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./amc-renewal/amc-renewal.component */ "./src/app/pages/ui-features/amc/amc-renewal/amc-renewal.component.ts");
/* harmony import */ var _dummy_dummy_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./dummy/dummy.component */ "./src/app/pages/ui-features/amc/dummy/dummy.component.ts");
/* harmony import */ var _main_amc_main_amc_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./main-amc/main-amc.component */ "./src/app/pages/ui-features/amc/main-amc/main-amc.component.ts");
/* harmony import */ var _total_amc_total_amc_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./total-amc/total-amc.component */ "./src/app/pages/ui-features/amc/total-amc/total-amc.component.ts");
/* harmony import */ var _main_amc_create_update_amc_create_update_amc_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./main-amc/create-update-amc/create-update-amc.component */ "./src/app/pages/ui-features/amc/main-amc/create-update-amc/create-update-amc.component.ts");
/* harmony import */ var _main_amc_elevator_passwords_elevator_passwords_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./main-amc/elevator-passwords/elevator-passwords.component */ "./src/app/pages/ui-features/amc/main-amc/elevator-passwords/elevator-passwords.component.ts");
/* harmony import */ var _main_amc_maintance_report_maintance_report_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./main-amc/maintance-report/maintance-report.component */ "./src/app/pages/ui-features/amc/main-amc/maintance-report/maintance-report.component.ts");
/* harmony import */ var _main_amc_print_amc_print_amc_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./main-amc/print-amc/print-amc.component */ "./src/app/pages/ui-features/amc/main-amc/print-amc/print-amc.component.ts");
/* harmony import */ var _main_amc_services_stage_services_stage_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./main-amc/services-stage/services-stage.component */ "./src/app/pages/ui-features/amc/main-amc/services-stage/services-stage.component.ts");
/* harmony import */ var _main_amc_upload_amc_doc_upload_amc_doc_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./main-amc/upload-amc-doc/upload-amc-doc.component */ "./src/app/pages/ui-features/amc/main-amc/upload-amc-doc/upload-amc-doc.component.ts");
/* harmony import */ var _main_amc_print_amc_amc_without_logo_amc_without_logo_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./main-amc/print-amc/amc-without-logo/amc-without-logo.component */ "./src/app/pages/ui-features/amc/main-amc/print-amc/amc-without-logo/amc-without-logo.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









 //importing the module
















 // <-- import the module
var AMCModule = /** @class */ (function () {
    function AMCModule() {
    }
    AMCModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _amc_routing_module__WEBPACK_IMPORTED_MODULE_12__["AMCRoutingModule"],
                ng2_smart_table__WEBPACK_IMPORTED_MODULE_5__["Ng2SmartTableModule"],
                _nebular_theme__WEBPACK_IMPORTED_MODULE_4__["NbCardModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModule"],
                angular_tree_component__WEBPACK_IMPORTED_MODULE_7__["TreeModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _node_modules_angular_custom_modal__WEBPACK_IMPORTED_MODULE_11__["ModalModule"],
                ng2_search_filter__WEBPACK_IMPORTED_MODULE_8__["Ng2SearchPipeModule"],
                ng2_order_pipe__WEBPACK_IMPORTED_MODULE_9__["Ng2OrderModule"],
                ngx_pagination__WEBPACK_IMPORTED_MODULE_10__["NgxPaginationModule"],
                angular2_signaturepad__WEBPACK_IMPORTED_MODULE_6__["SignaturePadModule"],
            ],
            declarations: [
                _amc_component__WEBPACK_IMPORTED_MODULE_13__["AmcComponent"],
                _total_amc_total_amc_component__WEBPACK_IMPORTED_MODULE_18__["TotalAMCComponent"],
                _main_amc_main_amc_component__WEBPACK_IMPORTED_MODULE_17__["MainAMCComponent"],
                _main_amc_create_update_amc_create_update_amc_component__WEBPACK_IMPORTED_MODULE_19__["CreateUpdateAMCComponent"],
                _main_amc_services_stage_services_stage_component__WEBPACK_IMPORTED_MODULE_23__["ServicesStageComponent"],
                // ContainerComponent, 
                _dummy_dummy_component__WEBPACK_IMPORTED_MODULE_16__["DummyComponent"], _main_amc_print_amc_print_amc_component__WEBPACK_IMPORTED_MODULE_22__["PrintAmcComponent"], _main_amc_elevator_passwords_elevator_passwords_component__WEBPACK_IMPORTED_MODULE_20__["ElevatorPasswordsComponent"], _main_amc_maintance_report_maintance_report_component__WEBPACK_IMPORTED_MODULE_21__["MaintanceReportComponent"], _main_amc_print_amc_amc_without_logo_amc_without_logo_component__WEBPACK_IMPORTED_MODULE_25__["AmcWithoutLogoComponent"], _main_amc_upload_amc_doc_upload_amc_doc_component__WEBPACK_IMPORTED_MODULE_24__["UploadAmcDocComponent"], _amc_renewal_amc_renewal_component__WEBPACK_IMPORTED_MODULE_15__["AmcRenewalComponent"], _amc_maintenance_report_amc_maintenance_report_component__WEBPACK_IMPORTED_MODULE_14__["AmcMaintenanceReportComponent"],
            ],
            providers: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["DatePipe"]
            ]
        })
    ], AMCModule);
    return AMCModule;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/amc/dummy/dummy.component.html":
/*!******************************************************************!*\
  !*** ./src/app/pages/ui-features/amc/dummy/dummy.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  dummy works!\n</p>\n"

/***/ }),

/***/ "./src/app/pages/ui-features/amc/dummy/dummy.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/pages/ui-features/amc/dummy/dummy.component.scss ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VpLWZlYXR1cmVzL2FtYy9kdW1teS9kdW1teS5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/pages/ui-features/amc/dummy/dummy.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/pages/ui-features/amc/dummy/dummy.component.ts ***!
  \****************************************************************/
/*! exports provided: DummyComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DummyComponent", function() { return DummyComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var DummyComponent = /** @class */ (function () {
    function DummyComponent() {
    }
    DummyComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-dummy',
            template: __webpack_require__(/*! ./dummy.component.html */ "./src/app/pages/ui-features/amc/dummy/dummy.component.html"),
            styles: [__webpack_require__(/*! ./dummy.component.scss */ "./src/app/pages/ui-features/amc/dummy/dummy.component.scss")]
        })
    ], DummyComponent);
    return DummyComponent;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/amc/main-amc/create-update-amc/create-update-amc.component.html":
/*!***************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/amc/main-amc/create-update-amc/create-update-amc.component.html ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<button class=\"btn btn-outline-primary\" type=\"button\" (click)=\"updateDetails()\" style=\"float: right;\">Update Client Details</button>\n\n<br>\n<br>\n<form #myForm=\"ngForm\" name=\"form\" (ngSubmit)=\"myForm.form.valid && createAndEdit(myForm)\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-md-6\">\n                <div class=\"form-group input-group-sm\">\n                    <label class=\"clientName\">Company Name</label>\n                    <input type=\"text\" class=\"form-control\" [(ngModel)]=\"data1.customerName\" name=\"customerName\">\n                </div>\n            </div>\n            <div class=\"col-md-6\">\n                <div class=\"form-group input-group-sm\">\n                    <label class=\"SiteName\">Site Name</label>\n                    <input type=\"text\" class=\"form-control\" [(ngModel)]=\"data1.siteName\" name=\"siteName\">\n                </div>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-md-6\">\n                <div class=\"form-group input-group-sm\">\n                    <label class=\"clientAddress\">Company Address</label>\n                    <textarea type=\"text\" class=\"form-control\" [(ngModel)]=\"data1.clientAddress\" name=\"clientAddress\"></textarea>\n                </div>\n            </div>\n            <div class=\"col-md-6\">\n                <div class=\"form-group input-group-sm\">\n                    <label class=\"sitetAddress\">Site Address</label>\n                    <textarea type=\"text\" class=\"form-control\" [(ngModel)]=\"data1.siteAddress\" name=\"siteAddress\"></textarea>\n                </div>\n            </div>\n        </div>\n\n\n        <div class=\"row\" style=\"background-color: #c5c5c5\">\n            <div class=\"col-sm-4\">\n                <div class=\"form-group input-group-sm\">\n                    <label for=\"jobids\">AMC Agreement Prefix</label>\n                    <!-- {{data1.amcPrefix}} -->\n                    <input type=\"text\" class=\"form-control\" name=\"amcPrefix\" id=\"jobids\" value=\"{{data1.amcPrefix}}\" #amcPrefix [(ngModel)]=\"value\" (blur)=\"update(amcPrefix.value)\" placeholder=\"AMC Agreement Prefix\">\n\n                </div>\n            </div>\n            <div class=\"col-sm-4\">\n                <div class=\"form-group input-group-sm\">\n                    <label for=\"jobid\">AMC Agreement No</label>\n                    <!-- {{data1.amcNo}} -->\n                    <input type=\"number\" class=\"form-control\" name=\"amcNo\" #amcNo id=\"jobid\" value=\"{{data1.amcNo}}\" (blur)=\"update(amcNo.value)\" [(ngModel)]=\"values\" placeholder=\"AMC Agreement No\">\n\n                </div>\n            </div>\n            <div class=\"col-sm-4\">\n                <div class=\"form-group input-group-sm\">\n                    <label for=\"jobid\">AMC Number</label>\n                    <input type=\"text\" class=\"form-control\" name=\"amcid\" id=\"jobid\" [(ngModel)]=\"amcid\" placeholder=\"AMC Number\">\n\n                </div>\n            </div>\n        </div>\n\n\n        <div class=\"row\">\n            <div class=\"col-md-6\">\n                <div class=\"form-group input-group-sm\" *ngIf=\"!switch\">\n                    <label class=\"amount\">AMC Amount</label>\n                    <input type=\"text\" class=\"form-control\" placeholder=\"AMC Amount\" [disabled]=\"this.editValue === false\" (blur)=changeAmt($event.target.value) required value=\"{{amcAmount}}\" [(ngModel)]=\"amcAmount\" name=\"amcAmount\" min=\"0\" value=\"0\" step=\"0.01\" title=\"Currency\"\n                        pattern=\"^\\d+(?:\\.\\d{1,2})?$\">\n                    <!-- VIPIN CODE FOR CURRENCY -->\n                </div>\n                <div *ngIf='moduleEdit' class=\"form-group input-group-sm\">\n                    <!-- <label class=\"amounts\">AMC Free</label><br> -->\n                    <label for=\"Designs\">Select 'NO' to Change AMC from free to Normal </label>\n                    <br>\n                    <label class=\"switch\">\n              <input type=\"checkbox\" name=\"switch\" [(ngModel)]=\"data1.switch\" (click)=\"toggle()\">\n              <span class=\"slider round\"></span>\n            </label>\n                </div>\n            </div>\n            <div class=\"col-md-6\">\n                <div class=\"container\">\n                    <div class=\"form-group input-group-sm\">\n                        <label class=\"SiteName\">AMC Period</label>\n                        <div class=\"row\">\n                            <div class=\"col-6\">\n                                <div class=\"input-group-sm\">\n                                    <label class=\"amcs\">From</label>\n                                    <input type=\"date\" data-date=\"\" data-date-format=\"DD MMMM YYYY\" class=\"form-control\" [(ngModel)]=\"data1.fromData\" name=\"fromData\">\n                                </div>\n                            </div>\n                            <div class=\"col-6\">\n                                <div class=\"input-group-sm\">\n                                    <label class=\"amcs\">To</label>\n                                    <input type=\"date\" class=\"form-control\" [(ngModel)]=\"data1.toData\" name=\"toData\">\n                                </div>\n                            </div>\n                            <!-- <div class=\"col-4\">\n                  <div class=\"input-group-sm\">\n                    <label class=\"amcs\">Total Days</label>\n                    <input type=\"text\" class=\"form-control\" [(ngModel)]=\"diffDays\" name=\"toData\" disabled>\n                  </div>\n                </div> -->\n\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <!-- <div class=\"row\">\n  <div class=\"col-4\">\n    <div class=\"input-group-sm\">\n      <label class=\"amcs\">Total Days</label>\n      <input type=\"text\" class=\"form-control\"   value={{diffDays}} disabled>\n  \n    </div>\n  </div>\n</div> -->\n        <div class=\"row\">\n            <div class=\"col-md-4\">\n                <div class=\"form-group input-group-sm\">\n                    <label class=\"clientName\">Contact Person Name</label>\n                    <input type=\"text\" class=\"form-control\" name=\"PersonName\" [(ngModel)]=\"data1.PersonName\">\n                </div>\n            </div>\n            <!-- <div class=\"col-md-4\">\n            <div class=\"form-group input-group-sm\">\n              <label class=\"SiteName\">contact Person Number</label>\n              <input type=\"text\" class=\"form-control\" name=\"PersonName\" [(ngModel)]=\"data1.PersonName\">\n            </div>\n          </div> -->\n            <div class=\"col-md-4\">\n                <div class=\"form-group input-group-sm\">\n                    <label class=\"SiteName\">Select Service Type</label>\n                    <select class=\"form-control\" id=\"sel1\" name=\"servies\" (change)='selectService($event.target.value)' size=\"small\" [(ngModel)]=\"data1.servies\" style=\"padding-top: 3px;\">\n                      <option value='' disabled>Select AnyOne</option>\n                      <option *ngFor=\"let servie of servies;let i = index\" [value]=\"servie.type\">{{servie.type}}</option>\n                    </select>\n                </div>\n            </div>\n            <div class=\"col-md-4\" *ngIf='moduleEdit'>\n                <div class=\"form-group input-group-sm\">\n                    <br>\n                    <button type=\"button\" (click)=\"openModel()\" class=\"btn btn-outline-primary\" shape=\"semi-round\" style=\"float: left;\">Edit Services</button>\n                </div>\n            </div>\n        </div>\n\n    </div>\n    <div class=\"row foot\" style=\"margin: auto;\">\n        <div class=\"col-6\" style=\"margin-top:3px;\">\n            <button type=\"button\" (click)=\"priviewButton()\" class=\"btn btn-outline-primary\" shape=\"semi-round\" style=\"float: left;\">print Preview</button>\n        </div>\n        <div class=\"col-sm-6\" style=\"margin-top:3px;\" *ngIf='moduleEdit || moduleCreate'>\n            <button type=\"submit\" class=\"btn btn-outline-primary buttons\" shape=\"semi-round\" style=\"float: right;\">Save</button>\n            <!-- <button type=\"submit\" class=\"btn btn-outline-primary buttonss\" shape=\"semi-round\">Print</button> -->\n        </div>\n    </div>\n\n</form>\n\n\n\n\n<modal class=\"modale\" #componentInsidePrint>\n    <ng-template #modalHeader>\n    </ng-template>\n    <ng-template #modalBody>\n        <ngx-print-amc></ngx-print-amc>\n    </ng-template>\n\n</modal>\n\n<modal class=\"modale\" #componentInsidePrints>\n    <ng-template #modalHeader>\n    </ng-template>\n    <ng-template #modalBody>\n        <ngx-amc-without-logo></ngx-amc-without-logo>\n    </ng-template>\n\n</modal>\n\n<modal class=\"modale\" #componentservices>\n    <ng-template #modalHeader>\n        {{data1.servies}} Points\n        <div class=\"col-6\" style=\"display: flex;margin-left: 20%;\">\n            <label style=\"margin-right: 3%;margin-top: 2%;\">Selection Section</label>\n            <select class=\"form-control\" style=\"width:45%;\" [(ngModel)]='sectionSel' type=\"text\" size=\"small\" (change)='section($event.target.value)'>\n                <option *ngFor=\"let state of sections\">{{state}}</option>\n            </select>\n        </div>\n\n    </ng-template>\n    <ng-template #modalBody>\n        {{data1.servies}} Service List\n        <button type=\"button\" class=\"btn btn-outline-primary buttons\" (click)='addNewCompany()' shape=\"semi-round\" style=\"float: right;margin: -1%;\">Add</button>\n        <form [formGroup]=\"myFormTest\" (ngSubmit)='savePoints(myFormTest)'>\n            <div formArrayName=\"serviesPoint\" *ngIf=\"!length\">\n                <div *ngFor=\"let comp of myFormTest.get('serviesPoint').controls; let i=index\">\n                    <div [formGroupName]=\"i\" style=\"width: 98%;margin-bottom: 1%;margin-top: 1%;\">\n                        <div style=\"display: flex;\">\n                            <span style=\"margin: 12px;\">{{i+1}})</span>\n                            <textarea formControlName=\"mainPonit\" type=\"text\" type=\"text\" rows=\"1\" cols='100' class=\"form-control\"></textarea>\n                            <span style=\"float: right !important;margin-top: 1%;margin-left: 2%;\">\n                                <i style=\"cursor: pointer;margin-right: 1%;\" class=\"fa fa-plus-circle\" (click)=\"addSubPoint(comp.controls.subPonit)\"></i>\n                              <i style=\"cursor: pointer;\" class=\"fa fa-trash\" (click)=\"deleteMainPoint(i)\"></i></span>\n                        </div>\n                        <div formArrayName=\"subPonit\">\n                            <div *ngFor=\"let project of comp.get('subPonit').controls; let j=index\">\n                                <div [formGroupName]=\"j\" style=\"width: 96%;display: flex;margin-top: 1%;\">\n                                    <span style=\"margin: 12px;margin-left: 3%;\">{{i+1}}.{{j+1}})</span>\n                                    <textarea formControlName=\"subPoints\" style=\"margin-right: 2%;\" type=\"text\" type=\"text\" rows=\"1\" cols='80' class=\"form-control\"></textarea>\n                                    <span style=\"margin-top: 1%;margin-right: -3%;\">\n                                      <i style=\"cursor: pointer;\" class=\"fa fa-trash\" (click)=\"deleteSubPoint(comp.controls.subPonit, j)\"></i>\n                                    </span><br>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n\n                </div>\n            </div><br>\n            <div *ngIf=\"length\">\n                <h3 style=\"text-align: center !important\">No Points in {{data1.servies}} Service List Found.</h3>\n            </div>\n            <button type=\"submit\" class=\"btn btn-outline-primary buttons\" shape=\"semi-round\" style=\"float: right;margin: -1%;\">Submit</button>\n            <button type=\"button\" class=\"btn btn-outline-primary buttons\" shape=\"semi-round\" (click)='close()' style=\"float: left;margin: -1%;\">Close</button>\n        </form>\n    </ng-template>\n</modal>"

/***/ }),

/***/ "./src/app/pages/ui-features/amc/main-amc/create-update-amc/create-update-amc.component.scss":
/*!***************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/amc/main-amc/create-update-amc/create-update-amc.component.scss ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".amc {\n  display: -webkit-inline-box;\n  display: -ms-inline-flexbox;\n  display: inline-flex; }\n\n.amcs {\n  padding-top: 9px; }\n\n.buttons {\n  float: right; }\n\n.buttonss {\n  float: left; }\n\n.row {\n  display: -webkit-box !important; }\n\nnb-card {\n  border: 0px solid #d5dbe0; }\n\nnb-card-body {\n  background-color: #e1f5fe; }\n\n@media (min-width: 576px) {\n  /deep/ modal.modalss .modal-dialog {\n    max-width: 100%;\n    margin: 1.75rem auto;\n    overflow: scroll; }\n  /deep/ modal.modals .modal-dialog {\n    max-width: 80%;\n    margin: 1.75rem auto;\n    overflow: scroll; }\n  /deep/ modal.modale .modal-dialog {\n    max-width: 50%;\n    margin: 1.75rem auto;\n    overflow: scroll;\n    scroll-behavior: smooth; } }\n\n/deep/modal .modal-footer {\n  background-color: none !important; }\n\n.switch {\n  position: relative;\n  display: inline-block;\n  width: 55px;\n  height: 25px; }\n\n.switch input {\n  opacity: 0;\n  width: 0;\n  height: 0; }\n\n.slider {\n  position: absolute;\n  cursor: pointer;\n  top: 0;\n  left: 0;\n  right: 6px;\n  bottom: 0;\n  background-color: #ccc;\n  -webkit-transition: .4s;\n  transition: .4s; }\n\n.slider:before {\n  position: absolute;\n  content: \"\";\n  height: 26px;\n  width: 26px;\n  left: 0px;\n  bottom: 0px;\n  background-color: white;\n  -webkit-transition: .4s;\n  transition: .4s; }\n\ninput:checked + .slider {\n  background-color: #2196F3; }\n\ninput:focus + .slider {\n  -webkit-box-shadow: 0 0 1px #2196F3;\n          box-shadow: 0 0 1px #2196F3; }\n\ninput:checked + .slider:before {\n  -webkit-transform: translateX(26px);\n  transform: translateX(26px); }\n\n/* Rounded sliders */\n\n.slider.round {\n  border-radius: 34px; }\n\n.slider.round:before {\n  border-radius: 50%; }\n\n@media (max-width: 1920px) {\n  /deep/ modal.modale .modal-dialog {\n    max-width: 79%;\n    margin: 1.75rem auto;\n    overflow: scroll; } }\n\n.on {\n  display: none; }\n\n.on,\n.off {\n  color: black;\n  position: absolute;\n  -webkit-transform: translate(-50%, -50%);\n          transform: translate(-50%, -50%);\n  top: 50%;\n  left: 74%;\n  font-size: 13px;\n  font-family: Verdana, sans-serif; }\n\ninput:checked + .slider .on {\n  display: block; }\n\ninput:checked + .slider .off {\n  display: none; }\n\n.switchs {\n  left: 20%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3ByYWthc2gvVmlwaW4vaXJvbmJpcmQvMjBGRUIyMDIwL2lyb25iaXJkLWNsaWVudHMvc3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9hbWMvbWFpbi1hbWMvY3JlYXRlLXVwZGF0ZS1hbWMvY3JlYXRlLXVwZGF0ZS1hbWMuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSwyQkFBb0I7RUFBcEIsMkJBQW9CO0VBQXBCLG9CQUFvQixFQUFBOztBQUd4QjtFQUNJLGdCQUFnQixFQUFBOztBQUdwQjtFQUNJLFlBQVksRUFBQTs7QUFHaEI7RUFDSSxXQUFXLEVBQUE7O0FBR2Y7RUFDSSwrQkFBK0IsRUFBQTs7QUFHbkM7RUFDSSx5QkFBeUIsRUFBQTs7QUFHN0I7RUFDSSx5QkFBeUIsRUFBQTs7QUFHN0I7RUFDSTtJQUNJLGVBQWU7SUFDZixvQkFBb0I7SUFDcEIsZ0JBQWdCLEVBQUE7RUFFcEI7SUFDSSxjQUFjO0lBQ2Qsb0JBQW9CO0lBQ3BCLGdCQUFnQixFQUFBO0VBRXBCO0lBQ0ksY0FBYztJQUNkLG9CQUFvQjtJQUNwQixnQkFBZ0I7SUFDaEIsdUJBQXVCLEVBQUEsRUFDMUI7O0FBR0w7RUFDSSxpQ0FBaUMsRUFBQTs7QUFHckM7RUFDSSxrQkFBa0I7RUFDbEIscUJBQXFCO0VBQ3JCLFdBQVc7RUFDWCxZQUFZLEVBQUE7O0FBR2hCO0VBQ0ksVUFBVTtFQUNWLFFBQVE7RUFDUixTQUFTLEVBQUE7O0FBR2I7RUFDSSxrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLE1BQU07RUFDTixPQUFPO0VBQ1AsVUFBVTtFQUNWLFNBQVM7RUFDVCxzQkFBc0I7RUFDdEIsdUJBQXVCO0VBQ3ZCLGVBQWUsRUFBQTs7QUFHbkI7RUFDSSxrQkFBa0I7RUFDbEIsV0FBVztFQUNYLFlBQVk7RUFDWixXQUFXO0VBQ1gsU0FBUztFQUNULFdBQVc7RUFDWCx1QkFBdUI7RUFDdkIsdUJBQXVCO0VBQ3ZCLGVBQWUsRUFBQTs7QUFHbkI7RUFDSSx5QkFBeUIsRUFBQTs7QUFHN0I7RUFDSSxtQ0FBMkI7VUFBM0IsMkJBQTJCLEVBQUE7O0FBRy9CO0VBQ0ksbUNBQW1DO0VBRW5DLDJCQUEyQixFQUFBOztBQUkvQixvQkFBQTs7QUFFQTtFQUNJLG1CQUFtQixFQUFBOztBQUd2QjtFQUNJLGtCQUFrQixFQUFBOztBQUd0QjtFQUNJO0lBQ0ksY0FBYztJQUNkLG9CQUFvQjtJQUNwQixnQkFBZ0IsRUFBQSxFQUNuQjs7QUFHTDtFQUNJLGFBQWEsRUFBQTs7QUFHakI7O0VBRUksWUFBWTtFQUNaLGtCQUFrQjtFQUNsQix3Q0FBZ0M7VUFBaEMsZ0NBQWdDO0VBQ2hDLFFBQVE7RUFDUixTQUFTO0VBQ1QsZUFBZTtFQUNmLGdDQUFnQyxFQUFBOztBQUdwQztFQUNJLGNBQWMsRUFBQTs7QUFHbEI7RUFDSSxhQUFhLEVBQUE7O0FBR2pCO0VBQ0ksU0FBUyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvdWktZmVhdHVyZXMvYW1jL21haW4tYW1jL2NyZWF0ZS11cGRhdGUtYW1jL2NyZWF0ZS11cGRhdGUtYW1jLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmFtYyB7XG4gICAgZGlzcGxheTogaW5saW5lLWZsZXg7XG59XG5cbi5hbWNzIHtcbiAgICBwYWRkaW5nLXRvcDogOXB4O1xufVxuXG4uYnV0dG9ucyB7XG4gICAgZmxvYXQ6IHJpZ2h0O1xufVxuXG4uYnV0dG9uc3Mge1xuICAgIGZsb2F0OiBsZWZ0O1xufVxuXG4ucm93IHtcbiAgICBkaXNwbGF5OiAtd2Via2l0LWJveCAhaW1wb3J0YW50O1xufVxuXG5uYi1jYXJkIHtcbiAgICBib3JkZXI6IDBweCBzb2xpZCAjZDVkYmUwO1xufVxuXG5uYi1jYXJkLWJvZHkge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNlMWY1ZmU7XG59XG5cbkBtZWRpYSAobWluLXdpZHRoOiA1NzZweCkge1xuICAgIC9kZWVwLyBtb2RhbC5tb2RhbHNzIC5tb2RhbC1kaWFsb2cge1xuICAgICAgICBtYXgtd2lkdGg6IDEwMCU7XG4gICAgICAgIG1hcmdpbjogMS43NXJlbSBhdXRvO1xuICAgICAgICBvdmVyZmxvdzogc2Nyb2xsO1xuICAgIH1cbiAgICAvZGVlcC8gbW9kYWwubW9kYWxzIC5tb2RhbC1kaWFsb2cge1xuICAgICAgICBtYXgtd2lkdGg6IDgwJTtcbiAgICAgICAgbWFyZ2luOiAxLjc1cmVtIGF1dG87XG4gICAgICAgIG92ZXJmbG93OiBzY3JvbGw7XG4gICAgfVxuICAgIC9kZWVwLyBtb2RhbC5tb2RhbGUgLm1vZGFsLWRpYWxvZyB7XG4gICAgICAgIG1heC13aWR0aDogNTAlO1xuICAgICAgICBtYXJnaW46IDEuNzVyZW0gYXV0bztcbiAgICAgICAgb3ZlcmZsb3c6IHNjcm9sbDtcbiAgICAgICAgc2Nyb2xsLWJlaGF2aW9yOiBzbW9vdGg7XG4gICAgfVxufVxuXG4vZGVlcC9tb2RhbCAubW9kYWwtZm9vdGVyIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBub25lICFpbXBvcnRhbnQ7XG59XG5cbi5zd2l0Y2gge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgd2lkdGg6IDU1cHg7XG4gICAgaGVpZ2h0OiAyNXB4O1xufVxuXG4uc3dpdGNoIGlucHV0IHtcbiAgICBvcGFjaXR5OiAwO1xuICAgIHdpZHRoOiAwO1xuICAgIGhlaWdodDogMDtcbn1cblxuLnNsaWRlciB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICB0b3A6IDA7XG4gICAgbGVmdDogMDtcbiAgICByaWdodDogNnB4O1xuICAgIGJvdHRvbTogMDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjY2NjO1xuICAgIC13ZWJraXQtdHJhbnNpdGlvbjogLjRzO1xuICAgIHRyYW5zaXRpb246IC40cztcbn1cblxuLnNsaWRlcjpiZWZvcmUge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBjb250ZW50OiBcIlwiO1xuICAgIGhlaWdodDogMjZweDtcbiAgICB3aWR0aDogMjZweDtcbiAgICBsZWZ0OiAwcHg7XG4gICAgYm90dG9tOiAwcHg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gICAgLXdlYmtpdC10cmFuc2l0aW9uOiAuNHM7XG4gICAgdHJhbnNpdGlvbjogLjRzO1xufVxuXG5pbnB1dDpjaGVja2VkKy5zbGlkZXIge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMyMTk2RjM7XG59XG5cbmlucHV0OmZvY3VzKy5zbGlkZXIge1xuICAgIGJveC1zaGFkb3c6IDAgMCAxcHggIzIxOTZGMztcbn1cblxuaW5wdXQ6Y2hlY2tlZCsuc2xpZGVyOmJlZm9yZSB7XG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZVgoMjZweCk7XG4gICAgLW1zLXRyYW5zZm9ybTogdHJhbnNsYXRlWCgyNnB4KTtcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVgoMjZweCk7XG59XG5cblxuLyogUm91bmRlZCBzbGlkZXJzICovXG5cbi5zbGlkZXIucm91bmQge1xuICAgIGJvcmRlci1yYWRpdXM6IDM0cHg7XG59XG5cbi5zbGlkZXIucm91bmQ6YmVmb3JlIHtcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XG59XG5cbkBtZWRpYSAobWF4LXdpZHRoOjE5MjBweCkge1xuICAgIC9kZWVwLyBtb2RhbC5tb2RhbGUgLm1vZGFsLWRpYWxvZyB7XG4gICAgICAgIG1heC13aWR0aDogNzklO1xuICAgICAgICBtYXJnaW46IDEuNzVyZW0gYXV0bztcbiAgICAgICAgb3ZlcmZsb3c6IHNjcm9sbDtcbiAgICB9XG59XG5cbi5vbiB7XG4gICAgZGlzcGxheTogbm9uZTtcbn1cblxuLm9uLFxuLm9mZiB7XG4gICAgY29sb3I6IGJsYWNrO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcbiAgICB0b3A6IDUwJTtcbiAgICBsZWZ0OiA3NCU7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIGZvbnQtZmFtaWx5OiBWZXJkYW5hLCBzYW5zLXNlcmlmO1xufVxuXG5pbnB1dDpjaGVja2VkKy5zbGlkZXIgLm9uIHtcbiAgICBkaXNwbGF5OiBibG9jaztcbn1cblxuaW5wdXQ6Y2hlY2tlZCsuc2xpZGVyIC5vZmYge1xuICAgIGRpc3BsYXk6IG5vbmU7XG59XG5cbi5zd2l0Y2hzIHtcbiAgICBsZWZ0OiAyMCU7XG59Il19 */"

/***/ }),

/***/ "./src/app/pages/ui-features/amc/main-amc/create-update-amc/create-update-amc.component.ts":
/*!*************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/amc/main-amc/create-update-amc/create-update-amc.component.ts ***!
  \*************************************************************************************************/
/*! exports provided: CreateUpdateAMCComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateUpdateAMCComponent", function() { return CreateUpdateAMCComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _alert_dialog_alert_dialog_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../alert-dialog/alert-dialog.service */ "./src/app/alert-dialog/alert-dialog.service.ts");
/* harmony import */ var _confirmation_dialog_confirmation_dialog_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../confirmation-dialog/confirmation-dialog.service */ "./src/app/confirmation-dialog/confirmation-dialog.service.ts");
/* harmony import */ var _services_currency_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../services/currency.service */ "./src/app/services/currency.service.ts");
/* harmony import */ var _services_AMC_amc_service_points_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../../services/AMC/amc-service-points.service */ "./src/app/services/AMC/amc-service-points.service.ts");
/* harmony import */ var _services_AMC_amc_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../../services/AMC/amc.service */ "./src/app/services/AMC/amc.service.ts");
/* harmony import */ var _services_token_token_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../../services/token/token.service */ "./src/app/services/token/token.service.ts");
/* harmony import */ var _node_modules_angular_custom_modal__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../../../../node_modules/angular-custom-modal */ "./node_modules/angular-custom-modal/angular-custom-modal.es5.js");
/* harmony import */ var _services_AMC_amcType_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../../../services/AMC/amcType.service */ "./src/app/services/AMC/amcType.service.ts");
/* harmony import */ var _services_admin_quotation_quotation_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../../../services/admin/quotation/quotation.service */ "./src/app/services/admin/quotation/quotation.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








 // VIPIN CODE FOR CURRENCY


var CreateUpdateAMCComponent = /** @class */ (function () {
    function CreateUpdateAMCComponent(fb, userServ, QuotationServ, AmcServ, confirmationDialogService, currency, // VIPIN CODE FOR CURRENCY
    AmcTypeServ, AmcServicePointsSer, alertDialogService) {
        // this.myFormTest = this.fb.group({
        //   serviesPoint: this.fb.array([])
        // })
        this.fb = fb;
        this.userServ = userServ;
        this.QuotationServ = QuotationServ;
        this.AmcServ = AmcServ;
        this.confirmationDialogService = confirmationDialogService;
        this.currency = currency;
        this.AmcTypeServ = AmcTypeServ;
        this.AmcServicePointsSer = AmcServicePointsSer;
        this.alertDialogService = alertDialogService;
        this.data1 = { invoiceItems: [] };
        this.show = true; // data2: object = {};
        this.editShow = false;
        this.listOfAmc = [];
        this.regExpr = /[₹,]/g;
        this.editValue = true;
        this.sectionSel = 'IRONBIRD ELEVATORS PVT LTD.';
        this.sections = ['IRONBIRD ELEVATORS PVT LTD.', 'The Customer Shall', 'General Terms'];
        this.AmcChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.moduleCreate = true;
        this.moduleEdit = true;
        this.moduleView = true;
        this.dataPoint = {};
        this.sectionArray = [];
        this.length = false;
        this.value = '';
        this.values = '';
        // this.setServies();
    }
    CreateUpdateAMCComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.userServ.userAccess !== undefined) {
            this.userServ.userAccess
                .subscribe(function (data) {
                data.forEach(function (ele) {
                    if (ele.name === 'AMC  Details') {
                        _this.moduleCreate = ele.create;
                        _this.moduleEdit = ele.edit;
                        _this.moduleView = ele.view;
                    }
                });
            }, function (error) {
                console.log(error);
            });
        }
        this.getAmcDetail();
        // this.getAmcDetailPoints()
    };
    CreateUpdateAMCComponent.prototype.section = function (event) {
        this.sectionSel = event;
        this.myFormTest = this.fb.group({
            serviesPoint: this.fb.array([])
        });
        this.setServies();
    };
    CreateUpdateAMCComponent.prototype.updateDetails = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            var resultAleart, update, res;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.confirmationDialogService.confirm('Alert Message', 'Are you sure you want to Update Client Details?')];
                    case 1:
                        resultAleart = _a.sent();
                        if (!resultAleart) return [3 /*break*/, 3];
                        update = { 'from': 'amc' };
                        return [4 /*yield*/, this.QuotationServ.UpdateClientDetails(this.id, update)];
                    case 2:
                        res = _a.sent();
                        if (res.msg === 'success') {
                            this.data1.customerName = res.customerName;
                            this.data1.siteName = res.siteName;
                            this.data1.clientAddress = res.clientAddress;
                            this.data1.siteAddress = res.siteAddress;
                        }
                        _a.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    // VIPIN CODE FOR CURRENCY
    CreateUpdateAMCComponent.prototype.changeAmt = function (amt) {
        if (amt[0] === '₹') {
            amt = amt.replace(this.regExpr, "");
        }
        this.amcAmt = '';
        this.amcAmt = this.currency.convertToInrFormat(amt); // VIPIN CODE FOR CURRENCY
    };
    CreateUpdateAMCComponent.prototype.getAmcDetail = function () {
        var _this = this;
        this.AmcServ.AmcDone
            .subscribe(function (data) {
            _this.data1 = data;
            _this.amcid = _this.data1.amcid;
            _this.value = _this.data1.amcPrefix;
            _this.values = _this.data1.amcNo;
            _this.type = _this.data1.servies;
            _this.dataPoint = {};
            _this.switch = _this.data1.switch;
            Object.assign(_this.dataPoint, { serviesPoint: _this.data1.serviesPoint, serviesPoint0: _this.data1.serviesPoint0, serviesPoint1: _this.data1.serviesPoint1 });
            if (_this.data1.servies !== '' && _this.data1.serviesPoint.length !== 0) {
                _this.type0 = _this.type;
                if (_this.data1.serviesPoint[0].mainPonit !== '') {
                    _this.myFormTest = _this.fb.group({
                        serviesPoint: _this.fb.array([])
                    });
                    _this.setServies();
                }
            }
            else {
                _this.length = true;
                _this.myFormTest = _this.fb.group({
                    serviesPoint: _this.fb.array([])
                });
            }
            var date1 = new Date();
            var date2 = new Date(data.toData);
            if (date1.getTime() > date2.getTime()) {
                _this.diffDays = 0;
            }
            else {
                var diffTime = Math.abs(date2.getTime() - date1.getTime());
                _this.diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
            }
            if (_this.diffDays <= 30) {
                // console.log("hello")
                _this.editValue = true;
            }
            else {
                _this.editValue = false;
            }
            _this.amcAmount = _this.currency.convertToInrFormat(_this.data1.amcAmount); // VIPIN CODE FOR CURRENCY
            // console.log(this.amcAmount)
            if (!_this.data1.id) {
                return _this.show = false;
            }
            _this.id = _this.data1.id;
        }, function (error) {
            console.log(error);
        });
        this.allAmcService();
    };
    /**
        * Create AMC Detail
        */
    CreateUpdateAMCComponent.prototype.createJob = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            var resultAleart, result, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.confirmationDialogService.confirm('Alert Message', 'Are you sure you want to Create?', 'success')];
                    case 1:
                        resultAleart = _a.sent();
                        if (!resultAleart) return [3 /*break*/, 6];
                        _a.label = 2;
                    case 2:
                        _a.trys.push([2, 4, , 5]);
                        return [4 /*yield*/, this.AmcServ.createAmc(data)];
                    case 3:
                        result = _a.sent();
                        this.AmcChange.emit(result);
                        return [3 /*break*/, 5];
                    case 4:
                        error_1 = _a.sent();
                        console.log(error_1);
                        return [3 /*break*/, 5];
                    case 5:
                        data.confirm.resolve();
                        return [3 /*break*/, 7];
                    case 6:
                        data.confirm.reject();
                        _a.label = 7;
                    case 7: return [2 /*return*/];
                }
            });
        });
    };
    CreateUpdateAMCComponent.prototype.createData = function (data) {
        this.listOfAmc.push(data);
    };
    /**
         Edit  Inventory
       **/
    CreateUpdateAMCComponent.prototype.editAgreementData = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            var resultAleart, result, error_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.confirmationDialogService.confirm('Alert Message', 'Are you sure you want to Update?')];
                    case 1:
                        resultAleart = _a.sent();
                        if (!resultAleart) return [3 /*break*/, 5];
                        _a.label = 2;
                    case 2:
                        _a.trys.push([2, 4, , 5]);
                        Object.assign(data, { switch: this.switch, amcAmount: this.amcAmount, amcid: this.amcid, amcPrefix: this.value, amcNo: this.values });
                        return [4 /*yield*/, this.AmcServ.UpdateAmcDetails(data, this.id)];
                    case 3:
                        result = _a.sent();
                        this.AmcServ.AmcDone.next(result);
                        return [3 /*break*/, 5];
                    case 4:
                        error_2 = _a.sent();
                        console.log(error_2);
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    CreateUpdateAMCComponent.prototype.createAndEdit = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            var value;
            return __generator(this, function (_a) {
                value = data.value;
                // console.log(value)
                if (this.editShow) {
                    return [2 /*return*/, this.createJob(value)];
                }
                else {
                    if (this.type0 !== '' && this.type0 !== undefined) {
                        this.type = this.type0;
                    }
                    Object.assign(value, { amcAmount: this.amcAmount, serviesPoint: this.serviesSet, serviesPoint0: this.serviesSet0, serviesPoint1: this.serviesSet1, servies: this.type });
                    this.editAgreementData(value);
                }
                return [2 /*return*/];
            });
        });
    };
    CreateUpdateAMCComponent.prototype.priviewButton = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.componentInsidePrints.open();
                return [2 /*return*/];
            });
        });
    };
    CreateUpdateAMCComponent.prototype.allAmcService = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.AmcTypeServ.getAllAmcType()];
                    case 1:
                        result = _a.sent();
                        this.servies = result;
                        return [2 /*return*/];
                }
            });
        });
    };
    CreateUpdateAMCComponent.prototype.openModel = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(this.type0);
                        if (!(this.type0 === undefined || this.type0 === '')) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.alertDialogService.confirm('Alert Message', 'Please select service type first.')];
                    case 1:
                        result = _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        this.componentservices.open();
                        _a.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    CreateUpdateAMCComponent.prototype.selectService = function (event) {
        this.type0 = event;
        this.setServies();
    };
    CreateUpdateAMCComponent.prototype.savePoints = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!(this.sectionSel === 'IRONBIRD ELEVATORS PVT LTD.')) return [3 /*break*/, 2];
                        this.serviesSet = data.value.serviesPoint;
                        return [4 /*yield*/, this.alertDialogService.confirm('Alert Message', 'Successfully Added.')];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 6];
                    case 2:
                        if (!(this.sectionSel === 'The Customer Shall')) return [3 /*break*/, 4];
                        this.serviesSet0 = data.value.serviesPoint;
                        return [4 /*yield*/, this.alertDialogService.confirm('Alert Message', 'Successfully Added.')];
                    case 3:
                        _a.sent();
                        return [3 /*break*/, 6];
                    case 4:
                        this.serviesSet1 = data.value.serviesPoint;
                        return [4 /*yield*/, this.alertDialogService.confirm('Alert Message', 'Successfully Added.')];
                    case 5:
                        _a.sent();
                        _a.label = 6;
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    CreateUpdateAMCComponent.prototype.close = function () {
        this.componentservices.close();
    };
    CreateUpdateAMCComponent.prototype.update = function (value, name) {
        this[name] = value;
        this.amcid = "" + this.value + this.values;
    };
    CreateUpdateAMCComponent.prototype.addNewCompany = function () {
        this.length = false;
        var control = this.myFormTest.controls.serviesPoint;
        control.push(this.fb.group({
            mainPonit: [''],
            subPonit: this.fb.array([])
        }));
    };
    CreateUpdateAMCComponent.prototype.deleteMainPoint = function (index) {
        var control = this.myFormTest.controls.serviesPoint;
        control.removeAt(index);
        if (control.length === 0) {
            this.length = true;
        }
        else {
            this.length = false;
        }
    };
    CreateUpdateAMCComponent.prototype.addSubPoint = function (control) {
        control.push(this.fb.group({
            subPoints: ['']
        }));
    };
    CreateUpdateAMCComponent.prototype.deleteSubPoint = function (control, index) {
        control.removeAt(index);
    };
    CreateUpdateAMCComponent.prototype.setServies = function () {
        var _this = this;
        this.length = true;
        if (this.sectionSel === 'IRONBIRD ELEVATORS PVT LTD.') {
            var control_1 = this.myFormTest.controls.serviesPoint;
            if (this.dataPoint.serviesPoint.length > 0 && this.type !== '' && this.type !== undefined && this.type === this.type0) {
                this.length = false;
                this.dataPoint.serviesPoint.forEach(function (x) {
                    control_1.push(_this.fb.group({
                        mainPonit: x.mainPonit,
                        subPonit: _this.setProjects(x)
                    }));
                });
            }
            else {
                this.servies.forEach(function (ele) {
                    if (_this.type0 === ele.type) {
                        if (ele.serviesPoint.length > 0) {
                            _this.length = false;
                            ele.serviesPoint.forEach(function (x) {
                                control_1.push(_this.fb.group({
                                    mainPonit: x.mainPonit,
                                    subPonit: _this.setProjects(x)
                                }));
                            });
                        }
                    }
                });
            }
        }
        else if (this.sectionSel === 'The Customer Shall') {
            var control_2 = this.myFormTest.controls.serviesPoint;
            if (this.dataPoint.serviesPoint0.length > 0 && this.type !== '' && this.type !== undefined && this.type === this.type0) {
                this.length = false;
                this.dataPoint.serviesPoint0.forEach(function (x) {
                    control_2.push(_this.fb.group({
                        mainPonit: x.mainPonit,
                        subPonit: _this.setProjects(x)
                    }));
                });
            }
            else {
                this.servies.forEach(function (ele) {
                    if (_this.type0 === ele.type) {
                        if (ele.serviesPoint0.length > 0) {
                            _this.length = false;
                            ele.serviesPoint0.forEach(function (x) {
                                control_2.push(_this.fb.group({
                                    mainPonit: x.mainPonit,
                                    subPonit: _this.setProjects(x)
                                }));
                            });
                        }
                    }
                });
            }
        }
        else {
            var control_3 = this.myFormTest.controls.serviesPoint;
            if (this.dataPoint.serviesPoint1.length > 0 && this.type !== '' && this.type !== undefined && this.type === this.type0) {
                this.length = false;
                this.dataPoint.serviesPoint1.forEach(function (x) {
                    control_3.push(_this.fb.group({
                        mainPonit: x.mainPonit,
                        subPonit: _this.setProjects(x)
                    }));
                });
            }
            else {
                this.servies.forEach(function (ele) {
                    if (_this.type0 === ele.type) {
                        if (ele.serviesPoint1.length > 0) {
                            _this.length = false;
                            ele.serviesPoint1.forEach(function (x) {
                                control_3.push(_this.fb.group({
                                    mainPonit: x.mainPonit,
                                    subPonit: _this.setProjects(x)
                                }));
                            });
                        }
                    }
                });
            }
        }
    };
    CreateUpdateAMCComponent.prototype.setProjects = function (x) {
        var _this = this;
        var arr = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormArray"]([]);
        x.subPonit.forEach(function (y) {
            arr.push(_this.fb.group({
                subPoints: y.subPoints
            }));
        });
        return arr;
    };
    CreateUpdateAMCComponent.prototype.toggle = function () {
        this.switch = !this.switch;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('componentInsideModal'),
        __metadata("design:type", _node_modules_angular_custom_modal__WEBPACK_IMPORTED_MODULE_8__["ModalComponent"])
    ], CreateUpdateAMCComponent.prototype, "componentInsideModal", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('componentInsidePrints'),
        __metadata("design:type", _node_modules_angular_custom_modal__WEBPACK_IMPORTED_MODULE_8__["ModalComponent"])
    ], CreateUpdateAMCComponent.prototype, "componentInsidePrints", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('componentInsidePrint'),
        __metadata("design:type", _node_modules_angular_custom_modal__WEBPACK_IMPORTED_MODULE_8__["ModalComponent"])
    ], CreateUpdateAMCComponent.prototype, "componentInsidePrint", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('componentservices'),
        __metadata("design:type", _node_modules_angular_custom_modal__WEBPACK_IMPORTED_MODULE_8__["ModalComponent"])
    ], CreateUpdateAMCComponent.prototype, "componentservices", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], CreateUpdateAMCComponent.prototype, "AmcChange", void 0);
    CreateUpdateAMCComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-create-update-amc',
            template: __webpack_require__(/*! ./create-update-amc.component.html */ "./src/app/pages/ui-features/amc/main-amc/create-update-amc/create-update-amc.component.html"),
            styles: [__webpack_require__(/*! ./create-update-amc.component.scss */ "./src/app/pages/ui-features/amc/main-amc/create-update-amc/create-update-amc.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            _services_token_token_service__WEBPACK_IMPORTED_MODULE_7__["TokenService"],
            _services_admin_quotation_quotation_service__WEBPACK_IMPORTED_MODULE_10__["QuotationService"],
            _services_AMC_amc_service__WEBPACK_IMPORTED_MODULE_6__["AmcService"],
            _confirmation_dialog_confirmation_dialog_service__WEBPACK_IMPORTED_MODULE_3__["ConfirmationDialogService"],
            _services_currency_service__WEBPACK_IMPORTED_MODULE_4__["CurrencyService"],
            _services_AMC_amcType_service__WEBPACK_IMPORTED_MODULE_9__["AmcTypeService"],
            _services_AMC_amc_service_points_service__WEBPACK_IMPORTED_MODULE_5__["AmcServicePointsService"],
            _alert_dialog_alert_dialog_service__WEBPACK_IMPORTED_MODULE_2__["AlertDialogService"]])
    ], CreateUpdateAMCComponent);
    return CreateUpdateAMCComponent;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/amc/main-amc/elevator-passwords/elevator-passwords.component.html":
/*!*****************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/amc/main-amc/elevator-passwords/elevator-passwords.component.html ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<br><br>\n\n<form #AmcPasswords=\"ngForm\" name=\"form\" (ngSubmit)=\"AmcPasswords.form.valid && createAMCPasswords(AmcPasswords)\">\n\n    <div class=\"container-fluid\">\n        <p class=\"back\">Client Name: {{data1.customerName}}&nbsp; And &nbsp;Site Name:{{data1.siteName}}</p>\n        <div class=\"row\">\n            <div class=\"col-md-6\">\n                <div class=\"form-group input-group-sm\">\n                    <label class=\"clientName\">Password Type:</label>\n                    <select class=\"form-control\" id=\"power\" placeholder=\"Password Type\" size=\"small\" [(ngModel)]=\"data2.passType\" name=\"passType\" required>\n          <option disabled>Select Any one</option>\n          <option>Count Password</option>\n          <option>Program password</option>\n          <option>Door Drive password</option>\n          <option>Other</option>\n          </select>\n                </div>\n            </div>\n            <div class=\"col-md-6\">\n                <div class=\"form-group input-group-sm\">\n                    <label class=\"SiteName\">Password</label>\n                    <input type=\"password\" class=\"form-control\" [(ngModel)]=\"data2.passwords\" name=\"passwords\" #passwords=\"ngModel\" [ngClass]=\"{ 'is-invalid': AmcPasswords.submitted && passwords.invalid}\" required>\n\n                    <div *ngIf=\" AmcPasswords.submitted && passwords.invalid\" class=\"invalid-feedback\">\n                        <div *ngIf=\"passwords.errors.required\">Password is Required</div>\n                    </div>\n                </div>\n            </div>\n\n        </div>\n        <div class=\"row foot\">\n            <div class=\"col-6\" style=\"margin-top:3px;\">\n\n            </div>\n            <div class=\"col-sm-6\" style=\"margin-top:3px;\" *ngIf='moduleCreate || moduleEdit'>\n                <button type=\"submit\" class=\"btn btn-outline-primary buttons\" shape=\"semi-round\" style=\"float: right;\">Save</button>\n                <!-- <button type=\"submit\" class=\"btn btn-outline-primary buttonss\" shape=\"semi-round\">Print</button> -->\n            </div>\n        </div>\n    </div>\n</form>\n\n<button *ngIf='moduleCreate || moduleEdit' type=\"button \" class=\"btn btn-outline-primary justify-content-md-center\" shape=\"semi-round\" style=\"float: left;\" (click)=\"editDis()\">View Passwords</button>\n<br>\n<!-- <button type=\"button \" class=\"btn btn-outline-primary justify-content-md-center\" shape=\"semi-round\"\n                style=\"float: right;\" (click)=\"editDis()\">View</button> -->\n<div class=\"container\" *ngIf=\"!creatShow\">\n    <ng2-smart-table [settings]=\"settings\" [source]=\"source\" (userRowSelect)=\"onUserRowSelect($event)\"></ng2-smart-table>\n</div>\n<div class=\"container\" *ngIf=\"creatShow\">\n    <ng2-smart-table [settings]=\"settingsPassword\" [source]=\"source\" (userRowSelect)=\"onUserRowSelects($event)\">\n    </ng2-smart-table>\n</div>\n<br>"

/***/ }),

/***/ "./src/app/pages/ui-features/amc/main-amc/elevator-passwords/elevator-passwords.component.scss":
/*!*****************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/amc/main-amc/elevator-passwords/elevator-passwords.component.scss ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "nb-card {\n  border: 0px solid #d5dbe0; }\n\nnb-card-body {\n  background-color: #e1f5fe; }\n\n:host /deep/ .hidetext {\n  -webkit-text-security: disc;\n  /* Default */ }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3ByYWthc2gvVmlwaW4vaXJvbmJpcmQvMjBGRUIyMDIwL2lyb25iaXJkLWNsaWVudHMvc3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9hbWMvbWFpbi1hbWMvZWxldmF0b3ItcGFzc3dvcmRzL2VsZXZhdG9yLXBhc3N3b3Jkcy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHlCQUF5QixFQUFBOztBQUUzQjtFQUNJLHlCQUF5QixFQUFBOztBQUU3QjtFQUNFLDJCQUEyQjtFQUMzQixZQUFBLEVBQWEiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9hbWMvbWFpbi1hbWMvZWxldmF0b3ItcGFzc3dvcmRzL2VsZXZhdG9yLXBhc3N3b3Jkcy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIm5iLWNhcmR7XG4gICAgYm9yZGVyOiAwcHggc29saWQgI2Q1ZGJlMDtcbiAgfVxuICBuYi1jYXJkLWJvZHl7XG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTFmNWZlO1xuICB9XG4gIDpob3N0IC9kZWVwLyAuaGlkZXRleHQge1xuICAgIC13ZWJraXQtdGV4dC1zZWN1cml0eTogZGlzYztcbiAgICAvKiBEZWZhdWx0ICovXG4gIH0iXX0= */"

/***/ }),

/***/ "./src/app/pages/ui-features/amc/main-amc/elevator-passwords/elevator-passwords.component.ts":
/*!***************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/amc/main-amc/elevator-passwords/elevator-passwords.component.ts ***!
  \***************************************************************************************************/
/*! exports provided: ElevatorPasswordsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ElevatorPasswordsComponent", function() { return ElevatorPasswordsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var angular_custom_modal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! angular-custom-modal */ "./node_modules/angular-custom-modal/angular-custom-modal.es5.js");
/* harmony import */ var _confirmation_dialog_confirmation_dialog_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../confirmation-dialog/confirmation-dialog.service */ "./src/app/confirmation-dialog/confirmation-dialog.service.ts");
/* harmony import */ var _password_edit_password_edit_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../password-edit/password-edit.service */ "./src/app/password-edit/password-edit.service.ts");
/* harmony import */ var _services_AMC_amc_passwords_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../../services/AMC/amc-passwords.service */ "./src/app/services/AMC/amc-passwords.service.ts");
/* harmony import */ var _services_AMC_amc_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../../services/AMC/amc.service */ "./src/app/services/AMC/amc.service.ts");
/* harmony import */ var _services_token_token_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../../services/token/token.service */ "./src/app/services/token/token.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








var ElevatorPasswordsComponent = /** @class */ (function () {
    function ElevatorPasswordsComponent(AmcPass, userServ, AmcServ, confirmationDialogService, PasswordEditService) {
        this.AmcPass = AmcPass;
        this.userServ = userServ;
        this.AmcServ = AmcServ;
        this.confirmationDialogService = confirmationDialogService;
        this.PasswordEditService = PasswordEditService;
        this.data2 = {};
        this.data1 = {};
        this.listOfAmc = [];
        this.AmcChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.source = new ng2_smart_table__WEBPACK_IMPORTED_MODULE_1__["LocalDataSource"](); // add a property to the component
        this.moduleCreate = true;
        this.moduleEdit = true;
        this.moduleView = true;
        this.settings = {
            actions: {
                edit: false,
                add: false,
                delete: false,
            },
            columns: {
                passType: {
                    title: 'Password Type',
                    type: 'string'
                },
                passwords: {
                    title: 'Password ',
                    type: "html",
                    valuePrepareFunction: function (cell, row) {
                        return "<div class=\"hidetext\"  >" + cell + "</div>";
                    }
                },
            },
        };
        this.settingsPassword = {
            actions: {
                edit: false,
                add: false,
                delete: false,
            },
            columns: {
                passType: {
                    title: 'Password Type',
                    type: 'string'
                },
                passwords: {
                    title: 'Password ',
                    type: "string",
                },
            },
        };
        this.passwords = new ng2_smart_table__WEBPACK_IMPORTED_MODULE_1__["LocalDataSource"]();
        this.getAmcDetail();
    }
    ElevatorPasswordsComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.userServ.userAccess !== undefined) {
            this.userServ.userAccess
                .subscribe(function (data) {
                data.forEach(function (ele) {
                    if (ele.name === 'AMC  Details') {
                        _this.moduleCreate = ele.create;
                        _this.moduleEdit = ele.edit;
                        _this.moduleView = ele.view;
                    }
                });
            }, function (error) {
                console.log(error);
            });
        }
        // this.getAMCDetails();
    };
    ElevatorPasswordsComponent.prototype.getAmcDetail = function () {
        var _this = this;
        this.AmcServ.AmcDone
            .subscribe(function (data) {
            _this.data1 = data;
            // this.elevator = this.data1.invoiceItems[0];
            _this.id = _this.data1.id;
            _this.siteId = _this.data1.siteId;
            // console.log(this.siteId)
            _this.getAMCDetails();
        }, function (error) {
            console.log(error);
        });
    };
    ElevatorPasswordsComponent.prototype.getAMCDetails = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, error_1;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        _a = this;
                        return [4 /*yield*/, this.AmcPass.getAllAmcId(this.siteId)];
                    case 1:
                        _a.listOfAmc = _b.sent();
                        this.listOfAmc.reverse();
                        this.source.load(this.listOfAmc);
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _b.sent();
                        console.error(error_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ElevatorPasswordsComponent.prototype.createAMCPasswords = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            var resultAleart, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.confirmationDialogService.confirm('Alert Message', 'Are you sure you want to Save?')];
                    case 1:
                        resultAleart = _a.sent();
                        if (!resultAleart) return [3 /*break*/, 3];
                        Object.assign(data.value, { siteId: this.siteId });
                        return [4 /*yield*/, this.AmcPass.createAmc(data.value)];
                    case 2:
                        result = _a.sent();
                        this.AmcChange.emit(result);
                        this.listOfAmc.push(result);
                        this.source.load(this.listOfAmc);
                        this.formValues.resetForm();
                        _a.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ElevatorPasswordsComponent.prototype.onUserRowSelect = function (event) {
        var data = event.data;
        this.data2 = data;
        // console.log(this.data2)
    };
    ElevatorPasswordsComponent.prototype.editSite = function (event) {
        return __awaiter(this, void 0, void 0, function () {
            var resultAleart, value;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.confirmationDialogService.confirm('Alert Message', 'Are you sure you want to Update?')];
                    case 1:
                        resultAleart = _a.sent();
                        if (resultAleart) {
                            value = event.value;
                        }
                        else {
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    ElevatorPasswordsComponent.prototype.editDis = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.PasswordEditService.confirm('Alert Message', 'Enter Super Admin Password', 'success')];
                    case 1:
                        result2 = _a.sent();
                        if (result2) {
                            if (this.data === 'data') {
                                return [2 /*return*/, this.creatShow = false];
                            }
                            this.creatShow = true;
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('componentInsideModals'),
        __metadata("design:type", angular_custom_modal__WEBPACK_IMPORTED_MODULE_2__["ModalComponent"])
    ], ElevatorPasswordsComponent.prototype, "componentInsideModals", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('componentInsidePrint'),
        __metadata("design:type", angular_custom_modal__WEBPACK_IMPORTED_MODULE_2__["ModalComponent"])
    ], ElevatorPasswordsComponent.prototype, "componentInsidePrint", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('AmcPasswords'),
        __metadata("design:type", Object)
    ], ElevatorPasswordsComponent.prototype, "formValues", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], ElevatorPasswordsComponent.prototype, "AmcChange", void 0);
    ElevatorPasswordsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-elevator-passwords',
            template: __webpack_require__(/*! ./elevator-passwords.component.html */ "./src/app/pages/ui-features/amc/main-amc/elevator-passwords/elevator-passwords.component.html"),
            styles: [__webpack_require__(/*! ./elevator-passwords.component.scss */ "./src/app/pages/ui-features/amc/main-amc/elevator-passwords/elevator-passwords.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_AMC_amc_passwords_service__WEBPACK_IMPORTED_MODULE_5__["AmcPasswordsService"],
            _services_token_token_service__WEBPACK_IMPORTED_MODULE_7__["TokenService"],
            _services_AMC_amc_service__WEBPACK_IMPORTED_MODULE_6__["AmcService"],
            _confirmation_dialog_confirmation_dialog_service__WEBPACK_IMPORTED_MODULE_3__["ConfirmationDialogService"],
            _password_edit_password_edit_service__WEBPACK_IMPORTED_MODULE_4__["PasswordEditService"]])
    ], ElevatorPasswordsComponent);
    return ElevatorPasswordsComponent;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/amc/main-amc/main-amc.component.html":
/*!************************************************************************!*\
  !*** ./src/app/pages/ui-features/amc/main-amc/main-amc.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nb-card>\n    <ngb-tabset>\n        <ngb-tab title=\"Details\" *ngIf='amcdetails'>\n            <ng-template ngbTabContent>\n                <!-- <nb-card-header>wellcome</nb-card-header> -->\n                <ngx-create-update-amc></ngx-create-update-amc>\n            </ng-template>\n        </ngb-tab>\n\n        <ngb-tab title=\"AMC Documents\" *ngIf='amcAttachments'>\n            <ng-template ngbTabContent>\n                <ngx-upload-amc-doc></ngx-upload-amc-doc>\n            </ng-template>\n        </ngb-tab>\n        <ngb-tab title=\"Elevator Passwords\" *ngIf='amcPass'>\n            <ng-template ngbTabContent>\n                <ngx-elevator-passwords></ngx-elevator-passwords>\n            </ng-template>\n        </ngb-tab>\n        <ngb-tab title=\"Elevator Maintenance Report\" *ngIf='amcReports'>\n            <ng-template ngbTabContent>\n                <ngx-maintance-report></ngx-maintance-report>\n            </ng-template>\n        </ngb-tab>\n\n    </ngb-tabset>\n\n</nb-card>"

/***/ }),

/***/ "./src/app/pages/ui-features/amc/main-amc/main-amc.component.scss":
/*!************************************************************************!*\
  !*** ./src/app/pages/ui-features/amc/main-amc/main-amc.component.scss ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "nb-card {\n  background-color: #E1F5FE; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3ByYWthc2gvVmlwaW4vaXJvbmJpcmQvMjBGRUIyMDIwL2lyb25iaXJkLWNsaWVudHMvc3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9hbWMvbWFpbi1hbWMvbWFpbi1hbWMuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSx5QkFBeUIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VpLWZlYXR1cmVzL2FtYy9tYWluLWFtYy9tYWluLWFtYy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIm5iLWNhcmQge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNFMUY1RkU7XG59Il19 */"

/***/ }),

/***/ "./src/app/pages/ui-features/amc/main-amc/main-amc.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/pages/ui-features/amc/main-amc/main-amc.component.ts ***!
  \**********************************************************************/
/*! exports provided: MainAMCComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainAMCComponent", function() { return MainAMCComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_token_token_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../services/token/token.service */ "./src/app/services/token/token.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MainAMCComponent = /** @class */ (function () {
    function MainAMCComponent(token) {
        this.token = token;
        this.amcAttachments = false;
        this.amcPass = false;
        this.amcReports = false;
        this.amcdetails = false;
    }
    MainAMCComponent.prototype.ngOnInit = function () {
        this.tokens = this.token.getDecodedToken();
        this.amcPass = this.tokens.rolesHideAndShow.amcPass;
        this.amcAttachments = this.tokens.rolesHideAndShow.amcAttachments;
        this.amcReports = this.tokens.rolesHideAndShow.amcReports;
        this.amcdetails = this.tokens.rolesHideAndShow.amcdetails;
    };
    MainAMCComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-main-amc',
            template: __webpack_require__(/*! ./main-amc.component.html */ "./src/app/pages/ui-features/amc/main-amc/main-amc.component.html"),
            styles: [__webpack_require__(/*! ./main-amc.component.scss */ "./src/app/pages/ui-features/amc/main-amc/main-amc.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_token_token_service__WEBPACK_IMPORTED_MODULE_1__["TokenService"]])
    ], MainAMCComponent);
    return MainAMCComponent;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/amc/main-amc/maintance-report/maintance-report.component.html":
/*!*************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/amc/main-amc/maintance-report/maintance-report.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <div class=\"row\">\n        <div class=\"col-md-12\" style=\" background-color: #e1f5fe;\">\n            <nb-card-header>\n                <p class=\"back\"> Client Name: {{data1.customerName}}&nbsp; And &nbsp;Site Name:{{data1.siteName}}</p>\n                <button style=\"float: right;\" *ngIf='moduleCreate || moduleEdit' class=\"btn btn-outline-primary \" shape=\"semi-round\" (click)=\"create()\" mdbWavesEffect>Create Maintenance Report</button>\n                <!-- <button style=\"float: left;\" *ngIf='moduleCreate || moduleEdit' class=\"btn btn-outline-primary \" shape=\"semi-round\" (click)=\"componentInsideModals.open()\" mdbWavesEffect>Create Maintenance Report</button> -->\n                <label>Select Year</label>\n                <select class=\"form-group\" (change)=\"changeYear($event.target.value)\" style=\"width: 13%;margin-left: 2%;\" [(ngModel)]='yearCurrent'>\n                    <option value=\"\" disabled>Select Year</option>\n                    <option *ngFor=\"let year of range\" [ngValue]=\"year\">{{year}}</option>\n                 </select>\n            </nb-card-header>\n            <nb-card-body>\n                <div class=\"col-12\" style=\"margin-bottom: 2%;margin-bottom: 2%;text-align: center;\" *ngIf='listOfAmcDisplay.length === 0'>\n                    <label style=\"font-weight: 600;font-size: xx-large;\">No Data Found</label>\n                </div>\n                <div class=\"col-12\" style=\"margin-bottom: 2%;margin-top: 2%;\" *ngIf='listOfAmcDisplay.length !== 0'>\n                    <table class=\"table table-bordered table-striped table-responsive-md btn-table\" style=\"text-align: center;background: white;\">\n                        <thead>\n                            <tr>\n                                <th>Month</th>\n                                <th>Date Attended</th>\n                                <th>Maintenance Work Description</th>\n                                <th>Contract Type</th>\n                                <th>View</th>\n                                <th>Edit</th>\n                            </tr>\n                        </thead>\n                        <tbody>\n                            <tr *ngFor=\"let list of listOfAmcDisplay | filter:filter  | paginate: { itemsPerPage: 10, currentPage: p }; let i = index\">\n                                <td>{{list.month}}</td>\n                                <td>{{list.compaintDate.split('-').reverse().join('/')}}</td>\n                                <td>{{list.work}}</td>\n                                <td>{{list.contract}}</td>\n                                <td><i class=\"fa fa-eye\" style=\"cursor: pointer;\" (click)='view(list.id)' title=\"View Details\"></i></td>\n                                <td><i class=\"fa fa-edit\" style=\"cursor: pointer;\" (click)='edit(list.id)' title=\"Edit Details\"></i></td>\n                            </tr>\n                        </tbody>\n                    </table>\n                </div>\n                <div class=\"col-12\" *ngIf='listOfAmcDisplay.length !== 0'>\n                    <pagination-controls style=\"float: right;\" (pageChange)=\"p = $event\"></pagination-controls>\n                </div>\n                <!-- <ng2-smart-table [settings]=\"settings\" [source]=\"source\" (userRowSelect)=\"onUserRowSelect($event)\"></ng2-smart-table> -->\n            </nb-card-body>\n        </div>\n    </div>\n</div>\n\n\n<modal class=\"mode modes\" #componentInsideModals>\n    <ng-template #modalHeader>\n        <h4 class=\"text-center\"> Maintenance/ Breakdown Attendance Report </h4>\n        <br><br>\n\n    </ng-template>\n    <ng-template #modalBody>\n        <form #AmcReport=\"ngForm\" name=\"form\" (ngSubmit)=\"AmcReport.form.valid && createAMCReport(AmcReport)\">\n            <div class=\"container\">\n                <div class=\"row\">\n                    <div class=\"col-md-3\">\n                        <div class=\"form-group input-group-sm\">\n                            <label class=\"clientName\">Client Name</label>\n                            <input type=\"text\" class=\"form-control\" name=\"customerName\" [(ngModel)]=\"data1.customerName\" placeholder=\"Client Name\" #customerName=\"ngModel\" [ngClass]=\"{ 'is-invalid': AmcReport.submitted && customerName.invalid}\" required>\n\n                            <div class=\"invalid-feedback\" *ngIf=\"AmcReport.submitted && customerName.invalid\">\n                                <div *ngIf=\"customerName.errors.required\"> Client Name is Required</div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"col-md-3\">\n                        <div class=\"form-group input-group-sm\">\n                            <label class=\"clientName\">Installation Address</label>\n                            <input type=\"text\" class=\"form-control\" name=\"clientAddress\" [(ngModel)]=\"data1.clientAddress\" placeholder=\"Installation Address\">\n                        </div>\n                    </div>\n                    <div class=\"col-md-3\">\n                        <div class=\"form-group input-group-sm\">\n                            <label class=\"clientName\">Contact Person</label>\n                            <input type=\"text\" class=\"form-control\" name=\"PersonName\" [(ngModel)]=\"data1.PersonName\" placeholder=\"Contact Person\">\n                        </div>\n                    </div>\n                    <div class=\"col-md-3\">\n                        <div class=\"form-group input-group-sm\">\n                            <label class=\"clientName\">Telephone Number</label>\n                            <input type=\"text\" class=\"form-control\" name=\"telephone\" [(ngModel)]=\"data1.telephone\" placeholder=\"Telephone Number\">\n                        </div>\n                    </div>\n                </div>\n                <div class=\"row\">\n                    <div class=\"col-md-3\">\n                        <div class=\"form-group input-group-sm\">\n                            <label class=\"clientName\">Complaint Time</label>\n                            <input type=\"time\" class=\"form-control\" name=\"compaintTime\" placeholder=\"Complaint Time\" [(ngModel)]=\"data1.compaintTime\" value=\"{{today | date:'shortTime'}}\">\n                        </div>\n                    </div>\n                    <div class=\"col-md-3\">\n                        <div class=\"form-group input-group-sm\">\n                            <label class=\"clientName\">Date</label>\n                            <input type=\"date\" class=\"form-control\" (change)='date($event.target.value)' name=\"compaintDate\" [(ngModel)]=\"data1.compaintDate\" placeholder=\"Date\" value=\"{{today | date}}\">\n                        </div>\n                    </div>\n                    <div class=\"col-md-3\">\n                        <div class=\"form-group input-group-sm\">\n                            <label class=\"clientName\">Job No</label>\n                            <input type=\"text\" class=\"form-control\" name=\"jobid\" [(ngModel)]=\"data1.jobid\" placeholder=\"Job No\">\n                        </div>\n                    </div>\n                    <div class=\"col-md-3\">\n                        <div class=\"form-group input-group-sm\">\n                            <label class=\"clientName\">SL.No</label>\n                            <input type=\"text\" class=\"form-control\" name=\"SlNo\" [(ngModel)]=\"data1.SlNo\" placeholder=\"SI.No\">\n                        </div>\n                    </div>\n                </div>\n                <div class=\"row\">\n                    <div class=\"col-md-3\">\n                        <div class=\"form-group input-group-sm\">\n                            <label class=\"clientName\">Branch</label>\n                            <input type=\"text\" class=\"form-control\" name=\"siteName\" [(ngModel)]=\"data1.siteName\" placeholder=\"Branch\">\n                        </div>\n                    </div>\n                    <div class=\"col-md-3\">\n                        <div class=\"form-group input-group-sm\">\n                            <label class=\"clientName\">Depot</label>\n                            <input type=\"text\" class=\"form-control\" name=\"Depot\" [(ngModel)]=\"data1.Depot\" placeholder=\"Depot\">\n                        </div>\n                    </div>\n                    <div class=\"col-md-3\">\n                        <div class=\"form-group input-group-sm\">\n                            <label class=\"clientName\">Elevator</label>\n                            <input type=\"text\" class=\"form-control\" name=\"lift_type\" [(ngModel)]=\"elevator.lift_type\" placeholder=\"Elevator\">\n                        </div>\n                    </div>\n                    <div class=\"col-md-3\">\n                        <div class=\"form-group input-group-sm\">\n                            <label class=\"clientName\">Make</label>\n                            <input type=\"text\" class=\"form-control\" name=\"make\" [(ngModel)]=\"data1.make\" placeholder=\"Make\">\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"row\">\n                    <div class=\"col-sm-6\">\n                        <div class=\"form-group input-group-sm\">\n                            <label>Contract</label>\n                            <select class=\"form-control\" size=\"small\" [(ngModel)]=\"data1.contract\" name=\"contract\">\n                <option disabled>Select One</option>\n                <option>Free</option>\n                <option>Normal</option>\n                <option>Semi</option>\n                <option>Comp.</option>\n\n              </select>\n\n                        </div>\n                    </div>\n\n                    <div class=\"col-sm-6\">\n                        <div class=\"form-group input-group-sm\">\n                            <label>Contract Valid till</label>\n                            <input type=\"text\" class=\"form-control\" name=\"toData\" [(ngModel)]=\"data1.toData\" placeholder=\"Contract Valid Date\">\n                        </div>\n                    </div>\n\n                </div>\n\n                <div class=\"row\">\n                    <div class=\"col-sm-6\">\n                        <div class=\"form-group input-group-sm\">\n                            <label>Type of Building</label>\n                            <select class=\"form-control\" size=\"small\" [(ngModel)]=\"data1.building\" name=\"building\" (click)=\"show = !show\">\n                <option disabled>Select One</option>\n                <option>Commercial</option>\n                <option>Residential</option>\n                <option>Hotel</option>\n                <option>Hospital</option>\n                <option>Other</option>\n              </select>\n                            <!-- <div *ngIf=\"show\" class=\"form-group input-group-sm\">\n                <br>\n                <input type=\"text\" class=\"form-control\" \n                  placeholder=\"If other selected Means Please fill this field it\">\n              </div> -->\n                        </div>\n                    </div>\n\n                    <div class=\"col-sm-6\">\n                        <div class=\"form-group input-group-sm\">\n                            <label>Description of work</label>\n                            <select class=\"form-control\" size=\"small\" [(ngModel)]=\"data1.work\" name=\"work\">\n                <option disabled>Select One</option>\n                <option>Breakdown</option>\n                <option>Routine Maintenance</option>\n                <option>Annual Inspection</option>\n                <option>Safety Check</option>\n                <option>Equipment Survey</option>\n              </select>\n\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"row\">\n                    <div class=\"col-md-4\">\n                        <div class=\"form-group input-group-sm\">\n                            <label class=\"clientName\">Complaint Received At</label>\n                            <input type=\"text\" class=\"form-control\" name=\"received\" [(ngModel)]=\"data1.received\" placeholder=\"Complaint Received At\">\n                        </div>\n                    </div>\n                    <div class=\"col-md-4\">\n                        <div class=\"form-group input-group-sm\">\n                            <label class=\"clientName\">Reporting Time</label>\n                            <input type=\"text\" class=\"form-control\" name=\"reportTime\" [(ngModel)]=\"data1.reportTime\" placeholder=\"Reporting Time\">\n                        </div>\n                    </div>\n                    <div class=\"col-md-4\">\n                        <div class=\"form-group input-group-sm\">\n                            <label class=\"clientName\">Restoration At</label>\n                            <input type=\"text\" class=\"form-control\" name=\"restoration\" [(ngModel)]=\"data1.restoration\" placeholder=\"Restoration At\">\n                        </div>\n                    </div>\n\n                </div>\n\n                <div class=\"row\">\n                    <div class=\"col-md-6\">\n                        <div class=\"form-group input-group-sm\">\n                            <label>Names of Tech./EngrBranch</label>\n                            <textarea class=\"form-control\" [(ngModel)]=\"data1.Tech_EngrBrach\" name=\"Tech_EngrBranch\" rows=\"6\"></textarea>\n                        </div>\n                    </div>\n                    <div class=\"col-md-6\">\n                        <div class=\"form-group input-group-sm\">\n                            <label>Rectification done</label>\n                            <textarea class=\"form-control\" [(ngModel)]=\"data1.rectification\" name=\"rectification\" rows=\"6\"></textarea>\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"row\">\n                    <div class=\"col-md-6\">\n                        <div class=\"form-group input-group-sm\">\n                            <br>\n                            <label>Fault Reported</label>\n                            <textarea class=\"form-control\" rows=\"6\" [(ngModel)]=\"data1.faultReport\" name=\"faultReport\"></textarea>\n                        </div>\n                    </div>\n                    <div class=\"col-md-6\">\n                        <div class=\"form-group input-group-sm\">\n                            <label>Spares Recommended for Replacement/Repair, not covered under maintenance Contract</label>\n                            <textarea class=\"form-control\" [(ngModel)]=\"data1.Replacement_Repair\" name=\"Replacement_Repair\" rows=\"6\"></textarea>\n                        </div>\n                    </div>\n                </div>\n\n\n                <div class=\"row\">\n                    <div class=\"col-12\">\n                        <p>HandOver the Elevator in safe working condition</p>\n                        <hr>\n                        <div class=\"form-group input-group-sm\">\n                            <label>Observations/ Remarks</label>\n                            <textarea class=\"form-control\" [(ngModel)]=\"data1.observations_remarks\" name=\"observations_remarks\" rows=\"4\"></textarea>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"row\">\n                    <div class=\"col-sm-4\">\n                        <div class=\"form-group input-group-sm\">\n                            <label>Name of Tech./Engr</label>\n                            <input type=\"text\" class=\"form-control\" [(ngModel)]=\"data1.employee\" name=\"employee\" placeholder=\"Name of Tech./Engr\">\n                        </div>\n                    </div>\n                    <div class=\"col-sm-4\">\n                        <div class=\"form-group input-group-sm\">\n                            <label>Signature</label>\n                            <!-- <signature-pad [options]=\"signaturePadOption\"   (onEndEvent)=\"drawComplete()\"></signature-pad> -->\n                            <signature-pad #sigpad1 [options]=\"signaturePadOptions\" (onBeginEvent)=\"drawStart()\" (onEndEvent)=\"drawComplete()\"></signature-pad>\n                            <button type=\"button\" (click)=\"signaturePad.clear()\">clear</button>\n                            <!-- <textarea class=\"form-control\" rows=\"4\" [options]=\"signaturePadOptions\" (onBeginEvent)=\"drawStart()\" (onEndEvent)=\"drawComplete()\"></textarea> -->\n                        </div>\n                    </div>\n                    <div class=\"col-sm-4\">\n                        <div class=\"form-group input-group-sm\">\n                            <label>Date</label>\n                            <input type=\"date\" class=\"form-control\" [(ngModel)]=\"data1.WorkDate\" name=\"WorkDate\" value=\"{{today | date}}\" placeholder=\"Date\">\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"row\">\n                    <div class=\"col-12\">\n                        <p>Accepted the elevator in safe working condition after the maintenance work</p>\n                        <hr>\n                        <div class=\"form-group input-group-sm\">\n                            <label>Remarks / Feedback</label>\n                            <textarea class=\"form-control\" [(ngModel)]=\"data1.remarks_Feedback\" name=\"remarks_Feedback\" rows=\"4\"></textarea>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"row\">\n                    <div class=\"col-sm-4\">\n                        <div class=\"form-group input-group-sm\">\n                            <label>Name of client</label>\n                            <input type=\"text\" class=\"form-control\" [(ngModel)]=\"data1.customerName\" name=\"customerName\" placeholder=\"Name of client\" #customerName=\"ngModel\" [ngClass]=\"{ 'is-invalid': AmcReport.submitted && customerName.invalid}\" required>\n\n                            <div class=\"invalid-feedback\" *ngIf=\"AmcReport.submitted && customerName.invalid\">\n                                <div *ngIf=\"customerName.errors.required\"> Client Name is Required</div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"col-sm-4\">\n                        <div class=\"form-group input-group-sm\">\n                            <label>Signature</label>\n                            <!-- <signature-pad [options]=\"signaturePadOptions\"  (onEndEvent)=\"drawCompleteForCustomer()\" ></signature-pad> -->\n                            <signature-pad #sigpad2 [options]=\"signaturePadOptions\" (onBeginEvent)=\"drawStarts()\" (onEndEvent)=\"drawCompleteForCustomer()\"></signature-pad>\n                            <!-- <textarea class=\"form-control\" rows=\"4\"></textarea> -->\n                            <button type=\"button\" (click)=\"this.sigpad2.clear()\">clear</button>\n\n\n                        </div>\n                    </div>\n                    <div class=\"col-sm-4\">\n                        <div class=\"form-group input-group-sm\">\n                            <label>Date</label>\n                            <input type=\"date\" class=\"form-control\" name=\"handDate\" [(ngModel)]=\"data1.handDate\" value=\"{{today | date}}\" placeholder=\"Date\">\n                        </div>\n                    </div>\n                </div>\n\n            </div>\n            <button type=\"button\" class=\"btn btn-outline-primary \" shape=\"semi-round\" style=\"float: left;\" (click)=\"componentInsideModals.close()\">Cancel</button>\n            <!-- <button class=\"btn btn-outline-primary \" style=\"float: left;\" shape=\"semi-round\" (click)=\"componentInsidePrint.open()\"\n        mdbWavesEffect>Print Preview</button> -->\n            <button *ngIf='!editDetails' type=\"submit\" class=\"btn btn-outline-primary \" shape=\"semi-round\">Save</button>\n            <button *ngIf='editDetails' type=\"submit\" class=\"btn btn-outline-primary \" shape=\"semi-round\">Update</button>\n        </form>\n\n\n    </ng-template>\n    <!-- <ng-template #modalFooter>\n    \n  </ng-template> -->\n</modal>\n\n\n\n<modal class=\"mode\" #componentInsidePrint>\n    <ng-template #modalHeader>\n        <h4 class=\"text-center\"> Maintenance/ Breakdown Attendance Report </h4>\n        <br><br>\n\n    </ng-template>\n    <ng-template #modalBody>\n        <div class=\"container\">\n            <div class=\"A4\" id=\"maintancesPage\">\n                <div class=\"col-12\">\n                    <p class=MsoNormal>&nbsp;</p>\n                    <nb-card-header>\n                        <img src=\"assets/images/banner.png\">\n                        <p class=\"parag\">Maintenance/ Breakdown Attendance Report</p>\n                    </nb-card-header>\n\n\n                    <table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0 width=1884 style='width:100%;border-collapse:collapse;border:none'>\n                        <tr style='height:23.6pt'>\n                            <td width=627 colspan=2 valign=top style='width:470.6pt;border:solid windowtext 1.0pt;\n  padding:0in 5.4pt 0in 5.4pt;height:23.6pt'>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>&nbsp;</b></p>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>Branch: {{data2.siteName}}                              Depot: {{data2.Depot}}</b></p>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>&nbsp;</b></p>\n                            </td>\n                            <td width=628 colspan=2 rowspan=3 valign=top style='width:470.65pt;\n  border:solid windowtext 1.0pt;border-left:none;padding:0in 5.4pt 0in 5.4pt;\n  height:23.6pt'>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>&nbsp;</b></p>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>Customer Name: {{data2.customerName}}</b></p>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>&nbsp;</b></p>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>Installation Address: {{data2.clientAddress}}</b></p>\n                            </td>\n                            <td width=629 colspan=2 valign=top style='width:471.95pt;border:solid windowtext 1.0pt;\n  border-left:none;padding:0in 5.4pt 0in 5.4pt;height:23.6pt'>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>&nbsp;</b></p>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>Complaint Time: {{data2.compaintTime}}</b></p>\n                            </td>\n                        </tr>\n                        <tr style='height:27.25pt'>\n                            <td width=627 colspan=2 valign=top style='width:470.6pt;border:solid windowtext 1.0pt;\n  border-top:none;padding:0in 5.4pt 0in 5.4pt;height:27.25pt'>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>&nbsp;</b></p>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>Elevator:  {{data2.lift_type}}                   Make: {{data2.make}}</b></p>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>&nbsp;</b></p>\n                            </td>\n                            <td width=629 colspan=2 valign=top style='width:471.95pt;border-top:solid black 1px;\n  border-left:solid black 1px;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;\n  padding:0in 5.4pt 0in 5.4pt;height:27.25pt'>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>&nbsp;</b></p>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>Date: {{data2.compaintDate.split('-').reverse().join('/')}}</b></p>\n                            </td>\n                        </tr>\n                        <tr style='height:28.15pt'>\n                            <td width=627 colspan=2 valign=top style='width:470.6pt;border:solid windowtext 1.0pt;\n  border-top:none;padding:0in 5.4pt 0in 5.4pt;height:28.15pt'>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>&nbsp;</b></p>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>Contact: {{data2.contract}}</b></p>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>&nbsp;</b></p>\n                            </td>\n                            <td width=629 colspan=2 valign=top style='width:471.95pt;border-top:solid black 1px;\n  border-left:solid black 1px;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;\n  padding:0in 5.4pt 0in 5.4pt;height:28.15pt'>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>&nbsp;</b></p>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>Job No: {{data2.jobid}}</b></p>\n                            </td>\n                        </tr>\n                        <tr style='height:29.05pt'>\n                            <td width=627 colspan=2 valign=top style='width:470.6pt;border:solid windowtext 1.0pt;\n  border-top:none;padding:0in 5.4pt 0in 5.4pt;height:29.05pt'>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>&nbsp;</b></p>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>Contact Valid till: {{data2.toData.split('-').reverse().join('/')}}</b></p>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>&nbsp;</b></p>\n                            </td>\n                            <td width=628 colspan=2 valign=top style='width:470.65pt;border-top:none;\n  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;\n  padding:0in 5.4pt 0in 5.4pt;height:29.05pt'>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>&nbsp;</b></p>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>Contact Person &amp; Tel.No: {{data2.PersonName}} &amp; {{data2.telephone}}</b></p>\n                            </td>\n                            <td width=629 colspan=2 valign=top style='width:471.95pt;border-top:none;\n  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;\n  padding:0in 5.4pt 0in 5.4pt;height:29.05pt'>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>&nbsp;</b></p>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>SlNo: {{data2.SlNo}}</b></p>\n                            </td>\n                        </tr>\n                        <tr style='height:29.95pt'>\n                            <td width=1884 colspan=6 valign=top style='width:1413.2pt;border:solid windowtext 1.0pt;\n  border-top:none;padding:0in 5.4pt 0in 5.4pt;height:29.95pt'>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>Type of Building: {{data2.building}}</b></p>\n                            </td>\n                        </tr>\n                        <tr style='height:25.45pt'>\n                            <td width=1884 colspan=6 valign=top style='width:1413.2pt;border:solid windowtext 1.0pt;\n  border-top:none;padding:0in 5.4pt 0in 5.4pt;height:25.45pt'>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>&nbsp;</b></p>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>Description of Work: {{data2.work}}</b></p>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>&nbsp;</b></p>\n                            </td>\n                        </tr>\n                        <tr style='height:99.9pt'>\n                            <td width=946 colspan=3 valign=top style='width:709.15pt;border:solid windowtext 1.0pt;\n  border-top:none;padding:0in 5.4pt 0in 5.4pt;height:99.9pt'>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>&nbsp;</b></p>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>Name of Tech./EngrBranch: {{data2.Tech_EngrBrach}}</b></p>\n                            </td>\n                            <td width=939 colspan=3 valign=top style='width:704.05pt;border-top:none;\n  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;\n  padding:0in 5.4pt 0in 5.4pt;height:99.9pt'>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>&nbsp;</b></p>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>Fault Report: {{data2.faultReport}}</b></p>\n                            </td>\n                        </tr>\n                        <tr style='height:156.75pt'>\n                            <td width=946 colspan=3 valign=top style='width:709.15pt;border:solid windowtext 1.0pt;\n  border-top:none;padding:0in 5.4pt 0in 5.4pt;height:156.75pt'>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>&nbsp;</b></p>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>Rectification Done: {{data2.rectification}}</b></p>\n                            </td>\n                            <td width=939 colspan=3 valign=top style='width:704.05pt;border-top:none;\n  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;\n  padding:0in 5.4pt 0in 5.4pt;height:156.75pt'>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>&nbsp;</b></p>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>Spares Recommended for Replacement/Repair, not covered under\n  Maintenance Contact: {{data2.Replacement_Repair}}</b></p>\n                            </td>\n                        </tr>\n                        <tr style='height:20.25pt'>\n                            <td width=946 colspan=3 valign=top style='width:709.15pt;border:solid windowtext 1.0pt;\n  border-top:none;padding:0in 5.4pt 0in 5.4pt;height:20.25pt'>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>&nbsp;</b></p>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>Handed over the Elevator in safe working condition.</b></p>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>&nbsp;</b></p>\n                            </td>\n                            <td width=939 colspan=3 valign=top style='width:704.05pt;border-top:none;\n  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;\n  padding:0in 5.4pt 0in 5.4pt;height:20.25pt'>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>&nbsp;</b></p>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>Accepted the elevator in  safe working condition after the\n  maintenance work</b></p>\n                            </td>\n                        </tr>\n                        <tr style='height:28.5pt'>\n                            <td width=423 valign=top style='width:317.25pt;border:solid windowtext 1.0pt;\n  border-top:none;padding:0in 5.4pt 0in 5.4pt;height:28.5pt'>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>&nbsp;</b></p>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>Name Of Tech/Engr.: {{data2.employee}}</b></p>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>&nbsp;</b></p>\n                            </td>\n                            <td width=523 colspan=2 rowspan=3 valign=top style='width:391.9pt;border-top:\n  none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;\n  padding:0in 5.4pt 0in 5.4pt;height:28.5pt'>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>&nbsp;</b></p>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>Observations/Remark </b><br>{{data2.observations_remarks}}</p>\n                            </td>\n                            <td width=467 colspan=2 valign=top style='width:350.25pt;border-top:solid black 1px;\n  border-left:solid black 1px;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;\n  padding:0in 5.4pt 0in 5.4pt;height:28.5pt'>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>&nbsp;</b></p>\n                            </td>\n                            <td width=472 rowspan=3 valign=top style='width:353.8pt;border-top:solid black 1px;\n  border-left:solid black 1px;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;\n  padding:0in 5.4pt 0in 5.4pt;height:28.5pt'>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>&nbsp;</b></p>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>Remarks/ Feedback</b>\n                                    <br> {{data2.remarks_Feedback}}\n                                </p>\n                            </td>\n                        </tr>\n                        <tr style='height:96.0pt'>\n                            <td width=423 valign=top style='width:317.25pt;border:solid windowtext 1.0pt;\n  border-top:solid black 1px;padding:0in 5.4pt 0in 5.4pt;height:96.0pt ;border-right:solid windowtext 1.0pt;'>\n                                <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>&nbsp;</b></p>\n                                <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>&nbsp;</b></p>\n                                <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>&nbsp;</b></p>\n\n                                <br>\n                                <p>\n                                    <img [src]=\"data2.employeeSignature\" width=\"100%\" style=\"width: 100%;margin: 0px;\"></p>\n                                <br><br>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>Signature:</b>\n                                </p>\n\n                            </td>\n                            <td width=467 colspan=2 valign=top style='width:350.25pt;border-top:solid black 1px;\n  border-right:solid black 1px;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;\n  padding:0in 5.4pt 0in 5.4pt;height:96.0pt'>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>&nbsp;</b></p>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>&nbsp;</b></p>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>&nbsp;</b></p>\n                                <br>\n                                <p>\n                                    <img [src]=\"data2.customerSignature\" width=\"100%\" style=\"width: 100%;margin: 0px;\"></p>\n                                <br><br>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>Signature:</b><br>\n                                    <!-- {{data.}} -->\n                                </p>\n                            </td>\n                        </tr>\n                        <tr style='height:29.25pt'>\n                            <td width=423 valign=top style='width:317.25pt;border:solid windowtext 1.0pt;\n  border-top:none;padding:0in 5.4pt 0in 5.4pt;height:29.25pt'>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>&nbsp;</b></p>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>Date:</b>\n                                    <br>{{data2.WorkDate}}\n                                </p>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>&nbsp;</b></p>\n                            </td>\n\n\n                            <td width=467 colspan=2 valign=top style='width:350.25pt;border-top:solid black 1px;\n  border-left:solid black 1px;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;\n  padding:0in 5.4pt 0in 5.4pt;height:29.25pt'>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>&nbsp;</b></p>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>Date:</b>\n                                    <br>{{data2.handDate}} </p>\n                            </td>\n                        </tr>\n                        <tr style='height:33.75pt'>\n                            <td width=1884 colspan=6 valign=top style='width:1413.2pt;border:solid windowtext 1.0pt;\n  border-top:solid black 1px;padding:0in 5.4pt 0in 5.4pt;height:33.75pt'>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>&nbsp;</b></p>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>For Office Use Only</b></p>\n                            </td>\n                        </tr>\n                        <tr style='height:32.25pt'>\n                            <td width=1884 colspan=6 valign=top style='width:1413.2pt;border:solid windowtext 1.0pt;\n  border-top:none;padding:0in 5.4pt 0in 5.4pt;height:32.25pt'>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>&nbsp;</b></p>\n                            </td>\n                        </tr>\n                        <tr style='height:24.75pt'>\n                            <td width=1884 colspan=6 valign=top style='width:1413.2pt;border:solid windowtext 1.0pt;\n  border-top:none;padding:0in 5.4pt 0in 5.4pt;height:24.75pt'>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>&nbsp;</b></p>\n                            </td>\n                        </tr>\n                        <tr style='height:25.5pt'>\n                            <td width=1884 colspan=6 valign=top style='width:1413.2pt;border:solid windowtext 1.0pt;\n  border-top:none;padding:0in 5.4pt 0in 5.4pt;height:25.5pt'>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>&nbsp;</b></p>\n                            </td>\n                        </tr>\n                        <tr style='height:30.75pt'>\n                            <td width=1884 colspan=6 valign=top style='width:1413.2pt;border:solid windowtext 1.0pt;\n  border-top:none;padding:0in 5.4pt 0in 5.4pt;height:30.75pt'>\n                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:\n  normal'><b>&nbsp;</b></p>\n                            </td>\n                        </tr>\n                        <tr height=0>\n                            <td width=423 style='border:none'></td>\n                            <td width=204 style='border:none'></td>\n                            <td width=318 style='border:none'></td>\n                            <td width=309 style='border:none'></td>\n                            <td width=158 style='border:none'></td>\n                            <td width=472 style='border:none'></td>\n                        </tr>\n                    </table>\n\n                    <p class=MsoNormal><b>&nbsp;</b></p>\n                </div>\n            </div>\n            <button class=\"btn btn-outline-primary float-left\" shape=\"semi-round\" (click)=\"capturefullScreen()\">Print/Download</button>\n        </div>\n\n\n    </ng-template>\n    <!-- <ng-template #modalFooter>\n      \n    </ng-template> -->\n</modal>"

/***/ }),

/***/ "./src/app/pages/ui-features/amc/main-amc/maintance-report/maintance-report.component.scss":
/*!*************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/amc/main-amc/maintance-report/maintance-report.component.scss ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "button {\n  float: right; }\n\n@media (max-width: 600px) {\n  button {\n    float: none;\n    margin-top: 1px; } }\n\n.A4 {\n  background: white;\n  width: 21cm;\n  display: block;\n  margin: 0 auto;\n  padding: 10px 25px;\n  margin-bottom: 0.5cm;\n  -webkit-box-shadow: 0 0 0.5cm rgba(0, 0, 0, 0.5);\n          box-shadow: 0 0 0.5cm rgba(0, 0, 0, 0.5);\n  overflow-y: scroll;\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  font-size: 12pt; }\n\n@media (min-width: 576px) {\n  /deep/ modal.mode .modal .modal-dialog {\n    max-width: 1000px !important; }\n  /deep/ modal.mode .modal .modal-dialog .parag {\n    margin-bottom: 0px;\n    margin-top: -36px;\n    margin-left: 14%;\n    text-align: center; } }\n\nimg {\n  width: 30%;\n  margin-bottom: 10px; }\n\n@font-face {\n  font-family: Calibri;\n  panose-1: 2 15 5 2 2 2 4 3 2 4; }\n\n/* Style Definitions */\n\np.MsoNormal, li.MsoNormal, div.MsoNormal {\n  margin-top: 0in;\n  margin-right: 0in;\n  margin-bottom: 10.0pt;\n  margin-left: 0in;\n  line-height: 115%;\n  font-size: 11.0pt;\n  font-family: \"Calibri\",\"sans-serif\"; }\n\n.MsoChpDefault {\n  font-family: \"Calibri\",\"sans-serif\"; }\n\n.MsoPapDefault {\n  margin-bottom: 10.0pt;\n  line-height: 115%; }\n\n@page WordSection1 {\n  size: 8.5in 11.0in;\n  margin: 1.0in 1.0in 1.0in 1.0in; }\n\ndiv.WordSection1 {\n  page: WordSection1; }\n\nbutton {\n  float: right; }\n\n@media (max-width: 600px) {\n  button {\n    float: none;\n    margin-top: 1px; } }\n\nmodal.mode h4 {\n  margin-top: 25px; }\n\nmodal.mode .row {\n  margin-bottom: 20px; }\n\nmodal.mode .row .row {\n  margin-top: 10px;\n  margin-bottom: 0; }\n\nmodal.mode [class*=\"col-\"] {\n  padding-top: 15px;\n  padding-bottom: 15px; }\n\nmodal.mode.modes [class*=\"col-\"] {\n  padding-top: 15px;\n  padding-bottom: 15px;\n  background-color: #eee;\n  background-color: rgba(86, 61, 124, 0.15);\n  border: 1px solid #ddd;\n  border: 1px solid rgba(86, 61, 124, 0.2); }\n\nmodal.mode hr {\n  margin-top: 40px;\n  margin-bottom: 40px; }\n\nmodal.mode .dndPlaceholder {\n  background-color: #ddd;\n  display: block;\n  min-height: 42px;\n  padding-top: 15px;\n  padding-bottom: 15px;\n  border: 1px solid rgba(86, 61, 124, 0.2);\n  position: relative;\n  padding-right: 15px;\n  padding-left: 15px; }\n\n@media (min-width: 576px) {\n  /deep/ modal.models .modal .modal-dialog {\n    max-width: 1000px !important; } }\n\n@media (min-width: 576px) {\n  .modal-dialog {\n    max-width: 1000px !important; } }\n\n.nb-arrow-retweet, .nb-angle-double-right, .nb-angle-double-left {\n  font-size: 50px; }\n\n.containers {\n  margin-top: 120px; }\n\n.nb-edit {\n  font-size: 1.5rem;\n  cursor: pointer;\n  float: right; }\n\n.img-vert {\n  -webkit-transform: rotateX(180deg);\n          transform: rotateX(180deg); }\n\n/deep/ modal.mode hr {\n  margin-top: 0px !important;\n  margin-bottom: 0px !important; }\n\n/deep/signature-pad canvas {\n  border: 1px solid;\n  background: white; }\n\ntable tbody tr:nth-child(odd) {\n  background: none !important; }\n\ntable tbody tr:nth-child(even) {\n  background: none !important; }\n\n/deep/ modal.mode .modal-body {\n  overflow-y: scroll; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3ByYWthc2gvVmlwaW4vaXJvbmJpcmQvMjBGRUIyMDIwL2lyb25iaXJkLWNsaWVudHMvc3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9hbWMvbWFpbi1hbWMvbWFpbnRhbmNlLXJlcG9ydC9tYWludGFuY2UtcmVwb3J0LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0ksWUFBWSxFQUFBOztBQUVoQjtFQUNJO0lBQ0ksV0FBVztJQUNYLGVBQWUsRUFBQSxFQUVsQjs7QUFHTDtFQUNFLGlCQUFpQjtFQUNqQixXQUFXO0VBRVgsY0FBYztFQUNkLGNBQWM7RUFDZCxrQkFBa0I7RUFDbEIsb0JBQW9CO0VBQ3BCLGdEQUF3QztVQUF4Qyx3Q0FBd0M7RUFDeEMsa0JBQWtCO0VBQ2xCLDhCQUFzQjtVQUF0QixzQkFBc0I7RUFDdEIsZUFBZSxFQUFBOztBQUVqQjtFQUNJO0lBQ0ksNEJBQTRCLEVBQUE7RUFFaEM7SUFDSSxrQkFBa0I7SUFDbEIsaUJBQWlCO0lBQ2pCLGdCQUFnQjtJQUNoQixrQkFBa0IsRUFBQSxFQUNyQjs7QUFFTDtFQUNJLFVBQVU7RUFDVixtQkFBbUIsRUFBQTs7QUFJdkI7RUFDQyxvQkFBbUI7RUFDcEIsOEJBQTZCLEVBQUE7O0FBQzdCLHNCQUFBOztBQUNBO0VBQ0MsZUFBYztFQUNmLGlCQUFnQjtFQUNoQixxQkFBb0I7RUFDcEIsZ0JBQWU7RUFDZixpQkFBZ0I7RUFDaEIsaUJBQWdCO0VBQ2hCLG1DQUFrQyxFQUFBOztBQUNsQztFQUNDLG1DQUFrQyxFQUFBOztBQUNuQztFQUNDLHFCQUFvQjtFQUNyQixpQkFBZ0IsRUFBQTs7QUFDaEI7RUFDQyxrQkFBaUI7RUFDbEIsK0JBQThCLEVBQUE7O0FBQzlCO0VBQ0Msa0JBQWlCLEVBQUE7O0FBR2xCO0VBQ0ksWUFBWSxFQUFBOztBQUVoQjtFQUNJO0lBQ0ksV0FBVztJQUNYLGVBQWUsRUFBQSxFQUNsQjs7QUFHTDtFQUNJLGdCQUFnQixFQUFBOztBQUVsQjtFQUNFLG1CQUFtQixFQUFBOztBQUVyQjtFQUNFLGdCQUFnQjtFQUNoQixnQkFBZ0IsRUFBQTs7QUFFbEI7RUFDRSxpQkFBaUI7RUFDakIsb0JBQW9CLEVBQUE7O0FBTXRCO0VBQ0UsaUJBQWlCO0VBQ2pCLG9CQUFvQjtFQUNuQixzQkFBc0I7RUFDdEIseUNBQXFDO0VBQ3JDLHNCQUFzQjtFQUN0Qix3Q0FBb0MsRUFBQTs7QUFFdkM7RUFDRSxnQkFBZ0I7RUFDaEIsbUJBQW1CLEVBQUE7O0FBR3JCO0VBQ0Usc0JBQXNCO0VBQ3RCLGNBQWM7RUFDZCxnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLG9CQUFvQjtFQUNwQix3Q0FBb0M7RUFDcEMsa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixrQkFBa0IsRUFBQTs7QUFJcEI7RUFDSTtJQUNJLDRCQUE0QixFQUFBLEVBQy9COztBQUVMO0VBQ0Y7SUFDSSw0QkFBNEIsRUFBQSxFQUMvQjs7QUFJQTtFQUNHLGVBQWUsRUFBQTs7QUFFakI7RUFDRSxpQkFBaUIsRUFBQTs7QUFHbkI7RUFDRSxpQkFBaUI7RUFDakIsZUFBZTtFQUNmLFlBQVksRUFBQTs7QUFHZDtFQUNFLGtDQUEwQjtVQUExQiwwQkFBMEIsRUFBQTs7QUFHNUI7RUFDRSwwQkFBMEI7RUFDMUIsNkJBQTZCLEVBQUE7O0FBR2pDO0VBQ0ksaUJBQWlCO0VBQ2pCLGlCQUFpQixFQUFBOztBQUVwQjtFQUNDLDJCQUEyQixFQUFBOztBQUc3QjtFQUNFLDJCQUEyQixFQUFBOztBQUU3QjtFQUNFLGtCQUFrQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvdWktZmVhdHVyZXMvYW1jL21haW4tYW1jL21haW50YW5jZS1yZXBvcnQvbWFpbnRhbmNlLXJlcG9ydC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIlxuYnV0dG9ue1xuICAgIGZsb2F0OiByaWdodDtcbn1cbkBtZWRpYSAobWF4LXdpZHRoOjYwMHB4KXtcbiAgICBidXR0b257XG4gICAgICAgIGZsb2F0OiBub25lO1xuICAgICAgICBtYXJnaW4tdG9wOiAxcHg7XG4gICAgICAgIFxuICAgIH1cbn1cblxuLkE0IHtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIHdpZHRoOiAyMWNtO1xuICAvLyBoZWlnaHQ6IDMwLjdjbTtcbiAgZGlzcGxheTogYmxvY2s7XG4gIG1hcmdpbjogMCBhdXRvO1xuICBwYWRkaW5nOiAxMHB4IDI1cHg7XG4gIG1hcmdpbi1ib3R0b206IDAuNWNtO1xuICBib3gtc2hhZG93OiAwIDAgMC41Y20gcmdiYSgwLCAwLCAwLCAwLjUpO1xuICBvdmVyZmxvdy15OiBzY3JvbGw7XG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gIGZvbnQtc2l6ZTogMTJwdDtcbn1cbkBtZWRpYShtaW4td2lkdGg6IDU3NnB4KXtcbiAgICAvZGVlcC8gbW9kYWwubW9kZSAubW9kYWwgLm1vZGFsLWRpYWxvZ3tcbiAgICAgICAgbWF4LXdpZHRoOiAxMDAwcHggIWltcG9ydGFudDtcbiAgICB9XG4gICAgL2RlZXAvIG1vZGFsLm1vZGUgLm1vZGFsIC5tb2RhbC1kaWFsb2cgLnBhcmFne1xuICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gICAgICAgIG1hcmdpbi10b3A6IC0zNnB4O1xuICAgICAgICBtYXJnaW4tbGVmdDogMTQlO1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgfVxufVxuaW1ne1xuICAgIHdpZHRoOiAzMCU7XG4gICAgbWFyZ2luLWJvdHRvbTogMTBweDtcbn1cblxuXG5AZm9udC1mYWNlXG57Zm9udC1mYW1pbHk6Q2FsaWJyaTtcbnBhbm9zZS0xOjIgMTUgNSAyIDIgMiA0IDMgMiA0O31cbi8qIFN0eWxlIERlZmluaXRpb25zICovXG5wLk1zb05vcm1hbCwgbGkuTXNvTm9ybWFsLCBkaXYuTXNvTm9ybWFsXG57bWFyZ2luLXRvcDowaW47XG5tYXJnaW4tcmlnaHQ6MGluO1xubWFyZ2luLWJvdHRvbToxMC4wcHQ7XG5tYXJnaW4tbGVmdDowaW47XG5saW5lLWhlaWdodDoxMTUlO1xuZm9udC1zaXplOjExLjBwdDtcbmZvbnQtZmFtaWx5OlwiQ2FsaWJyaVwiLFwic2Fucy1zZXJpZlwiO31cbi5Nc29DaHBEZWZhdWx0XG57Zm9udC1mYW1pbHk6XCJDYWxpYnJpXCIsXCJzYW5zLXNlcmlmXCI7fVxuLk1zb1BhcERlZmF1bHRcbnttYXJnaW4tYm90dG9tOjEwLjBwdDtcbmxpbmUtaGVpZ2h0OjExNSU7fVxuQHBhZ2UgV29yZFNlY3Rpb24xXG57c2l6ZTo4LjVpbiAxMS4waW47XG5tYXJnaW46MS4waW4gMS4waW4gMS4waW4gMS4waW47fVxuZGl2LldvcmRTZWN0aW9uMVxue3BhZ2U6V29yZFNlY3Rpb24xO31cblxuXG5idXR0b257XG4gICAgZmxvYXQ6IHJpZ2h0O1xufVxuQG1lZGlhIChtYXgtd2lkdGg6NjAwcHgpe1xuICAgIGJ1dHRvbntcbiAgICAgICAgZmxvYXQ6IG5vbmU7XG4gICAgICAgIG1hcmdpbi10b3A6IDFweDtcbiAgICB9XG59XG5cbm1vZGFsLm1vZGUgaDQge1xuICAgIG1hcmdpbi10b3A6IDI1cHg7XG4gIH1cbiAgbW9kYWwubW9kZSAucm93IHtcbiAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xuICB9XG4gIG1vZGFsLm1vZGUgLnJvdyAucm93IHtcbiAgICBtYXJnaW4tdG9wOiAxMHB4O1xuICAgIG1hcmdpbi1ib3R0b206IDA7XG4gIH1cbiAgbW9kYWwubW9kZSBbY2xhc3MqPVwiY29sLVwiXSB7XG4gICAgcGFkZGluZy10b3A6IDE1cHg7XG4gICAgcGFkZGluZy1ib3R0b206IDE1cHg7XG4gICAgLy8gYmFja2dyb3VuZC1jb2xvcjogI2VlZTtcbiAgICAvLyBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDg2LDYxLDEyNCwuMTUpO1xuICAgIC8vIGJvcmRlcjogMXB4IHNvbGlkICNkZGQ7XG4gICAgLy8gYm9yZGVyOiAxcHggc29saWQgcmdiYSg4Niw2MSwxMjQsLjIpO1xuICB9XG4gIG1vZGFsLm1vZGUubW9kZXMgW2NsYXNzKj1cImNvbC1cIl0ge1xuICAgIHBhZGRpbmctdG9wOiAxNXB4O1xuICAgIHBhZGRpbmctYm90dG9tOiAxNXB4O1xuICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWVlO1xuICAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDg2LDYxLDEyNCwuMTUpO1xuICAgICBib3JkZXI6IDFweCBzb2xpZCAjZGRkO1xuICAgICBib3JkZXI6IDFweCBzb2xpZCByZ2JhKDg2LDYxLDEyNCwuMik7XG4gIH1cbiAgbW9kYWwubW9kZSBociB7XG4gICAgbWFyZ2luLXRvcDogNDBweDtcbiAgICBtYXJnaW4tYm90dG9tOiA0MHB4O1xuICB9XG5cbiAgbW9kYWwubW9kZSAuZG5kUGxhY2Vob2xkZXJ7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2RkZDtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBtaW4taGVpZ2h0OiA0MnB4O1xuICAgIHBhZGRpbmctdG9wOiAxNXB4O1xuICAgIHBhZGRpbmctYm90dG9tOiAxNXB4O1xuICAgIGJvcmRlcjogMXB4IHNvbGlkIHJnYmEoODYsNjEsMTI0LC4yKTtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgcGFkZGluZy1yaWdodDogMTVweDtcbiAgICBwYWRkaW5nLWxlZnQ6IDE1cHg7XG4gICAgXG4gIH1cblxuICBAbWVkaWEobWluLXdpZHRoOiA1NzZweCl7XG4gICAgICAvZGVlcC8gbW9kYWwubW9kZWxzIC5tb2RhbCAubW9kYWwtZGlhbG9ne1xuICAgICAgICAgIG1heC13aWR0aDogMTAwMHB4ICFpbXBvcnRhbnQ7XG4gICAgICB9XG4gIH1cbiAgQG1lZGlhIChtaW4td2lkdGg6IDU3NnB4KXtcbi5tb2RhbC1kaWFsb2cge1xuICAgIG1heC13aWR0aDogMTAwMHB4ICFpbXBvcnRhbnQ7XG59XG59XG5cblxuIC5uYi1hcnJvdy1yZXR3ZWV0LCAubmItYW5nbGUtZG91YmxlLXJpZ2h0LCAubmItYW5nbGUtZG91YmxlLWxlZnR7XG4gICAgZm9udC1zaXplOiA1MHB4O1xuICB9XG4gIC5jb250YWluZXJze1xuICAgIG1hcmdpbi10b3A6IDEyMHB4O1xuICB9XG5cbiAgLm5iLWVkaXR7XG4gICAgZm9udC1zaXplOiAxLjVyZW07XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIGZsb2F0OiByaWdodDtcbiAgfVxuXG4gIC5pbWctdmVydCB7XG4gICAgdHJhbnNmb3JtOiByb3RhdGVYKDE4MGRlZyk7XG4gIH1cblxuICAvZGVlcC8gbW9kYWwubW9kZSBociB7XG4gICAgbWFyZ2luLXRvcDogMHB4ICFpbXBvcnRhbnQ7XG4gICAgbWFyZ2luLWJvdHRvbTogMHB4ICFpbXBvcnRhbnQ7XG59XG5cbi9kZWVwL3NpZ25hdHVyZS1wYWQgY2FudmFzIHtcbiAgICBib3JkZXI6IDFweCBzb2xpZDtcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbn1cbiB0YWJsZSB0Ym9keSB0cjpudGgtY2hpbGQob2RkKSB7XG4gIGJhY2tncm91bmQ6IG5vbmUgIWltcG9ydGFudDsgXG59XG5cbnRhYmxlIHRib2R5IHRyOm50aC1jaGlsZChldmVuKSB7XG4gIGJhY2tncm91bmQ6IG5vbmUgIWltcG9ydGFudDsgXG59XG4vZGVlcC8gbW9kYWwubW9kZSAubW9kYWwtYm9keSB7XG4gIG92ZXJmbG93LXk6IHNjcm9sbDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/ui-features/amc/main-amc/maintance-report/maintance-report.component.ts":
/*!***********************************************************************************************!*\
  !*** ./src/app/pages/ui-features/amc/main-amc/maintance-report/maintance-report.component.ts ***!
  \***********************************************************************************************/
/*! exports provided: MaintanceReportComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MaintanceReportComponent", function() { return MaintanceReportComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var jspdf__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! jspdf */ "./node_modules/jspdf/dist/jspdf.min.js");
/* harmony import */ var jspdf__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(jspdf__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var html2canvas__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! html2canvas */ "./node_modules/html2canvas/dist/npm/index.js");
/* harmony import */ var html2canvas__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(html2canvas__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var angular_custom_modal__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! angular-custom-modal */ "./node_modules/angular-custom-modal/angular-custom-modal.es5.js");
/* harmony import */ var angular2_signaturepad_signature_pad__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! angular2-signaturepad/signature-pad */ "./node_modules/angular2-signaturepad/signature-pad.js");
/* harmony import */ var angular2_signaturepad_signature_pad__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(angular2_signaturepad_signature_pad__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _confirmation_dialog_confirmation_dialog_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../../confirmation-dialog/confirmation-dialog.service */ "./src/app/confirmation-dialog/confirmation-dialog.service.ts");
/* harmony import */ var _services_AMC_amc_maintanance_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../../services/AMC/amc-maintanance.service */ "./src/app/services/AMC/amc-maintanance.service.ts");
/* harmony import */ var _services_AMC_amc_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../../services/AMC/amc.service */ "./src/app/services/AMC/amc.service.ts");
/* harmony import */ var _services_token_token_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../../../services/token/token.service */ "./src/app/services/token/token.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};










var MaintanceReportComponent = /** @class */ (function () {
    function MaintanceReportComponent(AmcServ, userServ, AmcMaintananceServ, confirmationDialogService) {
        this.AmcServ = AmcServ;
        this.userServ = userServ;
        this.AmcMaintananceServ = AmcMaintananceServ;
        this.confirmationDialogService = confirmationDialogService;
        this.AmcChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.data1 = {};
        this.editDetails = false;
        this.listOfAmc = [];
        this.listOfAmcDisplay = [];
        this.elevator = [];
        this.source = new ng2_smart_table__WEBPACK_IMPORTED_MODULE_3__["LocalDataSource"]();
        /**
         * Table code started here for maintance report
         */
        this.settings = {
            actions: {
                edit: false,
                add: false,
                delete: false,
            },
            columns: {
                // employee: {
                //   title: 'Employee Name',
                //   type: 'string',
                // },
                WorkDate: {
                    title: 'Date',
                    type: 'string',
                    valuePrepareFunction: function (value) {
                        return value.split('-').reverse().join('/');
                    },
                },
                remarks_Feedback: {
                    title: 'Feedback',
                    type: 'string'
                },
            },
        };
        this.data2 = {};
        /**
         * signature part will be started from here
         */
        this.signaturePadOptions = {
            'minWidth': 1,
            'canvasWidth': 290,
            'canvasHeight': 150
        };
        this.moduleCreate = true;
        this.moduleEdit = true;
        this.moduleView = true;
        this.range = [];
        this.getAmcDetail();
    }
    /**
         Get  AMC Details
       **/
    MaintanceReportComponent.prototype.getAmcDetail = function () {
        var _this = this;
        this.AmcServ.AmcDone
            .subscribe(function (data) {
            _this.data1 = data;
            _this.elevator = _this.data1.invoiceItems[0];
            _this.id = _this.data1.id;
            _this.siteId = _this.data1.siteId;
            // console.log(this.siteId);
            // this.getAMCDetails();
        }, function (error) {
            console.log(error);
        });
    };
    /**
     * cancel button
     */
    MaintanceReportComponent.prototype.close = function () {
        this.componentInsideModals.close();
    };
    MaintanceReportComponent.prototype.onUserRowSelect = function (event) {
        var data = event.data;
        this.data2 = data;
        // console.log( this.data2)
        this.componentInsidePrint.open();
    };
    MaintanceReportComponent.prototype.view = function (data) {
        var _this = this;
        this.listOfAmc.forEach(function (ele) {
            if (ele.id === data) {
                _this.data2 = ele;
            }
        });
        this.componentInsidePrint.open();
    };
    MaintanceReportComponent.prototype.edit = function (data) {
        var _this = this;
        this.editId = data;
        this.editDetails = true;
        this.listOfAmc.forEach(function (ele) {
            if (ele.id === data) {
                _this.data1 = ele;
            }
        });
        this.componentInsideModals.open();
    };
    MaintanceReportComponent.prototype.getAMCDetails = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, error_1;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        this.listOfAmcDisplay = [];
                        _a = this;
                        return [4 /*yield*/, this.AmcMaintananceServ.getAllAmcId(this.id)];
                    case 1:
                        _a.listOfAmc = _b.sent();
                        this.listOfAmc.reverse();
                        console.log(this.listOfAmc);
                        // sort by name
                        this.listOfAmc.forEach(function (ele) {
                            if (ele.compaintDate.split('-')[0] === _this.yearCurrent.toString()) {
                                _this.listOfAmcDisplay.push(ele);
                            }
                        });
                        if (this.listOfAmcDisplay.length > 1) {
                            this.sortByMonth(this.listOfAmcDisplay);
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _b.sent();
                        console.error(error_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    // public set: string;
    //  ngAfterViewInit() {
    // //   // this.signaturePad is now available
    //     // this.signaturePad.set('minWidth', 1); 
    //    this.signaturePad.clear(); 
    //   //  this.signaturePads.set('minWidth', 1); 
    //    this.signaturePads.clear();
    //  }
    MaintanceReportComponent.prototype.drawComplete = function () {
        // will be notified of signature_pad's onEnd event
        // console.log( this.signaturePad.toDataURL() ,"Employee");
        // console.log(this.signaturePads.toDataURL(),"Customer");
    };
    MaintanceReportComponent.prototype.drawCompleteForCustomer = function () {
        //  will be notified of signature_pad's onEnd event
        //  console.log(this.signaturePad2.toDataURL(),"Customer");
    };
    MaintanceReportComponent.prototype.drawStart = function () {
        // will be notified of szimek/signature_pad's onBegin event
        // console.log('begin drawing');
    };
    MaintanceReportComponent.prototype.drawStarts = function () {
        // will be notified of szimek/signature_pad's onBegin event
        // console.log('begin drawing');
    };
    MaintanceReportComponent.prototype.ngOnInit = function () {
        var _this = this;
        var year = new Date().getFullYear();
        this.yearCurrent = year;
        this.range.push(year);
        for (var i = 1; i < 16; i++) {
            this.range.push(year - i);
        }
        this.range.reverse();
        if (this.userServ.userAccess !== undefined) {
            this.userServ.userAccess
                .subscribe(function (data) {
                data.forEach(function (ele) {
                    if (ele.name === 'AMC Details') {
                        _this.moduleCreate = ele.create;
                        _this.moduleEdit = ele.edit;
                        _this.moduleView = ele.view;
                    }
                });
            }, function (error) {
                console.log(error);
            });
        }
        this.getAMCDetails();
    };
    MaintanceReportComponent.prototype.create = function () {
        this.editDetails = false;
        this.editId = '';
        this.componentInsideModals.open();
    };
    MaintanceReportComponent.prototype.createAMCReport = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            var resultAleart, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.confirmationDialogService.confirm('Alert Message', 'Are you sure you want to Save?')];
                    case 1:
                        resultAleart = _a.sent();
                        if (!resultAleart) return [3 /*break*/, 6];
                        result = void 0;
                        Object.assign(data.value, { amcId: this.id, month: this.selectedMonth, employeeSignature: this.signaturePad.toDataURL(), customerSignature: this.signaturePad2.toDataURL(), siteId: this.siteId });
                        if (!(this.editId !== '')) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.AmcMaintananceServ.UpdateAmcDetails(data.value, this.editId)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 5];
                    case 3: return [4 /*yield*/, this.AmcMaintananceServ.createAmc(data.value)];
                    case 4:
                        result = _a.sent();
                        _a.label = 5;
                    case 5:
                        this.AmcChange.emit(result);
                        // this.listOfAmc.push(result);
                        // this.source.load(this.listOfAmc);
                        this.getAMCDetails();
                        this.componentInsideModals.close();
                        _a.label = 6;
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    MaintanceReportComponent.prototype.date = function (e) {
        var today = new Date(e);
        this.selectedMonth = today.toLocaleString('default', { month: 'short' });
    };
    // ******* For print the screen Start here **********//
    MaintanceReportComponent.prototype.capturefullScreen = function (event) {
        return __awaiter(this, void 0, void 0, function () {
            var resultAleart;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.confirmationDialogService.confirm('Alert Message', 'Are You Sure You Want To Download ?')];
                    case 1:
                        resultAleart = _a.sent();
                        if (resultAleart) {
                            this.maintancesTechnicals(event);
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    MaintanceReportComponent.prototype.maintancesTechnicals = function (event) {
        var _this = this;
        var data = document.getElementById('maintancesPage');
        html2canvas__WEBPACK_IMPORTED_MODULE_2___default()(data).then(function (canvas) {
            // Few necessary setting options  
            var imgWidth = 208;
            var pageHeight = 295;
            var imgHeight = 1000 * imgWidth / canvas.width;
            var heightLeft = imgHeight;
            var contentDataURL = canvas.toDataURL('image/png');
            var pdf = new jspdf__WEBPACK_IMPORTED_MODULE_1__(); // A4 size page of PDF  
            var position = 0;
            pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight, heightLeft);
            pdf.save('IRB/BLR/' + _this.data2.jobid + 'Agreement-Maintances'); // Generated PDF   
        });
    };
    // ******* For print the screen End here **********//
    MaintanceReportComponent.prototype.sortByMonth = function (arr) {
        var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        arr.sort(function (a, b) {
            return months.indexOf(a.month)
                - months.indexOf(b.month);
        });
    };
    MaintanceReportComponent.prototype.changeYear = function (e) {
        var _this = this;
        this.listOfAmcDisplay = [];
        var selectedYear = e.split(': ')[1];
        this.listOfAmc.forEach(function (ele) {
            if (ele.compaintDate.split('-')[0] === selectedYear.toString()) {
                _this.listOfAmcDisplay.push(ele);
            }
        });
        if (this.listOfAmcDisplay.length > 1) {
            this.sortByMonth(this.listOfAmcDisplay);
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('componentInsideModals'),
        __metadata("design:type", angular_custom_modal__WEBPACK_IMPORTED_MODULE_4__["ModalComponent"])
    ], MaintanceReportComponent.prototype, "componentInsideModals", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('componentInsidePrint'),
        __metadata("design:type", angular_custom_modal__WEBPACK_IMPORTED_MODULE_4__["ModalComponent"])
    ], MaintanceReportComponent.prototype, "componentInsidePrint", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('sigpad1'),
        __metadata("design:type", angular2_signaturepad_signature_pad__WEBPACK_IMPORTED_MODULE_5__["SignaturePad"])
    ], MaintanceReportComponent.prototype, "signaturePad", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('sigpad2'),
        __metadata("design:type", angular2_signaturepad_signature_pad__WEBPACK_IMPORTED_MODULE_5__["SignaturePad"])
    ], MaintanceReportComponent.prototype, "signaturePad2", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], MaintanceReportComponent.prototype, "AmcChange", void 0);
    MaintanceReportComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-maintance-report',
            template: __webpack_require__(/*! ./maintance-report.component.html */ "./src/app/pages/ui-features/amc/main-amc/maintance-report/maintance-report.component.html"),
            styles: [__webpack_require__(/*! ./maintance-report.component.scss */ "./src/app/pages/ui-features/amc/main-amc/maintance-report/maintance-report.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_AMC_amc_service__WEBPACK_IMPORTED_MODULE_8__["AmcService"],
            _services_token_token_service__WEBPACK_IMPORTED_MODULE_9__["TokenService"],
            _services_AMC_amc_maintanance_service__WEBPACK_IMPORTED_MODULE_7__["AmcMaintananceService"],
            _confirmation_dialog_confirmation_dialog_service__WEBPACK_IMPORTED_MODULE_6__["ConfirmationDialogService"]])
    ], MaintanceReportComponent);
    return MaintanceReportComponent;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/amc/main-amc/print-amc/amc-without-logo/amc-without-logo.component.html":
/*!***********************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/amc/main-amc/print-amc/amc-without-logo/amc-without-logo.component.html ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"A4\" id=\"contentToConvert\">\n    <br><br><br><br><br><br><br>\n    <h3> AMC</h3>\n    <div style=\"text-align: right\">\n\n    </div>\n    <br>\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-6\">\n                <b>Name & Address of the Client:{{data1.customerName}} <br> {{data1.clientAddress}}</b>\n                <br><br>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-12\">\n                <b>Installation Address: <div class=\"row\">\n            <div class=\"col-5\">{{data1.siteAddress}}</div>\n          </div></b>\n                <br><br>\n            </div>\n            <div class=\"col-12\">\n                <b>AMC Amount: {{amcAmt}}/-  ({{amcAmtWords}})</b><br>\n                <!-- VIPIN CODE FOR CURRENCY -->\n                (Above price is exclusive of GST 18%)\n                <br><br>\n            </div>\n            <div class=\"col-4\">\n                <b>AMC JOB No</b>\n            </div>\n            <div class=\"col-8\">\n                <p>:&nbsp;&nbsp; {{data1.amcid}} </p>\n            </div>\n            <div class=\"col-4\">\n                <b>AMC Period</b>\n                <br><br>\n            </div>\n            <div class=\"col-8\">\n                <p>:&nbsp;&nbsp; from <b>{{ data1.fromData | date}}</b> to <b>{{data1.toData | date}}</b></p>\n                <br><br>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-6\">\n                <b>Contact Person: <p></p></b>\n                <br><br>\n            </div>\n            <div class=\"col-6\">\n                <b>Phone Number: <p></p></b>\n                <br><br>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-6\">\n                <b>E Mailid:Signature:</b>\n                <br><br>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-6\">\n                <b>SIGNED ON BEHALF OF IRONBIRD ELEVATORS</b>\n                <p>Signature (authorized signatory):</p>\n                <br><br>\n                <br>\n                <p>Name & Designation:</p>\n                <br><br>\n            </div>\n        </div>\n        <br> <br><br>\n        <div class=\"row\">\n            <div class=\"col-12\">\n                <b>In case of breakdown,our contact number:8849586611,8494811110,080-23511445</b><br>\n            </div>\n        </div>\n    </div>\n    <br><br><br><br><br><br><br><br><br>\n    <br><br><br><br><br>\n    <div *ngIf='servies !== \"\" && elements.length !== 0'>\n        <div>\n            <h3 style=\"text-align:center\"> Type of Service {{data1.servies}}</h3>\n        </div>\n        <br>\n        <div *ngIf='servicesPonitsList.length !== 0'>\n            <label><strong>IRONBIRD ELEVATORS PVT LTD.</strong></label>\n            <ol style='padding-left: 4%'>\n                <li *ngFor=\"let listPoints of servicesPonitsList;let i = index\">\n                    <p style=\"margin-bottom: 0;\">{{ listPoints.mainPonit }} </p>\n                    <ol type=\"a\" [hidden]=\"listPoints.subPonit.length === 0\">\n                        <li *ngFor=\"let data of listPoints.subPonit;let i = index\">\n                            <p style=\"margin-bottom: 0;\">{{data.subPoints}} </p>\n                        </li>\n                    </ol>\n                </li>\n            </ol>\n        </div>\n        <div *ngIf='servicesPonitsList0.length !== 0'>\n            <label><strong>The Customer Shall</strong></label>\n            <ol style='padding-left: 4%'>\n                <li *ngFor=\"let listPoints of servicesPonitsList0;let i = index\">\n                    <p style=\"margin-bottom: 0;\">{{ listPoints.mainPonit }} </p>\n                    <ol type=\"a\" [hidden]=\"listPoints.subPonit.length === 0\">\n                        <li *ngFor=\"let data of listPoints.subPonit;let i = index\">\n                            <p style=\"margin-bottom: 0;\">{{data.subPoints}} </p>\n                        </li>\n                    </ol>\n                </li>\n            </ol>\n        </div>\n        <div *ngIf='servicesPonitsList1.length !== 0'>\n            <label><strong>General terms</strong></label>\n            <ol style='padding-left: 4%'>\n                <li *ngFor=\"let listPoints of servicesPonitsList1;let i = index\">\n                    <p style=\"margin-bottom: 0;\">{{ listPoints.mainPonit }} </p>\n                    <ol type=\"a\" [hidden]=\"listPoints.subPonit.length === 0\">\n                        <li *ngFor=\"let data of listPoints.subPonit;let i = index\">\n                            <p style=\"margin-bottom: 0;\">{{data.subPoints}} </p>\n                        </li>\n                    </ol>\n                </li>\n            </ol>\n        </div>\n    </div>\n</div>\n<!-- <div class=\"A4\" id=\"contentToConvert\">\n    <h3> AMC</h3>\n    <div>\n        <h3 style=\"text-align:center\"> Type of Service {{data1.servies}}</h3>\n    \n    </div>\n    <br>\n<ol>\n    <li *ngFor=\"let listPoints of elements;let i = index\">\n      <p>{{ listPoints.mainPonit }} </p>\n      <ol type=\"a\" [hidden]=\"listPoints.subPonit.length === 0\">\n        <li *ngFor=\"let data of listPoints.subPonit;let i = index\">\n          <p>{{data.subPoints}} </p>\n        </li>\n      </ol>\n    </li>\n  </ol>\n</div> -->\n<div class=\"row\">\n    <div class=\"col-12\">\n        <br><br>\n        <button class=\"btn btn-outline-primary float-left\" shape=\"semi-round\" (click)=\"capturefullScreen()\">Print/Download</button>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/pages/ui-features/amc/main-amc/print-amc/amc-without-logo/amc-without-logo.component.scss":
/*!***********************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/amc/main-amc/print-amc/amc-without-logo/amc-without-logo.component.scss ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "h3 {\n  text-align: center; }\n\n.borders {\n  border: 1px solid black; }\n\n.A4 {\n  background: white;\n  width: 21cm;\n  display: block;\n  margin: 0 auto;\n  padding: 10px 25px;\n  margin-bottom: 0.5cm;\n  -webkit-box-shadow: 0 0 0.5cm rgba(0, 0, 0, 0.5);\n          box-shadow: 0 0 0.5cm rgba(0, 0, 0, 0.5);\n  overflow-y: scroll;\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  font-size: 12pt; }\n\n@media print {\n  .page-break {\n    display: block;\n    page-break-before: always; } }\n\n@media print {\n  body {\n    margin: 0;\n    padding: 0; }\n  .A4 {\n    -webkit-box-shadow: none;\n            box-shadow: none;\n    margin: 0;\n    width: auto;\n    height: auto; }\n  .enable-print {\n    display: block; } }\n\nimg {\n  max-width: 97%; }\n\ntd {\n  width: 50%;\n  font-size: 12px;\n  line-height: 2.5; }\n\nul {\n  list-style-type: none; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3ByYWthc2gvVmlwaW4vaXJvbmJpcmQvMjBGRUIyMDIwL2lyb25iaXJkLWNsaWVudHMvc3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9hbWMvbWFpbi1hbWMvcHJpbnQtYW1jL2FtYy13aXRob3V0LWxvZ28vYW1jLXdpdGhvdXQtbG9nby5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGtCQUFrQixFQUFBOztBQUV0QjtFQUNJLHVCQUNKLEVBQUE7O0FBQ0E7RUFDSSxpQkFBaUI7RUFDakIsV0FBVztFQUVYLGNBQWM7RUFDZCxjQUFjO0VBQ2Qsa0JBQWtCO0VBQ2xCLG9CQUFvQjtFQUNwQixnREFBd0M7VUFBeEMsd0NBQXdDO0VBQ3hDLGtCQUFrQjtFQUNsQiw4QkFBc0I7VUFBdEIsc0JBQXNCO0VBQ3RCLGVBQWUsRUFBQTs7QUFHakI7RUFDRTtJQUNFLGNBQWM7SUFDZCx5QkFBeUIsRUFBQSxFQUMxQjs7QUFJSDtFQUNFO0lBQ0UsU0FBUztJQUNULFVBQVUsRUFBQTtFQUVaO0lBQ0Usd0JBQWdCO1lBQWhCLGdCQUFnQjtJQUNoQixTQUFTO0lBQ1QsV0FBVztJQUNYLFlBQVksRUFBQTtFQUdkO0lBQ0UsY0FBYyxFQUFBLEVBQ2Y7O0FBR0g7RUFDRSxjQUFjLEVBQUE7O0FBR2hCO0VBQ0UsVUFBVTtFQUNWLGVBQWU7RUFDZixnQkFBZ0IsRUFBQTs7QUFFbEI7RUFDRSxxQkFBb0IsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VpLWZlYXR1cmVzL2FtYy9tYWluLWFtYy9wcmludC1hbWMvYW1jLXdpdGhvdXQtbG9nby9hbWMtd2l0aG91dC1sb2dvLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaDN7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLmJvcmRlcnN7XG4gICAgYm9yZGVyOiAxcHggc29saWQgYmxhY2tcbn1cbi5BNCB7XG4gICAgYmFja2dyb3VuZDogd2hpdGU7XG4gICAgd2lkdGg6IDIxY207XG4gICAgLy8gaGVpZ2h0OiAzMC43Y207XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgbWFyZ2luOiAwIGF1dG87XG4gICAgcGFkZGluZzogMTBweCAyNXB4O1xuICAgIG1hcmdpbi1ib3R0b206IDAuNWNtO1xuICAgIGJveC1zaGFkb3c6IDAgMCAwLjVjbSByZ2JhKDAsIDAsIDAsIDAuNSk7XG4gICAgb3ZlcmZsb3cteTogc2Nyb2xsO1xuICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gICAgZm9udC1zaXplOiAxMnB0O1xuICB9XG4gIFxuICBAbWVkaWEgcHJpbnQge1xuICAgIC5wYWdlLWJyZWFrIHtcbiAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgcGFnZS1icmVhay1iZWZvcmU6IGFsd2F5cztcbiAgICB9XG4gICAgLy8gc2l6ZTogQTQgcG9ydHJhaXQ7XG4gIH1cbiAgXG4gIEBtZWRpYSBwcmludCB7XG4gICAgYm9keSB7XG4gICAgICBtYXJnaW46IDA7XG4gICAgICBwYWRkaW5nOiAwO1xuICAgIH1cbiAgICAuQTQge1xuICAgICAgYm94LXNoYWRvdzogbm9uZTtcbiAgICAgIG1hcmdpbjogMDtcbiAgICAgIHdpZHRoOiBhdXRvO1xuICAgICAgaGVpZ2h0OiBhdXRvO1xuICAgIH1cbiAgICBcbiAgICAuZW5hYmxlLXByaW50IHtcbiAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIH1cbiAgfVxuXG4gIGltZ3tcbiAgICBtYXgtd2lkdGg6IDk3JTtcbiBcbiAgfVxuICB0ZHtcbiAgICB3aWR0aDogNTAlO1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICBsaW5lLWhlaWdodDogMi41O1xuICB9XG4gIHVse1xuICAgIGxpc3Qtc3R5bGUtdHlwZTpub25lO1xuICB9Il19 */"

/***/ }),

/***/ "./src/app/pages/ui-features/amc/main-amc/print-amc/amc-without-logo/amc-without-logo.component.ts":
/*!*********************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/amc/main-amc/print-amc/amc-without-logo/amc-without-logo.component.ts ***!
  \*********************************************************************************************************/
/*! exports provided: AmcWithoutLogoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AmcWithoutLogoComponent", function() { return AmcWithoutLogoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! pdfmake/build/pdfmake */ "./node_modules/pdfmake/build/pdfmake.js");
/* harmony import */ var pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! pdfmake/build/vfs_fonts */ "./node_modules/pdfmake/build/vfs_fonts.js");
/* harmony import */ var pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _confirmation_dialog_confirmation_dialog_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../confirmation-dialog/confirmation-dialog.service */ "./src/app/confirmation-dialog/confirmation-dialog.service.ts");
/* harmony import */ var _services_currency_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../../services/currency.service */ "./src/app/services/currency.service.ts");
/* harmony import */ var _services_AMC_amc_service_points_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../../../services/AMC/amc-service-points.service */ "./src/app/services/AMC/amc-service-points.service.ts");
/* harmony import */ var _services_AMC_amc_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../../../services/AMC/amc.service */ "./src/app/services/AMC/amc.service.ts");
/* harmony import */ var _services_image_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../../../services/image.service */ "./src/app/services/image.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






 // VIPIN CODE FOR CURRENCY

pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_1___default.a.vfs = pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_2___default.a.pdfMake.vfs;
var AmcWithoutLogoComponent = /** @class */ (function () {
    // @Output() AmcChange = new EventEmitter()
    function AmcWithoutLogoComponent(AmcServ, confirmationDialogService, AmcServicePointsSer, img, currency) {
        this.AmcServ = AmcServ;
        this.confirmationDialogService = confirmationDialogService;
        this.AmcServicePointsSer = AmcServicePointsSer;
        this.img = img;
        this.currency = currency;
        // @ViewChild('componentInsideModal') componentInsideModal: ModalComponent;
        this.data1 = { invoiceItems: [] };
        this.show = true; // data2: object = {};
        this.editShow = false;
        this.listOfAmc = [];
        this.elements = [];
        this.type = {};
        this.service = false;
        this.exs = [];
        this.exs0 = [];
        this.exs1 = [];
        this.arr = [];
        // ******* For print the screen started here **********//
        this.options = false;
        this.getAmcDetail();
        // this.getAmcDetailPoints();
    }
    AmcWithoutLogoComponent.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _b;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.img.image];
                    case 1:
                        _a.image = _c.sent();
                        _b = this;
                        return [4 /*yield*/, this.img.image1];
                    case 2:
                        _b.image1 = _c.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AmcWithoutLogoComponent.prototype.getAmcDetail = function () {
        var _this = this;
        this.AmcServ.AmcDone
            .subscribe(function (data) {
            _this.data1 = data;
            // NEW CODE STARTS           // VIPIN CODE FOR CURRENCY
            _this.amcAmt = _this.currency.convertToInrFormat(_this.data1.totalPrice); // VIPIN CODE FOR CURRENCY
            _this.amcAmtWords = _this.currency.convertNumToWords(_this.data1.unitprice.split(".")[0]); // VIPIN CODE FOR CURRENCY
            // NEW CODE ENDS           // VIPIN CODE FOR CURRENCY
            _this.servies = data.servies;
            // console.log(this.data1)
            if (!_this.data1.id) {
                return _this.show = false;
            }
            _this.id = _this.data1.id;
            // to check if services added
            _this.servies = data.servies;
            if (_this.servies !== '') {
                _this.service = true;
                _this.servicesPonitsList = _this.data1.serviesPoint;
                _this.servicesPonitsList0 = _this.data1.serviesPoint0;
                _this.servicesPonitsList1 = _this.data1.serviesPoint1;
                _this.servicesPonitsList.forEach(function (element) {
                    _this.elements.push(element);
                    return;
                });
            }
            else {
                _this.service = false;
            }
            if (_this.service && _this.data1.serviesPoint.length !== 0) {
                _this.exs.push({
                    text: "(" + _this.servies.toUpperCase() + ")",
                    fontSize: 15,
                    bold: true,
                    alignment: 'center',
                    width: '*'
                });
                _this.exs0.push({
                    text: "" + _this.servies.toUpperCase(),
                    fontSize: 15,
                    bold: true,
                    alignment: 'center',
                    width: '*',
                    italics: true,
                    pageBreak: 'before',
                    margin: [0, 35, 0, 0]
                });
                _this.exs;
                _this.exs0;
                if (_this.data1.serviesPoint.length > 0) {
                    _this.arr = [];
                    _this.exs1.push({
                        text: 'Section1.',
                        fontSize: 12,
                    }, {
                        text: 'IRONBIRD ELEVATORS PVT LTD.',
                        fontSize: 12,
                        bold: true
                    }, {
                        fontSize: 11,
                        margin: [10, 5, 0, 15],
                        ol: _this.data1.serviesPoint.map(function (ele) {
                            return [
                                ele.mainPonit,
                                {
                                    type: 'lower-alpha',
                                    ol: ele.subPonit.map(function (s) { return s.subPoints; })
                                }
                            ];
                        }).slice()
                    });
                }
                if (_this.data1.serviesPoint0.length > 0) {
                    _this.exs1.push({
                        text: 'Section2.',
                        fontSize: 12,
                    }, {
                        text: 'The Customer Shall',
                        fontSize: 12,
                        bold: true
                    }, {
                        fontSize: 11,
                        margin: [10, 5, 0, 15],
                        ol: _this.data1.serviesPoint0.map(function (ele) {
                            return [
                                ele.mainPonit,
                                {
                                    type: 'lower-alpha',
                                    ol: ele.subPonit.map(function (s) { return s.subPoints; })
                                }
                            ];
                        }).slice()
                    });
                }
                if (_this.data1.serviesPoint1.length > 0) {
                    _this.exs1.push({
                        text: 'Section3.',
                        fontSize: 12,
                    }, {
                        text: 'General terms',
                        fontSize: 12,
                        bold: true
                    }, {
                        margin: [10, 5, 0, 15],
                        fontSize: 11,
                        ol: _this.data1.serviesPoint1.map(function (ele) {
                            return [
                                ele.mainPonit,
                                {
                                    type: 'lower-alpha',
                                    ol: ele.subPonit.map(function (s) { return s.subPoints; })
                                }
                            ];
                        }).slice()
                    });
                }
                return;
            }
            else {
            }
        }, function (error) {
            console.log(error);
        });
    };
    AmcWithoutLogoComponent.prototype.createData = function (data) {
        this.listOfAmc.push(data);
    };
    AmcWithoutLogoComponent.prototype.getAmcDetailPoints = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.AmcServicePointsSer.getAllAmcPoints()];
                    case 1:
                        _a.listOfPoints = _b.sent();
                        this.listOfPoints.forEach(function (element) {
                            // console.log(element.serviceType ,"===",  this.servies,"hi",)
                            if (element.serviceType === _this.servies) {
                                // console.log(element.serviceType, "===", this.servies, "hii")
                                _this.elements.push(element);
                                return;
                            }
                            else {
                                _this.service = false;
                            }
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    AmcWithoutLogoComponent.prototype.capturefullScreen = function (event) {
        return __awaiter(this, void 0, void 0, function () {
            var resultAleart;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.confirmationDialogService.confirm('Alert Message', 'Do You Want To Download With Logo')];
                    case 1:
                        resultAleart = _a.sent();
                        if (resultAleart) {
                            // this.captureScreen(event);
                            this.options = true;
                        }
                        else {
                            this.options = false;
                        }
                        this.capturefullScreen1();
                        return [2 /*return*/];
                }
            });
        });
    };
    // public captureScreen(event) {
    //   html2canvas(document.getElementById('contentToConvert')).then(canvas => {
    //     var imgWidth = 208;
    //     var pageHeight = 272;
    //     var position = 30;
    //     var imgHeight = canvas.height * imgWidth / canvas.width;
    //     var heightLeft = imgHeight;
    //     const img = canvas.toDataURL("image/png");
    //     var pdf = new jspdf('p', 'mm', 'a4', true);
    //     while (heightLeft >= 0) {
    //       position = heightLeft - imgHeight;
    //       // console.log(position, "hii")
    //       pdf.addImage(img, 'png', 0, position, imgWidth, imgHeight, '', 'FAST');
    //       pdf.setFontSize(8)
    //       // pdf.setBackGroundColor(255, 0, 0)
    //       pdf.line(0, 279, 300, 279);
    //       pdf.text('#21, 4th MAIN, BALAJI LAYOUT, NEAR BADRAPPA LAYOUT, SANJAY NAGAR POST, BANGALORE-560094.', 100, 288, 'center');
    //       pdf.text(' Mail: ironbirdelevators@gmail.com, Landline: 080-23511445.', 100, 290, 'center')
    //       pdf.text(' ', 100, 290, 'center')
    //       pdf.addPage();
    //       heightLeft -= pageHeight;
    //     }
    //     pdf.save('IRB/BLR/' + this.data1.jobid + 'AMC');
    //   });
    // }
    AmcWithoutLogoComponent.prototype.capturefullScreen1 = function () {
        var documentDefinition = this.getDocumentDefinition(this.options);
        pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_1___default.a.createPdf(documentDefinition).download(this.data1.amcid + "_AMC.pdf");
    };
    AmcWithoutLogoComponent.prototype.getDocumentDefinition = function (options) {
        // this.printDate = this.quotation.agreementdate.split('-').reverse().join('/')
        var testImageDataUrl = this.image;
        var testImageDataUrl1 = this.image1;
        return {
            pageMargins: [20, 115, 20, 70],
            pageSize: 'A4',
            footer: function () {
                if (options) {
                    return [
                        [
                            // { canvas: [{ type: 'line', x1: 0, y1: 0, x2: 595, y2: 0, lineWidth: 0.5 }] },
                            {
                                stack: [
                                    {
                                        columns: [
                                            {
                                                text: '#21, 4th MAIN, BALAJI LAYOUT, NEAR BADRAPPA LAYOUT, SANJAY NAGAR POST, BANGALORE-560094.',
                                                width: '*',
                                                alignment: 'left',
                                                fontSize: 10,
                                                margin: [40, 15, 0, 0]
                                            }
                                        ]
                                    },
                                    {
                                        columns: [
                                            {
                                                text: 'Mail: ironbirdelevators@gmail.com, Landline: 080-23511445.',
                                                width: '*',
                                                alignment: 'left',
                                                fontSize: 10,
                                                margin: [40, 2, 0, 0]
                                            },
                                            {
                                                image: testImageDataUrl1,
                                                width: 80,
                                            }
                                        ]
                                    }
                                ]
                            }
                        ],
                    ];
                }
                else {
                    return;
                }
            },
            header: function () {
                // you can apply any logic and return any valid pdfmake element
                if (options) {
                    return [
                        {
                            image: testImageDataUrl,
                            width: 600,
                            height: 100,
                            margin: [0, 20, 0, 0]
                        }
                    ];
                }
                else {
                    return;
                }
            },
            content: [
                {
                    text: 'AMC AGREEMENT',
                    style: 'invoiceTitle',
                    width: '*'
                },
                '\n',
                this.exs,
                {
                    text: "Date: 12/02/2020",
                    alignment: 'right',
                    bold: true,
                    fontSize: 12,
                    margin: [0, 10, 0, 0]
                },
                '\n',
                {
                    margin: [0, 10, 0, 0],
                    columns: [
                        {
                            width: 170,
                            text: 'Name & Address of the client',
                            fontSize: 11,
                            bold: true
                        },
                        {
                            width: 150,
                            text: this.data1.customerName + "\n" + this.data1.clientAddress,
                            fontSize: 11
                        }
                    ]
                },
                {
                    margin: [0, 10, 0, 0],
                    columns: [
                        {
                            width: 170,
                            text: 'Installation address',
                            fontSize: 11,
                            bold: true
                        },
                        {
                            width: 150,
                            text: "" + this.data1.siteAddress,
                            fontSize: 11
                        }
                    ]
                },
                {
                    margin: [0, 10, 0, 0],
                    columns: [
                        {
                            width: 170,
                            text: 'AMC Amount',
                            fontSize: 11,
                            bold: true
                        },
                        {
                            width: 200,
                            text: [this.amcAmt + "/- ( " + this.amcAmtWords + " )\n",
                                { text: '(Above price is exclusive of GST 18%)', bold: true }],
                            fontSize: 11
                        }
                    ]
                },
                {
                    margin: [0, 10, 0, 0],
                    columns: [
                        {
                            width: 170,
                            text: 'AMC JOB No',
                            fontSize: 11,
                            bold: true
                        },
                        {
                            width: 150,
                            text: "" + this.data1.amcid,
                            fontSize: 11
                        }
                    ]
                },
                {
                    margin: [0, 10, 0, 0],
                    columns: [
                        {
                            width: 170,
                            text: 'AMC period',
                            fontSize: 11,
                            bold: true
                        },
                        {
                            width: 200,
                            text: [
                                { text: 'From ', bold: true },
                                "" + this.data1.fromData.split('-').reverse().join('/'),
                                { text: ' to ', bold: true },
                                "" + this.data1.toData.split('-').reverse().join('/')
                            ],
                            fontSize: 11
                        }
                    ]
                },
                {
                    text: 'Contact Person',
                    style: 'text1'
                },
                {
                    text: 'Phone Number',
                    style: 'text1'
                },
                {
                    text: 'E-Mail id',
                    style: 'text1'
                },
                {
                    text: 'Signature',
                    style: 'text1'
                },
                {
                    text: 'SIGNED ON BEHALF OF IRONBIRD ELEVATORS PVT LTD',
                    bold: true,
                    margin: [0, 25, 0, 0],
                    fontSize: 12
                },
                {
                    text: 'Signature (authorized signatory)',
                    margin: [0, 5, 0, 40],
                    fontSize: 11
                },
                {
                    text: 'Name & Designation',
                    fontSize: 11
                },
                {
                    text: 'In case of breakdown, our contact number: 8495866611, 1800-313-313-4, 849481111',
                    italics: true,
                    bold: true,
                    margin: [0, 40, 0, 0],
                    fontSize: 12,
                    alignment: 'center'
                },
                this.exs0,
                '\n\n',
                this.exs1
            ],
            styles: {
                // Invoice Title
                invoiceTitle: {
                    fontSize: 15,
                    bold: true,
                    alignment: 'center',
                    margin: [0, 35, 0, 0],
                },
                text1: {
                    bold: true,
                    margin: [0, 10, 0, 0],
                    fontSize: 11
                }
            },
            defaultStyle: {
                columnGap: 10,
            }
        };
    };
    AmcWithoutLogoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-amc-without-logo',
            template: __webpack_require__(/*! ./amc-without-logo.component.html */ "./src/app/pages/ui-features/amc/main-amc/print-amc/amc-without-logo/amc-without-logo.component.html"),
            styles: [__webpack_require__(/*! ./amc-without-logo.component.scss */ "./src/app/pages/ui-features/amc/main-amc/print-amc/amc-without-logo/amc-without-logo.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_AMC_amc_service__WEBPACK_IMPORTED_MODULE_6__["AmcService"],
            _confirmation_dialog_confirmation_dialog_service__WEBPACK_IMPORTED_MODULE_3__["ConfirmationDialogService"],
            _services_AMC_amc_service_points_service__WEBPACK_IMPORTED_MODULE_5__["AmcServicePointsService"],
            _services_image_service__WEBPACK_IMPORTED_MODULE_7__["ImageService"],
            _services_currency_service__WEBPACK_IMPORTED_MODULE_4__["CurrencyService"]])
    ], AmcWithoutLogoComponent);
    return AmcWithoutLogoComponent;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/amc/main-amc/print-amc/print-amc.component.html":
/*!***********************************************************************************!*\
  !*** ./src/app/pages/ui-features/amc/main-amc/print-amc/print-amc.component.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"A4\" id=\"contentToConvert\">\n    <div>\n        <img src=\"assets/images/banner.png\">\n    </div>\n    <h3> AMC</h3>\n    <br>\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-6\">\n                <b>Name & Address of the Client:{{data1.customerName}} <br> {{data1.clientAddress}}</b>\n                <br><br>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-12\">\n                <b>Installation Address: <div class=\"row\">\n            <div class=\"col-5\">{{data1.siteAddress}}</div>\n          </div></b>\n                <br><br>\n            </div>\n            <div class=\"col-12\">\n                <b>AMC Amount: {{amcAmt}}/-  ({{amcAmtWords}})</b><br>\n                <!-- VIPIN CODE FOR CURRENCY -->\n                (Above price is exclusive of GST 18%)\n                <br><br>\n            </div>\n            <div class=\"col-4\">\n                <b>AMC JOB No</b>\n            </div>\n            <div class=\"col-8\">\n                <p>:&nbsp;&nbsp; {{data1.amcid}}</p>\n            </div>\n            <div class=\"col-4\">\n                <b>AMC Period</b>\n                <br><br>\n            </div>\n            <div class=\"col-8\">\n                <p>:&nbsp;&nbsp; from <b>{{ data1.fromData | date}}</b> to <b>{{data1.toData | date}}</b></p>\n                <br><br>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-6\">\n                <b>Contact Person: <p>{{data1.customerName}}</p></b>\n                <br><br>\n            </div>\n            <div class=\"col-6\">\n                <b>Phone Number: <p></p></b>\n                <br><br>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-6\">\n                <b>E Mailid:Signature:</b>\n                <br><br>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-6\">\n                <b>SIGNED ON BEHALF OF IRONBIRD ELEVATORS</b>\n                <p>Signature (authorized signatory):</p>\n                <br><br>\n                <br>\n                <p>Name & Designation:</p>\n                <br><br>\n            </div>\n        </div>\n        <br> <br><br>\n        <div class=\"row\">\n            <div class=\"col-12\">\n                <b>In case of breakdown,our contact number:8849586611,8494811110,080-23511445</b><br>\n            </div>\n        </div>\n    </div>\n    <br><br><br><br><br><br><br><br><br>\n    <br><br><br><br><br>\n    <div *ngIf='servies !== \"\"'>\n        <h3 style=\"text-align:center\"> Type of Service {{servies}}</h3>\n        <br>\n        <ol>\n            <li *ngFor=\"let listPoints of elements;let i = index\">\n                <p>{{ listPoints.mainPonit }} </p>\n                <ol type=\"a\" [hidden]=\"listPoints.subPonit.length === 0\">\n                    <li *ngFor=\"let data of listPoints.subPonit;let i = index\">\n                        <p>{{data.subPoints}} </p>\n                    </li>\n                </ol>\n            </li>\n        </ol>\n    </div>\n</div>\n<!-- <div class=\"A4\" id=\"contentToConvert\">\n\n  <div>\n    <img src=\"assets/images/banner.png\">\n  </div>\n  <h3> AMC</h3>\n  <h3 style=\"text-align:center\"> Type of Service {{data1.servies}}</h3>\n  <br>\n  <ol>\n    <li *ngFor=\"let listPoints of elements;let i = index\">\n      <p>{{ listPoints.mainPonit }} </p>\n      <ol type=\"a\" [hidden]=\"listPoints.subPonit.length === 0\">\n        <li *ngFor=\"let data of listPoints.subPonit;let i = index\">\n          <p>{{data.subPoints}} </p>\n        </li>\n      </ol>\n    </li>\n  </ol>\n</div> -->\n<div class=\"row\">\n    <div class=\"col-12\">\n        <br><br>\n        <button class=\"btn btn-outline-primary float-left\" shape=\"semi-round\" (click)=\"capturefullScreen()\">Print/Download</button>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/pages/ui-features/amc/main-amc/print-amc/print-amc.component.scss":
/*!***********************************************************************************!*\
  !*** ./src/app/pages/ui-features/amc/main-amc/print-amc/print-amc.component.scss ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "h3 {\n  text-align: center; }\n\n.borders {\n  border: 1px solid black; }\n\n.A4 {\n  background: white;\n  width: 21cm;\n  display: block;\n  margin: 0 auto;\n  padding: 10px 25px;\n  margin-bottom: 0.5cm;\n  -webkit-box-shadow: 0 0 0.5cm rgba(0, 0, 0, 0.5);\n          box-shadow: 0 0 0.5cm rgba(0, 0, 0, 0.5);\n  overflow-y: scroll;\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  font-size: 12pt; }\n\n@media print {\n  .page-break {\n    display: block;\n    page-break-before: always; } }\n\n@media print {\n  body {\n    margin: 0;\n    padding: 0; }\n  .A4 {\n    -webkit-box-shadow: none;\n            box-shadow: none;\n    margin: 0;\n    width: auto;\n    height: auto; }\n  .enable-print {\n    display: block; } }\n\nimg {\n  max-width: 97%; }\n\ntd {\n  width: 50%;\n  font-size: 12px;\n  line-height: 2.5; }\n\nul {\n  list-style-type: none; }\n\n/deep/ modal.modale .modal-body {\n  overflow-y: scroll; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3ByYWthc2gvVmlwaW4vaXJvbmJpcmQvMjBGRUIyMDIwL2lyb25iaXJkLWNsaWVudHMvc3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9hbWMvbWFpbi1hbWMvcHJpbnQtYW1jL3ByaW50LWFtYy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGtCQUFrQixFQUFBOztBQUdwQjtFQUNFLHVCQUNGLEVBQUE7O0FBRUE7RUFDRSxpQkFBaUI7RUFDakIsV0FBVztFQUVYLGNBQWM7RUFDZCxjQUFjO0VBQ2Qsa0JBQWtCO0VBQ2xCLG9CQUFvQjtFQUNwQixnREFBd0M7VUFBeEMsd0NBQXdDO0VBQ3hDLGtCQUFrQjtFQUNsQiw4QkFBc0I7VUFBdEIsc0JBQXNCO0VBQ3RCLGVBQWUsRUFBQTs7QUFHakI7RUFDRTtJQUNJLGNBQWM7SUFDZCx5QkFBeUIsRUFBQSxFQUM1Qjs7QUFJSDtFQUNFO0lBQ0ksU0FBUztJQUNULFVBQVUsRUFBQTtFQUVkO0lBQ0ksd0JBQWdCO1lBQWhCLGdCQUFnQjtJQUNoQixTQUFTO0lBQ1QsV0FBVztJQUNYLFlBQVksRUFBQTtFQUVoQjtJQUNJLGNBQWMsRUFBQSxFQUNqQjs7QUFHSDtFQUNFLGNBQWMsRUFBQTs7QUFHaEI7RUFDRSxVQUFVO0VBQ1YsZUFBZTtFQUNmLGdCQUFnQixFQUFBOztBQUdsQjtFQUNFLHFCQUFxQixFQUFBOztBQUd2QjtFQUNFLGtCQUFrQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvdWktZmVhdHVyZXMvYW1jL21haW4tYW1jL3ByaW50LWFtYy9wcmludC1hbWMuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJoMyB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmJvcmRlcnMge1xuICBib3JkZXI6IDFweCBzb2xpZCBibGFja1xufVxuXG4uQTQge1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgd2lkdGg6IDIxY207XG4gIC8vIGhlaWdodDogMzAuN2NtO1xuICBkaXNwbGF5OiBibG9jaztcbiAgbWFyZ2luOiAwIGF1dG87XG4gIHBhZGRpbmc6IDEwcHggMjVweDtcbiAgbWFyZ2luLWJvdHRvbTogMC41Y207XG4gIGJveC1zaGFkb3c6IDAgMCAwLjVjbSByZ2JhKDAsIDAsIDAsIDAuNSk7XG4gIG92ZXJmbG93LXk6IHNjcm9sbDtcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgZm9udC1zaXplOiAxMnB0O1xufVxuXG5AbWVkaWEgcHJpbnQge1xuICAucGFnZS1icmVhayB7XG4gICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgIHBhZ2UtYnJlYWstYmVmb3JlOiBhbHdheXM7XG4gIH1cbiAgLy8gc2l6ZTogQTQgcG9ydHJhaXQ7XG59XG5cbkBtZWRpYSBwcmludCB7XG4gIGJvZHkge1xuICAgICAgbWFyZ2luOiAwO1xuICAgICAgcGFkZGluZzogMDtcbiAgfVxuICAuQTQge1xuICAgICAgYm94LXNoYWRvdzogbm9uZTtcbiAgICAgIG1hcmdpbjogMDtcbiAgICAgIHdpZHRoOiBhdXRvO1xuICAgICAgaGVpZ2h0OiBhdXRvO1xuICB9XG4gIC5lbmFibGUtcHJpbnQge1xuICAgICAgZGlzcGxheTogYmxvY2s7XG4gIH1cbn1cblxuaW1nIHtcbiAgbWF4LXdpZHRoOiA5NyU7XG59XG5cbnRkIHtcbiAgd2lkdGg6IDUwJTtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBsaW5lLWhlaWdodDogMi41O1xufVxuXG51bCB7XG4gIGxpc3Qtc3R5bGUtdHlwZTogbm9uZTtcbn1cblxuL2RlZXAvIG1vZGFsLm1vZGFsZSAubW9kYWwtYm9keSB7XG4gIG92ZXJmbG93LXk6IHNjcm9sbDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/ui-features/amc/main-amc/print-amc/print-amc.component.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/pages/ui-features/amc/main-amc/print-amc/print-amc.component.ts ***!
  \*********************************************************************************/
/*! exports provided: PrintAmcComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PrintAmcComponent", function() { return PrintAmcComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _confirmation_dialog_confirmation_dialog_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../confirmation-dialog/confirmation-dialog.service */ "./src/app/confirmation-dialog/confirmation-dialog.service.ts");
/* harmony import */ var _services_currency_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../services/currency.service */ "./src/app/services/currency.service.ts");
/* harmony import */ var _services_AMC_amc_service_points_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../services/AMC/amc-service-points.service */ "./src/app/services/AMC/amc-service-points.service.ts");
/* harmony import */ var _services_AMC_amc_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../services/AMC/amc.service */ "./src/app/services/AMC/amc.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




 // VIPIN CODE FOR CURRENCY
var PrintAmcComponent = /** @class */ (function () {
    // @Output() AmcChange = new EventEmitter()
    function PrintAmcComponent(AmcServ, confirmationDialogService, AmcServicePointsSer, currency) {
        this.AmcServ = AmcServ;
        this.confirmationDialogService = confirmationDialogService;
        this.AmcServicePointsSer = AmcServicePointsSer;
        this.currency = currency;
        // @ViewChild('componentInsideModal') componentInsideModal: ModalComponent;
        this.data1 = { invoiceItems: [] };
        this.show = true; // data2: object = {};
        this.editShow = false;
        this.listOfAmc = [];
        this.elements = [];
        this.type = {};
        this.getAmcDetail();
        // this.getAmcDetailPoints();
    }
    PrintAmcComponent.prototype.ngOnInit = function () {
    };
    PrintAmcComponent.prototype.getAmcDetail = function () {
        var _this = this;
        this.AmcServ.AmcDone
            .subscribe(function (data) {
            _this.data1 = data;
            // NEW CODE STARTS           // VIPIN CODE FOR CURRENCY
            _this.amcAmt = _this.currency.convertToInrFormat(_this.data1.totalPrice); // VIPIN CODE FOR CURRENCY
            _this.amcAmtWords = _this.currency.convertNumToWords(_this.data1.unitprice.split(".")[0]); // VIPIN CODE FOR CURRENCY
            // NEW CODE ENDS           // VIPIN CODE FOR CURRENCY
            // console.log(typeof "word")
            if (!_this.data1.id) {
                return _this.show = false;
            }
            _this.id = _this.data1.id;
            // to check if services added
            _this.servies = data.servies;
            if (_this.servies !== '') {
                _this.servicesPonitsList = _this.data1.serviesPoint;
                _this.servicesPonitsList.forEach(function (element) {
                    _this.elements.push(element);
                    return;
                });
            }
        }, function (error) {
            console.log(error);
        });
    };
    PrintAmcComponent.prototype.createData = function (data) {
        this.listOfAmc.push(data);
    };
    // async getAmcDetailPoints() {
    //   this.listOfPoints = await this.AmcServicePointsSer.getAllAmcPoints();
    //   this.listOfPoints.forEach((element) => {
    //     // console.log(element.serviceType ,"===",  this.servies,"hi",)
    //     if (element.serviceType === this.servies) {
    //       console.log(element.serviceType, "===", this.servies, "hii")
    //       this.elements.push(element);
    //       return;
    //     } else {
    //     }
    //   })
    // }
    // ******* For print the screen started here **********//
    PrintAmcComponent.prototype.capturefullScreen = function (event) {
        return __awaiter(this, void 0, void 0, function () {
            var resultAleart;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.confirmationDialogService.confirm('Alert Message', 'Are You Sure You Want To Download ?')];
                    case 1:
                        resultAleart = _a.sent();
                        if (resultAleart) {
                            // this.captureScreen(event);
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    PrintAmcComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-print-amc',
            template: __webpack_require__(/*! ./print-amc.component.html */ "./src/app/pages/ui-features/amc/main-amc/print-amc/print-amc.component.html"),
            styles: [__webpack_require__(/*! ./print-amc.component.scss */ "./src/app/pages/ui-features/amc/main-amc/print-amc/print-amc.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_AMC_amc_service__WEBPACK_IMPORTED_MODULE_4__["AmcService"],
            _confirmation_dialog_confirmation_dialog_service__WEBPACK_IMPORTED_MODULE_1__["ConfirmationDialogService"],
            _services_AMC_amc_service_points_service__WEBPACK_IMPORTED_MODULE_3__["AmcServicePointsService"],
            _services_currency_service__WEBPACK_IMPORTED_MODULE_2__["CurrencyService"]])
    ], PrintAmcComponent);
    return PrintAmcComponent;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/amc/main-amc/services-stage/services-stage.component.html":
/*!*********************************************************************************************!*\
  !*** ./src/app/pages/ui-features/amc/main-amc/services-stage/services-stage.component.html ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div class=\"row\">\n    <div class=\"col-md-12\" style=\" background-color: #e1f5fe;\">\n      <br>\n      <nb-card-header>\n        {{servies}} Service List\n        <!-- <button class=\"btn btn-outline-primary \" shape=\"semi-round\" (click)=\"componentInsideModals.open()\"  mdbWavesEffect >Add,Delete and Update </button>  -->\n      </nb-card-header>\n      <nb-card-body>\n        <br>\n        <ol>\n            <li *ngFor=\"let listPoints of elements;let i = index\">\n              <!-- <p>{{ listPoints.mainPonit }} </p> -->\n              <textarea type=\"text\" type=\"text\" class=\"form-control\" [(ngModel)]=\"listPoints.mainPonit\"></textarea>\n              <ol type=\"a\" [hidden]=\"listPoints.subPonit.length === 0\">\n                <br>\n                <li *ngFor=\"let data of listPoints.subPonit;let i = index\">\n                  <!-- <p>{{data.subPoints}} </p> -->\n                  <input type=\"text\" type=\"text\" class=\"form-control\" [(ngModel)]=\"data.subPoints\">\n                </li>\n              </ol>\n              <br>\n            </li>\n          </ol>\n      </nb-card-body>\n    </div>\n  </div>\n</div>\n\n"

/***/ }),

/***/ "./src/app/pages/ui-features/amc/main-amc/services-stage/services-stage.component.scss":
/*!*********************************************************************************************!*\
  !*** ./src/app/pages/ui-features/amc/main-amc/services-stage/services-stage.component.scss ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "button {\n  float: right; }\n\n@media (max-width: 600px) {\n  button {\n    float: none;\n    margin-top: 1px; } }\n\nmodal.mode h4 {\n  margin-top: 25px; }\n\nmodal.mode .row {\n  margin-bottom: 20px; }\n\nmodal.mode .row .row {\n  margin-top: 10px;\n  margin-bottom: 0; }\n\nmodal.mode [class*=\"col-\"] {\n  padding-top: 15px;\n  padding-bottom: 15px;\n  background-color: #eee;\n  background-color: rgba(86, 61, 124, 0.15);\n  border: 1px solid #ddd;\n  border: 1px solid rgba(86, 61, 124, 0.2); }\n\nmodal.mode hr {\n  margin-top: 40px;\n  margin-bottom: 40px; }\n\nmodal.mode .dndPlaceholder {\n  background-color: #ddd;\n  display: block;\n  min-height: 42px;\n  padding-top: 15px;\n  padding-bottom: 15px;\n  border: 1px solid rgba(86, 61, 124, 0.2);\n  position: relative;\n  padding-right: 15px;\n  padding-left: 15px; }\n\n@media (min-width: 576px) {\n  /deep/ modal.models .modal .modal-dialog {\n    max-width: 1000px !important; } }\n\n@media (min-width: 576px) {\n  .modal-dialog {\n    max-width: 1000px !important; } }\n\n.nb-arrow-retweet, .nb-angle-double-right, .nb-angle-double-left {\n  font-size: 50px; }\n\n.containers {\n  margin-top: 120px; }\n\n.nb-edit {\n  font-size: 1.5rem;\n  cursor: pointer;\n  float: right; }\n\n.img-vert {\n  -webkit-transform: rotateX(180deg);\n          transform: rotateX(180deg); }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3ByYWthc2gvVmlwaW4vaXJvbmJpcmQvMjBGRUIyMDIwL2lyb25iaXJkLWNsaWVudHMvc3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9hbWMvbWFpbi1hbWMvc2VydmljZXMtc3RhZ2Uvc2VydmljZXMtc3RhZ2UuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxZQUFZLEVBQUE7O0FBRWhCO0VBQ0k7SUFDSSxXQUFXO0lBQ1gsZUFBZSxFQUFBLEVBQ2xCOztBQUdMO0VBQ0ksZ0JBQWdCLEVBQUE7O0FBRWxCO0VBQ0UsbUJBQW1CLEVBQUE7O0FBRXJCO0VBQ0UsZ0JBQWdCO0VBQ2hCLGdCQUFnQixFQUFBOztBQUVsQjtFQUNFLGlCQUFpQjtFQUNqQixvQkFBb0I7RUFDcEIsc0JBQXNCO0VBQ3RCLHlDQUFxQztFQUNyQyxzQkFBc0I7RUFDdEIsd0NBQW9DLEVBQUE7O0FBR3RDO0VBQ0UsZ0JBQWdCO0VBQ2hCLG1CQUFtQixFQUFBOztBQUdyQjtFQUNFLHNCQUFzQjtFQUN0QixjQUFjO0VBQ2QsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixvQkFBb0I7RUFDcEIsd0NBQW9DO0VBQ3BDLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsa0JBQWtCLEVBQUE7O0FBSXBCO0VBQ0k7SUFDSSw0QkFBNEIsRUFBQSxFQUMvQjs7QUFFTDtFQUNGO0lBQ0ksNEJBQTRCLEVBQUEsRUFDL0I7O0FBSUE7RUFDRyxlQUFlLEVBQUE7O0FBRWpCO0VBQ0UsaUJBQWlCLEVBQUE7O0FBR25CO0VBQ0UsaUJBQWlCO0VBQ2pCLGVBQWU7RUFDZixZQUFZLEVBQUE7O0FBR2Q7RUFDRSxrQ0FBMEI7VUFBMUIsMEJBQTBCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9hbWMvbWFpbi1hbWMvc2VydmljZXMtc3RhZ2Uvc2VydmljZXMtc3RhZ2UuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJidXR0b257XG4gICAgZmxvYXQ6IHJpZ2h0O1xufVxuQG1lZGlhIChtYXgtd2lkdGg6NjAwcHgpe1xuICAgIGJ1dHRvbntcbiAgICAgICAgZmxvYXQ6IG5vbmU7XG4gICAgICAgIG1hcmdpbi10b3A6IDFweDtcbiAgICB9XG59XG5cbm1vZGFsLm1vZGUgaDQge1xuICAgIG1hcmdpbi10b3A6IDI1cHg7XG4gIH1cbiAgbW9kYWwubW9kZSAucm93IHtcbiAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xuICB9XG4gIG1vZGFsLm1vZGUgLnJvdyAucm93IHtcbiAgICBtYXJnaW4tdG9wOiAxMHB4O1xuICAgIG1hcmdpbi1ib3R0b206IDA7XG4gIH1cbiAgbW9kYWwubW9kZSBbY2xhc3MqPVwiY29sLVwiXSB7XG4gICAgcGFkZGluZy10b3A6IDE1cHg7XG4gICAgcGFkZGluZy1ib3R0b206IDE1cHg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2VlZTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDg2LDYxLDEyNCwuMTUpO1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNkZGQ7XG4gICAgYm9yZGVyOiAxcHggc29saWQgcmdiYSg4Niw2MSwxMjQsLjIpO1xuICB9XG4gIFxuICBtb2RhbC5tb2RlIGhyIHtcbiAgICBtYXJnaW4tdG9wOiA0MHB4O1xuICAgIG1hcmdpbi1ib3R0b206IDQwcHg7XG4gIH1cblxuICBtb2RhbC5tb2RlIC5kbmRQbGFjZWhvbGRlcntcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZGRkO1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIG1pbi1oZWlnaHQ6IDQycHg7XG4gICAgcGFkZGluZy10b3A6IDE1cHg7XG4gICAgcGFkZGluZy1ib3R0b206IDE1cHg7XG4gICAgYm9yZGVyOiAxcHggc29saWQgcmdiYSg4Niw2MSwxMjQsLjIpO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBwYWRkaW5nLXJpZ2h0OiAxNXB4O1xuICAgIHBhZGRpbmctbGVmdDogMTVweDtcbiAgICBcbiAgfVxuXG4gIEBtZWRpYShtaW4td2lkdGg6IDU3NnB4KXtcbiAgICAgIC9kZWVwLyBtb2RhbC5tb2RlbHMgLm1vZGFsIC5tb2RhbC1kaWFsb2d7XG4gICAgICAgICAgbWF4LXdpZHRoOiAxMDAwcHggIWltcG9ydGFudDtcbiAgICAgIH1cbiAgfVxuICBAbWVkaWEgKG1pbi13aWR0aDogNTc2cHgpe1xuLm1vZGFsLWRpYWxvZyB7XG4gICAgbWF4LXdpZHRoOiAxMDAwcHggIWltcG9ydGFudDtcbn1cbn1cblxuXG4gLm5iLWFycm93LXJldHdlZXQsIC5uYi1hbmdsZS1kb3VibGUtcmlnaHQsIC5uYi1hbmdsZS1kb3VibGUtbGVmdHtcbiAgICBmb250LXNpemU6IDUwcHg7XG4gIH1cbiAgLmNvbnRhaW5lcnN7XG4gICAgbWFyZ2luLXRvcDogMTIwcHg7XG4gIH1cblxuICAubmItZWRpdHtcbiAgICBmb250LXNpemU6IDEuNXJlbTtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgZmxvYXQ6IHJpZ2h0O1xuICB9XG5cbiAgLmltZy12ZXJ0IHtcbiAgICB0cmFuc2Zvcm06IHJvdGF0ZVgoMTgwZGVnKTtcbiAgfSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/ui-features/amc/main-amc/services-stage/services-stage.component.ts":
/*!*******************************************************************************************!*\
  !*** ./src/app/pages/ui-features/amc/main-amc/services-stage/services-stage.component.ts ***!
  \*******************************************************************************************/
/*! exports provided: ServicesStageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServicesStageComponent", function() { return ServicesStageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_AMC_amc_service_points_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../services/AMC/amc-service-points.service */ "./src/app/services/AMC/amc-service-points.service.ts");
/* harmony import */ var _services_AMC_amc_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../services/AMC/amc.service */ "./src/app/services/AMC/amc.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var ServicesStageComponent = /** @class */ (function () {
    function ServicesStageComponent(fb, AmcServicePointsSer, AmcServ) {
        this.fb = fb;
        this.AmcServicePointsSer = AmcServicePointsSer;
        this.AmcServ = AmcServ;
        this.elements = [];
        this.type = {};
        this.data1 = { invoiceItems: [] };
        this.getAmcDetailPoints();
    }
    ServicesStageComponent.prototype.getAmcDetail = function () {
        var _this = this;
        this.AmcServ.AmcDone
            .subscribe(function (data) {
            _this.data1 = data;
            _this.servies = data.servies;
            //  console.log(this.servies)
            // this.getAmcDetailPoints();
        }, function (error) {
            console.log(error);
        });
    };
    ServicesStageComponent.prototype.getAmcDetailPoints = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        this.getAmcDetail();
                        _a = this;
                        return [4 /*yield*/, this.AmcServicePointsSer.getAllAmcPoints()];
                    case 1:
                        _a.listOfPoints = _b.sent();
                        this.listOfPoints.forEach(function (element) {
                            if (element.serviceType === _this.servies) {
                                // console.log(element.serviceType ,"===", this.servies,"hii")
                                _this.elements.push(element);
                                return;
                            }
                            else {
                            }
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    ServicesStageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-services-stage',
            template: __webpack_require__(/*! ./services-stage.component.html */ "./src/app/pages/ui-features/amc/main-amc/services-stage/services-stage.component.html"),
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewEncapsulation"].None,
            styles: [__webpack_require__(/*! ./services-stage.component.scss */ "./src/app/pages/ui-features/amc/main-amc/services-stage/services-stage.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            _services_AMC_amc_service_points_service__WEBPACK_IMPORTED_MODULE_2__["AmcServicePointsService"],
            _services_AMC_amc_service__WEBPACK_IMPORTED_MODULE_3__["AmcService"]])
    ], ServicesStageComponent);
    return ServicesStageComponent;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/amc/main-amc/upload-amc-doc/upload-amc-doc.component.html":
/*!*********************************************************************************************!*\
  !*** ./src/app/pages/ui-features/amc/main-amc/upload-amc-doc/upload-amc-doc.component.html ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\" *ngIf=\"!show;\">\n    <div class=\"row\">\n        <div class=\"col-12\">\n            <br>\n            <p class=\"back\">Client & Site: {{data1.customerName}} & {{data1.siteName}}</p>\n        </div>\n    </div>\n\n    <div>\n        <div>\n            <h4>Total Files attached\n                <button *ngIf='moduleCreate' class=\"btn btn-outline-primary\" shape=\"semi-round\" (click)=\"componentInsideModals.open()\" mdbWavesEffect>Add\n          New Attachments</button>\n            </h4>\n            <p>Click on the description to view</p>\n        </div>\n\n    </div>\n\n\n    <ng2-smart-table [settings]=\"settings\" [source]=\"source\" (deleteConfirm)=\"onDeleteConfirm($event)\" (custom)=\"onUserRowSelect($event)\" (userRowSelect)=\"onUserRowSelect($event)\">\n    </ng2-smart-table>\n    <br>\n\n</div>\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n<modal #componentInsideModal>\n    <ng-template #modalHeader>\n    </ng-template>\n    <ng-template #modalBody>\n        <img src=\"assets/liftImage/standard.png\" style=\"width: 100%\">\n        <br>\n    </ng-template>\n    <ng-template #modalFooter></ng-template>\n</modal>\n\n<modal class=\"models\" #componentInsideModals>\n    <ng-template #modalHeader>\n        <h4 class=\"text-center\"> Attachments</h4>\n    </ng-template>\n    <ng-template #modalBody>\n        <form #AttachmentFortm=\"ngForm\" name=\"form\" (ngSubmit)=\"AttachmentFortm.form.valid && createNewAttachment(AttachmentFortm);\" enctype=\"multipart/form-data\" novalidate>\n            <div class=\"container\">\n\n                <div class=\"form-group input-group-sm \">\n                    <label for=\"exampleFormControlFile2\">File Name</label>\n                    <input type=\"text\" class=\"form-control form-control-file\" id=\"exampleFormControlFile2\" [(ngModel)]=\"data1.filenames\" name=\"filenames\" #filenames=\"ngModel\" [ngClass]=\"{ 'is-invalid': AttachmentFortm.submitted && filenames.invalid }\" placeholder=\"File Name\"\n                        required>\n                    <div *ngIf=\"AttachmentFortm.submitted && filenames.invalid\" class=\"invalid-feedback\">\n                        <div *ngIf=\"filenames.errors.required\">File Name is required</div>\n                    </div>\n                </div>\n                <div class=\"form-group input-group-sm\">\n                    <label for=\"inputLastName\"> Description</label>\n                    <div class=\"input-group\">\n                        <textarea rows=\"3\" placeholder=\"Description\" class=\"form-control\" [(ngModel)]=\"data1.descriptions\" name=\"descriptions\" #description=\"ngModel\" [ngClass]=\"{ 'is-invalid': AttachmentFortm.submitted && description.invalid }\" required></textarea>\n                        <div *ngIf=\"AttachmentFortm.submitted && description.invalid\" class=\"invalid-feedback\">\n                            <div *ngIf=\"description.errors.required\">Description is required</div>\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"form-group input-group-sm\">\n                    <label for=\"exampleFormControlFile1\">Upload Files</label>\n                    <input type=\"file\" class=\"form-control form-control-file\" id=\"exampleFormControlFile1\" accept=\"application/pdf\" (change)=\"coverImageChangeEvent($event.target.files);\" [(ngModel)]=\"data1.attachement\" name=\"quotes-attach\" #attachement=\"ngModel\" [ngClass]=\"{ 'is-invalid': AttachmentFortm.submitted && attachement.invalid }\"\n                        required>\n                    <div *ngIf=\"AttachmentFortm.submitted && attachement.invalid\" class=\"invalid-feedback\">\n                        <div *ngIf=\"attachement.errors.required\">Upload is required</div>\n                    </div>\n                </div>\n                <div class=\"row foot\">\n                    <div class=\"col-sm-12\">\n                        <button type=\"submit\" class=\"btn btn-outline-primary \" shape=\"semi-round\" style=\"margin-top: 3px;\">Save</button>\n                    </div>\n                </div>\n\n            </div>\n        </form>\n    </ng-template>\n    <ng-template #modalFooter></ng-template>\n</modal>"

/***/ }),

/***/ "./src/app/pages/ui-features/amc/main-amc/upload-amc-doc/upload-amc-doc.component.scss":
/*!*********************************************************************************************!*\
  !*** ./src/app/pages/ui-features/amc/main-amc/upload-amc-doc/upload-amc-doc.component.scss ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * This is a starting point where we declare the maps of themes and globally available functions/mixins\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * This mixin generates keyfames.\n * Because of all keyframes can't be scoped,\n * we need to puts unique name in each btn-pulse call.\n */\n/*\n\nAccording to the specification (https://www.w3.org/TR/css-scoping-1/#host-selector)\n:host and :host-context are pseudo-classes. So we assume they could be combined,\nlike other pseudo-classes, even same ones.\nFor example: ':nth-of-type(2n):nth-of-type(even)'.\n\nIdeal solution would be to prepend any selector with :host-context([dir=rtl]).\nThen nebular components will behave as an html element and respond to [dir] attribute on any level,\nso direction could be overridden on any component level.\n\nImplementation code:\n\n@mixin nb-rtl() {\n  // add # to scss interpolation statement.\n  // it works in comments and we can't use it here\n  @at-root {selector-append(':host-context([dir=rtl])', &)} {\n    @content;\n  }\n}\n\nAnd when we call it somewhere:\n\n:host {\n  .some-class {\n    @include nb-rtl() {\n      ...\n    }\n  }\n}\n:host-context(...) {\n  .some-class {\n    @include nb-rtl() {\n      ...\n    }\n  }\n}\n\nResult will look like:\n\n:host-context([dir=rtl]):host .some-class {\n  ...\n}\n:host-context([dir=rtl]):host-context(...) .some-class {\n  ...\n}\n\n*\n  Side note:\n  :host-context():host selector are valid. https://lists.w3.org/Archives/Public/www-style/2015Feb/0305.html\n\n  :host-context([dir=rtl]):host-context(...) should match any permutation,\n  so order is not important.\n*\n\n\nCurrently, there're two problems with this approach:\n\nFirst, is that we can't combine :host, :host-context. Angular bugs #14349, #19199.\nFor the moment of writing, the only possible way is:\n:host {\n  :host-context(...) {\n    ...\n  }\n}\nIt doesn't work for us because mixin could be called somewhere deeper, like:\n:host {\n  p {\n    @include nb-rtl() { ... }\n  }\n}\nWe are not able to go up to :host level to place content passed to mixin.\n\nThe second problem is that we only can be sure that we appending :host-context([dir=rtl]) to another\n:host/:host-context pseudo-class when called in theme files (*.theme.scss).\n  *\n    Side note:\n    Currently, nb-install-component uses another approach where :host prepended with the theme name\n    (https://github.com/angular/angular/blob/5b96078624b0a4760f2dbcf6fdf0bd62791be5bb/packages/compiler/src/shadow_css.ts#L441),\n    but it was made to be able to use current realization of rtl and it can be rewritten back to\n    :host-context($theme) once we will be able to use multiple shadow selectors.\n  *\nBut when it's called in *.component.scss we can't be sure, that selector starts with :host/:host-context,\nbecause angular allows omitting pseudo-classes if we don't need to style :host component itself.\nWe can break such selectors, by just appending :host-context([dir=rtl]) to them.\n  ***\n    Possible solution\n    check if we in theme by some theme variables and if so append, otherwise nest like\n    @at-root :host-context([dir=rtl]) {\n      // add # to scss interpolation statement.\n      // it works in comments and we can't use it here\n      {&} {\n        @content;\n      }\n    }\n    What if :host specified? Can we add space in :host-context(...) :host?\n    Or maybe add :host selector anyway? If multiple :host selectors are allowed\n  ***\n\n\nProblems with the current approach.\n\n1. Direction can be applied only on document level, because mixin prepends theme class,\nwhich placed on the body.\n2. *.component.scss styles should be in :host selector. Otherwise angular will add host\nattribute to [dir=rtl] attribute as well.\n\n\nGeneral problems.\n\nLtr is default document direction, but for proper work of nb-ltr (means ltr only),\n[dir=ltr] should be specified at least somewhere. ':not([dir=rtl]' not applicable here,\nbecause it's satisfy any parent, that don't have [dir=rtl] attribute.\nPrevious approach was to use single rtl mixin and reset ltr properties to initial value.\nBut sometimes it's hard to find, what the previous value should be. And such mixin call looks too verbose.\n*/\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * This mixin generates keyfames.\n * Because of all keyframes can't be scoped,\n * we need to puts unique name in each btn-pulse call.\n */\n/*\n\nAccording to the specification (https://www.w3.org/TR/css-scoping-1/#host-selector)\n:host and :host-context are pseudo-classes. So we assume they could be combined,\nlike other pseudo-classes, even same ones.\nFor example: ':nth-of-type(2n):nth-of-type(even)'.\n\nIdeal solution would be to prepend any selector with :host-context([dir=rtl]).\nThen nebular components will behave as an html element and respond to [dir] attribute on any level,\nso direction could be overridden on any component level.\n\nImplementation code:\n\n@mixin nb-rtl() {\n  // add # to scss interpolation statement.\n  // it works in comments and we can't use it here\n  @at-root {selector-append(':host-context([dir=rtl])', &)} {\n    @content;\n  }\n}\n\nAnd when we call it somewhere:\n\n:host {\n  .some-class {\n    @include nb-rtl() {\n      ...\n    }\n  }\n}\n:host-context(...) {\n  .some-class {\n    @include nb-rtl() {\n      ...\n    }\n  }\n}\n\nResult will look like:\n\n:host-context([dir=rtl]):host .some-class {\n  ...\n}\n:host-context([dir=rtl]):host-context(...) .some-class {\n  ...\n}\n\n*\n  Side note:\n  :host-context():host selector are valid. https://lists.w3.org/Archives/Public/www-style/2015Feb/0305.html\n\n  :host-context([dir=rtl]):host-context(...) should match any permutation,\n  so order is not important.\n*\n\n\nCurrently, there're two problems with this approach:\n\nFirst, is that we can't combine :host, :host-context. Angular bugs #14349, #19199.\nFor the moment of writing, the only possible way is:\n:host {\n  :host-context(...) {\n    ...\n  }\n}\nIt doesn't work for us because mixin could be called somewhere deeper, like:\n:host {\n  p {\n    @include nb-rtl() { ... }\n  }\n}\nWe are not able to go up to :host level to place content passed to mixin.\n\nThe second problem is that we only can be sure that we appending :host-context([dir=rtl]) to another\n:host/:host-context pseudo-class when called in theme files (*.theme.scss).\n  *\n    Side note:\n    Currently, nb-install-component uses another approach where :host prepended with the theme name\n    (https://github.com/angular/angular/blob/5b96078624b0a4760f2dbcf6fdf0bd62791be5bb/packages/compiler/src/shadow_css.ts#L441),\n    but it was made to be able to use current realization of rtl and it can be rewritten back to\n    :host-context($theme) once we will be able to use multiple shadow selectors.\n  *\nBut when it's called in *.component.scss we can't be sure, that selector starts with :host/:host-context,\nbecause angular allows omitting pseudo-classes if we don't need to style :host component itself.\nWe can break such selectors, by just appending :host-context([dir=rtl]) to them.\n  ***\n    Possible solution\n    check if we in theme by some theme variables and if so append, otherwise nest like\n    @at-root :host-context([dir=rtl]) {\n      // add # to scss interpolation statement.\n      // it works in comments and we can't use it here\n      {&} {\n        @content;\n      }\n    }\n    What if :host specified? Can we add space in :host-context(...) :host?\n    Or maybe add :host selector anyway? If multiple :host selectors are allowed\n  ***\n\n\nProblems with the current approach.\n\n1. Direction can be applied only on document level, because mixin prepends theme class,\nwhich placed on the body.\n2. *.component.scss styles should be in :host selector. Otherwise angular will add host\nattribute to [dir=rtl] attribute as well.\n\n\nGeneral problems.\n\nLtr is default document direction, but for proper work of nb-ltr (means ltr only),\n[dir=ltr] should be specified at least somewhere. ':not([dir=rtl]' not applicable here,\nbecause it's satisfy any parent, that don't have [dir=rtl] attribute.\nPrevious approach was to use single rtl mixin and reset ltr properties to initial value.\nBut sometimes it's hard to find, what the previous value should be. And such mixin call looks too verbose.\n*/\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * This mixin generates keyfames.\n * Because of all keyframes can't be scoped,\n * we need to puts unique name in each btn-pulse call.\n */\n/*\n\nAccording to the specification (https://www.w3.org/TR/css-scoping-1/#host-selector)\n:host and :host-context are pseudo-classes. So we assume they could be combined,\nlike other pseudo-classes, even same ones.\nFor example: ':nth-of-type(2n):nth-of-type(even)'.\n\nIdeal solution would be to prepend any selector with :host-context([dir=rtl]).\nThen nebular components will behave as an html element and respond to [dir] attribute on any level,\nso direction could be overridden on any component level.\n\nImplementation code:\n\n@mixin nb-rtl() {\n  // add # to scss interpolation statement.\n  // it works in comments and we can't use it here\n  @at-root {selector-append(':host-context([dir=rtl])', &)} {\n    @content;\n  }\n}\n\nAnd when we call it somewhere:\n\n:host {\n  .some-class {\n    @include nb-rtl() {\n      ...\n    }\n  }\n}\n:host-context(...) {\n  .some-class {\n    @include nb-rtl() {\n      ...\n    }\n  }\n}\n\nResult will look like:\n\n:host-context([dir=rtl]):host .some-class {\n  ...\n}\n:host-context([dir=rtl]):host-context(...) .some-class {\n  ...\n}\n\n*\n  Side note:\n  :host-context():host selector are valid. https://lists.w3.org/Archives/Public/www-style/2015Feb/0305.html\n\n  :host-context([dir=rtl]):host-context(...) should match any permutation,\n  so order is not important.\n*\n\n\nCurrently, there're two problems with this approach:\n\nFirst, is that we can't combine :host, :host-context. Angular bugs #14349, #19199.\nFor the moment of writing, the only possible way is:\n:host {\n  :host-context(...) {\n    ...\n  }\n}\nIt doesn't work for us because mixin could be called somewhere deeper, like:\n:host {\n  p {\n    @include nb-rtl() { ... }\n  }\n}\nWe are not able to go up to :host level to place content passed to mixin.\n\nThe second problem is that we only can be sure that we appending :host-context([dir=rtl]) to another\n:host/:host-context pseudo-class when called in theme files (*.theme.scss).\n  *\n    Side note:\n    Currently, nb-install-component uses another approach where :host prepended with the theme name\n    (https://github.com/angular/angular/blob/5b96078624b0a4760f2dbcf6fdf0bd62791be5bb/packages/compiler/src/shadow_css.ts#L441),\n    but it was made to be able to use current realization of rtl and it can be rewritten back to\n    :host-context($theme) once we will be able to use multiple shadow selectors.\n  *\nBut when it's called in *.component.scss we can't be sure, that selector starts with :host/:host-context,\nbecause angular allows omitting pseudo-classes if we don't need to style :host component itself.\nWe can break such selectors, by just appending :host-context([dir=rtl]) to them.\n  ***\n    Possible solution\n    check if we in theme by some theme variables and if so append, otherwise nest like\n    @at-root :host-context([dir=rtl]) {\n      // add # to scss interpolation statement.\n      // it works in comments and we can't use it here\n      {&} {\n        @content;\n      }\n    }\n    What if :host specified? Can we add space in :host-context(...) :host?\n    Or maybe add :host selector anyway? If multiple :host selectors are allowed\n  ***\n\n\nProblems with the current approach.\n\n1. Direction can be applied only on document level, because mixin prepends theme class,\nwhich placed on the body.\n2. *.component.scss styles should be in :host selector. Otherwise angular will add host\nattribute to [dir=rtl] attribute as well.\n\n\nGeneral problems.\n\nLtr is default document direction, but for proper work of nb-ltr (means ltr only),\n[dir=ltr] should be specified at least somewhere. ':not([dir=rtl]' not applicable here,\nbecause it's satisfy any parent, that don't have [dir=rtl] attribute.\nPrevious approach was to use single rtl mixin and reset ltr properties to initial value.\nBut sometimes it's hard to find, what the previous value should be. And such mixin call looks too verbose.\n*/\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * This mixin generates keyfames.\n * Because of all keyframes can't be scoped,\n * we need to puts unique name in each btn-pulse call.\n */\n/*\n\nAccording to the specification (https://www.w3.org/TR/css-scoping-1/#host-selector)\n:host and :host-context are pseudo-classes. So we assume they could be combined,\nlike other pseudo-classes, even same ones.\nFor example: ':nth-of-type(2n):nth-of-type(even)'.\n\nIdeal solution would be to prepend any selector with :host-context([dir=rtl]).\nThen nebular components will behave as an html element and respond to [dir] attribute on any level,\nso direction could be overridden on any component level.\n\nImplementation code:\n\n@mixin nb-rtl() {\n  // add # to scss interpolation statement.\n  // it works in comments and we can't use it here\n  @at-root {selector-append(':host-context([dir=rtl])', &)} {\n    @content;\n  }\n}\n\nAnd when we call it somewhere:\n\n:host {\n  .some-class {\n    @include nb-rtl() {\n      ...\n    }\n  }\n}\n:host-context(...) {\n  .some-class {\n    @include nb-rtl() {\n      ...\n    }\n  }\n}\n\nResult will look like:\n\n:host-context([dir=rtl]):host .some-class {\n  ...\n}\n:host-context([dir=rtl]):host-context(...) .some-class {\n  ...\n}\n\n*\n  Side note:\n  :host-context():host selector are valid. https://lists.w3.org/Archives/Public/www-style/2015Feb/0305.html\n\n  :host-context([dir=rtl]):host-context(...) should match any permutation,\n  so order is not important.\n*\n\n\nCurrently, there're two problems with this approach:\n\nFirst, is that we can't combine :host, :host-context. Angular bugs #14349, #19199.\nFor the moment of writing, the only possible way is:\n:host {\n  :host-context(...) {\n    ...\n  }\n}\nIt doesn't work for us because mixin could be called somewhere deeper, like:\n:host {\n  p {\n    @include nb-rtl() { ... }\n  }\n}\nWe are not able to go up to :host level to place content passed to mixin.\n\nThe second problem is that we only can be sure that we appending :host-context([dir=rtl]) to another\n:host/:host-context pseudo-class when called in theme files (*.theme.scss).\n  *\n    Side note:\n    Currently, nb-install-component uses another approach where :host prepended with the theme name\n    (https://github.com/angular/angular/blob/5b96078624b0a4760f2dbcf6fdf0bd62791be5bb/packages/compiler/src/shadow_css.ts#L441),\n    but it was made to be able to use current realization of rtl and it can be rewritten back to\n    :host-context($theme) once we will be able to use multiple shadow selectors.\n  *\nBut when it's called in *.component.scss we can't be sure, that selector starts with :host/:host-context,\nbecause angular allows omitting pseudo-classes if we don't need to style :host component itself.\nWe can break such selectors, by just appending :host-context([dir=rtl]) to them.\n  ***\n    Possible solution\n    check if we in theme by some theme variables and if so append, otherwise nest like\n    @at-root :host-context([dir=rtl]) {\n      // add # to scss interpolation statement.\n      // it works in comments and we can't use it here\n      {&} {\n        @content;\n      }\n    }\n    What if :host specified? Can we add space in :host-context(...) :host?\n    Or maybe add :host selector anyway? If multiple :host selectors are allowed\n  ***\n\n\nProblems with the current approach.\n\n1. Direction can be applied only on document level, because mixin prepends theme class,\nwhich placed on the body.\n2. *.component.scss styles should be in :host selector. Otherwise angular will add host\nattribute to [dir=rtl] attribute as well.\n\n\nGeneral problems.\n\nLtr is default document direction, but for proper work of nb-ltr (means ltr only),\n[dir=ltr] should be specified at least somewhere. ':not([dir=rtl]' not applicable here,\nbecause it's satisfy any parent, that don't have [dir=rtl] attribute.\nPrevious approach was to use single rtl mixin and reset ltr properties to initial value.\nBut sometimes it's hard to find, what the previous value should be. And such mixin call looks too verbose.\n*/\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * This mixin generates keyfames.\n * Because of all keyframes can't be scoped,\n * we need to puts unique name in each btn-pulse call.\n */\n/*\n\nAccording to the specification (https://www.w3.org/TR/css-scoping-1/#host-selector)\n:host and :host-context are pseudo-classes. So we assume they could be combined,\nlike other pseudo-classes, even same ones.\nFor example: ':nth-of-type(2n):nth-of-type(even)'.\n\nIdeal solution would be to prepend any selector with :host-context([dir=rtl]).\nThen nebular components will behave as an html element and respond to [dir] attribute on any level,\nso direction could be overridden on any component level.\n\nImplementation code:\n\n@mixin nb-rtl() {\n  // add # to scss interpolation statement.\n  // it works in comments and we can't use it here\n  @at-root {selector-append(':host-context([dir=rtl])', &)} {\n    @content;\n  }\n}\n\nAnd when we call it somewhere:\n\n:host {\n  .some-class {\n    @include nb-rtl() {\n      ...\n    }\n  }\n}\n:host-context(...) {\n  .some-class {\n    @include nb-rtl() {\n      ...\n    }\n  }\n}\n\nResult will look like:\n\n:host-context([dir=rtl]):host .some-class {\n  ...\n}\n:host-context([dir=rtl]):host-context(...) .some-class {\n  ...\n}\n\n*\n  Side note:\n  :host-context():host selector are valid. https://lists.w3.org/Archives/Public/www-style/2015Feb/0305.html\n\n  :host-context([dir=rtl]):host-context(...) should match any permutation,\n  so order is not important.\n*\n\n\nCurrently, there're two problems with this approach:\n\nFirst, is that we can't combine :host, :host-context. Angular bugs #14349, #19199.\nFor the moment of writing, the only possible way is:\n:host {\n  :host-context(...) {\n    ...\n  }\n}\nIt doesn't work for us because mixin could be called somewhere deeper, like:\n:host {\n  p {\n    @include nb-rtl() { ... }\n  }\n}\nWe are not able to go up to :host level to place content passed to mixin.\n\nThe second problem is that we only can be sure that we appending :host-context([dir=rtl]) to another\n:host/:host-context pseudo-class when called in theme files (*.theme.scss).\n  *\n    Side note:\n    Currently, nb-install-component uses another approach where :host prepended with the theme name\n    (https://github.com/angular/angular/blob/5b96078624b0a4760f2dbcf6fdf0bd62791be5bb/packages/compiler/src/shadow_css.ts#L441),\n    but it was made to be able to use current realization of rtl and it can be rewritten back to\n    :host-context($theme) once we will be able to use multiple shadow selectors.\n  *\nBut when it's called in *.component.scss we can't be sure, that selector starts with :host/:host-context,\nbecause angular allows omitting pseudo-classes if we don't need to style :host component itself.\nWe can break such selectors, by just appending :host-context([dir=rtl]) to them.\n  ***\n    Possible solution\n    check if we in theme by some theme variables and if so append, otherwise nest like\n    @at-root :host-context([dir=rtl]) {\n      // add # to scss interpolation statement.\n      // it works in comments and we can't use it here\n      {&} {\n        @content;\n      }\n    }\n    What if :host specified? Can we add space in :host-context(...) :host?\n    Or maybe add :host selector anyway? If multiple :host selectors are allowed\n  ***\n\n\nProblems with the current approach.\n\n1. Direction can be applied only on document level, because mixin prepends theme class,\nwhich placed on the body.\n2. *.component.scss styles should be in :host selector. Otherwise angular will add host\nattribute to [dir=rtl] attribute as well.\n\n\nGeneral problems.\n\nLtr is default document direction, but for proper work of nb-ltr (means ltr only),\n[dir=ltr] should be specified at least somewhere. ':not([dir=rtl]' not applicable here,\nbecause it's satisfy any parent, that don't have [dir=rtl] attribute.\nPrevious approach was to use single rtl mixin and reset ltr properties to initial value.\nBut sometimes it's hard to find, what the previous value should be. And such mixin call looks too verbose.\n*/\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * This mixin generates keyfames.\n * Because of all keyframes can't be scoped,\n * we need to puts unique name in each btn-pulse call.\n */\n/*\n\nAccording to the specification (https://www.w3.org/TR/css-scoping-1/#host-selector)\n:host and :host-context are pseudo-classes. So we assume they could be combined,\nlike other pseudo-classes, even same ones.\nFor example: ':nth-of-type(2n):nth-of-type(even)'.\n\nIdeal solution would be to prepend any selector with :host-context([dir=rtl]).\nThen nebular components will behave as an html element and respond to [dir] attribute on any level,\nso direction could be overridden on any component level.\n\nImplementation code:\n\n@mixin nb-rtl() {\n  // add # to scss interpolation statement.\n  // it works in comments and we can't use it here\n  @at-root {selector-append(':host-context([dir=rtl])', &)} {\n    @content;\n  }\n}\n\nAnd when we call it somewhere:\n\n:host {\n  .some-class {\n    @include nb-rtl() {\n      ...\n    }\n  }\n}\n:host-context(...) {\n  .some-class {\n    @include nb-rtl() {\n      ...\n    }\n  }\n}\n\nResult will look like:\n\n:host-context([dir=rtl]):host .some-class {\n  ...\n}\n:host-context([dir=rtl]):host-context(...) .some-class {\n  ...\n}\n\n*\n  Side note:\n  :host-context():host selector are valid. https://lists.w3.org/Archives/Public/www-style/2015Feb/0305.html\n\n  :host-context([dir=rtl]):host-context(...) should match any permutation,\n  so order is not important.\n*\n\n\nCurrently, there're two problems with this approach:\n\nFirst, is that we can't combine :host, :host-context. Angular bugs #14349, #19199.\nFor the moment of writing, the only possible way is:\n:host {\n  :host-context(...) {\n    ...\n  }\n}\nIt doesn't work for us because mixin could be called somewhere deeper, like:\n:host {\n  p {\n    @include nb-rtl() { ... }\n  }\n}\nWe are not able to go up to :host level to place content passed to mixin.\n\nThe second problem is that we only can be sure that we appending :host-context([dir=rtl]) to another\n:host/:host-context pseudo-class when called in theme files (*.theme.scss).\n  *\n    Side note:\n    Currently, nb-install-component uses another approach where :host prepended with the theme name\n    (https://github.com/angular/angular/blob/5b96078624b0a4760f2dbcf6fdf0bd62791be5bb/packages/compiler/src/shadow_css.ts#L441),\n    but it was made to be able to use current realization of rtl and it can be rewritten back to\n    :host-context($theme) once we will be able to use multiple shadow selectors.\n  *\nBut when it's called in *.component.scss we can't be sure, that selector starts with :host/:host-context,\nbecause angular allows omitting pseudo-classes if we don't need to style :host component itself.\nWe can break such selectors, by just appending :host-context([dir=rtl]) to them.\n  ***\n    Possible solution\n    check if we in theme by some theme variables and if so append, otherwise nest like\n    @at-root :host-context([dir=rtl]) {\n      // add # to scss interpolation statement.\n      // it works in comments and we can't use it here\n      {&} {\n        @content;\n      }\n    }\n    What if :host specified? Can we add space in :host-context(...) :host?\n    Or maybe add :host selector anyway? If multiple :host selectors are allowed\n  ***\n\n\nProblems with the current approach.\n\n1. Direction can be applied only on document level, because mixin prepends theme class,\nwhich placed on the body.\n2. *.component.scss styles should be in :host selector. Otherwise angular will add host\nattribute to [dir=rtl] attribute as well.\n\n\nGeneral problems.\n\nLtr is default document direction, but for proper work of nb-ltr (means ltr only),\n[dir=ltr] should be specified at least somewhere. ':not([dir=rtl]' not applicable here,\nbecause it's satisfy any parent, that don't have [dir=rtl] attribute.\nPrevious approach was to use single rtl mixin and reset ltr properties to initial value.\nBut sometimes it's hard to find, what the previous value should be. And such mixin call looks too verbose.\n*/\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/*\n      :host can be prefixed\n      https://github.com/angular/angular/blob/8d0ee34939f14c07876d222c25b405ed458a34d3/packages/compiler/src/shadow_css.ts#L441\n\n      We have to use :host insted of :host-context($theme), to be able to prefix theme class\n      with something defined inside of @content, by prefixing &.\n      For example this scss code:\n        .nb-theme-default {\n          .some-selector & {\n            ...\n          }\n        }\n      Will result in next css:\n        .some-selector .nb-theme-default {\n          ...\n        }\n\n      It doesn't work with :host-context because angular splitting it in two selectors and removes\n      prefix in one of the selectors.\n    */\n.nb-theme-default :host button {\n  float: right; }\n.nb-theme-default :host /deep/ ng2-smart-table i {\n  color: #212529 !important; }\n.nb-theme-default :host /deep/ ng2-smart-table nav ul li a {\n  background-color: #212529 !important; }\n.nb-theme-default :host /deep/ ng2-smart-table .ng2-smart-title {\n  color: white !important; }\n.nb-theme-default :host nb-card {\n  border: 0px solid #d5dbe0; }\n.nb-theme-default :host .form-check-input {\n  position: absolute;\n  margin-top: -0.05rem;\n  margin-left: -1.5rem;\n  width: 20px;\n  height: 20px; }\n.nb-theme-default :host /deep/ ng2-smart-table i {\n  position: relative !important;\n  top: 0px !important; }\n.nb-theme-default :host /deep/ ng2-st-actions i {\n  color: white !important; }\n.nb-theme-default :host /deep/ ng2-smart-table select {\n  height: 2.6rem !important;\n  padding-top: 5px !important; }\n@media (min-width: 576px) {\n  .nb-theme-default :host /deep/ modal.modalss .modal-dialog {\n    max-width: 70% !important; }\n  .nb-theme-default :host /deep/ modal.modalss .modal-dialog .modal-body {\n    height: auto; }\n  .nb-theme-default :host /deep/ modal.modalss .modal-dialog .modal-footer {\n    display: none !important; } }\n.nb-theme-default :host /deep/ng2-smart-table table tbody tr:nth-child(odd) {\n  background: white !important; }\n.nb-theme-default :host /deep/ng2-smart-table .ng2-smart-row td ng2-smart-table-cell input-editor input {\n  border: #1d8ece solid !important; }\n.nb-theme-default :host /deep/ng2-smart-table .ng2-smart-row td ng2-smart-table-cell select-editor select {\n  border: #1d8ece solid !important; }\n.nb-theme-default :host /deep/ ng2-st-tbody-edit-delete {\n  display: -webkit-inline-box !important;\n  height: 0px !important;\n  float: left;\n  margin-left: 36px;\n  margin-top: -15px !important;\n  /* margin: auto; */ }\n.nb-theme-default :host /deep/ ng2-st-tbody-custom a.ng2-smart-action.ng2-smart-action-custom-custom {\n  width: 50px;\n  text-align: center;\n  font-size: 0.1em; }\n.nb-theme-default :host /deep/ ng2-st-tbody-custom a.ng2-smart-action.ng2-smart-action-custom-custom img {\n  width: 57% !important;\n  margin-left: 34px; }\n.nb-theme-default :host /deep/ ng2-st-tbody-custom a.ng2-smart-action.ng2-smart-action-custom-custom:hover {\n  color: #5dcfe3; }\n.nb-theme-default :host /deep/ ng2-st-tbody-edit-delete i {\n  position: relative !important;\n  top: -14px;\n  margin-left: 40px; }\n/*\n      :host can be prefixed\n      https://github.com/angular/angular/blob/8d0ee34939f14c07876d222c25b405ed458a34d3/packages/compiler/src/shadow_css.ts#L441\n\n      We have to use :host insted of :host-context($theme), to be able to prefix theme class\n      with something defined inside of @content, by prefixing &.\n      For example this scss code:\n        .nb-theme-default {\n          .some-selector & {\n            ...\n          }\n        }\n      Will result in next css:\n        .some-selector .nb-theme-default {\n          ...\n        }\n\n      It doesn't work with :host-context because angular splitting it in two selectors and removes\n      prefix in one of the selectors.\n    */\n.nb-theme-cosmic :host button {\n  float: right; }\n.nb-theme-cosmic :host /deep/ ng2-smart-table i {\n  color: #212529 !important; }\n.nb-theme-cosmic :host /deep/ ng2-smart-table nav ul li a {\n  background-color: #212529 !important; }\n.nb-theme-cosmic :host /deep/ ng2-smart-table .ng2-smart-title {\n  color: white !important; }\n.nb-theme-cosmic :host nb-card {\n  border: 0px solid #d5dbe0; }\n.nb-theme-cosmic :host .form-check-input {\n  position: absolute;\n  margin-top: -0.05rem;\n  margin-left: -1.5rem;\n  width: 20px;\n  height: 20px; }\n.nb-theme-cosmic :host /deep/ ng2-smart-table i {\n  position: relative !important;\n  top: 0px !important; }\n.nb-theme-cosmic :host /deep/ ng2-st-actions i {\n  color: white !important; }\n.nb-theme-cosmic :host /deep/ ng2-smart-table select {\n  height: 2.6rem !important;\n  padding-top: 5px !important; }\n@media (min-width: 576px) {\n  .nb-theme-cosmic :host /deep/ modal.modalss .modal-dialog {\n    max-width: 70% !important; }\n  .nb-theme-cosmic :host /deep/ modal.modalss .modal-dialog .modal-body {\n    height: auto; }\n  .nb-theme-cosmic :host /deep/ modal.modalss .modal-dialog .modal-footer {\n    display: none !important; } }\n.nb-theme-cosmic :host /deep/ng2-smart-table table tbody tr:nth-child(odd) {\n  background: white !important; }\n.nb-theme-cosmic :host /deep/ng2-smart-table .ng2-smart-row td ng2-smart-table-cell input-editor input {\n  border: #1d8ece solid !important; }\n.nb-theme-cosmic :host /deep/ng2-smart-table .ng2-smart-row td ng2-smart-table-cell select-editor select {\n  border: #1d8ece solid !important; }\n.nb-theme-cosmic :host /deep/ ng2-st-tbody-edit-delete {\n  display: -webkit-inline-box !important;\n  height: 0px !important;\n  float: left;\n  margin-left: 36px;\n  margin-top: -15px !important;\n  /* margin: auto; */ }\n.nb-theme-cosmic :host /deep/ ng2-st-tbody-custom a.ng2-smart-action.ng2-smart-action-custom-custom {\n  width: 50px;\n  text-align: center;\n  font-size: 0.1em; }\n.nb-theme-cosmic :host /deep/ ng2-st-tbody-custom a.ng2-smart-action.ng2-smart-action-custom-custom img {\n  width: 57% !important;\n  margin-left: 34px; }\n.nb-theme-cosmic :host /deep/ ng2-st-tbody-custom a.ng2-smart-action.ng2-smart-action-custom-custom:hover {\n  color: #5dcfe3; }\n.nb-theme-cosmic :host /deep/ ng2-st-tbody-edit-delete i {\n  position: relative !important;\n  top: -14px;\n  margin-left: 40px; }\n/*\n      :host can be prefixed\n      https://github.com/angular/angular/blob/8d0ee34939f14c07876d222c25b405ed458a34d3/packages/compiler/src/shadow_css.ts#L441\n\n      We have to use :host insted of :host-context($theme), to be able to prefix theme class\n      with something defined inside of @content, by prefixing &.\n      For example this scss code:\n        .nb-theme-default {\n          .some-selector & {\n            ...\n          }\n        }\n      Will result in next css:\n        .some-selector .nb-theme-default {\n          ...\n        }\n\n      It doesn't work with :host-context because angular splitting it in two selectors and removes\n      prefix in one of the selectors.\n    */\n.nb-theme-corporate :host button {\n  float: right; }\n.nb-theme-corporate :host /deep/ ng2-smart-table i {\n  color: #212529 !important; }\n.nb-theme-corporate :host /deep/ ng2-smart-table nav ul li a {\n  background-color: #212529 !important; }\n.nb-theme-corporate :host /deep/ ng2-smart-table .ng2-smart-title {\n  color: white !important; }\n.nb-theme-corporate :host nb-card {\n  border: 0px solid #d5dbe0; }\n.nb-theme-corporate :host .form-check-input {\n  position: absolute;\n  margin-top: -0.05rem;\n  margin-left: -1.5rem;\n  width: 20px;\n  height: 20px; }\n.nb-theme-corporate :host /deep/ ng2-smart-table i {\n  position: relative !important;\n  top: 0px !important; }\n.nb-theme-corporate :host /deep/ ng2-st-actions i {\n  color: white !important; }\n.nb-theme-corporate :host /deep/ ng2-smart-table select {\n  height: 2.6rem !important;\n  padding-top: 5px !important; }\n@media (min-width: 576px) {\n  .nb-theme-corporate :host /deep/ modal.modalss .modal-dialog {\n    max-width: 70% !important; }\n  .nb-theme-corporate :host /deep/ modal.modalss .modal-dialog .modal-body {\n    height: auto; }\n  .nb-theme-corporate :host /deep/ modal.modalss .modal-dialog .modal-footer {\n    display: none !important; } }\n.nb-theme-corporate :host /deep/ng2-smart-table table tbody tr:nth-child(odd) {\n  background: white !important; }\n.nb-theme-corporate :host /deep/ng2-smart-table .ng2-smart-row td ng2-smart-table-cell input-editor input {\n  border: #1d8ece solid !important; }\n.nb-theme-corporate :host /deep/ng2-smart-table .ng2-smart-row td ng2-smart-table-cell select-editor select {\n  border: #1d8ece solid !important; }\n.nb-theme-corporate :host /deep/ ng2-st-tbody-edit-delete {\n  display: -webkit-inline-box !important;\n  height: 0px !important;\n  float: left;\n  margin-left: 36px;\n  margin-top: -15px !important;\n  /* margin: auto; */ }\n.nb-theme-corporate :host /deep/ ng2-st-tbody-custom a.ng2-smart-action.ng2-smart-action-custom-custom {\n  width: 50px;\n  text-align: center;\n  font-size: 0.1em; }\n.nb-theme-corporate :host /deep/ ng2-st-tbody-custom a.ng2-smart-action.ng2-smart-action-custom-custom img {\n  width: 57% !important;\n  margin-left: 34px; }\n.nb-theme-corporate :host /deep/ ng2-st-tbody-custom a.ng2-smart-action.ng2-smart-action-custom-custom:hover {\n  color: #5dcfe3; }\n.nb-theme-corporate :host /deep/ ng2-st-tbody-edit-delete i {\n  position: relative !important;\n  top: -14px;\n  margin-left: 40px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3ByYWthc2gvVmlwaW4vaXJvbmJpcmQvMjBGRUIyMDIwL2lyb25iaXJkLWNsaWVudHMvbm9kZV9tb2R1bGVzL0BuZWJ1bGFyL3RoZW1lL3N0eWxlcy9fdGhlbWluZy5zY3NzIiwic3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9hbWMvbWFpbi1hbWMvdXBsb2FkLWFtYy1kb2MvdXBsb2FkLWFtYy1kb2MuY29tcG9uZW50LnNjc3MiLCIvaG9tZS9wcmFrYXNoL1ZpcGluL2lyb25iaXJkLzIwRkVCMjAyMC9pcm9uYmlyZC1jbGllbnRzL25vZGVfbW9kdWxlcy9AbmVidWxhci90aGVtZS9zdHlsZXMvY29yZS9fbWl4aW5zLnNjc3MiLCIvaG9tZS9wcmFrYXNoL1ZpcGluL2lyb25iaXJkLzIwRkVCMjAyMC9pcm9uYmlyZC1jbGllbnRzL25vZGVfbW9kdWxlcy9AbmVidWxhci90aGVtZS9zdHlsZXMvY29yZS9fZnVuY3Rpb25zLnNjc3MiLCIvaG9tZS9wcmFrYXNoL1ZpcGluL2lyb25iaXJkLzIwRkVCMjAyMC9pcm9uYmlyZC1jbGllbnRzL25vZGVfbW9kdWxlcy9AbmVidWxhci90aGVtZS9zdHlsZXMvdGhlbWVzL19kZWZhdWx0LnNjc3MiLCIvaG9tZS9wcmFrYXNoL1ZpcGluL2lyb25iaXJkLzIwRkVCMjAyMC9pcm9uYmlyZC1jbGllbnRzL25vZGVfbW9kdWxlcy9AbmVidWxhci90aGVtZS9zdHlsZXMvdGhlbWVzL19jb3NtaWMuc2NzcyIsIi9ob21lL3ByYWthc2gvVmlwaW4vaXJvbmJpcmQvMjBGRUIyMDIwL2lyb25iaXJkLWNsaWVudHMvbm9kZV9tb2R1bGVzL0BuZWJ1bGFyL3RoZW1lL3N0eWxlcy90aGVtZXMvX2NvcnBvcmF0ZS5zY3NzIiwiL2hvbWUvcHJha2FzaC9WaXBpbi9pcm9uYmlyZC8yMEZFQjIwMjAvaXJvbmJpcmQtY2xpZW50cy9ub2RlX21vZHVsZXMvQG5lYnVsYXIvdGhlbWUvc3R5bGVzL2dsb2JhbC9ib290c3RyYXAvX2JyZWFrcG9pbnRzLnNjc3MiLCIvaG9tZS9wcmFrYXNoL1ZpcGluL2lyb25iaXJkLzIwRkVCMjAyMC9pcm9uYmlyZC1jbGllbnRzL3NyYy9hcHAvcGFnZXMvdWktZmVhdHVyZXMvYW1jL21haW4tYW1jL3VwbG9hZC1hbWMtZG9jL3VwbG9hZC1hbWMtZG9jLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOzs7O0VDSUU7QURHRjs7RUNBRTtBQ1BGOzs7O0VEWUU7QUM4SkY7Ozs7RUR6SkU7QUNtTEY7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Q0QvREM7QUVySUQ7Ozs7RUYwSUU7QUcxSUY7Ozs7RUgrSUU7QUUvSUY7Ozs7RUZvSkU7QUNwSkY7Ozs7RUR5SkU7QUNpQkY7Ozs7RURaRTtBQ3NDRjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztDRDhFQztBSWxSRDs7OztFSnVSRTtBRXZSRjs7OztFRjRSRTtBQzVSRjs7OztFRGlTRTtBQ3ZIRjs7OztFRDRIRTtBQ2xHRjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztDRHNOQztBRzFaRDs7OztFSCtaRTtBRS9aRjs7OztFRm9hRTtBQ3BhRjs7OztFRHlhRTtBQy9QRjs7OztFRG9RRTtBQzFPRjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztDRDhWQztBS2xpQkQ7Ozs7RUx1aUJFO0FFdmlCRjs7OztFRjRpQkU7QUM1aUJGOzs7O0VEaWpCRTtBQ3ZZRjs7OztFRDRZRTtBQ2xYRjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztDRHNlQztBRzFxQkQ7Ozs7RUgrcUJFO0FFL3FCRjs7OztFRm9yQkU7QUNwckJGOzs7O0VEeXJCRTtBQy9nQkY7Ozs7RURvaEJFO0FDMWZGOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0NEOG1CQztBTWx6QkQ7Ozs7RU51ekJFO0FEcnNCRTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztLQ3l0QkM7QU9yMEJMO0VBQ0UsWUFBWSxFQUFBO0FBRWQ7RUFDSSx5QkFDQSxFQUFBO0FBRUE7RUFDRSxvQ0FDQSxFQUFBO0FBRUE7RUFDRSx1QkFDQSxFQUFBO0FBQ0E7RUFDRSx5QkFBeUIsRUFBQTtBQUczQjtFQUNFLGtCQUFrQjtFQUNsQixvQkFBb0I7RUFDcEIsb0JBQW9CO0VBQ3BCLFdBQVc7RUFDWCxZQUFZLEVBQUE7QUFFaEI7RUFDRSw2QkFBNkI7RUFDN0IsbUJBQW1CLEVBQUE7QUFFdkI7RUFDRSx1QkFBc0IsRUFBQTtBQUcxQjtFQUNFLHlCQUF3QjtFQUN4QiwyQkFBMkIsRUFBQTtBQUc3QjtFQUNFO0lBQ0kseUJBQXlCLEVBQUE7RUFHN0I7SUFDRSxZQUFhLEVBQUE7RUFFZjtJQUNDLHdCQUF3QixFQUFBLEVBQzFCO0FBUUM7RUFDRSw0QkFBNEIsRUFBQTtBQVNoQztFQUNFLGdDQUFnQyxFQUFBO0FBR2xDO0VBQ0UsZ0NBQWdDLEVBQUE7QUFFbEM7RUFDRSxzQ0FBc0M7RUFDdEMsc0JBQXNCO0VBQ3RCLFdBQVc7RUFDWCxpQkFBaUI7RUFDakIsNEJBQTRCO0VBQzVCLGtCQUFBLEVBQW1CO0FBR25CO0VBRUMsV0FBVztFQUNYLGtCQUFrQjtFQUNsQixnQkFBZ0IsRUFBQTtBQUVsQjtFQUNDLHFCQUFxQjtFQUNyQixpQkFBaUIsRUFBQTtBQUVuQjtFQUNHLGNBQWMsRUFBQTtBQUdkO0VBQ0MsNkJBQTZCO0VBQzdCLFVBQVc7RUFDWCxpQkFBaUIsRUFBQTtBUlNuQjs7Ozs7Ozs7Ozs7Ozs7Ozs7OztLQ3d6QkM7QU9wNkJMO0VBQ0UsWUFBWSxFQUFBO0FBRWQ7RUFDSSx5QkFDQSxFQUFBO0FBRUE7RUFDRSxvQ0FDQSxFQUFBO0FBRUE7RUFDRSx1QkFDQSxFQUFBO0FBQ0E7RUFDRSx5QkFBeUIsRUFBQTtBQUczQjtFQUNFLGtCQUFrQjtFQUNsQixvQkFBb0I7RUFDcEIsb0JBQW9CO0VBQ3BCLFdBQVc7RUFDWCxZQUFZLEVBQUE7QUFFaEI7RUFDRSw2QkFBNkI7RUFDN0IsbUJBQW1CLEVBQUE7QUFFdkI7RUFDRSx1QkFBc0IsRUFBQTtBQUcxQjtFQUNFLHlCQUF3QjtFQUN4QiwyQkFBMkIsRUFBQTtBQUc3QjtFQUNFO0lBQ0kseUJBQXlCLEVBQUE7RUFHN0I7SUFDRSxZQUFhLEVBQUE7RUFFZjtJQUNDLHdCQUF3QixFQUFBLEVBQzFCO0FBUUM7RUFDRSw0QkFBNEIsRUFBQTtBQVNoQztFQUNFLGdDQUFnQyxFQUFBO0FBR2xDO0VBQ0UsZ0NBQWdDLEVBQUE7QUFFbEM7RUFDRSxzQ0FBc0M7RUFDdEMsc0JBQXNCO0VBQ3RCLFdBQVc7RUFDWCxpQkFBaUI7RUFDakIsNEJBQTRCO0VBQzVCLGtCQUFBLEVBQW1CO0FBR25CO0VBRUMsV0FBVztFQUNYLGtCQUFrQjtFQUNsQixnQkFBZ0IsRUFBQTtBQUVsQjtFQUNDLHFCQUFxQjtFQUNyQixpQkFBaUIsRUFBQTtBQUVuQjtFQUNHLGNBQWMsRUFBQTtBQUdkO0VBQ0MsNkJBQTZCO0VBQzdCLFVBQVc7RUFDWCxpQkFBaUIsRUFBQTtBUlNuQjs7Ozs7Ozs7Ozs7Ozs7Ozs7OztLQ3U1QkM7QU9uZ0NMO0VBQ0UsWUFBWSxFQUFBO0FBRWQ7RUFDSSx5QkFDQSxFQUFBO0FBRUE7RUFDRSxvQ0FDQSxFQUFBO0FBRUE7RUFDRSx1QkFDQSxFQUFBO0FBQ0E7RUFDRSx5QkFBeUIsRUFBQTtBQUczQjtFQUNFLGtCQUFrQjtFQUNsQixvQkFBb0I7RUFDcEIsb0JBQW9CO0VBQ3BCLFdBQVc7RUFDWCxZQUFZLEVBQUE7QUFFaEI7RUFDRSw2QkFBNkI7RUFDN0IsbUJBQW1CLEVBQUE7QUFFdkI7RUFDRSx1QkFBc0IsRUFBQTtBQUcxQjtFQUNFLHlCQUF3QjtFQUN4QiwyQkFBMkIsRUFBQTtBQUc3QjtFQUNFO0lBQ0kseUJBQXlCLEVBQUE7RUFHN0I7SUFDRSxZQUFhLEVBQUE7RUFFZjtJQUNDLHdCQUF3QixFQUFBLEVBQzFCO0FBUUM7RUFDRSw0QkFBNEIsRUFBQTtBQVNoQztFQUNFLGdDQUFnQyxFQUFBO0FBR2xDO0VBQ0UsZ0NBQWdDLEVBQUE7QUFFbEM7RUFDRSxzQ0FBc0M7RUFDdEMsc0JBQXNCO0VBQ3RCLFdBQVc7RUFDWCxpQkFBaUI7RUFDakIsNEJBQTRCO0VBQzVCLGtCQUFBLEVBQW1CO0FBR25CO0VBRUMsV0FBVztFQUNYLGtCQUFrQjtFQUNsQixnQkFBZ0IsRUFBQTtBQUVsQjtFQUNDLHFCQUFxQjtFQUNyQixpQkFBaUIsRUFBQTtBQUVuQjtFQUNHLGNBQWMsRUFBQTtBQUdkO0VBQ0MsNkJBQTZCO0VBQzdCLFVBQVc7RUFDWCxpQkFBaUIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VpLWZlYXR1cmVzL2FtYy9tYWluLWFtYy91cGxvYWQtYW1jLWRvYy91cGxvYWQtYW1jLWRvYy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQGxpY2Vuc2VcbiAqIENvcHlyaWdodCBBa3Zlby4gQWxsIFJpZ2h0cyBSZXNlcnZlZC5cbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgTGljZW5zZS4gU2VlIExpY2Vuc2UudHh0IGluIHRoZSBwcm9qZWN0IHJvb3QgZm9yIGxpY2Vuc2UgaW5mb3JtYXRpb24uXG4gKi9cblxuXG4vKipcbiAqIFRoaXMgaXMgYSBzdGFydGluZyBwb2ludCB3aGVyZSB3ZSBkZWNsYXJlIHRoZSBtYXBzIG9mIHRoZW1lcyBhbmQgZ2xvYmFsbHkgYXZhaWxhYmxlIGZ1bmN0aW9ucy9taXhpbnNcbiAqL1xuXG5AaW1wb3J0ICdjb3JlL21peGlucyc7XG5AaW1wb3J0ICdjb3JlL2Z1bmN0aW9ucyc7XG5cbiRuYi1lbmFibGVkLXRoZW1lczogKCkgIWdsb2JhbDtcbiRuYi1lbmFibGUtY3NzLXZhcmlhYmxlczogZmFsc2UgIWdsb2JhbDtcblxuJG5iLXRoZW1lczogKCkgIWdsb2JhbDtcbiRuYi10aGVtZXMtbm9uLXByb2Nlc3NlZDogKCkgIWdsb2JhbDtcbiRuYi10aGVtZXMtZXhwb3J0OiAoKSAhZ2xvYmFsO1xuXG5AZnVuY3Rpb24gbmItdGhlbWUoJGtleSkge1xuICBAcmV0dXJuIG1hcC1nZXQoJHRoZW1lLCAka2V5KTtcbn1cblxuQGZ1bmN0aW9uIG5iLWdldC12YWx1ZSgkdGhlbWUsICRrZXksICR2YWx1ZSkge1xuICBAaWYgKHR5cGUtb2YoJHZhbHVlKSA9PSAnc3RyaW5nJykge1xuICAgICR0bXA6IG1hcC1nZXQoJHRoZW1lLCAkdmFsdWUpO1xuXG4gICAgQGlmICgkdG1wICE9IG51bGwpIHtcbiAgICAgIEByZXR1cm4gbmItZ2V0LXZhbHVlKCR0aGVtZSwgJHZhbHVlLCAkdG1wKTtcbiAgICB9XG4gIH1cblxuICBAcmV0dXJuIG1hcC1nZXQoJHRoZW1lLCAka2V5KTtcbn1cblxuQGZ1bmN0aW9uIGNvbnZlcnQtdG8tY3NzLXZhcmlhYmxlcygkdmFyaWFibGVzKSB7XG4gICRyZXN1bHQ6ICgpO1xuICBAZWFjaCAkdmFyLCAkdmFsdWUgaW4gJHZhcmlhYmxlcyB7XG4gICAgJHJlc3VsdDogbWFwLXNldCgkcmVzdWx0LCAkdmFyLCAnLS12YXIoI3skdmFyfSknKTtcbiAgfVxuXG4gIEBkZWJ1ZyAkcmVzdWx0O1xuICBAcmV0dXJuICRyZXN1bHQ7XG59XG5cbkBmdW5jdGlvbiBzZXQtZ2xvYmFsLXRoZW1lLXZhcnMoJHRoZW1lLCAkdGhlbWUtbmFtZSkge1xuICAkdGhlbWU6ICR0aGVtZSAhZ2xvYmFsO1xuICAkdGhlbWUtbmFtZTogJHRoZW1lLW5hbWUgIWdsb2JhbDtcbiAgQGlmICgkbmItZW5hYmxlLWNzcy12YXJpYWJsZXMpIHtcbiAgICAkdGhlbWU6IGNvbnZlcnQtdG8tY3NzLXZhcmlhYmxlcygkdGhlbWUpICFnbG9iYWw7XG4gIH1cbiAgQHJldHVybiAkdGhlbWU7XG59XG5cbkBmdW5jdGlvbiBuYi1yZWdpc3Rlci10aGVtZSgkdGhlbWUsICRuYW1lLCAkZGVmYXVsdDogbnVsbCkge1xuXG4gICR0aGVtZS1kYXRhOiAoKTtcblxuXG4gIEBpZiAoJGRlZmF1bHQgIT0gbnVsbCkge1xuXG4gICAgJHRoZW1lOiBtYXAtbWVyZ2UobWFwLWdldCgkbmItdGhlbWVzLW5vbi1wcm9jZXNzZWQsICRkZWZhdWx0KSwgJHRoZW1lKTtcbiAgICAkbmItdGhlbWVzLW5vbi1wcm9jZXNzZWQ6IG1hcC1zZXQoJG5iLXRoZW1lcy1ub24tcHJvY2Vzc2VkLCAkbmFtZSwgJHRoZW1lKSAhZ2xvYmFsO1xuXG4gICAgJHRoZW1lLWRhdGE6IG1hcC1zZXQoJHRoZW1lLWRhdGEsIGRhdGEsICR0aGVtZSk7XG4gICAgJG5iLXRoZW1lcy1leHBvcnQ6IG1hcC1zZXQoJG5iLXRoZW1lcy1leHBvcnQsICRuYW1lLCBtYXAtc2V0KCR0aGVtZS1kYXRhLCBwYXJlbnQsICRkZWZhdWx0KSkgIWdsb2JhbDtcblxuICB9IEBlbHNlIHtcbiAgICAkbmItdGhlbWVzLW5vbi1wcm9jZXNzZWQ6IG1hcC1zZXQoJG5iLXRoZW1lcy1ub24tcHJvY2Vzc2VkLCAkbmFtZSwgJHRoZW1lKSAhZ2xvYmFsO1xuXG4gICAgJHRoZW1lLWRhdGE6IG1hcC1zZXQoJHRoZW1lLWRhdGEsIGRhdGEsICR0aGVtZSk7XG4gICAgJG5iLXRoZW1lcy1leHBvcnQ6IG1hcC1zZXQoJG5iLXRoZW1lcy1leHBvcnQsICRuYW1lLCBtYXAtc2V0KCR0aGVtZS1kYXRhLCBwYXJlbnQsIG51bGwpKSAhZ2xvYmFsO1xuICB9XG5cbiAgJHRoZW1lLXBhcnNlZDogKCk7XG4gIEBlYWNoICRrZXksICR2YWx1ZSBpbiAkdGhlbWUge1xuICAgICR0aGVtZS1wYXJzZWQ6IG1hcC1zZXQoJHRoZW1lLXBhcnNlZCwgJGtleSwgbmItZ2V0LXZhbHVlKCR0aGVtZSwgJGtleSwgJHZhbHVlKSk7XG4gIH1cblxuICAvLyBlbmFibGUgcmlnaHQgYXdheSB3aGVuIGluc3RhbGxlZFxuICAkdGhlbWUtcGFyc2VkOiBzZXQtZ2xvYmFsLXRoZW1lLXZhcnMoJHRoZW1lLXBhcnNlZCwgJG5hbWUpO1xuICBAcmV0dXJuIG1hcC1zZXQoJG5iLXRoZW1lcywgJG5hbWUsICR0aGVtZS1wYXJzZWQpO1xufVxuXG5AZnVuY3Rpb24gZ2V0LWVuYWJsZWQtdGhlbWVzKCkge1xuICAkdGhlbWVzLXRvLWluc3RhbGw6ICgpO1xuXG4gIEBpZiAobGVuZ3RoKCRuYi1lbmFibGVkLXRoZW1lcykgPiAwKSB7XG4gICAgQGVhY2ggJHRoZW1lLW5hbWUgaW4gJG5iLWVuYWJsZWQtdGhlbWVzIHtcbiAgICAgICR0aGVtZXMtdG8taW5zdGFsbDogbWFwLXNldCgkdGhlbWVzLXRvLWluc3RhbGwsICR0aGVtZS1uYW1lLCBtYXAtZ2V0KCRuYi10aGVtZXMsICR0aGVtZS1uYW1lKSk7XG4gICAgfVxuICB9IEBlbHNlIHtcbiAgICAkdGhlbWVzLXRvLWluc3RhbGw6ICRuYi10aGVtZXM7XG4gIH1cblxuICBAcmV0dXJuICR0aGVtZXMtdG8taW5zdGFsbDtcbn1cblxuQG1peGluIGluc3RhbGwtY3NzLXZhcmlhYmxlcygkdGhlbWUtbmFtZSwgJHZhcmlhYmxlcykge1xuICAubmItdGhlbWUtI3skdGhlbWUtbmFtZX0ge1xuICAgIEBlYWNoICR2YXIsICR2YWx1ZSBpbiAkdmFyaWFibGVzIHtcbiAgICAgIC0tI3skdmFyfTogJHZhbHVlO1xuICAgIH1cbiAgfVxufVxuXG4vLyBUT0RPOiB3ZSBoaWRlIDpob3N0IGluc2lkZSBvZiBpdCB3aGljaCBpcyBub3Qgb2J2aW91c1xuQG1peGluIG5iLWluc3RhbGwtY29tcG9uZW50KCkge1xuXG4gICR0aGVtZXMtdG8taW5zdGFsbDogZ2V0LWVuYWJsZWQtdGhlbWVzKCk7XG5cbiAgQGVhY2ggJHRoZW1lLW5hbWUsICR0aGVtZSBpbiAkdGhlbWVzLXRvLWluc3RhbGwge1xuICAgIC8qXG4gICAgICA6aG9zdCBjYW4gYmUgcHJlZml4ZWRcbiAgICAgIGh0dHBzOi8vZ2l0aHViLmNvbS9hbmd1bGFyL2FuZ3VsYXIvYmxvYi84ZDBlZTM0OTM5ZjE0YzA3ODc2ZDIyMmMyNWI0MDVlZDQ1OGEzNGQzL3BhY2thZ2VzL2NvbXBpbGVyL3NyYy9zaGFkb3dfY3NzLnRzI0w0NDFcblxuICAgICAgV2UgaGF2ZSB0byB1c2UgOmhvc3QgaW5zdGVkIG9mIDpob3N0LWNvbnRleHQoJHRoZW1lKSwgdG8gYmUgYWJsZSB0byBwcmVmaXggdGhlbWUgY2xhc3NcbiAgICAgIHdpdGggc29tZXRoaW5nIGRlZmluZWQgaW5zaWRlIG9mIEBjb250ZW50LCBieSBwcmVmaXhpbmcgJi5cbiAgICAgIEZvciBleGFtcGxlIHRoaXMgc2NzcyBjb2RlOlxuICAgICAgICAubmItdGhlbWUtZGVmYXVsdCB7XG4gICAgICAgICAgLnNvbWUtc2VsZWN0b3IgJiB7XG4gICAgICAgICAgICAuLi5cbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIFdpbGwgcmVzdWx0IGluIG5leHQgY3NzOlxuICAgICAgICAuc29tZS1zZWxlY3RvciAubmItdGhlbWUtZGVmYXVsdCB7XG4gICAgICAgICAgLi4uXG4gICAgICAgIH1cblxuICAgICAgSXQgZG9lc24ndCB3b3JrIHdpdGggOmhvc3QtY29udGV4dCBiZWNhdXNlIGFuZ3VsYXIgc3BsaXR0aW5nIGl0IGluIHR3byBzZWxlY3RvcnMgYW5kIHJlbW92ZXNcbiAgICAgIHByZWZpeCBpbiBvbmUgb2YgdGhlIHNlbGVjdG9ycy5cbiAgICAqL1xuICAgIC5uYi10aGVtZS0jeyR0aGVtZS1uYW1lfSA6aG9zdCB7XG4gICAgICAkdGhlbWU6IHNldC1nbG9iYWwtdGhlbWUtdmFycygkdGhlbWUsICR0aGVtZS1uYW1lKTtcbiAgICAgIEBjb250ZW50O1xuICAgIH1cbiAgfVxufVxuXG5AbWl4aW4gbmItZm9yLXRoZW1lKCRuYW1lKSB7XG4gIEBpZiAoJHRoZW1lLW5hbWUgPT0gJG5hbWUpIHtcbiAgICBAY29udGVudDtcbiAgfVxufVxuXG5AbWl4aW4gbmItZXhjZXB0LXRoZW1lKCRuYW1lKSB7XG4gIEBpZiAoJHRoZW1lLW5hbWUgIT0gJG5hbWUpIHtcbiAgICBAY29udGVudDtcbiAgfVxufVxuXG4vLyBUT0RPOiBhbm90aGVyIG1peGluZyBmb3IgdGhlIGFsbW9zdCBzYW1lIHRoaW5nXG5AbWl4aW4gbmItaW5zdGFsbC1yb290LWNvbXBvbmVudCgpIHtcbiAgQHdhcm4gJ2BuYi1pbnN0YWxsLXJvb3QtY29tcG9uZW50YCBpcyBkZXByaWNhdGVkLCByZXBsYWNlIHdpdGggYG5iLWluc3RhbGwtY29tcG9uZW50YCwgYXMgYGJvZHlgIGlzIHJvb3QgZWxlbWVudCBub3cnO1xuXG4gIEBpbmNsdWRlIG5iLWluc3RhbGwtY29tcG9uZW50KCkge1xuICAgIEBjb250ZW50O1xuICB9XG59XG5cbkBtaXhpbiBuYi1pbnN0YWxsLWdsb2JhbCgpIHtcbiAgJHRoZW1lcy10by1pbnN0YWxsOiBnZXQtZW5hYmxlZC10aGVtZXMoKTtcblxuICBAZWFjaCAkdGhlbWUtbmFtZSwgJHRoZW1lIGluICR0aGVtZXMtdG8taW5zdGFsbCB7XG4gICAgLm5iLXRoZW1lLSN7JHRoZW1lLW5hbWV9IHtcbiAgICAgICR0aGVtZTogc2V0LWdsb2JhbC10aGVtZS12YXJzKCR0aGVtZSwgJHRoZW1lLW5hbWUpO1xuICAgICAgQGNvbnRlbnQ7XG4gICAgfVxuICB9XG59XG4iLCIvKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG4vKipcbiAqIFRoaXMgaXMgYSBzdGFydGluZyBwb2ludCB3aGVyZSB3ZSBkZWNsYXJlIHRoZSBtYXBzIG9mIHRoZW1lcyBhbmQgZ2xvYmFsbHkgYXZhaWxhYmxlIGZ1bmN0aW9ucy9taXhpbnNcbiAqL1xuLyoqXG4gKiBAbGljZW5zZVxuICogQ29weXJpZ2h0IEFrdmVvLiBBbGwgUmlnaHRzIFJlc2VydmVkLlxuICogTGljZW5zZWQgdW5kZXIgdGhlIE1JVCBMaWNlbnNlLiBTZWUgTGljZW5zZS50eHQgaW4gdGhlIHByb2plY3Qgcm9vdCBmb3IgbGljZW5zZSBpbmZvcm1hdGlvbi5cbiAqL1xuLyoqXG4gKiBUaGlzIG1peGluIGdlbmVyYXRlcyBrZXlmYW1lcy5cbiAqIEJlY2F1c2Ugb2YgYWxsIGtleWZyYW1lcyBjYW4ndCBiZSBzY29wZWQsXG4gKiB3ZSBuZWVkIHRvIHB1dHMgdW5pcXVlIG5hbWUgaW4gZWFjaCBidG4tcHVsc2UgY2FsbC5cbiAqL1xuLypcblxuQWNjb3JkaW5nIHRvIHRoZSBzcGVjaWZpY2F0aW9uIChodHRwczovL3d3dy53My5vcmcvVFIvY3NzLXNjb3BpbmctMS8jaG9zdC1zZWxlY3Rvcilcbjpob3N0IGFuZCA6aG9zdC1jb250ZXh0IGFyZSBwc2V1ZG8tY2xhc3Nlcy4gU28gd2UgYXNzdW1lIHRoZXkgY291bGQgYmUgY29tYmluZWQsXG5saWtlIG90aGVyIHBzZXVkby1jbGFzc2VzLCBldmVuIHNhbWUgb25lcy5cbkZvciBleGFtcGxlOiAnOm50aC1vZi10eXBlKDJuKTpudGgtb2YtdHlwZShldmVuKScuXG5cbklkZWFsIHNvbHV0aW9uIHdvdWxkIGJlIHRvIHByZXBlbmQgYW55IHNlbGVjdG9yIHdpdGggOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pLlxuVGhlbiBuZWJ1bGFyIGNvbXBvbmVudHMgd2lsbCBiZWhhdmUgYXMgYW4gaHRtbCBlbGVtZW50IGFuZCByZXNwb25kIHRvIFtkaXJdIGF0dHJpYnV0ZSBvbiBhbnkgbGV2ZWwsXG5zbyBkaXJlY3Rpb24gY291bGQgYmUgb3ZlcnJpZGRlbiBvbiBhbnkgY29tcG9uZW50IGxldmVsLlxuXG5JbXBsZW1lbnRhdGlvbiBjb2RlOlxuXG5AbWl4aW4gbmItcnRsKCkge1xuICAvLyBhZGQgIyB0byBzY3NzIGludGVycG9sYXRpb24gc3RhdGVtZW50LlxuICAvLyBpdCB3b3JrcyBpbiBjb21tZW50cyBhbmQgd2UgY2FuJ3QgdXNlIGl0IGhlcmVcbiAgQGF0LXJvb3Qge3NlbGVjdG9yLWFwcGVuZCgnOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pJywgJil9IHtcbiAgICBAY29udGVudDtcbiAgfVxufVxuXG5BbmQgd2hlbiB3ZSBjYWxsIGl0IHNvbWV3aGVyZTpcblxuOmhvc3Qge1xuICAuc29tZS1jbGFzcyB7XG4gICAgQGluY2x1ZGUgbmItcnRsKCkge1xuICAgICAgLi4uXG4gICAgfVxuICB9XG59XG46aG9zdC1jb250ZXh0KC4uLikge1xuICAuc29tZS1jbGFzcyB7XG4gICAgQGluY2x1ZGUgbmItcnRsKCkge1xuICAgICAgLi4uXG4gICAgfVxuICB9XG59XG5cblJlc3VsdCB3aWxsIGxvb2sgbGlrZTpcblxuOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pOmhvc3QgLnNvbWUtY2xhc3Mge1xuICAuLi5cbn1cbjpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKTpob3N0LWNvbnRleHQoLi4uKSAuc29tZS1jbGFzcyB7XG4gIC4uLlxufVxuXG4qXG4gIFNpZGUgbm90ZTpcbiAgOmhvc3QtY29udGV4dCgpOmhvc3Qgc2VsZWN0b3IgYXJlIHZhbGlkLiBodHRwczovL2xpc3RzLnczLm9yZy9BcmNoaXZlcy9QdWJsaWMvd3d3LXN0eWxlLzIwMTVGZWIvMDMwNS5odG1sXG5cbiAgOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pOmhvc3QtY29udGV4dCguLi4pIHNob3VsZCBtYXRjaCBhbnkgcGVybXV0YXRpb24sXG4gIHNvIG9yZGVyIGlzIG5vdCBpbXBvcnRhbnQuXG4qXG5cblxuQ3VycmVudGx5LCB0aGVyZSdyZSB0d28gcHJvYmxlbXMgd2l0aCB0aGlzIGFwcHJvYWNoOlxuXG5GaXJzdCwgaXMgdGhhdCB3ZSBjYW4ndCBjb21iaW5lIDpob3N0LCA6aG9zdC1jb250ZXh0LiBBbmd1bGFyIGJ1Z3MgIzE0MzQ5LCAjMTkxOTkuXG5Gb3IgdGhlIG1vbWVudCBvZiB3cml0aW5nLCB0aGUgb25seSBwb3NzaWJsZSB3YXkgaXM6XG46aG9zdCB7XG4gIDpob3N0LWNvbnRleHQoLi4uKSB7XG4gICAgLi4uXG4gIH1cbn1cbkl0IGRvZXNuJ3Qgd29yayBmb3IgdXMgYmVjYXVzZSBtaXhpbiBjb3VsZCBiZSBjYWxsZWQgc29tZXdoZXJlIGRlZXBlciwgbGlrZTpcbjpob3N0IHtcbiAgcCB7XG4gICAgQGluY2x1ZGUgbmItcnRsKCkgeyAuLi4gfVxuICB9XG59XG5XZSBhcmUgbm90IGFibGUgdG8gZ28gdXAgdG8gOmhvc3QgbGV2ZWwgdG8gcGxhY2UgY29udGVudCBwYXNzZWQgdG8gbWl4aW4uXG5cblRoZSBzZWNvbmQgcHJvYmxlbSBpcyB0aGF0IHdlIG9ubHkgY2FuIGJlIHN1cmUgdGhhdCB3ZSBhcHBlbmRpbmcgOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pIHRvIGFub3RoZXJcbjpob3N0Lzpob3N0LWNvbnRleHQgcHNldWRvLWNsYXNzIHdoZW4gY2FsbGVkIGluIHRoZW1lIGZpbGVzICgqLnRoZW1lLnNjc3MpLlxuICAqXG4gICAgU2lkZSBub3RlOlxuICAgIEN1cnJlbnRseSwgbmItaW5zdGFsbC1jb21wb25lbnQgdXNlcyBhbm90aGVyIGFwcHJvYWNoIHdoZXJlIDpob3N0IHByZXBlbmRlZCB3aXRoIHRoZSB0aGVtZSBuYW1lXG4gICAgKGh0dHBzOi8vZ2l0aHViLmNvbS9hbmd1bGFyL2FuZ3VsYXIvYmxvYi81Yjk2MDc4NjI0YjBhNDc2MGYyZGJjZjZmZGYwYmQ2Mjc5MWJlNWJiL3BhY2thZ2VzL2NvbXBpbGVyL3NyYy9zaGFkb3dfY3NzLnRzI0w0NDEpLFxuICAgIGJ1dCBpdCB3YXMgbWFkZSB0byBiZSBhYmxlIHRvIHVzZSBjdXJyZW50IHJlYWxpemF0aW9uIG9mIHJ0bCBhbmQgaXQgY2FuIGJlIHJld3JpdHRlbiBiYWNrIHRvXG4gICAgOmhvc3QtY29udGV4dCgkdGhlbWUpIG9uY2Ugd2Ugd2lsbCBiZSBhYmxlIHRvIHVzZSBtdWx0aXBsZSBzaGFkb3cgc2VsZWN0b3JzLlxuICAqXG5CdXQgd2hlbiBpdCdzIGNhbGxlZCBpbiAqLmNvbXBvbmVudC5zY3NzIHdlIGNhbid0IGJlIHN1cmUsIHRoYXQgc2VsZWN0b3Igc3RhcnRzIHdpdGggOmhvc3QvOmhvc3QtY29udGV4dCxcbmJlY2F1c2UgYW5ndWxhciBhbGxvd3Mgb21pdHRpbmcgcHNldWRvLWNsYXNzZXMgaWYgd2UgZG9uJ3QgbmVlZCB0byBzdHlsZSA6aG9zdCBjb21wb25lbnQgaXRzZWxmLlxuV2UgY2FuIGJyZWFrIHN1Y2ggc2VsZWN0b3JzLCBieSBqdXN0IGFwcGVuZGluZyA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkgdG8gdGhlbS5cbiAgKioqXG4gICAgUG9zc2libGUgc29sdXRpb25cbiAgICBjaGVjayBpZiB3ZSBpbiB0aGVtZSBieSBzb21lIHRoZW1lIHZhcmlhYmxlcyBhbmQgaWYgc28gYXBwZW5kLCBvdGhlcndpc2UgbmVzdCBsaWtlXG4gICAgQGF0LXJvb3QgOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pIHtcbiAgICAgIC8vIGFkZCAjIHRvIHNjc3MgaW50ZXJwb2xhdGlvbiBzdGF0ZW1lbnQuXG4gICAgICAvLyBpdCB3b3JrcyBpbiBjb21tZW50cyBhbmQgd2UgY2FuJ3QgdXNlIGl0IGhlcmVcbiAgICAgIHsmfSB7XG4gICAgICAgIEBjb250ZW50O1xuICAgICAgfVxuICAgIH1cbiAgICBXaGF0IGlmIDpob3N0IHNwZWNpZmllZD8gQ2FuIHdlIGFkZCBzcGFjZSBpbiA6aG9zdC1jb250ZXh0KC4uLikgOmhvc3Q/XG4gICAgT3IgbWF5YmUgYWRkIDpob3N0IHNlbGVjdG9yIGFueXdheT8gSWYgbXVsdGlwbGUgOmhvc3Qgc2VsZWN0b3JzIGFyZSBhbGxvd2VkXG4gICoqKlxuXG5cblByb2JsZW1zIHdpdGggdGhlIGN1cnJlbnQgYXBwcm9hY2guXG5cbjEuIERpcmVjdGlvbiBjYW4gYmUgYXBwbGllZCBvbmx5IG9uIGRvY3VtZW50IGxldmVsLCBiZWNhdXNlIG1peGluIHByZXBlbmRzIHRoZW1lIGNsYXNzLFxud2hpY2ggcGxhY2VkIG9uIHRoZSBib2R5LlxuMi4gKi5jb21wb25lbnQuc2NzcyBzdHlsZXMgc2hvdWxkIGJlIGluIDpob3N0IHNlbGVjdG9yLiBPdGhlcndpc2UgYW5ndWxhciB3aWxsIGFkZCBob3N0XG5hdHRyaWJ1dGUgdG8gW2Rpcj1ydGxdIGF0dHJpYnV0ZSBhcyB3ZWxsLlxuXG5cbkdlbmVyYWwgcHJvYmxlbXMuXG5cbkx0ciBpcyBkZWZhdWx0IGRvY3VtZW50IGRpcmVjdGlvbiwgYnV0IGZvciBwcm9wZXIgd29yayBvZiBuYi1sdHIgKG1lYW5zIGx0ciBvbmx5KSxcbltkaXI9bHRyXSBzaG91bGQgYmUgc3BlY2lmaWVkIGF0IGxlYXN0IHNvbWV3aGVyZS4gJzpub3QoW2Rpcj1ydGxdJyBub3QgYXBwbGljYWJsZSBoZXJlLFxuYmVjYXVzZSBpdCdzIHNhdGlzZnkgYW55IHBhcmVudCwgdGhhdCBkb24ndCBoYXZlIFtkaXI9cnRsXSBhdHRyaWJ1dGUuXG5QcmV2aW91cyBhcHByb2FjaCB3YXMgdG8gdXNlIHNpbmdsZSBydGwgbWl4aW4gYW5kIHJlc2V0IGx0ciBwcm9wZXJ0aWVzIHRvIGluaXRpYWwgdmFsdWUuXG5CdXQgc29tZXRpbWVzIGl0J3MgaGFyZCB0byBmaW5kLCB3aGF0IHRoZSBwcmV2aW91cyB2YWx1ZSBzaG91bGQgYmUuIEFuZCBzdWNoIG1peGluIGNhbGwgbG9va3MgdG9vIHZlcmJvc2UuXG4qL1xuLyoqXG4gKiBAbGljZW5zZVxuICogQ29weXJpZ2h0IEFrdmVvLiBBbGwgUmlnaHRzIFJlc2VydmVkLlxuICogTGljZW5zZWQgdW5kZXIgdGhlIE1JVCBMaWNlbnNlLiBTZWUgTGljZW5zZS50eHQgaW4gdGhlIHByb2plY3Qgcm9vdCBmb3IgbGljZW5zZSBpbmZvcm1hdGlvbi5cbiAqL1xuLyoqXG4gKiBAbGljZW5zZVxuICogQ29weXJpZ2h0IEFrdmVvLiBBbGwgUmlnaHRzIFJlc2VydmVkLlxuICogTGljZW5zZWQgdW5kZXIgdGhlIE1JVCBMaWNlbnNlLiBTZWUgTGljZW5zZS50eHQgaW4gdGhlIHByb2plY3Qgcm9vdCBmb3IgbGljZW5zZSBpbmZvcm1hdGlvbi5cbiAqL1xuLyoqXG4gKiBAbGljZW5zZVxuICogQ29weXJpZ2h0IEFrdmVvLiBBbGwgUmlnaHRzIFJlc2VydmVkLlxuICogTGljZW5zZWQgdW5kZXIgdGhlIE1JVCBMaWNlbnNlLiBTZWUgTGljZW5zZS50eHQgaW4gdGhlIHByb2plY3Qgcm9vdCBmb3IgbGljZW5zZSBpbmZvcm1hdGlvbi5cbiAqL1xuLyoqXG4gKiBAbGljZW5zZVxuICogQ29weXJpZ2h0IEFrdmVvLiBBbGwgUmlnaHRzIFJlc2VydmVkLlxuICogTGljZW5zZWQgdW5kZXIgdGhlIE1JVCBMaWNlbnNlLiBTZWUgTGljZW5zZS50eHQgaW4gdGhlIHByb2plY3Qgcm9vdCBmb3IgbGljZW5zZSBpbmZvcm1hdGlvbi5cbiAqL1xuLyoqXG4gKiBUaGlzIG1peGluIGdlbmVyYXRlcyBrZXlmYW1lcy5cbiAqIEJlY2F1c2Ugb2YgYWxsIGtleWZyYW1lcyBjYW4ndCBiZSBzY29wZWQsXG4gKiB3ZSBuZWVkIHRvIHB1dHMgdW5pcXVlIG5hbWUgaW4gZWFjaCBidG4tcHVsc2UgY2FsbC5cbiAqL1xuLypcblxuQWNjb3JkaW5nIHRvIHRoZSBzcGVjaWZpY2F0aW9uIChodHRwczovL3d3dy53My5vcmcvVFIvY3NzLXNjb3BpbmctMS8jaG9zdC1zZWxlY3Rvcilcbjpob3N0IGFuZCA6aG9zdC1jb250ZXh0IGFyZSBwc2V1ZG8tY2xhc3Nlcy4gU28gd2UgYXNzdW1lIHRoZXkgY291bGQgYmUgY29tYmluZWQsXG5saWtlIG90aGVyIHBzZXVkby1jbGFzc2VzLCBldmVuIHNhbWUgb25lcy5cbkZvciBleGFtcGxlOiAnOm50aC1vZi10eXBlKDJuKTpudGgtb2YtdHlwZShldmVuKScuXG5cbklkZWFsIHNvbHV0aW9uIHdvdWxkIGJlIHRvIHByZXBlbmQgYW55IHNlbGVjdG9yIHdpdGggOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pLlxuVGhlbiBuZWJ1bGFyIGNvbXBvbmVudHMgd2lsbCBiZWhhdmUgYXMgYW4gaHRtbCBlbGVtZW50IGFuZCByZXNwb25kIHRvIFtkaXJdIGF0dHJpYnV0ZSBvbiBhbnkgbGV2ZWwsXG5zbyBkaXJlY3Rpb24gY291bGQgYmUgb3ZlcnJpZGRlbiBvbiBhbnkgY29tcG9uZW50IGxldmVsLlxuXG5JbXBsZW1lbnRhdGlvbiBjb2RlOlxuXG5AbWl4aW4gbmItcnRsKCkge1xuICAvLyBhZGQgIyB0byBzY3NzIGludGVycG9sYXRpb24gc3RhdGVtZW50LlxuICAvLyBpdCB3b3JrcyBpbiBjb21tZW50cyBhbmQgd2UgY2FuJ3QgdXNlIGl0IGhlcmVcbiAgQGF0LXJvb3Qge3NlbGVjdG9yLWFwcGVuZCgnOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pJywgJil9IHtcbiAgICBAY29udGVudDtcbiAgfVxufVxuXG5BbmQgd2hlbiB3ZSBjYWxsIGl0IHNvbWV3aGVyZTpcblxuOmhvc3Qge1xuICAuc29tZS1jbGFzcyB7XG4gICAgQGluY2x1ZGUgbmItcnRsKCkge1xuICAgICAgLi4uXG4gICAgfVxuICB9XG59XG46aG9zdC1jb250ZXh0KC4uLikge1xuICAuc29tZS1jbGFzcyB7XG4gICAgQGluY2x1ZGUgbmItcnRsKCkge1xuICAgICAgLi4uXG4gICAgfVxuICB9XG59XG5cblJlc3VsdCB3aWxsIGxvb2sgbGlrZTpcblxuOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pOmhvc3QgLnNvbWUtY2xhc3Mge1xuICAuLi5cbn1cbjpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKTpob3N0LWNvbnRleHQoLi4uKSAuc29tZS1jbGFzcyB7XG4gIC4uLlxufVxuXG4qXG4gIFNpZGUgbm90ZTpcbiAgOmhvc3QtY29udGV4dCgpOmhvc3Qgc2VsZWN0b3IgYXJlIHZhbGlkLiBodHRwczovL2xpc3RzLnczLm9yZy9BcmNoaXZlcy9QdWJsaWMvd3d3LXN0eWxlLzIwMTVGZWIvMDMwNS5odG1sXG5cbiAgOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pOmhvc3QtY29udGV4dCguLi4pIHNob3VsZCBtYXRjaCBhbnkgcGVybXV0YXRpb24sXG4gIHNvIG9yZGVyIGlzIG5vdCBpbXBvcnRhbnQuXG4qXG5cblxuQ3VycmVudGx5LCB0aGVyZSdyZSB0d28gcHJvYmxlbXMgd2l0aCB0aGlzIGFwcHJvYWNoOlxuXG5GaXJzdCwgaXMgdGhhdCB3ZSBjYW4ndCBjb21iaW5lIDpob3N0LCA6aG9zdC1jb250ZXh0LiBBbmd1bGFyIGJ1Z3MgIzE0MzQ5LCAjMTkxOTkuXG5Gb3IgdGhlIG1vbWVudCBvZiB3cml0aW5nLCB0aGUgb25seSBwb3NzaWJsZSB3YXkgaXM6XG46aG9zdCB7XG4gIDpob3N0LWNvbnRleHQoLi4uKSB7XG4gICAgLi4uXG4gIH1cbn1cbkl0IGRvZXNuJ3Qgd29yayBmb3IgdXMgYmVjYXVzZSBtaXhpbiBjb3VsZCBiZSBjYWxsZWQgc29tZXdoZXJlIGRlZXBlciwgbGlrZTpcbjpob3N0IHtcbiAgcCB7XG4gICAgQGluY2x1ZGUgbmItcnRsKCkgeyAuLi4gfVxuICB9XG59XG5XZSBhcmUgbm90IGFibGUgdG8gZ28gdXAgdG8gOmhvc3QgbGV2ZWwgdG8gcGxhY2UgY29udGVudCBwYXNzZWQgdG8gbWl4aW4uXG5cblRoZSBzZWNvbmQgcHJvYmxlbSBpcyB0aGF0IHdlIG9ubHkgY2FuIGJlIHN1cmUgdGhhdCB3ZSBhcHBlbmRpbmcgOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pIHRvIGFub3RoZXJcbjpob3N0Lzpob3N0LWNvbnRleHQgcHNldWRvLWNsYXNzIHdoZW4gY2FsbGVkIGluIHRoZW1lIGZpbGVzICgqLnRoZW1lLnNjc3MpLlxuICAqXG4gICAgU2lkZSBub3RlOlxuICAgIEN1cnJlbnRseSwgbmItaW5zdGFsbC1jb21wb25lbnQgdXNlcyBhbm90aGVyIGFwcHJvYWNoIHdoZXJlIDpob3N0IHByZXBlbmRlZCB3aXRoIHRoZSB0aGVtZSBuYW1lXG4gICAgKGh0dHBzOi8vZ2l0aHViLmNvbS9hbmd1bGFyL2FuZ3VsYXIvYmxvYi81Yjk2MDc4NjI0YjBhNDc2MGYyZGJjZjZmZGYwYmQ2Mjc5MWJlNWJiL3BhY2thZ2VzL2NvbXBpbGVyL3NyYy9zaGFkb3dfY3NzLnRzI0w0NDEpLFxuICAgIGJ1dCBpdCB3YXMgbWFkZSB0byBiZSBhYmxlIHRvIHVzZSBjdXJyZW50IHJlYWxpemF0aW9uIG9mIHJ0bCBhbmQgaXQgY2FuIGJlIHJld3JpdHRlbiBiYWNrIHRvXG4gICAgOmhvc3QtY29udGV4dCgkdGhlbWUpIG9uY2Ugd2Ugd2lsbCBiZSBhYmxlIHRvIHVzZSBtdWx0aXBsZSBzaGFkb3cgc2VsZWN0b3JzLlxuICAqXG5CdXQgd2hlbiBpdCdzIGNhbGxlZCBpbiAqLmNvbXBvbmVudC5zY3NzIHdlIGNhbid0IGJlIHN1cmUsIHRoYXQgc2VsZWN0b3Igc3RhcnRzIHdpdGggOmhvc3QvOmhvc3QtY29udGV4dCxcbmJlY2F1c2UgYW5ndWxhciBhbGxvd3Mgb21pdHRpbmcgcHNldWRvLWNsYXNzZXMgaWYgd2UgZG9uJ3QgbmVlZCB0byBzdHlsZSA6aG9zdCBjb21wb25lbnQgaXRzZWxmLlxuV2UgY2FuIGJyZWFrIHN1Y2ggc2VsZWN0b3JzLCBieSBqdXN0IGFwcGVuZGluZyA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkgdG8gdGhlbS5cbiAgKioqXG4gICAgUG9zc2libGUgc29sdXRpb25cbiAgICBjaGVjayBpZiB3ZSBpbiB0aGVtZSBieSBzb21lIHRoZW1lIHZhcmlhYmxlcyBhbmQgaWYgc28gYXBwZW5kLCBvdGhlcndpc2UgbmVzdCBsaWtlXG4gICAgQGF0LXJvb3QgOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pIHtcbiAgICAgIC8vIGFkZCAjIHRvIHNjc3MgaW50ZXJwb2xhdGlvbiBzdGF0ZW1lbnQuXG4gICAgICAvLyBpdCB3b3JrcyBpbiBjb21tZW50cyBhbmQgd2UgY2FuJ3QgdXNlIGl0IGhlcmVcbiAgICAgIHsmfSB7XG4gICAgICAgIEBjb250ZW50O1xuICAgICAgfVxuICAgIH1cbiAgICBXaGF0IGlmIDpob3N0IHNwZWNpZmllZD8gQ2FuIHdlIGFkZCBzcGFjZSBpbiA6aG9zdC1jb250ZXh0KC4uLikgOmhvc3Q/XG4gICAgT3IgbWF5YmUgYWRkIDpob3N0IHNlbGVjdG9yIGFueXdheT8gSWYgbXVsdGlwbGUgOmhvc3Qgc2VsZWN0b3JzIGFyZSBhbGxvd2VkXG4gICoqKlxuXG5cblByb2JsZW1zIHdpdGggdGhlIGN1cnJlbnQgYXBwcm9hY2guXG5cbjEuIERpcmVjdGlvbiBjYW4gYmUgYXBwbGllZCBvbmx5IG9uIGRvY3VtZW50IGxldmVsLCBiZWNhdXNlIG1peGluIHByZXBlbmRzIHRoZW1lIGNsYXNzLFxud2hpY2ggcGxhY2VkIG9uIHRoZSBib2R5LlxuMi4gKi5jb21wb25lbnQuc2NzcyBzdHlsZXMgc2hvdWxkIGJlIGluIDpob3N0IHNlbGVjdG9yLiBPdGhlcndpc2UgYW5ndWxhciB3aWxsIGFkZCBob3N0XG5hdHRyaWJ1dGUgdG8gW2Rpcj1ydGxdIGF0dHJpYnV0ZSBhcyB3ZWxsLlxuXG5cbkdlbmVyYWwgcHJvYmxlbXMuXG5cbkx0ciBpcyBkZWZhdWx0IGRvY3VtZW50IGRpcmVjdGlvbiwgYnV0IGZvciBwcm9wZXIgd29yayBvZiBuYi1sdHIgKG1lYW5zIGx0ciBvbmx5KSxcbltkaXI9bHRyXSBzaG91bGQgYmUgc3BlY2lmaWVkIGF0IGxlYXN0IHNvbWV3aGVyZS4gJzpub3QoW2Rpcj1ydGxdJyBub3QgYXBwbGljYWJsZSBoZXJlLFxuYmVjYXVzZSBpdCdzIHNhdGlzZnkgYW55IHBhcmVudCwgdGhhdCBkb24ndCBoYXZlIFtkaXI9cnRsXSBhdHRyaWJ1dGUuXG5QcmV2aW91cyBhcHByb2FjaCB3YXMgdG8gdXNlIHNpbmdsZSBydGwgbWl4aW4gYW5kIHJlc2V0IGx0ciBwcm9wZXJ0aWVzIHRvIGluaXRpYWwgdmFsdWUuXG5CdXQgc29tZXRpbWVzIGl0J3MgaGFyZCB0byBmaW5kLCB3aGF0IHRoZSBwcmV2aW91cyB2YWx1ZSBzaG91bGQgYmUuIEFuZCBzdWNoIG1peGluIGNhbGwgbG9va3MgdG9vIHZlcmJvc2UuXG4qL1xuLyoqXG4gKiBAbGljZW5zZVxuICogQ29weXJpZ2h0IEFrdmVvLiBBbGwgUmlnaHRzIFJlc2VydmVkLlxuICogTGljZW5zZWQgdW5kZXIgdGhlIE1JVCBMaWNlbnNlLiBTZWUgTGljZW5zZS50eHQgaW4gdGhlIHByb2plY3Qgcm9vdCBmb3IgbGljZW5zZSBpbmZvcm1hdGlvbi5cbiAqL1xuLyoqXG4gKiBAbGljZW5zZVxuICogQ29weXJpZ2h0IEFrdmVvLiBBbGwgUmlnaHRzIFJlc2VydmVkLlxuICogTGljZW5zZWQgdW5kZXIgdGhlIE1JVCBMaWNlbnNlLiBTZWUgTGljZW5zZS50eHQgaW4gdGhlIHByb2plY3Qgcm9vdCBmb3IgbGljZW5zZSBpbmZvcm1hdGlvbi5cbiAqL1xuLyoqXG4gKiBAbGljZW5zZVxuICogQ29weXJpZ2h0IEFrdmVvLiBBbGwgUmlnaHRzIFJlc2VydmVkLlxuICogTGljZW5zZWQgdW5kZXIgdGhlIE1JVCBMaWNlbnNlLiBTZWUgTGljZW5zZS50eHQgaW4gdGhlIHByb2plY3Qgcm9vdCBmb3IgbGljZW5zZSBpbmZvcm1hdGlvbi5cbiAqL1xuLyoqXG4gKiBUaGlzIG1peGluIGdlbmVyYXRlcyBrZXlmYW1lcy5cbiAqIEJlY2F1c2Ugb2YgYWxsIGtleWZyYW1lcyBjYW4ndCBiZSBzY29wZWQsXG4gKiB3ZSBuZWVkIHRvIHB1dHMgdW5pcXVlIG5hbWUgaW4gZWFjaCBidG4tcHVsc2UgY2FsbC5cbiAqL1xuLypcblxuQWNjb3JkaW5nIHRvIHRoZSBzcGVjaWZpY2F0aW9uIChodHRwczovL3d3dy53My5vcmcvVFIvY3NzLXNjb3BpbmctMS8jaG9zdC1zZWxlY3Rvcilcbjpob3N0IGFuZCA6aG9zdC1jb250ZXh0IGFyZSBwc2V1ZG8tY2xhc3Nlcy4gU28gd2UgYXNzdW1lIHRoZXkgY291bGQgYmUgY29tYmluZWQsXG5saWtlIG90aGVyIHBzZXVkby1jbGFzc2VzLCBldmVuIHNhbWUgb25lcy5cbkZvciBleGFtcGxlOiAnOm50aC1vZi10eXBlKDJuKTpudGgtb2YtdHlwZShldmVuKScuXG5cbklkZWFsIHNvbHV0aW9uIHdvdWxkIGJlIHRvIHByZXBlbmQgYW55IHNlbGVjdG9yIHdpdGggOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pLlxuVGhlbiBuZWJ1bGFyIGNvbXBvbmVudHMgd2lsbCBiZWhhdmUgYXMgYW4gaHRtbCBlbGVtZW50IGFuZCByZXNwb25kIHRvIFtkaXJdIGF0dHJpYnV0ZSBvbiBhbnkgbGV2ZWwsXG5zbyBkaXJlY3Rpb24gY291bGQgYmUgb3ZlcnJpZGRlbiBvbiBhbnkgY29tcG9uZW50IGxldmVsLlxuXG5JbXBsZW1lbnRhdGlvbiBjb2RlOlxuXG5AbWl4aW4gbmItcnRsKCkge1xuICAvLyBhZGQgIyB0byBzY3NzIGludGVycG9sYXRpb24gc3RhdGVtZW50LlxuICAvLyBpdCB3b3JrcyBpbiBjb21tZW50cyBhbmQgd2UgY2FuJ3QgdXNlIGl0IGhlcmVcbiAgQGF0LXJvb3Qge3NlbGVjdG9yLWFwcGVuZCgnOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pJywgJil9IHtcbiAgICBAY29udGVudDtcbiAgfVxufVxuXG5BbmQgd2hlbiB3ZSBjYWxsIGl0IHNvbWV3aGVyZTpcblxuOmhvc3Qge1xuICAuc29tZS1jbGFzcyB7XG4gICAgQGluY2x1ZGUgbmItcnRsKCkge1xuICAgICAgLi4uXG4gICAgfVxuICB9XG59XG46aG9zdC1jb250ZXh0KC4uLikge1xuICAuc29tZS1jbGFzcyB7XG4gICAgQGluY2x1ZGUgbmItcnRsKCkge1xuICAgICAgLi4uXG4gICAgfVxuICB9XG59XG5cblJlc3VsdCB3aWxsIGxvb2sgbGlrZTpcblxuOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pOmhvc3QgLnNvbWUtY2xhc3Mge1xuICAuLi5cbn1cbjpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKTpob3N0LWNvbnRleHQoLi4uKSAuc29tZS1jbGFzcyB7XG4gIC4uLlxufVxuXG4qXG4gIFNpZGUgbm90ZTpcbiAgOmhvc3QtY29udGV4dCgpOmhvc3Qgc2VsZWN0b3IgYXJlIHZhbGlkLiBodHRwczovL2xpc3RzLnczLm9yZy9BcmNoaXZlcy9QdWJsaWMvd3d3LXN0eWxlLzIwMTVGZWIvMDMwNS5odG1sXG5cbiAgOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pOmhvc3QtY29udGV4dCguLi4pIHNob3VsZCBtYXRjaCBhbnkgcGVybXV0YXRpb24sXG4gIHNvIG9yZGVyIGlzIG5vdCBpbXBvcnRhbnQuXG4qXG5cblxuQ3VycmVudGx5LCB0aGVyZSdyZSB0d28gcHJvYmxlbXMgd2l0aCB0aGlzIGFwcHJvYWNoOlxuXG5GaXJzdCwgaXMgdGhhdCB3ZSBjYW4ndCBjb21iaW5lIDpob3N0LCA6aG9zdC1jb250ZXh0LiBBbmd1bGFyIGJ1Z3MgIzE0MzQ5LCAjMTkxOTkuXG5Gb3IgdGhlIG1vbWVudCBvZiB3cml0aW5nLCB0aGUgb25seSBwb3NzaWJsZSB3YXkgaXM6XG46aG9zdCB7XG4gIDpob3N0LWNvbnRleHQoLi4uKSB7XG4gICAgLi4uXG4gIH1cbn1cbkl0IGRvZXNuJ3Qgd29yayBmb3IgdXMgYmVjYXVzZSBtaXhpbiBjb3VsZCBiZSBjYWxsZWQgc29tZXdoZXJlIGRlZXBlciwgbGlrZTpcbjpob3N0IHtcbiAgcCB7XG4gICAgQGluY2x1ZGUgbmItcnRsKCkgeyAuLi4gfVxuICB9XG59XG5XZSBhcmUgbm90IGFibGUgdG8gZ28gdXAgdG8gOmhvc3QgbGV2ZWwgdG8gcGxhY2UgY29udGVudCBwYXNzZWQgdG8gbWl4aW4uXG5cblRoZSBzZWNvbmQgcHJvYmxlbSBpcyB0aGF0IHdlIG9ubHkgY2FuIGJlIHN1cmUgdGhhdCB3ZSBhcHBlbmRpbmcgOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pIHRvIGFub3RoZXJcbjpob3N0Lzpob3N0LWNvbnRleHQgcHNldWRvLWNsYXNzIHdoZW4gY2FsbGVkIGluIHRoZW1lIGZpbGVzICgqLnRoZW1lLnNjc3MpLlxuICAqXG4gICAgU2lkZSBub3RlOlxuICAgIEN1cnJlbnRseSwgbmItaW5zdGFsbC1jb21wb25lbnQgdXNlcyBhbm90aGVyIGFwcHJvYWNoIHdoZXJlIDpob3N0IHByZXBlbmRlZCB3aXRoIHRoZSB0aGVtZSBuYW1lXG4gICAgKGh0dHBzOi8vZ2l0aHViLmNvbS9hbmd1bGFyL2FuZ3VsYXIvYmxvYi81Yjk2MDc4NjI0YjBhNDc2MGYyZGJjZjZmZGYwYmQ2Mjc5MWJlNWJiL3BhY2thZ2VzL2NvbXBpbGVyL3NyYy9zaGFkb3dfY3NzLnRzI0w0NDEpLFxuICAgIGJ1dCBpdCB3YXMgbWFkZSB0byBiZSBhYmxlIHRvIHVzZSBjdXJyZW50IHJlYWxpemF0aW9uIG9mIHJ0bCBhbmQgaXQgY2FuIGJlIHJld3JpdHRlbiBiYWNrIHRvXG4gICAgOmhvc3QtY29udGV4dCgkdGhlbWUpIG9uY2Ugd2Ugd2lsbCBiZSBhYmxlIHRvIHVzZSBtdWx0aXBsZSBzaGFkb3cgc2VsZWN0b3JzLlxuICAqXG5CdXQgd2hlbiBpdCdzIGNhbGxlZCBpbiAqLmNvbXBvbmVudC5zY3NzIHdlIGNhbid0IGJlIHN1cmUsIHRoYXQgc2VsZWN0b3Igc3RhcnRzIHdpdGggOmhvc3QvOmhvc3QtY29udGV4dCxcbmJlY2F1c2UgYW5ndWxhciBhbGxvd3Mgb21pdHRpbmcgcHNldWRvLWNsYXNzZXMgaWYgd2UgZG9uJ3QgbmVlZCB0byBzdHlsZSA6aG9zdCBjb21wb25lbnQgaXRzZWxmLlxuV2UgY2FuIGJyZWFrIHN1Y2ggc2VsZWN0b3JzLCBieSBqdXN0IGFwcGVuZGluZyA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkgdG8gdGhlbS5cbiAgKioqXG4gICAgUG9zc2libGUgc29sdXRpb25cbiAgICBjaGVjayBpZiB3ZSBpbiB0aGVtZSBieSBzb21lIHRoZW1lIHZhcmlhYmxlcyBhbmQgaWYgc28gYXBwZW5kLCBvdGhlcndpc2UgbmVzdCBsaWtlXG4gICAgQGF0LXJvb3QgOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pIHtcbiAgICAgIC8vIGFkZCAjIHRvIHNjc3MgaW50ZXJwb2xhdGlvbiBzdGF0ZW1lbnQuXG4gICAgICAvLyBpdCB3b3JrcyBpbiBjb21tZW50cyBhbmQgd2UgY2FuJ3QgdXNlIGl0IGhlcmVcbiAgICAgIHsmfSB7XG4gICAgICAgIEBjb250ZW50O1xuICAgICAgfVxuICAgIH1cbiAgICBXaGF0IGlmIDpob3N0IHNwZWNpZmllZD8gQ2FuIHdlIGFkZCBzcGFjZSBpbiA6aG9zdC1jb250ZXh0KC4uLikgOmhvc3Q/XG4gICAgT3IgbWF5YmUgYWRkIDpob3N0IHNlbGVjdG9yIGFueXdheT8gSWYgbXVsdGlwbGUgOmhvc3Qgc2VsZWN0b3JzIGFyZSBhbGxvd2VkXG4gICoqKlxuXG5cblByb2JsZW1zIHdpdGggdGhlIGN1cnJlbnQgYXBwcm9hY2guXG5cbjEuIERpcmVjdGlvbiBjYW4gYmUgYXBwbGllZCBvbmx5IG9uIGRvY3VtZW50IGxldmVsLCBiZWNhdXNlIG1peGluIHByZXBlbmRzIHRoZW1lIGNsYXNzLFxud2hpY2ggcGxhY2VkIG9uIHRoZSBib2R5LlxuMi4gKi5jb21wb25lbnQuc2NzcyBzdHlsZXMgc2hvdWxkIGJlIGluIDpob3N0IHNlbGVjdG9yLiBPdGhlcndpc2UgYW5ndWxhciB3aWxsIGFkZCBob3N0XG5hdHRyaWJ1dGUgdG8gW2Rpcj1ydGxdIGF0dHJpYnV0ZSBhcyB3ZWxsLlxuXG5cbkdlbmVyYWwgcHJvYmxlbXMuXG5cbkx0ciBpcyBkZWZhdWx0IGRvY3VtZW50IGRpcmVjdGlvbiwgYnV0IGZvciBwcm9wZXIgd29yayBvZiBuYi1sdHIgKG1lYW5zIGx0ciBvbmx5KSxcbltkaXI9bHRyXSBzaG91bGQgYmUgc3BlY2lmaWVkIGF0IGxlYXN0IHNvbWV3aGVyZS4gJzpub3QoW2Rpcj1ydGxdJyBub3QgYXBwbGljYWJsZSBoZXJlLFxuYmVjYXVzZSBpdCdzIHNhdGlzZnkgYW55IHBhcmVudCwgdGhhdCBkb24ndCBoYXZlIFtkaXI9cnRsXSBhdHRyaWJ1dGUuXG5QcmV2aW91cyBhcHByb2FjaCB3YXMgdG8gdXNlIHNpbmdsZSBydGwgbWl4aW4gYW5kIHJlc2V0IGx0ciBwcm9wZXJ0aWVzIHRvIGluaXRpYWwgdmFsdWUuXG5CdXQgc29tZXRpbWVzIGl0J3MgaGFyZCB0byBmaW5kLCB3aGF0IHRoZSBwcmV2aW91cyB2YWx1ZSBzaG91bGQgYmUuIEFuZCBzdWNoIG1peGluIGNhbGwgbG9va3MgdG9vIHZlcmJvc2UuXG4qL1xuLyoqXG4gKiBAbGljZW5zZVxuICogQ29weXJpZ2h0IEFrdmVvLiBBbGwgUmlnaHRzIFJlc2VydmVkLlxuICogTGljZW5zZWQgdW5kZXIgdGhlIE1JVCBMaWNlbnNlLiBTZWUgTGljZW5zZS50eHQgaW4gdGhlIHByb2plY3Qgcm9vdCBmb3IgbGljZW5zZSBpbmZvcm1hdGlvbi5cbiAqL1xuLyoqXG4gKiBAbGljZW5zZVxuICogQ29weXJpZ2h0IEFrdmVvLiBBbGwgUmlnaHRzIFJlc2VydmVkLlxuICogTGljZW5zZWQgdW5kZXIgdGhlIE1JVCBMaWNlbnNlLiBTZWUgTGljZW5zZS50eHQgaW4gdGhlIHByb2plY3Qgcm9vdCBmb3IgbGljZW5zZSBpbmZvcm1hdGlvbi5cbiAqL1xuLyoqXG4gKiBAbGljZW5zZVxuICogQ29weXJpZ2h0IEFrdmVvLiBBbGwgUmlnaHRzIFJlc2VydmVkLlxuICogTGljZW5zZWQgdW5kZXIgdGhlIE1JVCBMaWNlbnNlLiBTZWUgTGljZW5zZS50eHQgaW4gdGhlIHByb2plY3Qgcm9vdCBmb3IgbGljZW5zZSBpbmZvcm1hdGlvbi5cbiAqL1xuLyoqXG4gKiBUaGlzIG1peGluIGdlbmVyYXRlcyBrZXlmYW1lcy5cbiAqIEJlY2F1c2Ugb2YgYWxsIGtleWZyYW1lcyBjYW4ndCBiZSBzY29wZWQsXG4gKiB3ZSBuZWVkIHRvIHB1dHMgdW5pcXVlIG5hbWUgaW4gZWFjaCBidG4tcHVsc2UgY2FsbC5cbiAqL1xuLypcblxuQWNjb3JkaW5nIHRvIHRoZSBzcGVjaWZpY2F0aW9uIChodHRwczovL3d3dy53My5vcmcvVFIvY3NzLXNjb3BpbmctMS8jaG9zdC1zZWxlY3Rvcilcbjpob3N0IGFuZCA6aG9zdC1jb250ZXh0IGFyZSBwc2V1ZG8tY2xhc3Nlcy4gU28gd2UgYXNzdW1lIHRoZXkgY291bGQgYmUgY29tYmluZWQsXG5saWtlIG90aGVyIHBzZXVkby1jbGFzc2VzLCBldmVuIHNhbWUgb25lcy5cbkZvciBleGFtcGxlOiAnOm50aC1vZi10eXBlKDJuKTpudGgtb2YtdHlwZShldmVuKScuXG5cbklkZWFsIHNvbHV0aW9uIHdvdWxkIGJlIHRvIHByZXBlbmQgYW55IHNlbGVjdG9yIHdpdGggOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pLlxuVGhlbiBuZWJ1bGFyIGNvbXBvbmVudHMgd2lsbCBiZWhhdmUgYXMgYW4gaHRtbCBlbGVtZW50IGFuZCByZXNwb25kIHRvIFtkaXJdIGF0dHJpYnV0ZSBvbiBhbnkgbGV2ZWwsXG5zbyBkaXJlY3Rpb24gY291bGQgYmUgb3ZlcnJpZGRlbiBvbiBhbnkgY29tcG9uZW50IGxldmVsLlxuXG5JbXBsZW1lbnRhdGlvbiBjb2RlOlxuXG5AbWl4aW4gbmItcnRsKCkge1xuICAvLyBhZGQgIyB0byBzY3NzIGludGVycG9sYXRpb24gc3RhdGVtZW50LlxuICAvLyBpdCB3b3JrcyBpbiBjb21tZW50cyBhbmQgd2UgY2FuJ3QgdXNlIGl0IGhlcmVcbiAgQGF0LXJvb3Qge3NlbGVjdG9yLWFwcGVuZCgnOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pJywgJil9IHtcbiAgICBAY29udGVudDtcbiAgfVxufVxuXG5BbmQgd2hlbiB3ZSBjYWxsIGl0IHNvbWV3aGVyZTpcblxuOmhvc3Qge1xuICAuc29tZS1jbGFzcyB7XG4gICAgQGluY2x1ZGUgbmItcnRsKCkge1xuICAgICAgLi4uXG4gICAgfVxuICB9XG59XG46aG9zdC1jb250ZXh0KC4uLikge1xuICAuc29tZS1jbGFzcyB7XG4gICAgQGluY2x1ZGUgbmItcnRsKCkge1xuICAgICAgLi4uXG4gICAgfVxuICB9XG59XG5cblJlc3VsdCB3aWxsIGxvb2sgbGlrZTpcblxuOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pOmhvc3QgLnNvbWUtY2xhc3Mge1xuICAuLi5cbn1cbjpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKTpob3N0LWNvbnRleHQoLi4uKSAuc29tZS1jbGFzcyB7XG4gIC4uLlxufVxuXG4qXG4gIFNpZGUgbm90ZTpcbiAgOmhvc3QtY29udGV4dCgpOmhvc3Qgc2VsZWN0b3IgYXJlIHZhbGlkLiBodHRwczovL2xpc3RzLnczLm9yZy9BcmNoaXZlcy9QdWJsaWMvd3d3LXN0eWxlLzIwMTVGZWIvMDMwNS5odG1sXG5cbiAgOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pOmhvc3QtY29udGV4dCguLi4pIHNob3VsZCBtYXRjaCBhbnkgcGVybXV0YXRpb24sXG4gIHNvIG9yZGVyIGlzIG5vdCBpbXBvcnRhbnQuXG4qXG5cblxuQ3VycmVudGx5LCB0aGVyZSdyZSB0d28gcHJvYmxlbXMgd2l0aCB0aGlzIGFwcHJvYWNoOlxuXG5GaXJzdCwgaXMgdGhhdCB3ZSBjYW4ndCBjb21iaW5lIDpob3N0LCA6aG9zdC1jb250ZXh0LiBBbmd1bGFyIGJ1Z3MgIzE0MzQ5LCAjMTkxOTkuXG5Gb3IgdGhlIG1vbWVudCBvZiB3cml0aW5nLCB0aGUgb25seSBwb3NzaWJsZSB3YXkgaXM6XG46aG9zdCB7XG4gIDpob3N0LWNvbnRleHQoLi4uKSB7XG4gICAgLi4uXG4gIH1cbn1cbkl0IGRvZXNuJ3Qgd29yayBmb3IgdXMgYmVjYXVzZSBtaXhpbiBjb3VsZCBiZSBjYWxsZWQgc29tZXdoZXJlIGRlZXBlciwgbGlrZTpcbjpob3N0IHtcbiAgcCB7XG4gICAgQGluY2x1ZGUgbmItcnRsKCkgeyAuLi4gfVxuICB9XG59XG5XZSBhcmUgbm90IGFibGUgdG8gZ28gdXAgdG8gOmhvc3QgbGV2ZWwgdG8gcGxhY2UgY29udGVudCBwYXNzZWQgdG8gbWl4aW4uXG5cblRoZSBzZWNvbmQgcHJvYmxlbSBpcyB0aGF0IHdlIG9ubHkgY2FuIGJlIHN1cmUgdGhhdCB3ZSBhcHBlbmRpbmcgOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pIHRvIGFub3RoZXJcbjpob3N0Lzpob3N0LWNvbnRleHQgcHNldWRvLWNsYXNzIHdoZW4gY2FsbGVkIGluIHRoZW1lIGZpbGVzICgqLnRoZW1lLnNjc3MpLlxuICAqXG4gICAgU2lkZSBub3RlOlxuICAgIEN1cnJlbnRseSwgbmItaW5zdGFsbC1jb21wb25lbnQgdXNlcyBhbm90aGVyIGFwcHJvYWNoIHdoZXJlIDpob3N0IHByZXBlbmRlZCB3aXRoIHRoZSB0aGVtZSBuYW1lXG4gICAgKGh0dHBzOi8vZ2l0aHViLmNvbS9hbmd1bGFyL2FuZ3VsYXIvYmxvYi81Yjk2MDc4NjI0YjBhNDc2MGYyZGJjZjZmZGYwYmQ2Mjc5MWJlNWJiL3BhY2thZ2VzL2NvbXBpbGVyL3NyYy9zaGFkb3dfY3NzLnRzI0w0NDEpLFxuICAgIGJ1dCBpdCB3YXMgbWFkZSB0byBiZSBhYmxlIHRvIHVzZSBjdXJyZW50IHJlYWxpemF0aW9uIG9mIHJ0bCBhbmQgaXQgY2FuIGJlIHJld3JpdHRlbiBiYWNrIHRvXG4gICAgOmhvc3QtY29udGV4dCgkdGhlbWUpIG9uY2Ugd2Ugd2lsbCBiZSBhYmxlIHRvIHVzZSBtdWx0aXBsZSBzaGFkb3cgc2VsZWN0b3JzLlxuICAqXG5CdXQgd2hlbiBpdCdzIGNhbGxlZCBpbiAqLmNvbXBvbmVudC5zY3NzIHdlIGNhbid0IGJlIHN1cmUsIHRoYXQgc2VsZWN0b3Igc3RhcnRzIHdpdGggOmhvc3QvOmhvc3QtY29udGV4dCxcbmJlY2F1c2UgYW5ndWxhciBhbGxvd3Mgb21pdHRpbmcgcHNldWRvLWNsYXNzZXMgaWYgd2UgZG9uJ3QgbmVlZCB0byBzdHlsZSA6aG9zdCBjb21wb25lbnQgaXRzZWxmLlxuV2UgY2FuIGJyZWFrIHN1Y2ggc2VsZWN0b3JzLCBieSBqdXN0IGFwcGVuZGluZyA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkgdG8gdGhlbS5cbiAgKioqXG4gICAgUG9zc2libGUgc29sdXRpb25cbiAgICBjaGVjayBpZiB3ZSBpbiB0aGVtZSBieSBzb21lIHRoZW1lIHZhcmlhYmxlcyBhbmQgaWYgc28gYXBwZW5kLCBvdGhlcndpc2UgbmVzdCBsaWtlXG4gICAgQGF0LXJvb3QgOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pIHtcbiAgICAgIC8vIGFkZCAjIHRvIHNjc3MgaW50ZXJwb2xhdGlvbiBzdGF0ZW1lbnQuXG4gICAgICAvLyBpdCB3b3JrcyBpbiBjb21tZW50cyBhbmQgd2UgY2FuJ3QgdXNlIGl0IGhlcmVcbiAgICAgIHsmfSB7XG4gICAgICAgIEBjb250ZW50O1xuICAgICAgfVxuICAgIH1cbiAgICBXaGF0IGlmIDpob3N0IHNwZWNpZmllZD8gQ2FuIHdlIGFkZCBzcGFjZSBpbiA6aG9zdC1jb250ZXh0KC4uLikgOmhvc3Q/XG4gICAgT3IgbWF5YmUgYWRkIDpob3N0IHNlbGVjdG9yIGFueXdheT8gSWYgbXVsdGlwbGUgOmhvc3Qgc2VsZWN0b3JzIGFyZSBhbGxvd2VkXG4gICoqKlxuXG5cblByb2JsZW1zIHdpdGggdGhlIGN1cnJlbnQgYXBwcm9hY2guXG5cbjEuIERpcmVjdGlvbiBjYW4gYmUgYXBwbGllZCBvbmx5IG9uIGRvY3VtZW50IGxldmVsLCBiZWNhdXNlIG1peGluIHByZXBlbmRzIHRoZW1lIGNsYXNzLFxud2hpY2ggcGxhY2VkIG9uIHRoZSBib2R5LlxuMi4gKi5jb21wb25lbnQuc2NzcyBzdHlsZXMgc2hvdWxkIGJlIGluIDpob3N0IHNlbGVjdG9yLiBPdGhlcndpc2UgYW5ndWxhciB3aWxsIGFkZCBob3N0XG5hdHRyaWJ1dGUgdG8gW2Rpcj1ydGxdIGF0dHJpYnV0ZSBhcyB3ZWxsLlxuXG5cbkdlbmVyYWwgcHJvYmxlbXMuXG5cbkx0ciBpcyBkZWZhdWx0IGRvY3VtZW50IGRpcmVjdGlvbiwgYnV0IGZvciBwcm9wZXIgd29yayBvZiBuYi1sdHIgKG1lYW5zIGx0ciBvbmx5KSxcbltkaXI9bHRyXSBzaG91bGQgYmUgc3BlY2lmaWVkIGF0IGxlYXN0IHNvbWV3aGVyZS4gJzpub3QoW2Rpcj1ydGxdJyBub3QgYXBwbGljYWJsZSBoZXJlLFxuYmVjYXVzZSBpdCdzIHNhdGlzZnkgYW55IHBhcmVudCwgdGhhdCBkb24ndCBoYXZlIFtkaXI9cnRsXSBhdHRyaWJ1dGUuXG5QcmV2aW91cyBhcHByb2FjaCB3YXMgdG8gdXNlIHNpbmdsZSBydGwgbWl4aW4gYW5kIHJlc2V0IGx0ciBwcm9wZXJ0aWVzIHRvIGluaXRpYWwgdmFsdWUuXG5CdXQgc29tZXRpbWVzIGl0J3MgaGFyZCB0byBmaW5kLCB3aGF0IHRoZSBwcmV2aW91cyB2YWx1ZSBzaG91bGQgYmUuIEFuZCBzdWNoIG1peGluIGNhbGwgbG9va3MgdG9vIHZlcmJvc2UuXG4qL1xuLyoqXG4gKiBAbGljZW5zZVxuICogQ29weXJpZ2h0IEFrdmVvLiBBbGwgUmlnaHRzIFJlc2VydmVkLlxuICogTGljZW5zZWQgdW5kZXIgdGhlIE1JVCBMaWNlbnNlLiBTZWUgTGljZW5zZS50eHQgaW4gdGhlIHByb2plY3Qgcm9vdCBmb3IgbGljZW5zZSBpbmZvcm1hdGlvbi5cbiAqL1xuLyoqXG4gKiBAbGljZW5zZVxuICogQ29weXJpZ2h0IEFrdmVvLiBBbGwgUmlnaHRzIFJlc2VydmVkLlxuICogTGljZW5zZWQgdW5kZXIgdGhlIE1JVCBMaWNlbnNlLiBTZWUgTGljZW5zZS50eHQgaW4gdGhlIHByb2plY3Qgcm9vdCBmb3IgbGljZW5zZSBpbmZvcm1hdGlvbi5cbiAqL1xuLyoqXG4gKiBAbGljZW5zZVxuICogQ29weXJpZ2h0IEFrdmVvLiBBbGwgUmlnaHRzIFJlc2VydmVkLlxuICogTGljZW5zZWQgdW5kZXIgdGhlIE1JVCBMaWNlbnNlLiBTZWUgTGljZW5zZS50eHQgaW4gdGhlIHByb2plY3Qgcm9vdCBmb3IgbGljZW5zZSBpbmZvcm1hdGlvbi5cbiAqL1xuLyoqXG4gKiBUaGlzIG1peGluIGdlbmVyYXRlcyBrZXlmYW1lcy5cbiAqIEJlY2F1c2Ugb2YgYWxsIGtleWZyYW1lcyBjYW4ndCBiZSBzY29wZWQsXG4gKiB3ZSBuZWVkIHRvIHB1dHMgdW5pcXVlIG5hbWUgaW4gZWFjaCBidG4tcHVsc2UgY2FsbC5cbiAqL1xuLypcblxuQWNjb3JkaW5nIHRvIHRoZSBzcGVjaWZpY2F0aW9uIChodHRwczovL3d3dy53My5vcmcvVFIvY3NzLXNjb3BpbmctMS8jaG9zdC1zZWxlY3Rvcilcbjpob3N0IGFuZCA6aG9zdC1jb250ZXh0IGFyZSBwc2V1ZG8tY2xhc3Nlcy4gU28gd2UgYXNzdW1lIHRoZXkgY291bGQgYmUgY29tYmluZWQsXG5saWtlIG90aGVyIHBzZXVkby1jbGFzc2VzLCBldmVuIHNhbWUgb25lcy5cbkZvciBleGFtcGxlOiAnOm50aC1vZi10eXBlKDJuKTpudGgtb2YtdHlwZShldmVuKScuXG5cbklkZWFsIHNvbHV0aW9uIHdvdWxkIGJlIHRvIHByZXBlbmQgYW55IHNlbGVjdG9yIHdpdGggOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pLlxuVGhlbiBuZWJ1bGFyIGNvbXBvbmVudHMgd2lsbCBiZWhhdmUgYXMgYW4gaHRtbCBlbGVtZW50IGFuZCByZXNwb25kIHRvIFtkaXJdIGF0dHJpYnV0ZSBvbiBhbnkgbGV2ZWwsXG5zbyBkaXJlY3Rpb24gY291bGQgYmUgb3ZlcnJpZGRlbiBvbiBhbnkgY29tcG9uZW50IGxldmVsLlxuXG5JbXBsZW1lbnRhdGlvbiBjb2RlOlxuXG5AbWl4aW4gbmItcnRsKCkge1xuICAvLyBhZGQgIyB0byBzY3NzIGludGVycG9sYXRpb24gc3RhdGVtZW50LlxuICAvLyBpdCB3b3JrcyBpbiBjb21tZW50cyBhbmQgd2UgY2FuJ3QgdXNlIGl0IGhlcmVcbiAgQGF0LXJvb3Qge3NlbGVjdG9yLWFwcGVuZCgnOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pJywgJil9IHtcbiAgICBAY29udGVudDtcbiAgfVxufVxuXG5BbmQgd2hlbiB3ZSBjYWxsIGl0IHNvbWV3aGVyZTpcblxuOmhvc3Qge1xuICAuc29tZS1jbGFzcyB7XG4gICAgQGluY2x1ZGUgbmItcnRsKCkge1xuICAgICAgLi4uXG4gICAgfVxuICB9XG59XG46aG9zdC1jb250ZXh0KC4uLikge1xuICAuc29tZS1jbGFzcyB7XG4gICAgQGluY2x1ZGUgbmItcnRsKCkge1xuICAgICAgLi4uXG4gICAgfVxuICB9XG59XG5cblJlc3VsdCB3aWxsIGxvb2sgbGlrZTpcblxuOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pOmhvc3QgLnNvbWUtY2xhc3Mge1xuICAuLi5cbn1cbjpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKTpob3N0LWNvbnRleHQoLi4uKSAuc29tZS1jbGFzcyB7XG4gIC4uLlxufVxuXG4qXG4gIFNpZGUgbm90ZTpcbiAgOmhvc3QtY29udGV4dCgpOmhvc3Qgc2VsZWN0b3IgYXJlIHZhbGlkLiBodHRwczovL2xpc3RzLnczLm9yZy9BcmNoaXZlcy9QdWJsaWMvd3d3LXN0eWxlLzIwMTVGZWIvMDMwNS5odG1sXG5cbiAgOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pOmhvc3QtY29udGV4dCguLi4pIHNob3VsZCBtYXRjaCBhbnkgcGVybXV0YXRpb24sXG4gIHNvIG9yZGVyIGlzIG5vdCBpbXBvcnRhbnQuXG4qXG5cblxuQ3VycmVudGx5LCB0aGVyZSdyZSB0d28gcHJvYmxlbXMgd2l0aCB0aGlzIGFwcHJvYWNoOlxuXG5GaXJzdCwgaXMgdGhhdCB3ZSBjYW4ndCBjb21iaW5lIDpob3N0LCA6aG9zdC1jb250ZXh0LiBBbmd1bGFyIGJ1Z3MgIzE0MzQ5LCAjMTkxOTkuXG5Gb3IgdGhlIG1vbWVudCBvZiB3cml0aW5nLCB0aGUgb25seSBwb3NzaWJsZSB3YXkgaXM6XG46aG9zdCB7XG4gIDpob3N0LWNvbnRleHQoLi4uKSB7XG4gICAgLi4uXG4gIH1cbn1cbkl0IGRvZXNuJ3Qgd29yayBmb3IgdXMgYmVjYXVzZSBtaXhpbiBjb3VsZCBiZSBjYWxsZWQgc29tZXdoZXJlIGRlZXBlciwgbGlrZTpcbjpob3N0IHtcbiAgcCB7XG4gICAgQGluY2x1ZGUgbmItcnRsKCkgeyAuLi4gfVxuICB9XG59XG5XZSBhcmUgbm90IGFibGUgdG8gZ28gdXAgdG8gOmhvc3QgbGV2ZWwgdG8gcGxhY2UgY29udGVudCBwYXNzZWQgdG8gbWl4aW4uXG5cblRoZSBzZWNvbmQgcHJvYmxlbSBpcyB0aGF0IHdlIG9ubHkgY2FuIGJlIHN1cmUgdGhhdCB3ZSBhcHBlbmRpbmcgOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pIHRvIGFub3RoZXJcbjpob3N0Lzpob3N0LWNvbnRleHQgcHNldWRvLWNsYXNzIHdoZW4gY2FsbGVkIGluIHRoZW1lIGZpbGVzICgqLnRoZW1lLnNjc3MpLlxuICAqXG4gICAgU2lkZSBub3RlOlxuICAgIEN1cnJlbnRseSwgbmItaW5zdGFsbC1jb21wb25lbnQgdXNlcyBhbm90aGVyIGFwcHJvYWNoIHdoZXJlIDpob3N0IHByZXBlbmRlZCB3aXRoIHRoZSB0aGVtZSBuYW1lXG4gICAgKGh0dHBzOi8vZ2l0aHViLmNvbS9hbmd1bGFyL2FuZ3VsYXIvYmxvYi81Yjk2MDc4NjI0YjBhNDc2MGYyZGJjZjZmZGYwYmQ2Mjc5MWJlNWJiL3BhY2thZ2VzL2NvbXBpbGVyL3NyYy9zaGFkb3dfY3NzLnRzI0w0NDEpLFxuICAgIGJ1dCBpdCB3YXMgbWFkZSB0byBiZSBhYmxlIHRvIHVzZSBjdXJyZW50IHJlYWxpemF0aW9uIG9mIHJ0bCBhbmQgaXQgY2FuIGJlIHJld3JpdHRlbiBiYWNrIHRvXG4gICAgOmhvc3QtY29udGV4dCgkdGhlbWUpIG9uY2Ugd2Ugd2lsbCBiZSBhYmxlIHRvIHVzZSBtdWx0aXBsZSBzaGFkb3cgc2VsZWN0b3JzLlxuICAqXG5CdXQgd2hlbiBpdCdzIGNhbGxlZCBpbiAqLmNvbXBvbmVudC5zY3NzIHdlIGNhbid0IGJlIHN1cmUsIHRoYXQgc2VsZWN0b3Igc3RhcnRzIHdpdGggOmhvc3QvOmhvc3QtY29udGV4dCxcbmJlY2F1c2UgYW5ndWxhciBhbGxvd3Mgb21pdHRpbmcgcHNldWRvLWNsYXNzZXMgaWYgd2UgZG9uJ3QgbmVlZCB0byBzdHlsZSA6aG9zdCBjb21wb25lbnQgaXRzZWxmLlxuV2UgY2FuIGJyZWFrIHN1Y2ggc2VsZWN0b3JzLCBieSBqdXN0IGFwcGVuZGluZyA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkgdG8gdGhlbS5cbiAgKioqXG4gICAgUG9zc2libGUgc29sdXRpb25cbiAgICBjaGVjayBpZiB3ZSBpbiB0aGVtZSBieSBzb21lIHRoZW1lIHZhcmlhYmxlcyBhbmQgaWYgc28gYXBwZW5kLCBvdGhlcndpc2UgbmVzdCBsaWtlXG4gICAgQGF0LXJvb3QgOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pIHtcbiAgICAgIC8vIGFkZCAjIHRvIHNjc3MgaW50ZXJwb2xhdGlvbiBzdGF0ZW1lbnQuXG4gICAgICAvLyBpdCB3b3JrcyBpbiBjb21tZW50cyBhbmQgd2UgY2FuJ3QgdXNlIGl0IGhlcmVcbiAgICAgIHsmfSB7XG4gICAgICAgIEBjb250ZW50O1xuICAgICAgfVxuICAgIH1cbiAgICBXaGF0IGlmIDpob3N0IHNwZWNpZmllZD8gQ2FuIHdlIGFkZCBzcGFjZSBpbiA6aG9zdC1jb250ZXh0KC4uLikgOmhvc3Q/XG4gICAgT3IgbWF5YmUgYWRkIDpob3N0IHNlbGVjdG9yIGFueXdheT8gSWYgbXVsdGlwbGUgOmhvc3Qgc2VsZWN0b3JzIGFyZSBhbGxvd2VkXG4gICoqKlxuXG5cblByb2JsZW1zIHdpdGggdGhlIGN1cnJlbnQgYXBwcm9hY2guXG5cbjEuIERpcmVjdGlvbiBjYW4gYmUgYXBwbGllZCBvbmx5IG9uIGRvY3VtZW50IGxldmVsLCBiZWNhdXNlIG1peGluIHByZXBlbmRzIHRoZW1lIGNsYXNzLFxud2hpY2ggcGxhY2VkIG9uIHRoZSBib2R5LlxuMi4gKi5jb21wb25lbnQuc2NzcyBzdHlsZXMgc2hvdWxkIGJlIGluIDpob3N0IHNlbGVjdG9yLiBPdGhlcndpc2UgYW5ndWxhciB3aWxsIGFkZCBob3N0XG5hdHRyaWJ1dGUgdG8gW2Rpcj1ydGxdIGF0dHJpYnV0ZSBhcyB3ZWxsLlxuXG5cbkdlbmVyYWwgcHJvYmxlbXMuXG5cbkx0ciBpcyBkZWZhdWx0IGRvY3VtZW50IGRpcmVjdGlvbiwgYnV0IGZvciBwcm9wZXIgd29yayBvZiBuYi1sdHIgKG1lYW5zIGx0ciBvbmx5KSxcbltkaXI9bHRyXSBzaG91bGQgYmUgc3BlY2lmaWVkIGF0IGxlYXN0IHNvbWV3aGVyZS4gJzpub3QoW2Rpcj1ydGxdJyBub3QgYXBwbGljYWJsZSBoZXJlLFxuYmVjYXVzZSBpdCdzIHNhdGlzZnkgYW55IHBhcmVudCwgdGhhdCBkb24ndCBoYXZlIFtkaXI9cnRsXSBhdHRyaWJ1dGUuXG5QcmV2aW91cyBhcHByb2FjaCB3YXMgdG8gdXNlIHNpbmdsZSBydGwgbWl4aW4gYW5kIHJlc2V0IGx0ciBwcm9wZXJ0aWVzIHRvIGluaXRpYWwgdmFsdWUuXG5CdXQgc29tZXRpbWVzIGl0J3MgaGFyZCB0byBmaW5kLCB3aGF0IHRoZSBwcmV2aW91cyB2YWx1ZSBzaG91bGQgYmUuIEFuZCBzdWNoIG1peGluIGNhbGwgbG9va3MgdG9vIHZlcmJvc2UuXG4qL1xuLyoqXG4gKiBAbGljZW5zZVxuICogQ29weXJpZ2h0IEFrdmVvLiBBbGwgUmlnaHRzIFJlc2VydmVkLlxuICogTGljZW5zZWQgdW5kZXIgdGhlIE1JVCBMaWNlbnNlLiBTZWUgTGljZW5zZS50eHQgaW4gdGhlIHByb2plY3Qgcm9vdCBmb3IgbGljZW5zZSBpbmZvcm1hdGlvbi5cbiAqL1xuLyoqXG4gKiBAbGljZW5zZVxuICogQ29weXJpZ2h0IEFrdmVvLiBBbGwgUmlnaHRzIFJlc2VydmVkLlxuICogTGljZW5zZWQgdW5kZXIgdGhlIE1JVCBMaWNlbnNlLiBTZWUgTGljZW5zZS50eHQgaW4gdGhlIHByb2plY3Qgcm9vdCBmb3IgbGljZW5zZSBpbmZvcm1hdGlvbi5cbiAqL1xuLyoqXG4gKiBAbGljZW5zZVxuICogQ29weXJpZ2h0IEFrdmVvLiBBbGwgUmlnaHRzIFJlc2VydmVkLlxuICogTGljZW5zZWQgdW5kZXIgdGhlIE1JVCBMaWNlbnNlLiBTZWUgTGljZW5zZS50eHQgaW4gdGhlIHByb2plY3Qgcm9vdCBmb3IgbGljZW5zZSBpbmZvcm1hdGlvbi5cbiAqL1xuLyoqXG4gKiBUaGlzIG1peGluIGdlbmVyYXRlcyBrZXlmYW1lcy5cbiAqIEJlY2F1c2Ugb2YgYWxsIGtleWZyYW1lcyBjYW4ndCBiZSBzY29wZWQsXG4gKiB3ZSBuZWVkIHRvIHB1dHMgdW5pcXVlIG5hbWUgaW4gZWFjaCBidG4tcHVsc2UgY2FsbC5cbiAqL1xuLypcblxuQWNjb3JkaW5nIHRvIHRoZSBzcGVjaWZpY2F0aW9uIChodHRwczovL3d3dy53My5vcmcvVFIvY3NzLXNjb3BpbmctMS8jaG9zdC1zZWxlY3Rvcilcbjpob3N0IGFuZCA6aG9zdC1jb250ZXh0IGFyZSBwc2V1ZG8tY2xhc3Nlcy4gU28gd2UgYXNzdW1lIHRoZXkgY291bGQgYmUgY29tYmluZWQsXG5saWtlIG90aGVyIHBzZXVkby1jbGFzc2VzLCBldmVuIHNhbWUgb25lcy5cbkZvciBleGFtcGxlOiAnOm50aC1vZi10eXBlKDJuKTpudGgtb2YtdHlwZShldmVuKScuXG5cbklkZWFsIHNvbHV0aW9uIHdvdWxkIGJlIHRvIHByZXBlbmQgYW55IHNlbGVjdG9yIHdpdGggOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pLlxuVGhlbiBuZWJ1bGFyIGNvbXBvbmVudHMgd2lsbCBiZWhhdmUgYXMgYW4gaHRtbCBlbGVtZW50IGFuZCByZXNwb25kIHRvIFtkaXJdIGF0dHJpYnV0ZSBvbiBhbnkgbGV2ZWwsXG5zbyBkaXJlY3Rpb24gY291bGQgYmUgb3ZlcnJpZGRlbiBvbiBhbnkgY29tcG9uZW50IGxldmVsLlxuXG5JbXBsZW1lbnRhdGlvbiBjb2RlOlxuXG5AbWl4aW4gbmItcnRsKCkge1xuICAvLyBhZGQgIyB0byBzY3NzIGludGVycG9sYXRpb24gc3RhdGVtZW50LlxuICAvLyBpdCB3b3JrcyBpbiBjb21tZW50cyBhbmQgd2UgY2FuJ3QgdXNlIGl0IGhlcmVcbiAgQGF0LXJvb3Qge3NlbGVjdG9yLWFwcGVuZCgnOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pJywgJil9IHtcbiAgICBAY29udGVudDtcbiAgfVxufVxuXG5BbmQgd2hlbiB3ZSBjYWxsIGl0IHNvbWV3aGVyZTpcblxuOmhvc3Qge1xuICAuc29tZS1jbGFzcyB7XG4gICAgQGluY2x1ZGUgbmItcnRsKCkge1xuICAgICAgLi4uXG4gICAgfVxuICB9XG59XG46aG9zdC1jb250ZXh0KC4uLikge1xuICAuc29tZS1jbGFzcyB7XG4gICAgQGluY2x1ZGUgbmItcnRsKCkge1xuICAgICAgLi4uXG4gICAgfVxuICB9XG59XG5cblJlc3VsdCB3aWxsIGxvb2sgbGlrZTpcblxuOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pOmhvc3QgLnNvbWUtY2xhc3Mge1xuICAuLi5cbn1cbjpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKTpob3N0LWNvbnRleHQoLi4uKSAuc29tZS1jbGFzcyB7XG4gIC4uLlxufVxuXG4qXG4gIFNpZGUgbm90ZTpcbiAgOmhvc3QtY29udGV4dCgpOmhvc3Qgc2VsZWN0b3IgYXJlIHZhbGlkLiBodHRwczovL2xpc3RzLnczLm9yZy9BcmNoaXZlcy9QdWJsaWMvd3d3LXN0eWxlLzIwMTVGZWIvMDMwNS5odG1sXG5cbiAgOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pOmhvc3QtY29udGV4dCguLi4pIHNob3VsZCBtYXRjaCBhbnkgcGVybXV0YXRpb24sXG4gIHNvIG9yZGVyIGlzIG5vdCBpbXBvcnRhbnQuXG4qXG5cblxuQ3VycmVudGx5LCB0aGVyZSdyZSB0d28gcHJvYmxlbXMgd2l0aCB0aGlzIGFwcHJvYWNoOlxuXG5GaXJzdCwgaXMgdGhhdCB3ZSBjYW4ndCBjb21iaW5lIDpob3N0LCA6aG9zdC1jb250ZXh0LiBBbmd1bGFyIGJ1Z3MgIzE0MzQ5LCAjMTkxOTkuXG5Gb3IgdGhlIG1vbWVudCBvZiB3cml0aW5nLCB0aGUgb25seSBwb3NzaWJsZSB3YXkgaXM6XG46aG9zdCB7XG4gIDpob3N0LWNvbnRleHQoLi4uKSB7XG4gICAgLi4uXG4gIH1cbn1cbkl0IGRvZXNuJ3Qgd29yayBmb3IgdXMgYmVjYXVzZSBtaXhpbiBjb3VsZCBiZSBjYWxsZWQgc29tZXdoZXJlIGRlZXBlciwgbGlrZTpcbjpob3N0IHtcbiAgcCB7XG4gICAgQGluY2x1ZGUgbmItcnRsKCkgeyAuLi4gfVxuICB9XG59XG5XZSBhcmUgbm90IGFibGUgdG8gZ28gdXAgdG8gOmhvc3QgbGV2ZWwgdG8gcGxhY2UgY29udGVudCBwYXNzZWQgdG8gbWl4aW4uXG5cblRoZSBzZWNvbmQgcHJvYmxlbSBpcyB0aGF0IHdlIG9ubHkgY2FuIGJlIHN1cmUgdGhhdCB3ZSBhcHBlbmRpbmcgOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pIHRvIGFub3RoZXJcbjpob3N0Lzpob3N0LWNvbnRleHQgcHNldWRvLWNsYXNzIHdoZW4gY2FsbGVkIGluIHRoZW1lIGZpbGVzICgqLnRoZW1lLnNjc3MpLlxuICAqXG4gICAgU2lkZSBub3RlOlxuICAgIEN1cnJlbnRseSwgbmItaW5zdGFsbC1jb21wb25lbnQgdXNlcyBhbm90aGVyIGFwcHJvYWNoIHdoZXJlIDpob3N0IHByZXBlbmRlZCB3aXRoIHRoZSB0aGVtZSBuYW1lXG4gICAgKGh0dHBzOi8vZ2l0aHViLmNvbS9hbmd1bGFyL2FuZ3VsYXIvYmxvYi81Yjk2MDc4NjI0YjBhNDc2MGYyZGJjZjZmZGYwYmQ2Mjc5MWJlNWJiL3BhY2thZ2VzL2NvbXBpbGVyL3NyYy9zaGFkb3dfY3NzLnRzI0w0NDEpLFxuICAgIGJ1dCBpdCB3YXMgbWFkZSB0byBiZSBhYmxlIHRvIHVzZSBjdXJyZW50IHJlYWxpemF0aW9uIG9mIHJ0bCBhbmQgaXQgY2FuIGJlIHJld3JpdHRlbiBiYWNrIHRvXG4gICAgOmhvc3QtY29udGV4dCgkdGhlbWUpIG9uY2Ugd2Ugd2lsbCBiZSBhYmxlIHRvIHVzZSBtdWx0aXBsZSBzaGFkb3cgc2VsZWN0b3JzLlxuICAqXG5CdXQgd2hlbiBpdCdzIGNhbGxlZCBpbiAqLmNvbXBvbmVudC5zY3NzIHdlIGNhbid0IGJlIHN1cmUsIHRoYXQgc2VsZWN0b3Igc3RhcnRzIHdpdGggOmhvc3QvOmhvc3QtY29udGV4dCxcbmJlY2F1c2UgYW5ndWxhciBhbGxvd3Mgb21pdHRpbmcgcHNldWRvLWNsYXNzZXMgaWYgd2UgZG9uJ3QgbmVlZCB0byBzdHlsZSA6aG9zdCBjb21wb25lbnQgaXRzZWxmLlxuV2UgY2FuIGJyZWFrIHN1Y2ggc2VsZWN0b3JzLCBieSBqdXN0IGFwcGVuZGluZyA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkgdG8gdGhlbS5cbiAgKioqXG4gICAgUG9zc2libGUgc29sdXRpb25cbiAgICBjaGVjayBpZiB3ZSBpbiB0aGVtZSBieSBzb21lIHRoZW1lIHZhcmlhYmxlcyBhbmQgaWYgc28gYXBwZW5kLCBvdGhlcndpc2UgbmVzdCBsaWtlXG4gICAgQGF0LXJvb3QgOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pIHtcbiAgICAgIC8vIGFkZCAjIHRvIHNjc3MgaW50ZXJwb2xhdGlvbiBzdGF0ZW1lbnQuXG4gICAgICAvLyBpdCB3b3JrcyBpbiBjb21tZW50cyBhbmQgd2UgY2FuJ3QgdXNlIGl0IGhlcmVcbiAgICAgIHsmfSB7XG4gICAgICAgIEBjb250ZW50O1xuICAgICAgfVxuICAgIH1cbiAgICBXaGF0IGlmIDpob3N0IHNwZWNpZmllZD8gQ2FuIHdlIGFkZCBzcGFjZSBpbiA6aG9zdC1jb250ZXh0KC4uLikgOmhvc3Q/XG4gICAgT3IgbWF5YmUgYWRkIDpob3N0IHNlbGVjdG9yIGFueXdheT8gSWYgbXVsdGlwbGUgOmhvc3Qgc2VsZWN0b3JzIGFyZSBhbGxvd2VkXG4gICoqKlxuXG5cblByb2JsZW1zIHdpdGggdGhlIGN1cnJlbnQgYXBwcm9hY2guXG5cbjEuIERpcmVjdGlvbiBjYW4gYmUgYXBwbGllZCBvbmx5IG9uIGRvY3VtZW50IGxldmVsLCBiZWNhdXNlIG1peGluIHByZXBlbmRzIHRoZW1lIGNsYXNzLFxud2hpY2ggcGxhY2VkIG9uIHRoZSBib2R5LlxuMi4gKi5jb21wb25lbnQuc2NzcyBzdHlsZXMgc2hvdWxkIGJlIGluIDpob3N0IHNlbGVjdG9yLiBPdGhlcndpc2UgYW5ndWxhciB3aWxsIGFkZCBob3N0XG5hdHRyaWJ1dGUgdG8gW2Rpcj1ydGxdIGF0dHJpYnV0ZSBhcyB3ZWxsLlxuXG5cbkdlbmVyYWwgcHJvYmxlbXMuXG5cbkx0ciBpcyBkZWZhdWx0IGRvY3VtZW50IGRpcmVjdGlvbiwgYnV0IGZvciBwcm9wZXIgd29yayBvZiBuYi1sdHIgKG1lYW5zIGx0ciBvbmx5KSxcbltkaXI9bHRyXSBzaG91bGQgYmUgc3BlY2lmaWVkIGF0IGxlYXN0IHNvbWV3aGVyZS4gJzpub3QoW2Rpcj1ydGxdJyBub3QgYXBwbGljYWJsZSBoZXJlLFxuYmVjYXVzZSBpdCdzIHNhdGlzZnkgYW55IHBhcmVudCwgdGhhdCBkb24ndCBoYXZlIFtkaXI9cnRsXSBhdHRyaWJ1dGUuXG5QcmV2aW91cyBhcHByb2FjaCB3YXMgdG8gdXNlIHNpbmdsZSBydGwgbWl4aW4gYW5kIHJlc2V0IGx0ciBwcm9wZXJ0aWVzIHRvIGluaXRpYWwgdmFsdWUuXG5CdXQgc29tZXRpbWVzIGl0J3MgaGFyZCB0byBmaW5kLCB3aGF0IHRoZSBwcmV2aW91cyB2YWx1ZSBzaG91bGQgYmUuIEFuZCBzdWNoIG1peGluIGNhbGwgbG9va3MgdG9vIHZlcmJvc2UuXG4qL1xuLyoqXG4gKiBAbGljZW5zZVxuICogQ29weXJpZ2h0IEFrdmVvLiBBbGwgUmlnaHRzIFJlc2VydmVkLlxuICogTGljZW5zZWQgdW5kZXIgdGhlIE1JVCBMaWNlbnNlLiBTZWUgTGljZW5zZS50eHQgaW4gdGhlIHByb2plY3Qgcm9vdCBmb3IgbGljZW5zZSBpbmZvcm1hdGlvbi5cbiAqL1xuLypcbiAgICAgIDpob3N0IGNhbiBiZSBwcmVmaXhlZFxuICAgICAgaHR0cHM6Ly9naXRodWIuY29tL2FuZ3VsYXIvYW5ndWxhci9ibG9iLzhkMGVlMzQ5MzlmMTRjMDc4NzZkMjIyYzI1YjQwNWVkNDU4YTM0ZDMvcGFja2FnZXMvY29tcGlsZXIvc3JjL3NoYWRvd19jc3MudHMjTDQ0MVxuXG4gICAgICBXZSBoYXZlIHRvIHVzZSA6aG9zdCBpbnN0ZWQgb2YgOmhvc3QtY29udGV4dCgkdGhlbWUpLCB0byBiZSBhYmxlIHRvIHByZWZpeCB0aGVtZSBjbGFzc1xuICAgICAgd2l0aCBzb21ldGhpbmcgZGVmaW5lZCBpbnNpZGUgb2YgQGNvbnRlbnQsIGJ5IHByZWZpeGluZyAmLlxuICAgICAgRm9yIGV4YW1wbGUgdGhpcyBzY3NzIGNvZGU6XG4gICAgICAgIC5uYi10aGVtZS1kZWZhdWx0IHtcbiAgICAgICAgICAuc29tZS1zZWxlY3RvciAmIHtcbiAgICAgICAgICAgIC4uLlxuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgV2lsbCByZXN1bHQgaW4gbmV4dCBjc3M6XG4gICAgICAgIC5zb21lLXNlbGVjdG9yIC5uYi10aGVtZS1kZWZhdWx0IHtcbiAgICAgICAgICAuLi5cbiAgICAgICAgfVxuXG4gICAgICBJdCBkb2Vzbid0IHdvcmsgd2l0aCA6aG9zdC1jb250ZXh0IGJlY2F1c2UgYW5ndWxhciBzcGxpdHRpbmcgaXQgaW4gdHdvIHNlbGVjdG9ycyBhbmQgcmVtb3Zlc1xuICAgICAgcHJlZml4IGluIG9uZSBvZiB0aGUgc2VsZWN0b3JzLlxuICAgICovXG4ubmItdGhlbWUtZGVmYXVsdCA6aG9zdCBidXR0b24ge1xuICBmbG9hdDogcmlnaHQ7IH1cblxuLm5iLXRoZW1lLWRlZmF1bHQgOmhvc3QgL2RlZXAvIG5nMi1zbWFydC10YWJsZSBpIHtcbiAgY29sb3I6ICMyMTI1MjkgIWltcG9ydGFudDsgfVxuXG4ubmItdGhlbWUtZGVmYXVsdCA6aG9zdCAvZGVlcC8gbmcyLXNtYXJ0LXRhYmxlIG5hdiB1bCBsaSBhIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzIxMjUyOSAhaW1wb3J0YW50OyB9XG5cbi5uYi10aGVtZS1kZWZhdWx0IDpob3N0IC9kZWVwLyBuZzItc21hcnQtdGFibGUgLm5nMi1zbWFydC10aXRsZSB7XG4gIGNvbG9yOiB3aGl0ZSAhaW1wb3J0YW50OyB9XG5cbi5uYi10aGVtZS1kZWZhdWx0IDpob3N0IG5iLWNhcmQge1xuICBib3JkZXI6IDBweCBzb2xpZCAjZDVkYmUwOyB9XG5cbi5uYi10aGVtZS1kZWZhdWx0IDpob3N0IC5mb3JtLWNoZWNrLWlucHV0IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBtYXJnaW4tdG9wOiAtMC4wNXJlbTtcbiAgbWFyZ2luLWxlZnQ6IC0xLjVyZW07XG4gIHdpZHRoOiAyMHB4O1xuICBoZWlnaHQ6IDIwcHg7IH1cblxuLm5iLXRoZW1lLWRlZmF1bHQgOmhvc3QgL2RlZXAvIG5nMi1zbWFydC10YWJsZSBpIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlICFpbXBvcnRhbnQ7XG4gIHRvcDogMHB4ICFpbXBvcnRhbnQ7IH1cblxuLm5iLXRoZW1lLWRlZmF1bHQgOmhvc3QgL2RlZXAvIG5nMi1zdC1hY3Rpb25zIGkge1xuICBjb2xvcjogd2hpdGUgIWltcG9ydGFudDsgfVxuXG4ubmItdGhlbWUtZGVmYXVsdCA6aG9zdCAvZGVlcC8gbmcyLXNtYXJ0LXRhYmxlIHNlbGVjdCB7XG4gIGhlaWdodDogMi42cmVtICFpbXBvcnRhbnQ7XG4gIHBhZGRpbmctdG9wOiA1cHggIWltcG9ydGFudDsgfVxuXG5AbWVkaWEgKG1pbi13aWR0aDogNTc2cHgpIHtcbiAgLm5iLXRoZW1lLWRlZmF1bHQgOmhvc3QgL2RlZXAvIG1vZGFsLm1vZGFsc3MgLm1vZGFsLWRpYWxvZyB7XG4gICAgbWF4LXdpZHRoOiA3MCUgIWltcG9ydGFudDsgfVxuICAubmItdGhlbWUtZGVmYXVsdCA6aG9zdCAvZGVlcC8gbW9kYWwubW9kYWxzcyAubW9kYWwtZGlhbG9nIC5tb2RhbC1ib2R5IHtcbiAgICBoZWlnaHQ6IGF1dG87IH1cbiAgLm5iLXRoZW1lLWRlZmF1bHQgOmhvc3QgL2RlZXAvIG1vZGFsLm1vZGFsc3MgLm1vZGFsLWRpYWxvZyAubW9kYWwtZm9vdGVyIHtcbiAgICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7IH0gfVxuXG4ubmItdGhlbWUtZGVmYXVsdCA6aG9zdCAvZGVlcC9uZzItc21hcnQtdGFibGUgdGFibGUgdGJvZHkgdHI6bnRoLWNoaWxkKG9kZCkge1xuICBiYWNrZ3JvdW5kOiB3aGl0ZSAhaW1wb3J0YW50OyB9XG5cbi5uYi10aGVtZS1kZWZhdWx0IDpob3N0IC9kZWVwL25nMi1zbWFydC10YWJsZSAubmcyLXNtYXJ0LXJvdyB0ZCBuZzItc21hcnQtdGFibGUtY2VsbCBpbnB1dC1lZGl0b3IgaW5wdXQge1xuICBib3JkZXI6ICMxZDhlY2Ugc29saWQgIWltcG9ydGFudDsgfVxuXG4ubmItdGhlbWUtZGVmYXVsdCA6aG9zdCAvZGVlcC9uZzItc21hcnQtdGFibGUgLm5nMi1zbWFydC1yb3cgdGQgbmcyLXNtYXJ0LXRhYmxlLWNlbGwgc2VsZWN0LWVkaXRvciBzZWxlY3Qge1xuICBib3JkZXI6ICMxZDhlY2Ugc29saWQgIWltcG9ydGFudDsgfVxuXG4ubmItdGhlbWUtZGVmYXVsdCA6aG9zdCAvZGVlcC8gbmcyLXN0LXRib2R5LWVkaXQtZGVsZXRlIHtcbiAgZGlzcGxheTogLXdlYmtpdC1pbmxpbmUtYm94ICFpbXBvcnRhbnQ7XG4gIGhlaWdodDogMHB4ICFpbXBvcnRhbnQ7XG4gIGZsb2F0OiBsZWZ0O1xuICBtYXJnaW4tbGVmdDogMzZweDtcbiAgbWFyZ2luLXRvcDogLTE1cHggIWltcG9ydGFudDtcbiAgLyogbWFyZ2luOiBhdXRvOyAqLyB9XG5cbi5uYi10aGVtZS1kZWZhdWx0IDpob3N0IC9kZWVwLyBuZzItc3QtdGJvZHktY3VzdG9tIGEubmcyLXNtYXJ0LWFjdGlvbi5uZzItc21hcnQtYWN0aW9uLWN1c3RvbS1jdXN0b20ge1xuICB3aWR0aDogNTBweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDAuMWVtOyB9XG5cbi5uYi10aGVtZS1kZWZhdWx0IDpob3N0IC9kZWVwLyBuZzItc3QtdGJvZHktY3VzdG9tIGEubmcyLXNtYXJ0LWFjdGlvbi5uZzItc21hcnQtYWN0aW9uLWN1c3RvbS1jdXN0b20gaW1nIHtcbiAgd2lkdGg6IDU3JSAhaW1wb3J0YW50O1xuICBtYXJnaW4tbGVmdDogMzRweDsgfVxuXG4ubmItdGhlbWUtZGVmYXVsdCA6aG9zdCAvZGVlcC8gbmcyLXN0LXRib2R5LWN1c3RvbSBhLm5nMi1zbWFydC1hY3Rpb24ubmcyLXNtYXJ0LWFjdGlvbi1jdXN0b20tY3VzdG9tOmhvdmVyIHtcbiAgY29sb3I6ICM1ZGNmZTM7IH1cblxuLm5iLXRoZW1lLWRlZmF1bHQgOmhvc3QgL2RlZXAvIG5nMi1zdC10Ym9keS1lZGl0LWRlbGV0ZSBpIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlICFpbXBvcnRhbnQ7XG4gIHRvcDogLTE0cHg7XG4gIG1hcmdpbi1sZWZ0OiA0MHB4OyB9XG5cbi8qXG4gICAgICA6aG9zdCBjYW4gYmUgcHJlZml4ZWRcbiAgICAgIGh0dHBzOi8vZ2l0aHViLmNvbS9hbmd1bGFyL2FuZ3VsYXIvYmxvYi84ZDBlZTM0OTM5ZjE0YzA3ODc2ZDIyMmMyNWI0MDVlZDQ1OGEzNGQzL3BhY2thZ2VzL2NvbXBpbGVyL3NyYy9zaGFkb3dfY3NzLnRzI0w0NDFcblxuICAgICAgV2UgaGF2ZSB0byB1c2UgOmhvc3QgaW5zdGVkIG9mIDpob3N0LWNvbnRleHQoJHRoZW1lKSwgdG8gYmUgYWJsZSB0byBwcmVmaXggdGhlbWUgY2xhc3NcbiAgICAgIHdpdGggc29tZXRoaW5nIGRlZmluZWQgaW5zaWRlIG9mIEBjb250ZW50LCBieSBwcmVmaXhpbmcgJi5cbiAgICAgIEZvciBleGFtcGxlIHRoaXMgc2NzcyBjb2RlOlxuICAgICAgICAubmItdGhlbWUtZGVmYXVsdCB7XG4gICAgICAgICAgLnNvbWUtc2VsZWN0b3IgJiB7XG4gICAgICAgICAgICAuLi5cbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIFdpbGwgcmVzdWx0IGluIG5leHQgY3NzOlxuICAgICAgICAuc29tZS1zZWxlY3RvciAubmItdGhlbWUtZGVmYXVsdCB7XG4gICAgICAgICAgLi4uXG4gICAgICAgIH1cblxuICAgICAgSXQgZG9lc24ndCB3b3JrIHdpdGggOmhvc3QtY29udGV4dCBiZWNhdXNlIGFuZ3VsYXIgc3BsaXR0aW5nIGl0IGluIHR3byBzZWxlY3RvcnMgYW5kIHJlbW92ZXNcbiAgICAgIHByZWZpeCBpbiBvbmUgb2YgdGhlIHNlbGVjdG9ycy5cbiAgICAqL1xuLm5iLXRoZW1lLWNvc21pYyA6aG9zdCBidXR0b24ge1xuICBmbG9hdDogcmlnaHQ7IH1cblxuLm5iLXRoZW1lLWNvc21pYyA6aG9zdCAvZGVlcC8gbmcyLXNtYXJ0LXRhYmxlIGkge1xuICBjb2xvcjogIzIxMjUyOSAhaW1wb3J0YW50OyB9XG5cbi5uYi10aGVtZS1jb3NtaWMgOmhvc3QgL2RlZXAvIG5nMi1zbWFydC10YWJsZSBuYXYgdWwgbGkgYSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMyMTI1MjkgIWltcG9ydGFudDsgfVxuXG4ubmItdGhlbWUtY29zbWljIDpob3N0IC9kZWVwLyBuZzItc21hcnQtdGFibGUgLm5nMi1zbWFydC10aXRsZSB7XG4gIGNvbG9yOiB3aGl0ZSAhaW1wb3J0YW50OyB9XG5cbi5uYi10aGVtZS1jb3NtaWMgOmhvc3QgbmItY2FyZCB7XG4gIGJvcmRlcjogMHB4IHNvbGlkICNkNWRiZTA7IH1cblxuLm5iLXRoZW1lLWNvc21pYyA6aG9zdCAuZm9ybS1jaGVjay1pbnB1dCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbWFyZ2luLXRvcDogLTAuMDVyZW07XG4gIG1hcmdpbi1sZWZ0OiAtMS41cmVtO1xuICB3aWR0aDogMjBweDtcbiAgaGVpZ2h0OiAyMHB4OyB9XG5cbi5uYi10aGVtZS1jb3NtaWMgOmhvc3QgL2RlZXAvIG5nMi1zbWFydC10YWJsZSBpIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlICFpbXBvcnRhbnQ7XG4gIHRvcDogMHB4ICFpbXBvcnRhbnQ7IH1cblxuLm5iLXRoZW1lLWNvc21pYyA6aG9zdCAvZGVlcC8gbmcyLXN0LWFjdGlvbnMgaSB7XG4gIGNvbG9yOiB3aGl0ZSAhaW1wb3J0YW50OyB9XG5cbi5uYi10aGVtZS1jb3NtaWMgOmhvc3QgL2RlZXAvIG5nMi1zbWFydC10YWJsZSBzZWxlY3Qge1xuICBoZWlnaHQ6IDIuNnJlbSAhaW1wb3J0YW50O1xuICBwYWRkaW5nLXRvcDogNXB4ICFpbXBvcnRhbnQ7IH1cblxuQG1lZGlhIChtaW4td2lkdGg6IDU3NnB4KSB7XG4gIC5uYi10aGVtZS1jb3NtaWMgOmhvc3QgL2RlZXAvIG1vZGFsLm1vZGFsc3MgLm1vZGFsLWRpYWxvZyB7XG4gICAgbWF4LXdpZHRoOiA3MCUgIWltcG9ydGFudDsgfVxuICAubmItdGhlbWUtY29zbWljIDpob3N0IC9kZWVwLyBtb2RhbC5tb2RhbHNzIC5tb2RhbC1kaWFsb2cgLm1vZGFsLWJvZHkge1xuICAgIGhlaWdodDogYXV0bzsgfVxuICAubmItdGhlbWUtY29zbWljIDpob3N0IC9kZWVwLyBtb2RhbC5tb2RhbHNzIC5tb2RhbC1kaWFsb2cgLm1vZGFsLWZvb3RlciB7XG4gICAgZGlzcGxheTogbm9uZSAhaW1wb3J0YW50OyB9IH1cblxuLm5iLXRoZW1lLWNvc21pYyA6aG9zdCAvZGVlcC9uZzItc21hcnQtdGFibGUgdGFibGUgdGJvZHkgdHI6bnRoLWNoaWxkKG9kZCkge1xuICBiYWNrZ3JvdW5kOiB3aGl0ZSAhaW1wb3J0YW50OyB9XG5cbi5uYi10aGVtZS1jb3NtaWMgOmhvc3QgL2RlZXAvbmcyLXNtYXJ0LXRhYmxlIC5uZzItc21hcnQtcm93IHRkIG5nMi1zbWFydC10YWJsZS1jZWxsIGlucHV0LWVkaXRvciBpbnB1dCB7XG4gIGJvcmRlcjogIzFkOGVjZSBzb2xpZCAhaW1wb3J0YW50OyB9XG5cbi5uYi10aGVtZS1jb3NtaWMgOmhvc3QgL2RlZXAvbmcyLXNtYXJ0LXRhYmxlIC5uZzItc21hcnQtcm93IHRkIG5nMi1zbWFydC10YWJsZS1jZWxsIHNlbGVjdC1lZGl0b3Igc2VsZWN0IHtcbiAgYm9yZGVyOiAjMWQ4ZWNlIHNvbGlkICFpbXBvcnRhbnQ7IH1cblxuLm5iLXRoZW1lLWNvc21pYyA6aG9zdCAvZGVlcC8gbmcyLXN0LXRib2R5LWVkaXQtZGVsZXRlIHtcbiAgZGlzcGxheTogLXdlYmtpdC1pbmxpbmUtYm94ICFpbXBvcnRhbnQ7XG4gIGhlaWdodDogMHB4ICFpbXBvcnRhbnQ7XG4gIGZsb2F0OiBsZWZ0O1xuICBtYXJnaW4tbGVmdDogMzZweDtcbiAgbWFyZ2luLXRvcDogLTE1cHggIWltcG9ydGFudDtcbiAgLyogbWFyZ2luOiBhdXRvOyAqLyB9XG5cbi5uYi10aGVtZS1jb3NtaWMgOmhvc3QgL2RlZXAvIG5nMi1zdC10Ym9keS1jdXN0b20gYS5uZzItc21hcnQtYWN0aW9uLm5nMi1zbWFydC1hY3Rpb24tY3VzdG9tLWN1c3RvbSB7XG4gIHdpZHRoOiA1MHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMC4xZW07IH1cblxuLm5iLXRoZW1lLWNvc21pYyA6aG9zdCAvZGVlcC8gbmcyLXN0LXRib2R5LWN1c3RvbSBhLm5nMi1zbWFydC1hY3Rpb24ubmcyLXNtYXJ0LWFjdGlvbi1jdXN0b20tY3VzdG9tIGltZyB7XG4gIHdpZHRoOiA1NyUgIWltcG9ydGFudDtcbiAgbWFyZ2luLWxlZnQ6IDM0cHg7IH1cblxuLm5iLXRoZW1lLWNvc21pYyA6aG9zdCAvZGVlcC8gbmcyLXN0LXRib2R5LWN1c3RvbSBhLm5nMi1zbWFydC1hY3Rpb24ubmcyLXNtYXJ0LWFjdGlvbi1jdXN0b20tY3VzdG9tOmhvdmVyIHtcbiAgY29sb3I6ICM1ZGNmZTM7IH1cblxuLm5iLXRoZW1lLWNvc21pYyA6aG9zdCAvZGVlcC8gbmcyLXN0LXRib2R5LWVkaXQtZGVsZXRlIGkge1xuICBwb3NpdGlvbjogcmVsYXRpdmUgIWltcG9ydGFudDtcbiAgdG9wOiAtMTRweDtcbiAgbWFyZ2luLWxlZnQ6IDQwcHg7IH1cblxuLypcbiAgICAgIDpob3N0IGNhbiBiZSBwcmVmaXhlZFxuICAgICAgaHR0cHM6Ly9naXRodWIuY29tL2FuZ3VsYXIvYW5ndWxhci9ibG9iLzhkMGVlMzQ5MzlmMTRjMDc4NzZkMjIyYzI1YjQwNWVkNDU4YTM0ZDMvcGFja2FnZXMvY29tcGlsZXIvc3JjL3NoYWRvd19jc3MudHMjTDQ0MVxuXG4gICAgICBXZSBoYXZlIHRvIHVzZSA6aG9zdCBpbnN0ZWQgb2YgOmhvc3QtY29udGV4dCgkdGhlbWUpLCB0byBiZSBhYmxlIHRvIHByZWZpeCB0aGVtZSBjbGFzc1xuICAgICAgd2l0aCBzb21ldGhpbmcgZGVmaW5lZCBpbnNpZGUgb2YgQGNvbnRlbnQsIGJ5IHByZWZpeGluZyAmLlxuICAgICAgRm9yIGV4YW1wbGUgdGhpcyBzY3NzIGNvZGU6XG4gICAgICAgIC5uYi10aGVtZS1kZWZhdWx0IHtcbiAgICAgICAgICAuc29tZS1zZWxlY3RvciAmIHtcbiAgICAgICAgICAgIC4uLlxuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgV2lsbCByZXN1bHQgaW4gbmV4dCBjc3M6XG4gICAgICAgIC5zb21lLXNlbGVjdG9yIC5uYi10aGVtZS1kZWZhdWx0IHtcbiAgICAgICAgICAuLi5cbiAgICAgICAgfVxuXG4gICAgICBJdCBkb2Vzbid0IHdvcmsgd2l0aCA6aG9zdC1jb250ZXh0IGJlY2F1c2UgYW5ndWxhciBzcGxpdHRpbmcgaXQgaW4gdHdvIHNlbGVjdG9ycyBhbmQgcmVtb3Zlc1xuICAgICAgcHJlZml4IGluIG9uZSBvZiB0aGUgc2VsZWN0b3JzLlxuICAgICovXG4ubmItdGhlbWUtY29ycG9yYXRlIDpob3N0IGJ1dHRvbiB7XG4gIGZsb2F0OiByaWdodDsgfVxuXG4ubmItdGhlbWUtY29ycG9yYXRlIDpob3N0IC9kZWVwLyBuZzItc21hcnQtdGFibGUgaSB7XG4gIGNvbG9yOiAjMjEyNTI5ICFpbXBvcnRhbnQ7IH1cblxuLm5iLXRoZW1lLWNvcnBvcmF0ZSA6aG9zdCAvZGVlcC8gbmcyLXNtYXJ0LXRhYmxlIG5hdiB1bCBsaSBhIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzIxMjUyOSAhaW1wb3J0YW50OyB9XG5cbi5uYi10aGVtZS1jb3Jwb3JhdGUgOmhvc3QgL2RlZXAvIG5nMi1zbWFydC10YWJsZSAubmcyLXNtYXJ0LXRpdGxlIHtcbiAgY29sb3I6IHdoaXRlICFpbXBvcnRhbnQ7IH1cblxuLm5iLXRoZW1lLWNvcnBvcmF0ZSA6aG9zdCBuYi1jYXJkIHtcbiAgYm9yZGVyOiAwcHggc29saWQgI2Q1ZGJlMDsgfVxuXG4ubmItdGhlbWUtY29ycG9yYXRlIDpob3N0IC5mb3JtLWNoZWNrLWlucHV0IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBtYXJnaW4tdG9wOiAtMC4wNXJlbTtcbiAgbWFyZ2luLWxlZnQ6IC0xLjVyZW07XG4gIHdpZHRoOiAyMHB4O1xuICBoZWlnaHQ6IDIwcHg7IH1cblxuLm5iLXRoZW1lLWNvcnBvcmF0ZSA6aG9zdCAvZGVlcC8gbmcyLXNtYXJ0LXRhYmxlIGkge1xuICBwb3NpdGlvbjogcmVsYXRpdmUgIWltcG9ydGFudDtcbiAgdG9wOiAwcHggIWltcG9ydGFudDsgfVxuXG4ubmItdGhlbWUtY29ycG9yYXRlIDpob3N0IC9kZWVwLyBuZzItc3QtYWN0aW9ucyBpIHtcbiAgY29sb3I6IHdoaXRlICFpbXBvcnRhbnQ7IH1cblxuLm5iLXRoZW1lLWNvcnBvcmF0ZSA6aG9zdCAvZGVlcC8gbmcyLXNtYXJ0LXRhYmxlIHNlbGVjdCB7XG4gIGhlaWdodDogMi42cmVtICFpbXBvcnRhbnQ7XG4gIHBhZGRpbmctdG9wOiA1cHggIWltcG9ydGFudDsgfVxuXG5AbWVkaWEgKG1pbi13aWR0aDogNTc2cHgpIHtcbiAgLm5iLXRoZW1lLWNvcnBvcmF0ZSA6aG9zdCAvZGVlcC8gbW9kYWwubW9kYWxzcyAubW9kYWwtZGlhbG9nIHtcbiAgICBtYXgtd2lkdGg6IDcwJSAhaW1wb3J0YW50OyB9XG4gIC5uYi10aGVtZS1jb3Jwb3JhdGUgOmhvc3QgL2RlZXAvIG1vZGFsLm1vZGFsc3MgLm1vZGFsLWRpYWxvZyAubW9kYWwtYm9keSB7XG4gICAgaGVpZ2h0OiBhdXRvOyB9XG4gIC5uYi10aGVtZS1jb3Jwb3JhdGUgOmhvc3QgL2RlZXAvIG1vZGFsLm1vZGFsc3MgLm1vZGFsLWRpYWxvZyAubW9kYWwtZm9vdGVyIHtcbiAgICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7IH0gfVxuXG4ubmItdGhlbWUtY29ycG9yYXRlIDpob3N0IC9kZWVwL25nMi1zbWFydC10YWJsZSB0YWJsZSB0Ym9keSB0cjpudGgtY2hpbGQob2RkKSB7XG4gIGJhY2tncm91bmQ6IHdoaXRlICFpbXBvcnRhbnQ7IH1cblxuLm5iLXRoZW1lLWNvcnBvcmF0ZSA6aG9zdCAvZGVlcC9uZzItc21hcnQtdGFibGUgLm5nMi1zbWFydC1yb3cgdGQgbmcyLXNtYXJ0LXRhYmxlLWNlbGwgaW5wdXQtZWRpdG9yIGlucHV0IHtcbiAgYm9yZGVyOiAjMWQ4ZWNlIHNvbGlkICFpbXBvcnRhbnQ7IH1cblxuLm5iLXRoZW1lLWNvcnBvcmF0ZSA6aG9zdCAvZGVlcC9uZzItc21hcnQtdGFibGUgLm5nMi1zbWFydC1yb3cgdGQgbmcyLXNtYXJ0LXRhYmxlLWNlbGwgc2VsZWN0LWVkaXRvciBzZWxlY3Qge1xuICBib3JkZXI6ICMxZDhlY2Ugc29saWQgIWltcG9ydGFudDsgfVxuXG4ubmItdGhlbWUtY29ycG9yYXRlIDpob3N0IC9kZWVwLyBuZzItc3QtdGJvZHktZWRpdC1kZWxldGUge1xuICBkaXNwbGF5OiAtd2Via2l0LWlubGluZS1ib3ggIWltcG9ydGFudDtcbiAgaGVpZ2h0OiAwcHggIWltcG9ydGFudDtcbiAgZmxvYXQ6IGxlZnQ7XG4gIG1hcmdpbi1sZWZ0OiAzNnB4O1xuICBtYXJnaW4tdG9wOiAtMTVweCAhaW1wb3J0YW50O1xuICAvKiBtYXJnaW46IGF1dG87ICovIH1cblxuLm5iLXRoZW1lLWNvcnBvcmF0ZSA6aG9zdCAvZGVlcC8gbmcyLXN0LXRib2R5LWN1c3RvbSBhLm5nMi1zbWFydC1hY3Rpb24ubmcyLXNtYXJ0LWFjdGlvbi1jdXN0b20tY3VzdG9tIHtcbiAgd2lkdGg6IDUwcHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAwLjFlbTsgfVxuXG4ubmItdGhlbWUtY29ycG9yYXRlIDpob3N0IC9kZWVwLyBuZzItc3QtdGJvZHktY3VzdG9tIGEubmcyLXNtYXJ0LWFjdGlvbi5uZzItc21hcnQtYWN0aW9uLWN1c3RvbS1jdXN0b20gaW1nIHtcbiAgd2lkdGg6IDU3JSAhaW1wb3J0YW50O1xuICBtYXJnaW4tbGVmdDogMzRweDsgfVxuXG4ubmItdGhlbWUtY29ycG9yYXRlIDpob3N0IC9kZWVwLyBuZzItc3QtdGJvZHktY3VzdG9tIGEubmcyLXNtYXJ0LWFjdGlvbi5uZzItc21hcnQtYWN0aW9uLWN1c3RvbS1jdXN0b206aG92ZXIge1xuICBjb2xvcjogIzVkY2ZlMzsgfVxuXG4ubmItdGhlbWUtY29ycG9yYXRlIDpob3N0IC9kZWVwLyBuZzItc3QtdGJvZHktZWRpdC1kZWxldGUgaSB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZSAhaW1wb3J0YW50O1xuICB0b3A6IC0xNHB4O1xuICBtYXJnaW4tbGVmdDogNDBweDsgfVxuIiwiLyoqXG4gKiBAbGljZW5zZVxuICogQ29weXJpZ2h0IEFrdmVvLiBBbGwgUmlnaHRzIFJlc2VydmVkLlxuICogTGljZW5zZWQgdW5kZXIgdGhlIE1JVCBMaWNlbnNlLiBTZWUgTGljZW5zZS50eHQgaW4gdGhlIHByb2plY3Qgcm9vdCBmb3IgbGljZW5zZSBpbmZvcm1hdGlvbi5cbiAqL1xuXG5AbWl4aW4gbmItc2Nyb2xsYmFycygkZmcsICRiZywgJHNpemUsICRib3JkZXItcmFkaXVzOiAkc2l6ZSAvIDIpIHtcbiAgOjotd2Via2l0LXNjcm9sbGJhciB7XG4gICAgd2lkdGg6ICRzaXplO1xuICAgIGhlaWdodDogJHNpemU7XG4gIH1cblxuICA6Oi13ZWJraXQtc2Nyb2xsYmFyLXRodW1iIHtcbiAgICBiYWNrZ3JvdW5kOiAkZmc7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIGJvcmRlci1yYWRpdXM6ICRib3JkZXItcmFkaXVzO1xuICB9XG5cbiAgOjotd2Via2l0LXNjcm9sbGJhci10cmFjayB7XG4gICAgYmFja2dyb3VuZDogJGJnO1xuICB9XG5cbiAgLy8gVE9ETzogcmVtb3ZlXG4gIC8vIEZvciBJbnRlcm5ldCBFeHBsb3JlclxuICBzY3JvbGxiYXItZmFjZS1jb2xvcjogJGZnO1xuICBzY3JvbGxiYXItdHJhY2stY29sb3I6ICRiZztcbn1cblxuQG1peGluIG5iLXJhZGlhbC1ncmFkaWVudCgkY29sb3ItMSwgJGNvbG9yLTIsICRjb2xvci0zKSB7XG4gIGJhY2tncm91bmQ6ICRjb2xvci0yOyAvKiBPbGQgYnJvd3NlcnMgKi9cbiAgYmFja2dyb3VuZDogLW1vei1yYWRpYWwtZ3JhZGllbnQoYm90dG9tLCBlbGxpcHNlIGNvdmVyLCAkY29sb3ItMSAwJSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkY29sb3ItMiA0NSUsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJGNvbG9yLTMgMTAwJSk7IC8qIEZGMy42LTE1ICovXG4gIGJhY2tncm91bmQ6IC13ZWJraXQtcmFkaWFsLWdyYWRpZW50KGJvdHRvbSwgZWxsaXBzZSBjb3ZlciwgJGNvbG9yLTEgMCUsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJGNvbG9yLTIgNDUlLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRjb2xvci0zIDEwMCUpOyAvKiBDaHJvbWUxMC0yNSxTYWZhcmk1LjEtNiAqL1xuICBiYWNrZ3JvdW5kOiByYWRpYWwtZ3JhZGllbnQoZWxsaXBzZSBhdCBib3R0b20sICRjb2xvci0xIDAlLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRjb2xvci0yIDQ1JSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkY29sb3ItMyAxMDAlKTsgLyogVzNDLCBJRTEwKywgRkYxNissIENocm9tZTI2KywgT3BlcmExMissIFNhZmFyaTcrICovXG4gIGZpbHRlcjogcHJvZ2lkOmR4aW1hZ2V0cmFuc2Zvcm0ubWljcm9zb2Z0LmdyYWRpZW50KHN0YXJ0Q29sb3JzdHI9JyRjb2xvci0xJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZW5kQ29sb3JzdHI9JyRjb2xvci0zJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgR3JhZGllbnRUeXBlPTEpOyAvKiBJRTYtOSBmYWxsYmFjayBvbiBob3Jpem9udGFsIGdyYWRpZW50ICovXG59XG5cbkBtaXhpbiBuYi1yaWdodC1ncmFkaWVudCgkbGVmdC1jb2xvciwgJHJpZ2h0LWNvbG9yKSB7XG4gIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgJGxlZnQtY29sb3IsICRyaWdodC1jb2xvcik7XG59XG5cbkBtaXhpbiBuYi1oZWFkaW5ncygkZnJvbTogMSwgJHRvOiA2KSB7XG4gIEBmb3IgJGkgZnJvbSAkZnJvbSB0aHJvdWdoICR0byB7XG4gICAgaCN7JGl9IHtcbiAgICAgIG1hcmdpbjogMDtcbiAgICB9XG4gIH1cbn1cblxuQG1peGluIGhvdmVyLWZvY3VzLWFjdGl2ZSB7XG4gICY6Zm9jdXMsXG4gICY6YWN0aXZlLFxuICAmOmhvdmVyIHtcbiAgICBAY29udGVudDtcbiAgfVxufVxuXG5AbWl4aW4gY2VudGVyLWhvcml6b250YWwtYWJzb2x1dGUge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIDApO1xuICBsZWZ0OiA1MCU7XG59XG5cbkBtaXhpbiBpbnN0YWxsLXRodW1iKCkge1xuICAkdGh1bWItc2VsZWN0b3JzOiAoXG4gICAgJzo6LXdlYmtpdC1zbGlkZXItdGh1bWInXG4gICAgJzo6LW1vei1yYW5nZS10aHVtYidcbiAgICAnOjotbXMtdGh1bWInXG4gICk7XG5cbiAgQGVhY2ggJHNlbGVjdG9yIGluICR0aHVtYi1zZWxlY3RvcnMge1xuICAgICYjeyRzZWxlY3Rvcn0ge1xuICAgICAgLXdlYmtpdC1hcHBlYXJhbmNlOiBub25lO1xuICAgICAgLW1vei1hcHBlYXJhbmNlOiBub25lO1xuICAgICAgQGNvbnRlbnQ7XG4gICAgfVxuICB9XG59XG5cbkBtaXhpbiBpbnN0YWxsLXRyYWNrKCkge1xuICAkdGh1bWItc2VsZWN0b3JzOiAoXG4gICAgJzo6LXdlYmtpdC1zbGlkZXItcnVubmFibGUtdHJhY2snXG4gICAgJzo6LW1vei1yYW5nZS10cmFjaydcbiAgICAnOjotbXMtdHJhY2snXG4gICk7XG5cbiAgQGVhY2ggJHNlbGVjdG9yIGluICR0aHVtYi1zZWxlY3RvcnMge1xuICAgICYjeyRzZWxlY3Rvcn0ge1xuICAgICAgLXdlYmtpdC1hcHBlYXJhbmNlOiBub25lO1xuICAgICAgLW1vei1hcHBlYXJhbmNlOiBub25lO1xuICAgICAgQGNvbnRlbnQ7XG4gICAgfVxuICB9XG59XG5cbkBtaXhpbiBpbnN0YWxsLXBsYWNlaG9sZGVyKCRjb2xvciwgJGZvbnQtc2l6ZSkge1xuICAkcGxhY2Vob2xkZXItc2VsZWN0b3JzOiAoXG4gICAgJzo6LXdlYmtpdC1pbnB1dC1wbGFjZWhvbGRlcidcbiAgICAnOjotbW96LXBsYWNlaG9sZGVyJ1xuICAgICc6LW1vei1wbGFjZWhvbGRlcidcbiAgICAnOi1tcy1pbnB1dC1wbGFjZWhvbGRlcidcbiAgKTtcblxuICAmOjpwbGFjZWhvbGRlciB7XG4gICAgQGluY2x1ZGUgcGxhY2Vob2xkZXIoJGNvbG9yLCAkZm9udC1zaXplKTtcbiAgfVxuXG4gIEBlYWNoICRzZWxlY3RvciBpbiAkcGxhY2Vob2xkZXItc2VsZWN0b3JzIHtcbiAgICAmI3skc2VsZWN0b3J9IHtcbiAgICAgIEBpbmNsdWRlIHBsYWNlaG9sZGVyKCRjb2xvciwgJGZvbnQtc2l6ZSk7XG4gICAgfVxuXG4gICAgJjpmb2N1cyN7JHNlbGVjdG9yfSB7XG4gICAgICBAaW5jbHVkZSBwbGFjZWhvbGRlci1mb2N1cygpO1xuICAgIH1cbiAgfVxufVxuXG5AbWl4aW4gcGxhY2Vob2xkZXIoJGNvbG9yLCAkZm9udC1zaXplKSB7XG4gIGNvbG9yOiAkY29sb3I7XG4gIGZvbnQtc2l6ZTogJGZvbnQtc2l6ZTtcbiAgb3BhY2l0eTogMTtcbiAgdHJhbnNpdGlvbjogb3BhY2l0eSAwLjNzIGVhc2U7XG4gIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xufVxuXG5AbWl4aW4gcGxhY2Vob2xkZXItZm9jdXMoKSB7XG4gIG9wYWNpdHk6IDA7XG4gIHRyYW5zaXRpb246IG9wYWNpdHkgMC4zcyBlYXNlO1xufVxuXG5AbWl4aW4gYW5pbWF0aW9uKCRhbmltYXRlLi4uKSB7XG4gICRtYXg6IGxlbmd0aCgkYW5pbWF0ZSk7XG4gICRhbmltYXRpb25zOiAnJztcblxuICBAZm9yICRpIGZyb20gMSB0aHJvdWdoICRtYXgge1xuICAgICRhbmltYXRpb25zOiAjeyRhbmltYXRpb25zICsgbnRoKCRhbmltYXRlLCAkaSl9O1xuXG4gICAgQGlmICRpIDwgJG1heCB7XG4gICAgICAkYW5pbWF0aW9uczogI3skYW5pbWF0aW9ucyArICcsICd9O1xuICAgIH1cbiAgfVxuICAtd2Via2l0LWFuaW1hdGlvbjogJGFuaW1hdGlvbnM7XG4gIC1tb3otYW5pbWF0aW9uOiAgICAkYW5pbWF0aW9ucztcbiAgLW8tYW5pbWF0aW9uOiAgICAgICRhbmltYXRpb25zO1xuICBhbmltYXRpb246ICAgICAgICAgJGFuaW1hdGlvbnM7XG59XG5cbkBtaXhpbiBrZXlmcmFtZXMoJGFuaW1hdGlvbk5hbWUpIHtcbiAgQC13ZWJraXQta2V5ZnJhbWVzICN7JGFuaW1hdGlvbk5hbWV9IHtcbiAgICBAY29udGVudDtcbiAgfVxuICBALW1vei1rZXlmcmFtZXMgI3skYW5pbWF0aW9uTmFtZX0ge1xuICAgIEBjb250ZW50O1xuICB9XG4gIEAtby1rZXlmcmFtZXMgI3skYW5pbWF0aW9uTmFtZX0ge1xuICAgIEBjb250ZW50O1xuICB9XG4gIEBrZXlmcmFtZXMgI3skYW5pbWF0aW9uTmFtZX0ge1xuICAgIEBjb250ZW50O1xuICB9XG59XG5cbi8qKlxuICogVGhpcyBtaXhpbiBnZW5lcmF0ZXMga2V5ZmFtZXMuXG4gKiBCZWNhdXNlIG9mIGFsbCBrZXlmcmFtZXMgY2FuJ3QgYmUgc2NvcGVkLFxuICogd2UgbmVlZCB0byBwdXRzIHVuaXF1ZSBuYW1lIGluIGVhY2ggYnRuLXB1bHNlIGNhbGwuXG4gKi9cbkBtaXhpbiBidG4tcHVsc2UoJG5hbWUsICRjb2xvcikge1xuICAmLmJ0bi1wdWxzZSB7XG4gICAgQGluY2x1ZGUgYW5pbWF0aW9uKGJ0bi0jeyRuYW1lfS1wdWxzZSAxLjVzIGluZmluaXRlKTtcbiAgfVxuXG4gIEBpbmNsdWRlIGtleWZyYW1lcyhidG4tI3skbmFtZX0tcHVsc2UpIHtcbiAgICAwJSB7XG4gICAgICBib3gtc2hhZG93OiBub25lO1xuICAgICAgb3BhY2l0eTogbmItdGhlbWUoYnRuLWRpc2FibGVkLW9wYWNpdHkpO1xuICAgIH1cbiAgICA1MCUge1xuICAgICAgYm94LXNoYWRvdzogMCAwIDFyZW0gMCAkY29sb3I7XG4gICAgICBvcGFjaXR5OiAwLjg7XG4gICAgfVxuICAgIDEwMCUge1xuICAgICAgYm94LXNoYWRvdzogbm9uZTtcbiAgICAgIG9wYWNpdHk6IG5iLXRoZW1lKGJ0bi1kaXNhYmxlZC1vcGFjaXR5KTtcbiAgICB9XG4gIH1cbn1cblxuLypcblxuQWNjb3JkaW5nIHRvIHRoZSBzcGVjaWZpY2F0aW9uIChodHRwczovL3d3dy53My5vcmcvVFIvY3NzLXNjb3BpbmctMS8jaG9zdC1zZWxlY3Rvcilcbjpob3N0IGFuZCA6aG9zdC1jb250ZXh0IGFyZSBwc2V1ZG8tY2xhc3Nlcy4gU28gd2UgYXNzdW1lIHRoZXkgY291bGQgYmUgY29tYmluZWQsXG5saWtlIG90aGVyIHBzZXVkby1jbGFzc2VzLCBldmVuIHNhbWUgb25lcy5cbkZvciBleGFtcGxlOiAnOm50aC1vZi10eXBlKDJuKTpudGgtb2YtdHlwZShldmVuKScuXG5cbklkZWFsIHNvbHV0aW9uIHdvdWxkIGJlIHRvIHByZXBlbmQgYW55IHNlbGVjdG9yIHdpdGggOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pLlxuVGhlbiBuZWJ1bGFyIGNvbXBvbmVudHMgd2lsbCBiZWhhdmUgYXMgYW4gaHRtbCBlbGVtZW50IGFuZCByZXNwb25kIHRvIFtkaXJdIGF0dHJpYnV0ZSBvbiBhbnkgbGV2ZWwsXG5zbyBkaXJlY3Rpb24gY291bGQgYmUgb3ZlcnJpZGRlbiBvbiBhbnkgY29tcG9uZW50IGxldmVsLlxuXG5JbXBsZW1lbnRhdGlvbiBjb2RlOlxuXG5AbWl4aW4gbmItcnRsKCkge1xuICAvLyBhZGQgIyB0byBzY3NzIGludGVycG9sYXRpb24gc3RhdGVtZW50LlxuICAvLyBpdCB3b3JrcyBpbiBjb21tZW50cyBhbmQgd2UgY2FuJ3QgdXNlIGl0IGhlcmVcbiAgQGF0LXJvb3Qge3NlbGVjdG9yLWFwcGVuZCgnOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pJywgJil9IHtcbiAgICBAY29udGVudDtcbiAgfVxufVxuXG5BbmQgd2hlbiB3ZSBjYWxsIGl0IHNvbWV3aGVyZTpcblxuOmhvc3Qge1xuICAuc29tZS1jbGFzcyB7XG4gICAgQGluY2x1ZGUgbmItcnRsKCkge1xuICAgICAgLi4uXG4gICAgfVxuICB9XG59XG46aG9zdC1jb250ZXh0KC4uLikge1xuICAuc29tZS1jbGFzcyB7XG4gICAgQGluY2x1ZGUgbmItcnRsKCkge1xuICAgICAgLi4uXG4gICAgfVxuICB9XG59XG5cblJlc3VsdCB3aWxsIGxvb2sgbGlrZTpcblxuOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pOmhvc3QgLnNvbWUtY2xhc3Mge1xuICAuLi5cbn1cbjpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKTpob3N0LWNvbnRleHQoLi4uKSAuc29tZS1jbGFzcyB7XG4gIC4uLlxufVxuXG4qXG4gIFNpZGUgbm90ZTpcbiAgOmhvc3QtY29udGV4dCgpOmhvc3Qgc2VsZWN0b3IgYXJlIHZhbGlkLiBodHRwczovL2xpc3RzLnczLm9yZy9BcmNoaXZlcy9QdWJsaWMvd3d3LXN0eWxlLzIwMTVGZWIvMDMwNS5odG1sXG5cbiAgOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pOmhvc3QtY29udGV4dCguLi4pIHNob3VsZCBtYXRjaCBhbnkgcGVybXV0YXRpb24sXG4gIHNvIG9yZGVyIGlzIG5vdCBpbXBvcnRhbnQuXG4qXG5cblxuQ3VycmVudGx5LCB0aGVyZSdyZSB0d28gcHJvYmxlbXMgd2l0aCB0aGlzIGFwcHJvYWNoOlxuXG5GaXJzdCwgaXMgdGhhdCB3ZSBjYW4ndCBjb21iaW5lIDpob3N0LCA6aG9zdC1jb250ZXh0LiBBbmd1bGFyIGJ1Z3MgIzE0MzQ5LCAjMTkxOTkuXG5Gb3IgdGhlIG1vbWVudCBvZiB3cml0aW5nLCB0aGUgb25seSBwb3NzaWJsZSB3YXkgaXM6XG46aG9zdCB7XG4gIDpob3N0LWNvbnRleHQoLi4uKSB7XG4gICAgLi4uXG4gIH1cbn1cbkl0IGRvZXNuJ3Qgd29yayBmb3IgdXMgYmVjYXVzZSBtaXhpbiBjb3VsZCBiZSBjYWxsZWQgc29tZXdoZXJlIGRlZXBlciwgbGlrZTpcbjpob3N0IHtcbiAgcCB7XG4gICAgQGluY2x1ZGUgbmItcnRsKCkgeyAuLi4gfVxuICB9XG59XG5XZSBhcmUgbm90IGFibGUgdG8gZ28gdXAgdG8gOmhvc3QgbGV2ZWwgdG8gcGxhY2UgY29udGVudCBwYXNzZWQgdG8gbWl4aW4uXG5cblRoZSBzZWNvbmQgcHJvYmxlbSBpcyB0aGF0IHdlIG9ubHkgY2FuIGJlIHN1cmUgdGhhdCB3ZSBhcHBlbmRpbmcgOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pIHRvIGFub3RoZXJcbjpob3N0Lzpob3N0LWNvbnRleHQgcHNldWRvLWNsYXNzIHdoZW4gY2FsbGVkIGluIHRoZW1lIGZpbGVzICgqLnRoZW1lLnNjc3MpLlxuICAqXG4gICAgU2lkZSBub3RlOlxuICAgIEN1cnJlbnRseSwgbmItaW5zdGFsbC1jb21wb25lbnQgdXNlcyBhbm90aGVyIGFwcHJvYWNoIHdoZXJlIDpob3N0IHByZXBlbmRlZCB3aXRoIHRoZSB0aGVtZSBuYW1lXG4gICAgKGh0dHBzOi8vZ2l0aHViLmNvbS9hbmd1bGFyL2FuZ3VsYXIvYmxvYi81Yjk2MDc4NjI0YjBhNDc2MGYyZGJjZjZmZGYwYmQ2Mjc5MWJlNWJiL3BhY2thZ2VzL2NvbXBpbGVyL3NyYy9zaGFkb3dfY3NzLnRzI0w0NDEpLFxuICAgIGJ1dCBpdCB3YXMgbWFkZSB0byBiZSBhYmxlIHRvIHVzZSBjdXJyZW50IHJlYWxpemF0aW9uIG9mIHJ0bCBhbmQgaXQgY2FuIGJlIHJld3JpdHRlbiBiYWNrIHRvXG4gICAgOmhvc3QtY29udGV4dCgkdGhlbWUpIG9uY2Ugd2Ugd2lsbCBiZSBhYmxlIHRvIHVzZSBtdWx0aXBsZSBzaGFkb3cgc2VsZWN0b3JzLlxuICAqXG5CdXQgd2hlbiBpdCdzIGNhbGxlZCBpbiAqLmNvbXBvbmVudC5zY3NzIHdlIGNhbid0IGJlIHN1cmUsIHRoYXQgc2VsZWN0b3Igc3RhcnRzIHdpdGggOmhvc3QvOmhvc3QtY29udGV4dCxcbmJlY2F1c2UgYW5ndWxhciBhbGxvd3Mgb21pdHRpbmcgcHNldWRvLWNsYXNzZXMgaWYgd2UgZG9uJ3QgbmVlZCB0byBzdHlsZSA6aG9zdCBjb21wb25lbnQgaXRzZWxmLlxuV2UgY2FuIGJyZWFrIHN1Y2ggc2VsZWN0b3JzLCBieSBqdXN0IGFwcGVuZGluZyA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkgdG8gdGhlbS5cbiAgKioqXG4gICAgUG9zc2libGUgc29sdXRpb25cbiAgICBjaGVjayBpZiB3ZSBpbiB0aGVtZSBieSBzb21lIHRoZW1lIHZhcmlhYmxlcyBhbmQgaWYgc28gYXBwZW5kLCBvdGhlcndpc2UgbmVzdCBsaWtlXG4gICAgQGF0LXJvb3QgOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pIHtcbiAgICAgIC8vIGFkZCAjIHRvIHNjc3MgaW50ZXJwb2xhdGlvbiBzdGF0ZW1lbnQuXG4gICAgICAvLyBpdCB3b3JrcyBpbiBjb21tZW50cyBhbmQgd2UgY2FuJ3QgdXNlIGl0IGhlcmVcbiAgICAgIHsmfSB7XG4gICAgICAgIEBjb250ZW50O1xuICAgICAgfVxuICAgIH1cbiAgICBXaGF0IGlmIDpob3N0IHNwZWNpZmllZD8gQ2FuIHdlIGFkZCBzcGFjZSBpbiA6aG9zdC1jb250ZXh0KC4uLikgOmhvc3Q/XG4gICAgT3IgbWF5YmUgYWRkIDpob3N0IHNlbGVjdG9yIGFueXdheT8gSWYgbXVsdGlwbGUgOmhvc3Qgc2VsZWN0b3JzIGFyZSBhbGxvd2VkXG4gICoqKlxuXG5cblByb2JsZW1zIHdpdGggdGhlIGN1cnJlbnQgYXBwcm9hY2guXG5cbjEuIERpcmVjdGlvbiBjYW4gYmUgYXBwbGllZCBvbmx5IG9uIGRvY3VtZW50IGxldmVsLCBiZWNhdXNlIG1peGluIHByZXBlbmRzIHRoZW1lIGNsYXNzLFxud2hpY2ggcGxhY2VkIG9uIHRoZSBib2R5LlxuMi4gKi5jb21wb25lbnQuc2NzcyBzdHlsZXMgc2hvdWxkIGJlIGluIDpob3N0IHNlbGVjdG9yLiBPdGhlcndpc2UgYW5ndWxhciB3aWxsIGFkZCBob3N0XG5hdHRyaWJ1dGUgdG8gW2Rpcj1ydGxdIGF0dHJpYnV0ZSBhcyB3ZWxsLlxuXG5cbkdlbmVyYWwgcHJvYmxlbXMuXG5cbkx0ciBpcyBkZWZhdWx0IGRvY3VtZW50IGRpcmVjdGlvbiwgYnV0IGZvciBwcm9wZXIgd29yayBvZiBuYi1sdHIgKG1lYW5zIGx0ciBvbmx5KSxcbltkaXI9bHRyXSBzaG91bGQgYmUgc3BlY2lmaWVkIGF0IGxlYXN0IHNvbWV3aGVyZS4gJzpub3QoW2Rpcj1ydGxdJyBub3QgYXBwbGljYWJsZSBoZXJlLFxuYmVjYXVzZSBpdCdzIHNhdGlzZnkgYW55IHBhcmVudCwgdGhhdCBkb24ndCBoYXZlIFtkaXI9cnRsXSBhdHRyaWJ1dGUuXG5QcmV2aW91cyBhcHByb2FjaCB3YXMgdG8gdXNlIHNpbmdsZSBydGwgbWl4aW4gYW5kIHJlc2V0IGx0ciBwcm9wZXJ0aWVzIHRvIGluaXRpYWwgdmFsdWUuXG5CdXQgc29tZXRpbWVzIGl0J3MgaGFyZCB0byBmaW5kLCB3aGF0IHRoZSBwcmV2aW91cyB2YWx1ZSBzaG91bGQgYmUuIEFuZCBzdWNoIG1peGluIGNhbGwgbG9va3MgdG9vIHZlcmJvc2UuXG4qL1xuXG5AbWl4aW4gX3ByZXBlbmQtd2l0aC1zZWxlY3Rvcigkc2VsZWN0b3IsICRwcm9wOiBudWxsLCAkdmFsdWU6IG51bGwpIHtcbiAgI3skc2VsZWN0b3J9ICYge1xuICAgIEBpZiAkcHJvcCAhPSBudWxsIHtcbiAgICAgICN7JHByb3B9OiAkdmFsdWU7XG4gICAgfVxuXG4gICAgQGNvbnRlbnQ7XG4gIH1cbn1cblxuQG1peGluIG5iLWx0cigkcHJvcDogbnVsbCwgJHZhbHVlOiBudWxsKSB7XG4gIEBpbmNsdWRlIF9wcmVwZW5kLXdpdGgtc2VsZWN0b3IoJ1tkaXI9bHRyXScsICRwcm9wLCAkdmFsdWUpIHtcbiAgICBAY29udGVudDtcbiAgfVxufVxuXG5AbWl4aW4gbmItcnRsKCRwcm9wOiBudWxsLCAkdmFsdWU6IG51bGwpIHtcbiAgQGluY2x1ZGUgX3ByZXBlbmQtd2l0aC1zZWxlY3RvcignW2Rpcj1ydGxdJywgJHByb3AsICR2YWx1ZSkge1xuICAgIEBjb250ZW50O1xuICB9O1xufVxuIiwiLyoqXG4gKiBAbGljZW5zZVxuICogQ29weXJpZ2h0IEFrdmVvLiBBbGwgUmlnaHRzIFJlc2VydmVkLlxuICogTGljZW5zZWQgdW5kZXIgdGhlIE1JVCBMaWNlbnNlLiBTZWUgTGljZW5zZS50eHQgaW4gdGhlIHByb2plY3Qgcm9vdCBmb3IgbGljZW5zZSBpbmZvcm1hdGlvbi5cbiAqL1xuXG4vLy8gU2xpZ2h0bHkgbGlnaHRlbiBhIGNvbG9yXG4vLy8gQGFjY2VzcyBwdWJsaWNcbi8vLyBAcGFyYW0ge0NvbG9yfSAkY29sb3IgLSBjb2xvciB0byB0aW50XG4vLy8gQHBhcmFtIHtOdW1iZXJ9ICRwZXJjZW50YWdlIC0gcGVyY2VudGFnZSBvZiBgJGNvbG9yYCBpbiByZXR1cm5lZCBjb2xvclxuLy8vIEByZXR1cm4ge0NvbG9yfVxuQGZ1bmN0aW9uIHRpbnQoJGNvbG9yLCAkcGVyY2VudGFnZSkge1xuICBAcmV0dXJuIG1peCh3aGl0ZSwgJGNvbG9yLCAkcGVyY2VudGFnZSk7XG59XG5cbi8vLyBTbGlnaHRseSBkYXJrZW4gYSBjb2xvclxuLy8vIEBhY2Nlc3MgcHVibGljXG4vLy8gQHBhcmFtIHtDb2xvcn0gJGNvbG9yIC0gY29sb3IgdG8gc2hhZGVcbi8vLyBAcGFyYW0ge051bWJlcn0gJHBlcmNlbnRhZ2UgLSBwZXJjZW50YWdlIG9mIGAkY29sb3JgIGluIHJldHVybmVkIGNvbG9yXG4vLy8gQHJldHVybiB7Q29sb3J9XG5AZnVuY3Rpb24gc2hhZGUoJGNvbG9yLCAkcGVyY2VudGFnZSkge1xuICBAcmV0dXJuIG1peChibGFjaywgJGNvbG9yLCAkcGVyY2VudGFnZSk7XG59XG5cbkBmdW5jdGlvbiBtYXAtc2V0KCRtYXAsICRrZXksICR2YWx1ZTogbnVsbCkge1xuICAkbmV3OiAoJGtleTogJHZhbHVlKTtcbiAgQHJldHVybiBtYXAtbWVyZ2UoJG1hcCwgJG5ldyk7XG59XG4iLCIvKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG5cbkBpbXBvcnQgJy4uL2NvcmUvZnVuY3Rpb25zJztcbkBpbXBvcnQgJy4uL2NvcmUvbWl4aW5zJztcblxuJHRoZW1lOiAoXG4gIGZvbnQtbWFpbjogdW5xdW90ZSgnXCJTZWdvZSBVSVwiLCBSb2JvdG8sIFwiSGVsdmV0aWNhIE5ldWVcIiwgQXJpYWwsIHNhbnMtc2VyaWYnKSxcbiAgZm9udC1zZWNvbmRhcnk6IGZvbnQtbWFpbixcblxuICBmb250LXdlaWdodC10aGluOiAyMDAsXG4gIGZvbnQtd2VpZ2h0LWxpZ2h0OiAzMDAsXG4gIGZvbnQtd2VpZ2h0LW5vcm1hbDogNDAwLFxuICBmb250LXdlaWdodC1ib2xkZXI6IDUwMCxcbiAgZm9udC13ZWlnaHQtYm9sZDogNjAwLFxuICBmb250LXdlaWdodC11bHRyYS1ib2xkOiA4MDAsXG5cbiAgLy8gVE9ETzogdXNlIGl0IGFzIGEgZGVmYXVsdCBmb250LXNpemVcbiAgYmFzZS1mb250LXNpemU6IDE2cHgsXG5cbiAgZm9udC1zaXplLXhsZzogMS4yNXJlbSxcbiAgZm9udC1zaXplLWxnOiAxLjEyNXJlbSxcbiAgZm9udC1zaXplOiAxcmVtLFxuICBmb250LXNpemUtc206IDAuODc1cmVtLFxuICBmb250LXNpemUteHM6IDAuNzVyZW0sXG5cbiAgcmFkaXVzOiAwLjM3NXJlbSxcbiAgcGFkZGluZzogMS4yNXJlbSxcbiAgbWFyZ2luOiAxLjVyZW0sXG4gIGxpbmUtaGVpZ2h0OiAxLjI1LFxuXG4gIGNvbG9yLWJnOiAjZmZmZmZmLFxuICBjb2xvci1iZy1hY3RpdmU6ICNlOWVkZjIsXG4gIGNvbG9yLWZnOiAjYTRhYmIzLFxuICBjb2xvci1mZy1oZWFkaW5nOiAjMmEyYTJhLFxuICBjb2xvci1mZy10ZXh0OiAjNGI0YjRiLFxuICBjb2xvci1mZy1oaWdobGlnaHQ6ICM0MGRjN2UsXG5cbiAgc2VwYXJhdG9yOiAjZWJlZWYyLFxuXG4gIGNvbG9yLWdyYXk6IHJnYmEoODEsIDExMywgMTY1LCAwLjE1KSxcbiAgY29sb3ItbmV1dHJhbDogdHJhbnNwYXJlbnQsXG4gIGNvbG9yLXdoaXRlOiAjZmZmZmZmLFxuICBjb2xvci1kaXNhYmxlZDogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjQpLFxuXG4gIGNvbG9yLXByaW1hcnk6ICM4YTdmZmYsXG4gIGNvbG9yLXN1Y2Nlc3M6ICM0MGRjN2UsXG4gIGNvbG9yLWluZm86ICM0Y2E2ZmYsXG4gIGNvbG9yLXdhcm5pbmc6ICNmZmExMDAsXG4gIGNvbG9yLWRhbmdlcjogI2ZmNGM2YSxcblxuICAvLyBUT0RPOiBtb3ZlIHRvIGNvbnN0YW50c1xuICBzb2NpYWwtY29sb3ItZmFjZWJvb2s6ICMzYjU5OTgsXG4gIHNvY2lhbC1jb2xvci10d2l0dGVyOiAjNTVhY2VlLFxuICBzb2NpYWwtY29sb3ItZ29vZ2xlOiAjZGQ0YjM5LFxuICBzb2NpYWwtY29sb3ItbGlua2VkaW46ICMwMTc3YjUsXG4gIHNvY2lhbC1jb2xvci1naXRodWI6ICM2YjZiNmIsXG4gIHNvY2lhbC1jb2xvci1zdGFja292ZXJmbG93OiAjMmY5NmU4LFxuICBzb2NpYWwtY29sb3ItZHJpYmJsZTogI2YyNjc5OCxcbiAgc29jaWFsLWNvbG9yLWJlaGFuY2U6ICMwMDkzZmEsXG5cbiAgYm9yZGVyLWNvbG9yOiBjb2xvci1ncmF5LFxuICBzaGFkb3c6IDAgMnB4IDEycHggMCAjZGZlM2ViLFxuXG4gIGxpbmstY29sb3I6ICMzZGNjNmQsXG4gIGxpbmstY29sb3ItaG92ZXI6ICMyZWU1NmIsXG4gIGxpbmstY29sb3ItdmlzaXRlZDogbGluay1jb2xvcixcblxuICBzY3JvbGxiYXItZmc6ICNkYWRhZGEsXG4gIHNjcm9sbGJhci1iZzogI2YyZjJmMixcbiAgc2Nyb2xsYmFyLXdpZHRoOiA1cHgsXG4gIHNjcm9sbGJhci10aHVtYi1yYWRpdXM6IDIuNXB4LFxuXG4gIHJhZGlhbC1ncmFkaWVudDogbm9uZSxcbiAgbGluZWFyLWdyYWRpZW50OiBub25lLFxuXG4gIGNhcmQtZm9udC1zaXplOiBmb250LXNpemUsXG4gIGNhcmQtbGluZS1oZWlnaHQ6IGxpbmUtaGVpZ2h0LFxuICBjYXJkLWZvbnQtd2VpZ2h0OiBmb250LXdlaWdodC1ub3JtYWwsXG4gIGNhcmQtZmc6IGNvbG9yLWZnLCAvLyBUT0RPOiBub3QgdXNlZFxuICBjYXJkLWZnLXRleHQ6IGNvbG9yLWZnLXRleHQsXG4gIGNhcmQtZmctaGVhZGluZzogY29sb3ItZmctaGVhZGluZywgLy8gVE9ETzogbm90IHVzZWRcbiAgY2FyZC1iZzogY29sb3ItYmcsXG4gIGNhcmQtaGVpZ2h0LXh4c21hbGw6IDk2cHgsXG4gIGNhcmQtaGVpZ2h0LXhzbWFsbDogMjE2cHgsXG4gIGNhcmQtaGVpZ2h0LXNtYWxsOiAzMzZweCxcbiAgY2FyZC1oZWlnaHQtbWVkaXVtOiA0NTZweCxcbiAgY2FyZC1oZWlnaHQtbGFyZ2U6IDU3NnB4LFxuICBjYXJkLWhlaWdodC14bGFyZ2U6IDY5NnB4LFxuICBjYXJkLWhlaWdodC14eGxhcmdlOiA4MTZweCxcbiAgY2FyZC1zaGFkb3c6IHNoYWRvdyxcbiAgY2FyZC1ib3JkZXItd2lkdGg6IDAsXG4gIGNhcmQtYm9yZGVyLXR5cGU6IHNvbGlkLFxuICBjYXJkLWJvcmRlci1jb2xvcjogY29sb3ItYmcsXG4gIGNhcmQtYm9yZGVyLXJhZGl1czogcmFkaXVzLFxuICBjYXJkLXBhZGRpbmc6IHBhZGRpbmcsXG4gIGNhcmQtbWFyZ2luOiBtYXJnaW4sXG4gIGNhcmQtaGVhZGVyLWZvbnQtZmFtaWx5OiBmb250LXNlY29uZGFyeSxcbiAgY2FyZC1oZWFkZXItZm9udC1zaXplOiBmb250LXNpemUtbGcsXG4gIGNhcmQtaGVhZGVyLWZvbnQtd2VpZ2h0OiBmb250LXdlaWdodC1ib2xkLFxuICBjYXJkLXNlcGFyYXRvcjogc2VwYXJhdG9yLFxuICBjYXJkLWhlYWRlci1mZzogY29sb3ItZmcsIC8vIFRPRE86IG5vdCB1c2VkXG4gIGNhcmQtaGVhZGVyLWZnLWhlYWRpbmc6IGNvbG9yLWZnLWhlYWRpbmcsXG4gIGNhcmQtaGVhZGVyLWFjdGl2ZS1iZzogY29sb3ItZmcsXG4gIGNhcmQtaGVhZGVyLWFjdGl2ZS1mZzogY29sb3ItYmcsXG4gIGNhcmQtaGVhZGVyLWRpc2FibGVkLWJnOiBjb2xvci1kaXNhYmxlZCxcbiAgY2FyZC1oZWFkZXItcHJpbWFyeS1iZzogY29sb3ItcHJpbWFyeSxcbiAgY2FyZC1oZWFkZXItaW5mby1iZzogY29sb3ItaW5mbyxcbiAgY2FyZC1oZWFkZXItc3VjY2Vzcy1iZzogY29sb3Itc3VjY2VzcyxcbiAgY2FyZC1oZWFkZXItd2FybmluZy1iZzogY29sb3Itd2FybmluZyxcbiAgY2FyZC1oZWFkZXItZGFuZ2VyLWJnOiBjb2xvci1kYW5nZXIsXG4gIGNhcmQtaGVhZGVyLWJvcmRlci13aWR0aDogMXB4LFxuICBjYXJkLWhlYWRlci1ib3JkZXItdHlwZTogc29saWQsXG4gIGNhcmQtaGVhZGVyLWJvcmRlci1jb2xvcjogY2FyZC1zZXBhcmF0b3IsXG5cbiAgaGVhZGVyLWZvbnQtZmFtaWx5OiBmb250LXNlY29uZGFyeSxcbiAgaGVhZGVyLWZvbnQtc2l6ZTogZm9udC1zaXplLFxuICBoZWFkZXItbGluZS1oZWlnaHQ6IGxpbmUtaGVpZ2h0LFxuICBoZWFkZXItZmc6IGNvbG9yLWZnLWhlYWRpbmcsXG4gIGhlYWRlci1iZzogY29sb3ItYmcsXG4gIGhlYWRlci1oZWlnaHQ6IDQuNzVyZW0sXG4gIGhlYWRlci1wYWRkaW5nOiAxLjI1cmVtLFxuICBoZWFkZXItc2hhZG93OiBzaGFkb3csXG5cbiAgZm9vdGVyLWhlaWdodDogNC43MjVyZW0sXG4gIGZvb3Rlci1wYWRkaW5nOiAxLjI1cmVtLFxuICBmb290ZXItZmc6IGNvbG9yLWZnLWhlYWRpbmcsXG4gIGZvb3Rlci1mZy1oaWdobGlnaHQ6IGNvbG9yLWZnLWhlYWRpbmcsXG4gIGZvb3Rlci1iZzogY29sb3ItYmcsXG4gIGZvb3Rlci1zZXBhcmF0b3I6IHNlcGFyYXRvcixcbiAgZm9vdGVyLXNoYWRvdzogc2hhZG93LFxuXG4gIGxheW91dC1mb250LWZhbWlseTogZm9udC1tYWluLFxuICBsYXlvdXQtZm9udC1zaXplOiBmb250LXNpemUsXG4gIGxheW91dC1saW5lLWhlaWdodDogbGluZS1oZWlnaHQsXG4gIGxheW91dC1mZzogY29sb3ItZmcsXG4gIGxheW91dC1iZzogI2ViZWZmNSxcbiAgbGF5b3V0LW1pbi1oZWlnaHQ6IDEwMHZoLFxuICBsYXlvdXQtY29udGVudC13aWR0aDogOTAwcHgsXG4gIGxheW91dC13aW5kb3ctbW9kZS1taW4td2lkdGg6IDMwMHB4LFxuICBsYXlvdXQtd2luZG93LW1vZGUtbWF4LXdpZHRoOiAxOTIwcHgsXG4gIGxheW91dC13aW5kb3ctbW9kZS1iZzogbGF5b3V0LWJnLFxuICBsYXlvdXQtd2luZG93LW1vZGUtcGFkZGluZy10b3A6IDQuNzVyZW0sXG4gIGxheW91dC13aW5kb3ctc2hhZG93OiBzaGFkb3csXG4gIGxheW91dC1wYWRkaW5nOiAyLjI1cmVtIDIuMjVyZW0gMC43NXJlbSxcbiAgbGF5b3V0LW1lZGl1bS1wYWRkaW5nOiAxLjVyZW0gMS41cmVtIDAuNXJlbSxcbiAgbGF5b3V0LXNtYWxsLXBhZGRpbmc6IDFyZW0gMXJlbSAwLFxuXG4gIHNpZGViYXItZm9udC1zaXplOiBmb250LXNpemUsXG4gIHNpZGViYXItbGluZS1oZWlnaHQ6IGxpbmUtaGVpZ2h0LFxuICBzaWRlYmFyLWZnOiBjb2xvci1mZy1oZWFkaW5nLFxuICBzaWRlYmFyLWJnOiBjb2xvci1iZyxcbiAgc2lkZWJhci1oZWlnaHQ6IDEwMHZoLFxuICBzaWRlYmFyLXdpZHRoOiAxNnJlbSxcbiAgc2lkZWJhci13aWR0aC1jb21wYWN0OiAzLjVyZW0sXG4gIHNpZGViYXItcGFkZGluZzogcGFkZGluZyxcbiAgc2lkZWJhci1oZWFkZXItaGVpZ2h0OiAzLjVyZW0sXG4gIHNpZGViYXItZm9vdGVyLWhlaWdodDogMy41cmVtLFxuICBzaWRlYmFyLXNoYWRvdzogc2hhZG93LFxuXG4gIG1lbnUtZm9udC1mYW1pbHk6IGZvbnQtc2Vjb25kYXJ5LFxuICBtZW51LWZvbnQtc2l6ZTogZm9udC1zaXplLFxuICBtZW51LWZvbnQtd2VpZ2h0OiBmb250LXdlaWdodC1ib2xkZXIsXG4gIG1lbnUtZmc6IGNvbG9yLWZnLXRleHQsXG4gIG1lbnUtYmc6IGNvbG9yLWJnLFxuICBtZW51LWFjdGl2ZS1mZzogY29sb3ItZmctaGVhZGluZyxcbiAgbWVudS1hY3RpdmUtYmc6IGNvbG9yLWJnLFxuICBtZW51LWFjdGl2ZS1mb250LXdlaWdodDogZm9udC13ZWlnaHQtYm9sZCxcblxuICBtZW51LXN1Ym1lbnUtYmc6IGNvbG9yLWJnLFxuICBtZW51LXN1Ym1lbnUtZmc6IGNvbG9yLWZnLXRleHQsXG4gIG1lbnUtc3VibWVudS1hY3RpdmUtZmc6IGNvbG9yLWZnLWhlYWRpbmcsXG4gIG1lbnUtc3VibWVudS1hY3RpdmUtYmc6IGNvbG9yLWJnLFxuICBtZW51LXN1Ym1lbnUtYWN0aXZlLWJvcmRlci1jb2xvcjogY29sb3ItZmctaGlnaGxpZ2h0LFxuICBtZW51LXN1Ym1lbnUtYWN0aXZlLXNoYWRvdzogbm9uZSxcbiAgbWVudS1zdWJtZW51LWhvdmVyLWZnOiBtZW51LXN1Ym1lbnUtYWN0aXZlLWZnLFxuICBtZW51LXN1Ym1lbnUtaG92ZXItYmc6IG1lbnUtc3VibWVudS1iZyxcbiAgbWVudS1zdWJtZW51LWl0ZW0tYm9yZGVyLXdpZHRoOiAwLjEyNXJlbSxcbiAgbWVudS1zdWJtZW51LWl0ZW0tYm9yZGVyLXJhZGl1czogcmFkaXVzLFxuICBtZW51LXN1Ym1lbnUtaXRlbS1wYWRkaW5nOiAwLjVyZW0gMXJlbSxcbiAgbWVudS1zdWJtZW51LWl0ZW0tY29udGFpbmVyLXBhZGRpbmc6IDAgMS4yNXJlbSxcbiAgbWVudS1zdWJtZW51LXBhZGRpbmc6IDAuNXJlbSxcblxuICBtZW51LWdyb3VwLWZvbnQtd2VpZ2h0OiBmb250LXdlaWdodC1ib2xkZXIsXG4gIG1lbnUtZ3JvdXAtZm9udC1zaXplOiAwLjg3NXJlbSxcbiAgbWVudS1ncm91cC1mZzogY29sb3ItZmcsXG4gIG1lbnUtZ3JvdXAtcGFkZGluZzogMXJlbSAxLjI1cmVtLFxuICBtZW51LWl0ZW0tcGFkZGluZzogMC42NzVyZW0gMC43NXJlbSxcbiAgbWVudS1pdGVtLXNlcGFyYXRvcjogc2VwYXJhdG9yLFxuICBtZW51LWljb24tZm9udC1zaXplOiAyLjVyZW0sXG4gIG1lbnUtaWNvbi1tYXJnaW46IDAgMC4yNXJlbSAwLFxuICBtZW51LWljb24tY29sb3I6IGNvbG9yLWZnLFxuICBtZW51LWljb24tYWN0aXZlLWNvbG9yOiBjb2xvci1mZy1oZWFkaW5nLFxuXG4gIHRhYnMtZm9udC1mYW1pbHk6IGZvbnQtc2Vjb25kYXJ5LFxuICB0YWJzLWZvbnQtc2l6ZTogZm9udC1zaXplLWxnLFxuICB0YWJzLWNvbnRlbnQtZm9udC1mYW1pbHk6IGZvbnQtbWFpbixcbiAgdGFicy1jb250ZW50LWZvbnQtc2l6ZTogZm9udC1zaXplLFxuICB0YWJzLWFjdGl2ZS1iZzogdHJhbnNwYXJlbnQsXG4gIHRhYnMtYWN0aXZlLWZvbnQtd2VpZ2h0OiBjYXJkLWhlYWRlci1mb250LXdlaWdodCxcbiAgdGFicy1wYWRkaW5nOiBwYWRkaW5nLFxuICB0YWJzLWNvbnRlbnQtcGFkZGluZzogMCxcbiAgdGFicy1oZWFkZXItYmc6IHRyYW5zcGFyZW50LFxuICB0YWJzLXNlcGFyYXRvcjogc2VwYXJhdG9yLFxuICB0YWJzLWZnOiBjb2xvci1mZyxcbiAgdGFicy1mZy10ZXh0OiBjb2xvci1mZy10ZXh0LFxuICB0YWJzLWZnLWhlYWRpbmc6IGNvbG9yLWZnLWhlYWRpbmcsXG4gIHRhYnMtYmc6IHRyYW5zcGFyZW50LFxuICB0YWJzLXNlbGVjdGVkOiBjb2xvci1zdWNjZXNzLFxuICB0YWJzLWljb24tb25seS1tYXgtd2lkdGg6IDU3NnB4LFxuXG4gIHJvdXRlLXRhYnMtZm9udC1mYW1pbHk6IGZvbnQtc2Vjb25kYXJ5LFxuICByb3V0ZS10YWJzLWZvbnQtc2l6ZTogZm9udC1zaXplLWxnLFxuICByb3V0ZS10YWJzLWFjdGl2ZS1iZzogdHJhbnNwYXJlbnQsXG4gIHJvdXRlLXRhYnMtYWN0aXZlLWZvbnQtd2VpZ2h0OiBjYXJkLWhlYWRlci1mb250LXdlaWdodCxcbiAgcm91dGUtdGFicy1wYWRkaW5nOiBwYWRkaW5nLFxuICByb3V0ZS10YWJzLWhlYWRlci1iZzogdHJhbnNwYXJlbnQsXG4gIHJvdXRlLXRhYnMtc2VwYXJhdG9yOiBzZXBhcmF0b3IsXG4gIHJvdXRlLXRhYnMtZmc6IGNvbG9yLWZnLFxuICByb3V0ZS10YWJzLWZnLWhlYWRpbmc6IGNvbG9yLWZnLWhlYWRpbmcsXG4gIHJvdXRlLXRhYnMtYmc6IHRyYW5zcGFyZW50LFxuICByb3V0ZS10YWJzLXNlbGVjdGVkOiBjb2xvci1zdWNjZXNzLFxuICByb3V0ZS10YWJzLWljb24tb25seS1tYXgtd2lkdGg6IDU3NnB4LFxuXG4gIHVzZXItZm9udC1zaXplOiBmb250LXNpemUsXG4gIHVzZXItbGluZS1oZWlnaHQ6IGxpbmUtaGVpZ2h0LFxuICB1c2VyLWJnOiBjb2xvci1iZyxcbiAgdXNlci1mZzogY29sb3ItZmcsXG4gIHVzZXItZmctaGlnaGxpZ2h0OiAjYmNjM2NjLFxuICB1c2VyLWZvbnQtZmFtaWx5LXNlY29uZGFyeTogZm9udC1zZWNvbmRhcnksXG4gIHVzZXItc2l6ZS1zbWFsbDogMS41cmVtLFxuICB1c2VyLXNpemUtbWVkaXVtOiAyLjVyZW0sXG4gIHVzZXItc2l6ZS1sYXJnZTogMy4yNXJlbSxcbiAgdXNlci1zaXplLXhsYXJnZTogNHJlbSxcblxuICBwb3BvdmVyLWZnOiBjb2xvci1mZy1oZWFkaW5nLFxuICBwb3BvdmVyLWJnOiBjb2xvci1iZyxcbiAgcG9wb3Zlci1ib3JkZXI6IGNvbG9yLXN1Y2Nlc3MsXG4gIHBvcG92ZXItc2hhZG93OiBub25lLFxuXG4gIGNvbnRleHQtbWVudS1mZzogY29sb3ItZmctaGVhZGluZyxcbiAgY29udGV4dC1tZW51LWFjdGl2ZS1mZzogY29sb3Itd2hpdGUsXG4gIGNvbnRleHQtbWVudS1hY3RpdmUtYmc6IGNvbG9yLXN1Y2Nlc3MsXG5cbiAgYWN0aW9ucy1mb250LXNpemU6IGZvbnQtc2l6ZSxcbiAgYWN0aW9ucy1mb250LWZhbWlseTogZm9udC1zZWNvbmRhcnksXG4gIGFjdGlvbnMtbGluZS1oZWlnaHQ6IGxpbmUtaGVpZ2h0LFxuICBhY3Rpb25zLWZnOiBjb2xvci1mZyxcbiAgYWN0aW9ucy1iZzogY29sb3ItYmcsXG4gIGFjdGlvbnMtc2VwYXJhdG9yOiBzZXBhcmF0b3IsXG4gIGFjdGlvbnMtcGFkZGluZzogcGFkZGluZyxcbiAgYWN0aW9ucy1zaXplLXNtYWxsOiAxLjVyZW0sXG4gIGFjdGlvbnMtc2l6ZS1tZWRpdW06IDIuMjVyZW0sXG4gIGFjdGlvbnMtc2l6ZS1sYXJnZTogMy41cmVtLFxuXG4gIHNlYXJjaC1idG4tb3Blbi1mZzogY29sb3ItZmcsXG4gIHNlYXJjaC1idG4tY2xvc2UtZmc6XHRjb2xvci1mZyxcbiAgc2VhcmNoLWJnOiBsYXlvdXQtYmcsXG4gIHNlYXJjaC1iZy1zZWNvbmRhcnk6IGNvbG9yLWZnLFxuICBzZWFyY2gtdGV4dDogY29sb3ItZmctaGVhZGluZyxcbiAgc2VhcmNoLWluZm86IGNvbG9yLWZnLFxuICBzZWFyY2gtZGFzaDogY29sb3ItZmcsXG4gIHNlYXJjaC1wbGFjZWhvbGRlcjogY29sb3ItZmcsXG5cbiAgc21hcnQtdGFibGUtaGVhZGVyLWZvbnQtZmFtaWx5OiBmb250LXNlY29uZGFyeSxcbiAgc21hcnQtdGFibGUtaGVhZGVyLWZvbnQtc2l6ZTogZm9udC1zaXplLFxuICBzbWFydC10YWJsZS1oZWFkZXItZm9udC13ZWlnaHQ6IGZvbnQtd2VpZ2h0LWJvbGQsXG4gIHNtYXJ0LXRhYmxlLWhlYWRlci1saW5lLWhlaWdodDogbGluZS1oZWlnaHQsXG4gIHNtYXJ0LXRhYmxlLWhlYWRlci1mZzogY29sb3ItZmctaGVhZGluZyxcbiAgc21hcnQtdGFibGUtaGVhZGVyLWJnOiBjb2xvci1iZyxcblxuICBzbWFydC10YWJsZS1mb250LWZhbWlseTogZm9udC1tYWluLFxuICBzbWFydC10YWJsZS1mb250LXNpemU6IGZvbnQtc2l6ZSxcbiAgc21hcnQtdGFibGUtZm9udC13ZWlnaHQ6IGZvbnQtd2VpZ2h0LW5vcm1hbCxcbiAgc21hcnQtdGFibGUtbGluZS1oZWlnaHQ6IGxpbmUtaGVpZ2h0LFxuICBzbWFydC10YWJsZS1mZzogY29sb3ItZmctaGVhZGluZyxcbiAgc21hcnQtdGFibGUtYmc6IGNvbG9yLWJnLFxuXG4gIHNtYXJ0LXRhYmxlLWJnLWV2ZW46ICNmNWY3ZmMsXG4gIHNtYXJ0LXRhYmxlLWZnLXNlY29uZGFyeTogY29sb3ItZmcsXG4gIHNtYXJ0LXRhYmxlLWJnLWFjdGl2ZTogI2U2ZjNmZixcbiAgc21hcnQtdGFibGUtcGFkZGluZzogMC44NzVyZW0gMS4yNXJlbSxcbiAgc21hcnQtdGFibGUtZmlsdGVyLXBhZGRpbmc6IDAuMzc1cmVtIDAuNXJlbSxcbiAgc21hcnQtdGFibGUtc2VwYXJhdG9yOiBzZXBhcmF0b3IsXG4gIHNtYXJ0LXRhYmxlLWJvcmRlci1yYWRpdXM6IHJhZGl1cyxcblxuICBzbWFydC10YWJsZS1wYWdpbmctYm9yZGVyLWNvbG9yOiBzZXBhcmF0b3IsXG4gIHNtYXJ0LXRhYmxlLXBhZ2luZy1ib3JkZXItd2lkdGg6IDFweCxcbiAgc21hcnQtdGFibGUtcGFnaW5nLWZnLWFjdGl2ZTogI2ZmZmZmZixcbiAgc21hcnQtdGFibGUtcGFnaW5nLWJnLWFjdGl2ZTogY29sb3Itc3VjY2VzcyxcbiAgc21hcnQtdGFibGUtcGFnaW5nLWhvdmVyOiByZ2JhKDAsIDAsIDAsIDAuMDUpLFxuXG4gIHRvYXN0ZXItYmc6IGNvbG9yLXByaW1hcnksXG4gIHRvYXN0ZXItZmctZGVmYXVsdDogY29sb3ItaW52ZXJzZSxcbiAgdG9hc3Rlci1idG4tY2xvc2UtYmc6IHRyYW5zcGFyZW50LFxuICB0b2FzdGVyLWJ0bi1jbG9zZS1mZzogdG9hc3Rlci1mZy1kZWZhdWx0LFxuICB0b2FzdGVyLXNoYWRvdzogc2hhZG93LFxuXG4gIHRvYXN0ZXItZmc6IGNvbG9yLXdoaXRlLFxuICB0b2FzdGVyLXN1Y2Nlc3M6IGNvbG9yLXN1Y2Nlc3MsXG4gIHRvYXN0ZXItaW5mbzogY29sb3ItaW5mbyxcbiAgdG9hc3Rlci13YXJuaW5nOiBjb2xvci13YXJuaW5nLFxuICB0b2FzdGVyLXdhaXQ6IGNvbG9yLXByaW1hcnksXG4gIHRvYXN0ZXItZXJyb3I6IGNvbG9yLWRhbmdlcixcblxuICBidG4tZmc6IGNvbG9yLXdoaXRlLFxuICBidG4tZm9udC1mYW1pbHk6IGZvbnQtc2Vjb25kYXJ5LFxuICBidG4tbGluZS1oZWlnaHQ6IGxpbmUtaGVpZ2h0LFxuICBidG4tZGlzYWJsZWQtb3BhY2l0eTogMC4zLFxuICBidG4tY3Vyc29yOiBkZWZhdWx0LFxuXG4gIGJ0bi1wcmltYXJ5LWJnOiBjb2xvci1wcmltYXJ5LFxuICBidG4tc2Vjb25kYXJ5LWJnOiB0cmFuc3BhcmVudCxcbiAgYnRuLWluZm8tYmc6IGNvbG9yLWluZm8sXG4gIGJ0bi1zdWNjZXNzLWJnOiBjb2xvci1zdWNjZXNzLFxuICBidG4td2FybmluZy1iZzogY29sb3Itd2FybmluZyxcbiAgYnRuLWRhbmdlci1iZzogY29sb3ItZGFuZ2VyLFxuXG4gIGJ0bi1zZWNvbmRhcnktYm9yZGVyOiAjZGFkZmU2LFxuICBidG4tc2Vjb25kYXJ5LWJvcmRlci13aWR0aDogMnB4LFxuXG4gIGJ0bi1wYWRkaW5nLXktbGc6IDAuODc1cmVtLFxuICBidG4tcGFkZGluZy14LWxnOiAxLjc1cmVtLFxuICBidG4tZm9udC1zaXplLWxnOiBmb250LXNpemUtbGcsXG5cbiAgLy8gZGVmYXVsdCBzaXplXG4gIGJ0bi1wYWRkaW5nLXktbWQ6IDAuNzVyZW0sXG4gIGJ0bi1wYWRkaW5nLXgtbWQ6IDEuNXJlbSxcbiAgYnRuLWZvbnQtc2l6ZS1tZDogMXJlbSxcblxuICBidG4tcGFkZGluZy15LXNtOiAwLjYyNXJlbSxcbiAgYnRuLXBhZGRpbmcteC1zbTogMS41cmVtLFxuICBidG4tZm9udC1zaXplLXNtOiAwLjg3NXJlbSxcblxuICBidG4tcGFkZGluZy15LXhzOiAwLjVyZW0sXG4gIGJ0bi1wYWRkaW5nLXgteHM6IDEuMjVyZW0sXG4gIGJ0bi1mb250LXNpemUteHM6IDAuNzVyZW0sXG5cbiAgYnRuLWJvcmRlci1yYWRpdXM6IHJhZGl1cyxcbiAgYnRuLXJlY3RhbmdsZS1ib3JkZXItcmFkaXVzOiAwLjI1cmVtLFxuICBidG4tc2VtaS1yb3VuZC1ib3JkZXItcmFkaXVzOiAwLjc1cmVtLFxuICBidG4tcm91bmQtYm9yZGVyLXJhZGl1czogMS41cmVtLFxuXG4gIGJ0bi1oZXJvLXNoYWRvdzogbm9uZSxcbiAgYnRuLWhlcm8tdGV4dC1zaGFkb3c6IG5vbmUsXG4gIGJ0bi1oZXJvLWJldmVsLXNpemU6IDAgMCAwIDAsXG4gIGJ0bi1oZXJvLWdsb3ctc2l6ZTogMCAwIDAgMCxcbiAgYnRuLWhlcm8tcHJpbWFyeS1nbG93LXNpemU6IGJ0bi1oZXJvLWdsb3ctc2l6ZSxcbiAgYnRuLWhlcm8tc3VjY2Vzcy1nbG93LXNpemU6IGJ0bi1oZXJvLWdsb3ctc2l6ZSxcbiAgYnRuLWhlcm8td2FybmluZy1nbG93LXNpemU6IGJ0bi1oZXJvLWdsb3ctc2l6ZSxcbiAgYnRuLWhlcm8taW5mby1nbG93LXNpemU6IGJ0bi1oZXJvLWdsb3ctc2l6ZSxcbiAgYnRuLWhlcm8tZGFuZ2VyLWdsb3ctc2l6ZTogYnRuLWhlcm8tZ2xvdy1zaXplLFxuICBidG4taGVyby1zZWNvbmRhcnktZ2xvdy1zaXplOiBidG4taGVyby1nbG93LXNpemUsXG4gIGJ0bi1oZXJvLWRlZ3JlZTogMjBkZWcsXG4gIGJ0bi1oZXJvLXByaW1hcnktZGVncmVlOiBidG4taGVyby1kZWdyZWUsXG4gIGJ0bi1oZXJvLXN1Y2Nlc3MtZGVncmVlOiBidG4taGVyby1kZWdyZWUsXG4gIGJ0bi1oZXJvLXdhcm5pbmctZGVncmVlOiAxMGRlZyxcbiAgYnRuLWhlcm8taW5mby1kZWdyZWU6IC0xMGRlZyxcbiAgYnRuLWhlcm8tZGFuZ2VyLWRlZ3JlZTogLTIwZGVnLFxuICBidG4taGVyby1zZWNvbmRhcnktZGVncmVlOiBidG4taGVyby1kZWdyZWUsXG4gIGJ0bi1oZXJvLWJvcmRlci1yYWRpdXM6IHJhZGl1cyxcblxuICBidG4tb3V0bGluZS1mZzogY29sb3ItZmctaGVhZGluZyxcbiAgYnRuLW91dGxpbmUtaG92ZXItZmc6ICNmZmZmZmYsXG4gIGJ0bi1vdXRsaW5lLWZvY3VzLWZnOiBjb2xvci1mZy1oZWFkaW5nLFxuXG4gIGJ0bi1ncm91cC1iZzogbGF5b3V0LWJnLFxuICBidG4tZ3JvdXAtZmc6IGNvbG9yLWZnLWhlYWRpbmcsXG4gIGJ0bi1ncm91cC1zZXBhcmF0b3I6ICNkYWRmZTYsXG5cbiAgZm9ybS1jb250cm9sLXRleHQtcHJpbWFyeS1jb2xvcjogY29sb3ItZmctaGVhZGluZyxcbiAgZm9ybS1jb250cm9sLWJnOiBjb2xvci1iZyxcbiAgZm9ybS1jb250cm9sLWZvY3VzLWJnOiBjb2xvci1iZyxcblxuICBmb3JtLWNvbnRyb2wtYm9yZGVyLXdpZHRoOiAycHgsXG4gIGZvcm0tY29udHJvbC1ib3JkZXItdHlwZTogc29saWQsXG4gIGZvcm0tY29udHJvbC1ib3JkZXItcmFkaXVzOiByYWRpdXMsXG4gIGZvcm0tY29udHJvbC1zZW1pLXJvdW5kLWJvcmRlci1yYWRpdXM6IDAuNzVyZW0sXG4gIGZvcm0tY29udHJvbC1yb3VuZC1ib3JkZXItcmFkaXVzOiAxLjVyZW0sXG4gIGZvcm0tY29udHJvbC1ib3JkZXItY29sb3I6ICNkYWRmZTYsXG4gIGZvcm0tY29udHJvbC1zZWxlY3RlZC1ib3JkZXItY29sb3I6IGNvbG9yLXN1Y2Nlc3MsXG5cbiAgZm9ybS1jb250cm9sLWluZm8tYm9yZGVyLWNvbG9yOiBjb2xvci1pbmZvLFxuICBmb3JtLWNvbnRyb2wtc3VjY2Vzcy1ib3JkZXItY29sb3I6IGNvbG9yLXN1Y2Nlc3MsXG4gIGZvcm0tY29udHJvbC1kYW5nZXItYm9yZGVyLWNvbG9yOiBjb2xvci1kYW5nZXIsXG4gIGZvcm0tY29udHJvbC13YXJuaW5nLWJvcmRlci1jb2xvcjogY29sb3Itd2FybmluZyxcblxuICBmb3JtLWNvbnRyb2wtcGxhY2Vob2xkZXItY29sb3I6IGNvbG9yLWZnLFxuICBmb3JtLWNvbnRyb2wtcGxhY2Vob2xkZXItZm9udC1zaXplOiAxcmVtLFxuXG4gIGZvcm0tY29udHJvbC1mb250LXNpemU6IDFyZW0sXG4gIGZvcm0tY29udHJvbC1wYWRkaW5nOiAwLjc1cmVtIDEuMTI1cmVtLFxuICBmb3JtLWNvbnRyb2wtZm9udC1zaXplLXNtOiBmb250LXNpemUtc20sXG4gIGZvcm0tY29udHJvbC1wYWRkaW5nLXNtOiAwLjM3NXJlbSAxLjEyNXJlbSxcbiAgZm9ybS1jb250cm9sLWZvbnQtc2l6ZS1sZzogZm9udC1zaXplLWxnLFxuICBmb3JtLWNvbnRyb2wtcGFkZGluZy1sZzogMS4xMjVyZW0sXG5cbiAgZm9ybS1jb250cm9sLWxhYmVsLWZvbnQtd2VpZ2h0OiA0MDAsXG5cbiAgZm9ybS1jb250cm9sLWZlZWRiYWNrLWZvbnQtc2l6ZTogMC44NzVyZW0sXG4gIGZvcm0tY29udHJvbC1mZWVkYmFjay1mb250LXdlaWdodDogZm9udC13ZWlnaHQtbm9ybWFsLFxuXG4gIGNoZWNrYm94LWJnOiB0cmFuc3BhcmVudCxcbiAgY2hlY2tib3gtc2l6ZTogMS4yNXJlbSxcbiAgY2hlY2tib3gtYm9yZGVyLXNpemU6IDJweCxcbiAgY2hlY2tib3gtYm9yZGVyLWNvbG9yOiBmb3JtLWNvbnRyb2wtYm9yZGVyLWNvbG9yLFxuICBjaGVja2JveC1jaGVja21hcms6IHRyYW5zcGFyZW50LFxuXG4gIGNoZWNrYm94LWNoZWNrZWQtYmc6IHRyYW5zcGFyZW50LFxuICBjaGVja2JveC1jaGVja2VkLXNpemU6IDEuMjVyZW0sXG4gIGNoZWNrYm94LWNoZWNrZWQtYm9yZGVyLXNpemU6IDJweCxcbiAgY2hlY2tib3gtY2hlY2tlZC1ib3JkZXItY29sb3I6IGNvbG9yLXN1Y2Nlc3MsXG4gIGNoZWNrYm94LWNoZWNrZWQtY2hlY2ttYXJrOiBjb2xvci1mZy1oZWFkaW5nLFxuXG4gIGNoZWNrYm94LWRpc2FibGVkLWJnOiB0cmFuc3BhcmVudCxcbiAgY2hlY2tib3gtZGlzYWJsZWQtc2l6ZTogMS4yNXJlbSxcbiAgY2hlY2tib3gtZGlzYWJsZWQtYm9yZGVyLXNpemU6IDJweCxcbiAgY2hlY2tib3gtZGlzYWJsZWQtYm9yZGVyLWNvbG9yOiBjb2xvci1mZy1oZWFkaW5nLFxuICBjaGVja2JveC1kaXNhYmxlZC1jaGVja21hcms6IGNvbG9yLWZnLWhlYWRpbmcsXG5cbiAgcmFkaW8tZmc6IGNvbG9yLXN1Y2Nlc3MsXG5cbiAgbW9kYWwtZm9udC1zaXplOiBmb250LXNpemUsXG4gIG1vZGFsLWxpbmUtaGVpZ2h0OiBsaW5lLWhlaWdodCxcbiAgbW9kYWwtZm9udC13ZWlnaHQ6IGZvbnQtd2VpZ2h0LW5vcm1hbCxcbiAgbW9kYWwtZmc6IGNvbG9yLWZnLXRleHQsXG4gIG1vZGFsLWZnLWhlYWRpbmc6IGNvbG9yLWZnLWhlYWRpbmcsXG4gIG1vZGFsLWJnOiBjb2xvci1iZyxcbiAgbW9kYWwtYm9yZGVyOiB0cmFuc3BhcmVudCxcbiAgbW9kYWwtYm9yZGVyLXJhZGl1czogcmFkaXVzLFxuICBtb2RhbC1wYWRkaW5nOiBwYWRkaW5nLFxuICBtb2RhbC1oZWFkZXItZm9udC1mYW1pbHk6IGZvbnQtc2Vjb25kYXJ5LFxuICBtb2RhbC1oZWFkZXItZm9udC13ZWlnaHQ6IGZvbnQtd2VpZ2h0LWJvbGRlcixcbiAgbW9kYWwtaGVhZGVyLWZvbnQtc2l6ZTogZm9udC1zaXplLWxnLFxuICBtb2RhbC1ib2R5LWZvbnQtZmFtaWx5OiBmb250LW1haW4sXG4gIG1vZGFsLWJvZHktZm9udC13ZWlnaHQ6IGZvbnQtd2VpZ2h0LW5vcm1hbCxcbiAgbW9kYWwtYm9keS1mb250LXNpemU6IGZvbnQtc2l6ZSxcbiAgbW9kYWwtc2VwYXJhdG9yOiBzZXBhcmF0b3IsXG5cbiAgYmFkZ2UtZmctdGV4dDogY29sb3Itd2hpdGUsXG4gIGJhZGdlLXByaW1hcnktYmctY29sb3I6IGNvbG9yLXByaW1hcnksXG4gIGJhZGdlLXN1Y2Nlc3MtYmctY29sb3I6IGNvbG9yLXN1Y2Nlc3MsXG4gIGJhZGdlLWluZm8tYmctY29sb3I6IGNvbG9yLWluZm8sXG4gIGJhZGdlLXdhcm5pbmctYmctY29sb3I6IGNvbG9yLXdhcm5pbmcsXG4gIGJhZGdlLWRhbmdlci1iZy1jb2xvcjogY29sb3ItZGFuZ2VyLFxuXG4gIHByb2dyZXNzLWJhci1oZWlnaHQteGxnOiAxLjc1cmVtLFxuICBwcm9ncmVzcy1iYXItaGVpZ2h0LWxnOiAxLjVyZW0sXG4gIHByb2dyZXNzLWJhci1oZWlnaHQ6IDEuMzc1cmVtLFxuICBwcm9ncmVzcy1iYXItaGVpZ2h0LXNtOiAxLjI1cmVtLFxuICBwcm9ncmVzcy1iYXItaGVpZ2h0LXhzOiAxcmVtLFxuICBwcm9ncmVzcy1iYXItYW5pbWF0aW9uLWR1cmF0aW9uOiA0MDBtcyxcbiAgcHJvZ3Jlc3MtYmFyLWZvbnQtc2l6ZS14bGc6IGZvbnQtc2l6ZS14bGcsXG4gIHByb2dyZXNzLWJhci1mb250LXNpemUtbGc6IGZvbnQtc2l6ZS1sZyxcbiAgcHJvZ3Jlc3MtYmFyLWZvbnQtc2l6ZTogZm9udC1zaXplLFxuICBwcm9ncmVzcy1iYXItZm9udC1zaXplLXNtOiBmb250LXNpemUtc20sXG4gIHByb2dyZXNzLWJhci1mb250LXNpemUteHM6IGZvbnQtc2l6ZS14cyxcbiAgcHJvZ3Jlc3MtYmFyLXJhZGl1czogcmFkaXVzLFxuICBwcm9ncmVzcy1iYXItYmc6IGxheW91dC1iZyxcbiAgcHJvZ3Jlc3MtYmFyLWZvbnQtY29sb3I6IGNvbG9yLXdoaXRlLFxuICBwcm9ncmVzcy1iYXItZm9udC13ZWlnaHQ6IGZvbnQtd2VpZ2h0LWJvbGQsXG4gIHByb2dyZXNzLWJhci1kZWZhdWx0LWJnOiBjb2xvci1pbmZvLFxuICBwcm9ncmVzcy1iYXItcHJpbWFyeS1iZzogY29sb3ItcHJpbWFyeSxcbiAgcHJvZ3Jlc3MtYmFyLXN1Y2Nlc3MtYmc6IGNvbG9yLXN1Y2Nlc3MsXG4gIHByb2dyZXNzLWJhci1pbmZvLWJnOiBjb2xvci1pbmZvLFxuICBwcm9ncmVzcy1iYXItd2FybmluZy1iZzogY29sb3Itd2FybmluZyxcbiAgcHJvZ3Jlc3MtYmFyLWRhbmdlci1iZzogY29sb3ItZGFuZ2VyLFxuXG4gIGFsZXJ0LWZvbnQtc2l6ZTogZm9udC1zaXplLFxuICBhbGVydC1saW5lLWhlaWdodDogbGluZS1oZWlnaHQsXG4gIGFsZXJ0LWZvbnQtd2VpZ2h0OiBmb250LXdlaWdodC1ub3JtYWwsXG4gIGFsZXJ0LWZnOiBjb2xvci13aGl0ZSxcbiAgYWxlcnQtb3V0bGluZS1mZzogY29sb3ItZmcsXG4gIGFsZXJ0LWJnOiBjb2xvci1iZyxcbiAgYWxlcnQtYWN0aXZlLWJnOiBjb2xvci1mZyxcbiAgYWxlcnQtZGlzYWJsZWQtYmc6IGNvbG9yLWRpc2FibGVkLFxuICBhbGVydC1kaXNhYmxlZC1mZzogY29sb3ItZmcsXG4gIGFsZXJ0LXByaW1hcnktYmc6IGNvbG9yLXByaW1hcnksXG4gIGFsZXJ0LWluZm8tYmc6IGNvbG9yLWluZm8sXG4gIGFsZXJ0LXN1Y2Nlc3MtYmc6IGNvbG9yLXN1Y2Nlc3MsXG4gIGFsZXJ0LXdhcm5pbmctYmc6IGNvbG9yLXdhcm5pbmcsXG4gIGFsZXJ0LWRhbmdlci1iZzogY29sb3ItZGFuZ2VyLFxuICBhbGVydC1oZWlnaHQteHhzbWFsbDogNTJweCxcbiAgYWxlcnQtaGVpZ2h0LXhzbWFsbDogNzJweCxcbiAgYWxlcnQtaGVpZ2h0LXNtYWxsOiA5MnB4LFxuICBhbGVydC1oZWlnaHQtbWVkaXVtOiAxMTJweCxcbiAgYWxlcnQtaGVpZ2h0LWxhcmdlOiAxMzJweCxcbiAgYWxlcnQtaGVpZ2h0LXhsYXJnZTogMTUycHgsXG4gIGFsZXJ0LWhlaWdodC14eGxhcmdlOiAxNzJweCxcbiAgYWxlcnQtc2hhZG93OiBub25lLFxuICBhbGVydC1ib3JkZXItcmFkaXVzOiByYWRpdXMsXG4gIGFsZXJ0LXBhZGRpbmc6IDFyZW0gMS4xMjVyZW0sXG4gIGFsZXJ0LWNsb3NhYmxlLXBhZGRpbmc6IDNyZW0sXG4gIGFsZXJ0LWJ1dHRvbi1wYWRkaW5nOiAzcmVtLFxuICBhbGVydC1tYXJnaW46IG1hcmdpbixcblxuICBjaGF0LWZvbnQtc2l6ZTogZm9udC1zaXplLFxuICBjaGF0LWZnOiBjb2xvci13aGl0ZSxcbiAgY2hhdC1iZzogY29sb3ItYmcsXG4gIGNoYXQtYm9yZGVyLXJhZGl1czogcmFkaXVzLFxuICBjaGF0LWZnLXRleHQ6IGNvbG9yLWZnLXRleHQsXG4gIGNoYXQtaGVpZ2h0LXh4c21hbGw6IDk2cHgsXG4gIGNoYXQtaGVpZ2h0LXhzbWFsbDogMjE2cHgsXG4gIGNoYXQtaGVpZ2h0LXNtYWxsOiAzMzZweCxcbiAgY2hhdC1oZWlnaHQtbWVkaXVtOiA0NTZweCxcbiAgY2hhdC1oZWlnaHQtbGFyZ2U6IDU3NnB4LFxuICBjaGF0LWhlaWdodC14bGFyZ2U6IDY5NnB4LFxuICBjaGF0LWhlaWdodC14eGxhcmdlOiA4MTZweCxcbiAgY2hhdC1ib3JkZXI6IGJvcmRlcixcbiAgY2hhdC1wYWRkaW5nOiBwYWRkaW5nLFxuICBjaGF0LXNoYWRvdzogc2hhZG93LFxuICBjaGF0LXNlcGFyYXRvcjogc2VwYXJhdG9yLFxuICBjaGF0LW1lc3NhZ2UtZmc6IGNvbG9yLXdoaXRlLFxuICBjaGF0LW1lc3NhZ2UtYmc6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgIzRjYTZmZiwgIzU5YmZmZiksXG4gIGNoYXQtbWVzc2FnZS1yZXBseS1iZzogY29sb3ItYmctYWN0aXZlLFxuICBjaGF0LW1lc3NhZ2UtcmVwbHktZmc6IGNvbG9yLWZnLXRleHQsXG4gIGNoYXQtbWVzc2FnZS1hdmF0YXItYmc6IGNvbG9yLWZnLFxuICBjaGF0LW1lc3NhZ2Utc2VuZGVyLWZnOiBjb2xvci1mZyxcbiAgY2hhdC1tZXNzYWdlLXF1b3RlLWZnOiBjb2xvci1mZyxcbiAgY2hhdC1tZXNzYWdlLXF1b3RlLWJnOiBjb2xvci1iZy1hY3RpdmUsXG4gIGNoYXQtbWVzc2FnZS1maWxlLWZnOiBjb2xvci1mZyxcbiAgY2hhdC1tZXNzYWdlLWZpbGUtYmc6IHRyYW5zcGFyZW50LFxuICBjaGF0LWZvcm0tYmc6IHRyYW5zcGFyZW50LFxuICBjaGF0LWZvcm0tZmc6IGNvbG9yLWZnLWhlYWRpbmcsXG4gIGNoYXQtZm9ybS1ib3JkZXI6IHNlcGFyYXRvcixcbiAgY2hhdC1mb3JtLXBsYWNlaG9sZGVyLWZnOiBjb2xvci1mZyxcbiAgY2hhdC1mb3JtLWFjdGl2ZS1ib3JkZXI6IGNvbG9yLWZnLFxuICBjaGF0LWFjdGl2ZS1iZzogY29sb3ItZmcsXG4gIGNoYXQtZGlzYWJsZWQtYmc6IGNvbG9yLWRpc2FibGVkLFxuICBjaGF0LWRpc2FibGVkLWZnOiBjb2xvci1mZyxcbiAgY2hhdC1wcmltYXJ5LWJnOiBjb2xvci1wcmltYXJ5LFxuICBjaGF0LWluZm8tYmc6IGNvbG9yLWluZm8sXG4gIGNoYXQtc3VjY2Vzcy1iZzogY29sb3Itc3VjY2VzcyxcbiAgY2hhdC13YXJuaW5nLWJnOiBjb2xvci13YXJuaW5nLFxuICBjaGF0LWRhbmdlci1iZzogY29sb3ItZGFuZ2VyLFxuXG4gIHNwaW5uZXItYmc6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC44MyksXG4gIHNwaW5uZXItY2lyY2xlLWJnOiBjb2xvci1iZy1hY3RpdmUsXG4gIHNwaW5uZXItZmc6IGNvbG9yLWZnLXRleHQsXG4gIHNwaW5uZXItYWN0aXZlLWJnOiBjb2xvci1mZyxcbiAgc3Bpbm5lci1kaXNhYmxlZC1iZzogY29sb3ItZGlzYWJsZWQsXG4gIHNwaW5uZXItZGlzYWJsZWQtZmc6IGNvbG9yLWZnLFxuICBzcGlubmVyLXByaW1hcnktYmc6IGNvbG9yLXByaW1hcnksXG4gIHNwaW5uZXItaW5mby1iZzogY29sb3ItaW5mbyxcbiAgc3Bpbm5lci1zdWNjZXNzLWJnOiBjb2xvci1zdWNjZXNzLFxuICBzcGlubmVyLXdhcm5pbmctYmc6IGNvbG9yLXdhcm5pbmcsXG4gIHNwaW5uZXItZGFuZ2VyLWJnOiBjb2xvci1kYW5nZXIsXG4gIHNwaW5uZXIteHhzbWFsbDogMS4yNXJlbSxcbiAgc3Bpbm5lci14c21hbGw6IDEuNXJlbSxcbiAgc3Bpbm5lci1zbWFsbDogMS43NXJlbSxcbiAgc3Bpbm5lci1tZWRpdW06IDJyZW0sXG4gIHNwaW5uZXItbGFyZ2U6IDIuMjVyZW0sXG4gIHNwaW5uZXIteGxhcmdlOiAyLjVyZW0sXG4gIHNwaW5uZXIteHhsYXJnZTogM3JlbSxcblxuICBzdGVwcGVyLWluZGV4LXNpemU6IDJyZW0sXG4gIHN0ZXBwZXItbGFiZWwtZm9udC1zaXplOiBmb250LXNpemUtc20sXG4gIHN0ZXBwZXItbGFiZWwtZm9udC13ZWlnaHQ6IGZvbnQtd2VpZ2h0LWJvbGRlcixcbiAgc3RlcHBlci1hY2NlbnQtY29sb3I6IGNvbG9yLXByaW1hcnksXG4gIHN0ZXBwZXItY29tcGxldGVkLWZnOiBjb2xvci13aGl0ZSxcbiAgc3RlcHBlci1mZzogY29sb3ItZmcsXG4gIHN0ZXBwZXItY29tcGxldGVkLWljb24tc2l6ZTogMS41cmVtLFxuICBzdGVwcGVyLWNvbXBsZXRlZC1pY29uLXdlaWdodDogZm9udC13ZWlnaHQtdWx0cmEtYm9sZCxcbiAgc3RlcHBlci1zdGVwLXBhZGRpbmc6IHBhZGRpbmcsXG5cbiAgYWNjb3JkaW9uLXBhZGRpbmc6IHBhZGRpbmcsXG4gIGFjY29yZGlvbi1zZXBhcmF0b3I6IHNlcGFyYXRvcixcbiAgYWNjb3JkaW9uLWhlYWRlci1mb250LWZhbWlseTogZm9udC1zZWNvbmRhcnksXG4gIGFjY29yZGlvbi1oZWFkZXItZm9udC1zaXplOiBmb250LXNpemUtbGcsXG4gIGFjY29yZGlvbi1oZWFkZXItZm9udC13ZWlnaHQ6IGZvbnQtd2VpZ2h0LW5vcm1hbCxcbiAgYWNjb3JkaW9uLWhlYWRlci1mZy1oZWFkaW5nOiBjb2xvci1mZy1oZWFkaW5nLFxuICBhY2NvcmRpb24taGVhZGVyLWRpc2FibGVkLWZnOiBjb2xvci1mZyxcbiAgYWNjb3JkaW9uLWhlYWRlci1ib3JkZXItd2lkdGg6IDFweCxcbiAgYWNjb3JkaW9uLWhlYWRlci1ib3JkZXItdHlwZTogc29saWQsXG4gIGFjY29yZGlvbi1oZWFkZXItYm9yZGVyLWNvbG9yOiBhY2NvcmRpb24tc2VwYXJhdG9yLFxuICBhY2NvcmRpb24tYm9yZGVyLXJhZGl1czogcmFkaXVzLFxuICBhY2NvcmRpb24taXRlbS1iZzogY29sb3ItYmcsXG4gIGFjY29yZGlvbi1pdGVtLWZvbnQtc2l6ZTogZm9udC1zaXplLFxuICBhY2NvcmRpb24taXRlbS1mb250LXdlaWdodDogZm9udC13ZWlnaHQtbm9ybWFsLFxuICBhY2NvcmRpb24taXRlbS1mb250LWZhbWlseTogZm9udC1tYWluLFxuICBhY2NvcmRpb24taXRlbS1mZy10ZXh0OiBjb2xvci1mZy10ZXh0LFxuICBhY2NvcmRpb24taXRlbS1zaGFkb3c6IHNoYWRvdyxcblxuICBsaXN0LWl0ZW0tYm9yZGVyLWNvbG9yOiB0YWJzLXNlcGFyYXRvcixcbiAgbGlzdC1pdGVtLXBhZGRpbmc6IDFyZW0sXG5cbiAgY2FsZW5kYXItd2lkdGg6IDIxLjg3NXJlbSxcbiAgY2FsZW5kYXItaGVpZ2h0OiAzMXJlbSxcbiAgY2FsZW5kYXItaGVhZGVyLXRpdGxlLWZvbnQtc2l6ZTogZm9udC1zaXplLXhsZyxcbiAgY2FsZW5kYXItaGVhZGVyLXRpdGxlLWZvbnQtd2VpZ2h0OiBmb250LXdlaWdodC1ib2xkLFxuICBjYWxlbmRhci1oZWFkZXItc3ViLXRpdGxlLWZvbnQtc2l6ZTogZm9udC1zaXplLFxuICBjYWxlbmRhci1oZWFkZXItc3ViLXRpdGxlLWZvbnQtd2VpZ2h0OiBmb250LXdlaWdodC10aGluLFxuICBjYWxlbmRhci1uYXZpZ2F0aW9uLWJ1dHRvbi13aWR0aDogMTByZW0sXG4gIGNhbGVuZGFyLXNlbGVjdGVkLWl0ZW0tYmc6IGNvbG9yLXN1Y2Nlc3MsXG4gIGNhbGVuZGFyLWhvdmVyLWl0ZW0tYmc6IGNhbGVuZGFyLXNlbGVjdGVkLWl0ZW0tYmcsXG4gIGNhbGVuZGFyLXRvZGF5LWl0ZW0tYmc6IGNvbG9yLWJnLWFjdGl2ZSxcbiAgY2FsZW5kYXItYWN0aXZlLWl0ZW0tYmc6IGNvbG9yLXN1Y2Nlc3MsXG4gIGNhbGVuZGFyLWZnOiBjb2xvci1mZy10ZXh0LFxuICBjYWxlbmRhci1zZWxlY3RlZC1mZzogY29sb3Itd2hpdGUsXG4gIGNhbGVuZGFyLXRvZGF5LWZnOiBjYWxlbmRhci1mZyxcbiAgY2FsZW5kYXItZGF5LWNlbGwtd2lkdGg6IDIuNjI1cmVtLFxuICBjYWxlbmRhci1kYXktY2VsbC1oZWlnaHQ6IDIuNjI1cmVtLFxuICBjYWxlbmRhci1tb250aC1jZWxsLXdpZHRoOiA0LjI1cmVtLFxuICBjYWxlbmRhci1tb250aC1jZWxsLWhlaWdodDogMi4zNzVyZW0sXG4gIGNhbGVuZGFyLXllYXItY2VsbC13aWR0aDogY2FsZW5kYXItbW9udGgtY2VsbC13aWR0aCxcbiAgY2FsZW5kYXIteWVhci1jZWxsLWhlaWdodDogY2FsZW5kYXItbW9udGgtY2VsbC1oZWlnaHQsXG4gIGNhbGVuZGFyLWluYWN0aXZlLW9wYWNpdHk6IDAuNSxcbiAgY2FsZW5kYXItZGlzYWJsZWQtb3BhY2l0eTogMC4zLFxuICBjYWxlbmRhci1ib3JkZXItcmFkaXVzOiByYWRpdXMsXG4gIGNhbGVuZGFyLXdlZWtkYXktd2lkdGg6IGNhbGVuZGFyLWRheS1jZWxsLXdpZHRoLFxuICBjYWxlbmRhci13ZWVrZGF5LWhlaWdodDogMS43NXJlbSxcbiAgY2FsZW5kYXItd2Vla2RheS1mb250LXNpemU6IGZvbnQtc2l6ZS14cyxcbiAgY2FsZW5kYXItd2Vla2RheS1mb250LXdlaWdodDogZm9udC13ZWlnaHQtbm9ybWFsLFxuICBjYWxlbmRhci13ZWVrZGF5LWZnOiBjb2xvci1mZyxcbiAgY2FsZW5kYXItd2Vla2RheS1ob2xpZGF5LWZnOiBjb2xvci1kYW5nZXIsXG4gIGNhbGVuZGFyLXJhbmdlLWJnLWluLXJhbmdlOiAjZWJmYmYyLFxuXG4gIGNhbGVuZGFyLWxhcmdlLXdpZHRoOiAyNC4zNzVyZW0sXG4gIGNhbGVuZGFyLWxhcmdlLWhlaWdodDogMzMuMTI1cmVtLFxuICBjYWxlbmRhci1kYXktY2VsbC1sYXJnZS13aWR0aDogM3JlbSxcbiAgY2FsZW5kYXItZGF5LWNlbGwtbGFyZ2UtaGVpZ2h0OiAzcmVtLFxuICBjYWxlbmRhci1tb250aC1jZWxsLWxhcmdlLXdpZHRoOiA0LjI1cmVtLFxuICBjYWxlbmRhci1tb250aC1jZWxsLWxhcmdlLWhlaWdodDogMi4zNzVyZW0sXG4gIGNhbGVuZGFyLXllYXItY2VsbC1sYXJnZS13aWR0aDogY2FsZW5kYXItbW9udGgtY2VsbC13aWR0aCxcbiAgY2FsZW5kYXIteWVhci1jZWxsLWxhcmdlLWhlaWdodDogY2FsZW5kYXItbW9udGgtY2VsbC1oZWlnaHQsXG4pO1xuXG4vLyByZWdpc3RlciB0aGUgdGhlbWVcbiRuYi10aGVtZXM6IG5iLXJlZ2lzdGVyLXRoZW1lKCR0aGVtZSwgZGVmYXVsdCk7XG4iLCIvKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG5cbkBpbXBvcnQgJy4uL2NvcmUvZnVuY3Rpb25zJztcbkBpbXBvcnQgJy4uL2NvcmUvbWl4aW5zJztcbkBpbXBvcnQgJ2RlZmF1bHQnO1xuXG4vLyBkZWZhdWx0IHRoZSBiYXNlIHRoZW1lXG4kdGhlbWU6IChcbiAgcmFkaXVzOiAwLjVyZW0sXG5cbiAgY29sb3ItYmc6ICMzZDM3ODAsXG4gIGNvbG9yLWJnLWFjdGl2ZTogIzQ5NDI5OSxcbiAgY29sb3ItZmc6ICNhMWExZTUsXG4gIGNvbG9yLWZnLWhlYWRpbmc6ICNmZmZmZmYsXG4gIGNvbG9yLWZnLXRleHQ6ICNkMWQxZmYsXG4gIGNvbG9yLWZnLWhpZ2hsaWdodDogIzAwZjlhNixcblxuICBjb2xvci1ncmF5OiByZ2JhKDgxLCAxMTMsIDE2NSwgMC4xNSksXG4gIGNvbG9yLW5ldXRyYWw6IHRyYW5zcGFyZW50LFxuICBjb2xvci13aGl0ZTogI2ZmZmZmZixcbiAgY29sb3ItZGlzYWJsZWQ6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC40KSxcblxuICBjb2xvci1wcmltYXJ5OiAjNzY1OWZmLFxuICBjb2xvci1zdWNjZXNzOiAjMDBkOTc3LFxuICBjb2xvci1pbmZvOiAjMDA4OGZmLFxuICBjb2xvci13YXJuaW5nOiAjZmZhMTAwLFxuICBjb2xvci1kYW5nZXI6ICNmZjM4NmEsXG5cbiAgbGluay1jb2xvcjogIzAwZjlhNixcbiAgbGluay1jb2xvci1ob3ZlcjogIzE0ZmZiZSxcblxuICBzZXBhcmF0b3I6ICMzNDJlNzMsXG4gIHNoYWRvdzogMCA4cHggMjBweCAwIHJnYmEoNDAsIDM3LCA4OSwgMC42KSxcblxuICBjYXJkLWhlYWRlci1mb250LXdlaWdodDogZm9udC13ZWlnaHQtYm9sZGVyLFxuXG4gIGxheW91dC1iZzogIzJmMjk2YixcblxuICBzY3JvbGxiYXItZmc6ICM1NTRkYjMsXG4gIHNjcm9sbGJhci1iZzogIzMzMmU3MyxcblxuICByYWRpYWwtZ3JhZGllbnQ6IHJhZGlhbC1ncmFkaWVudChjaXJjbGUgYXQgNTAlIDUwJSwgIzQyM2Y4YywgIzMwMmM2ZSksXG4gIGxpbmVhci1ncmFkaWVudDogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjMTcxNzQ5LCAjNDEzNzg5KSxcblxuICBzaWRlYmFyLWZnOiBjb2xvci1zZWNvbmRhcnksXG4gIHNpZGViYXItYmc6IGNvbG9yLWJnLFxuXG4gIGhlYWRlci1mZzogY29sb3Itd2hpdGUsXG4gIGhlYWRlci1iZzogY29sb3ItYmcsXG5cbiAgZm9vdGVyLWZnOiBjb2xvci1mZyxcbiAgZm9vdGVyLWJnOiBjb2xvci1iZyxcblxuICBhY3Rpb25zLWZnOiBjb2xvci1mZyxcbiAgYWN0aW9ucy1iZzogY29sb3ItYmcsXG5cbiAgdXNlci1mZzogY29sb3ItYmcsXG4gIHVzZXItYmc6IGNvbG9yLWZnLFxuICB1c2VyLWZnLWhpZ2hsaWdodDogY29sb3ItZmctaGlnaGxpZ2h0LFxuXG4gIHBvcG92ZXItYm9yZGVyOiBjb2xvci1wcmltYXJ5LFxuICBwb3BvdmVyLXNoYWRvdzogc2hhZG93LFxuXG4gIGNvbnRleHQtbWVudS1hY3RpdmUtYmc6IGNvbG9yLXByaW1hcnksXG5cbiAgZm9vdGVyLWhlaWdodDogaGVhZGVyLWhlaWdodCxcblxuICBzaWRlYmFyLXdpZHRoOiAxNi4yNXJlbSxcbiAgc2lkZWJhci13aWR0aC1jb21wYWN0OiAzLjQ1cmVtLFxuXG4gIG1lbnUtZmc6IGNvbG9yLWZnLFxuICBtZW51LWJnOiBjb2xvci1iZyxcbiAgbWVudS1hY3RpdmUtZmc6IGNvbG9yLXdoaXRlLFxuICBtZW51LWdyb3VwLWZnOiBjb2xvci13aGl0ZSxcbiAgbWVudS1mb250LXdlaWdodDogZm9udC13ZWlnaHQtbm9ybWFsLFxuICBtZW51LWFjdGl2ZS1mb250LXdlaWdodDogZm9udC13ZWlnaHQtYm9sZGVyLFxuICBtZW51LXN1Ym1lbnUtYmc6IGxheW91dC1iZyxcbiAgbWVudS1zdWJtZW51LWZnOiBjb2xvci1mZyxcbiAgbWVudS1zdWJtZW51LWFjdGl2ZS1mZzogY29sb3ItZmctaGVhZGluZyxcbiAgbWVudS1zdWJtZW51LWFjdGl2ZS1iZzogcmdiYSgwLCAyNTUsIDE3MCwgMC4yNSksXG4gIG1lbnUtc3VibWVudS1hY3RpdmUtYm9yZGVyLWNvbG9yOiBjb2xvci1mZy1oaWdobGlnaHQsXG4gIG1lbnUtc3VibWVudS1hY3RpdmUtc2hhZG93OiAwIDJweCAxMnB4IDAgcmdiYSgwLCAyNTUsIDE3MCwgMC4yNSksXG4gIG1lbnUtaXRlbS1wYWRkaW5nOiAwLjI1cmVtIDAuNzVyZW0sXG4gIG1lbnUtaXRlbS1zZXBhcmF0b3I6IHRyYW5zcGFyZW50LFxuXG4gIGJ0bi1oZXJvLXNoYWRvdzogMCA0cHggMTBweCAwIHJnYmEoMzMsIDcsIDc3LCAwLjUpLFxuICBidG4taGVyby10ZXh0LXNoYWRvdzogMCAxcHggM3B4IHJnYmEoMCwgMCwgMCwgMC4zKSxcbiAgYnRuLWhlcm8tYmV2ZWwtc2l6ZTogMCAzcHggMCAwLFxuICBidG4taGVyby1nbG93LXNpemU6IDAgMnB4IDhweCAwLFxuICBidG4taGVyby1wcmltYXJ5LWdsb3ctc2l6ZTogYnRuLWhlcm8tZ2xvdy1zaXplLFxuICBidG4taGVyby1zdWNjZXNzLWdsb3ctc2l6ZTogYnRuLWhlcm8tZ2xvdy1zaXplLFxuICBidG4taGVyby13YXJuaW5nLWdsb3ctc2l6ZTogYnRuLWhlcm8tZ2xvdy1zaXplLFxuICBidG4taGVyby1pbmZvLWdsb3ctc2l6ZTogYnRuLWhlcm8tZ2xvdy1zaXplLFxuICBidG4taGVyby1kYW5nZXItZ2xvdy1zaXplOiBidG4taGVyby1nbG93LXNpemUsXG4gIGJ0bi1oZXJvLXNlY29uZGFyeS1nbG93LXNpemU6IGJ0bi1oZXJvLWdsb3ctc2l6ZSxcbiAgYnRuLXNlY29uZGFyeS1ib3JkZXI6IGNvbG9yLXByaW1hcnksXG4gIGJ0bi1vdXRsaW5lLWZnOiBjb2xvci1mZy1oZWFkaW5nLFxuICBidG4tb3V0bGluZS1ob3Zlci1mZzogY29sb3ItZmctaGVhZGluZyxcbiAgYnRuLW91dGxpbmUtZm9jdXMtZmc6IGNvbG9yLWZnLWhlYWRpbmcsXG4gIGJ0bi1ncm91cC1iZzogIzM3MzI3MyxcbiAgYnRuLWdyb3VwLXNlcGFyYXRvcjogIzMxMmM2NixcblxuICBmb3JtLWNvbnRyb2wtYmc6ICMzNzMxN2EsXG4gIGZvcm0tY29udHJvbC1mb2N1cy1iZzogc2VwYXJhdG9yLFxuICBmb3JtLWNvbnRyb2wtYm9yZGVyLWNvbG9yOiBzZXBhcmF0b3IsXG4gIGZvcm0tY29udHJvbC1zZWxlY3RlZC1ib3JkZXItY29sb3I6IGNvbG9yLXByaW1hcnksXG5cbiAgY2hlY2tib3gtYmc6IHRyYW5zcGFyZW50LFxuICBjaGVja2JveC1zaXplOiAxLjI1cmVtLFxuICBjaGVja2JveC1ib3JkZXItc2l6ZTogMnB4LFxuICBjaGVja2JveC1ib3JkZXItY29sb3I6IGNvbG9yLWZnLFxuICBjaGVja2JveC1jaGVja21hcms6IHRyYW5zcGFyZW50LFxuXG4gIGNoZWNrYm94LWNoZWNrZWQtYmc6IHRyYW5zcGFyZW50LFxuICBjaGVja2JveC1jaGVja2VkLXNpemU6IDEuMjVyZW0sXG4gIGNoZWNrYm94LWNoZWNrZWQtYm9yZGVyLXNpemU6IDJweCxcbiAgY2hlY2tib3gtY2hlY2tlZC1ib3JkZXItY29sb3I6IGNvbG9yLXN1Y2Nlc3MsXG4gIGNoZWNrYm94LWNoZWNrZWQtY2hlY2ttYXJrOiBjb2xvci1mZy1oZWFkaW5nLFxuXG4gIGNoZWNrYm94LWRpc2FibGVkLWJnOiB0cmFuc3BhcmVudCxcbiAgY2hlY2tib3gtZGlzYWJsZWQtc2l6ZTogMS4yNXJlbSxcbiAgY2hlY2tib3gtZGlzYWJsZWQtYm9yZGVyLXNpemU6IDJweCxcbiAgY2hlY2tib3gtZGlzYWJsZWQtYm9yZGVyLWNvbG9yOiBjb2xvci1mZy1oZWFkaW5nLFxuICBjaGVja2JveC1kaXNhYmxlZC1jaGVja21hcms6IGNvbG9yLWZnLWhlYWRpbmcsXG5cbiAgc2VhcmNoLWJnOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICMxNzE3NDksICM0MTM3ODkpLFxuXG4gIHNtYXJ0LXRhYmxlLWhlYWRlci1mb250LXdlaWdodDogZm9udC13ZWlnaHQtbm9ybWFsLFxuICBzbWFydC10YWJsZS1oZWFkZXItYmc6IGNvbG9yLWJnLWFjdGl2ZSxcbiAgc21hcnQtdGFibGUtYmctZXZlbjogIzNhMzQ3YSxcbiAgc21hcnQtdGFibGUtYmctYWN0aXZlOiBjb2xvci1iZy1hY3RpdmUsXG5cbiAgc21hcnQtdGFibGUtcGFnaW5nLWJvcmRlci1jb2xvcjogY29sb3ItcHJpbWFyeSxcbiAgc21hcnQtdGFibGUtcGFnaW5nLWJvcmRlci13aWR0aDogMnB4LFxuICBzbWFydC10YWJsZS1wYWdpbmctZmctYWN0aXZlOiBjb2xvci1mZy1oZWFkaW5nLFxuICBzbWFydC10YWJsZS1wYWdpbmctYmctYWN0aXZlOiBjb2xvci1wcmltYXJ5LFxuICBzbWFydC10YWJsZS1wYWdpbmctaG92ZXI6IHJnYmEoMCwgMCwgMCwgMC4yKSxcblxuICBiYWRnZS1mZy10ZXh0OiBjb2xvci13aGl0ZSxcbiAgYmFkZ2UtcHJpbWFyeS1iZy1jb2xvcjogY29sb3ItcHJpbWFyeSxcbiAgYmFkZ2Utc3VjY2Vzcy1iZy1jb2xvcjogY29sb3Itc3VjY2VzcyxcbiAgYmFkZ2UtaW5mby1iZy1jb2xvcjogY29sb3ItaW5mbyxcbiAgYmFkZ2Utd2FybmluZy1iZy1jb2xvcjogY29sb3Itd2FybmluZyxcbiAgYmFkZ2UtZGFuZ2VyLWJnLWNvbG9yOiBjb2xvci1kYW5nZXIsXG5cbiAgc3Bpbm5lci1iZzogcmdiYSg2MSwgNTUsIDEyOCwgMC45KSxcbiAgc3RlcHBlci1hY2NlbnQtY29sb3I6IGNvbG9yLXN1Y2Nlc3MsXG5cbiAgY2FsZW5kYXItYWN0aXZlLWl0ZW0tYmc6IGNvbG9yLXByaW1hcnksXG4gIGNhbGVuZGFyLXNlbGVjdGVkLWl0ZW0tYmc6IGNvbG9yLXByaW1hcnksXG4gIGNhbGVuZGFyLXJhbmdlLWJnLWluLXJhbmdlOiAjNGU0MDk1LFxuICBjYWxlbmRhci10b2RheS1pdGVtLWJnOiAjMzUyZjZlLFxuKTtcblxuLy8gcmVnaXN0ZXIgdGhlIHRoZW1lXG4kbmItdGhlbWVzOiBuYi1yZWdpc3Rlci10aGVtZSgkdGhlbWUsIGNvc21pYywgZGVmYXVsdCk7XG4iLCIvKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG5cbkBpbXBvcnQgJy4uL2NvcmUvZnVuY3Rpb25zJztcbkBpbXBvcnQgJy4uL2NvcmUvbWl4aW5zJztcbkBpbXBvcnQgJ2RlZmF1bHQnO1xuXG4vLyBkZWZhdWx0IHRoZSBiYXNlIHRoZW1lXG4kdGhlbWU6IChcbiAgaGVhZGVyLWZnOiAjZjdmYWZiLFxuICBoZWFkZXItYmc6ICMxMTEyMTgsXG5cbiAgbGF5b3V0LWJnOiAjZjFmNWY4LFxuXG4gIGNvbG9yLWZnLWhlYWRpbmc6ICMxODE4MTgsXG4gIGNvbG9yLWZnLXRleHQ6ICM0YjRiNGIsXG4gIGNvbG9yLWZnLWhpZ2hsaWdodDogY29sb3ItZmcsXG5cbiAgc2VwYXJhdG9yOiAjY2RkNWRjLFxuXG4gIHJhZGl1czogMC4xN3JlbSxcblxuICBzY3JvbGxiYXItYmc6ICNlM2U5ZWUsXG5cbiAgY29sb3ItcHJpbWFyeTogIzczYTFmZixcbiAgY29sb3Itc3VjY2VzczogIzVkY2ZlMyxcbiAgY29sb3ItaW5mbzogI2JhN2ZlYyxcbiAgY29sb3Itd2FybmluZzogI2ZmYTM2YixcbiAgY29sb3ItZGFuZ2VyOiAjZmY2YjgzLFxuXG4gIGJ0bi1zZWNvbmRhcnktYmc6ICNlZGYyZjUsXG4gIGJ0bi1zZWNvbmRhcnktYm9yZGVyOiAjZWRmMmY1LFxuXG4gIGFjdGlvbnMtZmc6ICNkM2RiZTUsXG4gIGFjdGlvbnMtYmc6IGNvbG9yLWJnLFxuXG4gIHNpZGViYXItYmc6ICNlM2U5ZWUsXG5cbiAgYm9yZGVyLWNvbG9yOiAjZDVkYmUwLFxuXG4gIG1lbnUtZm9udC13ZWlnaHQ6IGZvbnQtd2VpZ2h0LWJvbGRlcixcbiAgbWVudS1mZzogY29sb3ItZmctdGV4dCxcbiAgbWVudS1iZzogI2UzZTllZSxcbiAgbWVudS1hY3RpdmUtZmc6IGNvbG9yLWZnLWhlYWRpbmcsXG4gIG1lbnUtYWN0aXZlLWJnOiBtZW51LWJnLFxuXG4gIG1lbnUtc3VibWVudS1iZzogbWVudS1iZyxcbiAgbWVudS1zdWJtZW51LWZnOiBjb2xvci1mZy10ZXh0LFxuICBtZW51LXN1Ym1lbnUtYWN0aXZlLWZnOiBjb2xvci1mZy1oZWFkaW5nLFxuICBtZW51LXN1Ym1lbnUtYWN0aXZlLWJnOiAjY2RkNWRjLFxuICBtZW51LXN1Ym1lbnUtYWN0aXZlLWJvcmRlci1jb2xvcjogbWVudS1zdWJtZW51LWFjdGl2ZS1iZyxcbiAgbWVudS1zdWJtZW51LWFjdGl2ZS1zaGFkb3c6IG5vbmUsXG4gIG1lbnUtc3VibWVudS1ob3Zlci1mZzogbWVudS1zdWJtZW51LWFjdGl2ZS1mZyxcbiAgbWVudS1zdWJtZW51LWhvdmVyLWJnOiBtZW51LWJnLFxuICBtZW51LXN1Ym1lbnUtaXRlbS1ib3JkZXItd2lkdGg6IDAuMTI1cmVtLFxuICBtZW51LXN1Ym1lbnUtaXRlbS1ib3JkZXItcmFkaXVzOiByYWRpdXMsXG4gIG1lbnUtc3VibWVudS1pdGVtLXBhZGRpbmc6IDAuNXJlbSAxcmVtLFxuICBtZW51LXN1Ym1lbnUtaXRlbS1jb250YWluZXItcGFkZGluZzogMCAxLjI1cmVtLFxuICBtZW51LXN1Ym1lbnUtcGFkZGluZzogMC41cmVtLFxuXG4gIGJ0bi1ib3JkZXItcmFkaXVzOiBidG4tc2VtaS1yb3VuZC1ib3JkZXItcmFkaXVzLFxuXG4gIGJ0bi1oZXJvLWRlZ3JlZTogMGRlZyxcbiAgYnRuLWhlcm8tcHJpbWFyeS1kZWdyZWU6IGJ0bi1oZXJvLWRlZ3JlZSxcbiAgYnRuLWhlcm8tc3VjY2Vzcy1kZWdyZWU6IGJ0bi1oZXJvLWRlZ3JlZSxcbiAgYnRuLWhlcm8td2FybmluZy1kZWdyZWU6IGJ0bi1oZXJvLWRlZ3JlZSxcbiAgYnRuLWhlcm8taW5mby1kZWdyZWU6IGJ0bi1oZXJvLWRlZ3JlZSxcbiAgYnRuLWhlcm8tZGFuZ2VyLWRlZ3JlZTogYnRuLWhlcm8tZGVncmVlLFxuICBidG4taGVyby1zZWNvbmRhcnktZGVncmVlOiBidG4taGVyby1kZWdyZWUsXG4gIGJ0bi1oZXJvLWdsb3ctc2l6ZTogMCAwIDIwcHggMCxcbiAgYnRuLWhlcm8tcHJpbWFyeS1nbG93LXNpemU6IGJ0bi1oZXJvLWdsb3ctc2l6ZSxcbiAgYnRuLWhlcm8tc3VjY2Vzcy1nbG93LXNpemU6IGJ0bi1oZXJvLWdsb3ctc2l6ZSxcbiAgYnRuLWhlcm8td2FybmluZy1nbG93LXNpemU6IGJ0bi1oZXJvLWdsb3ctc2l6ZSxcbiAgYnRuLWhlcm8taW5mby1nbG93LXNpemU6IGJ0bi1oZXJvLWdsb3ctc2l6ZSxcbiAgYnRuLWhlcm8tZGFuZ2VyLWdsb3ctc2l6ZTogYnRuLWhlcm8tZ2xvdy1zaXplLFxuICBidG4taGVyby1zZWNvbmRhcnktZ2xvdy1zaXplOiAwIDAgMCAwLFxuICBidG4taGVyby1ib3JkZXItcmFkaXVzOiBidG4tYm9yZGVyLXJhZGl1cyxcblxuICBjYXJkLXNoYWRvdzogbm9uZSxcbiAgY2FyZC1ib3JkZXItd2lkdGg6IDFweCxcbiAgY2FyZC1ib3JkZXItY29sb3I6IGJvcmRlci1jb2xvcixcbiAgY2FyZC1oZWFkZXItYm9yZGVyLXdpZHRoOiAwLFxuXG4gIGxpbmstY29sb3I6ICM1ZGNmZTMsXG4gIGxpbmstY29sb3ItaG92ZXI6ICM3ZGNmZTMsXG4gIGxpbmstY29sb3ItdmlzaXRlZDogbGluay1jb2xvcixcblxuICBhY3Rpb25zLXNlcGFyYXRvcjogI2YxZjRmNSxcblxuICBtb2RhbC1zZXBhcmF0b3I6IGJvcmRlci1jb2xvcixcblxuICB0YWJzLXNlbGVjdGVkOiBjb2xvci1wcmltYXJ5LFxuICB0YWJzLXNlcGFyYXRvcjogI2ViZWNlZSxcblxuICBzbWFydC10YWJsZS1wYWdpbmctYmctYWN0aXZlOiBjb2xvci1wcmltYXJ5LFxuXG4gIHJvdXRlLXRhYnMtc2VsZWN0ZWQ6IGNvbG9yLXByaW1hcnksXG5cbiAgcG9wb3Zlci1ib3JkZXI6IGNvbG9yLXByaW1hcnksXG5cbiAgZm9vdGVyLXNoYWRvdzogbm9uZSxcbiAgZm9vdGVyLXNlcGFyYXRvcjogYm9yZGVyLWNvbG9yLFxuICBmb290ZXItZmctaGlnaGxpZ2h0OiAjMmEyYTJhLFxuXG4gIGNhbGVuZGFyLXRvZGF5LWl0ZW0tYmc6ICNhMmIyYzcsXG4gIGNhbGVuZGFyLWFjdGl2ZS1pdGVtLWJnOiBjb2xvci1wcmltYXJ5LFxuICBjYWxlbmRhci1yYW5nZS1iZy1pbi1yYW5nZTogI2UzZWNmZSxcbiAgY2FsZW5kYXItdG9kYXktZmc6IGNvbG9yLXdoaXRlLFxuKTtcblxuLy8gcmVnaXN0ZXIgdGhlIHRoZW1lXG4kbmItdGhlbWVzOiBuYi1yZWdpc3Rlci10aGVtZSgkdGhlbWUsIGNvcnBvcmF0ZSwgZGVmYXVsdCk7XG4iLCIvKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG5cbiRncmlkLWNvbHVtbnM6IDEyICFkZWZhdWx0O1xuJGdyaWQtZ3V0dGVyLXdpZHRoLWJhc2U6IDI0cHggIWRlZmF1bHQ7XG4kZ3JpZC1ndXR0ZXItd2lkdGhzOiAoXG4gIHhzOiAkZ3JpZC1ndXR0ZXItd2lkdGgtYmFzZSxcbiAgc206ICRncmlkLWd1dHRlci13aWR0aC1iYXNlLFxuICBtZDogJGdyaWQtZ3V0dGVyLXdpZHRoLWJhc2UsXG4gIGxnOiAkZ3JpZC1ndXR0ZXItd2lkdGgtYmFzZSxcbiAgeGw6ICRncmlkLWd1dHRlci13aWR0aC1iYXNlXG4pICFkZWZhdWx0O1xuXG5cbiRncmlkLWJyZWFrcG9pbnRzOiAoXG4gIHhzOiAwLFxuICBpczogNDAwcHgsXG4gIHNtOiA1NzZweCxcbiAgbWQ6IDc2OHB4LFxuICBsZzogOTkycHgsXG4gIHhsOiAxMjAwcHgsXG4gIHh4bDogMTQwMHB4LFxuICB4eHhsOiAxNjAwcHhcbik7XG5cbiRjb250YWluZXItbWF4LXdpZHRoczogKFxuICBpczogMzgwcHgsXG4gIHNtOiA1NDBweCxcbiAgbWQ6IDcyMHB4LFxuICBsZzogOTYwcHgsXG4gIHhsOiAxMTQwcHgsXG4gIHh4bDogMTMyMHB4LFxuICB4eHhsOiAxNTAwcHhcbik7XG4iLCJcbkBpbXBvcnQgJy4uLy4uLy4uLy4uLy4uL0B0aGVtZS9zdHlsZXMvdGhlbWVzJztcbkBpbXBvcnQgJ35ib290c3RyYXAvc2Nzcy9taXhpbnMvYnJlYWtwb2ludHMnO1xuQGltcG9ydCAnfkBuZWJ1bGFyL3RoZW1lL3N0eWxlcy9nbG9iYWwvYm9vdHN0cmFwL2JyZWFrcG9pbnRzJztcblxuQGluY2x1ZGUgbmItaW5zdGFsbC1jb21wb25lbnQoKSB7XG5idXR0b257XG4gIGZsb2F0OiByaWdodDtcbn1cbi9kZWVwLyBuZzItc21hcnQtdGFibGUgaXtcbiAgICBjb2xvcjojMjEyNTI5ICFpbXBvcnRhbnRcbiAgICB9XG4gIFxuICAgIC9kZWVwLyBuZzItc21hcnQtdGFibGUgbmF2IHVsIGxpIGF7XG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiMyMTI1MjkgIWltcG9ydGFudFxuICAgICAgfVxuXG4gICAgICAvZGVlcC8gbmcyLXNtYXJ0LXRhYmxlIC5uZzItc21hcnQtdGl0bGV7XG4gICAgICAgIGNvbG9yOndoaXRlICFpbXBvcnRhbnRcbiAgICAgICAgfVxuICAgICAgICBuYi1jYXJke1xuICAgICAgICAgIGJvcmRlcjogMHB4IHNvbGlkICNkNWRiZTA7XG4gICAgICAgIH1cblxuICAgICAgICAuZm9ybS1jaGVjay1pbnB1dCB7XG4gICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICAgIG1hcmdpbi10b3A6IC0wLjA1cmVtO1xuICAgICAgICAgIG1hcmdpbi1sZWZ0OiAtMS41cmVtO1xuICAgICAgICAgIHdpZHRoOiAyMHB4O1xuICAgICAgICAgIGhlaWdodDogMjBweDtcbiAgICAgIH1cbiAgICAgIC9kZWVwLyBuZzItc21hcnQtdGFibGUgaSB7XG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZSAhaW1wb3J0YW50O1xuICAgICAgICB0b3A6IDBweCAhaW1wb3J0YW50O1xuICAgIH1cbiAgICAvZGVlcC8gbmcyLXN0LWFjdGlvbnMgaXtcbiAgICAgIGNvbG9yOndoaXRlICFpbXBvcnRhbnQ7XG4gIH1cblxuICAvZGVlcC8gbmcyLXNtYXJ0LXRhYmxlIHNlbGVjdHtcbiAgICBoZWlnaHQ6Mi42cmVtICFpbXBvcnRhbnQ7XG4gICAgcGFkZGluZy10b3A6IDVweCAhaW1wb3J0YW50O1xuICB9XG5cbiAgQG1lZGlhIChtaW4td2lkdGg6IDU3NnB4KXtcbiAgICAvZGVlcC8gbW9kYWwubW9kYWxzcyAubW9kYWwtZGlhbG9ne1xuICAgICAgICBtYXgtd2lkdGg6IDcwJSAhaW1wb3J0YW50O1xuICAgICAgIFxuICAgIH1cbiAgICAvZGVlcC8gbW9kYWwubW9kYWxzcyAubW9kYWwtZGlhbG9nIC5tb2RhbC1ib2R5e1xuICAgICAgaGVpZ2h0OiBhdXRvIDtcbiAgICB9XG4gICAgL2RlZXAvIG1vZGFsLm1vZGFsc3MgLm1vZGFsLWRpYWxvZyAubW9kYWwtZm9vdGVye1xuICAgICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XG4gIH1cbiAgXG4gICAgfVxuXG4gICAgLy8gL2RlZXAvbmcyLXNtYXJ0LXRhYmxlLWNlbGwgaW5wdXQtZWRpdG9ye1xuICAgIC8vICAgYmFja2dyb3VuZC1jb2xvcjogcmVkICFpbXBvcnRhbnQ7XG4gICAgLy8gfVxuXG4gICAgL2RlZXAvbmcyLXNtYXJ0LXRhYmxlIHRhYmxlIHRib2R5IHRyOm50aC1jaGlsZChvZGQpIHtcbiAgICAgIGJhY2tncm91bmQ6IHdoaXRlICFpbXBvcnRhbnQ7XG4gIH1cbiAgLy8gL2RlZXAvbmcyLXNtYXJ0LXRhYmxlIC5uZzItc21hcnQtcm93OmZpcnN0LWNoaWxkIC5uZzItc21hcnQtYWN0aW9uc3tcbiAgLy8gICBiYWNrZ3JvdW5kLWNvbG9yOiAjMWQ4ZWNlO1xuICAvLyB9XG4gIC8vIC9kZWVwL25nMi1zbWFydC10YWJsZSAubmcyLXNtYXJ0LXJvdyB0ZHtcbiAgLy8gICBiYWNrZ3JvdW5kLWNvbG9yOiAjMWQ4ZWNlO1xuICAvLyB9XG5cbiAgL2RlZXAvbmcyLXNtYXJ0LXRhYmxlIC5uZzItc21hcnQtcm93IHRkIG5nMi1zbWFydC10YWJsZS1jZWxsIGlucHV0LWVkaXRvciBpbnB1dHtcbiAgICBib3JkZXI6ICMxZDhlY2Ugc29saWQgIWltcG9ydGFudDtcbiAgICAvLyBiYWNrZ3JvdW5kLWNvbG9yOiByZWQgIWltcG9ydGFudDtcbiAgfVxuICAvZGVlcC9uZzItc21hcnQtdGFibGUgLm5nMi1zbWFydC1yb3cgdGQgbmcyLXNtYXJ0LXRhYmxlLWNlbGwgc2VsZWN0LWVkaXRvciBzZWxlY3R7XG4gICAgYm9yZGVyOiAjMWQ4ZWNlIHNvbGlkICFpbXBvcnRhbnQ7XG4gIH1cbiAgL2RlZXAvIG5nMi1zdC10Ym9keS1lZGl0LWRlbGV0ZSB7XG4gICAgZGlzcGxheTogLXdlYmtpdC1pbmxpbmUtYm94ICFpbXBvcnRhbnQ7XG4gICAgaGVpZ2h0OiAwcHggIWltcG9ydGFudDtcbiAgICBmbG9hdDogbGVmdDtcbiAgICBtYXJnaW4tbGVmdDogMzZweDtcbiAgICBtYXJnaW4tdG9wOiAtMTVweCAhaW1wb3J0YW50O1xuICAgIC8qIG1hcmdpbjogYXV0bzsgKi9cbiAgIH1cbiAgIFxuICAgIC9kZWVwLyBuZzItc3QtdGJvZHktY3VzdG9tIGEubmcyLXNtYXJ0LWFjdGlvbi5uZzItc21hcnQtYWN0aW9uLWN1c3RvbS1jdXN0b20ge1xuICAgIC8vICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgIHdpZHRoOiA1MHB4O1xuICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgIGZvbnQtc2l6ZTogMC4xZW07XG4gICB9XG4gICAvZGVlcC8gbmcyLXN0LXRib2R5LWN1c3RvbSBhLm5nMi1zbWFydC1hY3Rpb24ubmcyLXNtYXJ0LWFjdGlvbi1jdXN0b20tY3VzdG9tIGltZ3tcbiAgICB3aWR0aDogNTclICFpbXBvcnRhbnQ7XG4gICAgbWFyZ2luLWxlZnQ6IDM0cHg7XG4gICB9XG4gIC9kZWVwLyBuZzItc3QtdGJvZHktY3VzdG9tIGEubmcyLXNtYXJ0LWFjdGlvbi5uZzItc21hcnQtYWN0aW9uLWN1c3RvbS1jdXN0b206aG92ZXIge1xuICAgICBjb2xvcjogIzVkY2ZlMztcbiAgIH1cbiBcbiAgICAgL2RlZXAvIG5nMi1zdC10Ym9keS1lZGl0LWRlbGV0ZSBpIHtcbiAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZSAhaW1wb3J0YW50O1xuICAgICAgdG9wOiAtMTRweCA7XG4gICAgICBtYXJnaW4tbGVmdDogNDBweDtcbiAgIH1cbiAgL2RlZXAvIG5nMi1zbWFydC10YWJsZSAubmcyLXNtYXJ0LWFjdGlvbnMgYS5uZzItc21hcnQtYWN0aW9uIHtcbiAgICBcbiAgICAvLyB3aWR0aDogMzMlO1xufVxuXG4vLyAvZGVlcC8gbmcyLXNtYXJ0LXRhYmxlIC5uZzItc21hcnQtYWN0aW9ucyBhLm5nMi1zbWFydC1hY3Rpb24ge1xuLy8gICAvKiB3aWR0aDogMzMlOyAqL1xuLy8gICAvKiBtYXJnaW46IDBweDsgKi9cbi8vICAgLyogbWFyZ2luLWxlZnQ6IDBweDsgKi9cbi8vIH1cbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/ui-features/amc/main-amc/upload-amc-doc/upload-amc-doc.component.ts":
/*!*******************************************************************************************!*\
  !*** ./src/app/pages/ui-features/amc/main-amc/upload-amc-doc/upload-amc-doc.component.ts ***!
  \*******************************************************************************************/
/*! exports provided: UploadAmcDocComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UploadAmcDocComponent", function() { return UploadAmcDocComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var angular_custom_modal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular-custom-modal */ "./node_modules/angular-custom-modal/angular-custom-modal.es5.js");
/* harmony import */ var _confirmation_dialog_confirmation_dialog_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../confirmation-dialog/confirmation-dialog.service */ "./src/app/confirmation-dialog/confirmation-dialog.service.ts");
/* harmony import */ var _services_AMC_amc_upload_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../../services/AMC/amc-upload.service */ "./src/app/services/AMC/amc-upload.service.ts");
/* harmony import */ var _services_AMC_amc_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../../services/AMC/amc.service */ "./src/app/services/AMC/amc.service.ts");
/* harmony import */ var _services_settings_settings_services_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../../services/settings/settings-services.service */ "./src/app/services/settings/settings-services.service.ts");
/* harmony import */ var _services_token_token_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../../services/token/token.service */ "./src/app/services/token/token.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};









var UploadAmcDocComponent = /** @class */ (function () {
    function UploadAmcDocComponent(AmcServ, userServ, settingservice, jobsServiceServ, confirmationDialogService) {
        this.AmcServ = AmcServ;
        this.userServ = userServ;
        this.settingservice = settingservice;
        this.jobsServiceServ = jobsServiceServ;
        this.confirmationDialogService = confirmationDialogService;
        this.data1 = {};
        this.jobChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.source = new ng2_smart_table__WEBPACK_IMPORTED_MODULE_2__["LocalDataSource"]();
        this.moduleCreate = true;
        this.moduleEdit = true;
        this.moduleView = true;
        // const data = this.service.getData();
        // this.source.load(data);
        this.getJobDetail();
        this.port = this.settingservice.port;
    }
    UploadAmcDocComponent.prototype.onDeleteConfirm = function (event) {
        return __awaiter(this, void 0, void 0, function () {
            var resultAleart, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.confirmationDialogService.confirm('Alert Message', 'Are you sure you want to Delete it?')];
                    case 1:
                        resultAleart = _a.sent();
                        if (!resultAleart) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.AmcServ.deleteAttachmentsAmcDoc(event.data, event.data.uid)];
                    case 2:
                        result = _a.sent();
                        event.confirm.resolve();
                        return [3 /*break*/, 4];
                    case 3:
                        event.confirm.reject();
                        _a.label = 4;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    UploadAmcDocComponent.prototype.onUserRowSelect = function (event) {
        if (this.moduleCreate || this.moduleEdit) {
            // console.log(event.data);
            var data = event.data;
            this.attachement = data.attachement;
            // console.log(this.attachement);
            // window.open(`//${location.hostname}:4000/amc-attachementes/${this.attachement.filename}`, '_blank');
            // window.open(`//${location.hostname}:80/amc-attachementes/${this.attachement.filename}`, '_blank');
            window.open("//" + location.hostname + ":" + this.port + "/amc-attachementes/" + this.attachement.filename, '_blank');
            // console.log(this.images);
        }
    };
    /**
       * Get Jobs Detail
       */
    UploadAmcDocComponent.prototype.getJobDetail = function () {
        var _this = this;
        this.jobsServiceServ.AmcDone
            .subscribe(function (data) {
            _this.data1 = data;
            _this.id = _this.data1.id;
            // console.log(this.data1,"Helo")
            _this.getAttachmentInfo();
        }, function (error) {
            console.log(error);
        });
    };
    UploadAmcDocComponent.prototype.createNewAttachment = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, descriptions, filenames, formData, resultAleart, result, error_1;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        Object.assign(data.value, { amcId: this.id });
                        _a = data.value, descriptions = _a.descriptions, filenames = _a.filenames;
                        formData = new FormData();
                        formData.append('filenames', filenames);
                        formData.append('descriptions', descriptions);
                        formData.append('amc-attach', this.getFile);
                        formData.append('id', this.id);
                        return [4 /*yield*/, this.confirmationDialogService.confirm('Alert Message', 'Are you sure you want to create?')];
                    case 1:
                        resultAleart = _b.sent();
                        if (!resultAleart) return [3 /*break*/, 6];
                        _b.label = 2;
                    case 2:
                        _b.trys.push([2, 4, , 5]);
                        return [4 /*yield*/, this.AmcServ.createAttachmentsAmcDoc(formData)];
                    case 3:
                        result = _b.sent();
                        // console.log(result)
                        // this.jobChange.emit(result);
                        this.listOfAttachments.push(result);
                        this.source.load(this.listOfAttachments);
                        this.componentInsideModals.close();
                        return [3 /*break*/, 5];
                    case 4:
                        error_1 = _b.sent();
                        console.log(error_1);
                        return [3 /*break*/, 5];
                    case 5: return [3 /*break*/, 7];
                    case 6:
                        data.confirm.reject();
                        _b.label = 7;
                    case 7: return [2 /*return*/];
                }
            });
        });
    };
    UploadAmcDocComponent.prototype.coverImageChangeEvent = function (files) {
        var file = files[0];
        this.getFile = file;
    };
    /**
       Get Quotation Attachments Details
    **/
    UploadAmcDocComponent.prototype.getAttachmentInfo = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, error_2;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        _a = this;
                        return [4 /*yield*/, this.AmcServ.getAttachmentsAmcDocsById(this.id)];
                    case 1:
                        _a.listOfAttachments = _b.sent();
                        this.source.load(this.listOfAttachments);
                        return [3 /*break*/, 3];
                    case 2:
                        error_2 = _b.sent();
                        console.error(error_2);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    UploadAmcDocComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.userServ.userAccess !== undefined) {
            this.userServ.userAccess
                .subscribe(function (data) {
                data.forEach(function (ele) {
                    if (ele.name === 'AMC  Details') {
                        _this.moduleCreate = ele.create;
                        _this.moduleEdit = ele.edit;
                        _this.moduleView = ele.view;
                    }
                });
            }, function (error) {
                console.log(error);
            });
        }
        if (this.moduleCreate || this.moduleEdit) {
            this.settings = {
                add: {
                    addButtonContent: '<i class="nb-plus"></i>',
                    createButtonContent: '<i class="nb-checkmark"></i>',
                    cancelButtonContent: '<i class="nb-close"></i>',
                },
                edit: {
                    editButtonContent: '<i class="nb-edit"></i>',
                    saveButtonContent: '<i class="nb-checkmark"></i>',
                    cancelButtonContent: '<i class="nb-close"></i>',
                },
                delete: {
                    deleteButtonContent: '<i class="nb-trash"></i>',
                    confirmDelete: true,
                },
                actions: {
                    edit: false,
                    add: false,
                    // delete: false,
                    custom: [
                        {
                            name: 'view',
                            title: '<img src="./assets/icons/eye-outline.png" >',
                        },
                    ],
                    position: 'right'
                },
                columns: {
                    descriptions: {
                        title: 'Description',
                        type: 'string',
                    },
                    filenames: {
                        title: 'File Name',
                        type: 'string',
                    },
                },
            };
        }
        else {
            this.settings = {
                add: {
                    addButtonContent: '<i class="nb-plus"></i>',
                    createButtonContent: '<i class="nb-checkmark"></i>',
                    cancelButtonContent: '<i class="nb-close"></i>',
                },
                edit: {
                    editButtonContent: '<i class="nb-edit"></i>',
                    saveButtonContent: '<i class="nb-checkmark"></i>',
                    cancelButtonContent: '<i class="nb-close"></i>',
                },
                delete: {
                    deleteButtonContent: '<i class="nb-trash"></i>',
                    confirmDelete: true,
                },
                actions: {
                    edit: false,
                    add: false,
                    delete: false,
                    custom: [
                        {
                            name: 'view',
                            title: '<img src="./assets/icons/eye-outline.png" >',
                        },
                    ],
                    position: 'right'
                },
                columns: {
                    descriptions: {
                        title: 'Description',
                        type: 'string',
                    },
                    filenames: {
                        title: 'File Name',
                        type: 'string',
                    },
                },
            };
        }
        this.getAttachmentInfo();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('componentInsideModals'),
        __metadata("design:type", angular_custom_modal__WEBPACK_IMPORTED_MODULE_3__["ModalComponent"])
    ], UploadAmcDocComponent.prototype, "componentInsideModals", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], UploadAmcDocComponent.prototype, "jobChange", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('form'),
        __metadata("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgForm"])
    ], UploadAmcDocComponent.prototype, "form", void 0);
    UploadAmcDocComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-upload-amc-doc',
            template: __webpack_require__(/*! ./upload-amc-doc.component.html */ "./src/app/pages/ui-features/amc/main-amc/upload-amc-doc/upload-amc-doc.component.html"),
            styles: [__webpack_require__(/*! ./upload-amc-doc.component.scss */ "./src/app/pages/ui-features/amc/main-amc/upload-amc-doc/upload-amc-doc.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_AMC_amc_upload_service__WEBPACK_IMPORTED_MODULE_5__["AmcDocService"],
            _services_token_token_service__WEBPACK_IMPORTED_MODULE_8__["TokenService"],
            _services_settings_settings_services_service__WEBPACK_IMPORTED_MODULE_7__["SettingsServicesService"],
            _services_AMC_amc_service__WEBPACK_IMPORTED_MODULE_6__["AmcService"],
            _confirmation_dialog_confirmation_dialog_service__WEBPACK_IMPORTED_MODULE_4__["ConfirmationDialogService"]])
    ], UploadAmcDocComponent);
    return UploadAmcDocComponent;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/amc/total-amc/total-amc.component.html":
/*!**************************************************************************!*\
  !*** ./src/app/pages/ui-features/amc/total-amc/total-amc.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\n    <div class=\"row\">\n        <div class=\"container-fluid\">\n            <div class=\"row\">\n                <div class=\"col-xl-5\">\n                    <nb-card>\n                        <nb-card-header>\n                            AMC List\n                            <!-- <button class=\"btn btn-outline-primary \" shape=\"semi-round\"  [routerLink]=\"['/pages/MainQuotation']\" style=\"margin-left: 8px;\">Clear</button> -->\n                            <button class=\"btn btn-outline-primary \" shape=\"semi-round\" (click)=\"showFun('create')\">Create New AMC</button>\n\n                        </nb-card-header>\n                        <nb-card-body>\n\n                            <ng2-smart-table [settings]=\"settings\" [source]=\"source\" (userRowSelect)=\"onUserRowSelect($event)\">\n                            </ng2-smart-table>\n\n                        </nb-card-body>\n                    </nb-card>\n\n                </div>\n                <div class=\"col-xl-7\" *ngIf=\"creatShow\">\n                    <nb-card>\n                        <nb-card-header>\n                            Create New AMC\n                        </nb-card-header>\n                        <nb-card-body>\n                            <form [formGroup]=\"myForm\" (ngSubmit)=\"create(myForm)\">\n                                <div class=\"container-fluid\">\n                                    <div class=\"row\">\n                                        <div class=\"col-md-6\">\n                                            <div class=\"form-group input-group-sm\">\n                                                <label class=\"clientName\">Company Name</label>\n                                                <input list=\"browsers\" type=\"text\" class=\"form-control\" id=\"Maintenance\" (change)=\"onSelect($event.target.value)\" formControlName=\"customerName\" autocomplete=\"OFF\" placeholder=\"Company Name\">\n                                                <datalist id=\"browsers\">\n                                                    <option *ngFor=\"let state of getCompanyNameList;let i = index\">{{state.companyName}}</option>\n                                                </datalist>\n                                            </div>\n                                        </div>\n                                        <div class=\"col-md-6\">\n                                            <div class=\"form-group input-group-sm\">\n                                                <label for=\"Maintenance\">Site Name</label>\n                                                <input list=\"site\" type=\"text\" class=\"form-control\" (change)=\"onSelectSite($event.target.value)\" [(ngModel)]='siteName' formControlName=\"siteName\" autocomplete=\"OFF\" placeholder=\"Site Name\">\n                                                <datalist id=\"site\">\n                                                    <option *ngFor=\"let sites of getSiteNameList;let i = index\">{{sites.siteName}}</option>\n                                                </datalist>\n                                            </div>\n                                        </div>\n                                    </div>\n                                    <div class=\"row\">\n                                        <div class=\"col-md-6\">\n                                            <div class=\"form-group input-group-sm\">\n                                                <label class=\"clientAddress\">Company Address</label>\n                                                <textarea type=\"text\" class=\"form-control\" formControlName=\"clientAddress\" ngModel name=\"clientAddress\"></textarea>\n                                            </div>\n                                        </div>\n                                        <div class=\"col-md-6\">\n                                            <div class=\"form-group input-group-sm\">\n                                                <label class=\"sitetAddress\">Site Address</label>\n                                                <textarea type=\"text\" class=\"form-control\" ngModel formControlName=\"siteAddress\" name=\"siteAddress\"></textarea>\n                                            </div>\n                                        </div>\n                                    </div>\n                                    <div class=\"row\" style=\"background-color: #c5c5c5\">\n                                        <div class=\"col-sm-4\">\n                                            <div class=\"form-group input-group-sm\">\n                                                <label for=\"jobids\">AMC Agreement Prefix</label>\n                                                <input type=\"text\" class=\"form-control\" name=\"amcPrefix\" formControlName=\"amcPrefix\" value=\"{{amcPrefix}}\" (blur)=\"updatePre($event.target.value)\" placeholder=\"AMC Agreement Prefix\">\n\n                                            </div>\n                                        </div>\n                                        <div class=\"col-sm-4\">\n                                            <div class=\"form-group input-group-sm\">\n                                                <label for=\"jobid\">AMC Agreement No</label>\n                                                <input type=\"number\" class=\"form-control\" name=\"amcNo\" formControlName=\"amcNo\" value=\"{{amcNo}}\" (blur)=\"updateNo($event.target.value)\" placeholder=\"AMC Agreement No\">\n\n                                            </div>\n                                        </div>\n                                        <div class=\"col-sm-4\">\n                                            <div class=\"form-group input-group-sm\">\n                                                <label for=\"jobid\">AMC Number</label>\n                                                <input type=\"text\" class=\"form-control\" disabled name=\"amcid\" formControlName=\"amcid\" value=\"{{amcid}}\" placeholder=\"AMC Number\">\n\n                                            </div>\n                                        </div>\n                                    </div>\n                                    <div class=\"row\" style=\"margin-top: 2%;\">\n                                        <div class=\"col-md-4\">\n                                            <div class=\"form-group input-group-sm\">\n                                                <label class=\"amount\">Contact Person Name</label>\n                                                <input type=\"text\" class=\"form-control\" placeholder=\"Contact Person Name\" formControlName='PersonName'>\n                                            </div>\n                                        </div>\n                                        <div class=\"col-md-4\">\n                                            <div class=\"form-group input-group-sm\">\n                                                <label class=\"amount\">Contact Staff Name</label>\n                                                <input type=\"text\" class=\"form-control\" placeholder=\"Contact Staff Name\" formControlName='employee' name='employee'>\n                                            </div>\n                                        </div>\n                                        <div class=\"col-md-4\">\n                                            <div class=\"form-group input-group-sm\">\n                                                <label class=\"amount\">AMC Amount</label>\n                                                <input type=\"text\" class=\"form-control\" placeholder=\"AMC Amount\" formControlName='amcAmount' (blur)=changeAmt($event.target.value) required value=\"{{amcAmount}}\" [(ngModel)]=\"amcAmount\" name=\"amcAmount\" min=\"0\" value=\"0\" step=\"0.01\" title=\"Currency\" pattern=\"^\\d+(?:\\.\\d{1,2})?$\">\n                                            </div>\n                                        </div>\n                                        <div class=\"col-md-8\">\n                                            <div class=\"container\">\n                                                <div class=\"form-group input-group-sm\">\n                                                    <label class=\"SiteName\">AMC Period</label>\n                                                    <div class=\"row\">\n                                                        <div class=\"col-6\">\n                                                            <div class=\"input-group-sm\">\n                                                                <label>From</label>\n                                                                <input type=\"date\" class=\"form-control\" value='{{fromData}}' formControlName='fromData' name=\"fromData\" (change)='from($event.target.value)'>\n                                                            </div>\n                                                        </div>\n                                                        <div class=\"col-6\">\n                                                            <div class=\"input-group-sm\">\n                                                                <label>To</label>\n                                                                <input type=\"date\" class=\"form-control\" value='{{toData}}' fromControlName='toData' name=\"toData\" (change)='to($event.target.value)'>\n                                                            </div>\n                                                        </div>\n                                                        <div class=\"col-12\" style=\"text-align: center;margin-top: 1%;\">\n                                                            <label>Note<span style=\"color: red;\">*</span> : <strong>From and To date are current date. Please check before creating AMC.</strong></label>\n                                                        </div>\n                                                    </div>\n                                                </div>\n                                            </div>\n                                        </div>\n                                    </div>\n                                    <!-- <div class=\"row\">\n                                        <div class=\"col-md-4\">\n                                            <div class=\"form-group input-group-sm\">\n                                                <label class=\"clientName\">Contact Person Name</label>\n                                                <input type=\"text\" class=\"form-control\" name=\"PersonName\" [(ngModel)]=\"data1.PersonName\">\n                                            </div>\n                                        </div>\n                                        <div class=\"col-md-4\">\n                                            <div class=\"form-group input-group-sm\">\n                                                <label class=\"SiteName\">Select Service Type</label>\n                                                <select class=\"form-control\" id=\"sel1\" name=\"servies\" (change)='selectService($event.target.value)' size=\"small\" [(ngModel)]=\"data1.servies\" style=\"padding-top: 3px;\">\n                                                  <option value='' disabled>Select AnyOne</option>\n                                                  <option *ngFor=\"let servie of servies;let i = index\" [value]=\"servie.type\">{{servie.type}}</option>\n                                                </select>\n                                            </div>\n                                        </div>\n                                        <div class=\"col-md-4\" *ngIf='moduleEdit'>\n                                            <div class=\"form-group input-group-sm\">\n                                                <br>\n                                                <button type=\"button\" (click)=\"openModel()\" class=\"btn btn-outline-primary\" shape=\"semi-round\" style=\"float: left;\">Edit Services</button>\n                                            </div>\n                                        </div>\n                                    </div> -->\n\n                                </div>\n                                <div class=\"row\" style=\"margin-top: 2%;float: right;margin-right: 0.6%;\">\n                                    <button type=\"submit\" class=\"btn btn-outline-primary buttons\" shape=\"semi-round\">Save</button>\n                                </div>\n                            </form>\n                        </nb-card-body>\n                    </nb-card>\n                    <!-- <ngx-create-update-amc (editedFormFieldUpdate)=\"updatedTableData($event)\"></ngx-create-update-amc> -->\n                </div>\n                <div class=\"col-xl-7\" *ngIf=\"tableShow\">\n                    <ngx-main-amc></ngx-main-amc>\n                </div>\n            </div>\n        </div>\n\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/pages/ui-features/amc/total-amc/total-amc.component.scss":
/*!**************************************************************************!*\
  !*** ./src/app/pages/ui-features/amc/total-amc/total-amc.component.scss ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "button {\n  float: right; }\n\n@media (max-width: 600px) {\n  button {\n    float: none;\n    margin-top: 1px; } }\n\n:host /deep/ ng2-smart-table table {\n  word-break: break-word; }\n\n:host /deep/ td.ng2-smart-actions a {\n  cursor: pointer;\n  color: #009ECE; }\n\n:host /deep/ ng2-smart-table thead > tr > th > select {\n  font-size: 0.875rem;\n  padding: 0.375rem 1.125rem; }\n\n:host /deep/ tr th .nb-theme-corporate select.form-control:not([size]):not([multiple]) {\n  height: calc(2rem + 4px) !important; }\n\n:host /deep/ tr, th .nb-theme-corporate .form-control {\n  font-size: 1rem !important;\n  font-size: 0.875rem !important;\n  padding: 0.375rem 1.125rem !important; }\n\nnb-card-body {\n  background-color: #E1F5FE; }\n\nnb-card-header {\n  background: #1D8ECE;\n  color: white; }\n\n:host /deep/ tr.solved td {\n  background-color: #ec7431 !important; }\n\n:host /deep/ tr.aborted td {\n  background-color: #BD2031 !important;\n  color: white !important; }\n\n:host /deep/ ng2-smart-table tbody tr:hover {\n  background: none !important; }\n\n:host /deep/ tr.Mycolor td {\n  background-color: #2dbd20 !important;\n  color: white !important; }\n\n:host /deep/ ng2-smart-table nav.ng2-smart-pagination-nav .pagination {\n  background-color: black; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3ByYWthc2gvVmlwaW4vaXJvbmJpcmQvMjBGRUIyMDIwL2lyb25iaXJkLWNsaWVudHMvc3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9hbWMvdG90YWwtYW1jL3RvdGFsLWFtYy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFlBQVksRUFBQTs7QUFFaEI7RUFDSTtJQUNJLFdBQVc7SUFDWCxlQUFlLEVBQUEsRUFDbEI7O0FBS0w7RUFDSSxzQkFBc0IsRUFBQTs7QUFHMUI7RUFBcUMsZUFBZTtFQUFDLGNBQWMsRUFBQTs7QUFFbkU7RUFDSSxtQkFBbUI7RUFDbkIsMEJBQTBCLEVBQUE7O0FBRzlCO0VBQ0ksbUNBQW1DLEVBQUE7O0FBUXZDO0VBQ0ksMEJBQTBCO0VBQzFCLDhCQUE4QjtFQUM5QixxQ0FBcUMsRUFBQTs7QUFFekM7RUFDSSx5QkFBd0IsRUFBQTs7QUFHMUI7RUFDRSxtQkFBbUI7RUFDbkIsWUFBWSxFQUFBOztBQVNaO0VBQ0ksb0NBQW1DLEVBQUE7O0FBTW5DO0VBQ0ksb0NBQW1DO0VBQ25DLHVCQUFzQixFQUFBOztBQUUxQjtFQUNJLDJCQUEwQixFQUFBOztBQUc5QjtFQUNJLG9DQUE0QztFQUM1Qyx1QkFBc0IsRUFBQTs7QUFHdEI7RUFDSSx1QkFBdUIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VpLWZlYXR1cmVzL2FtYy90b3RhbC1hbWMvdG90YWwtYW1jLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiYnV0dG9ue1xuICAgIGZsb2F0OiByaWdodDtcbn1cbkBtZWRpYSAobWF4LXdpZHRoOjYwMHB4KXtcbiAgICBidXR0b257XG4gICAgICAgIGZsb2F0OiBub25lO1xuICAgICAgICBtYXJnaW4tdG9wOiAxcHg7XG4gICAgfVxufVxuLy8gOmhvc3QgL2RlZXAvIG5nMi1zbWFydC10YWJsZS10aXRsZSBhOmZvY3VzIHsgY29sb3I6IHJnYigxNTgsIDIwNiwgMCkgIWltcG9ydGFudDsgfVxuXG4vLyA6aG9zdCAvZGVlcC8gbmcyLXNtYXJ0LXRhYmxlIHRoZWFkID4gdHIgPiB0aCA+IGRpdiB7IGNvbG9yOiByZ2IoNjIsIDAsIDIwNik7IH1cbjpob3N0IC9kZWVwL25nMi1zbWFydC10YWJsZSB0YWJsZXtcbiAgICB3b3JkLWJyZWFrOiBicmVhay13b3JkO1xufVxuXG46aG9zdCAvZGVlcC8gdGQubmcyLXNtYXJ0LWFjdGlvbnMgYSB7Y3Vyc29yOiBwb2ludGVyO2NvbG9yOiAjMDA5RUNFOyB9XG5cbjpob3N0IC9kZWVwLyBuZzItc21hcnQtdGFibGUgdGhlYWQgPnRyID4gdGggPiBzZWxlY3QgeyAgICBcbiAgICBmb250LXNpemU6IDAuODc1cmVtO1xuICAgIHBhZGRpbmc6IDAuMzc1cmVtIDEuMTI1cmVtOyBcbn1cblxuOmhvc3QgL2RlZXAvIHRyIHRoIC5uYi10aGVtZS1jb3Jwb3JhdGUgc2VsZWN0LmZvcm0tY29udHJvbDpub3QoW3NpemVdKTpub3QoW211bHRpcGxlXSkge1xuICAgIGhlaWdodDogY2FsYygycmVtICsgNHB4KSAhaW1wb3J0YW50O1xufVxuXG5cbi8vIHNlbGVjdC5mb3JtLWNvbnRyb2w6bm90KFtzaXplXSk6bm90KFttdWx0aXBsZV0pIHtcbi8vICAgICBoZWlnaHQ6IGNhbGMoMi4yNXJlbSArIDJweCk7XG4vLyB9XG5cbjpob3N0IC9kZWVwLyB0cix0aCAubmItdGhlbWUtY29ycG9yYXRlIC5mb3JtLWNvbnRyb2wge1xuICAgIGZvbnQtc2l6ZTogMXJlbSAhaW1wb3J0YW50O1xuICAgIGZvbnQtc2l6ZTogMC44NzVyZW0gIWltcG9ydGFudDtcbiAgICBwYWRkaW5nOiAwLjM3NXJlbSAxLjEyNXJlbSAhaW1wb3J0YW50O1xufVxubmItY2FyZC1ib2R5e1xuICAgIGJhY2tncm91bmQtY29sb3I6I0UxRjVGRTtcbiAgICAvLyBjb2xvcjp3aGl0ZTtcbiAgfVxuICBuYi1jYXJkLWhlYWRlcntcbiAgICBiYWNrZ3JvdW5kOiAjMUQ4RUNFO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgfVxuLy8gICA6aG9zdCAvZGVlcC8gdGFibGUgdGJvZHkgdHI6bnRoLWNoaWxkKGV2ZW4pIHtcbi8vICAgICBiYWNrZ3JvdW5kOiNlOWVhZWEhaW1wb3J0YW50O1xuLy8gICAgIH1cbi8vICAgICA6aG9zdCAvZGVlcC8gdGFibGUgdGJvZHkgdHI6bnRoLWNoaWxkKG9kZCkge1xuLy8gICAgIGJhY2tncm91bmQ6ICB3aGl0ZSFpbXBvcnRhbnQ7XG4vLyAgICAgfVxuXG4gICAgOmhvc3QgL2RlZXAvIHRyLnNvbHZlZCB0ZHtcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjojZWM3NDMxICFpbXBvcnRhbnQ7XG4gICAgICAgIH1cbiAgICAvLyA6aG9zdCAvZGVlcC8gdHIuYWJvcnRlZCB7XG4gICAgLy8gICAgIGJhY2tncm91bmQtY29sb3I6cmVkICFpbXBvcnRhbnQ7XG4gICAgLy8gICAgIGNvbG9yOndoaXRlICFpbXBvcnRhbnQ7XG4gICAgLy8gICAgIH1cbiAgICAgICAgOmhvc3QgL2RlZXAvIHRyLmFib3J0ZWQgdGQge1xuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjojQkQyMDMxICFpbXBvcnRhbnQ7XG4gICAgICAgICAgICBjb2xvcjp3aGl0ZSAhaW1wb3J0YW50O1xuICAgICAgICAgICAgfVxuICAgICAgICA6aG9zdCAvZGVlcC8gbmcyLXNtYXJ0LXRhYmxlIHRib2R5IHRyOmhvdmVyIHtcbiAgICAgICAgICAgIGJhY2tncm91bmQ6bm9uZSAhaW1wb3J0YW50O1xuICAgICAgICB9XG5cbiAgICAgICAgOmhvc3QgL2RlZXAvIHRyLk15Y29sb3IgdGQge1xuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjpyZ2IoNDUsIDE4OSwgMzIpICFpbXBvcnRhbnQ7XG4gICAgICAgICAgICBjb2xvcjp3aGl0ZSAhaW1wb3J0YW50O1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICA6aG9zdCAvZGVlcC8gbmcyLXNtYXJ0LXRhYmxlIG5hdi5uZzItc21hcnQtcGFnaW5hdGlvbi1uYXYgLnBhZ2luYXRpb24ge1xuICAgICAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xuICAgICAgICAgICAgfSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/ui-features/amc/total-amc/total-amc.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/pages/ui-features/amc/total-amc/total-amc.component.ts ***!
  \************************************************************************/
/*! exports provided: TotalAMCComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TotalAMCComponent", function() { return TotalAMCComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var _confirmation_dialog_confirmation_dialog_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../confirmation-dialog/confirmation-dialog.service */ "./src/app/confirmation-dialog/confirmation-dialog.service.ts");
/* harmony import */ var _services_currency_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../services/currency.service */ "./src/app/services/currency.service.ts");
/* harmony import */ var _services_AMC_amc_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../services/AMC/amc.service */ "./src/app/services/AMC/amc.service.ts");
/* harmony import */ var _services_admin_quotation_quotation_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../services/admin/quotation/quotation.service */ "./src/app/services/admin/quotation/quotation.service.ts");
/* harmony import */ var _services_quotations_site_site_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../services/quotations/site/site.service */ "./src/app/services/quotations/site/site.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};









_angular_common__WEBPACK_IMPORTED_MODULE_1__["DatePipe"];
var TotalAMCComponent = /** @class */ (function () {
    function TotalAMCComponent(fb, currency, // VIPIN CODE FOR CURRENCY
    siteServ, QuotationServ, AmcServ, confirmationDialogService, datePipe) {
        this.fb = fb;
        this.currency = currency;
        this.siteServ = siteServ;
        this.QuotationServ = QuotationServ;
        this.AmcServ = AmcServ;
        this.confirmationDialogService = confirmationDialogService;
        this.datePipe = datePipe;
        this.creatShow = false;
        this.tableShow = false;
        this.getSiteNameList = [];
        this.getCompanyNameList = [];
        this.regExpr = /[₹,]/g;
        this.amcPrefix = 'IBE';
        this.settings = {
            add: {
                addButtonContent: '<i class="nb-plus"></i>',
                createButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
            },
            edit: {
                editButtonContent: '<i class="nb-edit"></i>',
                saveButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
            },
            delete: {
                deleteButtonContent: '<i class="nb-trash"></i>',
                confirmDelete: true,
            },
            actions: {
                edit: false,
                add: false,
                delete: false,
            },
            rowClassFunction: function (row) {
                if (row.data.S_NO < 30) {
                    if (row.data.S_NO <= 0) {
                        return 'aborted';
                    }
                    return 'solved';
                }
                else {
                    if (row.data.amcAmount === "0" || row.data.amcAmount === "₹0.00") {
                        return 'Mycolor';
                    }
                }
            },
            columns: {
                amcid: {
                    title: 'AMC Job No',
                },
                jobid: {
                    title: 'Job No',
                    type: 'string',
                },
                // customerName: {
                //   title: 'Company Name',
                //   type: 'html',
                // },
                siteName: {
                    title: 'Site Name',
                    type: 'string',
                },
                toData: {
                    title: 'AMC End Date',
                    type: 'string',
                    valuePrepareFunction: function (date) {
                        return date.split('-').reverse().join('/');
                    }
                },
                S_NO: {
                    title: 'Valid Days',
                    type: Number,
                },
            },
        };
        this.source = new ng2_smart_table__WEBPACK_IMPORTED_MODULE_3__["LocalDataSource"]();
        this.amc = {};
        this.id = '';
        this.listOfAmc = [];
    }
    TotalAMCComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.AmcServ.AmcDone
            .subscribe(function (data) {
            _this.getAMCDetails();
        }, function (error) {
            console.log(error);
        });
        this.fromData = new Date().toISOString().substring(0, 10);
        this.toData = new Date().toISOString().substring(0, 10);
        // this.toData.setFullYear(this.toData.getFullYear() + 1);  // get date after 1 year from today
        this.createForm();
        this.getCompanyList();
        this.getDateFunction();
    };
    TotalAMCComponent.prototype.createForm = function () {
        this.myForm = this.fb.group({
            customerName: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            amcPrefix: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            amcNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            amcid: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            siteName: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            clientAddress: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            siteAddress: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            amcAmount: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            fromData: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            toData: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            clientId: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            siteId: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            unitprice: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            totalPrice: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            PersonName: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            employee: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
        });
    };
    TotalAMCComponent.prototype.changeAmt = function (amt) {
        if (amt[0] === '₹') {
            amt = amt.replace(this.regExpr, "");
        }
        this.myForm.get('unitprice').setValue(amt);
        this.amcAmt = '';
        this.amcAmt = this.currency.convertToInrFormat(amt); // VIPIN CODE FOR CURRENCY
        this.myForm.get('amcAmount').setValue(this.amcAmt);
        this.myForm.get('totalPrice').setValue(this.amcAmt);
    };
    TotalAMCComponent.prototype.getCompanyList = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.QuotationServ.getCompany()];
                    case 1:
                        result = _a.sent();
                        this.getCompanyNameList = result;
                        return [2 /*return*/];
                }
            });
        });
    };
    TotalAMCComponent.prototype.onSelect = function (getCompanyName) {
        var _this = this;
        this.todayDate = new Date();
        this.getCompanyNameList.forEach(function (data) {
            if (getCompanyName === data.companyName) {
                _this.myForm.get('clientAddress').setValue(data.address);
                _this.myForm.get('clientId').setValue(data.id);
                _this.siteServ.getSiteDetails(data.id)
                    .then(function (data) {
                    _this.getSiteNameList = data;
                });
                return;
            }
        });
    };
    TotalAMCComponent.prototype.onSelectSite = function (getSiteName) {
        var _this = this;
        this.getSiteNameList.forEach(function (data) {
            if (getSiteName === data.siteName) {
                _this.siteNameGet = data;
                _this.myForm.get('siteAddress').setValue(data.address);
                _this.myForm.get('siteId').setValue(data.siteId);
                // this.myForm.get('contactPerson').setValue(data.personName);
                // this.myForm.get('contactNumber').setValue(data.mobile);
                return;
            }
        });
    };
    TotalAMCComponent.prototype.getDateFunction = function () {
        var date = new Date();
        var day = date.getDate().toString();
        var hours = date.getHours().toString();
        var mini = date.getMinutes().toString();
        var sec = date.getSeconds().toString();
        this.amcNo = "" + day + hours + mini + sec;
        this.amcid = "" + this.amcPrefix + this.amcNo;
        this.myForm.get('amcPrefix').setValue(this.amcPrefix);
        this.myForm.get('amcNo').setValue(this.amcNo);
        this.myForm.get('amcid').setValue(this.amcid);
    };
    TotalAMCComponent.prototype.updatePre = function (e) {
        this.amcPrefix = e;
        this.amcid = "" + this.amcPrefix + this.amcNo;
        this.myForm.get('amcPrefix').setValue(this.amcPrefix);
        this.myForm.get('amcid').setValue(this.amcid);
    };
    TotalAMCComponent.prototype.updateNo = function (e) {
        this.amcNo = e;
        this.amcid = "" + this.amcPrefix + this.amcNo;
        this.myForm.get('amcNo').setValue(this.amcNo);
        this.myForm.get('amcid').setValue(this.amcid);
    };
    TotalAMCComponent.prototype.showFun = function () {
        this.creatShow = true;
        this.tableShow = false;
    };
    TotalAMCComponent.prototype.getAMCDetails = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, error_1;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        _a = this;
                        return [4 /*yield*/, this.AmcServ.getAllAmc()];
                    case 1:
                        _a.listOfAmc = _b.sent();
                        this.listOfAmc.sort(function (a, b) { return a.amcNo - b.amcNo; });
                        this.listOfAmc.forEach(function (ele) {
                            var date1 = new Date();
                            var date2 = new Date(ele.toData);
                            if (date1.getTime() > date2.getTime()) {
                                var diffTime = Math.abs(date2.getTime() - date1.getTime());
                                var diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
                                diffDays = -("" + diffDays);
                                Object.assign(ele, { S_NO: diffDays });
                            }
                            else {
                                var diffTime = Math.abs(date2.getTime() - date1.getTime());
                                var diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
                                Object.assign(ele, { S_NO: diffDays });
                            }
                        });
                        this.source.load(this.listOfAmc);
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _b.sent();
                        console.error(error_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    TotalAMCComponent.prototype.onUserRowSelect = function (event) {
        event.data.creatShow = false;
        this.AmcServ.AmcDone.next(event.data);
        if (event) {
            this.creatShow = false;
            return this.tableShow = true;
        }
        this.creatShow = true;
        this.tableShow = false;
    };
    TotalAMCComponent.prototype.create = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            var resultAleart, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.confirmationDialogService.confirm('Alert Message', 'Are you sure you want to Create?')];
                    case 1:
                        resultAleart = _a.sent();
                        if (!resultAleart) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.AmcServ.createNewAmc(data.value)];
                    case 2:
                        result = _a.sent();
                        this.AmcServ.AmcDone.next(result);
                        _a.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    // createData(data) {
    //   this.getAMCDetails();
    //   this.listOfAmc.push(data);
    //   this.listOfAmc.sort((a, b) => a.amcNo - b.amcNo);
    //   this.source.load(this.listOfAmc);
    // }
    TotalAMCComponent.prototype.from = function (e) {
        this.myForm.get('fromData').setValue(e);
    };
    TotalAMCComponent.prototype.to = function (e) {
        this.myForm.get('toData').setValue(e);
    };
    TotalAMCComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-total-amc',
            template: __webpack_require__(/*! ./total-amc.component.html */ "./src/app/pages/ui-features/amc/total-amc/total-amc.component.html"),
            styles: [__webpack_require__(/*! ./total-amc.component.scss */ "./src/app/pages/ui-features/amc/total-amc/total-amc.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _services_currency_service__WEBPACK_IMPORTED_MODULE_5__["CurrencyService"],
            _services_quotations_site_site_service__WEBPACK_IMPORTED_MODULE_8__["SiteService"],
            _services_admin_quotation_quotation_service__WEBPACK_IMPORTED_MODULE_7__["QuotationService"],
            _services_AMC_amc_service__WEBPACK_IMPORTED_MODULE_6__["AmcService"],
            _confirmation_dialog_confirmation_dialog_service__WEBPACK_IMPORTED_MODULE_4__["ConfirmationDialogService"],
            _angular_common__WEBPACK_IMPORTED_MODULE_1__["DatePipe"]])
    ], TotalAMCComponent);
    return TotalAMCComponent;
}());



/***/ }),

/***/ "./src/app/services/AMC/amc-maintanance.service.ts":
/*!*********************************************************!*\
  !*** ./src/app/services/AMC/amc-maintanance.service.ts ***!
  \*********************************************************/
/*! exports provided: AmcMaintananceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AmcMaintananceService", function() { return AmcMaintananceService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _settings_settings_services_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../settings/settings-services.service */ "./src/app/services/settings/settings-services.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AmcMaintananceService = /** @class */ (function () {
    function AmcMaintananceService(http, settings) {
        this.http = http;
        this.settings = settings;
        this.AmcDone = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]({});
        this.API_URL = this.settings.BASE_API_URL;
    }
    // updateAmc(data, id) {
    //   const url = `${this.API_URL}/amc/amc-maintanace/amcUpdate/${id}`;
    //   return new Promise((resolve, reject) => {
    //     this.http.put(url, data)
    //       .subscribe(resolve, reject);
    //   });
    // }
    AmcMaintananceService.prototype.createAmc = function (data) {
        var _this = this;
        var url = this.API_URL + "/amc/amc-maintanace";
        return new Promise(function (resolve, reject) {
            _this.http.post(url, data)
                .subscribe(resolve, reject);
        });
    };
    AmcMaintananceService.prototype.getAllAmc = function () {
        var _this = this;
        var url = this.API_URL + "/amc/amc-maintanace";
        return new Promise(function (resolve, reject) {
            _this.http.get(url)
                .subscribe(resolve, reject);
        });
    };
    AmcMaintananceService.prototype.getAllAmcId = function (id) {
        var _this = this;
        var url = this.API_URL + "/amc/amc-maintanace/" + id;
        return new Promise(function (resolve, reject) {
            _this.http.get(url)
                .subscribe(resolve, reject);
        });
    };
    AmcMaintananceService.prototype.UpdateAmcDetails = function (value, id) {
        var _this = this;
        var url = this.API_URL + "/amc/amc-maintanace/" + id;
        return new Promise(function (resolve, reject) {
            _this.http.put(url, value)
                .subscribe(resolve, reject);
        });
    };
    AmcMaintananceService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _settings_settings_services_service__WEBPACK_IMPORTED_MODULE_3__["SettingsServicesService"]])
    ], AmcMaintananceService);
    return AmcMaintananceService;
}());



/***/ }),

/***/ "./src/app/services/AMC/amc-passwords.service.ts":
/*!*******************************************************!*\
  !*** ./src/app/services/AMC/amc-passwords.service.ts ***!
  \*******************************************************/
/*! exports provided: AmcPasswordsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AmcPasswordsService", function() { return AmcPasswordsService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _settings_settings_services_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../settings/settings-services.service */ "./src/app/services/settings/settings-services.service.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AmcPasswordsService = /** @class */ (function () {
    function AmcPasswordsService(http, settings) {
        this.http = http;
        this.settings = settings;
        this.AmcDone = new rxjs__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"]({});
        this.API_URL = this.settings.BASE_API_URL;
    }
    AmcPasswordsService.prototype.createAmc = function (data) {
        var _this = this;
        var url = this.API_URL + "/amc/amc-passwords";
        return new Promise(function (resolve, reject) {
            _this.http.post(url, data)
                .subscribe(resolve, reject);
        });
    };
    AmcPasswordsService.prototype.getAllAmc = function () {
        var _this = this;
        var url = this.API_URL + "/amc/amc-passwords";
        return new Promise(function (resolve, reject) {
            _this.http.get(url)
                .subscribe(resolve, reject);
        });
    };
    AmcPasswordsService.prototype.getAllAmcId = function (id) {
        var _this = this;
        var url = this.API_URL + "/amc/amc-passwords/" + id;
        return new Promise(function (resolve, reject) {
            _this.http.get(url)
                .subscribe(resolve, reject);
        });
    };
    AmcPasswordsService.prototype.UpdateAmcDetails = function (value, id) {
        var _this = this;
        var url = this.API_URL + "/amc/amc-passwords/" + id;
        return new Promise(function (resolve, reject) {
            _this.http.put(url, value)
                .subscribe(resolve, reject);
        });
    };
    AmcPasswordsService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _settings_settings_services_service__WEBPACK_IMPORTED_MODULE_2__["SettingsServicesService"]])
    ], AmcPasswordsService);
    return AmcPasswordsService;
}());



/***/ }),

/***/ "./src/app/services/AMC/amc-upload.service.ts":
/*!****************************************************!*\
  !*** ./src/app/services/AMC/amc-upload.service.ts ***!
  \****************************************************/
/*! exports provided: AmcDocService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AmcDocService", function() { return AmcDocService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _settings_settings_services_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../settings/settings-services.service */ "./src/app/services/settings/settings-services.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AmcDocService = /** @class */ (function () {
    function AmcDocService(http, settings) {
        this.http = http;
        this.settings = settings;
        this.userAmcDoc = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]({});
        this.userAmcDoc1 = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]({});
        this.API_URL = this.settings.BASE_API_URL;
    }
    /**
          AmcDoc Attachments Details Started from here
       **/
    AmcDocService.prototype.createAttachmentsAmcDoc = function (attachment) {
        var _this = this;
        var url = this.API_URL + "/amc/amc-uploads/";
        return new Promise(function (resolve, reject) {
            _this.http.post(url, attachment)
                .subscribe(resolve, reject);
        });
    };
    AmcDocService.prototype.getAttachmentsAmcDocs = function () {
        var _this = this;
        var url = this.API_URL + "/amc/amc-uploads/";
        return new Promise(function (resolve, reject) {
            _this.http.get(url)
                .subscribe(resolve, reject);
        });
    };
    AmcDocService.prototype.getAttachmentsAmcDocsById = function (id) {
        var _this = this;
        var url = this.API_URL + "/amc/amc-uploads/" + id;
        return new Promise(function (resolve, reject) {
            _this.http.get(url)
                .subscribe(resolve, reject);
        });
    };
    AmcDocService.prototype.UpdateAttachmentsAmcDocsDetails = function (value) {
        var _this = this;
        var id = value.id;
        var url = this.API_URL + "/amc/amc-uploads/" + id;
        return new Promise(function (resolve, reject) {
            _this.http.put(url, value)
                .subscribe(resolve, reject);
        });
    };
    AmcDocService.prototype.deleteAttachmentsAmcDoc = function (value, id) {
        var _this = this;
        var url = this.API_URL + "/amc/amc-uploads/remove/" + id;
        return new Promise(function (resolve, reject) {
            _this.http.put(url, value)
                .subscribe(resolve, reject);
        });
    };
    AmcDocService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _settings_settings_services_service__WEBPACK_IMPORTED_MODULE_3__["SettingsServicesService"]])
    ], AmcDocService);
    return AmcDocService;
}());



/***/ })

}]);
//# sourceMappingURL=default~amc-amc-module~app-pages-pages-module~ui-features-ui-features-module.js.map