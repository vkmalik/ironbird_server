(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["hr-payroll-hr-payroll-module"],{

/***/ "./src/app/services/admin/employee-details/employee.service.ts":
/*!*********************************************************************!*\
  !*** ./src/app/services/admin/employee-details/employee.service.ts ***!
  \*********************************************************************/
/*! exports provided: EmployeeService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeeService", function() { return EmployeeService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _settings_settings_services_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../settings/settings-services.service */ "./src/app/services/settings/settings-services.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var EmployeeService = /** @class */ (function () {
    function EmployeeService(http, settings) {
        this.http = http;
        this.settings = settings;
        this.userEmployee = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]({});
        this.API_URL = this.settings.BASE_API_URL;
    }
    EmployeeService.prototype.createEmployee = function (data) {
        var _this = this;
        var url = this.API_URL + "/admin/employee-details";
        return new Promise(function (resolve, reject) {
            _this.http.post(url, data)
                .subscribe(resolve, reject);
        });
    };
    // to upload IMG
    EmployeeService.prototype.uploadImg = function (id, data) {
        var _this = this;
        var url = this.API_URL + "/admin/employee-details/image/" + id;
        return new Promise(function (resolve, reject) {
            _this.http.put(url, data)
                .subscribe(resolve, reject);
        });
    };
    // to upload GOVTID
    EmployeeService.prototype.uploadGovtID = function (id, data) {
        var _this = this;
        var url = this.API_URL + "/admin/employee-details/govtID/" + id;
        return new Promise(function (resolve, reject) {
            _this.http.put(url, data)
                .subscribe(resolve, reject);
        });
    };
    // to upload RESUME
    EmployeeService.prototype.uploadResume = function (id, data) {
        var _this = this;
        var url = this.API_URL + "/admin/employee-details/resume/" + id;
        return new Promise(function (resolve, reject) {
            _this.http.put(url, data)
                .subscribe(resolve, reject);
        });
    };
    EmployeeService.prototype.getEmployee = function () {
        var _this = this;
        var url = this.API_URL + "/admin/employee-details";
        return new Promise(function (resolve, reject) {
            _this.http.get(url)
                .subscribe(resolve, reject);
        });
    };
    EmployeeService.prototype.getEmployeeOutSide = function () {
        var _this = this;
        var url = this.API_URL + "/admin/employee-details/outside-employee";
        return new Promise(function (resolve, reject) {
            _this.http.get(url)
                .subscribe(resolve, reject);
        });
    };
    EmployeeService.prototype.updateEmployee = function (value, data) {
        var _this = this;
        var id = value.id;
        // console.log(id);
        // console.log(data);
        var url = this.API_URL + "/admin/employee-details/" + id;
        return new Promise(function (resolve, reject) {
            _this.http.put(url, data)
                .subscribe(resolve, reject);
        });
    };
    EmployeeService.prototype.getEmployeeDetails = function (id) {
        var _this = this;
        var url = this.API_URL + "/admin/employee-details/" + id;
        //  console.log(url,"link")
        return new Promise(function (resolve, reject) {
            _this.http.get(url)
                .subscribe(resolve, reject);
        });
    };
    EmployeeService.prototype.removeEmployee = function (value) {
        var _this = this;
        var userId = value.userId;
        var url = this.API_URL + "/admin/employee-details/" + userId;
        return new Promise(function (resolve, reject) {
            _this.http.delete(url)
                .subscribe(resolve, reject);
        });
    };
    EmployeeService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _settings_settings_services_service__WEBPACK_IMPORTED_MODULE_3__["SettingsServicesService"]])
    ], EmployeeService);
    return EmployeeService;
}());



/***/ }),

/***/ "./src/app/services/admin/user-management/user-management.service.ts":
/*!***************************************************************************!*\
  !*** ./src/app/services/admin/user-management/user-management.service.ts ***!
  \***************************************************************************/
/*! exports provided: UserManagementService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserManagementService", function() { return UserManagementService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _settings_settings_services_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../settings/settings-services.service */ "./src/app/services/settings/settings-services.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var UserManagementService = /** @class */ (function () {
    function UserManagementService(http, settings) {
        this.http = http;
        this.settings = settings;
        this.usersEmployee = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]({});
        this.API_URL = this.settings.BASE_API_URL;
    }
    UserManagementService.prototype.createUserManagement = function (data) {
        var _this = this;
        var url = this.API_URL + "/admin/user-managment";
        return new Promise(function (resolve, reject) {
            _this.http.post(url, data)
                .subscribe(resolve, reject);
        });
    };
    UserManagementService.prototype.getUserManagement = function () {
        var _this = this;
        var url = this.API_URL + "/admin/user-managment";
        return new Promise(function (resolve, reject) {
            _this.http.get(url)
                .subscribe(resolve, reject);
        });
    };
    UserManagementService.prototype.updateUserManagement = function (data) {
        var _this = this;
        var userId = data.userId;
        var url = this.API_URL + "/auth/reset-password/" + userId;
        return new Promise(function (resolve, reject) {
            _this.http.put(url, data)
                .subscribe(resolve, reject);
        });
    };
    UserManagementService.prototype.removeUserManagement = function (value) {
        var _this = this;
        var userId = value.userId;
        var url = this.API_URL + "/admin/user-managment/" + userId;
        return new Promise(function (resolve, reject) {
            _this.http.delete(url)
                .subscribe(resolve, reject);
        });
    };
    UserManagementService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _settings_settings_services_service__WEBPACK_IMPORTED_MODULE_3__["SettingsServicesService"]])
    ], UserManagementService);
    return UserManagementService;
}());



/***/ })

}]);
//# sourceMappingURL=hr-payroll-hr-payroll-module.js.map