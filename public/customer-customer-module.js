(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["customer-customer-module"],{

/***/ "./src/app/services/Inventory/inventory.service.ts":
/*!*********************************************************!*\
  !*** ./src/app/services/Inventory/inventory.service.ts ***!
  \*********************************************************/
/*! exports provided: InventoryService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InventoryService", function() { return InventoryService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _settings_settings_services_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../settings/settings-services.service */ "./src/app/services/settings/settings-services.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var InventoryService = /** @class */ (function () {
    function InventoryService(http, settings) {
        this.http = http;
        this.settings = settings;
        this.userInventory = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]({});
        this.API_URL = this.settings.BASE_API_URL;
    }
    InventoryService.prototype.createInventory = function (data) {
        var _this = this;
        var url = this.API_URL + "/inventory/inventory-details";
        return new Promise(function (resolve, reject) {
            _this.http.post(url, data)
                .subscribe(resolve, reject);
        });
    };
    InventoryService.prototype.getInventory = function () {
        var _this = this;
        var url = this.API_URL + "/inventory/inventory-details";
        return new Promise(function (resolve, reject) {
            _this.http.get(url)
                .subscribe(resolve, reject);
        });
    };
    InventoryService.prototype.UpdateInventoryDetails = function (value, id) {
        var _this = this;
        // console.log(value, id)
        var url = this.API_URL + "/inventory/inventory-details/" + id;
        return new Promise(function (resolve, reject) {
            _this.http.put(url, value)
                .subscribe(resolve, reject);
        });
    };
    InventoryService.prototype.getInventoryByID = function (id) {
        var _this = this;
        var url = this.API_URL + "/inventory/inventory-details/" + id;
        return new Promise(function (resolve, reject) {
            _this.http.get(url)
                .subscribe(resolve, reject);
        });
    };
    InventoryService.prototype.deleteInventoryByID = function (id) {
        var _this = this;
        var url = this.API_URL + "/inventory/inventory-details/" + id;
        return new Promise(function (resolve, reject) {
            _this.http.delete(url)
                .subscribe(resolve, reject);
        });
    };
    InventoryService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _settings_settings_services_service__WEBPACK_IMPORTED_MODULE_3__["SettingsServicesService"]])
    ], InventoryService);
    return InventoryService;
}());



/***/ }),

/***/ "./src/app/services/admin/isertParts/insert-parts.service.ts":
/*!*******************************************************************!*\
  !*** ./src/app/services/admin/isertParts/insert-parts.service.ts ***!
  \*******************************************************************/
/*! exports provided: InsertPartsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InsertPartsService", function() { return InsertPartsService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _settings_settings_services_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../settings/settings-services.service */ "./src/app/services/settings/settings-services.service.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var InsertPartsService = /** @class */ (function () {
    function InsertPartsService(http, settings) {
        this.http = http;
        this.settings = settings;
        this.partsDetails = new rxjs__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"]({});
        this.API_URL = this.settings.BASE_API_URL;
    }
    InsertPartsService.prototype.createPartDetails = function (data) {
        var _this = this;
        var url = this.API_URL + "/admin/part-details";
        return new Promise(function (resolve, reject) {
            _this.http.post(url, data)
                .subscribe(resolve, reject);
        });
    };
    InsertPartsService.prototype.getPartDetails = function () {
        var _this = this;
        var url = this.API_URL + "/admin/part-details/";
        return new Promise(function (resolve, reject) {
            _this.http.get(url)
                .subscribe(resolve, reject);
        });
    };
    InsertPartsService.prototype.updatePartDetails = function (data) {
        var _this = this;
        var id = data.id;
        var url = this.API_URL + "/admin/part-details/" + id;
        return new Promise(function (resolve, reject) {
            _this.http.put(url, data)
                .subscribe(resolve, reject);
        });
    };
    InsertPartsService.prototype.removePartDetails = function (value) {
        var _this = this;
        var id = value.id;
        var url = this.API_URL + "/admin/part-details/" + id;
        return new Promise(function (resolve, reject) {
            _this.http.delete(url)
                .subscribe(resolve, reject);
        });
    };
    InsertPartsService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _settings_settings_services_service__WEBPACK_IMPORTED_MODULE_2__["SettingsServicesService"]])
    ], InsertPartsService);
    return InsertPartsService;
}());



/***/ }),

/***/ "./src/app/services/customer/customer.service.ts":
/*!*******************************************************!*\
  !*** ./src/app/services/customer/customer.service.ts ***!
  \*******************************************************/
/*! exports provided: CustomerService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerService", function() { return CustomerService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _settings_settings_services_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../settings/settings-services.service */ "./src/app/services/settings/settings-services.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CustomerService = /** @class */ (function () {
    function CustomerService(http, settings) {
        this.http = http;
        this.settings = settings;
        this.userCustomer = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]({});
        this.userCustomer1 = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]({});
        this.API_URL = this.settings.BASE_API_URL;
    }
    CustomerService.prototype.createCustomer = function (data) {
        var _this = this;
        var url = this.API_URL + "/customer/customer-details";
        return new Promise(function (resolve, reject) {
            _this.http.post(url, data)
                .subscribe(resolve, reject);
        });
    };
    CustomerService.prototype.getCustomer = function () {
        var _this = this;
        var url = this.API_URL + "/customer/customer-details";
        return new Promise(function (resolve, reject) {
            _this.http.get(url)
                .subscribe(resolve, reject);
        });
    };
    CustomerService.prototype.UpdateCustomerDetails = function (value, id) {
        var _this = this;
        var url = this.API_URL + "/customer/customer-details/" + id;
        return new Promise(function (resolve, reject) {
            _this.http.put(url, value)
                .subscribe(resolve, reject);
        });
    };
    CustomerService.prototype.getCustomerByID = function (id) {
        var _this = this;
        var url = this.API_URL + "/customer/customer-details/" + id;
        return new Promise(function (resolve, reject) {
            _this.http.get(url)
                .subscribe(resolve, reject);
        });
    };
    CustomerService.prototype.createGeneralInvoice = function (value) {
        var _this = this;
        var url = this.API_URL + "/customer/generalInvoices";
        return new Promise(function (resolve, reject) {
            _this.http.post(url, value)
                .subscribe(resolve, reject);
        });
    };
    CustomerService.prototype.getGeneralInvoice = function () {
        var _this = this;
        var url = this.API_URL + "/customer/generalInvoices";
        return new Promise(function (resolve, reject) {
            _this.http.get(url)
                .subscribe(resolve, reject);
        });
    };
    CustomerService.prototype.getInvoices = function (id) {
        var _this = this;
        var url = this.API_URL + "/customer/generalInvoices/" + id;
        return new Promise(function (resolve, reject) {
            _this.http.get(url)
                .subscribe(resolve, reject);
        });
    };
    CustomerService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _settings_settings_services_service__WEBPACK_IMPORTED_MODULE_3__["SettingsServicesService"]])
    ], CustomerService);
    return CustomerService;
}());



/***/ }),

/***/ "./src/app/services/jobs/invoice-challans.service.ts":
/*!***********************************************************!*\
  !*** ./src/app/services/jobs/invoice-challans.service.ts ***!
  \***********************************************************/
/*! exports provided: InvoiceChallansService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InvoiceChallansService", function() { return InvoiceChallansService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _settings_settings_services_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../settings/settings-services.service */ "./src/app/services/settings/settings-services.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var InvoiceChallansService = /** @class */ (function () {
    function InvoiceChallansService(http, settings) {
        this.http = http;
        this.settings = settings;
        this.API_URL = this.settings.BASE_API_URL;
    }
    InvoiceChallansService.prototype.createinvoiceChallans = function (data) {
        var _this = this;
        var url = this.API_URL + "/jobs/invoice-challan";
        return new Promise(function (resolve, reject) {
            _this.http.post(url, data)
                .subscribe(resolve, reject);
        });
    };
    InvoiceChallansService.prototype.uploadinvoiceChallans = function (data, id) {
        var _this = this;
        var url = this.API_URL + "/jobs/invoice-challan/" + id;
        return new Promise(function (resolve, reject) {
            _this.http.put(url, data)
                .subscribe(resolve, reject);
        });
    };
    InvoiceChallansService.prototype.getAllInvoices = function () {
        var _this = this;
        var url = this.API_URL + "/jobs/invoice-challan/";
        return new Promise(function (resolve, reject) {
            _this.http.get(url)
                .subscribe(resolve, reject);
        });
    };
    InvoiceChallansService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _settings_settings_services_service__WEBPACK_IMPORTED_MODULE_2__["SettingsServicesService"]])
    ], InvoiceChallansService);
    return InvoiceChallansService;
}());



/***/ }),

/***/ "./src/app/services/jobs/total.service.ts":
/*!************************************************!*\
  !*** ./src/app/services/jobs/total.service.ts ***!
  \************************************************/
/*! exports provided: TotalJobsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TotalJobsService", function() { return TotalJobsService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _settings_settings_services_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../settings/settings-services.service */ "./src/app/services/settings/settings-services.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TotalJobsService = /** @class */ (function () {
    function TotalJobsService(http, settings) {
        this.http = http;
        this.settings = settings;
        this.jobList = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]({});
        this.jobList1 = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]({});
        this.API_URL = this.settings.BASE_API_URL;
    }
    TotalJobsService.prototype.createJobs = function (data) {
        var _this = this;
        var url = this.API_URL + "/admin/agreement-jobs";
        return new Promise(function (resolve, reject) {
            _this.http.post(url, data)
                .subscribe(resolve, reject);
        });
    };
    TotalJobsService.prototype.getJobListClintId = function (id) {
        var _this = this;
        var url = this.API_URL + "/admin/agreement-jobs/" + id;
        return new Promise(function (resolve, reject) {
            _this.http.get(url)
                .subscribe(resolve, reject);
        });
    };
    TotalJobsService.prototype.getJobsDetails = function () {
        var _this = this;
        var url = this.API_URL + "/admin/agreement-jobs";
        return new Promise(function (resolve, reject) {
            _this.http.get(url)
                .subscribe(resolve, reject);
        });
    };
    TotalJobsService.prototype.getJobs = function (id) {
        var _this = this;
        var url = this.API_URL + "/admin/agreement-jobs";
        //  console.log(url)
        if (id) {
            // console.log(id,"server id")
            var stages77 = ['stage1', 'stage2'];
            var result = stages77.findIndex(function (e) {
                return e === id;
            });
            //  console.log(result);
            if (result === -1) {
                url = this.API_URL + "/admin/agreement-jobs/" + id;
                // console.log(url,"hi in")
            }
            else {
                url = this.API_URL + "/admin/agreement-jobs/stage/" + id;
                //  console.log(url,"hi out")
            }
        }
        return new Promise(function (resolve, reject) {
            _this.http.get(url)
                .subscribe(resolve, reject);
        });
    };
    TotalJobsService.prototype.rowSelectedValue = function (data) {
        var url = this.API_URL + "/admin/agreement-jobs/" + data;
        return this.http.get(url);
    };
    TotalJobsService.prototype.getJobByID = function (id) {
        var _this = this;
        var url = this.API_URL + "/admin/agreement-jobs/" + id;
        return new Promise(function (resolve, reject) {
            _this.http.get(url)
                .subscribe(resolve, reject);
        });
    };
    TotalJobsService.prototype.getJobsById = function (data) {
        var _this = this;
        var url = this.API_URL + "/admin/agreement-jobs/individual/" + data;
        return new Promise(function (resolve, reject) {
            _this.http.get(url)
                .subscribe(resolve, reject);
        });
    };
    TotalJobsService.prototype.UpdateJobDetail = function (value, id) {
        var _this = this;
        var url = this.API_URL + "/admin/agreement-jobs/" + id;
        return new Promise(function (resolve, reject) {
            _this.http.put(url, value)
                .subscribe(resolve, reject);
        });
    };
    // to update maintenance
    TotalJobsService.prototype.UpdateJobMaintenance = function (value) {
        var _this = this;
        var id = value.id;
        // console.log(id);
        var url = this.API_URL + "/admin/agreement-jobs/maintenance/" + id;
        return new Promise(function (resolve, reject) {
            _this.http.put(url, value)
                .subscribe(resolve, reject);
        });
    };
    TotalJobsService.prototype.UpdateJobDetails = function (value) {
        var _this = this;
        var id = value.id;
        var url = this.API_URL + "/admin/agreement-jobs/" + id;
        return new Promise(function (resolve, reject) {
            _this.http.put(url, value)
                .subscribe(resolve, reject);
        });
    };
    TotalJobsService.prototype.getMaintaines = function () {
        var _this = this;
        var url = this.API_URL + "/admin/maintenance-details";
        return new Promise(function (resolve, reject) {
            _this.http.get(url)
                .subscribe(resolve, reject);
        });
    };
    TotalJobsService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _settings_settings_services_service__WEBPACK_IMPORTED_MODULE_3__["SettingsServicesService"]])
    ], TotalJobsService);
    return TotalJobsService;
}());



/***/ })

}]);
//# sourceMappingURL=customer-customer-module.js.map