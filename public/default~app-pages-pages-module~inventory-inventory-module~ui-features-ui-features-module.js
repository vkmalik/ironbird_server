(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~app-pages-pages-module~inventory-inventory-module~ui-features-ui-features-module"],{

/***/ "./src/app/pages/ui-features/inventory/inventory-dashboard/inventory-dashboard.component.html":
/*!****************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/inventory/inventory-dashboard/inventory-dashboard.component.html ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n    <nb-card size=\"large\">\n      <nb-tabset fullWidth style=\"background-color: #1D8ECE\">\n        <nb-tab tabTitle=\"Things To Do\" style=\"background-color: #1D8ECE\">\n          <div class=\"chart-container\">\n              <nb-card size=\"large\">\n                  <nb-card-header>\n                    Main Things To Do\n                  </nb-card-header>\n                  <nb-list>\n                    <nb-list-item *ngFor=\"let hero of heroes\">\n                        <span class=\"badge\">{{hero.id}}</span>  {{hero.name}}\n                    </nb-list-item>\n                  </nb-list>\n                </nb-card>\n            \n          </div>\n        </nb-tab>\n        <nb-tab tabTitle=\"List To Items\" [lazyLoad]=\"true\" style=\"background-color: #1D8ECE\">\n          <div class=\"chart-container\">\n             <!-- <a routerLink=\"/pages/ui-features/MainQuotation\"></a> -->\n              <nb-card-front>\n                <nb-card >\n          <nb-card-header>\n              Minimume Stock Alert\n            </nb-card-header>\n            <nb-card-body>\n                <ng2-smart-table [settings]=\"settings\" [source]=\"source\" (deleteConfirm)=\"onDeleteConfirm($event)\" >\n                  </ng2-smart-table>\n            </nb-card-body>\n            </nb-card>\n            </nb-card-front>\n            \n          </div>\n        </nb-tab>\n      </nb-tabset>\n    </nb-card>\n  "

/***/ }),

/***/ "./src/app/pages/ui-features/inventory/inventory-dashboard/inventory-dashboard.component.scss":
/*!****************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/inventory/inventory-dashboard/inventory-dashboard.component.scss ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "nb-card-body {\n  background-color: #E1F5FE;\n  color: white; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3ByYWthc2gvVmlwaW4vaXJvbmJpcmQvMjBGRUIyMDIwL2lyb25iaXJkLWNsaWVudHMvc3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9pbnZlbnRvcnkvaW52ZW50b3J5LWRhc2hib2FyZC9pbnZlbnRvcnktZGFzaGJvYXJkLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUlBO0VBQ0UseUJBQXdCO0VBQ3hCLFlBQVcsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VpLWZlYXR1cmVzL2ludmVudG9yeS9pbnZlbnRvcnktZGFzaGJvYXJkL2ludmVudG9yeS1kYXNoYm9hcmQuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBuYi1jYXJkLWhlYWRlcntcbi8vICAgICBiYWNrZ3JvdW5kOiAjMUQ4RUNFO1xuLy8gICAgIGNvbG9yOiB3aGl0ZTtcbi8vIH1cbm5iLWNhcmQtYm9keXtcbiAgYmFja2dyb3VuZC1jb2xvcjojRTFGNUZFO1xuICBjb2xvcjp3aGl0ZTtcbn1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/pages/ui-features/inventory/inventory-dashboard/inventory-dashboard.component.ts":
/*!**************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/inventory/inventory-dashboard/inventory-dashboard.component.ts ***!
  \**************************************************************************************************/
/*! exports provided: Hero, HEROES, InventoryDashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Hero", function() { return Hero; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HEROES", function() { return HEROES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InventoryDashboardComponent", function() { return InventoryDashboardComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var _core_data_stockalert_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../@core/data/stockalert.service */ "./src/app/@core/data/stockalert.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var Hero = /** @class */ (function () {
    function Hero() {
    }
    return Hero;
}());

var HEROES = [
    { id: 1, name: 'Followup on the agreement with Dayal Constructions. ' },
    { id: 2, name: 'Status update of stage 2 of Nagaarjuna constructions.' },
    { id: 3, name: 'Followup on the installation report of Sreenidi constructions.' },
    { id: 4, name: 'Erection status of Project 2' },
];
var InventoryDashboardComponent = /** @class */ (function () {
    function InventoryDashboardComponent(service) {
        this.service = service;
        this.heroes = HEROES;
        this.settings = {
            add: {
                addButtonContent: '<i class="nb-plus"></i>',
                createButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
            },
            edit: {
                editButtonContent: '<i class="nb-edit"></i>',
                saveButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
            },
            delete: {
                deleteButtonContent: '<i class="nb-trash"></i>',
                confirmDelete: true,
            },
            view: {
                viewButtonContent: '<i  class="nb-edit"></i>'
            },
            actions: {
                edit: false,
                add: false,
                delete: false,
                position: 'right'
            },
            columns: {
                id: {
                    title: 'SI No',
                    type: 'number',
                },
                item: {
                    title: 'Item Name',
                    type: 'string',
                },
                available: {
                    title: 'Available',
                    type: 'string',
                },
                required: {
                    title: 'Required Minimum',
                    type: 'string',
                },
                alerts: {
                    title: 'Alert',
                    type: 'string',
                },
            },
        };
        this.source = new ng2_smart_table__WEBPACK_IMPORTED_MODULE_1__["LocalDataSource"]();
        var data = this.service.getData();
        this.source.load(data);
    }
    InventoryDashboardComponent.prototype.ngOnInit = function () {
    };
    InventoryDashboardComponent.prototype.onDeleteConfirm = function (event) {
        if (window.confirm('Are you sure you want to delete?')) {
            event.confirm.resolve();
        }
        else {
            event.confirm.reject();
        }
    };
    InventoryDashboardComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-inventory-dashboard',
            template: __webpack_require__(/*! ./inventory-dashboard.component.html */ "./src/app/pages/ui-features/inventory/inventory-dashboard/inventory-dashboard.component.html"),
            styles: [__webpack_require__(/*! ./inventory-dashboard.component.scss */ "./src/app/pages/ui-features/inventory/inventory-dashboard/inventory-dashboard.component.scss")]
        }),
        __metadata("design:paramtypes", [_core_data_stockalert_service__WEBPACK_IMPORTED_MODULE_2__["StockAlertService"]])
    ], InventoryDashboardComponent);
    return InventoryDashboardComponent;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/inventory/inventory-main/inventory-parts/inventory-parts.component.html":
/*!***********************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/inventory/inventory-main/inventory-parts/inventory-parts.component.html ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n     \n               <ngb-tabset>\n                   <ngb-tab title=\"Details\">\n                     <ng-template ngbTabContent>\n                            <!-- <h4 class=\"text-center\"> Part details</h4> -->\n                         <ngx-partdetails></ngx-partdetails>\n                     </ng-template>\n                   </ngb-tab>\n                   \n                   <!-- <ngb-tab title=\"Purchase\" >\n                     <ng-template ngbTabContent>\n                         <ngx-partdetails></ngx-partdetails>\n                        </ng-template>\n                   </ngb-tab>\n                   <ngb-tab title=\"Invoices\" >\n                       <ng-template ngbTabContent>\n                           <ngx-partdetails></ngx-partdetails>\n                          </ng-template>\n                     </ngb-tab> -->\n                     \n                      \n                           \n                 </ngb-tabset>\n    \n  \n       "

/***/ }),

/***/ "./src/app/pages/ui-features/inventory/inventory-main/inventory-parts/inventory-parts.component.scss":
/*!***********************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/inventory/inventory-main/inventory-parts/inventory-parts.component.scss ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ngx-partdetails nb-card nb-card-header {\n  background: #E1F5FE !important;\n  color: black !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3ByYWthc2gvVmlwaW4vaXJvbmJpcmQvMjBGRUIyMDIwL2lyb25iaXJkLWNsaWVudHMvc3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9pbnZlbnRvcnkvaW52ZW50b3J5LW1haW4vaW52ZW50b3J5LXBhcnRzL2ludmVudG9yeS1wYXJ0cy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFJQTtFQUNJLDhCQUE4QjtFQUM5Qix1QkFBdUIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VpLWZlYXR1cmVzL2ludmVudG9yeS9pbnZlbnRvcnktbWFpbi9pbnZlbnRvcnktcGFydHMvaW52ZW50b3J5LXBhcnRzLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gbmItY2FyZHtcbi8vICAgICBiYWNrZ3JvdW5kLWNvbG9yOiNFMUY1RkU7XG4vLyB9XG5cbm5neC1wYXJ0ZGV0YWlscyBuYi1jYXJkIG5iLWNhcmQtaGVhZGVye1xuICAgIGJhY2tncm91bmQ6ICNFMUY1RkUgIWltcG9ydGFudDtcbiAgICBjb2xvcjogYmxhY2sgIWltcG9ydGFudDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/ui-features/inventory/inventory-main/inventory-parts/inventory-parts.component.ts":
/*!*********************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/inventory/inventory-main/inventory-parts/inventory-parts.component.ts ***!
  \*********************************************************************************************************/
/*! exports provided: InventoryPartsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InventoryPartsComponent", function() { return InventoryPartsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var InventoryPartsComponent = /** @class */ (function () {
    function InventoryPartsComponent() {
    }
    InventoryPartsComponent.prototype.ngOnInit = function () {
    };
    InventoryPartsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-inventory-parts',
            template: __webpack_require__(/*! ./inventory-parts.component.html */ "./src/app/pages/ui-features/inventory/inventory-main/inventory-parts/inventory-parts.component.html"),
            styles: [__webpack_require__(/*! ./inventory-parts.component.scss */ "./src/app/pages/ui-features/inventory/inventory-main/inventory-parts/inventory-parts.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], InventoryPartsComponent);
    return InventoryPartsComponent;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/inventory/inventory-main/inventory-parts/partdetails/partdetails.component.html":
/*!*******************************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/inventory/inventory-main/inventory-parts/partdetails/partdetails.component.html ***!
  \*******************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h4 class=\"text-center\">\n    Part Details\n</h4>\n<nb-card-body>\n    <form [formGroup]=\"myForm\" (ngSubmit)=\"createAndEdit(myForm)\">\n        <div class=\"container\">\n            <br>\n            <!-- <h4 class=\"text-center\"></h4> -->\n            <div class=\"row\">\n\n\n                <div class=\"col-6\">\n                    <div class=\"form-group input-group-sm\">\n                        <label for=\"Date\">Material Stage</label>\n                        <select class=\"form-control\" id=\"Date\" formControlName=\"selectedStage\" size=\"small\" [(ngModel)]=\"stageName\" (change)=\"selectMaterial(stageName)\" [ngClass]=\"{ 'is-invalid': submitted && f.selectedStage.errors }\">\n              <option *ngFor=\"let designation of designations;let i = index\" [value]=\"designation\">{{designation}}</option>\n            </select>\n                        <div *ngIf=\" submitted && f.selectedStage.errors\" class=\"invalid-feedback\">\n                            <div *ngIf=\"f.selectedStage.errors.required\">Material Stage is required</div>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"col-6\">\n                    <div class=\"form-group input-group-sm\">\n                        <label for=\"Date\"> Supplier Name</label>\n                        <input list=\"browsers\" type=\"text\" class=\"form-control\" id=\"Date\" (change)=\"onSelect(supplierName)\" [(ngModel)]='supplierName' formControlName=\"supplierName\" autocomplete=\"ON\" placeholder=\"Supplier Name\" [ngClass]=\"{ 'is-invalid': submitted && f.supplierName.errors }\">\n                        <div *ngIf=\" submitted && f.supplierName.errors\" class=\"invalid-feedback\">\n                            <div *ngIf=\"f.supplierName.errors.required\">Supplier Name is required</div>\n                        </div>\n                        <datalist id=\"browsers\">\n                            <option *ngFor=\"let supplier of getSupplierNameList;let i = index\" [value]=\"supplier.CompanyName\">{{supplier.CompanyName}}</option>\n                        </datalist>\n                    </div>\n                </div>\n            </div>\n\n            <div class=\"row\">\n                <div class=\"col-12 \">\n                    <label for=\"Maintenance\">Description</label>\n                    <textarea class=\"form-control\" type=\"text\" formControlName=\"description\" placeholder=\"Part Description\" [ngClass]=\"{ 'is-invalid': submitted && f.description.errors }\"></textarea>\n\n                    <div *ngIf=\" submitted && f.description.errors\" class=\"invalid-feedback\">\n                        <div *ngIf=\"f.description.errors.required\">Description is required</div>\n                    </div>\n\n                </div>&nbsp;\n                <!-- <div class=\"col-12\">\n          <button class=\"btn btn-outline-primary\" type=\"button\" shape=\"semi-round\" (click)=\"addItem()\" id=\"addItemButton\">Add\n            Parts</button><br>\n        </div>&nbsp; -->\n\n\n                <div class=\"container-fluid\">\n                    <div formArrayName=\"PartDetails\">\n                        <div *ngFor=\"let item of PartDetails.controls; let i=index\" [formGroupName]=\"i\">\n                            <div class=\"row\">\n                                <div class=\"col\">\n                                    <div class=\"form-group input-group-sm\">\n                                        <label for=\"Maintenance\">Material Name</label>\n                                        <input list=\"browser\" type=\"text\" class=\"form-control\" id=\"Maintenance\" (change)=\"selectMaterialPart(i)\" formControlName=\"PartDetail\" autocomplete=\"OFF\" placeholder=\"Material Name\">\n\n                                        <datalist id=\"browser\">\n                      <option *ngFor=\"let selectStage2 of selectedStageNew;let i = index\">{{selectStage2.partName}}</option>\n                    </datalist>\n\n                                    </div>\n                                </div>\n                                <div class=\"col\">\n                                    <div class=\"form-group input-group-sm\">\n                                        <label for=\"Maintenance\">HSN Code</label>\n                                        <input type=\"text\" class=\"form-control\" id=\"Maintenance\" formControlName=\"partNumber\" [(ngModel)]=\"partName\" placeholder=\"HSN Code\">\n                                    </div>\n                                </div>\n                                <div class=\"col\">\n                                    <div class=\"form-group input-group-sm\">\n                                        <label for=\"Date\"> Unit Price</label>\n                                        <input type=\"text\" class=\"form-control\" value='{{amount}}' (blur)='changeAmt($event.target.value)' formControlName=\"unitPrice\" placeholder=\"Unit Price\">\n                                        <!-- VIPIN CODE FOR CURRENCY -->\n                                    </div>\n                                </div>\n                                <div class=\"col\">\n                                    <div class=\"form-group input-group-sm\">\n                                        <label for=\"Delivary\">Unit of Measurment</label>\n                                        <!-- <input type=\"text\" class=\"form-control\" formControlName=\"unit\" \n                            placeholder=\"Unit\"> -->\n                                        <select id=\"select-type-basic\" class=\"form-control\" formControlName=\"unit\" size=\"small\">\n                      <option *ngFor=\"let status_item of status_values\">\n                        {{status_item}}\n                      </option>\n                    </select>\n                                    </div>\n                                </div>\n                                <div class=\"col\">\n                                    <div class=\"form-group input-group-sm\">\n                                        <label for=\"Delivary\">Qty on Hand</label>\n                                        <input type=\"number\" class=\"form-control\" formControlName=\"available\" placeholder=\"Available\">\n\n                                    </div>\n                                </div>\n                                <!-- <div class=\"col\">\n                  <div class=\"form-group input-group-sm\">\n                    <label for=\"Delivary\">Min Re-order Qty </label>\n                    <input type=\"number\" class=\"form-control\" formControlName=\"reorder\" placeholder=\"Min Re-order Quantity\">\n                    <i class=\"far fa-trash-alt\" (click)=\"removeItem(item)\"></i>\n                  </div>\n                </div> -->\n                            </div>&nbsp;\n                        </div>\n\n                    </div>\n                </div>\n\n\n\n            </div>\n            <div class=\"row foot\">\n                <div class=\"col-12\" style=\"margin-top: 3px;\">\n                    <button type=\"submit\" class=\"btn btn-outline-primary \" shape=\"semi-round\" style=\"float: right;\">Save</button>\n                </div>\n\n            </div>\n        </div>\n    </form>\n\n</nb-card-body>\n<!-- <ngx-details-form></ngx-details-form> -->"

/***/ }),

/***/ "./src/app/pages/ui-features/inventory/inventory-main/inventory-parts/partdetails/partdetails.component.scss":
/*!*******************************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/inventory/inventory-main/inventory-parts/partdetails/partdetails.component.scss ***!
  \*******************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "nb-card-body {\n  padding: 0rem; }\n\nnb-card-body, form {\n  background-color: #fff; }\n\nh4 {\n  background-color: #fff; }\n\nh1, h2, h3, h4, h5, h6 {\n  margin-top: 0;\n  margin-bottom: 0rem;\n  padding: 10px; }\n\n.far {\n  font-size: 1.5em;\n  color: #212529;\n  margin-top: 10px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3ByYWthc2gvVmlwaW4vaXJvbmJpcmQvMjBGRUIyMDIwL2lyb25iaXJkLWNsaWVudHMvc3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9pbnZlbnRvcnkvaW52ZW50b3J5LW1haW4vaW52ZW50b3J5LXBhcnRzL3BhcnRkZXRhaWxzL3BhcnRkZXRhaWxzLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFDO0VBQ0QsYUFBYSxFQUFBOztBQUVaO0VBQ0csc0JBQXFCLEVBQUE7O0FBR3hCO0VBQ0Usc0JBQXFCLEVBQUE7O0FBR3ZCO0VBQ0MsYUFBYTtFQUNiLG1CQUFtQjtFQUNuQixhQUFhLEVBQUE7O0FBSWY7RUFDRSxnQkFBZ0I7RUFDaEIsY0FBYztFQUNkLGdCQUFnQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvdWktZmVhdHVyZXMvaW52ZW50b3J5L2ludmVudG9yeS1tYWluL2ludmVudG9yeS1wYXJ0cy9wYXJ0ZGV0YWlscy9wYXJ0ZGV0YWlscy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiBuYi1jYXJkLWJvZHl7XG5wYWRkaW5nOiAwcmVtO1xuIH1cbiBuYi1jYXJkLWJvZHksIGZvcm17XG4gICAgYmFja2dyb3VuZC1jb2xvcjojZmZmO1xuIH1cblxuIGg0e1xuICAgYmFja2dyb3VuZC1jb2xvcjojZmZmO1xuICAgLy8gY29sb3I6IHdoaXRlO1xuIH1cbiBoMSwgaDIsIGgzLCBoNCwgaDUsIGg2IHtcbiAgbWFyZ2luLXRvcDogMDtcbiAgbWFyZ2luLWJvdHRvbTogMHJlbTtcbiAgcGFkZGluZzogMTBweDtcbn1cblxuXG4uZmFyIHtcbiAgZm9udC1zaXplOiAxLjVlbTtcbiAgY29sb3I6ICMyMTI1Mjk7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG59Il19 */"

/***/ }),

/***/ "./src/app/pages/ui-features/inventory/inventory-main/inventory-parts/partdetails/partdetails.component.ts":
/*!*****************************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/inventory/inventory-main/inventory-parts/partdetails/partdetails.component.ts ***!
  \*****************************************************************************************************************/
/*! exports provided: PartDetail, PartdetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PartDetail", function() { return PartDetail; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PartdetailsComponent", function() { return PartdetailsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _confirmation_dialog_confirmation_dialog_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../confirmation-dialog/confirmation-dialog.service */ "./src/app/confirmation-dialog/confirmation-dialog.service.ts");
/* harmony import */ var _services_currency_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../../services/currency.service */ "./src/app/services/currency.service.ts");
/* harmony import */ var _services_Inventory_inventory_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../../../services/Inventory/inventory.service */ "./src/app/services/Inventory/inventory.service.ts");
/* harmony import */ var _services_suppliers_supplier_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../../../services/suppliers/supplier.service */ "./src/app/services/suppliers/supplier.service.ts");
/* harmony import */ var _services_admin_isertParts_insert_parts_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../../../services/admin/isertParts/insert-parts.service */ "./src/app/services/admin/isertParts/insert-parts.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};







 // VIPIN CODE FOR CURRENCY
var PartDetail = /** @class */ (function () {
    function PartDetail() {
        this.PartDetail = '';
        this.partNumber = '';
        this.unitPrice = '';
        this.unit = '';
        this.available = '';
        this.reorder = '';
    }
    return PartDetail;
}());

var PartdetailsComponent = /** @class */ (function () {
    function PartdetailsComponent(partServiceServ, supplierServiceServ, confirmationDialogService, router, fb, partDetailsServ, currency // VIPIN CODE FOR CURRENCY
    ) {
        this.partServiceServ = partServiceServ;
        this.supplierServiceServ = supplierServiceServ;
        this.confirmationDialogService = confirmationDialogService;
        this.router = router;
        this.fb = fb;
        this.partDetailsServ = partDetailsServ;
        this.currency = currency;
        this.regExpr = /[₹,]/g;
        this.selectedStageNew = [];
        this.submitted = false;
        this.suppliers = {};
        this.inventory = {};
        this.getSupplierNameList = [];
        this.status_values = ["NO", "MTR", "LTR", "SET"];
        this.editShow = false;
        this.partCode = '';
        this.partName = '';
        this.designations = ['First Stage Materials', 'Second Stage Materials', 'Third Stage Materials', 'Commisioning Materials'];
        this.formFields = ['supplierName', 'description', 'selectedStage'];
        this.creatNewInventory = false;
        this.inventoryChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.createParts();
        this.addItem();
        this.getSupplierList();
    }
    /**
       * Search By Material Name
       */
    // vipin code
    PartdetailsComponent.prototype.onSelect = function (getSupplierName) {
        var _this = this;
        this.getSupplierNameList.forEach(function (data) {
            if (getSupplierName === data.CompanyName) {
                _this.selectedStageNew = [];
                data.supplierItems.forEach(function (ele) {
                    if (_this.selectedStageName != undefined && _this.selectedStageName != null) {
                        if (ele.stages === _this.selectedStageName) {
                            return _this.selectedStageNew.push({ 'HSN_Code': ele.hSNumber, 'partName': ele.itemName });
                            ;
                        }
                        else {
                            // console.log('hi......');
                        }
                    }
                    else {
                        _this.selectedStageNew.push({ 'HSN_Code': ele.hSNumber, 'partName': ele.itemName });
                    }
                });
                // this.myForm.get('SupplierName').setValue(data.address);
                _this.getSupplierList();
                return;
            }
        });
    };
    /**
     * Search By Material Name
     */
    // onSelect(getSupplierName) {
    //   this.getSupplierNameList.forEach(data => {
    //     if (getSupplierName === data.SupplierName) {
    //       this.myForm.get('SupplierName').setValue(data.address);
    //       this.getSupplierList();
    //       return;
    //     }
    //   })
    // }
    /**
     * Search By Supplier Name
     */
    PartdetailsComponent.prototype.getSupplierList = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.supplierServiceServ.getSupplier()];
                    case 1:
                        result = _a.sent();
                        this.getSupplierNameList = result;
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Reactive Form
     */
    PartdetailsComponent.prototype.createParts = function () {
        this.myForm = this.fb.group({
            selectedStage: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            supplierName: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            description: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            PartDetails: this.fb.array([]),
        });
    };
    Object.defineProperty(PartdetailsComponent.prototype, "PartDetails", {
        get: function () {
            return this.myForm.get('PartDetails');
        },
        enumerable: true,
        configurable: true
    });
    ;
    Object.defineProperty(PartdetailsComponent.prototype, "f", {
        get: function () { return this.myForm.controls; },
        enumerable: true,
        configurable: true
    });
    /**
       * Add And Remove for Parts in form
       */
    PartdetailsComponent.prototype.addItem = function () {
        this.PartDetails.push(this.fb.group(new PartDetail()));
    };
    PartdetailsComponent.prototype.removeItem = function (item) {
        var i = this.PartDetails.controls.indexOf(item);
        if (i != -1) {
            this.PartDetails.controls.splice(i, 1);
            var items = this.myForm.get('PartDetails');
            var data = { PartDetails: items };
        }
    };
    /**
       *  Create  Customer
       */
    PartdetailsComponent.prototype.createNewinventory = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            var resultAleart, value, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.submitted = true;
                        if (this.myForm.invalid) {
                            return [2 /*return*/];
                        }
                        return [4 /*yield*/, this.confirmationDialogService.confirm('Alert Message', 'Are you sure you want to Create?')];
                    case 1:
                        resultAleart = _a.sent();
                        if (!resultAleart) return [3 /*break*/, 3];
                        value = data;
                        this.id = value.id;
                        return [4 /*yield*/, this.partServiceServ.createInventory(value)];
                    case 2:
                        result = _a.sent();
                        this.inventoryChange.emit(result);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
       Edit  Inventory
     **/
    PartdetailsComponent.prototype.editInventory = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            var resultAleart, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.confirmationDialogService.confirm('Alert Message', 'Are you sure you want to Update?')];
                    case 1:
                        resultAleart = _a.sent();
                        if (!resultAleart) return [3 /*break*/, 6];
                        _a.label = 2;
                    case 2:
                        _a.trys.push([2, 4, , 5]);
                        return [4 /*yield*/, this.partServiceServ.UpdateInventoryDetails(data, this.id)];
                    case 3:
                        _a.sent();
                        return [3 /*break*/, 5];
                    case 4:
                        error_1 = _a.sent();
                        return [3 /*break*/, 5];
                    case 5:
                        data.confirm.resolve();
                        return [3 /*break*/, 7];
                    case 6:
                        data.confirm.reject();
                        _a.label = 7;
                    case 7: return [2 /*return*/];
                }
            });
        });
    };
    PartdetailsComponent.prototype.createAndEdit = function (formData) {
        return __awaiter(this, void 0, void 0, function () {
            var value;
            return __generator(this, function (_a) {
                value = formData.value;
                if (this.creatNewInventory) {
                    this.createNewinventory(value);
                    return [2 /*return*/];
                }
                this.editInventory(value);
                return [2 /*return*/];
            });
        });
    };
    PartdetailsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.partServiceServ.userInventory
            .subscribe(function (data) {
            if (data.newFormShow === true) {
                _this.creatNewInventory = true;
                return;
            }
            _this.id = data.id;
            _this.showFormData(data);
        }, function (error) {
            console.log(error);
        });
    };
    /**
      * Set Data Base Values in form Field
      */
    PartdetailsComponent.prototype.showFormData = function (data) {
        var _this = this;
        this.PartDetails.controls = [];
        this.formFields.map(function (fieldName) {
            _this.myForm.get(fieldName).setValue(data[fieldName]);
        });
        data.PartDetails.forEach(function (obj) {
            _this.PartDetails.push(_this.fb.group(obj));
        });
    };
    /**
       * Set Material stage Values in form Field
       */
    PartdetailsComponent.prototype.selectMaterial = function (stageName) {
        var _this = this;
        this.selectedStageName = stageName; // vipin code
        this.selectedStageNew.splice(0, this.selectedStageNew.length + 1);
        this.partDetailsServ.getPartDetails()
            .then(function (data) {
            data.forEach(function (element, index) {
                if (element.stages == stageName) {
                    _this.selectedStageNew.push(element);
                }
            });
        })
            .catch(function (error) {
            console.log(error);
        });
    };
    PartdetailsComponent.prototype.selectMaterialPart = function (index) {
        var HSN_Code = this.selectedStageNew[index].HSN_Code;
        this.partName = HSN_Code;
        // console.log(this.partName)
    };
    PartdetailsComponent.prototype.changeAmt = function (amt) {
        if (amt[0] === '₹') {
            amt = amt.replace(this.regExpr, "");
        }
        this.amount = this.currency.convertToInrFormat(amt); // VIPIN CODE FOR CURRENCY
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], PartdetailsComponent.prototype, "inventoryChange", void 0);
    PartdetailsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-partdetails',
            template: __webpack_require__(/*! ./partdetails.component.html */ "./src/app/pages/ui-features/inventory/inventory-main/inventory-parts/partdetails/partdetails.component.html"),
            styles: [__webpack_require__(/*! ./partdetails.component.scss */ "./src/app/pages/ui-features/inventory/inventory-main/inventory-parts/partdetails/partdetails.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_Inventory_inventory_service__WEBPACK_IMPORTED_MODULE_5__["InventoryService"],
            _services_suppliers_supplier_service__WEBPACK_IMPORTED_MODULE_6__["SupplierService"],
            _confirmation_dialog_confirmation_dialog_service__WEBPACK_IMPORTED_MODULE_3__["ConfirmationDialogService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _services_admin_isertParts_insert_parts_service__WEBPACK_IMPORTED_MODULE_7__["InsertPartsService"],
            _services_currency_service__WEBPACK_IMPORTED_MODULE_4__["CurrencyService"] // VIPIN CODE FOR CURRENCY
        ])
    ], PartdetailsComponent);
    return PartdetailsComponent;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/inventory/inventory-main/total-inventory/total-inventory.component.html":
/*!***********************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/inventory/inventory-main/total-inventory/total-inventory.component.html ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\n    <div class=\"row\">\n        <div class=\"col-12\">\n            <nb-card>\n                <nb-card-header>\n                    Parts List\n                    <button *ngIf='moduleCreate' class=\"btn btn-outline-primary \" shape=\"semi-round\" (click)=\"componentInsideModal.open() || showFun('create')\" mdbWavesEffect style=\"float:right\">Part Details</button>\n                </nb-card-header>\n                <nb-card-body>\n                    <div class=\"container\">\n                        <div class=\"form-group input-group-sm\">\n                            <label for=\"sel1\">Select Stage Material:</label>\n                            <select id=\"select-type-basic\" class=\"form-control\" [(ngModel)]=\"status\" size=\"small\" (change)=\"selectStage(status)\">\n                                <option *ngFor=\"let status_item of status_values\">\n                                {{status_item}}\n                                </option>\n                            </select>\n                        </div>\n                        <ng2-smart-table [settings]=\"settings\" [source]=\"source\" (editConfirm)=\"onSaveConfirm($event)\" (deleteConfirm)=\"onDeleteConfirm($event)\" (userRowSelect)=\"onUserRowSelect($event)\">\n                        </ng2-smart-table>\n                    </div>\n                </nb-card-body>\n            </nb-card>\n\n        </div>\n        <!-- <div class=\"col-xl-7\" *ngIf=\"tableShow\">\n      <nb-card style=\"\n          background-color:#E1F5FE;\">\n        <ngx-inventory-parts style=\"background-color:#E1F5FE;\"></ngx-inventory-parts>\n      </nb-card>\n    </div> -->\n        <!-- <div class=\"col-xl-7\" *ngIf=\"creatShow\"> -->\n        <!-- Nav tabs -->\n        <!-- <nb-card>\n        <ngx-partdetails (inventoryChange)=\"createData($event)\" style=\"background-color:#E1F5FE;\"></ngx-partdetails>\n      </nb-card> -->\n        <!-- Nav tabs -->\n        <!-- </div> -->\n    </div>\n</div>\n\n<modal class=\"modalss\" #componentInsideModal #ignoreClickOutside [closeOnOutsideClick]=\"false\">\n    <ng-template #modalHeader>\n        <!-- <h2>Entering the new Parts</h2> -->\n    </ng-template>\n    <ng-template #modalBody>\n        <!-- <h2>Add terms and condition</h2>\n          <hr/> -->\n        <ngx-partdetails (inventoryChange)=\"createData($event)\" style=\"background-color:#E1F5FE;\"></ngx-partdetails>\n        <!-- <button class=\"btn\" (click)=\"selectedOptions()\">closed</button> -->\n    </ng-template>\n    <ng-template #modalFooter></ng-template>\n</modal>"

/***/ }),

/***/ "./src/app/pages/ui-features/inventory/inventory-main/total-inventory/total-inventory.component.scss":
/*!***********************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/inventory/inventory-main/total-inventory/total-inventory.component.scss ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "nb-card-body {\n  background-color: #E1F5FE;\n  color: white; }\n\ncol-xl-6, nb-card-header {\n  background: #1D8ECE;\n  color: white; }\n\nlabel {\n  color: black; }\n\n@media (min-width: 576px) {\n  /deep/ modal.modalss .modal-dialog {\n    max-width: 50% !important;\n    margin: 1.75rem auto;\n    background-color: #fff; } }\n\n:host /deep/ table tbody tr:nth-child(even) {\n  background: #e9eaea !important; }\n\n:host /deep/ table tbody tr:nth-child(odd) {\n  background: white !important; }\n\nng-template .modal-footer {\n  background-color: #87898e;\n  display: none !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3ByYWthc2gvVmlwaW4vaXJvbmJpcmQvMjBGRUIyMDIwL2lyb25iaXJkLWNsaWVudHMvc3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9pbnZlbnRvcnkvaW52ZW50b3J5LW1haW4vdG90YWwtaW52ZW50b3J5L3RvdGFsLWludmVudG9yeS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHlCQUF3QjtFQUN4QixZQUFXLEVBQUE7O0FBRWI7RUFDRSxtQkFBbUI7RUFDbkIsWUFBWSxFQUFBOztBQUVkO0VBQ0UsWUFDRixFQUFBOztBQUNBO0VBRUY7SUFDSSx5QkFBd0I7SUFDeEIsb0JBQW9CO0lBQ3BCLHNCQUFzQixFQUFBLEVBQ3pCOztBQUVEO0VBQ0UsOEJBQTRCLEVBQUE7O0FBRTVCO0VBQ0EsNEJBQTRCLEVBQUE7O0FBRTVCO0VBQ0UseUJBQXlCO0VBQ3pCLHdCQUF1QixFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvdWktZmVhdHVyZXMvaW52ZW50b3J5L2ludmVudG9yeS1tYWluL3RvdGFsLWludmVudG9yeS90b3RhbC1pbnZlbnRvcnkuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJuYi1jYXJkLWJvZHl7XG4gICAgYmFja2dyb3VuZC1jb2xvcjojRTFGNUZFO1xuICAgIGNvbG9yOndoaXRlO1xuICB9XG4gIGNvbC14bC02LCBuYi1jYXJkLWhlYWRlcntcbiAgICBiYWNrZ3JvdW5kOiAjMUQ4RUNFO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgfVxuICBsYWJlbHtcbiAgICBjb2xvcjogYmxhY2tcbiAgfVxuICBAbWVkaWEgKG1pbi13aWR0aDogNTc2cHgpXG57XG4vZGVlcC8gbW9kYWwubW9kYWxzcyAubW9kYWwtZGlhbG9nIHtcbiAgICBtYXgtd2lkdGg6IDUwJSFpbXBvcnRhbnQ7XG4gICAgbWFyZ2luOiAxLjc1cmVtIGF1dG87XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbn1cbn1cbjpob3N0IC9kZWVwLyB0YWJsZSB0Ym9keSB0cjpudGgtY2hpbGQoZXZlbikge1xuICBiYWNrZ3JvdW5kOiNlOWVhZWEhaW1wb3J0YW50O1xuICB9XG4gIDpob3N0IC9kZWVwLyB0YWJsZSB0Ym9keSB0cjpudGgtY2hpbGQob2RkKSB7XG4gIGJhY2tncm91bmQ6ICB3aGl0ZSFpbXBvcnRhbnQ7XG4gIH1cbiAgbmctdGVtcGxhdGUgLm1vZGFsLWZvb3RlciB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzg3ODk4ZTtcbiAgICBkaXNwbGF5Om5vbmUgIWltcG9ydGFudDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/ui-features/inventory/inventory-main/total-inventory/total-inventory.component.ts":
/*!*********************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/inventory/inventory-main/total-inventory/total-inventory.component.ts ***!
  \*********************************************************************************************************/
/*! exports provided: TotalInventoryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TotalInventoryComponent", function() { return TotalInventoryComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var _confirmation_dialog_confirmation_dialog_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../confirmation-dialog/confirmation-dialog.service */ "./src/app/confirmation-dialog/confirmation-dialog.service.ts");
/* harmony import */ var _services_currency_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../services/currency.service */ "./src/app/services/currency.service.ts");
/* harmony import */ var _services_Inventory_inventory_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../services/Inventory/inventory.service */ "./src/app/services/Inventory/inventory.service.ts");
/* harmony import */ var _services_token_token_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../../services/token/token.service */ "./src/app/services/token/token.service.ts");
/* harmony import */ var _node_modules_angular_custom_modal__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../../../../node_modules/angular-custom-modal */ "./node_modules/angular-custom-modal/angular-custom-modal.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



 // VIPIN CODE FOR CURRENCY



var TotalInventoryComponent = /** @class */ (function () {
    function TotalInventoryComponent(userServ, confirmationDialogService, InventoryServ, currency) {
        var _this = this;
        this.userServ = userServ;
        this.confirmationDialogService = confirmationDialogService;
        this.InventoryServ = InventoryServ;
        this.currency = currency;
        this.creatShow = false;
        this.tableShow = false;
        this.status = 'All';
        this.listOfAllInventory = [];
        this.status_values = ['All', 'First Stage Materials', 'Second Stage Materials', 'Third Stage Materials', 'Commisioning Materials'];
        this.settings = {
            add: {
                addButtonContent: '<i class="nb-plus"></i>',
                createButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
            },
            edit: {
                editButtonContent: '<i class="nb-edit"></i>',
                saveButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
                confirmSave: true
            },
            delete: {
                deleteButtonContent: '<i class="nb-trash"></i>',
                confirmDelete: true,
            },
            view: {
                viewButtonContent: '<i  class="nb-edit"></i>'
            },
            actions: {
                edit: true,
                add: false,
                delete: true,
                position: 'right'
            },
            columns: {
                partNumber: {
                    title: 'HSN Code',
                    type: 'number',
                    editable: false,
                },
                PartDetail: {
                    title: 'Material Name',
                    type: 'string',
                    editable: false,
                },
                unitPrice: {
                    title: 'Unit Price Rs',
                    type: 'string',
                    valuePrepareFunction: function (value) { return value === 'Total' ? value : _this.currency.convertToInrFormat(value); },
                },
                available: {
                    title: 'Qty On Hand',
                    type: 'string',
                },
            },
        };
        this.source = new ng2_smart_table__WEBPACK_IMPORTED_MODULE_1__["LocalDataSource"]();
        this.inventory = {};
        this.id = '';
        this.listOfInventory = [];
        this.inventoryPartDetails = [];
        this.moduleCreate = true;
        this.moduleEdit = true;
        this.moduleView = true;
    }
    TotalInventoryComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.userServ.userAccess !== undefined) {
            this.userServ.userAccess
                .subscribe(function (data) {
                data.forEach(function (ele) {
                    if (ele.name === 'Inventory') {
                        ele.subMenu.forEach(function (element) {
                            if (element.name === 'Inventory Details') {
                                _this.moduleCreate = element.create;
                            }
                        });
                    }
                });
            }, function (error) {
                console.log(error);
            });
        }
        this.selectStage('All');
    };
    /**
     * Create New Inventor Function
     */
    TotalInventoryComponent.prototype.showFun = function (data) {
        this.creatShow = true;
        this.tableShow = false;
        this.InventoryServ.userInventory.next({ newFormShow: true });
    };
    /**
    * Stage select Function
    */
    TotalInventoryComponent.prototype.selectStage = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, result_1, error_1;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        _a = this;
                        return [4 /*yield*/, this.InventoryServ.getInventory()];
                    case 1:
                        _a.listOfInventory = _b.sent();
                        this.listOfInventory.reverse();
                        this.listOfAllInventory.splice(0, this.listOfAllInventory.length + 1);
                        this.listOfInventory.forEach(function (payload) {
                            if (data == payload.selectedStage) {
                                payload.PartDetails.forEach(function (data) {
                                    Object.assign(data, { 'id': payload.id });
                                    _this.listOfAllInventory.push(data);
                                });
                            }
                        });
                        if (data == 'All') {
                            result_1 = [];
                            this.listOfInventory.map(function (data) {
                                data.PartDetails.forEach(function (value) {
                                    Object.assign(value, { 'id': data.id });
                                    result_1.push(value);
                                });
                            });
                            return [2 /*return*/, this.source.load(result_1)];
                        }
                        this.source.load(this.listOfAllInventory);
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _b.sent();
                        console.error(error_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * User Selected Row
     */
    TotalInventoryComponent.prototype.onUserRowSelect = function (event) {
        event.data.creatShow = false;
        var getIndex = event.data.index;
        var getObject = this.listOfInventory[getIndex];
        this.InventoryServ.userInventory.next(getObject);
        if (event) {
            this.creatShow = false;
            return this.tableShow = true;
        }
        this.creatShow = true;
        this.tableShow = false;
    };
    /**
     *  Delete Row Data
     */
    TotalInventoryComponent.prototype.onDeleteConfirm = function (event) {
        return __awaiter(this, void 0, void 0, function () {
            var resultAleart;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.confirmationDialogService.confirm('Alert Message', 'Are you sure you want to Delete?')];
                    case 1:
                        resultAleart = _a.sent();
                        if (!resultAleart) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.InventoryServ.deleteInventoryByID(event.data.id)];
                    case 2:
                        _a.sent();
                        event.confirm.resolve();
                        _a.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
      *  Create New Inventory Show Function
      */
    TotalInventoryComponent.prototype.createData = function (data) {
        this.selectStage('All');
        // this.listOfInventory.push(data);
        // this.listOfInventory.reverse();
        // this.source.load(this.listOfInventory);
        this.componentInsideModal.close();
    };
    TotalInventoryComponent.prototype.onSaveConfirm = function (event) {
        return __awaiter(this, void 0, void 0, function () {
            var resultAleart;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.confirmationDialogService.confirm('Alert Message', 'Are you sure you want to Update?')];
                    case 1:
                        resultAleart = _a.sent();
                        if (!resultAleart) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.InventoryServ.UpdateInventoryDetails(event.newData, event.newData.id)];
                    case 2:
                        _a.sent();
                        event.confirm.resolve();
                        _a.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('componentInsideModal'),
        __metadata("design:type", _node_modules_angular_custom_modal__WEBPACK_IMPORTED_MODULE_6__["ModalComponent"])
    ], TotalInventoryComponent.prototype, "componentInsideModal", void 0);
    TotalInventoryComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-total-inventory',
            template: __webpack_require__(/*! ./total-inventory.component.html */ "./src/app/pages/ui-features/inventory/inventory-main/total-inventory/total-inventory.component.html"),
            styles: [__webpack_require__(/*! ./total-inventory.component.scss */ "./src/app/pages/ui-features/inventory/inventory-main/total-inventory/total-inventory.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_token_token_service__WEBPACK_IMPORTED_MODULE_5__["TokenService"],
            _confirmation_dialog_confirmation_dialog_service__WEBPACK_IMPORTED_MODULE_2__["ConfirmationDialogService"],
            _services_Inventory_inventory_service__WEBPACK_IMPORTED_MODULE_4__["InventoryService"],
            _services_currency_service__WEBPACK_IMPORTED_MODULE_3__["CurrencyService"]])
    ], TotalInventoryComponent);
    return TotalInventoryComponent;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/inventory/inventory-routing.module.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/ui-features/inventory/inventory-routing.module.ts ***!
  \*************************************************************************/
/*! exports provided: InventoryRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InventoryRoutingModule", function() { return InventoryRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _inventory_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./inventory.component */ "./src/app/pages/ui-features/inventory/inventory.component.ts");
/* harmony import */ var _inventory_dashboard_inventory_dashboard_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./inventory-dashboard/inventory-dashboard.component */ "./src/app/pages/ui-features/inventory/inventory-dashboard/inventory-dashboard.component.ts");
/* harmony import */ var _inventory_main_total_inventory_total_inventory_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./inventory-main/total-inventory/total-inventory.component */ "./src/app/pages/ui-features/inventory/inventory-main/total-inventory/total-inventory.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var routes = [{
        path: '',
        component: _inventory_component__WEBPACK_IMPORTED_MODULE_2__["InventoryComponent"],
        children: [{
                path: 'Inventory_Dashboard',
                component: _inventory_dashboard_inventory_dashboard_component__WEBPACK_IMPORTED_MODULE_3__["InventoryDashboardComponent"],
            },
            {
                path: 'Main_Inventory',
                component: _inventory_main_total_inventory_total_inventory_component__WEBPACK_IMPORTED_MODULE_4__["TotalInventoryComponent"],
            }
        ]
    }];
var InventoryRoutingModule = /** @class */ (function () {
    function InventoryRoutingModule() {
    }
    InventoryRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], InventoryRoutingModule);
    return InventoryRoutingModule;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/inventory/inventory.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/pages/ui-features/inventory/inventory.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./src/app/pages/ui-features/inventory/inventory.component.scss":
/*!**********************************************************************!*\
  !*** ./src/app/pages/ui-features/inventory/inventory.component.scss ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VpLWZlYXR1cmVzL2ludmVudG9yeS9pbnZlbnRvcnkuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/pages/ui-features/inventory/inventory.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/pages/ui-features/inventory/inventory.component.ts ***!
  \********************************************************************/
/*! exports provided: InventoryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InventoryComponent", function() { return InventoryComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var InventoryComponent = /** @class */ (function () {
    function InventoryComponent() {
    }
    InventoryComponent.prototype.ngOnInit = function () {
    };
    InventoryComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-inventory',
            template: __webpack_require__(/*! ./inventory.component.html */ "./src/app/pages/ui-features/inventory/inventory.component.html"),
            styles: [__webpack_require__(/*! ./inventory.component.scss */ "./src/app/pages/ui-features/inventory/inventory.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], InventoryComponent);
    return InventoryComponent;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/inventory/inventory.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/pages/ui-features/inventory/inventory.module.ts ***!
  \*****************************************************************/
/*! exports provided: InventoryModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InventoryModule", function() { return InventoryModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _nebular_theme__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @nebular/theme */ "./node_modules/@nebular/theme/index.js");
/* harmony import */ var ngx_echarts__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-echarts */ "./node_modules/ngx-echarts/ngx-echarts.es5.js");
/* harmony import */ var _swimlane_ngx_charts__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @swimlane/ngx-charts */ "./node_modules/@swimlane/ngx-charts/release/index.js");
/* harmony import */ var _swimlane_ngx_charts__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_swimlane_ngx_charts__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _theme_theme_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../@theme/theme.module */ "./src/app/@theme/theme.module.ts");
/* harmony import */ var _asymmetrik_ngx_leaflet__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @asymmetrik/ngx-leaflet */ "./node_modules/@asymmetrik/ngx-leaflet/dist/index.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var _admin_admin_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../admin/admin.module */ "./src/app/pages/ui-features/admin/admin.module.ts");
/* harmony import */ var _inventory_routing_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./inventory-routing.module */ "./src/app/pages/ui-features/inventory/inventory-routing.module.ts");
/* harmony import */ var _inventory_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./inventory.component */ "./src/app/pages/ui-features/inventory/inventory.component.ts");
/* harmony import */ var _inventory_dashboard_inventory_dashboard_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./inventory-dashboard/inventory-dashboard.component */ "./src/app/pages/ui-features/inventory/inventory-dashboard/inventory-dashboard.component.ts");
/* harmony import */ var _inventory_main_total_inventory_total_inventory_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./inventory-main/total-inventory/total-inventory.component */ "./src/app/pages/ui-features/inventory/inventory-main/total-inventory/total-inventory.component.ts");
/* harmony import */ var _inventory_main_inventory_parts_inventory_parts_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./inventory-main/inventory-parts/inventory-parts.component */ "./src/app/pages/ui-features/inventory/inventory-main/inventory-parts/inventory-parts.component.ts");
/* harmony import */ var _inventory_main_inventory_parts_partdetails_partdetails_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./inventory-main/inventory-parts/partdetails/partdetails.component */ "./src/app/pages/ui-features/inventory/inventory-main/inventory-parts/partdetails/partdetails.component.ts");
/* harmony import */ var _node_modules_angular_custom_modal__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../../../../../node_modules/angular-custom-modal */ "./node_modules/angular-custom-modal/angular-custom-modal.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





// import { ChartModule } from 'angular2-chartjs';













var InventoryModule = /** @class */ (function () {
    function InventoryModule() {
    }
    InventoryModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _inventory_routing_module__WEBPACK_IMPORTED_MODULE_10__["InventoryRoutingModule"],
                _nebular_theme__WEBPACK_IMPORTED_MODULE_2__["NbStepperModule"],
                ngx_echarts__WEBPACK_IMPORTED_MODULE_3__["NgxEchartsModule"],
                _swimlane_ngx_charts__WEBPACK_IMPORTED_MODULE_4__["NgxChartsModule"],
                _asymmetrik_ngx_leaflet__WEBPACK_IMPORTED_MODULE_6__["LeafletModule"],
                _nebular_theme__WEBPACK_IMPORTED_MODULE_2__["NbListModule"],
                _nebular_theme__WEBPACK_IMPORTED_MODULE_2__["NbProgressBarModule"],
                // ChartModule,
                _theme_theme_module__WEBPACK_IMPORTED_MODULE_5__["ThemeModule"],
                ng2_smart_table__WEBPACK_IMPORTED_MODULE_8__["Ng2SmartTableModule"],
                _admin_admin_module__WEBPACK_IMPORTED_MODULE_9__["AdminModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["ReactiveFormsModule"],
                _node_modules_angular_custom_modal__WEBPACK_IMPORTED_MODULE_16__["ModalModule"]
            ],
            declarations: [
                _inventory_component__WEBPACK_IMPORTED_MODULE_11__["InventoryComponent"],
                _inventory_dashboard_inventory_dashboard_component__WEBPACK_IMPORTED_MODULE_12__["InventoryDashboardComponent"], _inventory_main_total_inventory_total_inventory_component__WEBPACK_IMPORTED_MODULE_13__["TotalInventoryComponent"], _inventory_main_inventory_parts_inventory_parts_component__WEBPACK_IMPORTED_MODULE_14__["InventoryPartsComponent"], _inventory_main_inventory_parts_partdetails_partdetails_component__WEBPACK_IMPORTED_MODULE_15__["PartdetailsComponent"]
            ],
            providers: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["DatePipe"]
            ]
        })
    ], InventoryModule);
    return InventoryModule;
}());



/***/ })

}]);
//# sourceMappingURL=default~app-pages-pages-module~inventory-inventory-module~ui-features-ui-features-module.js.map