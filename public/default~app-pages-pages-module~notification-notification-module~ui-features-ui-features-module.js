(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~app-pages-pages-module~notification-notification-module~ui-features-ui-features-module"],{

/***/ "./src/app/pages/ui-features/notification/notification-routing.module.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/ui-features/notification/notification-routing.module.ts ***!
  \*******************************************************************************/
/*! exports provided: NotificationRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationRoutingModule", function() { return NotificationRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _notification_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./notification.component */ "./src/app/pages/ui-features/notification/notification.component.ts");
/* harmony import */ var _notifications_notifications_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./notifications/notifications.component */ "./src/app/pages/ui-features/notification/notifications/notifications.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var routes = [{
        path: '',
        component: _notification_component__WEBPACK_IMPORTED_MODULE_2__["NotificationComponent"],
        children: [
            {
                path: 'Notifications',
                component: _notifications_notifications_component__WEBPACK_IMPORTED_MODULE_3__["NotificationsComponent"],
            }
        ]
    }];
var NotificationRoutingModule = /** @class */ (function () {
    function NotificationRoutingModule() {
    }
    NotificationRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], NotificationRoutingModule);
    return NotificationRoutingModule;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/notification/notification.component.html":
/*!****************************************************************************!*\
  !*** ./src/app/pages/ui-features/notification/notification.component.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/pages/ui-features/notification/notification.component.scss":
/*!****************************************************************************!*\
  !*** ./src/app/pages/ui-features/notification/notification.component.scss ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VpLWZlYXR1cmVzL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/pages/ui-features/notification/notification.component.ts":
/*!**************************************************************************!*\
  !*** ./src/app/pages/ui-features/notification/notification.component.ts ***!
  \**************************************************************************/
/*! exports provided: NotificationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationComponent", function() { return NotificationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var NotificationComponent = /** @class */ (function () {
    function NotificationComponent() {
    }
    NotificationComponent.prototype.ngOnInit = function () {
    };
    NotificationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-notification',
            template: __webpack_require__(/*! ./notification.component.html */ "./src/app/pages/ui-features/notification/notification.component.html"),
            styles: [__webpack_require__(/*! ./notification.component.scss */ "./src/app/pages/ui-features/notification/notification.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], NotificationComponent);
    return NotificationComponent;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/notification/notification.module.ts":
/*!***********************************************************************!*\
  !*** ./src/app/pages/ui-features/notification/notification.module.ts ***!
  \***********************************************************************/
/*! exports provided: NotificationModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationModule", function() { return NotificationModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var _nebular_theme__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @nebular/theme */ "./node_modules/@nebular/theme/index.js");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var _notification_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./notification-routing.module */ "./src/app/pages/ui-features/notification/notification-routing.module.ts");
/* harmony import */ var _notification_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./notification.component */ "./src/app/pages/ui-features/notification/notification.component.ts");
/* harmony import */ var _notifications_notifications_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./notifications/notifications.component */ "./src/app/pages/ui-features/notification/notifications/notifications.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var NotificationModule = /** @class */ (function () {
    function NotificationModule() {
    }
    NotificationModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_notification_component__WEBPACK_IMPORTED_MODULE_6__["NotificationComponent"], _notifications_notifications_component__WEBPACK_IMPORTED_MODULE_7__["NotificationsComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _notification_routing_module__WEBPACK_IMPORTED_MODULE_5__["NotificationRoutingModule"],
                ng2_smart_table__WEBPACK_IMPORTED_MODULE_4__["Ng2SmartTableModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbModule"],
                _nebular_theme__WEBPACK_IMPORTED_MODULE_3__["NbCardModule"]
            ]
        })
    ], NotificationModule);
    return NotificationModule;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/notification/notifications/notifications.component.html":
/*!*******************************************************************************************!*\
  !*** ./src/app/pages/ui-features/notification/notifications/notifications.component.html ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n    <div class=\"col-xl-5\">\n        <nb-card>\n            <nb-card-header>\n                Jobs List\n            </nb-card-header>\n            <nb-card-body>\n                <ng2-smart-table [settings]=\"settings\" [source]=\"source\" (userRowSelect)=\"onUserRowSelect($event)\"></ng2-smart-table>\n            </nb-card-body>\n        </nb-card>\n    </div>\n    <div class=\"col-xl-7\" *ngIf='display'>\n        <nb-card>\n            <nb-card-header>\n                Client: {{clientName}}<br> JobNo: {{jobNo}}\n            </nb-card-header>\n            <nb-card-body>\n                Notifications\n                <ng2-smart-table [settings]=\"settings1\" [source]=\"source1\" (editConfirm)=\"onSaveConfirm($event)\"></ng2-smart-table>\n            </nb-card-body>\n        </nb-card>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/pages/ui-features/notification/notifications/notifications.component.scss":
/*!*******************************************************************************************!*\
  !*** ./src/app/pages/ui-features/notification/notifications/notifications.component.scss ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/deep/ ng2-smart-table .ng2-smart-actions {\n  padding: 0;\n  height: 0% !important; }\n\n/deep/ ng2-smart-table .ng2-smart-action.ng2-smart-action-custom-custom {\n  color: black !important;\n  float: left; }\n\n/deep/ ng2-smart-table .ng2-smart-title {\n  color: white !important; }\n\n/deep/ ng2-smart-table .ng2-smart-actions a.ng2-smart-action {\n  display: -ms-flexbox;\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-align: center;\n  -ms-flex-align: center;\n  -webkit-box-pack: center;\n  -ms-flex-pack: center;\n  justify-content: center;\n  height: 100%;\n  width: 60% !important;\n  font-size: 2rem !important;\n  color: #a4abb3; }\n\n/deep/ ng2-smart-table .ng2-smart-action-edit-edit {\n  color: black !important; }\n\nnb-card-body {\n  background-color: #E1F5FE;\n  color: white; }\n\ncol-xl-6,\nnb-card-header {\n  background: #1D8ECE;\n  color: white; }\n\n:host /deep/ tr.aborted td {\n  background-color: #f5805c !important;\n  color: black !important; }\n\n:host /deep/ tr.Mycolor td {\n  background-color: #81f177 !important;\n  color: black !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3ByYWthc2gvVmlwaW4vaXJvbmJpcmQvMjBGRUIyMDIwL2lyb25iaXJkLWNsaWVudHMvc3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9ucy9ub3RpZmljYXRpb25zLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksVUFBVTtFQUNWLHFCQUFxQixFQUFBOztBQUd6QjtFQUNJLHVCQUF1QjtFQUN2QixXQUFXLEVBQUE7O0FBR2Y7RUFDSSx1QkFDSixFQUFBOztBQUVBO0VBQ0ksb0JBQW9CO0VBQ3BCLG9CQUFhO0VBQWIsYUFBYTtFQUNiLHlCQUF5QjtFQUN6QixzQkFBc0I7RUFDdEIsd0JBQXdCO0VBQ3hCLHFCQUFxQjtFQUNyQix1QkFBdUI7RUFDdkIsWUFBWTtFQUNaLHFCQUFxQjtFQUNyQiwwQkFBMEI7RUFDMUIsY0FBYyxFQUFBOztBQUdsQjtFQUNJLHVCQUF1QixFQUFBOztBQUczQjtFQUNJLHlCQUF5QjtFQUN6QixZQUFZLEVBQUE7O0FBR2hCOztFQUVJLG1CQUFtQjtFQUNuQixZQUFZLEVBQUE7O0FBR2hCO0VBQ0ksb0NBQW9DO0VBQ3BDLHVCQUE4QixFQUFBOztBQUdsQztFQUNJLG9DQUErQztFQUMvQyx1QkFBOEIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VpLWZlYXR1cmVzL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb25zL25vdGlmaWNhdGlvbnMuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvZGVlcC8gbmcyLXNtYXJ0LXRhYmxlIC5uZzItc21hcnQtYWN0aW9ucyB7XG4gICAgcGFkZGluZzogMDtcbiAgICBoZWlnaHQ6IDAlICFpbXBvcnRhbnQ7XG59XG5cbi9kZWVwLyBuZzItc21hcnQtdGFibGUgLm5nMi1zbWFydC1hY3Rpb24ubmcyLXNtYXJ0LWFjdGlvbi1jdXN0b20tY3VzdG9tIHtcbiAgICBjb2xvcjogYmxhY2sgIWltcG9ydGFudDtcbiAgICBmbG9hdDogbGVmdDtcbn1cblxuL2RlZXAvIG5nMi1zbWFydC10YWJsZSAubmcyLXNtYXJ0LXRpdGxlIHtcbiAgICBjb2xvcjogd2hpdGUgIWltcG9ydGFudFxufVxuXG4vZGVlcC8gbmcyLXNtYXJ0LXRhYmxlIC5uZzItc21hcnQtYWN0aW9ucyBhLm5nMi1zbWFydC1hY3Rpb24ge1xuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgLXdlYmtpdC1ib3gtYWxpZ246IGNlbnRlcjtcbiAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xuICAgIC13ZWJraXQtYm94LXBhY2s6IGNlbnRlcjtcbiAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIHdpZHRoOiA2MCUgIWltcG9ydGFudDtcbiAgICBmb250LXNpemU6IDJyZW0gIWltcG9ydGFudDtcbiAgICBjb2xvcjogI2E0YWJiMztcbn1cblxuL2RlZXAvIG5nMi1zbWFydC10YWJsZSAubmcyLXNtYXJ0LWFjdGlvbi1lZGl0LWVkaXQge1xuICAgIGNvbG9yOiBibGFjayAhaW1wb3J0YW50O1xufVxuXG5uYi1jYXJkLWJvZHkge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNFMUY1RkU7XG4gICAgY29sb3I6IHdoaXRlO1xufVxuXG5jb2wteGwtNixcbm5iLWNhcmQtaGVhZGVyIHtcbiAgICBiYWNrZ3JvdW5kOiAjMUQ4RUNFO1xuICAgIGNvbG9yOiB3aGl0ZTtcbn1cblxuOmhvc3QgL2RlZXAvIHRyLmFib3J0ZWQgdGQge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNmNTgwNWMgIWltcG9ydGFudDtcbiAgICBjb2xvcjogcmdiKDAsIDAsIDApICFpbXBvcnRhbnQ7XG59XG5cbjpob3N0IC9kZWVwLyB0ci5NeWNvbG9yIHRkIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMTI5LCAyNDEsIDExOSkgIWltcG9ydGFudDtcbiAgICBjb2xvcjogcmdiKDAsIDAsIDApICFpbXBvcnRhbnQ7XG59Il19 */"

/***/ }),

/***/ "./src/app/pages/ui-features/notification/notifications/notifications.component.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/pages/ui-features/notification/notifications/notifications.component.ts ***!
  \*****************************************************************************************/
/*! exports provided: NotificationsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationsComponent", function() { return NotificationsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var _confirmation_dialog_confirmation_dialog_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../confirmation-dialog/confirmation-dialog.service */ "./src/app/confirmation-dialog/confirmation-dialog.service.ts");
/* harmony import */ var _services_notifications_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/notifications.service */ "./src/app/services/notifications.service.ts");
/* harmony import */ var _services_token_token_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/token/token.service */ "./src/app/services/token/token.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var NotificationsComponent = /** @class */ (function () {
    function NotificationsComponent(confirmationDialogService, token, noti) {
        this.confirmationDialogService = confirmationDialogService;
        this.token = token;
        this.noti = noti;
        this.settings = {
            add: {
                addButtonContent: '<i class="nb-plus"></i>',
                createButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
            },
            edit: {
                editButtonContent: '<i class="nb-edit"></i>',
                saveButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
                confirmSave: true
            },
            delete: {
                deleteButtonContent: '<i class="nb-trash"></i>',
                confirmDelete: true,
            },
            view: {
                viewButtonContent: '<i  class="nb-edit"></i>'
            },
            actions: {
                position: 'right',
                edit: false,
                add: false,
                delete: false
            },
            columns: {
                jobNo: {
                    title: 'JobNo',
                    type: 'string',
                    filter: false
                },
                clientName: {
                    title: 'Client',
                    type: 'string',
                    filter: false
                }
            },
        };
        this.settings1 = {
            add: {
                addButtonContent: '<i class="nb-plus"></i>',
                createButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
                confirmCreate: true,
            },
            edit: {
                editButtonContent: '<i class="nb-edit"></i>',
                saveButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
                confirmSave: true,
            },
            delete: {
                deleteButtonContent: '<i class="nb-trash"></i>',
                confirmDelete: true,
            },
            actions: {
                position: 'right',
                delete: false,
                add: false,
            },
            rowClassFunction: function (row) {
                if (row.data.status === 'Pending') {
                    return 'aborted';
                }
                else {
                    return 'Mycolor';
                }
            },
            columns: {
                stage: {
                    title: 'Stage',
                    type: 'string',
                    editable: false
                },
                description: {
                    title: 'Description',
                    type: 'string',
                    editable: false
                },
                status: {
                    title: 'Status',
                    type: 'html',
                    editor: {
                        type: 'list',
                        config: {
                            list: [
                                { value: 'Pending', title: 'Pending' },
                                { value: 'Completed', title: 'Completed' }
                            ],
                        },
                    }
                }
            },
        };
        this.source = new ng2_smart_table__WEBPACK_IMPORTED_MODULE_1__["LocalDataSource"]();
        this.source1 = new ng2_smart_table__WEBPACK_IMPORTED_MODULE_1__["LocalDataSource"]();
        this.display = false;
        this.TaskStatus = ['Pending', 'Completed'];
        this.list = [];
        this.list1 = [];
        this.statusArray = [];
    }
    NotificationsComponent.prototype.ngOnInit = function () {
        this.tokens = this.token.getDecodedToken();
        this.role = this.tokens.role;
        this.getNotifications();
    };
    NotificationsComponent.prototype.getNotifications = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.noti.getAllNotification()];
                    case 1:
                        result = _a.sent();
                        if (this.role === 'Admin') {
                            this.list = Array.from(new Set(result.map(function (s) { return s.jobNo; })))
                                .map(function (jobNo) {
                                return {
                                    jobNo: jobNo,
                                    clientName: result.find(function (s) { return s.jobNo === jobNo; }).clientName
                                };
                            });
                            return [2 /*return*/, this.source.load(this.list)];
                        }
                        else {
                            result.forEach(function (ele) {
                                var count = 0;
                                var notifications = [];
                                ele.notifications.forEach(function (ele1) {
                                    if (ele1.department === _this.role) {
                                        count++;
                                        notifications.push(ele1);
                                        ele.notifications = notifications;
                                    }
                                });
                                if (count > 0) {
                                    _this.list1.push(ele);
                                }
                            });
                            this.list = Array.from(new Set(this.list1.map(function (s) { return s.jobNo; })))
                                .map(function (jobNo) {
                                return {
                                    jobNo: jobNo,
                                    clientName: _this.list1.find(function (s) { return s.jobNo === jobNo; }).clientName
                                };
                            });
                            return [2 /*return*/, this.source.load(this.list)];
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    NotificationsComponent.prototype.onUserRowSelect = function (event) {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.statusArray = [];
                        this.display = true;
                        this.jobNo = event.data.jobNo;
                        this.clientName = event.data.clientName;
                        return [4 /*yield*/, this.noti.getAllNotification()];
                    case 1:
                        result = _a.sent();
                        result.forEach(function (element) {
                            if (element.jobNo === _this.jobNo) {
                                element.notifications.forEach(function (ele) {
                                    Object.assign(ele, { 'stage': element.stage });
                                    if (_this.role === 'Admin') {
                                        _this.statusArray.push(ele);
                                    }
                                    else {
                                        if (ele.department === _this.role) {
                                            _this.statusArray.push(ele);
                                        }
                                    }
                                });
                            }
                        });
                        return [2 /*return*/, this.source1.load(this.statusArray)];
                }
            });
        });
    };
    NotificationsComponent.prototype.onSaveConfirm = function (event) {
        return __awaiter(this, void 0, void 0, function () {
            var resultAleart, editData, result, event_1, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.confirmationDialogService.confirm('Alert Message', 'Are you sure you want to Save?')];
                    case 1:
                        resultAleart = _a.sent();
                        if (!resultAleart) return [3 /*break*/, 6];
                        _a.label = 2;
                    case 2:
                        _a.trys.push([2, 4, , 5]);
                        editData = event.newData;
                        Object.assign(editData, { 'jobNo': this.jobNo });
                        return [4 /*yield*/, this.noti.updateNotificationStatus(editData)];
                    case 3:
                        result = _a.sent();
                        if (result) {
                            this.noti.notification.next(result);
                            event_1 = { 'data': { 'jobNo': this.jobNo, 'clientName': this.clientName } };
                            this.onUserRowSelect(event_1);
                        }
                        event.confirm.resolve();
                        return [3 /*break*/, 5];
                    case 4:
                        error_1 = _a.sent();
                        console.log(error_1);
                        return [3 /*break*/, 5];
                    case 5: return [3 /*break*/, 7];
                    case 6:
                        event.confirm.reject();
                        _a.label = 7;
                    case 7: return [2 /*return*/];
                }
            });
        });
    };
    NotificationsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-notifications',
            template: __webpack_require__(/*! ./notifications.component.html */ "./src/app/pages/ui-features/notification/notifications/notifications.component.html"),
            styles: [__webpack_require__(/*! ./notifications.component.scss */ "./src/app/pages/ui-features/notification/notifications/notifications.component.scss")]
        }),
        __metadata("design:paramtypes", [_confirmation_dialog_confirmation_dialog_service__WEBPACK_IMPORTED_MODULE_2__["ConfirmationDialogService"],
            _services_token_token_service__WEBPACK_IMPORTED_MODULE_4__["TokenService"],
            _services_notifications_service__WEBPACK_IMPORTED_MODULE_3__["NotificationsService"]])
    ], NotificationsComponent);
    return NotificationsComponent;
}());



/***/ })

}]);
//# sourceMappingURL=default~app-pages-pages-module~notification-notification-module~ui-features-ui-features-module.js.map