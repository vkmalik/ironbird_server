(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~app-pages-pages-module~customer-customer-module~ui-features-ui-features-module"],{

/***/ "./src/app/pages/ui-features/customer/customer-routing.module.ts":
/*!***********************************************************************!*\
  !*** ./src/app/pages/ui-features/customer/customer-routing.module.ts ***!
  \***********************************************************************/
/*! exports provided: CustomerRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerRoutingModule", function() { return CustomerRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _customer_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./customer.component */ "./src/app/pages/ui-features/customer/customer.component.ts");
/* harmony import */ var _customers_customers_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./customers/customers.component */ "./src/app/pages/ui-features/customer/customers/customers.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var routes = [{
        path: '',
        component: _customer_component__WEBPACK_IMPORTED_MODULE_2__["CustomerComponent"],
        children: [{
                path: 'customers',
                component: _customers_customers_component__WEBPACK_IMPORTED_MODULE_3__["CustomersComponent"],
            },
        ]
    }];
var CustomerRoutingModule = /** @class */ (function () {
    function CustomerRoutingModule() {
    }
    CustomerRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], CustomerRoutingModule);
    return CustomerRoutingModule;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/customer/customer.component.html":
/*!********************************************************************!*\
  !*** ./src/app/pages/ui-features/customer/customer.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/pages/ui-features/customer/customer.component.scss":
/*!********************************************************************!*\
  !*** ./src/app/pages/ui-features/customer/customer.component.scss ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VpLWZlYXR1cmVzL2N1c3RvbWVyL2N1c3RvbWVyLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/pages/ui-features/customer/customer.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/pages/ui-features/customer/customer.component.ts ***!
  \******************************************************************/
/*! exports provided: CustomerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerComponent", function() { return CustomerComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var CustomerComponent = /** @class */ (function () {
    function CustomerComponent() {
    }
    CustomerComponent.prototype.ngOnInit = function () {
    };
    CustomerComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-customer',
            template: __webpack_require__(/*! ./customer.component.html */ "./src/app/pages/ui-features/customer/customer.component.html"),
            styles: [__webpack_require__(/*! ./customer.component.scss */ "./src/app/pages/ui-features/customer/customer.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], CustomerComponent);
    return CustomerComponent;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/customer/customer.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/ui-features/customer/customer.module.ts ***!
  \***************************************************************/
/*! exports provided: CustomerModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerModule", function() { return CustomerModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _asymmetrik_ngx_leaflet__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @asymmetrik/ngx-leaflet */ "./node_modules/@asymmetrik/ngx-leaflet/dist/index.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var _nebular_theme__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @nebular/theme */ "./node_modules/@nebular/theme/index.js");
/* harmony import */ var _swimlane_ngx_charts__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @swimlane/ngx-charts */ "./node_modules/@swimlane/ngx-charts/release/index.js");
/* harmony import */ var _swimlane_ngx_charts__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_swimlane_ngx_charts__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var ngx_echarts__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-echarts */ "./node_modules/ngx-echarts/ngx-echarts.es5.js");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var angular_custom_modal__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! angular-custom-modal */ "./node_modules/angular-custom-modal/angular-custom-modal.es5.js");
/* harmony import */ var _theme_theme_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../@theme/theme.module */ "./src/app/@theme/theme.module.ts");
/* harmony import */ var _shared_can_deactivate_guard__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../shared/can-deactivate.guard */ "./src/app/shared/can-deactivate.guard.ts");
/* harmony import */ var _core_data_smart_table_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../@core/data/smart-table.service */ "./src/app/@core/data/smart-table.service.ts");
/* harmony import */ var _customer_routing_module__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./customer-routing.module */ "./src/app/pages/ui-features/customer/customer-routing.module.ts");
/* harmony import */ var _customer_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./customer.component */ "./src/app/pages/ui-features/customer/customer.component.ts");
/* harmony import */ var _customers_customers_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./customers/customers.component */ "./src/app/pages/ui-features/customer/customers/customers.component.ts");
/* harmony import */ var _customers_detail_customers_detail_customers_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./customers/detail-customers/detail-customers.component */ "./src/app/pages/ui-features/customer/customers/detail-customers/detail-customers.component.ts");
/* harmony import */ var _customers_new_customer_new_customer_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./customers/new-customer/new-customer.component */ "./src/app/pages/ui-features/customer/customers/new-customer/new-customer.component.ts");
/* harmony import */ var _customers_detail_customers_customer_details_customer_details_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./customers/detail-customers/customer-details/customer-details.component */ "./src/app/pages/ui-features/customer/customers/detail-customers/customer-details/customer-details.component.ts");
/* harmony import */ var _customers_detail_customers_customer_invoices_customer_invoices_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./customers/detail-customers/customer-invoices/customer-invoices.component */ "./src/app/pages/ui-features/customer/customers/detail-customers/customer-invoices/customer-invoices.component.ts");
/* harmony import */ var _customers_detail_customers_customer_jobs_customer_jobs_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./customers/detail-customers/customer-jobs/customer-jobs.component */ "./src/app/pages/ui-features/customer/customers/detail-customers/customer-jobs/customer-jobs.component.ts");
/* harmony import */ var _customers_detail_customers_customer_quotations_customer_quotations_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./customers/detail-customers/customer-quotations/customer-quotations.component */ "./src/app/pages/ui-features/customer/customers/detail-customers/customer-quotations/customer-quotations.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






















var CustomerModule = /** @class */ (function () {
    function CustomerModule() {
    }
    CustomerModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _customer_routing_module__WEBPACK_IMPORTED_MODULE_12__["CustomerRoutingModule"],
                ng2_smart_table__WEBPACK_IMPORTED_MODULE_7__["Ng2SmartTableModule"],
                ngx_echarts__WEBPACK_IMPORTED_MODULE_6__["NgxEchartsModule"],
                _swimlane_ngx_charts__WEBPACK_IMPORTED_MODULE_5__["NgxChartsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModule"],
                _theme_theme_module__WEBPACK_IMPORTED_MODULE_9__["ThemeModule"],
                _asymmetrik_ngx_leaflet__WEBPACK_IMPORTED_MODULE_2__["LeafletModule"],
                _nebular_theme__WEBPACK_IMPORTED_MODULE_4__["NbInputModule"],
                angular_custom_modal__WEBPACK_IMPORTED_MODULE_8__["ModalModule"]
            ],
            declarations: [
                _customer_component__WEBPACK_IMPORTED_MODULE_13__["CustomerComponent"],
                _customers_customers_component__WEBPACK_IMPORTED_MODULE_14__["CustomersComponent"],
                // TotalCustomersComponent, 
                _customers_detail_customers_detail_customers_component__WEBPACK_IMPORTED_MODULE_15__["DetailCustomersComponent"],
                _customers_new_customer_new_customer_component__WEBPACK_IMPORTED_MODULE_16__["NewCustomerComponent"],
                _customers_detail_customers_customer_quotations_customer_quotations_component__WEBPACK_IMPORTED_MODULE_20__["CustomerQuotationsComponent"],
                _customers_detail_customers_customer_jobs_customer_jobs_component__WEBPACK_IMPORTED_MODULE_19__["CustomerJobsComponent"],
                _customers_detail_customers_customer_details_customer_details_component__WEBPACK_IMPORTED_MODULE_17__["CustomerDetailsComponent"], _customers_detail_customers_customer_invoices_customer_invoices_component__WEBPACK_IMPORTED_MODULE_18__["CustomerInvoicesComponent"]
            ],
            providers: [
                _core_data_smart_table_service__WEBPACK_IMPORTED_MODULE_11__["SmartTableService"],
                _shared_can_deactivate_guard__WEBPACK_IMPORTED_MODULE_10__["CanDeactivateGuard"],
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["DatePipe"]
            ],
        })
    ], CustomerModule);
    return CustomerModule;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/customer/customers/customers.component.html":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/ui-features/customer/customers/customers.component.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n    <div class=\"col-xl-5\">\n        <nb-card>\n            <nb-card-header>\n                <div class=\"row\">\n                    <div class=\"col\">\n                        Client List\n                        <!-- <p>Click on the customer name to view full details</p> -->\n                    </div>\n                    <div class=\"col\">\n                        <button *ngIf='moduleCreate' class=\"btn btn-outline-primary \" size=\"small\" shape=\"semi-round\" (click)=\"showFun('create')\">Create Client</button>\n                    </div>\n                </div>\n            </nb-card-header>\n            <nb-card-body>\n                <!-- <input #search class=\"search\" type=\"text\" placeholder=\"Search...\" (keydown.enter)=\"onSearch(search.value)\"> -->\n                <ng2-smart-table [settings]=\"settings\" [source]=\"source\" (deleteConfirm)=\"onDeleteConfirm($event)\" (userRowSelect)=\"onUserRowSelect($event)\">\n                </ng2-smart-table>\n            </nb-card-body>\n        </nb-card>\n    </div>\n    <div class=\"col-xl-7\" *ngIf=\"tableShow\">\n        <ngx-detail-customers></ngx-detail-customers>\n    </div>\n    <div class=\"col-xl-7\" *ngIf=\"creatShow\">\n        <!-- Nav tabs -->\n        <ngx-new-customer (customerChange)=\"createData($event)\"></ngx-new-customer>\n        <!-- Nav tabs -->\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/pages/ui-features/customer/customers/customers.component.scss":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/ui-features/customer/customers/customers.component.scss ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * This is a starting point where we declare the maps of themes and globally available functions/mixins\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * This mixin generates keyfames.\n * Because of all keyframes can't be scoped,\n * we need to puts unique name in each btn-pulse call.\n */\n/*\n\nAccording to the specification (https://www.w3.org/TR/css-scoping-1/#host-selector)\n:host and :host-context are pseudo-classes. So we assume they could be combined,\nlike other pseudo-classes, even same ones.\nFor example: ':nth-of-type(2n):nth-of-type(even)'.\n\nIdeal solution would be to prepend any selector with :host-context([dir=rtl]).\nThen nebular components will behave as an html element and respond to [dir] attribute on any level,\nso direction could be overridden on any component level.\n\nImplementation code:\n\n@mixin nb-rtl() {\n  // add # to scss interpolation statement.\n  // it works in comments and we can't use it here\n  @at-root {selector-append(':host-context([dir=rtl])', &)} {\n    @content;\n  }\n}\n\nAnd when we call it somewhere:\n\n:host {\n  .some-class {\n    @include nb-rtl() {\n      ...\n    }\n  }\n}\n:host-context(...) {\n  .some-class {\n    @include nb-rtl() {\n      ...\n    }\n  }\n}\n\nResult will look like:\n\n:host-context([dir=rtl]):host .some-class {\n  ...\n}\n:host-context([dir=rtl]):host-context(...) .some-class {\n  ...\n}\n\n*\n  Side note:\n  :host-context():host selector are valid. https://lists.w3.org/Archives/Public/www-style/2015Feb/0305.html\n\n  :host-context([dir=rtl]):host-context(...) should match any permutation,\n  so order is not important.\n*\n\n\nCurrently, there're two problems with this approach:\n\nFirst, is that we can't combine :host, :host-context. Angular bugs #14349, #19199.\nFor the moment of writing, the only possible way is:\n:host {\n  :host-context(...) {\n    ...\n  }\n}\nIt doesn't work for us because mixin could be called somewhere deeper, like:\n:host {\n  p {\n    @include nb-rtl() { ... }\n  }\n}\nWe are not able to go up to :host level to place content passed to mixin.\n\nThe second problem is that we only can be sure that we appending :host-context([dir=rtl]) to another\n:host/:host-context pseudo-class when called in theme files (*.theme.scss).\n  *\n    Side note:\n    Currently, nb-install-component uses another approach where :host prepended with the theme name\n    (https://github.com/angular/angular/blob/5b96078624b0a4760f2dbcf6fdf0bd62791be5bb/packages/compiler/src/shadow_css.ts#L441),\n    but it was made to be able to use current realization of rtl and it can be rewritten back to\n    :host-context($theme) once we will be able to use multiple shadow selectors.\n  *\nBut when it's called in *.component.scss we can't be sure, that selector starts with :host/:host-context,\nbecause angular allows omitting pseudo-classes if we don't need to style :host component itself.\nWe can break such selectors, by just appending :host-context([dir=rtl]) to them.\n  ***\n    Possible solution\n    check if we in theme by some theme variables and if so append, otherwise nest like\n    @at-root :host-context([dir=rtl]) {\n      // add # to scss interpolation statement.\n      // it works in comments and we can't use it here\n      {&} {\n        @content;\n      }\n    }\n    What if :host specified? Can we add space in :host-context(...) :host?\n    Or maybe add :host selector anyway? If multiple :host selectors are allowed\n  ***\n\n\nProblems with the current approach.\n\n1. Direction can be applied only on document level, because mixin prepends theme class,\nwhich placed on the body.\n2. *.component.scss styles should be in :host selector. Otherwise angular will add host\nattribute to [dir=rtl] attribute as well.\n\n\nGeneral problems.\n\nLtr is default document direction, but for proper work of nb-ltr (means ltr only),\n[dir=ltr] should be specified at least somewhere. ':not([dir=rtl]' not applicable here,\nbecause it's satisfy any parent, that don't have [dir=rtl] attribute.\nPrevious approach was to use single rtl mixin and reset ltr properties to initial value.\nBut sometimes it's hard to find, what the previous value should be. And such mixin call looks too verbose.\n*/\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * This mixin generates keyfames.\n * Because of all keyframes can't be scoped,\n * we need to puts unique name in each btn-pulse call.\n */\n/*\n\nAccording to the specification (https://www.w3.org/TR/css-scoping-1/#host-selector)\n:host and :host-context are pseudo-classes. So we assume they could be combined,\nlike other pseudo-classes, even same ones.\nFor example: ':nth-of-type(2n):nth-of-type(even)'.\n\nIdeal solution would be to prepend any selector with :host-context([dir=rtl]).\nThen nebular components will behave as an html element and respond to [dir] attribute on any level,\nso direction could be overridden on any component level.\n\nImplementation code:\n\n@mixin nb-rtl() {\n  // add # to scss interpolation statement.\n  // it works in comments and we can't use it here\n  @at-root {selector-append(':host-context([dir=rtl])', &)} {\n    @content;\n  }\n}\n\nAnd when we call it somewhere:\n\n:host {\n  .some-class {\n    @include nb-rtl() {\n      ...\n    }\n  }\n}\n:host-context(...) {\n  .some-class {\n    @include nb-rtl() {\n      ...\n    }\n  }\n}\n\nResult will look like:\n\n:host-context([dir=rtl]):host .some-class {\n  ...\n}\n:host-context([dir=rtl]):host-context(...) .some-class {\n  ...\n}\n\n*\n  Side note:\n  :host-context():host selector are valid. https://lists.w3.org/Archives/Public/www-style/2015Feb/0305.html\n\n  :host-context([dir=rtl]):host-context(...) should match any permutation,\n  so order is not important.\n*\n\n\nCurrently, there're two problems with this approach:\n\nFirst, is that we can't combine :host, :host-context. Angular bugs #14349, #19199.\nFor the moment of writing, the only possible way is:\n:host {\n  :host-context(...) {\n    ...\n  }\n}\nIt doesn't work for us because mixin could be called somewhere deeper, like:\n:host {\n  p {\n    @include nb-rtl() { ... }\n  }\n}\nWe are not able to go up to :host level to place content passed to mixin.\n\nThe second problem is that we only can be sure that we appending :host-context([dir=rtl]) to another\n:host/:host-context pseudo-class when called in theme files (*.theme.scss).\n  *\n    Side note:\n    Currently, nb-install-component uses another approach where :host prepended with the theme name\n    (https://github.com/angular/angular/blob/5b96078624b0a4760f2dbcf6fdf0bd62791be5bb/packages/compiler/src/shadow_css.ts#L441),\n    but it was made to be able to use current realization of rtl and it can be rewritten back to\n    :host-context($theme) once we will be able to use multiple shadow selectors.\n  *\nBut when it's called in *.component.scss we can't be sure, that selector starts with :host/:host-context,\nbecause angular allows omitting pseudo-classes if we don't need to style :host component itself.\nWe can break such selectors, by just appending :host-context([dir=rtl]) to them.\n  ***\n    Possible solution\n    check if we in theme by some theme variables and if so append, otherwise nest like\n    @at-root :host-context([dir=rtl]) {\n      // add # to scss interpolation statement.\n      // it works in comments and we can't use it here\n      {&} {\n        @content;\n      }\n    }\n    What if :host specified? Can we add space in :host-context(...) :host?\n    Or maybe add :host selector anyway? If multiple :host selectors are allowed\n  ***\n\n\nProblems with the current approach.\n\n1. Direction can be applied only on document level, because mixin prepends theme class,\nwhich placed on the body.\n2. *.component.scss styles should be in :host selector. Otherwise angular will add host\nattribute to [dir=rtl] attribute as well.\n\n\nGeneral problems.\n\nLtr is default document direction, but for proper work of nb-ltr (means ltr only),\n[dir=ltr] should be specified at least somewhere. ':not([dir=rtl]' not applicable here,\nbecause it's satisfy any parent, that don't have [dir=rtl] attribute.\nPrevious approach was to use single rtl mixin and reset ltr properties to initial value.\nBut sometimes it's hard to find, what the previous value should be. And such mixin call looks too verbose.\n*/\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * This mixin generates keyfames.\n * Because of all keyframes can't be scoped,\n * we need to puts unique name in each btn-pulse call.\n */\n/*\n\nAccording to the specification (https://www.w3.org/TR/css-scoping-1/#host-selector)\n:host and :host-context are pseudo-classes. So we assume they could be combined,\nlike other pseudo-classes, even same ones.\nFor example: ':nth-of-type(2n):nth-of-type(even)'.\n\nIdeal solution would be to prepend any selector with :host-context([dir=rtl]).\nThen nebular components will behave as an html element and respond to [dir] attribute on any level,\nso direction could be overridden on any component level.\n\nImplementation code:\n\n@mixin nb-rtl() {\n  // add # to scss interpolation statement.\n  // it works in comments and we can't use it here\n  @at-root {selector-append(':host-context([dir=rtl])', &)} {\n    @content;\n  }\n}\n\nAnd when we call it somewhere:\n\n:host {\n  .some-class {\n    @include nb-rtl() {\n      ...\n    }\n  }\n}\n:host-context(...) {\n  .some-class {\n    @include nb-rtl() {\n      ...\n    }\n  }\n}\n\nResult will look like:\n\n:host-context([dir=rtl]):host .some-class {\n  ...\n}\n:host-context([dir=rtl]):host-context(...) .some-class {\n  ...\n}\n\n*\n  Side note:\n  :host-context():host selector are valid. https://lists.w3.org/Archives/Public/www-style/2015Feb/0305.html\n\n  :host-context([dir=rtl]):host-context(...) should match any permutation,\n  so order is not important.\n*\n\n\nCurrently, there're two problems with this approach:\n\nFirst, is that we can't combine :host, :host-context. Angular bugs #14349, #19199.\nFor the moment of writing, the only possible way is:\n:host {\n  :host-context(...) {\n    ...\n  }\n}\nIt doesn't work for us because mixin could be called somewhere deeper, like:\n:host {\n  p {\n    @include nb-rtl() { ... }\n  }\n}\nWe are not able to go up to :host level to place content passed to mixin.\n\nThe second problem is that we only can be sure that we appending :host-context([dir=rtl]) to another\n:host/:host-context pseudo-class when called in theme files (*.theme.scss).\n  *\n    Side note:\n    Currently, nb-install-component uses another approach where :host prepended with the theme name\n    (https://github.com/angular/angular/blob/5b96078624b0a4760f2dbcf6fdf0bd62791be5bb/packages/compiler/src/shadow_css.ts#L441),\n    but it was made to be able to use current realization of rtl and it can be rewritten back to\n    :host-context($theme) once we will be able to use multiple shadow selectors.\n  *\nBut when it's called in *.component.scss we can't be sure, that selector starts with :host/:host-context,\nbecause angular allows omitting pseudo-classes if we don't need to style :host component itself.\nWe can break such selectors, by just appending :host-context([dir=rtl]) to them.\n  ***\n    Possible solution\n    check if we in theme by some theme variables and if so append, otherwise nest like\n    @at-root :host-context([dir=rtl]) {\n      // add # to scss interpolation statement.\n      // it works in comments and we can't use it here\n      {&} {\n        @content;\n      }\n    }\n    What if :host specified? Can we add space in :host-context(...) :host?\n    Or maybe add :host selector anyway? If multiple :host selectors are allowed\n  ***\n\n\nProblems with the current approach.\n\n1. Direction can be applied only on document level, because mixin prepends theme class,\nwhich placed on the body.\n2. *.component.scss styles should be in :host selector. Otherwise angular will add host\nattribute to [dir=rtl] attribute as well.\n\n\nGeneral problems.\n\nLtr is default document direction, but for proper work of nb-ltr (means ltr only),\n[dir=ltr] should be specified at least somewhere. ':not([dir=rtl]' not applicable here,\nbecause it's satisfy any parent, that don't have [dir=rtl] attribute.\nPrevious approach was to use single rtl mixin and reset ltr properties to initial value.\nBut sometimes it's hard to find, what the previous value should be. And such mixin call looks too verbose.\n*/\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * This mixin generates keyfames.\n * Because of all keyframes can't be scoped,\n * we need to puts unique name in each btn-pulse call.\n */\n/*\n\nAccording to the specification (https://www.w3.org/TR/css-scoping-1/#host-selector)\n:host and :host-context are pseudo-classes. So we assume they could be combined,\nlike other pseudo-classes, even same ones.\nFor example: ':nth-of-type(2n):nth-of-type(even)'.\n\nIdeal solution would be to prepend any selector with :host-context([dir=rtl]).\nThen nebular components will behave as an html element and respond to [dir] attribute on any level,\nso direction could be overridden on any component level.\n\nImplementation code:\n\n@mixin nb-rtl() {\n  // add # to scss interpolation statement.\n  // it works in comments and we can't use it here\n  @at-root {selector-append(':host-context([dir=rtl])', &)} {\n    @content;\n  }\n}\n\nAnd when we call it somewhere:\n\n:host {\n  .some-class {\n    @include nb-rtl() {\n      ...\n    }\n  }\n}\n:host-context(...) {\n  .some-class {\n    @include nb-rtl() {\n      ...\n    }\n  }\n}\n\nResult will look like:\n\n:host-context([dir=rtl]):host .some-class {\n  ...\n}\n:host-context([dir=rtl]):host-context(...) .some-class {\n  ...\n}\n\n*\n  Side note:\n  :host-context():host selector are valid. https://lists.w3.org/Archives/Public/www-style/2015Feb/0305.html\n\n  :host-context([dir=rtl]):host-context(...) should match any permutation,\n  so order is not important.\n*\n\n\nCurrently, there're two problems with this approach:\n\nFirst, is that we can't combine :host, :host-context. Angular bugs #14349, #19199.\nFor the moment of writing, the only possible way is:\n:host {\n  :host-context(...) {\n    ...\n  }\n}\nIt doesn't work for us because mixin could be called somewhere deeper, like:\n:host {\n  p {\n    @include nb-rtl() { ... }\n  }\n}\nWe are not able to go up to :host level to place content passed to mixin.\n\nThe second problem is that we only can be sure that we appending :host-context([dir=rtl]) to another\n:host/:host-context pseudo-class when called in theme files (*.theme.scss).\n  *\n    Side note:\n    Currently, nb-install-component uses another approach where :host prepended with the theme name\n    (https://github.com/angular/angular/blob/5b96078624b0a4760f2dbcf6fdf0bd62791be5bb/packages/compiler/src/shadow_css.ts#L441),\n    but it was made to be able to use current realization of rtl and it can be rewritten back to\n    :host-context($theme) once we will be able to use multiple shadow selectors.\n  *\nBut when it's called in *.component.scss we can't be sure, that selector starts with :host/:host-context,\nbecause angular allows omitting pseudo-classes if we don't need to style :host component itself.\nWe can break such selectors, by just appending :host-context([dir=rtl]) to them.\n  ***\n    Possible solution\n    check if we in theme by some theme variables and if so append, otherwise nest like\n    @at-root :host-context([dir=rtl]) {\n      // add # to scss interpolation statement.\n      // it works in comments and we can't use it here\n      {&} {\n        @content;\n      }\n    }\n    What if :host specified? Can we add space in :host-context(...) :host?\n    Or maybe add :host selector anyway? If multiple :host selectors are allowed\n  ***\n\n\nProblems with the current approach.\n\n1. Direction can be applied only on document level, because mixin prepends theme class,\nwhich placed on the body.\n2. *.component.scss styles should be in :host selector. Otherwise angular will add host\nattribute to [dir=rtl] attribute as well.\n\n\nGeneral problems.\n\nLtr is default document direction, but for proper work of nb-ltr (means ltr only),\n[dir=ltr] should be specified at least somewhere. ':not([dir=rtl]' not applicable here,\nbecause it's satisfy any parent, that don't have [dir=rtl] attribute.\nPrevious approach was to use single rtl mixin and reset ltr properties to initial value.\nBut sometimes it's hard to find, what the previous value should be. And such mixin call looks too verbose.\n*/\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * This mixin generates keyfames.\n * Because of all keyframes can't be scoped,\n * we need to puts unique name in each btn-pulse call.\n */\n/*\n\nAccording to the specification (https://www.w3.org/TR/css-scoping-1/#host-selector)\n:host and :host-context are pseudo-classes. So we assume they could be combined,\nlike other pseudo-classes, even same ones.\nFor example: ':nth-of-type(2n):nth-of-type(even)'.\n\nIdeal solution would be to prepend any selector with :host-context([dir=rtl]).\nThen nebular components will behave as an html element and respond to [dir] attribute on any level,\nso direction could be overridden on any component level.\n\nImplementation code:\n\n@mixin nb-rtl() {\n  // add # to scss interpolation statement.\n  // it works in comments and we can't use it here\n  @at-root {selector-append(':host-context([dir=rtl])', &)} {\n    @content;\n  }\n}\n\nAnd when we call it somewhere:\n\n:host {\n  .some-class {\n    @include nb-rtl() {\n      ...\n    }\n  }\n}\n:host-context(...) {\n  .some-class {\n    @include nb-rtl() {\n      ...\n    }\n  }\n}\n\nResult will look like:\n\n:host-context([dir=rtl]):host .some-class {\n  ...\n}\n:host-context([dir=rtl]):host-context(...) .some-class {\n  ...\n}\n\n*\n  Side note:\n  :host-context():host selector are valid. https://lists.w3.org/Archives/Public/www-style/2015Feb/0305.html\n\n  :host-context([dir=rtl]):host-context(...) should match any permutation,\n  so order is not important.\n*\n\n\nCurrently, there're two problems with this approach:\n\nFirst, is that we can't combine :host, :host-context. Angular bugs #14349, #19199.\nFor the moment of writing, the only possible way is:\n:host {\n  :host-context(...) {\n    ...\n  }\n}\nIt doesn't work for us because mixin could be called somewhere deeper, like:\n:host {\n  p {\n    @include nb-rtl() { ... }\n  }\n}\nWe are not able to go up to :host level to place content passed to mixin.\n\nThe second problem is that we only can be sure that we appending :host-context([dir=rtl]) to another\n:host/:host-context pseudo-class when called in theme files (*.theme.scss).\n  *\n    Side note:\n    Currently, nb-install-component uses another approach where :host prepended with the theme name\n    (https://github.com/angular/angular/blob/5b96078624b0a4760f2dbcf6fdf0bd62791be5bb/packages/compiler/src/shadow_css.ts#L441),\n    but it was made to be able to use current realization of rtl and it can be rewritten back to\n    :host-context($theme) once we will be able to use multiple shadow selectors.\n  *\nBut when it's called in *.component.scss we can't be sure, that selector starts with :host/:host-context,\nbecause angular allows omitting pseudo-classes if we don't need to style :host component itself.\nWe can break such selectors, by just appending :host-context([dir=rtl]) to them.\n  ***\n    Possible solution\n    check if we in theme by some theme variables and if so append, otherwise nest like\n    @at-root :host-context([dir=rtl]) {\n      // add # to scss interpolation statement.\n      // it works in comments and we can't use it here\n      {&} {\n        @content;\n      }\n    }\n    What if :host specified? Can we add space in :host-context(...) :host?\n    Or maybe add :host selector anyway? If multiple :host selectors are allowed\n  ***\n\n\nProblems with the current approach.\n\n1. Direction can be applied only on document level, because mixin prepends theme class,\nwhich placed on the body.\n2. *.component.scss styles should be in :host selector. Otherwise angular will add host\nattribute to [dir=rtl] attribute as well.\n\n\nGeneral problems.\n\nLtr is default document direction, but for proper work of nb-ltr (means ltr only),\n[dir=ltr] should be specified at least somewhere. ':not([dir=rtl]' not applicable here,\nbecause it's satisfy any parent, that don't have [dir=rtl] attribute.\nPrevious approach was to use single rtl mixin and reset ltr properties to initial value.\nBut sometimes it's hard to find, what the previous value should be. And such mixin call looks too verbose.\n*/\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * This mixin generates keyfames.\n * Because of all keyframes can't be scoped,\n * we need to puts unique name in each btn-pulse call.\n */\n/*\n\nAccording to the specification (https://www.w3.org/TR/css-scoping-1/#host-selector)\n:host and :host-context are pseudo-classes. So we assume they could be combined,\nlike other pseudo-classes, even same ones.\nFor example: ':nth-of-type(2n):nth-of-type(even)'.\n\nIdeal solution would be to prepend any selector with :host-context([dir=rtl]).\nThen nebular components will behave as an html element and respond to [dir] attribute on any level,\nso direction could be overridden on any component level.\n\nImplementation code:\n\n@mixin nb-rtl() {\n  // add # to scss interpolation statement.\n  // it works in comments and we can't use it here\n  @at-root {selector-append(':host-context([dir=rtl])', &)} {\n    @content;\n  }\n}\n\nAnd when we call it somewhere:\n\n:host {\n  .some-class {\n    @include nb-rtl() {\n      ...\n    }\n  }\n}\n:host-context(...) {\n  .some-class {\n    @include nb-rtl() {\n      ...\n    }\n  }\n}\n\nResult will look like:\n\n:host-context([dir=rtl]):host .some-class {\n  ...\n}\n:host-context([dir=rtl]):host-context(...) .some-class {\n  ...\n}\n\n*\n  Side note:\n  :host-context():host selector are valid. https://lists.w3.org/Archives/Public/www-style/2015Feb/0305.html\n\n  :host-context([dir=rtl]):host-context(...) should match any permutation,\n  so order is not important.\n*\n\n\nCurrently, there're two problems with this approach:\n\nFirst, is that we can't combine :host, :host-context. Angular bugs #14349, #19199.\nFor the moment of writing, the only possible way is:\n:host {\n  :host-context(...) {\n    ...\n  }\n}\nIt doesn't work for us because mixin could be called somewhere deeper, like:\n:host {\n  p {\n    @include nb-rtl() { ... }\n  }\n}\nWe are not able to go up to :host level to place content passed to mixin.\n\nThe second problem is that we only can be sure that we appending :host-context([dir=rtl]) to another\n:host/:host-context pseudo-class when called in theme files (*.theme.scss).\n  *\n    Side note:\n    Currently, nb-install-component uses another approach where :host prepended with the theme name\n    (https://github.com/angular/angular/blob/5b96078624b0a4760f2dbcf6fdf0bd62791be5bb/packages/compiler/src/shadow_css.ts#L441),\n    but it was made to be able to use current realization of rtl and it can be rewritten back to\n    :host-context($theme) once we will be able to use multiple shadow selectors.\n  *\nBut when it's called in *.component.scss we can't be sure, that selector starts with :host/:host-context,\nbecause angular allows omitting pseudo-classes if we don't need to style :host component itself.\nWe can break such selectors, by just appending :host-context([dir=rtl]) to them.\n  ***\n    Possible solution\n    check if we in theme by some theme variables and if so append, otherwise nest like\n    @at-root :host-context([dir=rtl]) {\n      // add # to scss interpolation statement.\n      // it works in comments and we can't use it here\n      {&} {\n        @content;\n      }\n    }\n    What if :host specified? Can we add space in :host-context(...) :host?\n    Or maybe add :host selector anyway? If multiple :host selectors are allowed\n  ***\n\n\nProblems with the current approach.\n\n1. Direction can be applied only on document level, because mixin prepends theme class,\nwhich placed on the body.\n2. *.component.scss styles should be in :host selector. Otherwise angular will add host\nattribute to [dir=rtl] attribute as well.\n\n\nGeneral problems.\n\nLtr is default document direction, but for proper work of nb-ltr (means ltr only),\n[dir=ltr] should be specified at least somewhere. ':not([dir=rtl]' not applicable here,\nbecause it's satisfy any parent, that don't have [dir=rtl] attribute.\nPrevious approach was to use single rtl mixin and reset ltr properties to initial value.\nBut sometimes it's hard to find, what the previous value should be. And such mixin call looks too verbose.\n*/\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/*\n      :host can be prefixed\n      https://github.com/angular/angular/blob/8d0ee34939f14c07876d222c25b405ed458a34d3/packages/compiler/src/shadow_css.ts#L441\n\n      We have to use :host insted of :host-context($theme), to be able to prefix theme class\n      with something defined inside of @content, by prefixing &.\n      For example this scss code:\n        .nb-theme-default {\n          .some-selector & {\n            ...\n          }\n        }\n      Will result in next css:\n        .some-selector .nb-theme-default {\n          ...\n        }\n\n      It doesn't work with :host-context because angular splitting it in two selectors and removes\n      prefix in one of the selectors.\n    */\n.nb-theme-default :host {\n  /* nb-card-body {\n    padding: 0 1.25rem 1.25rem 0;\n    display: flex;\n    flex-wrap: wrap;\n  } */ }\n.nb-theme-default :host .btn {\n    line-height: 14px !important; }\n.nb-theme-default :host .container-title {\n    margin-bottom: 0.25rem; }\n.nb-theme-default :host .size-container {\n    margin: 1.25rem 0 0 1.25rem; }\n.nb-theme-default :host .subheader {\n    margin-bottom: 0.75rem;\n    font-size: 0.875rem;\n    font-weight: 500;\n    color: #2a2a2a; }\n.nb-theme-default :host button {\n    float: right; }\n.nb-theme-default :host i {\n    cursor: pointer; }\n.nb-theme-default :host nb-card-body {\n    background-color: #E1F5FE;\n    color: white; }\n.nb-theme-default :host col-xl-6,\n  .nb-theme-default :host nb-card-header {\n    background: #1D8ECE;\n    color: white; }\n/*\n      :host can be prefixed\n      https://github.com/angular/angular/blob/8d0ee34939f14c07876d222c25b405ed458a34d3/packages/compiler/src/shadow_css.ts#L441\n\n      We have to use :host insted of :host-context($theme), to be able to prefix theme class\n      with something defined inside of @content, by prefixing &.\n      For example this scss code:\n        .nb-theme-default {\n          .some-selector & {\n            ...\n          }\n        }\n      Will result in next css:\n        .some-selector .nb-theme-default {\n          ...\n        }\n\n      It doesn't work with :host-context because angular splitting it in two selectors and removes\n      prefix in one of the selectors.\n    */\n.nb-theme-cosmic :host {\n  /* nb-card-body {\n    padding: 0 1.25rem 1.25rem 0;\n    display: flex;\n    flex-wrap: wrap;\n  } */ }\n.nb-theme-cosmic :host .btn {\n    line-height: 14px !important; }\n.nb-theme-cosmic :host .container-title {\n    margin-bottom: 0.25rem; }\n.nb-theme-cosmic :host .size-container {\n    margin: 1.25rem 0 0 1.25rem; }\n.nb-theme-cosmic :host .subheader {\n    margin-bottom: 0.75rem;\n    font-size: 0.875rem;\n    font-weight: 500;\n    color: #ffffff; }\n.nb-theme-cosmic :host button {\n    float: right; }\n.nb-theme-cosmic :host i {\n    cursor: pointer; }\n.nb-theme-cosmic :host nb-card-body {\n    background-color: #E1F5FE;\n    color: white; }\n.nb-theme-cosmic :host col-xl-6,\n  .nb-theme-cosmic :host nb-card-header {\n    background: #1D8ECE;\n    color: white; }\n/*\n      :host can be prefixed\n      https://github.com/angular/angular/blob/8d0ee34939f14c07876d222c25b405ed458a34d3/packages/compiler/src/shadow_css.ts#L441\n\n      We have to use :host insted of :host-context($theme), to be able to prefix theme class\n      with something defined inside of @content, by prefixing &.\n      For example this scss code:\n        .nb-theme-default {\n          .some-selector & {\n            ...\n          }\n        }\n      Will result in next css:\n        .some-selector .nb-theme-default {\n          ...\n        }\n\n      It doesn't work with :host-context because angular splitting it in two selectors and removes\n      prefix in one of the selectors.\n    */\n.nb-theme-corporate :host {\n  /* nb-card-body {\n    padding: 0 1.25rem 1.25rem 0;\n    display: flex;\n    flex-wrap: wrap;\n  } */ }\n.nb-theme-corporate :host .btn {\n    line-height: 14px !important; }\n.nb-theme-corporate :host .container-title {\n    margin-bottom: 0.25rem; }\n.nb-theme-corporate :host .size-container {\n    margin: 1.25rem 0 0 1.25rem; }\n.nb-theme-corporate :host .subheader {\n    margin-bottom: 0.75rem;\n    font-size: 0.875rem;\n    font-weight: 500;\n    color: #181818; }\n.nb-theme-corporate :host button {\n    float: right; }\n.nb-theme-corporate :host i {\n    cursor: pointer; }\n.nb-theme-corporate :host nb-card-body {\n    background-color: #E1F5FE;\n    color: white; }\n.nb-theme-corporate :host col-xl-6,\n  .nb-theme-corporate :host nb-card-header {\n    background: #1D8ECE;\n    color: white; }\n:host /deep/ table tbody tr:nth-child(even) {\n  background: #e9eaea !important; }\n:host /deep/ table tbody tr:nth-child(odd) {\n  background: white !important; }\n/deep/ ng2-smart-table-pager nav ul {\n  background-color: black; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3ByYWthc2gvVmlwaW4vaXJvbmJpcmQvMjBGRUIyMDIwL2lyb25iaXJkLWNsaWVudHMvbm9kZV9tb2R1bGVzL0BuZWJ1bGFyL3RoZW1lL3N0eWxlcy9fdGhlbWluZy5zY3NzIiwic3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9jdXN0b21lci9jdXN0b21lcnMvY3VzdG9tZXJzLmNvbXBvbmVudC5zY3NzIiwiL2hvbWUvcHJha2FzaC9WaXBpbi9pcm9uYmlyZC8yMEZFQjIwMjAvaXJvbmJpcmQtY2xpZW50cy9ub2RlX21vZHVsZXMvQG5lYnVsYXIvdGhlbWUvc3R5bGVzL2NvcmUvX21peGlucy5zY3NzIiwiL2hvbWUvcHJha2FzaC9WaXBpbi9pcm9uYmlyZC8yMEZFQjIwMjAvaXJvbmJpcmQtY2xpZW50cy9ub2RlX21vZHVsZXMvQG5lYnVsYXIvdGhlbWUvc3R5bGVzL2NvcmUvX2Z1bmN0aW9ucy5zY3NzIiwiL2hvbWUvcHJha2FzaC9WaXBpbi9pcm9uYmlyZC8yMEZFQjIwMjAvaXJvbmJpcmQtY2xpZW50cy9ub2RlX21vZHVsZXMvQG5lYnVsYXIvdGhlbWUvc3R5bGVzL3RoZW1lcy9fZGVmYXVsdC5zY3NzIiwiL2hvbWUvcHJha2FzaC9WaXBpbi9pcm9uYmlyZC8yMEZFQjIwMjAvaXJvbmJpcmQtY2xpZW50cy9ub2RlX21vZHVsZXMvQG5lYnVsYXIvdGhlbWUvc3R5bGVzL3RoZW1lcy9fY29zbWljLnNjc3MiLCIvaG9tZS9wcmFrYXNoL1ZpcGluL2lyb25iaXJkLzIwRkVCMjAyMC9pcm9uYmlyZC1jbGllbnRzL25vZGVfbW9kdWxlcy9AbmVidWxhci90aGVtZS9zdHlsZXMvdGhlbWVzL19jb3Jwb3JhdGUuc2NzcyIsIi9ob21lL3ByYWthc2gvVmlwaW4vaXJvbmJpcmQvMjBGRUIyMDIwL2lyb25iaXJkLWNsaWVudHMvbm9kZV9tb2R1bGVzL0BuZWJ1bGFyL3RoZW1lL3N0eWxlcy9nbG9iYWwvYm9vdHN0cmFwL19idXR0b25zLnNjc3MiLCIvaG9tZS9wcmFrYXNoL1ZpcGluL2lyb25iaXJkLzIwRkVCMjAyMC9pcm9uYmlyZC1jbGllbnRzL25vZGVfbW9kdWxlcy9AbmVidWxhci90aGVtZS9zdHlsZXMvZ2xvYmFsL2Jvb3RzdHJhcC9fc2hhcGUtYnV0dG9ucy5zY3NzIiwiL2hvbWUvcHJha2FzaC9WaXBpbi9pcm9uYmlyZC8yMEZFQjIwMjAvaXJvbmJpcmQtY2xpZW50cy9ub2RlX21vZHVsZXMvQG5lYnVsYXIvdGhlbWUvc3R5bGVzL2dsb2JhbC9ib290c3RyYXAvX3NpemUtYnV0dG9ucy5zY3NzIiwiL2hvbWUvcHJha2FzaC9WaXBpbi9pcm9uYmlyZC8yMEZFQjIwMjAvaXJvbmJpcmQtY2xpZW50cy9ub2RlX21vZHVsZXMvQG5lYnVsYXIvdGhlbWUvc3R5bGVzL2dsb2JhbC9ib290c3RyYXAvX2RlZmF1bHQtYnV0dG9ucy5zY3NzIiwiL2hvbWUvcHJha2FzaC9WaXBpbi9pcm9uYmlyZC8yMEZFQjIwMjAvaXJvbmJpcmQtY2xpZW50cy9ub2RlX21vZHVsZXMvQG5lYnVsYXIvdGhlbWUvc3R5bGVzL2dsb2JhbC9ib290c3RyYXAvX2hlcm8tYnV0dG9ucy5zY3NzIiwiL2hvbWUvcHJha2FzaC9WaXBpbi9pcm9uYmlyZC8yMEZFQjIwMjAvaXJvbmJpcmQtY2xpZW50cy9ub2RlX21vZHVsZXMvQG5lYnVsYXIvdGhlbWUvc3R5bGVzL2dsb2JhbC9ib290c3RyYXAvX291dGxpbmUtYnV0dG9ucy5zY3NzIiwiL2hvbWUvcHJha2FzaC9WaXBpbi9pcm9uYmlyZC8yMEZFQjIwMjAvaXJvbmJpcmQtY2xpZW50cy9zcmMvYXBwL3BhZ2VzL3VpLWZlYXR1cmVzL2N1c3RvbWVyL2N1c3RvbWVycy9jdXN0b21lcnMuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7RUNJRTtBREdGOztFQ0FFO0FDUEY7Ozs7RURZRTtBQzhKRjs7OztFRHpKRTtBQ21MRjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztDRC9EQztBRXJJRDs7OztFRjBJRTtBRzFJRjs7OztFSCtJRTtBRS9JRjs7OztFRm9KRTtBQ3BKRjs7OztFRHlKRTtBQ2lCRjs7OztFRFpFO0FDc0NGOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0NEOEVDO0FJbFJEOzs7O0VKdVJFO0FFdlJGOzs7O0VGNFJFO0FDNVJGOzs7O0VEaVNFO0FDdkhGOzs7O0VENEhFO0FDbEdGOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0NEc05DO0FHMVpEOzs7O0VIK1pFO0FFL1pGOzs7O0VGb2FFO0FDcGFGOzs7O0VEeWFFO0FDL1BGOzs7O0VEb1FFO0FDMU9GOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0NEOFZDO0FLbGlCRDs7OztFTHVpQkU7QUV2aUJGOzs7O0VGNGlCRTtBQzVpQkY7Ozs7RURpakJFO0FDdllGOzs7O0VENFlFO0FDbFhGOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0NEc2VDO0FHMXFCRDs7OztFSCtxQkU7QUUvcUJGOzs7O0VGb3JCRTtBQ3ByQkY7Ozs7RUR5ckJFO0FDL2dCRjs7OztFRG9oQkU7QUMxZkY7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Q0Q4bUJDO0FNbHpCRDs7OztFTnV6QkU7QU92ekJGOzs7O0VQNHpCRTtBUTV6QkY7Ozs7RVJpMEJFO0FTajBCRjs7OztFVHMwQkU7QVV0MEJGOzs7O0VWMjBCRTtBVzMwQkY7Ozs7RVhnMUJFO0FTaDFCRjs7OztFVHExQkU7QURudUJFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0tDdXZCQztBRG51QkQ7RWFuSUE7Ozs7S1o0MkJDLEVZeDJCQztBQUNGO0lBQ0ksNEJBQTRCLEVBQUE7QUFHaEM7SUFDSSxzQkFBc0IsRUFBQTtBQUUxQjtJQUNJLDJCQUEyQixFQUFBO0FBRS9CO0lBQ0ksc0JBQXNCO0lBQ3RCLG1CQUFtQjtJQUNuQixnQlRMaUI7SVNNakIsY1RlbUIsRUFBQTtBU2J2QjtJQUNJLFlBQVksRUFBQTtBQUdoQjtJQUNJLGVBQWUsRUFBQTtBQUVuQjtJQUNJLHlCQUF5QjtJQUN6QixZQUFZLEVBQUE7QUFFaEI7O0lBRUksbUJBQW1CO0lBQ25CLFlBQVksRUFBQTtBYjRFaEI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7S0N3eUJDO0FEcHhCRDtFYW5JQTs7OztLWjY1QkMsRVl6NUJDO0FBQ0Y7SUFDSSw0QkFBNEIsRUFBQTtBQUdoQztJQUNJLHNCQUFzQixFQUFBO0FBRTFCO0lBQ0ksMkJBQTJCLEVBQUE7QUFFL0I7SUFDSSxzQkFBc0I7SUFDdEIsbUJBQW1CO0lBQ25CLGdCVExpQjtJU01qQixjUkxtQixFQUFBO0FRT3ZCO0lBQ0ksWUFBWSxFQUFBO0FBR2hCO0lBQ0ksZUFBZSxFQUFBO0FBRW5CO0lBQ0kseUJBQXlCO0lBQ3pCLFlBQVksRUFBQTtBQUVoQjs7SUFFSSxtQkFBbUI7SUFDbkIsWUFBWSxFQUFBO0FiNEVoQjs7Ozs7Ozs7Ozs7Ozs7Ozs7OztLQ3kxQkM7QURyMEJEO0VhbklBOzs7O0taODhCQyxFWTE4QkM7QUFDRjtJQUNJLDRCQUE0QixFQUFBO0FBR2hDO0lBQ0ksc0JBQXNCLEVBQUE7QUFFMUI7SUFDSSwyQkFBMkIsRUFBQTtBQUUvQjtJQUNJLHNCQUFzQjtJQUN0QixtQkFBbUI7SUFDbkIsZ0JUTGlCO0lTTWpCLGNQTG1CLEVBQUE7QU9PdkI7SUFDSSxZQUFZLEVBQUE7QUFHaEI7SUFDSSxlQUFlLEVBQUE7QUFFbkI7SUFDSSx5QkFBeUI7SUFDekIsWUFBWSxFQUFBO0FBRWhCOztJQUVJLG1CQUFtQjtJQUNuQixZQUFZLEVBQUE7QUFJcEI7RUFDSSw4QkFBNkIsRUFBQTtBQUdoQztFQUNHLDRCQUEyQixFQUFBO0FBUy9CO0VBQ0ksdUJBQXVCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9jdXN0b21lci9jdXN0b21lcnMvY3VzdG9tZXJzLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBAbGljZW5zZVxuICogQ29weXJpZ2h0IEFrdmVvLiBBbGwgUmlnaHRzIFJlc2VydmVkLlxuICogTGljZW5zZWQgdW5kZXIgdGhlIE1JVCBMaWNlbnNlLiBTZWUgTGljZW5zZS50eHQgaW4gdGhlIHByb2plY3Qgcm9vdCBmb3IgbGljZW5zZSBpbmZvcm1hdGlvbi5cbiAqL1xuXG5cbi8qKlxuICogVGhpcyBpcyBhIHN0YXJ0aW5nIHBvaW50IHdoZXJlIHdlIGRlY2xhcmUgdGhlIG1hcHMgb2YgdGhlbWVzIGFuZCBnbG9iYWxseSBhdmFpbGFibGUgZnVuY3Rpb25zL21peGluc1xuICovXG5cbkBpbXBvcnQgJ2NvcmUvbWl4aW5zJztcbkBpbXBvcnQgJ2NvcmUvZnVuY3Rpb25zJztcblxuJG5iLWVuYWJsZWQtdGhlbWVzOiAoKSAhZ2xvYmFsO1xuJG5iLWVuYWJsZS1jc3MtdmFyaWFibGVzOiBmYWxzZSAhZ2xvYmFsO1xuXG4kbmItdGhlbWVzOiAoKSAhZ2xvYmFsO1xuJG5iLXRoZW1lcy1ub24tcHJvY2Vzc2VkOiAoKSAhZ2xvYmFsO1xuJG5iLXRoZW1lcy1leHBvcnQ6ICgpICFnbG9iYWw7XG5cbkBmdW5jdGlvbiBuYi10aGVtZSgka2V5KSB7XG4gIEByZXR1cm4gbWFwLWdldCgkdGhlbWUsICRrZXkpO1xufVxuXG5AZnVuY3Rpb24gbmItZ2V0LXZhbHVlKCR0aGVtZSwgJGtleSwgJHZhbHVlKSB7XG4gIEBpZiAodHlwZS1vZigkdmFsdWUpID09ICdzdHJpbmcnKSB7XG4gICAgJHRtcDogbWFwLWdldCgkdGhlbWUsICR2YWx1ZSk7XG5cbiAgICBAaWYgKCR0bXAgIT0gbnVsbCkge1xuICAgICAgQHJldHVybiBuYi1nZXQtdmFsdWUoJHRoZW1lLCAkdmFsdWUsICR0bXApO1xuICAgIH1cbiAgfVxuXG4gIEByZXR1cm4gbWFwLWdldCgkdGhlbWUsICRrZXkpO1xufVxuXG5AZnVuY3Rpb24gY29udmVydC10by1jc3MtdmFyaWFibGVzKCR2YXJpYWJsZXMpIHtcbiAgJHJlc3VsdDogKCk7XG4gIEBlYWNoICR2YXIsICR2YWx1ZSBpbiAkdmFyaWFibGVzIHtcbiAgICAkcmVzdWx0OiBtYXAtc2V0KCRyZXN1bHQsICR2YXIsICctLXZhcigjeyR2YXJ9KScpO1xuICB9XG5cbiAgQGRlYnVnICRyZXN1bHQ7XG4gIEByZXR1cm4gJHJlc3VsdDtcbn1cblxuQGZ1bmN0aW9uIHNldC1nbG9iYWwtdGhlbWUtdmFycygkdGhlbWUsICR0aGVtZS1uYW1lKSB7XG4gICR0aGVtZTogJHRoZW1lICFnbG9iYWw7XG4gICR0aGVtZS1uYW1lOiAkdGhlbWUtbmFtZSAhZ2xvYmFsO1xuICBAaWYgKCRuYi1lbmFibGUtY3NzLXZhcmlhYmxlcykge1xuICAgICR0aGVtZTogY29udmVydC10by1jc3MtdmFyaWFibGVzKCR0aGVtZSkgIWdsb2JhbDtcbiAgfVxuICBAcmV0dXJuICR0aGVtZTtcbn1cblxuQGZ1bmN0aW9uIG5iLXJlZ2lzdGVyLXRoZW1lKCR0aGVtZSwgJG5hbWUsICRkZWZhdWx0OiBudWxsKSB7XG5cbiAgJHRoZW1lLWRhdGE6ICgpO1xuXG5cbiAgQGlmICgkZGVmYXVsdCAhPSBudWxsKSB7XG5cbiAgICAkdGhlbWU6IG1hcC1tZXJnZShtYXAtZ2V0KCRuYi10aGVtZXMtbm9uLXByb2Nlc3NlZCwgJGRlZmF1bHQpLCAkdGhlbWUpO1xuICAgICRuYi10aGVtZXMtbm9uLXByb2Nlc3NlZDogbWFwLXNldCgkbmItdGhlbWVzLW5vbi1wcm9jZXNzZWQsICRuYW1lLCAkdGhlbWUpICFnbG9iYWw7XG5cbiAgICAkdGhlbWUtZGF0YTogbWFwLXNldCgkdGhlbWUtZGF0YSwgZGF0YSwgJHRoZW1lKTtcbiAgICAkbmItdGhlbWVzLWV4cG9ydDogbWFwLXNldCgkbmItdGhlbWVzLWV4cG9ydCwgJG5hbWUsIG1hcC1zZXQoJHRoZW1lLWRhdGEsIHBhcmVudCwgJGRlZmF1bHQpKSAhZ2xvYmFsO1xuXG4gIH0gQGVsc2Uge1xuICAgICRuYi10aGVtZXMtbm9uLXByb2Nlc3NlZDogbWFwLXNldCgkbmItdGhlbWVzLW5vbi1wcm9jZXNzZWQsICRuYW1lLCAkdGhlbWUpICFnbG9iYWw7XG5cbiAgICAkdGhlbWUtZGF0YTogbWFwLXNldCgkdGhlbWUtZGF0YSwgZGF0YSwgJHRoZW1lKTtcbiAgICAkbmItdGhlbWVzLWV4cG9ydDogbWFwLXNldCgkbmItdGhlbWVzLWV4cG9ydCwgJG5hbWUsIG1hcC1zZXQoJHRoZW1lLWRhdGEsIHBhcmVudCwgbnVsbCkpICFnbG9iYWw7XG4gIH1cblxuICAkdGhlbWUtcGFyc2VkOiAoKTtcbiAgQGVhY2ggJGtleSwgJHZhbHVlIGluICR0aGVtZSB7XG4gICAgJHRoZW1lLXBhcnNlZDogbWFwLXNldCgkdGhlbWUtcGFyc2VkLCAka2V5LCBuYi1nZXQtdmFsdWUoJHRoZW1lLCAka2V5LCAkdmFsdWUpKTtcbiAgfVxuXG4gIC8vIGVuYWJsZSByaWdodCBhd2F5IHdoZW4gaW5zdGFsbGVkXG4gICR0aGVtZS1wYXJzZWQ6IHNldC1nbG9iYWwtdGhlbWUtdmFycygkdGhlbWUtcGFyc2VkLCAkbmFtZSk7XG4gIEByZXR1cm4gbWFwLXNldCgkbmItdGhlbWVzLCAkbmFtZSwgJHRoZW1lLXBhcnNlZCk7XG59XG5cbkBmdW5jdGlvbiBnZXQtZW5hYmxlZC10aGVtZXMoKSB7XG4gICR0aGVtZXMtdG8taW5zdGFsbDogKCk7XG5cbiAgQGlmIChsZW5ndGgoJG5iLWVuYWJsZWQtdGhlbWVzKSA+IDApIHtcbiAgICBAZWFjaCAkdGhlbWUtbmFtZSBpbiAkbmItZW5hYmxlZC10aGVtZXMge1xuICAgICAgJHRoZW1lcy10by1pbnN0YWxsOiBtYXAtc2V0KCR0aGVtZXMtdG8taW5zdGFsbCwgJHRoZW1lLW5hbWUsIG1hcC1nZXQoJG5iLXRoZW1lcywgJHRoZW1lLW5hbWUpKTtcbiAgICB9XG4gIH0gQGVsc2Uge1xuICAgICR0aGVtZXMtdG8taW5zdGFsbDogJG5iLXRoZW1lcztcbiAgfVxuXG4gIEByZXR1cm4gJHRoZW1lcy10by1pbnN0YWxsO1xufVxuXG5AbWl4aW4gaW5zdGFsbC1jc3MtdmFyaWFibGVzKCR0aGVtZS1uYW1lLCAkdmFyaWFibGVzKSB7XG4gIC5uYi10aGVtZS0jeyR0aGVtZS1uYW1lfSB7XG4gICAgQGVhY2ggJHZhciwgJHZhbHVlIGluICR2YXJpYWJsZXMge1xuICAgICAgLS0jeyR2YXJ9OiAkdmFsdWU7XG4gICAgfVxuICB9XG59XG5cbi8vIFRPRE86IHdlIGhpZGUgOmhvc3QgaW5zaWRlIG9mIGl0IHdoaWNoIGlzIG5vdCBvYnZpb3VzXG5AbWl4aW4gbmItaW5zdGFsbC1jb21wb25lbnQoKSB7XG5cbiAgJHRoZW1lcy10by1pbnN0YWxsOiBnZXQtZW5hYmxlZC10aGVtZXMoKTtcblxuICBAZWFjaCAkdGhlbWUtbmFtZSwgJHRoZW1lIGluICR0aGVtZXMtdG8taW5zdGFsbCB7XG4gICAgLypcbiAgICAgIDpob3N0IGNhbiBiZSBwcmVmaXhlZFxuICAgICAgaHR0cHM6Ly9naXRodWIuY29tL2FuZ3VsYXIvYW5ndWxhci9ibG9iLzhkMGVlMzQ5MzlmMTRjMDc4NzZkMjIyYzI1YjQwNWVkNDU4YTM0ZDMvcGFja2FnZXMvY29tcGlsZXIvc3JjL3NoYWRvd19jc3MudHMjTDQ0MVxuXG4gICAgICBXZSBoYXZlIHRvIHVzZSA6aG9zdCBpbnN0ZWQgb2YgOmhvc3QtY29udGV4dCgkdGhlbWUpLCB0byBiZSBhYmxlIHRvIHByZWZpeCB0aGVtZSBjbGFzc1xuICAgICAgd2l0aCBzb21ldGhpbmcgZGVmaW5lZCBpbnNpZGUgb2YgQGNvbnRlbnQsIGJ5IHByZWZpeGluZyAmLlxuICAgICAgRm9yIGV4YW1wbGUgdGhpcyBzY3NzIGNvZGU6XG4gICAgICAgIC5uYi10aGVtZS1kZWZhdWx0IHtcbiAgICAgICAgICAuc29tZS1zZWxlY3RvciAmIHtcbiAgICAgICAgICAgIC4uLlxuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgV2lsbCByZXN1bHQgaW4gbmV4dCBjc3M6XG4gICAgICAgIC5zb21lLXNlbGVjdG9yIC5uYi10aGVtZS1kZWZhdWx0IHtcbiAgICAgICAgICAuLi5cbiAgICAgICAgfVxuXG4gICAgICBJdCBkb2Vzbid0IHdvcmsgd2l0aCA6aG9zdC1jb250ZXh0IGJlY2F1c2UgYW5ndWxhciBzcGxpdHRpbmcgaXQgaW4gdHdvIHNlbGVjdG9ycyBhbmQgcmVtb3Zlc1xuICAgICAgcHJlZml4IGluIG9uZSBvZiB0aGUgc2VsZWN0b3JzLlxuICAgICovXG4gICAgLm5iLXRoZW1lLSN7JHRoZW1lLW5hbWV9IDpob3N0IHtcbiAgICAgICR0aGVtZTogc2V0LWdsb2JhbC10aGVtZS12YXJzKCR0aGVtZSwgJHRoZW1lLW5hbWUpO1xuICAgICAgQGNvbnRlbnQ7XG4gICAgfVxuICB9XG59XG5cbkBtaXhpbiBuYi1mb3ItdGhlbWUoJG5hbWUpIHtcbiAgQGlmICgkdGhlbWUtbmFtZSA9PSAkbmFtZSkge1xuICAgIEBjb250ZW50O1xuICB9XG59XG5cbkBtaXhpbiBuYi1leGNlcHQtdGhlbWUoJG5hbWUpIHtcbiAgQGlmICgkdGhlbWUtbmFtZSAhPSAkbmFtZSkge1xuICAgIEBjb250ZW50O1xuICB9XG59XG5cbi8vIFRPRE86IGFub3RoZXIgbWl4aW5nIGZvciB0aGUgYWxtb3N0IHNhbWUgdGhpbmdcbkBtaXhpbiBuYi1pbnN0YWxsLXJvb3QtY29tcG9uZW50KCkge1xuICBAd2FybiAnYG5iLWluc3RhbGwtcm9vdC1jb21wb25lbnRgIGlzIGRlcHJpY2F0ZWQsIHJlcGxhY2Ugd2l0aCBgbmItaW5zdGFsbC1jb21wb25lbnRgLCBhcyBgYm9keWAgaXMgcm9vdCBlbGVtZW50IG5vdyc7XG5cbiAgQGluY2x1ZGUgbmItaW5zdGFsbC1jb21wb25lbnQoKSB7XG4gICAgQGNvbnRlbnQ7XG4gIH1cbn1cblxuQG1peGluIG5iLWluc3RhbGwtZ2xvYmFsKCkge1xuICAkdGhlbWVzLXRvLWluc3RhbGw6IGdldC1lbmFibGVkLXRoZW1lcygpO1xuXG4gIEBlYWNoICR0aGVtZS1uYW1lLCAkdGhlbWUgaW4gJHRoZW1lcy10by1pbnN0YWxsIHtcbiAgICAubmItdGhlbWUtI3skdGhlbWUtbmFtZX0ge1xuICAgICAgJHRoZW1lOiBzZXQtZ2xvYmFsLXRoZW1lLXZhcnMoJHRoZW1lLCAkdGhlbWUtbmFtZSk7XG4gICAgICBAY29udGVudDtcbiAgICB9XG4gIH1cbn1cbiIsIi8qKlxuICogQGxpY2Vuc2VcbiAqIENvcHlyaWdodCBBa3Zlby4gQWxsIFJpZ2h0cyBSZXNlcnZlZC5cbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgTGljZW5zZS4gU2VlIExpY2Vuc2UudHh0IGluIHRoZSBwcm9qZWN0IHJvb3QgZm9yIGxpY2Vuc2UgaW5mb3JtYXRpb24uXG4gKi9cbi8qKlxuICogVGhpcyBpcyBhIHN0YXJ0aW5nIHBvaW50IHdoZXJlIHdlIGRlY2xhcmUgdGhlIG1hcHMgb2YgdGhlbWVzIGFuZCBnbG9iYWxseSBhdmFpbGFibGUgZnVuY3Rpb25zL21peGluc1xuICovXG4vKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG4vKipcbiAqIFRoaXMgbWl4aW4gZ2VuZXJhdGVzIGtleWZhbWVzLlxuICogQmVjYXVzZSBvZiBhbGwga2V5ZnJhbWVzIGNhbid0IGJlIHNjb3BlZCxcbiAqIHdlIG5lZWQgdG8gcHV0cyB1bmlxdWUgbmFtZSBpbiBlYWNoIGJ0bi1wdWxzZSBjYWxsLlxuICovXG4vKlxuXG5BY2NvcmRpbmcgdG8gdGhlIHNwZWNpZmljYXRpb24gKGh0dHBzOi8vd3d3LnczLm9yZy9UUi9jc3Mtc2NvcGluZy0xLyNob3N0LXNlbGVjdG9yKVxuOmhvc3QgYW5kIDpob3N0LWNvbnRleHQgYXJlIHBzZXVkby1jbGFzc2VzLiBTbyB3ZSBhc3N1bWUgdGhleSBjb3VsZCBiZSBjb21iaW5lZCxcbmxpa2Ugb3RoZXIgcHNldWRvLWNsYXNzZXMsIGV2ZW4gc2FtZSBvbmVzLlxuRm9yIGV4YW1wbGU6ICc6bnRoLW9mLXR5cGUoMm4pOm50aC1vZi10eXBlKGV2ZW4pJy5cblxuSWRlYWwgc29sdXRpb24gd291bGQgYmUgdG8gcHJlcGVuZCBhbnkgc2VsZWN0b3Igd2l0aCA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkuXG5UaGVuIG5lYnVsYXIgY29tcG9uZW50cyB3aWxsIGJlaGF2ZSBhcyBhbiBodG1sIGVsZW1lbnQgYW5kIHJlc3BvbmQgdG8gW2Rpcl0gYXR0cmlidXRlIG9uIGFueSBsZXZlbCxcbnNvIGRpcmVjdGlvbiBjb3VsZCBiZSBvdmVycmlkZGVuIG9uIGFueSBjb21wb25lbnQgbGV2ZWwuXG5cbkltcGxlbWVudGF0aW9uIGNvZGU6XG5cbkBtaXhpbiBuYi1ydGwoKSB7XG4gIC8vIGFkZCAjIHRvIHNjc3MgaW50ZXJwb2xhdGlvbiBzdGF0ZW1lbnQuXG4gIC8vIGl0IHdvcmtzIGluIGNvbW1lbnRzIGFuZCB3ZSBjYW4ndCB1c2UgaXQgaGVyZVxuICBAYXQtcm9vdCB7c2VsZWN0b3ItYXBwZW5kKCc6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSknLCAmKX0ge1xuICAgIEBjb250ZW50O1xuICB9XG59XG5cbkFuZCB3aGVuIHdlIGNhbGwgaXQgc29tZXdoZXJlOlxuXG46aG9zdCB7XG4gIC5zb21lLWNsYXNzIHtcbiAgICBAaW5jbHVkZSBuYi1ydGwoKSB7XG4gICAgICAuLi5cbiAgICB9XG4gIH1cbn1cbjpob3N0LWNvbnRleHQoLi4uKSB7XG4gIC5zb21lLWNsYXNzIHtcbiAgICBAaW5jbHVkZSBuYi1ydGwoKSB7XG4gICAgICAuLi5cbiAgICB9XG4gIH1cbn1cblxuUmVzdWx0IHdpbGwgbG9vayBsaWtlOlxuXG46aG9zdC1jb250ZXh0KFtkaXI9cnRsXSk6aG9zdCAuc29tZS1jbGFzcyB7XG4gIC4uLlxufVxuOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pOmhvc3QtY29udGV4dCguLi4pIC5zb21lLWNsYXNzIHtcbiAgLi4uXG59XG5cbipcbiAgU2lkZSBub3RlOlxuICA6aG9zdC1jb250ZXh0KCk6aG9zdCBzZWxlY3RvciBhcmUgdmFsaWQuIGh0dHBzOi8vbGlzdHMudzMub3JnL0FyY2hpdmVzL1B1YmxpYy93d3ctc3R5bGUvMjAxNUZlYi8wMzA1Lmh0bWxcblxuICA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSk6aG9zdC1jb250ZXh0KC4uLikgc2hvdWxkIG1hdGNoIGFueSBwZXJtdXRhdGlvbixcbiAgc28gb3JkZXIgaXMgbm90IGltcG9ydGFudC5cbipcblxuXG5DdXJyZW50bHksIHRoZXJlJ3JlIHR3byBwcm9ibGVtcyB3aXRoIHRoaXMgYXBwcm9hY2g6XG5cbkZpcnN0LCBpcyB0aGF0IHdlIGNhbid0IGNvbWJpbmUgOmhvc3QsIDpob3N0LWNvbnRleHQuIEFuZ3VsYXIgYnVncyAjMTQzNDksICMxOTE5OS5cbkZvciB0aGUgbW9tZW50IG9mIHdyaXRpbmcsIHRoZSBvbmx5IHBvc3NpYmxlIHdheSBpczpcbjpob3N0IHtcbiAgOmhvc3QtY29udGV4dCguLi4pIHtcbiAgICAuLi5cbiAgfVxufVxuSXQgZG9lc24ndCB3b3JrIGZvciB1cyBiZWNhdXNlIG1peGluIGNvdWxkIGJlIGNhbGxlZCBzb21ld2hlcmUgZGVlcGVyLCBsaWtlOlxuOmhvc3Qge1xuICBwIHtcbiAgICBAaW5jbHVkZSBuYi1ydGwoKSB7IC4uLiB9XG4gIH1cbn1cbldlIGFyZSBub3QgYWJsZSB0byBnbyB1cCB0byA6aG9zdCBsZXZlbCB0byBwbGFjZSBjb250ZW50IHBhc3NlZCB0byBtaXhpbi5cblxuVGhlIHNlY29uZCBwcm9ibGVtIGlzIHRoYXQgd2Ugb25seSBjYW4gYmUgc3VyZSB0aGF0IHdlIGFwcGVuZGluZyA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkgdG8gYW5vdGhlclxuOmhvc3QvOmhvc3QtY29udGV4dCBwc2V1ZG8tY2xhc3Mgd2hlbiBjYWxsZWQgaW4gdGhlbWUgZmlsZXMgKCoudGhlbWUuc2NzcykuXG4gICpcbiAgICBTaWRlIG5vdGU6XG4gICAgQ3VycmVudGx5LCBuYi1pbnN0YWxsLWNvbXBvbmVudCB1c2VzIGFub3RoZXIgYXBwcm9hY2ggd2hlcmUgOmhvc3QgcHJlcGVuZGVkIHdpdGggdGhlIHRoZW1lIG5hbWVcbiAgICAoaHR0cHM6Ly9naXRodWIuY29tL2FuZ3VsYXIvYW5ndWxhci9ibG9iLzViOTYwNzg2MjRiMGE0NzYwZjJkYmNmNmZkZjBiZDYyNzkxYmU1YmIvcGFja2FnZXMvY29tcGlsZXIvc3JjL3NoYWRvd19jc3MudHMjTDQ0MSksXG4gICAgYnV0IGl0IHdhcyBtYWRlIHRvIGJlIGFibGUgdG8gdXNlIGN1cnJlbnQgcmVhbGl6YXRpb24gb2YgcnRsIGFuZCBpdCBjYW4gYmUgcmV3cml0dGVuIGJhY2sgdG9cbiAgICA6aG9zdC1jb250ZXh0KCR0aGVtZSkgb25jZSB3ZSB3aWxsIGJlIGFibGUgdG8gdXNlIG11bHRpcGxlIHNoYWRvdyBzZWxlY3RvcnMuXG4gICpcbkJ1dCB3aGVuIGl0J3MgY2FsbGVkIGluICouY29tcG9uZW50LnNjc3Mgd2UgY2FuJ3QgYmUgc3VyZSwgdGhhdCBzZWxlY3RvciBzdGFydHMgd2l0aCA6aG9zdC86aG9zdC1jb250ZXh0LFxuYmVjYXVzZSBhbmd1bGFyIGFsbG93cyBvbWl0dGluZyBwc2V1ZG8tY2xhc3NlcyBpZiB3ZSBkb24ndCBuZWVkIHRvIHN0eWxlIDpob3N0IGNvbXBvbmVudCBpdHNlbGYuXG5XZSBjYW4gYnJlYWsgc3VjaCBzZWxlY3RvcnMsIGJ5IGp1c3QgYXBwZW5kaW5nIDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKSB0byB0aGVtLlxuICAqKipcbiAgICBQb3NzaWJsZSBzb2x1dGlvblxuICAgIGNoZWNrIGlmIHdlIGluIHRoZW1lIGJ5IHNvbWUgdGhlbWUgdmFyaWFibGVzIGFuZCBpZiBzbyBhcHBlbmQsIG90aGVyd2lzZSBuZXN0IGxpa2VcbiAgICBAYXQtcm9vdCA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkge1xuICAgICAgLy8gYWRkICMgdG8gc2NzcyBpbnRlcnBvbGF0aW9uIHN0YXRlbWVudC5cbiAgICAgIC8vIGl0IHdvcmtzIGluIGNvbW1lbnRzIGFuZCB3ZSBjYW4ndCB1c2UgaXQgaGVyZVxuICAgICAgeyZ9IHtcbiAgICAgICAgQGNvbnRlbnQ7XG4gICAgICB9XG4gICAgfVxuICAgIFdoYXQgaWYgOmhvc3Qgc3BlY2lmaWVkPyBDYW4gd2UgYWRkIHNwYWNlIGluIDpob3N0LWNvbnRleHQoLi4uKSA6aG9zdD9cbiAgICBPciBtYXliZSBhZGQgOmhvc3Qgc2VsZWN0b3IgYW55d2F5PyBJZiBtdWx0aXBsZSA6aG9zdCBzZWxlY3RvcnMgYXJlIGFsbG93ZWRcbiAgKioqXG5cblxuUHJvYmxlbXMgd2l0aCB0aGUgY3VycmVudCBhcHByb2FjaC5cblxuMS4gRGlyZWN0aW9uIGNhbiBiZSBhcHBsaWVkIG9ubHkgb24gZG9jdW1lbnQgbGV2ZWwsIGJlY2F1c2UgbWl4aW4gcHJlcGVuZHMgdGhlbWUgY2xhc3MsXG53aGljaCBwbGFjZWQgb24gdGhlIGJvZHkuXG4yLiAqLmNvbXBvbmVudC5zY3NzIHN0eWxlcyBzaG91bGQgYmUgaW4gOmhvc3Qgc2VsZWN0b3IuIE90aGVyd2lzZSBhbmd1bGFyIHdpbGwgYWRkIGhvc3RcbmF0dHJpYnV0ZSB0byBbZGlyPXJ0bF0gYXR0cmlidXRlIGFzIHdlbGwuXG5cblxuR2VuZXJhbCBwcm9ibGVtcy5cblxuTHRyIGlzIGRlZmF1bHQgZG9jdW1lbnQgZGlyZWN0aW9uLCBidXQgZm9yIHByb3BlciB3b3JrIG9mIG5iLWx0ciAobWVhbnMgbHRyIG9ubHkpLFxuW2Rpcj1sdHJdIHNob3VsZCBiZSBzcGVjaWZpZWQgYXQgbGVhc3Qgc29tZXdoZXJlLiAnOm5vdChbZGlyPXJ0bF0nIG5vdCBhcHBsaWNhYmxlIGhlcmUsXG5iZWNhdXNlIGl0J3Mgc2F0aXNmeSBhbnkgcGFyZW50LCB0aGF0IGRvbid0IGhhdmUgW2Rpcj1ydGxdIGF0dHJpYnV0ZS5cblByZXZpb3VzIGFwcHJvYWNoIHdhcyB0byB1c2Ugc2luZ2xlIHJ0bCBtaXhpbiBhbmQgcmVzZXQgbHRyIHByb3BlcnRpZXMgdG8gaW5pdGlhbCB2YWx1ZS5cbkJ1dCBzb21ldGltZXMgaXQncyBoYXJkIHRvIGZpbmQsIHdoYXQgdGhlIHByZXZpb3VzIHZhbHVlIHNob3VsZCBiZS4gQW5kIHN1Y2ggbWl4aW4gY2FsbCBsb29rcyB0b28gdmVyYm9zZS5cbiovXG4vKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG4vKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG4vKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG4vKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG4vKipcbiAqIFRoaXMgbWl4aW4gZ2VuZXJhdGVzIGtleWZhbWVzLlxuICogQmVjYXVzZSBvZiBhbGwga2V5ZnJhbWVzIGNhbid0IGJlIHNjb3BlZCxcbiAqIHdlIG5lZWQgdG8gcHV0cyB1bmlxdWUgbmFtZSBpbiBlYWNoIGJ0bi1wdWxzZSBjYWxsLlxuICovXG4vKlxuXG5BY2NvcmRpbmcgdG8gdGhlIHNwZWNpZmljYXRpb24gKGh0dHBzOi8vd3d3LnczLm9yZy9UUi9jc3Mtc2NvcGluZy0xLyNob3N0LXNlbGVjdG9yKVxuOmhvc3QgYW5kIDpob3N0LWNvbnRleHQgYXJlIHBzZXVkby1jbGFzc2VzLiBTbyB3ZSBhc3N1bWUgdGhleSBjb3VsZCBiZSBjb21iaW5lZCxcbmxpa2Ugb3RoZXIgcHNldWRvLWNsYXNzZXMsIGV2ZW4gc2FtZSBvbmVzLlxuRm9yIGV4YW1wbGU6ICc6bnRoLW9mLXR5cGUoMm4pOm50aC1vZi10eXBlKGV2ZW4pJy5cblxuSWRlYWwgc29sdXRpb24gd291bGQgYmUgdG8gcHJlcGVuZCBhbnkgc2VsZWN0b3Igd2l0aCA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkuXG5UaGVuIG5lYnVsYXIgY29tcG9uZW50cyB3aWxsIGJlaGF2ZSBhcyBhbiBodG1sIGVsZW1lbnQgYW5kIHJlc3BvbmQgdG8gW2Rpcl0gYXR0cmlidXRlIG9uIGFueSBsZXZlbCxcbnNvIGRpcmVjdGlvbiBjb3VsZCBiZSBvdmVycmlkZGVuIG9uIGFueSBjb21wb25lbnQgbGV2ZWwuXG5cbkltcGxlbWVudGF0aW9uIGNvZGU6XG5cbkBtaXhpbiBuYi1ydGwoKSB7XG4gIC8vIGFkZCAjIHRvIHNjc3MgaW50ZXJwb2xhdGlvbiBzdGF0ZW1lbnQuXG4gIC8vIGl0IHdvcmtzIGluIGNvbW1lbnRzIGFuZCB3ZSBjYW4ndCB1c2UgaXQgaGVyZVxuICBAYXQtcm9vdCB7c2VsZWN0b3ItYXBwZW5kKCc6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSknLCAmKX0ge1xuICAgIEBjb250ZW50O1xuICB9XG59XG5cbkFuZCB3aGVuIHdlIGNhbGwgaXQgc29tZXdoZXJlOlxuXG46aG9zdCB7XG4gIC5zb21lLWNsYXNzIHtcbiAgICBAaW5jbHVkZSBuYi1ydGwoKSB7XG4gICAgICAuLi5cbiAgICB9XG4gIH1cbn1cbjpob3N0LWNvbnRleHQoLi4uKSB7XG4gIC5zb21lLWNsYXNzIHtcbiAgICBAaW5jbHVkZSBuYi1ydGwoKSB7XG4gICAgICAuLi5cbiAgICB9XG4gIH1cbn1cblxuUmVzdWx0IHdpbGwgbG9vayBsaWtlOlxuXG46aG9zdC1jb250ZXh0KFtkaXI9cnRsXSk6aG9zdCAuc29tZS1jbGFzcyB7XG4gIC4uLlxufVxuOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pOmhvc3QtY29udGV4dCguLi4pIC5zb21lLWNsYXNzIHtcbiAgLi4uXG59XG5cbipcbiAgU2lkZSBub3RlOlxuICA6aG9zdC1jb250ZXh0KCk6aG9zdCBzZWxlY3RvciBhcmUgdmFsaWQuIGh0dHBzOi8vbGlzdHMudzMub3JnL0FyY2hpdmVzL1B1YmxpYy93d3ctc3R5bGUvMjAxNUZlYi8wMzA1Lmh0bWxcblxuICA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSk6aG9zdC1jb250ZXh0KC4uLikgc2hvdWxkIG1hdGNoIGFueSBwZXJtdXRhdGlvbixcbiAgc28gb3JkZXIgaXMgbm90IGltcG9ydGFudC5cbipcblxuXG5DdXJyZW50bHksIHRoZXJlJ3JlIHR3byBwcm9ibGVtcyB3aXRoIHRoaXMgYXBwcm9hY2g6XG5cbkZpcnN0LCBpcyB0aGF0IHdlIGNhbid0IGNvbWJpbmUgOmhvc3QsIDpob3N0LWNvbnRleHQuIEFuZ3VsYXIgYnVncyAjMTQzNDksICMxOTE5OS5cbkZvciB0aGUgbW9tZW50IG9mIHdyaXRpbmcsIHRoZSBvbmx5IHBvc3NpYmxlIHdheSBpczpcbjpob3N0IHtcbiAgOmhvc3QtY29udGV4dCguLi4pIHtcbiAgICAuLi5cbiAgfVxufVxuSXQgZG9lc24ndCB3b3JrIGZvciB1cyBiZWNhdXNlIG1peGluIGNvdWxkIGJlIGNhbGxlZCBzb21ld2hlcmUgZGVlcGVyLCBsaWtlOlxuOmhvc3Qge1xuICBwIHtcbiAgICBAaW5jbHVkZSBuYi1ydGwoKSB7IC4uLiB9XG4gIH1cbn1cbldlIGFyZSBub3QgYWJsZSB0byBnbyB1cCB0byA6aG9zdCBsZXZlbCB0byBwbGFjZSBjb250ZW50IHBhc3NlZCB0byBtaXhpbi5cblxuVGhlIHNlY29uZCBwcm9ibGVtIGlzIHRoYXQgd2Ugb25seSBjYW4gYmUgc3VyZSB0aGF0IHdlIGFwcGVuZGluZyA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkgdG8gYW5vdGhlclxuOmhvc3QvOmhvc3QtY29udGV4dCBwc2V1ZG8tY2xhc3Mgd2hlbiBjYWxsZWQgaW4gdGhlbWUgZmlsZXMgKCoudGhlbWUuc2NzcykuXG4gICpcbiAgICBTaWRlIG5vdGU6XG4gICAgQ3VycmVudGx5LCBuYi1pbnN0YWxsLWNvbXBvbmVudCB1c2VzIGFub3RoZXIgYXBwcm9hY2ggd2hlcmUgOmhvc3QgcHJlcGVuZGVkIHdpdGggdGhlIHRoZW1lIG5hbWVcbiAgICAoaHR0cHM6Ly9naXRodWIuY29tL2FuZ3VsYXIvYW5ndWxhci9ibG9iLzViOTYwNzg2MjRiMGE0NzYwZjJkYmNmNmZkZjBiZDYyNzkxYmU1YmIvcGFja2FnZXMvY29tcGlsZXIvc3JjL3NoYWRvd19jc3MudHMjTDQ0MSksXG4gICAgYnV0IGl0IHdhcyBtYWRlIHRvIGJlIGFibGUgdG8gdXNlIGN1cnJlbnQgcmVhbGl6YXRpb24gb2YgcnRsIGFuZCBpdCBjYW4gYmUgcmV3cml0dGVuIGJhY2sgdG9cbiAgICA6aG9zdC1jb250ZXh0KCR0aGVtZSkgb25jZSB3ZSB3aWxsIGJlIGFibGUgdG8gdXNlIG11bHRpcGxlIHNoYWRvdyBzZWxlY3RvcnMuXG4gICpcbkJ1dCB3aGVuIGl0J3MgY2FsbGVkIGluICouY29tcG9uZW50LnNjc3Mgd2UgY2FuJ3QgYmUgc3VyZSwgdGhhdCBzZWxlY3RvciBzdGFydHMgd2l0aCA6aG9zdC86aG9zdC1jb250ZXh0LFxuYmVjYXVzZSBhbmd1bGFyIGFsbG93cyBvbWl0dGluZyBwc2V1ZG8tY2xhc3NlcyBpZiB3ZSBkb24ndCBuZWVkIHRvIHN0eWxlIDpob3N0IGNvbXBvbmVudCBpdHNlbGYuXG5XZSBjYW4gYnJlYWsgc3VjaCBzZWxlY3RvcnMsIGJ5IGp1c3QgYXBwZW5kaW5nIDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKSB0byB0aGVtLlxuICAqKipcbiAgICBQb3NzaWJsZSBzb2x1dGlvblxuICAgIGNoZWNrIGlmIHdlIGluIHRoZW1lIGJ5IHNvbWUgdGhlbWUgdmFyaWFibGVzIGFuZCBpZiBzbyBhcHBlbmQsIG90aGVyd2lzZSBuZXN0IGxpa2VcbiAgICBAYXQtcm9vdCA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkge1xuICAgICAgLy8gYWRkICMgdG8gc2NzcyBpbnRlcnBvbGF0aW9uIHN0YXRlbWVudC5cbiAgICAgIC8vIGl0IHdvcmtzIGluIGNvbW1lbnRzIGFuZCB3ZSBjYW4ndCB1c2UgaXQgaGVyZVxuICAgICAgeyZ9IHtcbiAgICAgICAgQGNvbnRlbnQ7XG4gICAgICB9XG4gICAgfVxuICAgIFdoYXQgaWYgOmhvc3Qgc3BlY2lmaWVkPyBDYW4gd2UgYWRkIHNwYWNlIGluIDpob3N0LWNvbnRleHQoLi4uKSA6aG9zdD9cbiAgICBPciBtYXliZSBhZGQgOmhvc3Qgc2VsZWN0b3IgYW55d2F5PyBJZiBtdWx0aXBsZSA6aG9zdCBzZWxlY3RvcnMgYXJlIGFsbG93ZWRcbiAgKioqXG5cblxuUHJvYmxlbXMgd2l0aCB0aGUgY3VycmVudCBhcHByb2FjaC5cblxuMS4gRGlyZWN0aW9uIGNhbiBiZSBhcHBsaWVkIG9ubHkgb24gZG9jdW1lbnQgbGV2ZWwsIGJlY2F1c2UgbWl4aW4gcHJlcGVuZHMgdGhlbWUgY2xhc3MsXG53aGljaCBwbGFjZWQgb24gdGhlIGJvZHkuXG4yLiAqLmNvbXBvbmVudC5zY3NzIHN0eWxlcyBzaG91bGQgYmUgaW4gOmhvc3Qgc2VsZWN0b3IuIE90aGVyd2lzZSBhbmd1bGFyIHdpbGwgYWRkIGhvc3RcbmF0dHJpYnV0ZSB0byBbZGlyPXJ0bF0gYXR0cmlidXRlIGFzIHdlbGwuXG5cblxuR2VuZXJhbCBwcm9ibGVtcy5cblxuTHRyIGlzIGRlZmF1bHQgZG9jdW1lbnQgZGlyZWN0aW9uLCBidXQgZm9yIHByb3BlciB3b3JrIG9mIG5iLWx0ciAobWVhbnMgbHRyIG9ubHkpLFxuW2Rpcj1sdHJdIHNob3VsZCBiZSBzcGVjaWZpZWQgYXQgbGVhc3Qgc29tZXdoZXJlLiAnOm5vdChbZGlyPXJ0bF0nIG5vdCBhcHBsaWNhYmxlIGhlcmUsXG5iZWNhdXNlIGl0J3Mgc2F0aXNmeSBhbnkgcGFyZW50LCB0aGF0IGRvbid0IGhhdmUgW2Rpcj1ydGxdIGF0dHJpYnV0ZS5cblByZXZpb3VzIGFwcHJvYWNoIHdhcyB0byB1c2Ugc2luZ2xlIHJ0bCBtaXhpbiBhbmQgcmVzZXQgbHRyIHByb3BlcnRpZXMgdG8gaW5pdGlhbCB2YWx1ZS5cbkJ1dCBzb21ldGltZXMgaXQncyBoYXJkIHRvIGZpbmQsIHdoYXQgdGhlIHByZXZpb3VzIHZhbHVlIHNob3VsZCBiZS4gQW5kIHN1Y2ggbWl4aW4gY2FsbCBsb29rcyB0b28gdmVyYm9zZS5cbiovXG4vKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG4vKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG4vKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG4vKipcbiAqIFRoaXMgbWl4aW4gZ2VuZXJhdGVzIGtleWZhbWVzLlxuICogQmVjYXVzZSBvZiBhbGwga2V5ZnJhbWVzIGNhbid0IGJlIHNjb3BlZCxcbiAqIHdlIG5lZWQgdG8gcHV0cyB1bmlxdWUgbmFtZSBpbiBlYWNoIGJ0bi1wdWxzZSBjYWxsLlxuICovXG4vKlxuXG5BY2NvcmRpbmcgdG8gdGhlIHNwZWNpZmljYXRpb24gKGh0dHBzOi8vd3d3LnczLm9yZy9UUi9jc3Mtc2NvcGluZy0xLyNob3N0LXNlbGVjdG9yKVxuOmhvc3QgYW5kIDpob3N0LWNvbnRleHQgYXJlIHBzZXVkby1jbGFzc2VzLiBTbyB3ZSBhc3N1bWUgdGhleSBjb3VsZCBiZSBjb21iaW5lZCxcbmxpa2Ugb3RoZXIgcHNldWRvLWNsYXNzZXMsIGV2ZW4gc2FtZSBvbmVzLlxuRm9yIGV4YW1wbGU6ICc6bnRoLW9mLXR5cGUoMm4pOm50aC1vZi10eXBlKGV2ZW4pJy5cblxuSWRlYWwgc29sdXRpb24gd291bGQgYmUgdG8gcHJlcGVuZCBhbnkgc2VsZWN0b3Igd2l0aCA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkuXG5UaGVuIG5lYnVsYXIgY29tcG9uZW50cyB3aWxsIGJlaGF2ZSBhcyBhbiBodG1sIGVsZW1lbnQgYW5kIHJlc3BvbmQgdG8gW2Rpcl0gYXR0cmlidXRlIG9uIGFueSBsZXZlbCxcbnNvIGRpcmVjdGlvbiBjb3VsZCBiZSBvdmVycmlkZGVuIG9uIGFueSBjb21wb25lbnQgbGV2ZWwuXG5cbkltcGxlbWVudGF0aW9uIGNvZGU6XG5cbkBtaXhpbiBuYi1ydGwoKSB7XG4gIC8vIGFkZCAjIHRvIHNjc3MgaW50ZXJwb2xhdGlvbiBzdGF0ZW1lbnQuXG4gIC8vIGl0IHdvcmtzIGluIGNvbW1lbnRzIGFuZCB3ZSBjYW4ndCB1c2UgaXQgaGVyZVxuICBAYXQtcm9vdCB7c2VsZWN0b3ItYXBwZW5kKCc6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSknLCAmKX0ge1xuICAgIEBjb250ZW50O1xuICB9XG59XG5cbkFuZCB3aGVuIHdlIGNhbGwgaXQgc29tZXdoZXJlOlxuXG46aG9zdCB7XG4gIC5zb21lLWNsYXNzIHtcbiAgICBAaW5jbHVkZSBuYi1ydGwoKSB7XG4gICAgICAuLi5cbiAgICB9XG4gIH1cbn1cbjpob3N0LWNvbnRleHQoLi4uKSB7XG4gIC5zb21lLWNsYXNzIHtcbiAgICBAaW5jbHVkZSBuYi1ydGwoKSB7XG4gICAgICAuLi5cbiAgICB9XG4gIH1cbn1cblxuUmVzdWx0IHdpbGwgbG9vayBsaWtlOlxuXG46aG9zdC1jb250ZXh0KFtkaXI9cnRsXSk6aG9zdCAuc29tZS1jbGFzcyB7XG4gIC4uLlxufVxuOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pOmhvc3QtY29udGV4dCguLi4pIC5zb21lLWNsYXNzIHtcbiAgLi4uXG59XG5cbipcbiAgU2lkZSBub3RlOlxuICA6aG9zdC1jb250ZXh0KCk6aG9zdCBzZWxlY3RvciBhcmUgdmFsaWQuIGh0dHBzOi8vbGlzdHMudzMub3JnL0FyY2hpdmVzL1B1YmxpYy93d3ctc3R5bGUvMjAxNUZlYi8wMzA1Lmh0bWxcblxuICA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSk6aG9zdC1jb250ZXh0KC4uLikgc2hvdWxkIG1hdGNoIGFueSBwZXJtdXRhdGlvbixcbiAgc28gb3JkZXIgaXMgbm90IGltcG9ydGFudC5cbipcblxuXG5DdXJyZW50bHksIHRoZXJlJ3JlIHR3byBwcm9ibGVtcyB3aXRoIHRoaXMgYXBwcm9hY2g6XG5cbkZpcnN0LCBpcyB0aGF0IHdlIGNhbid0IGNvbWJpbmUgOmhvc3QsIDpob3N0LWNvbnRleHQuIEFuZ3VsYXIgYnVncyAjMTQzNDksICMxOTE5OS5cbkZvciB0aGUgbW9tZW50IG9mIHdyaXRpbmcsIHRoZSBvbmx5IHBvc3NpYmxlIHdheSBpczpcbjpob3N0IHtcbiAgOmhvc3QtY29udGV4dCguLi4pIHtcbiAgICAuLi5cbiAgfVxufVxuSXQgZG9lc24ndCB3b3JrIGZvciB1cyBiZWNhdXNlIG1peGluIGNvdWxkIGJlIGNhbGxlZCBzb21ld2hlcmUgZGVlcGVyLCBsaWtlOlxuOmhvc3Qge1xuICBwIHtcbiAgICBAaW5jbHVkZSBuYi1ydGwoKSB7IC4uLiB9XG4gIH1cbn1cbldlIGFyZSBub3QgYWJsZSB0byBnbyB1cCB0byA6aG9zdCBsZXZlbCB0byBwbGFjZSBjb250ZW50IHBhc3NlZCB0byBtaXhpbi5cblxuVGhlIHNlY29uZCBwcm9ibGVtIGlzIHRoYXQgd2Ugb25seSBjYW4gYmUgc3VyZSB0aGF0IHdlIGFwcGVuZGluZyA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkgdG8gYW5vdGhlclxuOmhvc3QvOmhvc3QtY29udGV4dCBwc2V1ZG8tY2xhc3Mgd2hlbiBjYWxsZWQgaW4gdGhlbWUgZmlsZXMgKCoudGhlbWUuc2NzcykuXG4gICpcbiAgICBTaWRlIG5vdGU6XG4gICAgQ3VycmVudGx5LCBuYi1pbnN0YWxsLWNvbXBvbmVudCB1c2VzIGFub3RoZXIgYXBwcm9hY2ggd2hlcmUgOmhvc3QgcHJlcGVuZGVkIHdpdGggdGhlIHRoZW1lIG5hbWVcbiAgICAoaHR0cHM6Ly9naXRodWIuY29tL2FuZ3VsYXIvYW5ndWxhci9ibG9iLzViOTYwNzg2MjRiMGE0NzYwZjJkYmNmNmZkZjBiZDYyNzkxYmU1YmIvcGFja2FnZXMvY29tcGlsZXIvc3JjL3NoYWRvd19jc3MudHMjTDQ0MSksXG4gICAgYnV0IGl0IHdhcyBtYWRlIHRvIGJlIGFibGUgdG8gdXNlIGN1cnJlbnQgcmVhbGl6YXRpb24gb2YgcnRsIGFuZCBpdCBjYW4gYmUgcmV3cml0dGVuIGJhY2sgdG9cbiAgICA6aG9zdC1jb250ZXh0KCR0aGVtZSkgb25jZSB3ZSB3aWxsIGJlIGFibGUgdG8gdXNlIG11bHRpcGxlIHNoYWRvdyBzZWxlY3RvcnMuXG4gICpcbkJ1dCB3aGVuIGl0J3MgY2FsbGVkIGluICouY29tcG9uZW50LnNjc3Mgd2UgY2FuJ3QgYmUgc3VyZSwgdGhhdCBzZWxlY3RvciBzdGFydHMgd2l0aCA6aG9zdC86aG9zdC1jb250ZXh0LFxuYmVjYXVzZSBhbmd1bGFyIGFsbG93cyBvbWl0dGluZyBwc2V1ZG8tY2xhc3NlcyBpZiB3ZSBkb24ndCBuZWVkIHRvIHN0eWxlIDpob3N0IGNvbXBvbmVudCBpdHNlbGYuXG5XZSBjYW4gYnJlYWsgc3VjaCBzZWxlY3RvcnMsIGJ5IGp1c3QgYXBwZW5kaW5nIDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKSB0byB0aGVtLlxuICAqKipcbiAgICBQb3NzaWJsZSBzb2x1dGlvblxuICAgIGNoZWNrIGlmIHdlIGluIHRoZW1lIGJ5IHNvbWUgdGhlbWUgdmFyaWFibGVzIGFuZCBpZiBzbyBhcHBlbmQsIG90aGVyd2lzZSBuZXN0IGxpa2VcbiAgICBAYXQtcm9vdCA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkge1xuICAgICAgLy8gYWRkICMgdG8gc2NzcyBpbnRlcnBvbGF0aW9uIHN0YXRlbWVudC5cbiAgICAgIC8vIGl0IHdvcmtzIGluIGNvbW1lbnRzIGFuZCB3ZSBjYW4ndCB1c2UgaXQgaGVyZVxuICAgICAgeyZ9IHtcbiAgICAgICAgQGNvbnRlbnQ7XG4gICAgICB9XG4gICAgfVxuICAgIFdoYXQgaWYgOmhvc3Qgc3BlY2lmaWVkPyBDYW4gd2UgYWRkIHNwYWNlIGluIDpob3N0LWNvbnRleHQoLi4uKSA6aG9zdD9cbiAgICBPciBtYXliZSBhZGQgOmhvc3Qgc2VsZWN0b3IgYW55d2F5PyBJZiBtdWx0aXBsZSA6aG9zdCBzZWxlY3RvcnMgYXJlIGFsbG93ZWRcbiAgKioqXG5cblxuUHJvYmxlbXMgd2l0aCB0aGUgY3VycmVudCBhcHByb2FjaC5cblxuMS4gRGlyZWN0aW9uIGNhbiBiZSBhcHBsaWVkIG9ubHkgb24gZG9jdW1lbnQgbGV2ZWwsIGJlY2F1c2UgbWl4aW4gcHJlcGVuZHMgdGhlbWUgY2xhc3MsXG53aGljaCBwbGFjZWQgb24gdGhlIGJvZHkuXG4yLiAqLmNvbXBvbmVudC5zY3NzIHN0eWxlcyBzaG91bGQgYmUgaW4gOmhvc3Qgc2VsZWN0b3IuIE90aGVyd2lzZSBhbmd1bGFyIHdpbGwgYWRkIGhvc3RcbmF0dHJpYnV0ZSB0byBbZGlyPXJ0bF0gYXR0cmlidXRlIGFzIHdlbGwuXG5cblxuR2VuZXJhbCBwcm9ibGVtcy5cblxuTHRyIGlzIGRlZmF1bHQgZG9jdW1lbnQgZGlyZWN0aW9uLCBidXQgZm9yIHByb3BlciB3b3JrIG9mIG5iLWx0ciAobWVhbnMgbHRyIG9ubHkpLFxuW2Rpcj1sdHJdIHNob3VsZCBiZSBzcGVjaWZpZWQgYXQgbGVhc3Qgc29tZXdoZXJlLiAnOm5vdChbZGlyPXJ0bF0nIG5vdCBhcHBsaWNhYmxlIGhlcmUsXG5iZWNhdXNlIGl0J3Mgc2F0aXNmeSBhbnkgcGFyZW50LCB0aGF0IGRvbid0IGhhdmUgW2Rpcj1ydGxdIGF0dHJpYnV0ZS5cblByZXZpb3VzIGFwcHJvYWNoIHdhcyB0byB1c2Ugc2luZ2xlIHJ0bCBtaXhpbiBhbmQgcmVzZXQgbHRyIHByb3BlcnRpZXMgdG8gaW5pdGlhbCB2YWx1ZS5cbkJ1dCBzb21ldGltZXMgaXQncyBoYXJkIHRvIGZpbmQsIHdoYXQgdGhlIHByZXZpb3VzIHZhbHVlIHNob3VsZCBiZS4gQW5kIHN1Y2ggbWl4aW4gY2FsbCBsb29rcyB0b28gdmVyYm9zZS5cbiovXG4vKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG4vKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG4vKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG4vKipcbiAqIFRoaXMgbWl4aW4gZ2VuZXJhdGVzIGtleWZhbWVzLlxuICogQmVjYXVzZSBvZiBhbGwga2V5ZnJhbWVzIGNhbid0IGJlIHNjb3BlZCxcbiAqIHdlIG5lZWQgdG8gcHV0cyB1bmlxdWUgbmFtZSBpbiBlYWNoIGJ0bi1wdWxzZSBjYWxsLlxuICovXG4vKlxuXG5BY2NvcmRpbmcgdG8gdGhlIHNwZWNpZmljYXRpb24gKGh0dHBzOi8vd3d3LnczLm9yZy9UUi9jc3Mtc2NvcGluZy0xLyNob3N0LXNlbGVjdG9yKVxuOmhvc3QgYW5kIDpob3N0LWNvbnRleHQgYXJlIHBzZXVkby1jbGFzc2VzLiBTbyB3ZSBhc3N1bWUgdGhleSBjb3VsZCBiZSBjb21iaW5lZCxcbmxpa2Ugb3RoZXIgcHNldWRvLWNsYXNzZXMsIGV2ZW4gc2FtZSBvbmVzLlxuRm9yIGV4YW1wbGU6ICc6bnRoLW9mLXR5cGUoMm4pOm50aC1vZi10eXBlKGV2ZW4pJy5cblxuSWRlYWwgc29sdXRpb24gd291bGQgYmUgdG8gcHJlcGVuZCBhbnkgc2VsZWN0b3Igd2l0aCA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkuXG5UaGVuIG5lYnVsYXIgY29tcG9uZW50cyB3aWxsIGJlaGF2ZSBhcyBhbiBodG1sIGVsZW1lbnQgYW5kIHJlc3BvbmQgdG8gW2Rpcl0gYXR0cmlidXRlIG9uIGFueSBsZXZlbCxcbnNvIGRpcmVjdGlvbiBjb3VsZCBiZSBvdmVycmlkZGVuIG9uIGFueSBjb21wb25lbnQgbGV2ZWwuXG5cbkltcGxlbWVudGF0aW9uIGNvZGU6XG5cbkBtaXhpbiBuYi1ydGwoKSB7XG4gIC8vIGFkZCAjIHRvIHNjc3MgaW50ZXJwb2xhdGlvbiBzdGF0ZW1lbnQuXG4gIC8vIGl0IHdvcmtzIGluIGNvbW1lbnRzIGFuZCB3ZSBjYW4ndCB1c2UgaXQgaGVyZVxuICBAYXQtcm9vdCB7c2VsZWN0b3ItYXBwZW5kKCc6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSknLCAmKX0ge1xuICAgIEBjb250ZW50O1xuICB9XG59XG5cbkFuZCB3aGVuIHdlIGNhbGwgaXQgc29tZXdoZXJlOlxuXG46aG9zdCB7XG4gIC5zb21lLWNsYXNzIHtcbiAgICBAaW5jbHVkZSBuYi1ydGwoKSB7XG4gICAgICAuLi5cbiAgICB9XG4gIH1cbn1cbjpob3N0LWNvbnRleHQoLi4uKSB7XG4gIC5zb21lLWNsYXNzIHtcbiAgICBAaW5jbHVkZSBuYi1ydGwoKSB7XG4gICAgICAuLi5cbiAgICB9XG4gIH1cbn1cblxuUmVzdWx0IHdpbGwgbG9vayBsaWtlOlxuXG46aG9zdC1jb250ZXh0KFtkaXI9cnRsXSk6aG9zdCAuc29tZS1jbGFzcyB7XG4gIC4uLlxufVxuOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pOmhvc3QtY29udGV4dCguLi4pIC5zb21lLWNsYXNzIHtcbiAgLi4uXG59XG5cbipcbiAgU2lkZSBub3RlOlxuICA6aG9zdC1jb250ZXh0KCk6aG9zdCBzZWxlY3RvciBhcmUgdmFsaWQuIGh0dHBzOi8vbGlzdHMudzMub3JnL0FyY2hpdmVzL1B1YmxpYy93d3ctc3R5bGUvMjAxNUZlYi8wMzA1Lmh0bWxcblxuICA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSk6aG9zdC1jb250ZXh0KC4uLikgc2hvdWxkIG1hdGNoIGFueSBwZXJtdXRhdGlvbixcbiAgc28gb3JkZXIgaXMgbm90IGltcG9ydGFudC5cbipcblxuXG5DdXJyZW50bHksIHRoZXJlJ3JlIHR3byBwcm9ibGVtcyB3aXRoIHRoaXMgYXBwcm9hY2g6XG5cbkZpcnN0LCBpcyB0aGF0IHdlIGNhbid0IGNvbWJpbmUgOmhvc3QsIDpob3N0LWNvbnRleHQuIEFuZ3VsYXIgYnVncyAjMTQzNDksICMxOTE5OS5cbkZvciB0aGUgbW9tZW50IG9mIHdyaXRpbmcsIHRoZSBvbmx5IHBvc3NpYmxlIHdheSBpczpcbjpob3N0IHtcbiAgOmhvc3QtY29udGV4dCguLi4pIHtcbiAgICAuLi5cbiAgfVxufVxuSXQgZG9lc24ndCB3b3JrIGZvciB1cyBiZWNhdXNlIG1peGluIGNvdWxkIGJlIGNhbGxlZCBzb21ld2hlcmUgZGVlcGVyLCBsaWtlOlxuOmhvc3Qge1xuICBwIHtcbiAgICBAaW5jbHVkZSBuYi1ydGwoKSB7IC4uLiB9XG4gIH1cbn1cbldlIGFyZSBub3QgYWJsZSB0byBnbyB1cCB0byA6aG9zdCBsZXZlbCB0byBwbGFjZSBjb250ZW50IHBhc3NlZCB0byBtaXhpbi5cblxuVGhlIHNlY29uZCBwcm9ibGVtIGlzIHRoYXQgd2Ugb25seSBjYW4gYmUgc3VyZSB0aGF0IHdlIGFwcGVuZGluZyA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkgdG8gYW5vdGhlclxuOmhvc3QvOmhvc3QtY29udGV4dCBwc2V1ZG8tY2xhc3Mgd2hlbiBjYWxsZWQgaW4gdGhlbWUgZmlsZXMgKCoudGhlbWUuc2NzcykuXG4gICpcbiAgICBTaWRlIG5vdGU6XG4gICAgQ3VycmVudGx5LCBuYi1pbnN0YWxsLWNvbXBvbmVudCB1c2VzIGFub3RoZXIgYXBwcm9hY2ggd2hlcmUgOmhvc3QgcHJlcGVuZGVkIHdpdGggdGhlIHRoZW1lIG5hbWVcbiAgICAoaHR0cHM6Ly9naXRodWIuY29tL2FuZ3VsYXIvYW5ndWxhci9ibG9iLzViOTYwNzg2MjRiMGE0NzYwZjJkYmNmNmZkZjBiZDYyNzkxYmU1YmIvcGFja2FnZXMvY29tcGlsZXIvc3JjL3NoYWRvd19jc3MudHMjTDQ0MSksXG4gICAgYnV0IGl0IHdhcyBtYWRlIHRvIGJlIGFibGUgdG8gdXNlIGN1cnJlbnQgcmVhbGl6YXRpb24gb2YgcnRsIGFuZCBpdCBjYW4gYmUgcmV3cml0dGVuIGJhY2sgdG9cbiAgICA6aG9zdC1jb250ZXh0KCR0aGVtZSkgb25jZSB3ZSB3aWxsIGJlIGFibGUgdG8gdXNlIG11bHRpcGxlIHNoYWRvdyBzZWxlY3RvcnMuXG4gICpcbkJ1dCB3aGVuIGl0J3MgY2FsbGVkIGluICouY29tcG9uZW50LnNjc3Mgd2UgY2FuJ3QgYmUgc3VyZSwgdGhhdCBzZWxlY3RvciBzdGFydHMgd2l0aCA6aG9zdC86aG9zdC1jb250ZXh0LFxuYmVjYXVzZSBhbmd1bGFyIGFsbG93cyBvbWl0dGluZyBwc2V1ZG8tY2xhc3NlcyBpZiB3ZSBkb24ndCBuZWVkIHRvIHN0eWxlIDpob3N0IGNvbXBvbmVudCBpdHNlbGYuXG5XZSBjYW4gYnJlYWsgc3VjaCBzZWxlY3RvcnMsIGJ5IGp1c3QgYXBwZW5kaW5nIDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKSB0byB0aGVtLlxuICAqKipcbiAgICBQb3NzaWJsZSBzb2x1dGlvblxuICAgIGNoZWNrIGlmIHdlIGluIHRoZW1lIGJ5IHNvbWUgdGhlbWUgdmFyaWFibGVzIGFuZCBpZiBzbyBhcHBlbmQsIG90aGVyd2lzZSBuZXN0IGxpa2VcbiAgICBAYXQtcm9vdCA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkge1xuICAgICAgLy8gYWRkICMgdG8gc2NzcyBpbnRlcnBvbGF0aW9uIHN0YXRlbWVudC5cbiAgICAgIC8vIGl0IHdvcmtzIGluIGNvbW1lbnRzIGFuZCB3ZSBjYW4ndCB1c2UgaXQgaGVyZVxuICAgICAgeyZ9IHtcbiAgICAgICAgQGNvbnRlbnQ7XG4gICAgICB9XG4gICAgfVxuICAgIFdoYXQgaWYgOmhvc3Qgc3BlY2lmaWVkPyBDYW4gd2UgYWRkIHNwYWNlIGluIDpob3N0LWNvbnRleHQoLi4uKSA6aG9zdD9cbiAgICBPciBtYXliZSBhZGQgOmhvc3Qgc2VsZWN0b3IgYW55d2F5PyBJZiBtdWx0aXBsZSA6aG9zdCBzZWxlY3RvcnMgYXJlIGFsbG93ZWRcbiAgKioqXG5cblxuUHJvYmxlbXMgd2l0aCB0aGUgY3VycmVudCBhcHByb2FjaC5cblxuMS4gRGlyZWN0aW9uIGNhbiBiZSBhcHBsaWVkIG9ubHkgb24gZG9jdW1lbnQgbGV2ZWwsIGJlY2F1c2UgbWl4aW4gcHJlcGVuZHMgdGhlbWUgY2xhc3MsXG53aGljaCBwbGFjZWQgb24gdGhlIGJvZHkuXG4yLiAqLmNvbXBvbmVudC5zY3NzIHN0eWxlcyBzaG91bGQgYmUgaW4gOmhvc3Qgc2VsZWN0b3IuIE90aGVyd2lzZSBhbmd1bGFyIHdpbGwgYWRkIGhvc3RcbmF0dHJpYnV0ZSB0byBbZGlyPXJ0bF0gYXR0cmlidXRlIGFzIHdlbGwuXG5cblxuR2VuZXJhbCBwcm9ibGVtcy5cblxuTHRyIGlzIGRlZmF1bHQgZG9jdW1lbnQgZGlyZWN0aW9uLCBidXQgZm9yIHByb3BlciB3b3JrIG9mIG5iLWx0ciAobWVhbnMgbHRyIG9ubHkpLFxuW2Rpcj1sdHJdIHNob3VsZCBiZSBzcGVjaWZpZWQgYXQgbGVhc3Qgc29tZXdoZXJlLiAnOm5vdChbZGlyPXJ0bF0nIG5vdCBhcHBsaWNhYmxlIGhlcmUsXG5iZWNhdXNlIGl0J3Mgc2F0aXNmeSBhbnkgcGFyZW50LCB0aGF0IGRvbid0IGhhdmUgW2Rpcj1ydGxdIGF0dHJpYnV0ZS5cblByZXZpb3VzIGFwcHJvYWNoIHdhcyB0byB1c2Ugc2luZ2xlIHJ0bCBtaXhpbiBhbmQgcmVzZXQgbHRyIHByb3BlcnRpZXMgdG8gaW5pdGlhbCB2YWx1ZS5cbkJ1dCBzb21ldGltZXMgaXQncyBoYXJkIHRvIGZpbmQsIHdoYXQgdGhlIHByZXZpb3VzIHZhbHVlIHNob3VsZCBiZS4gQW5kIHN1Y2ggbWl4aW4gY2FsbCBsb29rcyB0b28gdmVyYm9zZS5cbiovXG4vKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG4vKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG4vKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG4vKipcbiAqIFRoaXMgbWl4aW4gZ2VuZXJhdGVzIGtleWZhbWVzLlxuICogQmVjYXVzZSBvZiBhbGwga2V5ZnJhbWVzIGNhbid0IGJlIHNjb3BlZCxcbiAqIHdlIG5lZWQgdG8gcHV0cyB1bmlxdWUgbmFtZSBpbiBlYWNoIGJ0bi1wdWxzZSBjYWxsLlxuICovXG4vKlxuXG5BY2NvcmRpbmcgdG8gdGhlIHNwZWNpZmljYXRpb24gKGh0dHBzOi8vd3d3LnczLm9yZy9UUi9jc3Mtc2NvcGluZy0xLyNob3N0LXNlbGVjdG9yKVxuOmhvc3QgYW5kIDpob3N0LWNvbnRleHQgYXJlIHBzZXVkby1jbGFzc2VzLiBTbyB3ZSBhc3N1bWUgdGhleSBjb3VsZCBiZSBjb21iaW5lZCxcbmxpa2Ugb3RoZXIgcHNldWRvLWNsYXNzZXMsIGV2ZW4gc2FtZSBvbmVzLlxuRm9yIGV4YW1wbGU6ICc6bnRoLW9mLXR5cGUoMm4pOm50aC1vZi10eXBlKGV2ZW4pJy5cblxuSWRlYWwgc29sdXRpb24gd291bGQgYmUgdG8gcHJlcGVuZCBhbnkgc2VsZWN0b3Igd2l0aCA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkuXG5UaGVuIG5lYnVsYXIgY29tcG9uZW50cyB3aWxsIGJlaGF2ZSBhcyBhbiBodG1sIGVsZW1lbnQgYW5kIHJlc3BvbmQgdG8gW2Rpcl0gYXR0cmlidXRlIG9uIGFueSBsZXZlbCxcbnNvIGRpcmVjdGlvbiBjb3VsZCBiZSBvdmVycmlkZGVuIG9uIGFueSBjb21wb25lbnQgbGV2ZWwuXG5cbkltcGxlbWVudGF0aW9uIGNvZGU6XG5cbkBtaXhpbiBuYi1ydGwoKSB7XG4gIC8vIGFkZCAjIHRvIHNjc3MgaW50ZXJwb2xhdGlvbiBzdGF0ZW1lbnQuXG4gIC8vIGl0IHdvcmtzIGluIGNvbW1lbnRzIGFuZCB3ZSBjYW4ndCB1c2UgaXQgaGVyZVxuICBAYXQtcm9vdCB7c2VsZWN0b3ItYXBwZW5kKCc6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSknLCAmKX0ge1xuICAgIEBjb250ZW50O1xuICB9XG59XG5cbkFuZCB3aGVuIHdlIGNhbGwgaXQgc29tZXdoZXJlOlxuXG46aG9zdCB7XG4gIC5zb21lLWNsYXNzIHtcbiAgICBAaW5jbHVkZSBuYi1ydGwoKSB7XG4gICAgICAuLi5cbiAgICB9XG4gIH1cbn1cbjpob3N0LWNvbnRleHQoLi4uKSB7XG4gIC5zb21lLWNsYXNzIHtcbiAgICBAaW5jbHVkZSBuYi1ydGwoKSB7XG4gICAgICAuLi5cbiAgICB9XG4gIH1cbn1cblxuUmVzdWx0IHdpbGwgbG9vayBsaWtlOlxuXG46aG9zdC1jb250ZXh0KFtkaXI9cnRsXSk6aG9zdCAuc29tZS1jbGFzcyB7XG4gIC4uLlxufVxuOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pOmhvc3QtY29udGV4dCguLi4pIC5zb21lLWNsYXNzIHtcbiAgLi4uXG59XG5cbipcbiAgU2lkZSBub3RlOlxuICA6aG9zdC1jb250ZXh0KCk6aG9zdCBzZWxlY3RvciBhcmUgdmFsaWQuIGh0dHBzOi8vbGlzdHMudzMub3JnL0FyY2hpdmVzL1B1YmxpYy93d3ctc3R5bGUvMjAxNUZlYi8wMzA1Lmh0bWxcblxuICA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSk6aG9zdC1jb250ZXh0KC4uLikgc2hvdWxkIG1hdGNoIGFueSBwZXJtdXRhdGlvbixcbiAgc28gb3JkZXIgaXMgbm90IGltcG9ydGFudC5cbipcblxuXG5DdXJyZW50bHksIHRoZXJlJ3JlIHR3byBwcm9ibGVtcyB3aXRoIHRoaXMgYXBwcm9hY2g6XG5cbkZpcnN0LCBpcyB0aGF0IHdlIGNhbid0IGNvbWJpbmUgOmhvc3QsIDpob3N0LWNvbnRleHQuIEFuZ3VsYXIgYnVncyAjMTQzNDksICMxOTE5OS5cbkZvciB0aGUgbW9tZW50IG9mIHdyaXRpbmcsIHRoZSBvbmx5IHBvc3NpYmxlIHdheSBpczpcbjpob3N0IHtcbiAgOmhvc3QtY29udGV4dCguLi4pIHtcbiAgICAuLi5cbiAgfVxufVxuSXQgZG9lc24ndCB3b3JrIGZvciB1cyBiZWNhdXNlIG1peGluIGNvdWxkIGJlIGNhbGxlZCBzb21ld2hlcmUgZGVlcGVyLCBsaWtlOlxuOmhvc3Qge1xuICBwIHtcbiAgICBAaW5jbHVkZSBuYi1ydGwoKSB7IC4uLiB9XG4gIH1cbn1cbldlIGFyZSBub3QgYWJsZSB0byBnbyB1cCB0byA6aG9zdCBsZXZlbCB0byBwbGFjZSBjb250ZW50IHBhc3NlZCB0byBtaXhpbi5cblxuVGhlIHNlY29uZCBwcm9ibGVtIGlzIHRoYXQgd2Ugb25seSBjYW4gYmUgc3VyZSB0aGF0IHdlIGFwcGVuZGluZyA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkgdG8gYW5vdGhlclxuOmhvc3QvOmhvc3QtY29udGV4dCBwc2V1ZG8tY2xhc3Mgd2hlbiBjYWxsZWQgaW4gdGhlbWUgZmlsZXMgKCoudGhlbWUuc2NzcykuXG4gICpcbiAgICBTaWRlIG5vdGU6XG4gICAgQ3VycmVudGx5LCBuYi1pbnN0YWxsLWNvbXBvbmVudCB1c2VzIGFub3RoZXIgYXBwcm9hY2ggd2hlcmUgOmhvc3QgcHJlcGVuZGVkIHdpdGggdGhlIHRoZW1lIG5hbWVcbiAgICAoaHR0cHM6Ly9naXRodWIuY29tL2FuZ3VsYXIvYW5ndWxhci9ibG9iLzViOTYwNzg2MjRiMGE0NzYwZjJkYmNmNmZkZjBiZDYyNzkxYmU1YmIvcGFja2FnZXMvY29tcGlsZXIvc3JjL3NoYWRvd19jc3MudHMjTDQ0MSksXG4gICAgYnV0IGl0IHdhcyBtYWRlIHRvIGJlIGFibGUgdG8gdXNlIGN1cnJlbnQgcmVhbGl6YXRpb24gb2YgcnRsIGFuZCBpdCBjYW4gYmUgcmV3cml0dGVuIGJhY2sgdG9cbiAgICA6aG9zdC1jb250ZXh0KCR0aGVtZSkgb25jZSB3ZSB3aWxsIGJlIGFibGUgdG8gdXNlIG11bHRpcGxlIHNoYWRvdyBzZWxlY3RvcnMuXG4gICpcbkJ1dCB3aGVuIGl0J3MgY2FsbGVkIGluICouY29tcG9uZW50LnNjc3Mgd2UgY2FuJ3QgYmUgc3VyZSwgdGhhdCBzZWxlY3RvciBzdGFydHMgd2l0aCA6aG9zdC86aG9zdC1jb250ZXh0LFxuYmVjYXVzZSBhbmd1bGFyIGFsbG93cyBvbWl0dGluZyBwc2V1ZG8tY2xhc3NlcyBpZiB3ZSBkb24ndCBuZWVkIHRvIHN0eWxlIDpob3N0IGNvbXBvbmVudCBpdHNlbGYuXG5XZSBjYW4gYnJlYWsgc3VjaCBzZWxlY3RvcnMsIGJ5IGp1c3QgYXBwZW5kaW5nIDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKSB0byB0aGVtLlxuICAqKipcbiAgICBQb3NzaWJsZSBzb2x1dGlvblxuICAgIGNoZWNrIGlmIHdlIGluIHRoZW1lIGJ5IHNvbWUgdGhlbWUgdmFyaWFibGVzIGFuZCBpZiBzbyBhcHBlbmQsIG90aGVyd2lzZSBuZXN0IGxpa2VcbiAgICBAYXQtcm9vdCA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkge1xuICAgICAgLy8gYWRkICMgdG8gc2NzcyBpbnRlcnBvbGF0aW9uIHN0YXRlbWVudC5cbiAgICAgIC8vIGl0IHdvcmtzIGluIGNvbW1lbnRzIGFuZCB3ZSBjYW4ndCB1c2UgaXQgaGVyZVxuICAgICAgeyZ9IHtcbiAgICAgICAgQGNvbnRlbnQ7XG4gICAgICB9XG4gICAgfVxuICAgIFdoYXQgaWYgOmhvc3Qgc3BlY2lmaWVkPyBDYW4gd2UgYWRkIHNwYWNlIGluIDpob3N0LWNvbnRleHQoLi4uKSA6aG9zdD9cbiAgICBPciBtYXliZSBhZGQgOmhvc3Qgc2VsZWN0b3IgYW55d2F5PyBJZiBtdWx0aXBsZSA6aG9zdCBzZWxlY3RvcnMgYXJlIGFsbG93ZWRcbiAgKioqXG5cblxuUHJvYmxlbXMgd2l0aCB0aGUgY3VycmVudCBhcHByb2FjaC5cblxuMS4gRGlyZWN0aW9uIGNhbiBiZSBhcHBsaWVkIG9ubHkgb24gZG9jdW1lbnQgbGV2ZWwsIGJlY2F1c2UgbWl4aW4gcHJlcGVuZHMgdGhlbWUgY2xhc3MsXG53aGljaCBwbGFjZWQgb24gdGhlIGJvZHkuXG4yLiAqLmNvbXBvbmVudC5zY3NzIHN0eWxlcyBzaG91bGQgYmUgaW4gOmhvc3Qgc2VsZWN0b3IuIE90aGVyd2lzZSBhbmd1bGFyIHdpbGwgYWRkIGhvc3RcbmF0dHJpYnV0ZSB0byBbZGlyPXJ0bF0gYXR0cmlidXRlIGFzIHdlbGwuXG5cblxuR2VuZXJhbCBwcm9ibGVtcy5cblxuTHRyIGlzIGRlZmF1bHQgZG9jdW1lbnQgZGlyZWN0aW9uLCBidXQgZm9yIHByb3BlciB3b3JrIG9mIG5iLWx0ciAobWVhbnMgbHRyIG9ubHkpLFxuW2Rpcj1sdHJdIHNob3VsZCBiZSBzcGVjaWZpZWQgYXQgbGVhc3Qgc29tZXdoZXJlLiAnOm5vdChbZGlyPXJ0bF0nIG5vdCBhcHBsaWNhYmxlIGhlcmUsXG5iZWNhdXNlIGl0J3Mgc2F0aXNmeSBhbnkgcGFyZW50LCB0aGF0IGRvbid0IGhhdmUgW2Rpcj1ydGxdIGF0dHJpYnV0ZS5cblByZXZpb3VzIGFwcHJvYWNoIHdhcyB0byB1c2Ugc2luZ2xlIHJ0bCBtaXhpbiBhbmQgcmVzZXQgbHRyIHByb3BlcnRpZXMgdG8gaW5pdGlhbCB2YWx1ZS5cbkJ1dCBzb21ldGltZXMgaXQncyBoYXJkIHRvIGZpbmQsIHdoYXQgdGhlIHByZXZpb3VzIHZhbHVlIHNob3VsZCBiZS4gQW5kIHN1Y2ggbWl4aW4gY2FsbCBsb29rcyB0b28gdmVyYm9zZS5cbiovXG4vKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG4vKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG4vKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG4vKipcbiAqIFRoaXMgbWl4aW4gZ2VuZXJhdGVzIGtleWZhbWVzLlxuICogQmVjYXVzZSBvZiBhbGwga2V5ZnJhbWVzIGNhbid0IGJlIHNjb3BlZCxcbiAqIHdlIG5lZWQgdG8gcHV0cyB1bmlxdWUgbmFtZSBpbiBlYWNoIGJ0bi1wdWxzZSBjYWxsLlxuICovXG4vKlxuXG5BY2NvcmRpbmcgdG8gdGhlIHNwZWNpZmljYXRpb24gKGh0dHBzOi8vd3d3LnczLm9yZy9UUi9jc3Mtc2NvcGluZy0xLyNob3N0LXNlbGVjdG9yKVxuOmhvc3QgYW5kIDpob3N0LWNvbnRleHQgYXJlIHBzZXVkby1jbGFzc2VzLiBTbyB3ZSBhc3N1bWUgdGhleSBjb3VsZCBiZSBjb21iaW5lZCxcbmxpa2Ugb3RoZXIgcHNldWRvLWNsYXNzZXMsIGV2ZW4gc2FtZSBvbmVzLlxuRm9yIGV4YW1wbGU6ICc6bnRoLW9mLXR5cGUoMm4pOm50aC1vZi10eXBlKGV2ZW4pJy5cblxuSWRlYWwgc29sdXRpb24gd291bGQgYmUgdG8gcHJlcGVuZCBhbnkgc2VsZWN0b3Igd2l0aCA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkuXG5UaGVuIG5lYnVsYXIgY29tcG9uZW50cyB3aWxsIGJlaGF2ZSBhcyBhbiBodG1sIGVsZW1lbnQgYW5kIHJlc3BvbmQgdG8gW2Rpcl0gYXR0cmlidXRlIG9uIGFueSBsZXZlbCxcbnNvIGRpcmVjdGlvbiBjb3VsZCBiZSBvdmVycmlkZGVuIG9uIGFueSBjb21wb25lbnQgbGV2ZWwuXG5cbkltcGxlbWVudGF0aW9uIGNvZGU6XG5cbkBtaXhpbiBuYi1ydGwoKSB7XG4gIC8vIGFkZCAjIHRvIHNjc3MgaW50ZXJwb2xhdGlvbiBzdGF0ZW1lbnQuXG4gIC8vIGl0IHdvcmtzIGluIGNvbW1lbnRzIGFuZCB3ZSBjYW4ndCB1c2UgaXQgaGVyZVxuICBAYXQtcm9vdCB7c2VsZWN0b3ItYXBwZW5kKCc6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSknLCAmKX0ge1xuICAgIEBjb250ZW50O1xuICB9XG59XG5cbkFuZCB3aGVuIHdlIGNhbGwgaXQgc29tZXdoZXJlOlxuXG46aG9zdCB7XG4gIC5zb21lLWNsYXNzIHtcbiAgICBAaW5jbHVkZSBuYi1ydGwoKSB7XG4gICAgICAuLi5cbiAgICB9XG4gIH1cbn1cbjpob3N0LWNvbnRleHQoLi4uKSB7XG4gIC5zb21lLWNsYXNzIHtcbiAgICBAaW5jbHVkZSBuYi1ydGwoKSB7XG4gICAgICAuLi5cbiAgICB9XG4gIH1cbn1cblxuUmVzdWx0IHdpbGwgbG9vayBsaWtlOlxuXG46aG9zdC1jb250ZXh0KFtkaXI9cnRsXSk6aG9zdCAuc29tZS1jbGFzcyB7XG4gIC4uLlxufVxuOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pOmhvc3QtY29udGV4dCguLi4pIC5zb21lLWNsYXNzIHtcbiAgLi4uXG59XG5cbipcbiAgU2lkZSBub3RlOlxuICA6aG9zdC1jb250ZXh0KCk6aG9zdCBzZWxlY3RvciBhcmUgdmFsaWQuIGh0dHBzOi8vbGlzdHMudzMub3JnL0FyY2hpdmVzL1B1YmxpYy93d3ctc3R5bGUvMjAxNUZlYi8wMzA1Lmh0bWxcblxuICA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSk6aG9zdC1jb250ZXh0KC4uLikgc2hvdWxkIG1hdGNoIGFueSBwZXJtdXRhdGlvbixcbiAgc28gb3JkZXIgaXMgbm90IGltcG9ydGFudC5cbipcblxuXG5DdXJyZW50bHksIHRoZXJlJ3JlIHR3byBwcm9ibGVtcyB3aXRoIHRoaXMgYXBwcm9hY2g6XG5cbkZpcnN0LCBpcyB0aGF0IHdlIGNhbid0IGNvbWJpbmUgOmhvc3QsIDpob3N0LWNvbnRleHQuIEFuZ3VsYXIgYnVncyAjMTQzNDksICMxOTE5OS5cbkZvciB0aGUgbW9tZW50IG9mIHdyaXRpbmcsIHRoZSBvbmx5IHBvc3NpYmxlIHdheSBpczpcbjpob3N0IHtcbiAgOmhvc3QtY29udGV4dCguLi4pIHtcbiAgICAuLi5cbiAgfVxufVxuSXQgZG9lc24ndCB3b3JrIGZvciB1cyBiZWNhdXNlIG1peGluIGNvdWxkIGJlIGNhbGxlZCBzb21ld2hlcmUgZGVlcGVyLCBsaWtlOlxuOmhvc3Qge1xuICBwIHtcbiAgICBAaW5jbHVkZSBuYi1ydGwoKSB7IC4uLiB9XG4gIH1cbn1cbldlIGFyZSBub3QgYWJsZSB0byBnbyB1cCB0byA6aG9zdCBsZXZlbCB0byBwbGFjZSBjb250ZW50IHBhc3NlZCB0byBtaXhpbi5cblxuVGhlIHNlY29uZCBwcm9ibGVtIGlzIHRoYXQgd2Ugb25seSBjYW4gYmUgc3VyZSB0aGF0IHdlIGFwcGVuZGluZyA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkgdG8gYW5vdGhlclxuOmhvc3QvOmhvc3QtY29udGV4dCBwc2V1ZG8tY2xhc3Mgd2hlbiBjYWxsZWQgaW4gdGhlbWUgZmlsZXMgKCoudGhlbWUuc2NzcykuXG4gICpcbiAgICBTaWRlIG5vdGU6XG4gICAgQ3VycmVudGx5LCBuYi1pbnN0YWxsLWNvbXBvbmVudCB1c2VzIGFub3RoZXIgYXBwcm9hY2ggd2hlcmUgOmhvc3QgcHJlcGVuZGVkIHdpdGggdGhlIHRoZW1lIG5hbWVcbiAgICAoaHR0cHM6Ly9naXRodWIuY29tL2FuZ3VsYXIvYW5ndWxhci9ibG9iLzViOTYwNzg2MjRiMGE0NzYwZjJkYmNmNmZkZjBiZDYyNzkxYmU1YmIvcGFja2FnZXMvY29tcGlsZXIvc3JjL3NoYWRvd19jc3MudHMjTDQ0MSksXG4gICAgYnV0IGl0IHdhcyBtYWRlIHRvIGJlIGFibGUgdG8gdXNlIGN1cnJlbnQgcmVhbGl6YXRpb24gb2YgcnRsIGFuZCBpdCBjYW4gYmUgcmV3cml0dGVuIGJhY2sgdG9cbiAgICA6aG9zdC1jb250ZXh0KCR0aGVtZSkgb25jZSB3ZSB3aWxsIGJlIGFibGUgdG8gdXNlIG11bHRpcGxlIHNoYWRvdyBzZWxlY3RvcnMuXG4gICpcbkJ1dCB3aGVuIGl0J3MgY2FsbGVkIGluICouY29tcG9uZW50LnNjc3Mgd2UgY2FuJ3QgYmUgc3VyZSwgdGhhdCBzZWxlY3RvciBzdGFydHMgd2l0aCA6aG9zdC86aG9zdC1jb250ZXh0LFxuYmVjYXVzZSBhbmd1bGFyIGFsbG93cyBvbWl0dGluZyBwc2V1ZG8tY2xhc3NlcyBpZiB3ZSBkb24ndCBuZWVkIHRvIHN0eWxlIDpob3N0IGNvbXBvbmVudCBpdHNlbGYuXG5XZSBjYW4gYnJlYWsgc3VjaCBzZWxlY3RvcnMsIGJ5IGp1c3QgYXBwZW5kaW5nIDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKSB0byB0aGVtLlxuICAqKipcbiAgICBQb3NzaWJsZSBzb2x1dGlvblxuICAgIGNoZWNrIGlmIHdlIGluIHRoZW1lIGJ5IHNvbWUgdGhlbWUgdmFyaWFibGVzIGFuZCBpZiBzbyBhcHBlbmQsIG90aGVyd2lzZSBuZXN0IGxpa2VcbiAgICBAYXQtcm9vdCA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkge1xuICAgICAgLy8gYWRkICMgdG8gc2NzcyBpbnRlcnBvbGF0aW9uIHN0YXRlbWVudC5cbiAgICAgIC8vIGl0IHdvcmtzIGluIGNvbW1lbnRzIGFuZCB3ZSBjYW4ndCB1c2UgaXQgaGVyZVxuICAgICAgeyZ9IHtcbiAgICAgICAgQGNvbnRlbnQ7XG4gICAgICB9XG4gICAgfVxuICAgIFdoYXQgaWYgOmhvc3Qgc3BlY2lmaWVkPyBDYW4gd2UgYWRkIHNwYWNlIGluIDpob3N0LWNvbnRleHQoLi4uKSA6aG9zdD9cbiAgICBPciBtYXliZSBhZGQgOmhvc3Qgc2VsZWN0b3IgYW55d2F5PyBJZiBtdWx0aXBsZSA6aG9zdCBzZWxlY3RvcnMgYXJlIGFsbG93ZWRcbiAgKioqXG5cblxuUHJvYmxlbXMgd2l0aCB0aGUgY3VycmVudCBhcHByb2FjaC5cblxuMS4gRGlyZWN0aW9uIGNhbiBiZSBhcHBsaWVkIG9ubHkgb24gZG9jdW1lbnQgbGV2ZWwsIGJlY2F1c2UgbWl4aW4gcHJlcGVuZHMgdGhlbWUgY2xhc3MsXG53aGljaCBwbGFjZWQgb24gdGhlIGJvZHkuXG4yLiAqLmNvbXBvbmVudC5zY3NzIHN0eWxlcyBzaG91bGQgYmUgaW4gOmhvc3Qgc2VsZWN0b3IuIE90aGVyd2lzZSBhbmd1bGFyIHdpbGwgYWRkIGhvc3RcbmF0dHJpYnV0ZSB0byBbZGlyPXJ0bF0gYXR0cmlidXRlIGFzIHdlbGwuXG5cblxuR2VuZXJhbCBwcm9ibGVtcy5cblxuTHRyIGlzIGRlZmF1bHQgZG9jdW1lbnQgZGlyZWN0aW9uLCBidXQgZm9yIHByb3BlciB3b3JrIG9mIG5iLWx0ciAobWVhbnMgbHRyIG9ubHkpLFxuW2Rpcj1sdHJdIHNob3VsZCBiZSBzcGVjaWZpZWQgYXQgbGVhc3Qgc29tZXdoZXJlLiAnOm5vdChbZGlyPXJ0bF0nIG5vdCBhcHBsaWNhYmxlIGhlcmUsXG5iZWNhdXNlIGl0J3Mgc2F0aXNmeSBhbnkgcGFyZW50LCB0aGF0IGRvbid0IGhhdmUgW2Rpcj1ydGxdIGF0dHJpYnV0ZS5cblByZXZpb3VzIGFwcHJvYWNoIHdhcyB0byB1c2Ugc2luZ2xlIHJ0bCBtaXhpbiBhbmQgcmVzZXQgbHRyIHByb3BlcnRpZXMgdG8gaW5pdGlhbCB2YWx1ZS5cbkJ1dCBzb21ldGltZXMgaXQncyBoYXJkIHRvIGZpbmQsIHdoYXQgdGhlIHByZXZpb3VzIHZhbHVlIHNob3VsZCBiZS4gQW5kIHN1Y2ggbWl4aW4gY2FsbCBsb29rcyB0b28gdmVyYm9zZS5cbiovXG4vKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG4vKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG4vKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG4vKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG4vKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG4vKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG4vKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG4vKlxuICAgICAgOmhvc3QgY2FuIGJlIHByZWZpeGVkXG4gICAgICBodHRwczovL2dpdGh1Yi5jb20vYW5ndWxhci9hbmd1bGFyL2Jsb2IvOGQwZWUzNDkzOWYxNGMwNzg3NmQyMjJjMjViNDA1ZWQ0NThhMzRkMy9wYWNrYWdlcy9jb21waWxlci9zcmMvc2hhZG93X2Nzcy50cyNMNDQxXG5cbiAgICAgIFdlIGhhdmUgdG8gdXNlIDpob3N0IGluc3RlZCBvZiA6aG9zdC1jb250ZXh0KCR0aGVtZSksIHRvIGJlIGFibGUgdG8gcHJlZml4IHRoZW1lIGNsYXNzXG4gICAgICB3aXRoIHNvbWV0aGluZyBkZWZpbmVkIGluc2lkZSBvZiBAY29udGVudCwgYnkgcHJlZml4aW5nICYuXG4gICAgICBGb3IgZXhhbXBsZSB0aGlzIHNjc3MgY29kZTpcbiAgICAgICAgLm5iLXRoZW1lLWRlZmF1bHQge1xuICAgICAgICAgIC5zb21lLXNlbGVjdG9yICYge1xuICAgICAgICAgICAgLi4uXG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICBXaWxsIHJlc3VsdCBpbiBuZXh0IGNzczpcbiAgICAgICAgLnNvbWUtc2VsZWN0b3IgLm5iLXRoZW1lLWRlZmF1bHQge1xuICAgICAgICAgIC4uLlxuICAgICAgICB9XG5cbiAgICAgIEl0IGRvZXNuJ3Qgd29yayB3aXRoIDpob3N0LWNvbnRleHQgYmVjYXVzZSBhbmd1bGFyIHNwbGl0dGluZyBpdCBpbiB0d28gc2VsZWN0b3JzIGFuZCByZW1vdmVzXG4gICAgICBwcmVmaXggaW4gb25lIG9mIHRoZSBzZWxlY3RvcnMuXG4gICAgKi9cbi5uYi10aGVtZS1kZWZhdWx0IDpob3N0IHtcbiAgLyogbmItY2FyZC1ib2R5IHtcbiAgICBwYWRkaW5nOiAwIDEuMjVyZW0gMS4yNXJlbSAwO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC13cmFwOiB3cmFwO1xuICB9ICovIH1cbiAgLm5iLXRoZW1lLWRlZmF1bHQgOmhvc3QgLmJ0biB7XG4gICAgbGluZS1oZWlnaHQ6IDE0cHggIWltcG9ydGFudDsgfVxuICAubmItdGhlbWUtZGVmYXVsdCA6aG9zdCAuY29udGFpbmVyLXRpdGxlIHtcbiAgICBtYXJnaW4tYm90dG9tOiAwLjI1cmVtOyB9XG4gIC5uYi10aGVtZS1kZWZhdWx0IDpob3N0IC5zaXplLWNvbnRhaW5lciB7XG4gICAgbWFyZ2luOiAxLjI1cmVtIDAgMCAxLjI1cmVtOyB9XG4gIC5uYi10aGVtZS1kZWZhdWx0IDpob3N0IC5zdWJoZWFkZXIge1xuICAgIG1hcmdpbi1ib3R0b206IDAuNzVyZW07XG4gICAgZm9udC1zaXplOiAwLjg3NXJlbTtcbiAgICBmb250LXdlaWdodDogNTAwO1xuICAgIGNvbG9yOiAjMmEyYTJhOyB9XG4gIC5uYi10aGVtZS1kZWZhdWx0IDpob3N0IGJ1dHRvbiB7XG4gICAgZmxvYXQ6IHJpZ2h0OyB9XG4gIC5uYi10aGVtZS1kZWZhdWx0IDpob3N0IGkge1xuICAgIGN1cnNvcjogcG9pbnRlcjsgfVxuICAubmItdGhlbWUtZGVmYXVsdCA6aG9zdCBuYi1jYXJkLWJvZHkge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNFMUY1RkU7XG4gICAgY29sb3I6IHdoaXRlOyB9XG4gIC5uYi10aGVtZS1kZWZhdWx0IDpob3N0IGNvbC14bC02LFxuICAubmItdGhlbWUtZGVmYXVsdCA6aG9zdCBuYi1jYXJkLWhlYWRlciB7XG4gICAgYmFja2dyb3VuZDogIzFEOEVDRTtcbiAgICBjb2xvcjogd2hpdGU7IH1cblxuLypcbiAgICAgIDpob3N0IGNhbiBiZSBwcmVmaXhlZFxuICAgICAgaHR0cHM6Ly9naXRodWIuY29tL2FuZ3VsYXIvYW5ndWxhci9ibG9iLzhkMGVlMzQ5MzlmMTRjMDc4NzZkMjIyYzI1YjQwNWVkNDU4YTM0ZDMvcGFja2FnZXMvY29tcGlsZXIvc3JjL3NoYWRvd19jc3MudHMjTDQ0MVxuXG4gICAgICBXZSBoYXZlIHRvIHVzZSA6aG9zdCBpbnN0ZWQgb2YgOmhvc3QtY29udGV4dCgkdGhlbWUpLCB0byBiZSBhYmxlIHRvIHByZWZpeCB0aGVtZSBjbGFzc1xuICAgICAgd2l0aCBzb21ldGhpbmcgZGVmaW5lZCBpbnNpZGUgb2YgQGNvbnRlbnQsIGJ5IHByZWZpeGluZyAmLlxuICAgICAgRm9yIGV4YW1wbGUgdGhpcyBzY3NzIGNvZGU6XG4gICAgICAgIC5uYi10aGVtZS1kZWZhdWx0IHtcbiAgICAgICAgICAuc29tZS1zZWxlY3RvciAmIHtcbiAgICAgICAgICAgIC4uLlxuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgV2lsbCByZXN1bHQgaW4gbmV4dCBjc3M6XG4gICAgICAgIC5zb21lLXNlbGVjdG9yIC5uYi10aGVtZS1kZWZhdWx0IHtcbiAgICAgICAgICAuLi5cbiAgICAgICAgfVxuXG4gICAgICBJdCBkb2Vzbid0IHdvcmsgd2l0aCA6aG9zdC1jb250ZXh0IGJlY2F1c2UgYW5ndWxhciBzcGxpdHRpbmcgaXQgaW4gdHdvIHNlbGVjdG9ycyBhbmQgcmVtb3Zlc1xuICAgICAgcHJlZml4IGluIG9uZSBvZiB0aGUgc2VsZWN0b3JzLlxuICAgICovXG4ubmItdGhlbWUtY29zbWljIDpob3N0IHtcbiAgLyogbmItY2FyZC1ib2R5IHtcbiAgICBwYWRkaW5nOiAwIDEuMjVyZW0gMS4yNXJlbSAwO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC13cmFwOiB3cmFwO1xuICB9ICovIH1cbiAgLm5iLXRoZW1lLWNvc21pYyA6aG9zdCAuYnRuIHtcbiAgICBsaW5lLWhlaWdodDogMTRweCAhaW1wb3J0YW50OyB9XG4gIC5uYi10aGVtZS1jb3NtaWMgOmhvc3QgLmNvbnRhaW5lci10aXRsZSB7XG4gICAgbWFyZ2luLWJvdHRvbTogMC4yNXJlbTsgfVxuICAubmItdGhlbWUtY29zbWljIDpob3N0IC5zaXplLWNvbnRhaW5lciB7XG4gICAgbWFyZ2luOiAxLjI1cmVtIDAgMCAxLjI1cmVtOyB9XG4gIC5uYi10aGVtZS1jb3NtaWMgOmhvc3QgLnN1YmhlYWRlciB7XG4gICAgbWFyZ2luLWJvdHRvbTogMC43NXJlbTtcbiAgICBmb250LXNpemU6IDAuODc1cmVtO1xuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgY29sb3I6ICNmZmZmZmY7IH1cbiAgLm5iLXRoZW1lLWNvc21pYyA6aG9zdCBidXR0b24ge1xuICAgIGZsb2F0OiByaWdodDsgfVxuICAubmItdGhlbWUtY29zbWljIDpob3N0IGkge1xuICAgIGN1cnNvcjogcG9pbnRlcjsgfVxuICAubmItdGhlbWUtY29zbWljIDpob3N0IG5iLWNhcmQtYm9keSB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0UxRjVGRTtcbiAgICBjb2xvcjogd2hpdGU7IH1cbiAgLm5iLXRoZW1lLWNvc21pYyA6aG9zdCBjb2wteGwtNixcbiAgLm5iLXRoZW1lLWNvc21pYyA6aG9zdCBuYi1jYXJkLWhlYWRlciB7XG4gICAgYmFja2dyb3VuZDogIzFEOEVDRTtcbiAgICBjb2xvcjogd2hpdGU7IH1cblxuLypcbiAgICAgIDpob3N0IGNhbiBiZSBwcmVmaXhlZFxuICAgICAgaHR0cHM6Ly9naXRodWIuY29tL2FuZ3VsYXIvYW5ndWxhci9ibG9iLzhkMGVlMzQ5MzlmMTRjMDc4NzZkMjIyYzI1YjQwNWVkNDU4YTM0ZDMvcGFja2FnZXMvY29tcGlsZXIvc3JjL3NoYWRvd19jc3MudHMjTDQ0MVxuXG4gICAgICBXZSBoYXZlIHRvIHVzZSA6aG9zdCBpbnN0ZWQgb2YgOmhvc3QtY29udGV4dCgkdGhlbWUpLCB0byBiZSBhYmxlIHRvIHByZWZpeCB0aGVtZSBjbGFzc1xuICAgICAgd2l0aCBzb21ldGhpbmcgZGVmaW5lZCBpbnNpZGUgb2YgQGNvbnRlbnQsIGJ5IHByZWZpeGluZyAmLlxuICAgICAgRm9yIGV4YW1wbGUgdGhpcyBzY3NzIGNvZGU6XG4gICAgICAgIC5uYi10aGVtZS1kZWZhdWx0IHtcbiAgICAgICAgICAuc29tZS1zZWxlY3RvciAmIHtcbiAgICAgICAgICAgIC4uLlxuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgV2lsbCByZXN1bHQgaW4gbmV4dCBjc3M6XG4gICAgICAgIC5zb21lLXNlbGVjdG9yIC5uYi10aGVtZS1kZWZhdWx0IHtcbiAgICAgICAgICAuLi5cbiAgICAgICAgfVxuXG4gICAgICBJdCBkb2Vzbid0IHdvcmsgd2l0aCA6aG9zdC1jb250ZXh0IGJlY2F1c2UgYW5ndWxhciBzcGxpdHRpbmcgaXQgaW4gdHdvIHNlbGVjdG9ycyBhbmQgcmVtb3Zlc1xuICAgICAgcHJlZml4IGluIG9uZSBvZiB0aGUgc2VsZWN0b3JzLlxuICAgICovXG4ubmItdGhlbWUtY29ycG9yYXRlIDpob3N0IHtcbiAgLyogbmItY2FyZC1ib2R5IHtcbiAgICBwYWRkaW5nOiAwIDEuMjVyZW0gMS4yNXJlbSAwO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC13cmFwOiB3cmFwO1xuICB9ICovIH1cbiAgLm5iLXRoZW1lLWNvcnBvcmF0ZSA6aG9zdCAuYnRuIHtcbiAgICBsaW5lLWhlaWdodDogMTRweCAhaW1wb3J0YW50OyB9XG4gIC5uYi10aGVtZS1jb3Jwb3JhdGUgOmhvc3QgLmNvbnRhaW5lci10aXRsZSB7XG4gICAgbWFyZ2luLWJvdHRvbTogMC4yNXJlbTsgfVxuICAubmItdGhlbWUtY29ycG9yYXRlIDpob3N0IC5zaXplLWNvbnRhaW5lciB7XG4gICAgbWFyZ2luOiAxLjI1cmVtIDAgMCAxLjI1cmVtOyB9XG4gIC5uYi10aGVtZS1jb3Jwb3JhdGUgOmhvc3QgLnN1YmhlYWRlciB7XG4gICAgbWFyZ2luLWJvdHRvbTogMC43NXJlbTtcbiAgICBmb250LXNpemU6IDAuODc1cmVtO1xuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgY29sb3I6ICMxODE4MTg7IH1cbiAgLm5iLXRoZW1lLWNvcnBvcmF0ZSA6aG9zdCBidXR0b24ge1xuICAgIGZsb2F0OiByaWdodDsgfVxuICAubmItdGhlbWUtY29ycG9yYXRlIDpob3N0IGkge1xuICAgIGN1cnNvcjogcG9pbnRlcjsgfVxuICAubmItdGhlbWUtY29ycG9yYXRlIDpob3N0IG5iLWNhcmQtYm9keSB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0UxRjVGRTtcbiAgICBjb2xvcjogd2hpdGU7IH1cbiAgLm5iLXRoZW1lLWNvcnBvcmF0ZSA6aG9zdCBjb2wteGwtNixcbiAgLm5iLXRoZW1lLWNvcnBvcmF0ZSA6aG9zdCBuYi1jYXJkLWhlYWRlciB7XG4gICAgYmFja2dyb3VuZDogIzFEOEVDRTtcbiAgICBjb2xvcjogd2hpdGU7IH1cblxuOmhvc3QgL2RlZXAvIHRhYmxlIHRib2R5IHRyOm50aC1jaGlsZChldmVuKSB7XG4gIGJhY2tncm91bmQ6ICNlOWVhZWEgIWltcG9ydGFudDsgfVxuXG46aG9zdCAvZGVlcC8gdGFibGUgdGJvZHkgdHI6bnRoLWNoaWxkKG9kZCkge1xuICBiYWNrZ3JvdW5kOiB3aGl0ZSAhaW1wb3J0YW50OyB9XG5cbi9kZWVwLyBuZzItc21hcnQtdGFibGUtcGFnZXIgbmF2IHVsIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7IH1cbiIsIi8qKlxuICogQGxpY2Vuc2VcbiAqIENvcHlyaWdodCBBa3Zlby4gQWxsIFJpZ2h0cyBSZXNlcnZlZC5cbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgTGljZW5zZS4gU2VlIExpY2Vuc2UudHh0IGluIHRoZSBwcm9qZWN0IHJvb3QgZm9yIGxpY2Vuc2UgaW5mb3JtYXRpb24uXG4gKi9cblxuQG1peGluIG5iLXNjcm9sbGJhcnMoJGZnLCAkYmcsICRzaXplLCAkYm9yZGVyLXJhZGl1czogJHNpemUgLyAyKSB7XG4gIDo6LXdlYmtpdC1zY3JvbGxiYXIge1xuICAgIHdpZHRoOiAkc2l6ZTtcbiAgICBoZWlnaHQ6ICRzaXplO1xuICB9XG5cbiAgOjotd2Via2l0LXNjcm9sbGJhci10aHVtYiB7XG4gICAgYmFja2dyb3VuZDogJGZnO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICBib3JkZXItcmFkaXVzOiAkYm9yZGVyLXJhZGl1cztcbiAgfVxuXG4gIDo6LXdlYmtpdC1zY3JvbGxiYXItdHJhY2sge1xuICAgIGJhY2tncm91bmQ6ICRiZztcbiAgfVxuXG4gIC8vIFRPRE86IHJlbW92ZVxuICAvLyBGb3IgSW50ZXJuZXQgRXhwbG9yZXJcbiAgc2Nyb2xsYmFyLWZhY2UtY29sb3I6ICRmZztcbiAgc2Nyb2xsYmFyLXRyYWNrLWNvbG9yOiAkYmc7XG59XG5cbkBtaXhpbiBuYi1yYWRpYWwtZ3JhZGllbnQoJGNvbG9yLTEsICRjb2xvci0yLCAkY29sb3ItMykge1xuICBiYWNrZ3JvdW5kOiAkY29sb3ItMjsgLyogT2xkIGJyb3dzZXJzICovXG4gIGJhY2tncm91bmQ6IC1tb3otcmFkaWFsLWdyYWRpZW50KGJvdHRvbSwgZWxsaXBzZSBjb3ZlciwgJGNvbG9yLTEgMCUsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJGNvbG9yLTIgNDUlLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRjb2xvci0zIDEwMCUpOyAvKiBGRjMuNi0xNSAqL1xuICBiYWNrZ3JvdW5kOiAtd2Via2l0LXJhZGlhbC1ncmFkaWVudChib3R0b20sIGVsbGlwc2UgY292ZXIsICRjb2xvci0xIDAlLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRjb2xvci0yIDQ1JSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkY29sb3ItMyAxMDAlKTsgLyogQ2hyb21lMTAtMjUsU2FmYXJpNS4xLTYgKi9cbiAgYmFja2dyb3VuZDogcmFkaWFsLWdyYWRpZW50KGVsbGlwc2UgYXQgYm90dG9tLCAkY29sb3ItMSAwJSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkY29sb3ItMiA0NSUsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJGNvbG9yLTMgMTAwJSk7IC8qIFczQywgSUUxMCssIEZGMTYrLCBDaHJvbWUyNissIE9wZXJhMTIrLCBTYWZhcmk3KyAqL1xuICBmaWx0ZXI6IHByb2dpZDpkeGltYWdldHJhbnNmb3JtLm1pY3Jvc29mdC5ncmFkaWVudChzdGFydENvbG9yc3RyPSckY29sb3ItMScsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGVuZENvbG9yc3RyPSckY29sb3ItMycsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIEdyYWRpZW50VHlwZT0xKTsgLyogSUU2LTkgZmFsbGJhY2sgb24gaG9yaXpvbnRhbCBncmFkaWVudCAqL1xufVxuXG5AbWl4aW4gbmItcmlnaHQtZ3JhZGllbnQoJGxlZnQtY29sb3IsICRyaWdodC1jb2xvcikge1xuICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICRsZWZ0LWNvbG9yLCAkcmlnaHQtY29sb3IpO1xufVxuXG5AbWl4aW4gbmItaGVhZGluZ3MoJGZyb206IDEsICR0bzogNikge1xuICBAZm9yICRpIGZyb20gJGZyb20gdGhyb3VnaCAkdG8ge1xuICAgIGgjeyRpfSB7XG4gICAgICBtYXJnaW46IDA7XG4gICAgfVxuICB9XG59XG5cbkBtaXhpbiBob3Zlci1mb2N1cy1hY3RpdmUge1xuICAmOmZvY3VzLFxuICAmOmFjdGl2ZSxcbiAgJjpob3ZlciB7XG4gICAgQGNvbnRlbnQ7XG4gIH1cbn1cblxuQG1peGluIGNlbnRlci1ob3Jpem9udGFsLWFic29sdXRlIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAwKTtcbiAgbGVmdDogNTAlO1xufVxuXG5AbWl4aW4gaW5zdGFsbC10aHVtYigpIHtcbiAgJHRodW1iLXNlbGVjdG9yczogKFxuICAgICc6Oi13ZWJraXQtc2xpZGVyLXRodW1iJ1xuICAgICc6Oi1tb3otcmFuZ2UtdGh1bWInXG4gICAgJzo6LW1zLXRodW1iJ1xuICApO1xuXG4gIEBlYWNoICRzZWxlY3RvciBpbiAkdGh1bWItc2VsZWN0b3JzIHtcbiAgICAmI3skc2VsZWN0b3J9IHtcbiAgICAgIC13ZWJraXQtYXBwZWFyYW5jZTogbm9uZTtcbiAgICAgIC1tb3otYXBwZWFyYW5jZTogbm9uZTtcbiAgICAgIEBjb250ZW50O1xuICAgIH1cbiAgfVxufVxuXG5AbWl4aW4gaW5zdGFsbC10cmFjaygpIHtcbiAgJHRodW1iLXNlbGVjdG9yczogKFxuICAgICc6Oi13ZWJraXQtc2xpZGVyLXJ1bm5hYmxlLXRyYWNrJ1xuICAgICc6Oi1tb3otcmFuZ2UtdHJhY2snXG4gICAgJzo6LW1zLXRyYWNrJ1xuICApO1xuXG4gIEBlYWNoICRzZWxlY3RvciBpbiAkdGh1bWItc2VsZWN0b3JzIHtcbiAgICAmI3skc2VsZWN0b3J9IHtcbiAgICAgIC13ZWJraXQtYXBwZWFyYW5jZTogbm9uZTtcbiAgICAgIC1tb3otYXBwZWFyYW5jZTogbm9uZTtcbiAgICAgIEBjb250ZW50O1xuICAgIH1cbiAgfVxufVxuXG5AbWl4aW4gaW5zdGFsbC1wbGFjZWhvbGRlcigkY29sb3IsICRmb250LXNpemUpIHtcbiAgJHBsYWNlaG9sZGVyLXNlbGVjdG9yczogKFxuICAgICc6Oi13ZWJraXQtaW5wdXQtcGxhY2Vob2xkZXInXG4gICAgJzo6LW1vei1wbGFjZWhvbGRlcidcbiAgICAnOi1tb3otcGxhY2Vob2xkZXInXG4gICAgJzotbXMtaW5wdXQtcGxhY2Vob2xkZXInXG4gICk7XG5cbiAgJjo6cGxhY2Vob2xkZXIge1xuICAgIEBpbmNsdWRlIHBsYWNlaG9sZGVyKCRjb2xvciwgJGZvbnQtc2l6ZSk7XG4gIH1cblxuICBAZWFjaCAkc2VsZWN0b3IgaW4gJHBsYWNlaG9sZGVyLXNlbGVjdG9ycyB7XG4gICAgJiN7JHNlbGVjdG9yfSB7XG4gICAgICBAaW5jbHVkZSBwbGFjZWhvbGRlcigkY29sb3IsICRmb250LXNpemUpO1xuICAgIH1cblxuICAgICY6Zm9jdXMjeyRzZWxlY3Rvcn0ge1xuICAgICAgQGluY2x1ZGUgcGxhY2Vob2xkZXItZm9jdXMoKTtcbiAgICB9XG4gIH1cbn1cblxuQG1peGluIHBsYWNlaG9sZGVyKCRjb2xvciwgJGZvbnQtc2l6ZSkge1xuICBjb2xvcjogJGNvbG9yO1xuICBmb250LXNpemU6ICRmb250LXNpemU7XG4gIG9wYWNpdHk6IDE7XG4gIHRyYW5zaXRpb246IG9wYWNpdHkgMC4zcyBlYXNlO1xuICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcbn1cblxuQG1peGluIHBsYWNlaG9sZGVyLWZvY3VzKCkge1xuICBvcGFjaXR5OiAwO1xuICB0cmFuc2l0aW9uOiBvcGFjaXR5IDAuM3MgZWFzZTtcbn1cblxuQG1peGluIGFuaW1hdGlvbigkYW5pbWF0ZS4uLikge1xuICAkbWF4OiBsZW5ndGgoJGFuaW1hdGUpO1xuICAkYW5pbWF0aW9uczogJyc7XG5cbiAgQGZvciAkaSBmcm9tIDEgdGhyb3VnaCAkbWF4IHtcbiAgICAkYW5pbWF0aW9uczogI3skYW5pbWF0aW9ucyArIG50aCgkYW5pbWF0ZSwgJGkpfTtcblxuICAgIEBpZiAkaSA8ICRtYXgge1xuICAgICAgJGFuaW1hdGlvbnM6ICN7JGFuaW1hdGlvbnMgKyAnLCAnfTtcbiAgICB9XG4gIH1cbiAgLXdlYmtpdC1hbmltYXRpb246ICRhbmltYXRpb25zO1xuICAtbW96LWFuaW1hdGlvbjogICAgJGFuaW1hdGlvbnM7XG4gIC1vLWFuaW1hdGlvbjogICAgICAkYW5pbWF0aW9ucztcbiAgYW5pbWF0aW9uOiAgICAgICAgICRhbmltYXRpb25zO1xufVxuXG5AbWl4aW4ga2V5ZnJhbWVzKCRhbmltYXRpb25OYW1lKSB7XG4gIEAtd2Via2l0LWtleWZyYW1lcyAjeyRhbmltYXRpb25OYW1lfSB7XG4gICAgQGNvbnRlbnQ7XG4gIH1cbiAgQC1tb3ota2V5ZnJhbWVzICN7JGFuaW1hdGlvbk5hbWV9IHtcbiAgICBAY29udGVudDtcbiAgfVxuICBALW8ta2V5ZnJhbWVzICN7JGFuaW1hdGlvbk5hbWV9IHtcbiAgICBAY29udGVudDtcbiAgfVxuICBAa2V5ZnJhbWVzICN7JGFuaW1hdGlvbk5hbWV9IHtcbiAgICBAY29udGVudDtcbiAgfVxufVxuXG4vKipcbiAqIFRoaXMgbWl4aW4gZ2VuZXJhdGVzIGtleWZhbWVzLlxuICogQmVjYXVzZSBvZiBhbGwga2V5ZnJhbWVzIGNhbid0IGJlIHNjb3BlZCxcbiAqIHdlIG5lZWQgdG8gcHV0cyB1bmlxdWUgbmFtZSBpbiBlYWNoIGJ0bi1wdWxzZSBjYWxsLlxuICovXG5AbWl4aW4gYnRuLXB1bHNlKCRuYW1lLCAkY29sb3IpIHtcbiAgJi5idG4tcHVsc2Uge1xuICAgIEBpbmNsdWRlIGFuaW1hdGlvbihidG4tI3skbmFtZX0tcHVsc2UgMS41cyBpbmZpbml0ZSk7XG4gIH1cblxuICBAaW5jbHVkZSBrZXlmcmFtZXMoYnRuLSN7JG5hbWV9LXB1bHNlKSB7XG4gICAgMCUge1xuICAgICAgYm94LXNoYWRvdzogbm9uZTtcbiAgICAgIG9wYWNpdHk6IG5iLXRoZW1lKGJ0bi1kaXNhYmxlZC1vcGFjaXR5KTtcbiAgICB9XG4gICAgNTAlIHtcbiAgICAgIGJveC1zaGFkb3c6IDAgMCAxcmVtIDAgJGNvbG9yO1xuICAgICAgb3BhY2l0eTogMC44O1xuICAgIH1cbiAgICAxMDAlIHtcbiAgICAgIGJveC1zaGFkb3c6IG5vbmU7XG4gICAgICBvcGFjaXR5OiBuYi10aGVtZShidG4tZGlzYWJsZWQtb3BhY2l0eSk7XG4gICAgfVxuICB9XG59XG5cbi8qXG5cbkFjY29yZGluZyB0byB0aGUgc3BlY2lmaWNhdGlvbiAoaHR0cHM6Ly93d3cudzMub3JnL1RSL2Nzcy1zY29waW5nLTEvI2hvc3Qtc2VsZWN0b3IpXG46aG9zdCBhbmQgOmhvc3QtY29udGV4dCBhcmUgcHNldWRvLWNsYXNzZXMuIFNvIHdlIGFzc3VtZSB0aGV5IGNvdWxkIGJlIGNvbWJpbmVkLFxubGlrZSBvdGhlciBwc2V1ZG8tY2xhc3NlcywgZXZlbiBzYW1lIG9uZXMuXG5Gb3IgZXhhbXBsZTogJzpudGgtb2YtdHlwZSgybik6bnRoLW9mLXR5cGUoZXZlbiknLlxuXG5JZGVhbCBzb2x1dGlvbiB3b3VsZCBiZSB0byBwcmVwZW5kIGFueSBzZWxlY3RvciB3aXRoIDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKS5cblRoZW4gbmVidWxhciBjb21wb25lbnRzIHdpbGwgYmVoYXZlIGFzIGFuIGh0bWwgZWxlbWVudCBhbmQgcmVzcG9uZCB0byBbZGlyXSBhdHRyaWJ1dGUgb24gYW55IGxldmVsLFxuc28gZGlyZWN0aW9uIGNvdWxkIGJlIG92ZXJyaWRkZW4gb24gYW55IGNvbXBvbmVudCBsZXZlbC5cblxuSW1wbGVtZW50YXRpb24gY29kZTpcblxuQG1peGluIG5iLXJ0bCgpIHtcbiAgLy8gYWRkICMgdG8gc2NzcyBpbnRlcnBvbGF0aW9uIHN0YXRlbWVudC5cbiAgLy8gaXQgd29ya3MgaW4gY29tbWVudHMgYW5kIHdlIGNhbid0IHVzZSBpdCBoZXJlXG4gIEBhdC1yb290IHtzZWxlY3Rvci1hcHBlbmQoJzpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKScsICYpfSB7XG4gICAgQGNvbnRlbnQ7XG4gIH1cbn1cblxuQW5kIHdoZW4gd2UgY2FsbCBpdCBzb21ld2hlcmU6XG5cbjpob3N0IHtcbiAgLnNvbWUtY2xhc3Mge1xuICAgIEBpbmNsdWRlIG5iLXJ0bCgpIHtcbiAgICAgIC4uLlxuICAgIH1cbiAgfVxufVxuOmhvc3QtY29udGV4dCguLi4pIHtcbiAgLnNvbWUtY2xhc3Mge1xuICAgIEBpbmNsdWRlIG5iLXJ0bCgpIHtcbiAgICAgIC4uLlxuICAgIH1cbiAgfVxufVxuXG5SZXN1bHQgd2lsbCBsb29rIGxpa2U6XG5cbjpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKTpob3N0IC5zb21lLWNsYXNzIHtcbiAgLi4uXG59XG46aG9zdC1jb250ZXh0KFtkaXI9cnRsXSk6aG9zdC1jb250ZXh0KC4uLikgLnNvbWUtY2xhc3Mge1xuICAuLi5cbn1cblxuKlxuICBTaWRlIG5vdGU6XG4gIDpob3N0LWNvbnRleHQoKTpob3N0IHNlbGVjdG9yIGFyZSB2YWxpZC4gaHR0cHM6Ly9saXN0cy53My5vcmcvQXJjaGl2ZXMvUHVibGljL3d3dy1zdHlsZS8yMDE1RmViLzAzMDUuaHRtbFxuXG4gIDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKTpob3N0LWNvbnRleHQoLi4uKSBzaG91bGQgbWF0Y2ggYW55IHBlcm11dGF0aW9uLFxuICBzbyBvcmRlciBpcyBub3QgaW1wb3J0YW50LlxuKlxuXG5cbkN1cnJlbnRseSwgdGhlcmUncmUgdHdvIHByb2JsZW1zIHdpdGggdGhpcyBhcHByb2FjaDpcblxuRmlyc3QsIGlzIHRoYXQgd2UgY2FuJ3QgY29tYmluZSA6aG9zdCwgOmhvc3QtY29udGV4dC4gQW5ndWxhciBidWdzICMxNDM0OSwgIzE5MTk5LlxuRm9yIHRoZSBtb21lbnQgb2Ygd3JpdGluZywgdGhlIG9ubHkgcG9zc2libGUgd2F5IGlzOlxuOmhvc3Qge1xuICA6aG9zdC1jb250ZXh0KC4uLikge1xuICAgIC4uLlxuICB9XG59XG5JdCBkb2Vzbid0IHdvcmsgZm9yIHVzIGJlY2F1c2UgbWl4aW4gY291bGQgYmUgY2FsbGVkIHNvbWV3aGVyZSBkZWVwZXIsIGxpa2U6XG46aG9zdCB7XG4gIHAge1xuICAgIEBpbmNsdWRlIG5iLXJ0bCgpIHsgLi4uIH1cbiAgfVxufVxuV2UgYXJlIG5vdCBhYmxlIHRvIGdvIHVwIHRvIDpob3N0IGxldmVsIHRvIHBsYWNlIGNvbnRlbnQgcGFzc2VkIHRvIG1peGluLlxuXG5UaGUgc2Vjb25kIHByb2JsZW0gaXMgdGhhdCB3ZSBvbmx5IGNhbiBiZSBzdXJlIHRoYXQgd2UgYXBwZW5kaW5nIDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKSB0byBhbm90aGVyXG46aG9zdC86aG9zdC1jb250ZXh0IHBzZXVkby1jbGFzcyB3aGVuIGNhbGxlZCBpbiB0aGVtZSBmaWxlcyAoKi50aGVtZS5zY3NzKS5cbiAgKlxuICAgIFNpZGUgbm90ZTpcbiAgICBDdXJyZW50bHksIG5iLWluc3RhbGwtY29tcG9uZW50IHVzZXMgYW5vdGhlciBhcHByb2FjaCB3aGVyZSA6aG9zdCBwcmVwZW5kZWQgd2l0aCB0aGUgdGhlbWUgbmFtZVxuICAgIChodHRwczovL2dpdGh1Yi5jb20vYW5ndWxhci9hbmd1bGFyL2Jsb2IvNWI5NjA3ODYyNGIwYTQ3NjBmMmRiY2Y2ZmRmMGJkNjI3OTFiZTViYi9wYWNrYWdlcy9jb21waWxlci9zcmMvc2hhZG93X2Nzcy50cyNMNDQxKSxcbiAgICBidXQgaXQgd2FzIG1hZGUgdG8gYmUgYWJsZSB0byB1c2UgY3VycmVudCByZWFsaXphdGlvbiBvZiBydGwgYW5kIGl0IGNhbiBiZSByZXdyaXR0ZW4gYmFjayB0b1xuICAgIDpob3N0LWNvbnRleHQoJHRoZW1lKSBvbmNlIHdlIHdpbGwgYmUgYWJsZSB0byB1c2UgbXVsdGlwbGUgc2hhZG93IHNlbGVjdG9ycy5cbiAgKlxuQnV0IHdoZW4gaXQncyBjYWxsZWQgaW4gKi5jb21wb25lbnQuc2NzcyB3ZSBjYW4ndCBiZSBzdXJlLCB0aGF0IHNlbGVjdG9yIHN0YXJ0cyB3aXRoIDpob3N0Lzpob3N0LWNvbnRleHQsXG5iZWNhdXNlIGFuZ3VsYXIgYWxsb3dzIG9taXR0aW5nIHBzZXVkby1jbGFzc2VzIGlmIHdlIGRvbid0IG5lZWQgdG8gc3R5bGUgOmhvc3QgY29tcG9uZW50IGl0c2VsZi5cbldlIGNhbiBicmVhayBzdWNoIHNlbGVjdG9ycywgYnkganVzdCBhcHBlbmRpbmcgOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pIHRvIHRoZW0uXG4gICoqKlxuICAgIFBvc3NpYmxlIHNvbHV0aW9uXG4gICAgY2hlY2sgaWYgd2UgaW4gdGhlbWUgYnkgc29tZSB0aGVtZSB2YXJpYWJsZXMgYW5kIGlmIHNvIGFwcGVuZCwgb3RoZXJ3aXNlIG5lc3QgbGlrZVxuICAgIEBhdC1yb290IDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKSB7XG4gICAgICAvLyBhZGQgIyB0byBzY3NzIGludGVycG9sYXRpb24gc3RhdGVtZW50LlxuICAgICAgLy8gaXQgd29ya3MgaW4gY29tbWVudHMgYW5kIHdlIGNhbid0IHVzZSBpdCBoZXJlXG4gICAgICB7Jn0ge1xuICAgICAgICBAY29udGVudDtcbiAgICAgIH1cbiAgICB9XG4gICAgV2hhdCBpZiA6aG9zdCBzcGVjaWZpZWQ/IENhbiB3ZSBhZGQgc3BhY2UgaW4gOmhvc3QtY29udGV4dCguLi4pIDpob3N0P1xuICAgIE9yIG1heWJlIGFkZCA6aG9zdCBzZWxlY3RvciBhbnl3YXk/IElmIG11bHRpcGxlIDpob3N0IHNlbGVjdG9ycyBhcmUgYWxsb3dlZFxuICAqKipcblxuXG5Qcm9ibGVtcyB3aXRoIHRoZSBjdXJyZW50IGFwcHJvYWNoLlxuXG4xLiBEaXJlY3Rpb24gY2FuIGJlIGFwcGxpZWQgb25seSBvbiBkb2N1bWVudCBsZXZlbCwgYmVjYXVzZSBtaXhpbiBwcmVwZW5kcyB0aGVtZSBjbGFzcyxcbndoaWNoIHBsYWNlZCBvbiB0aGUgYm9keS5cbjIuICouY29tcG9uZW50LnNjc3Mgc3R5bGVzIHNob3VsZCBiZSBpbiA6aG9zdCBzZWxlY3Rvci4gT3RoZXJ3aXNlIGFuZ3VsYXIgd2lsbCBhZGQgaG9zdFxuYXR0cmlidXRlIHRvIFtkaXI9cnRsXSBhdHRyaWJ1dGUgYXMgd2VsbC5cblxuXG5HZW5lcmFsIHByb2JsZW1zLlxuXG5MdHIgaXMgZGVmYXVsdCBkb2N1bWVudCBkaXJlY3Rpb24sIGJ1dCBmb3IgcHJvcGVyIHdvcmsgb2YgbmItbHRyIChtZWFucyBsdHIgb25seSksXG5bZGlyPWx0cl0gc2hvdWxkIGJlIHNwZWNpZmllZCBhdCBsZWFzdCBzb21ld2hlcmUuICc6bm90KFtkaXI9cnRsXScgbm90IGFwcGxpY2FibGUgaGVyZSxcbmJlY2F1c2UgaXQncyBzYXRpc2Z5IGFueSBwYXJlbnQsIHRoYXQgZG9uJ3QgaGF2ZSBbZGlyPXJ0bF0gYXR0cmlidXRlLlxuUHJldmlvdXMgYXBwcm9hY2ggd2FzIHRvIHVzZSBzaW5nbGUgcnRsIG1peGluIGFuZCByZXNldCBsdHIgcHJvcGVydGllcyB0byBpbml0aWFsIHZhbHVlLlxuQnV0IHNvbWV0aW1lcyBpdCdzIGhhcmQgdG8gZmluZCwgd2hhdCB0aGUgcHJldmlvdXMgdmFsdWUgc2hvdWxkIGJlLiBBbmQgc3VjaCBtaXhpbiBjYWxsIGxvb2tzIHRvbyB2ZXJib3NlLlxuKi9cblxuQG1peGluIF9wcmVwZW5kLXdpdGgtc2VsZWN0b3IoJHNlbGVjdG9yLCAkcHJvcDogbnVsbCwgJHZhbHVlOiBudWxsKSB7XG4gICN7JHNlbGVjdG9yfSAmIHtcbiAgICBAaWYgJHByb3AgIT0gbnVsbCB7XG4gICAgICAjeyRwcm9wfTogJHZhbHVlO1xuICAgIH1cblxuICAgIEBjb250ZW50O1xuICB9XG59XG5cbkBtaXhpbiBuYi1sdHIoJHByb3A6IG51bGwsICR2YWx1ZTogbnVsbCkge1xuICBAaW5jbHVkZSBfcHJlcGVuZC13aXRoLXNlbGVjdG9yKCdbZGlyPWx0cl0nLCAkcHJvcCwgJHZhbHVlKSB7XG4gICAgQGNvbnRlbnQ7XG4gIH1cbn1cblxuQG1peGluIG5iLXJ0bCgkcHJvcDogbnVsbCwgJHZhbHVlOiBudWxsKSB7XG4gIEBpbmNsdWRlIF9wcmVwZW5kLXdpdGgtc2VsZWN0b3IoJ1tkaXI9cnRsXScsICRwcm9wLCAkdmFsdWUpIHtcbiAgICBAY29udGVudDtcbiAgfTtcbn1cbiIsIi8qKlxuICogQGxpY2Vuc2VcbiAqIENvcHlyaWdodCBBa3Zlby4gQWxsIFJpZ2h0cyBSZXNlcnZlZC5cbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgTGljZW5zZS4gU2VlIExpY2Vuc2UudHh0IGluIHRoZSBwcm9qZWN0IHJvb3QgZm9yIGxpY2Vuc2UgaW5mb3JtYXRpb24uXG4gKi9cblxuLy8vIFNsaWdodGx5IGxpZ2h0ZW4gYSBjb2xvclxuLy8vIEBhY2Nlc3MgcHVibGljXG4vLy8gQHBhcmFtIHtDb2xvcn0gJGNvbG9yIC0gY29sb3IgdG8gdGludFxuLy8vIEBwYXJhbSB7TnVtYmVyfSAkcGVyY2VudGFnZSAtIHBlcmNlbnRhZ2Ugb2YgYCRjb2xvcmAgaW4gcmV0dXJuZWQgY29sb3Jcbi8vLyBAcmV0dXJuIHtDb2xvcn1cbkBmdW5jdGlvbiB0aW50KCRjb2xvciwgJHBlcmNlbnRhZ2UpIHtcbiAgQHJldHVybiBtaXgod2hpdGUsICRjb2xvciwgJHBlcmNlbnRhZ2UpO1xufVxuXG4vLy8gU2xpZ2h0bHkgZGFya2VuIGEgY29sb3Jcbi8vLyBAYWNjZXNzIHB1YmxpY1xuLy8vIEBwYXJhbSB7Q29sb3J9ICRjb2xvciAtIGNvbG9yIHRvIHNoYWRlXG4vLy8gQHBhcmFtIHtOdW1iZXJ9ICRwZXJjZW50YWdlIC0gcGVyY2VudGFnZSBvZiBgJGNvbG9yYCBpbiByZXR1cm5lZCBjb2xvclxuLy8vIEByZXR1cm4ge0NvbG9yfVxuQGZ1bmN0aW9uIHNoYWRlKCRjb2xvciwgJHBlcmNlbnRhZ2UpIHtcbiAgQHJldHVybiBtaXgoYmxhY2ssICRjb2xvciwgJHBlcmNlbnRhZ2UpO1xufVxuXG5AZnVuY3Rpb24gbWFwLXNldCgkbWFwLCAka2V5LCAkdmFsdWU6IG51bGwpIHtcbiAgJG5ldzogKCRrZXk6ICR2YWx1ZSk7XG4gIEByZXR1cm4gbWFwLW1lcmdlKCRtYXAsICRuZXcpO1xufVxuIiwiLyoqXG4gKiBAbGljZW5zZVxuICogQ29weXJpZ2h0IEFrdmVvLiBBbGwgUmlnaHRzIFJlc2VydmVkLlxuICogTGljZW5zZWQgdW5kZXIgdGhlIE1JVCBMaWNlbnNlLiBTZWUgTGljZW5zZS50eHQgaW4gdGhlIHByb2plY3Qgcm9vdCBmb3IgbGljZW5zZSBpbmZvcm1hdGlvbi5cbiAqL1xuXG5AaW1wb3J0ICcuLi9jb3JlL2Z1bmN0aW9ucyc7XG5AaW1wb3J0ICcuLi9jb3JlL21peGlucyc7XG5cbiR0aGVtZTogKFxuICBmb250LW1haW46IHVucXVvdGUoJ1wiU2Vnb2UgVUlcIiwgUm9ib3RvLCBcIkhlbHZldGljYSBOZXVlXCIsIEFyaWFsLCBzYW5zLXNlcmlmJyksXG4gIGZvbnQtc2Vjb25kYXJ5OiBmb250LW1haW4sXG5cbiAgZm9udC13ZWlnaHQtdGhpbjogMjAwLFxuICBmb250LXdlaWdodC1saWdodDogMzAwLFxuICBmb250LXdlaWdodC1ub3JtYWw6IDQwMCxcbiAgZm9udC13ZWlnaHQtYm9sZGVyOiA1MDAsXG4gIGZvbnQtd2VpZ2h0LWJvbGQ6IDYwMCxcbiAgZm9udC13ZWlnaHQtdWx0cmEtYm9sZDogODAwLFxuXG4gIC8vIFRPRE86IHVzZSBpdCBhcyBhIGRlZmF1bHQgZm9udC1zaXplXG4gIGJhc2UtZm9udC1zaXplOiAxNnB4LFxuXG4gIGZvbnQtc2l6ZS14bGc6IDEuMjVyZW0sXG4gIGZvbnQtc2l6ZS1sZzogMS4xMjVyZW0sXG4gIGZvbnQtc2l6ZTogMXJlbSxcbiAgZm9udC1zaXplLXNtOiAwLjg3NXJlbSxcbiAgZm9udC1zaXplLXhzOiAwLjc1cmVtLFxuXG4gIHJhZGl1czogMC4zNzVyZW0sXG4gIHBhZGRpbmc6IDEuMjVyZW0sXG4gIG1hcmdpbjogMS41cmVtLFxuICBsaW5lLWhlaWdodDogMS4yNSxcblxuICBjb2xvci1iZzogI2ZmZmZmZixcbiAgY29sb3ItYmctYWN0aXZlOiAjZTllZGYyLFxuICBjb2xvci1mZzogI2E0YWJiMyxcbiAgY29sb3ItZmctaGVhZGluZzogIzJhMmEyYSxcbiAgY29sb3ItZmctdGV4dDogIzRiNGI0YixcbiAgY29sb3ItZmctaGlnaGxpZ2h0OiAjNDBkYzdlLFxuXG4gIHNlcGFyYXRvcjogI2ViZWVmMixcblxuICBjb2xvci1ncmF5OiByZ2JhKDgxLCAxMTMsIDE2NSwgMC4xNSksXG4gIGNvbG9yLW5ldXRyYWw6IHRyYW5zcGFyZW50LFxuICBjb2xvci13aGl0ZTogI2ZmZmZmZixcbiAgY29sb3ItZGlzYWJsZWQ6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC40KSxcblxuICBjb2xvci1wcmltYXJ5OiAjOGE3ZmZmLFxuICBjb2xvci1zdWNjZXNzOiAjNDBkYzdlLFxuICBjb2xvci1pbmZvOiAjNGNhNmZmLFxuICBjb2xvci13YXJuaW5nOiAjZmZhMTAwLFxuICBjb2xvci1kYW5nZXI6ICNmZjRjNmEsXG5cbiAgLy8gVE9ETzogbW92ZSB0byBjb25zdGFudHNcbiAgc29jaWFsLWNvbG9yLWZhY2Vib29rOiAjM2I1OTk4LFxuICBzb2NpYWwtY29sb3ItdHdpdHRlcjogIzU1YWNlZSxcbiAgc29jaWFsLWNvbG9yLWdvb2dsZTogI2RkNGIzOSxcbiAgc29jaWFsLWNvbG9yLWxpbmtlZGluOiAjMDE3N2I1LFxuICBzb2NpYWwtY29sb3ItZ2l0aHViOiAjNmI2YjZiLFxuICBzb2NpYWwtY29sb3Itc3RhY2tvdmVyZmxvdzogIzJmOTZlOCxcbiAgc29jaWFsLWNvbG9yLWRyaWJibGU6ICNmMjY3OTgsXG4gIHNvY2lhbC1jb2xvci1iZWhhbmNlOiAjMDA5M2ZhLFxuXG4gIGJvcmRlci1jb2xvcjogY29sb3ItZ3JheSxcbiAgc2hhZG93OiAwIDJweCAxMnB4IDAgI2RmZTNlYixcblxuICBsaW5rLWNvbG9yOiAjM2RjYzZkLFxuICBsaW5rLWNvbG9yLWhvdmVyOiAjMmVlNTZiLFxuICBsaW5rLWNvbG9yLXZpc2l0ZWQ6IGxpbmstY29sb3IsXG5cbiAgc2Nyb2xsYmFyLWZnOiAjZGFkYWRhLFxuICBzY3JvbGxiYXItYmc6ICNmMmYyZjIsXG4gIHNjcm9sbGJhci13aWR0aDogNXB4LFxuICBzY3JvbGxiYXItdGh1bWItcmFkaXVzOiAyLjVweCxcblxuICByYWRpYWwtZ3JhZGllbnQ6IG5vbmUsXG4gIGxpbmVhci1ncmFkaWVudDogbm9uZSxcblxuICBjYXJkLWZvbnQtc2l6ZTogZm9udC1zaXplLFxuICBjYXJkLWxpbmUtaGVpZ2h0OiBsaW5lLWhlaWdodCxcbiAgY2FyZC1mb250LXdlaWdodDogZm9udC13ZWlnaHQtbm9ybWFsLFxuICBjYXJkLWZnOiBjb2xvci1mZywgLy8gVE9ETzogbm90IHVzZWRcbiAgY2FyZC1mZy10ZXh0OiBjb2xvci1mZy10ZXh0LFxuICBjYXJkLWZnLWhlYWRpbmc6IGNvbG9yLWZnLWhlYWRpbmcsIC8vIFRPRE86IG5vdCB1c2VkXG4gIGNhcmQtYmc6IGNvbG9yLWJnLFxuICBjYXJkLWhlaWdodC14eHNtYWxsOiA5NnB4LFxuICBjYXJkLWhlaWdodC14c21hbGw6IDIxNnB4LFxuICBjYXJkLWhlaWdodC1zbWFsbDogMzM2cHgsXG4gIGNhcmQtaGVpZ2h0LW1lZGl1bTogNDU2cHgsXG4gIGNhcmQtaGVpZ2h0LWxhcmdlOiA1NzZweCxcbiAgY2FyZC1oZWlnaHQteGxhcmdlOiA2OTZweCxcbiAgY2FyZC1oZWlnaHQteHhsYXJnZTogODE2cHgsXG4gIGNhcmQtc2hhZG93OiBzaGFkb3csXG4gIGNhcmQtYm9yZGVyLXdpZHRoOiAwLFxuICBjYXJkLWJvcmRlci10eXBlOiBzb2xpZCxcbiAgY2FyZC1ib3JkZXItY29sb3I6IGNvbG9yLWJnLFxuICBjYXJkLWJvcmRlci1yYWRpdXM6IHJhZGl1cyxcbiAgY2FyZC1wYWRkaW5nOiBwYWRkaW5nLFxuICBjYXJkLW1hcmdpbjogbWFyZ2luLFxuICBjYXJkLWhlYWRlci1mb250LWZhbWlseTogZm9udC1zZWNvbmRhcnksXG4gIGNhcmQtaGVhZGVyLWZvbnQtc2l6ZTogZm9udC1zaXplLWxnLFxuICBjYXJkLWhlYWRlci1mb250LXdlaWdodDogZm9udC13ZWlnaHQtYm9sZCxcbiAgY2FyZC1zZXBhcmF0b3I6IHNlcGFyYXRvcixcbiAgY2FyZC1oZWFkZXItZmc6IGNvbG9yLWZnLCAvLyBUT0RPOiBub3QgdXNlZFxuICBjYXJkLWhlYWRlci1mZy1oZWFkaW5nOiBjb2xvci1mZy1oZWFkaW5nLFxuICBjYXJkLWhlYWRlci1hY3RpdmUtYmc6IGNvbG9yLWZnLFxuICBjYXJkLWhlYWRlci1hY3RpdmUtZmc6IGNvbG9yLWJnLFxuICBjYXJkLWhlYWRlci1kaXNhYmxlZC1iZzogY29sb3ItZGlzYWJsZWQsXG4gIGNhcmQtaGVhZGVyLXByaW1hcnktYmc6IGNvbG9yLXByaW1hcnksXG4gIGNhcmQtaGVhZGVyLWluZm8tYmc6IGNvbG9yLWluZm8sXG4gIGNhcmQtaGVhZGVyLXN1Y2Nlc3MtYmc6IGNvbG9yLXN1Y2Nlc3MsXG4gIGNhcmQtaGVhZGVyLXdhcm5pbmctYmc6IGNvbG9yLXdhcm5pbmcsXG4gIGNhcmQtaGVhZGVyLWRhbmdlci1iZzogY29sb3ItZGFuZ2VyLFxuICBjYXJkLWhlYWRlci1ib3JkZXItd2lkdGg6IDFweCxcbiAgY2FyZC1oZWFkZXItYm9yZGVyLXR5cGU6IHNvbGlkLFxuICBjYXJkLWhlYWRlci1ib3JkZXItY29sb3I6IGNhcmQtc2VwYXJhdG9yLFxuXG4gIGhlYWRlci1mb250LWZhbWlseTogZm9udC1zZWNvbmRhcnksXG4gIGhlYWRlci1mb250LXNpemU6IGZvbnQtc2l6ZSxcbiAgaGVhZGVyLWxpbmUtaGVpZ2h0OiBsaW5lLWhlaWdodCxcbiAgaGVhZGVyLWZnOiBjb2xvci1mZy1oZWFkaW5nLFxuICBoZWFkZXItYmc6IGNvbG9yLWJnLFxuICBoZWFkZXItaGVpZ2h0OiA0Ljc1cmVtLFxuICBoZWFkZXItcGFkZGluZzogMS4yNXJlbSxcbiAgaGVhZGVyLXNoYWRvdzogc2hhZG93LFxuXG4gIGZvb3Rlci1oZWlnaHQ6IDQuNzI1cmVtLFxuICBmb290ZXItcGFkZGluZzogMS4yNXJlbSxcbiAgZm9vdGVyLWZnOiBjb2xvci1mZy1oZWFkaW5nLFxuICBmb290ZXItZmctaGlnaGxpZ2h0OiBjb2xvci1mZy1oZWFkaW5nLFxuICBmb290ZXItYmc6IGNvbG9yLWJnLFxuICBmb290ZXItc2VwYXJhdG9yOiBzZXBhcmF0b3IsXG4gIGZvb3Rlci1zaGFkb3c6IHNoYWRvdyxcblxuICBsYXlvdXQtZm9udC1mYW1pbHk6IGZvbnQtbWFpbixcbiAgbGF5b3V0LWZvbnQtc2l6ZTogZm9udC1zaXplLFxuICBsYXlvdXQtbGluZS1oZWlnaHQ6IGxpbmUtaGVpZ2h0LFxuICBsYXlvdXQtZmc6IGNvbG9yLWZnLFxuICBsYXlvdXQtYmc6ICNlYmVmZjUsXG4gIGxheW91dC1taW4taGVpZ2h0OiAxMDB2aCxcbiAgbGF5b3V0LWNvbnRlbnQtd2lkdGg6IDkwMHB4LFxuICBsYXlvdXQtd2luZG93LW1vZGUtbWluLXdpZHRoOiAzMDBweCxcbiAgbGF5b3V0LXdpbmRvdy1tb2RlLW1heC13aWR0aDogMTkyMHB4LFxuICBsYXlvdXQtd2luZG93LW1vZGUtYmc6IGxheW91dC1iZyxcbiAgbGF5b3V0LXdpbmRvdy1tb2RlLXBhZGRpbmctdG9wOiA0Ljc1cmVtLFxuICBsYXlvdXQtd2luZG93LXNoYWRvdzogc2hhZG93LFxuICBsYXlvdXQtcGFkZGluZzogMi4yNXJlbSAyLjI1cmVtIDAuNzVyZW0sXG4gIGxheW91dC1tZWRpdW0tcGFkZGluZzogMS41cmVtIDEuNXJlbSAwLjVyZW0sXG4gIGxheW91dC1zbWFsbC1wYWRkaW5nOiAxcmVtIDFyZW0gMCxcblxuICBzaWRlYmFyLWZvbnQtc2l6ZTogZm9udC1zaXplLFxuICBzaWRlYmFyLWxpbmUtaGVpZ2h0OiBsaW5lLWhlaWdodCxcbiAgc2lkZWJhci1mZzogY29sb3ItZmctaGVhZGluZyxcbiAgc2lkZWJhci1iZzogY29sb3ItYmcsXG4gIHNpZGViYXItaGVpZ2h0OiAxMDB2aCxcbiAgc2lkZWJhci13aWR0aDogMTZyZW0sXG4gIHNpZGViYXItd2lkdGgtY29tcGFjdDogMy41cmVtLFxuICBzaWRlYmFyLXBhZGRpbmc6IHBhZGRpbmcsXG4gIHNpZGViYXItaGVhZGVyLWhlaWdodDogMy41cmVtLFxuICBzaWRlYmFyLWZvb3Rlci1oZWlnaHQ6IDMuNXJlbSxcbiAgc2lkZWJhci1zaGFkb3c6IHNoYWRvdyxcblxuICBtZW51LWZvbnQtZmFtaWx5OiBmb250LXNlY29uZGFyeSxcbiAgbWVudS1mb250LXNpemU6IGZvbnQtc2l6ZSxcbiAgbWVudS1mb250LXdlaWdodDogZm9udC13ZWlnaHQtYm9sZGVyLFxuICBtZW51LWZnOiBjb2xvci1mZy10ZXh0LFxuICBtZW51LWJnOiBjb2xvci1iZyxcbiAgbWVudS1hY3RpdmUtZmc6IGNvbG9yLWZnLWhlYWRpbmcsXG4gIG1lbnUtYWN0aXZlLWJnOiBjb2xvci1iZyxcbiAgbWVudS1hY3RpdmUtZm9udC13ZWlnaHQ6IGZvbnQtd2VpZ2h0LWJvbGQsXG5cbiAgbWVudS1zdWJtZW51LWJnOiBjb2xvci1iZyxcbiAgbWVudS1zdWJtZW51LWZnOiBjb2xvci1mZy10ZXh0LFxuICBtZW51LXN1Ym1lbnUtYWN0aXZlLWZnOiBjb2xvci1mZy1oZWFkaW5nLFxuICBtZW51LXN1Ym1lbnUtYWN0aXZlLWJnOiBjb2xvci1iZyxcbiAgbWVudS1zdWJtZW51LWFjdGl2ZS1ib3JkZXItY29sb3I6IGNvbG9yLWZnLWhpZ2hsaWdodCxcbiAgbWVudS1zdWJtZW51LWFjdGl2ZS1zaGFkb3c6IG5vbmUsXG4gIG1lbnUtc3VibWVudS1ob3Zlci1mZzogbWVudS1zdWJtZW51LWFjdGl2ZS1mZyxcbiAgbWVudS1zdWJtZW51LWhvdmVyLWJnOiBtZW51LXN1Ym1lbnUtYmcsXG4gIG1lbnUtc3VibWVudS1pdGVtLWJvcmRlci13aWR0aDogMC4xMjVyZW0sXG4gIG1lbnUtc3VibWVudS1pdGVtLWJvcmRlci1yYWRpdXM6IHJhZGl1cyxcbiAgbWVudS1zdWJtZW51LWl0ZW0tcGFkZGluZzogMC41cmVtIDFyZW0sXG4gIG1lbnUtc3VibWVudS1pdGVtLWNvbnRhaW5lci1wYWRkaW5nOiAwIDEuMjVyZW0sXG4gIG1lbnUtc3VibWVudS1wYWRkaW5nOiAwLjVyZW0sXG5cbiAgbWVudS1ncm91cC1mb250LXdlaWdodDogZm9udC13ZWlnaHQtYm9sZGVyLFxuICBtZW51LWdyb3VwLWZvbnQtc2l6ZTogMC44NzVyZW0sXG4gIG1lbnUtZ3JvdXAtZmc6IGNvbG9yLWZnLFxuICBtZW51LWdyb3VwLXBhZGRpbmc6IDFyZW0gMS4yNXJlbSxcbiAgbWVudS1pdGVtLXBhZGRpbmc6IDAuNjc1cmVtIDAuNzVyZW0sXG4gIG1lbnUtaXRlbS1zZXBhcmF0b3I6IHNlcGFyYXRvcixcbiAgbWVudS1pY29uLWZvbnQtc2l6ZTogMi41cmVtLFxuICBtZW51LWljb24tbWFyZ2luOiAwIDAuMjVyZW0gMCxcbiAgbWVudS1pY29uLWNvbG9yOiBjb2xvci1mZyxcbiAgbWVudS1pY29uLWFjdGl2ZS1jb2xvcjogY29sb3ItZmctaGVhZGluZyxcblxuICB0YWJzLWZvbnQtZmFtaWx5OiBmb250LXNlY29uZGFyeSxcbiAgdGFicy1mb250LXNpemU6IGZvbnQtc2l6ZS1sZyxcbiAgdGFicy1jb250ZW50LWZvbnQtZmFtaWx5OiBmb250LW1haW4sXG4gIHRhYnMtY29udGVudC1mb250LXNpemU6IGZvbnQtc2l6ZSxcbiAgdGFicy1hY3RpdmUtYmc6IHRyYW5zcGFyZW50LFxuICB0YWJzLWFjdGl2ZS1mb250LXdlaWdodDogY2FyZC1oZWFkZXItZm9udC13ZWlnaHQsXG4gIHRhYnMtcGFkZGluZzogcGFkZGluZyxcbiAgdGFicy1jb250ZW50LXBhZGRpbmc6IDAsXG4gIHRhYnMtaGVhZGVyLWJnOiB0cmFuc3BhcmVudCxcbiAgdGFicy1zZXBhcmF0b3I6IHNlcGFyYXRvcixcbiAgdGFicy1mZzogY29sb3ItZmcsXG4gIHRhYnMtZmctdGV4dDogY29sb3ItZmctdGV4dCxcbiAgdGFicy1mZy1oZWFkaW5nOiBjb2xvci1mZy1oZWFkaW5nLFxuICB0YWJzLWJnOiB0cmFuc3BhcmVudCxcbiAgdGFicy1zZWxlY3RlZDogY29sb3Itc3VjY2VzcyxcbiAgdGFicy1pY29uLW9ubHktbWF4LXdpZHRoOiA1NzZweCxcblxuICByb3V0ZS10YWJzLWZvbnQtZmFtaWx5OiBmb250LXNlY29uZGFyeSxcbiAgcm91dGUtdGFicy1mb250LXNpemU6IGZvbnQtc2l6ZS1sZyxcbiAgcm91dGUtdGFicy1hY3RpdmUtYmc6IHRyYW5zcGFyZW50LFxuICByb3V0ZS10YWJzLWFjdGl2ZS1mb250LXdlaWdodDogY2FyZC1oZWFkZXItZm9udC13ZWlnaHQsXG4gIHJvdXRlLXRhYnMtcGFkZGluZzogcGFkZGluZyxcbiAgcm91dGUtdGFicy1oZWFkZXItYmc6IHRyYW5zcGFyZW50LFxuICByb3V0ZS10YWJzLXNlcGFyYXRvcjogc2VwYXJhdG9yLFxuICByb3V0ZS10YWJzLWZnOiBjb2xvci1mZyxcbiAgcm91dGUtdGFicy1mZy1oZWFkaW5nOiBjb2xvci1mZy1oZWFkaW5nLFxuICByb3V0ZS10YWJzLWJnOiB0cmFuc3BhcmVudCxcbiAgcm91dGUtdGFicy1zZWxlY3RlZDogY29sb3Itc3VjY2VzcyxcbiAgcm91dGUtdGFicy1pY29uLW9ubHktbWF4LXdpZHRoOiA1NzZweCxcblxuICB1c2VyLWZvbnQtc2l6ZTogZm9udC1zaXplLFxuICB1c2VyLWxpbmUtaGVpZ2h0OiBsaW5lLWhlaWdodCxcbiAgdXNlci1iZzogY29sb3ItYmcsXG4gIHVzZXItZmc6IGNvbG9yLWZnLFxuICB1c2VyLWZnLWhpZ2hsaWdodDogI2JjYzNjYyxcbiAgdXNlci1mb250LWZhbWlseS1zZWNvbmRhcnk6IGZvbnQtc2Vjb25kYXJ5LFxuICB1c2VyLXNpemUtc21hbGw6IDEuNXJlbSxcbiAgdXNlci1zaXplLW1lZGl1bTogMi41cmVtLFxuICB1c2VyLXNpemUtbGFyZ2U6IDMuMjVyZW0sXG4gIHVzZXItc2l6ZS14bGFyZ2U6IDRyZW0sXG5cbiAgcG9wb3Zlci1mZzogY29sb3ItZmctaGVhZGluZyxcbiAgcG9wb3Zlci1iZzogY29sb3ItYmcsXG4gIHBvcG92ZXItYm9yZGVyOiBjb2xvci1zdWNjZXNzLFxuICBwb3BvdmVyLXNoYWRvdzogbm9uZSxcblxuICBjb250ZXh0LW1lbnUtZmc6IGNvbG9yLWZnLWhlYWRpbmcsXG4gIGNvbnRleHQtbWVudS1hY3RpdmUtZmc6IGNvbG9yLXdoaXRlLFxuICBjb250ZXh0LW1lbnUtYWN0aXZlLWJnOiBjb2xvci1zdWNjZXNzLFxuXG4gIGFjdGlvbnMtZm9udC1zaXplOiBmb250LXNpemUsXG4gIGFjdGlvbnMtZm9udC1mYW1pbHk6IGZvbnQtc2Vjb25kYXJ5LFxuICBhY3Rpb25zLWxpbmUtaGVpZ2h0OiBsaW5lLWhlaWdodCxcbiAgYWN0aW9ucy1mZzogY29sb3ItZmcsXG4gIGFjdGlvbnMtYmc6IGNvbG9yLWJnLFxuICBhY3Rpb25zLXNlcGFyYXRvcjogc2VwYXJhdG9yLFxuICBhY3Rpb25zLXBhZGRpbmc6IHBhZGRpbmcsXG4gIGFjdGlvbnMtc2l6ZS1zbWFsbDogMS41cmVtLFxuICBhY3Rpb25zLXNpemUtbWVkaXVtOiAyLjI1cmVtLFxuICBhY3Rpb25zLXNpemUtbGFyZ2U6IDMuNXJlbSxcblxuICBzZWFyY2gtYnRuLW9wZW4tZmc6IGNvbG9yLWZnLFxuICBzZWFyY2gtYnRuLWNsb3NlLWZnOlx0Y29sb3ItZmcsXG4gIHNlYXJjaC1iZzogbGF5b3V0LWJnLFxuICBzZWFyY2gtYmctc2Vjb25kYXJ5OiBjb2xvci1mZyxcbiAgc2VhcmNoLXRleHQ6IGNvbG9yLWZnLWhlYWRpbmcsXG4gIHNlYXJjaC1pbmZvOiBjb2xvci1mZyxcbiAgc2VhcmNoLWRhc2g6IGNvbG9yLWZnLFxuICBzZWFyY2gtcGxhY2Vob2xkZXI6IGNvbG9yLWZnLFxuXG4gIHNtYXJ0LXRhYmxlLWhlYWRlci1mb250LWZhbWlseTogZm9udC1zZWNvbmRhcnksXG4gIHNtYXJ0LXRhYmxlLWhlYWRlci1mb250LXNpemU6IGZvbnQtc2l6ZSxcbiAgc21hcnQtdGFibGUtaGVhZGVyLWZvbnQtd2VpZ2h0OiBmb250LXdlaWdodC1ib2xkLFxuICBzbWFydC10YWJsZS1oZWFkZXItbGluZS1oZWlnaHQ6IGxpbmUtaGVpZ2h0LFxuICBzbWFydC10YWJsZS1oZWFkZXItZmc6IGNvbG9yLWZnLWhlYWRpbmcsXG4gIHNtYXJ0LXRhYmxlLWhlYWRlci1iZzogY29sb3ItYmcsXG5cbiAgc21hcnQtdGFibGUtZm9udC1mYW1pbHk6IGZvbnQtbWFpbixcbiAgc21hcnQtdGFibGUtZm9udC1zaXplOiBmb250LXNpemUsXG4gIHNtYXJ0LXRhYmxlLWZvbnQtd2VpZ2h0OiBmb250LXdlaWdodC1ub3JtYWwsXG4gIHNtYXJ0LXRhYmxlLWxpbmUtaGVpZ2h0OiBsaW5lLWhlaWdodCxcbiAgc21hcnQtdGFibGUtZmc6IGNvbG9yLWZnLWhlYWRpbmcsXG4gIHNtYXJ0LXRhYmxlLWJnOiBjb2xvci1iZyxcblxuICBzbWFydC10YWJsZS1iZy1ldmVuOiAjZjVmN2ZjLFxuICBzbWFydC10YWJsZS1mZy1zZWNvbmRhcnk6IGNvbG9yLWZnLFxuICBzbWFydC10YWJsZS1iZy1hY3RpdmU6ICNlNmYzZmYsXG4gIHNtYXJ0LXRhYmxlLXBhZGRpbmc6IDAuODc1cmVtIDEuMjVyZW0sXG4gIHNtYXJ0LXRhYmxlLWZpbHRlci1wYWRkaW5nOiAwLjM3NXJlbSAwLjVyZW0sXG4gIHNtYXJ0LXRhYmxlLXNlcGFyYXRvcjogc2VwYXJhdG9yLFxuICBzbWFydC10YWJsZS1ib3JkZXItcmFkaXVzOiByYWRpdXMsXG5cbiAgc21hcnQtdGFibGUtcGFnaW5nLWJvcmRlci1jb2xvcjogc2VwYXJhdG9yLFxuICBzbWFydC10YWJsZS1wYWdpbmctYm9yZGVyLXdpZHRoOiAxcHgsXG4gIHNtYXJ0LXRhYmxlLXBhZ2luZy1mZy1hY3RpdmU6ICNmZmZmZmYsXG4gIHNtYXJ0LXRhYmxlLXBhZ2luZy1iZy1hY3RpdmU6IGNvbG9yLXN1Y2Nlc3MsXG4gIHNtYXJ0LXRhYmxlLXBhZ2luZy1ob3ZlcjogcmdiYSgwLCAwLCAwLCAwLjA1KSxcblxuICB0b2FzdGVyLWJnOiBjb2xvci1wcmltYXJ5LFxuICB0b2FzdGVyLWZnLWRlZmF1bHQ6IGNvbG9yLWludmVyc2UsXG4gIHRvYXN0ZXItYnRuLWNsb3NlLWJnOiB0cmFuc3BhcmVudCxcbiAgdG9hc3Rlci1idG4tY2xvc2UtZmc6IHRvYXN0ZXItZmctZGVmYXVsdCxcbiAgdG9hc3Rlci1zaGFkb3c6IHNoYWRvdyxcblxuICB0b2FzdGVyLWZnOiBjb2xvci13aGl0ZSxcbiAgdG9hc3Rlci1zdWNjZXNzOiBjb2xvci1zdWNjZXNzLFxuICB0b2FzdGVyLWluZm86IGNvbG9yLWluZm8sXG4gIHRvYXN0ZXItd2FybmluZzogY29sb3Itd2FybmluZyxcbiAgdG9hc3Rlci13YWl0OiBjb2xvci1wcmltYXJ5LFxuICB0b2FzdGVyLWVycm9yOiBjb2xvci1kYW5nZXIsXG5cbiAgYnRuLWZnOiBjb2xvci13aGl0ZSxcbiAgYnRuLWZvbnQtZmFtaWx5OiBmb250LXNlY29uZGFyeSxcbiAgYnRuLWxpbmUtaGVpZ2h0OiBsaW5lLWhlaWdodCxcbiAgYnRuLWRpc2FibGVkLW9wYWNpdHk6IDAuMyxcbiAgYnRuLWN1cnNvcjogZGVmYXVsdCxcblxuICBidG4tcHJpbWFyeS1iZzogY29sb3ItcHJpbWFyeSxcbiAgYnRuLXNlY29uZGFyeS1iZzogdHJhbnNwYXJlbnQsXG4gIGJ0bi1pbmZvLWJnOiBjb2xvci1pbmZvLFxuICBidG4tc3VjY2Vzcy1iZzogY29sb3Itc3VjY2VzcyxcbiAgYnRuLXdhcm5pbmctYmc6IGNvbG9yLXdhcm5pbmcsXG4gIGJ0bi1kYW5nZXItYmc6IGNvbG9yLWRhbmdlcixcblxuICBidG4tc2Vjb25kYXJ5LWJvcmRlcjogI2RhZGZlNixcbiAgYnRuLXNlY29uZGFyeS1ib3JkZXItd2lkdGg6IDJweCxcblxuICBidG4tcGFkZGluZy15LWxnOiAwLjg3NXJlbSxcbiAgYnRuLXBhZGRpbmcteC1sZzogMS43NXJlbSxcbiAgYnRuLWZvbnQtc2l6ZS1sZzogZm9udC1zaXplLWxnLFxuXG4gIC8vIGRlZmF1bHQgc2l6ZVxuICBidG4tcGFkZGluZy15LW1kOiAwLjc1cmVtLFxuICBidG4tcGFkZGluZy14LW1kOiAxLjVyZW0sXG4gIGJ0bi1mb250LXNpemUtbWQ6IDFyZW0sXG5cbiAgYnRuLXBhZGRpbmcteS1zbTogMC42MjVyZW0sXG4gIGJ0bi1wYWRkaW5nLXgtc206IDEuNXJlbSxcbiAgYnRuLWZvbnQtc2l6ZS1zbTogMC44NzVyZW0sXG5cbiAgYnRuLXBhZGRpbmcteS14czogMC41cmVtLFxuICBidG4tcGFkZGluZy14LXhzOiAxLjI1cmVtLFxuICBidG4tZm9udC1zaXplLXhzOiAwLjc1cmVtLFxuXG4gIGJ0bi1ib3JkZXItcmFkaXVzOiByYWRpdXMsXG4gIGJ0bi1yZWN0YW5nbGUtYm9yZGVyLXJhZGl1czogMC4yNXJlbSxcbiAgYnRuLXNlbWktcm91bmQtYm9yZGVyLXJhZGl1czogMC43NXJlbSxcbiAgYnRuLXJvdW5kLWJvcmRlci1yYWRpdXM6IDEuNXJlbSxcblxuICBidG4taGVyby1zaGFkb3c6IG5vbmUsXG4gIGJ0bi1oZXJvLXRleHQtc2hhZG93OiBub25lLFxuICBidG4taGVyby1iZXZlbC1zaXplOiAwIDAgMCAwLFxuICBidG4taGVyby1nbG93LXNpemU6IDAgMCAwIDAsXG4gIGJ0bi1oZXJvLXByaW1hcnktZ2xvdy1zaXplOiBidG4taGVyby1nbG93LXNpemUsXG4gIGJ0bi1oZXJvLXN1Y2Nlc3MtZ2xvdy1zaXplOiBidG4taGVyby1nbG93LXNpemUsXG4gIGJ0bi1oZXJvLXdhcm5pbmctZ2xvdy1zaXplOiBidG4taGVyby1nbG93LXNpemUsXG4gIGJ0bi1oZXJvLWluZm8tZ2xvdy1zaXplOiBidG4taGVyby1nbG93LXNpemUsXG4gIGJ0bi1oZXJvLWRhbmdlci1nbG93LXNpemU6IGJ0bi1oZXJvLWdsb3ctc2l6ZSxcbiAgYnRuLWhlcm8tc2Vjb25kYXJ5LWdsb3ctc2l6ZTogYnRuLWhlcm8tZ2xvdy1zaXplLFxuICBidG4taGVyby1kZWdyZWU6IDIwZGVnLFxuICBidG4taGVyby1wcmltYXJ5LWRlZ3JlZTogYnRuLWhlcm8tZGVncmVlLFxuICBidG4taGVyby1zdWNjZXNzLWRlZ3JlZTogYnRuLWhlcm8tZGVncmVlLFxuICBidG4taGVyby13YXJuaW5nLWRlZ3JlZTogMTBkZWcsXG4gIGJ0bi1oZXJvLWluZm8tZGVncmVlOiAtMTBkZWcsXG4gIGJ0bi1oZXJvLWRhbmdlci1kZWdyZWU6IC0yMGRlZyxcbiAgYnRuLWhlcm8tc2Vjb25kYXJ5LWRlZ3JlZTogYnRuLWhlcm8tZGVncmVlLFxuICBidG4taGVyby1ib3JkZXItcmFkaXVzOiByYWRpdXMsXG5cbiAgYnRuLW91dGxpbmUtZmc6IGNvbG9yLWZnLWhlYWRpbmcsXG4gIGJ0bi1vdXRsaW5lLWhvdmVyLWZnOiAjZmZmZmZmLFxuICBidG4tb3V0bGluZS1mb2N1cy1mZzogY29sb3ItZmctaGVhZGluZyxcblxuICBidG4tZ3JvdXAtYmc6IGxheW91dC1iZyxcbiAgYnRuLWdyb3VwLWZnOiBjb2xvci1mZy1oZWFkaW5nLFxuICBidG4tZ3JvdXAtc2VwYXJhdG9yOiAjZGFkZmU2LFxuXG4gIGZvcm0tY29udHJvbC10ZXh0LXByaW1hcnktY29sb3I6IGNvbG9yLWZnLWhlYWRpbmcsXG4gIGZvcm0tY29udHJvbC1iZzogY29sb3ItYmcsXG4gIGZvcm0tY29udHJvbC1mb2N1cy1iZzogY29sb3ItYmcsXG5cbiAgZm9ybS1jb250cm9sLWJvcmRlci13aWR0aDogMnB4LFxuICBmb3JtLWNvbnRyb2wtYm9yZGVyLXR5cGU6IHNvbGlkLFxuICBmb3JtLWNvbnRyb2wtYm9yZGVyLXJhZGl1czogcmFkaXVzLFxuICBmb3JtLWNvbnRyb2wtc2VtaS1yb3VuZC1ib3JkZXItcmFkaXVzOiAwLjc1cmVtLFxuICBmb3JtLWNvbnRyb2wtcm91bmQtYm9yZGVyLXJhZGl1czogMS41cmVtLFxuICBmb3JtLWNvbnRyb2wtYm9yZGVyLWNvbG9yOiAjZGFkZmU2LFxuICBmb3JtLWNvbnRyb2wtc2VsZWN0ZWQtYm9yZGVyLWNvbG9yOiBjb2xvci1zdWNjZXNzLFxuXG4gIGZvcm0tY29udHJvbC1pbmZvLWJvcmRlci1jb2xvcjogY29sb3ItaW5mbyxcbiAgZm9ybS1jb250cm9sLXN1Y2Nlc3MtYm9yZGVyLWNvbG9yOiBjb2xvci1zdWNjZXNzLFxuICBmb3JtLWNvbnRyb2wtZGFuZ2VyLWJvcmRlci1jb2xvcjogY29sb3ItZGFuZ2VyLFxuICBmb3JtLWNvbnRyb2wtd2FybmluZy1ib3JkZXItY29sb3I6IGNvbG9yLXdhcm5pbmcsXG5cbiAgZm9ybS1jb250cm9sLXBsYWNlaG9sZGVyLWNvbG9yOiBjb2xvci1mZyxcbiAgZm9ybS1jb250cm9sLXBsYWNlaG9sZGVyLWZvbnQtc2l6ZTogMXJlbSxcblxuICBmb3JtLWNvbnRyb2wtZm9udC1zaXplOiAxcmVtLFxuICBmb3JtLWNvbnRyb2wtcGFkZGluZzogMC43NXJlbSAxLjEyNXJlbSxcbiAgZm9ybS1jb250cm9sLWZvbnQtc2l6ZS1zbTogZm9udC1zaXplLXNtLFxuICBmb3JtLWNvbnRyb2wtcGFkZGluZy1zbTogMC4zNzVyZW0gMS4xMjVyZW0sXG4gIGZvcm0tY29udHJvbC1mb250LXNpemUtbGc6IGZvbnQtc2l6ZS1sZyxcbiAgZm9ybS1jb250cm9sLXBhZGRpbmctbGc6IDEuMTI1cmVtLFxuXG4gIGZvcm0tY29udHJvbC1sYWJlbC1mb250LXdlaWdodDogNDAwLFxuXG4gIGZvcm0tY29udHJvbC1mZWVkYmFjay1mb250LXNpemU6IDAuODc1cmVtLFxuICBmb3JtLWNvbnRyb2wtZmVlZGJhY2stZm9udC13ZWlnaHQ6IGZvbnQtd2VpZ2h0LW5vcm1hbCxcblxuICBjaGVja2JveC1iZzogdHJhbnNwYXJlbnQsXG4gIGNoZWNrYm94LXNpemU6IDEuMjVyZW0sXG4gIGNoZWNrYm94LWJvcmRlci1zaXplOiAycHgsXG4gIGNoZWNrYm94LWJvcmRlci1jb2xvcjogZm9ybS1jb250cm9sLWJvcmRlci1jb2xvcixcbiAgY2hlY2tib3gtY2hlY2ttYXJrOiB0cmFuc3BhcmVudCxcblxuICBjaGVja2JveC1jaGVja2VkLWJnOiB0cmFuc3BhcmVudCxcbiAgY2hlY2tib3gtY2hlY2tlZC1zaXplOiAxLjI1cmVtLFxuICBjaGVja2JveC1jaGVja2VkLWJvcmRlci1zaXplOiAycHgsXG4gIGNoZWNrYm94LWNoZWNrZWQtYm9yZGVyLWNvbG9yOiBjb2xvci1zdWNjZXNzLFxuICBjaGVja2JveC1jaGVja2VkLWNoZWNrbWFyazogY29sb3ItZmctaGVhZGluZyxcblxuICBjaGVja2JveC1kaXNhYmxlZC1iZzogdHJhbnNwYXJlbnQsXG4gIGNoZWNrYm94LWRpc2FibGVkLXNpemU6IDEuMjVyZW0sXG4gIGNoZWNrYm94LWRpc2FibGVkLWJvcmRlci1zaXplOiAycHgsXG4gIGNoZWNrYm94LWRpc2FibGVkLWJvcmRlci1jb2xvcjogY29sb3ItZmctaGVhZGluZyxcbiAgY2hlY2tib3gtZGlzYWJsZWQtY2hlY2ttYXJrOiBjb2xvci1mZy1oZWFkaW5nLFxuXG4gIHJhZGlvLWZnOiBjb2xvci1zdWNjZXNzLFxuXG4gIG1vZGFsLWZvbnQtc2l6ZTogZm9udC1zaXplLFxuICBtb2RhbC1saW5lLWhlaWdodDogbGluZS1oZWlnaHQsXG4gIG1vZGFsLWZvbnQtd2VpZ2h0OiBmb250LXdlaWdodC1ub3JtYWwsXG4gIG1vZGFsLWZnOiBjb2xvci1mZy10ZXh0LFxuICBtb2RhbC1mZy1oZWFkaW5nOiBjb2xvci1mZy1oZWFkaW5nLFxuICBtb2RhbC1iZzogY29sb3ItYmcsXG4gIG1vZGFsLWJvcmRlcjogdHJhbnNwYXJlbnQsXG4gIG1vZGFsLWJvcmRlci1yYWRpdXM6IHJhZGl1cyxcbiAgbW9kYWwtcGFkZGluZzogcGFkZGluZyxcbiAgbW9kYWwtaGVhZGVyLWZvbnQtZmFtaWx5OiBmb250LXNlY29uZGFyeSxcbiAgbW9kYWwtaGVhZGVyLWZvbnQtd2VpZ2h0OiBmb250LXdlaWdodC1ib2xkZXIsXG4gIG1vZGFsLWhlYWRlci1mb250LXNpemU6IGZvbnQtc2l6ZS1sZyxcbiAgbW9kYWwtYm9keS1mb250LWZhbWlseTogZm9udC1tYWluLFxuICBtb2RhbC1ib2R5LWZvbnQtd2VpZ2h0OiBmb250LXdlaWdodC1ub3JtYWwsXG4gIG1vZGFsLWJvZHktZm9udC1zaXplOiBmb250LXNpemUsXG4gIG1vZGFsLXNlcGFyYXRvcjogc2VwYXJhdG9yLFxuXG4gIGJhZGdlLWZnLXRleHQ6IGNvbG9yLXdoaXRlLFxuICBiYWRnZS1wcmltYXJ5LWJnLWNvbG9yOiBjb2xvci1wcmltYXJ5LFxuICBiYWRnZS1zdWNjZXNzLWJnLWNvbG9yOiBjb2xvci1zdWNjZXNzLFxuICBiYWRnZS1pbmZvLWJnLWNvbG9yOiBjb2xvci1pbmZvLFxuICBiYWRnZS13YXJuaW5nLWJnLWNvbG9yOiBjb2xvci13YXJuaW5nLFxuICBiYWRnZS1kYW5nZXItYmctY29sb3I6IGNvbG9yLWRhbmdlcixcblxuICBwcm9ncmVzcy1iYXItaGVpZ2h0LXhsZzogMS43NXJlbSxcbiAgcHJvZ3Jlc3MtYmFyLWhlaWdodC1sZzogMS41cmVtLFxuICBwcm9ncmVzcy1iYXItaGVpZ2h0OiAxLjM3NXJlbSxcbiAgcHJvZ3Jlc3MtYmFyLWhlaWdodC1zbTogMS4yNXJlbSxcbiAgcHJvZ3Jlc3MtYmFyLWhlaWdodC14czogMXJlbSxcbiAgcHJvZ3Jlc3MtYmFyLWFuaW1hdGlvbi1kdXJhdGlvbjogNDAwbXMsXG4gIHByb2dyZXNzLWJhci1mb250LXNpemUteGxnOiBmb250LXNpemUteGxnLFxuICBwcm9ncmVzcy1iYXItZm9udC1zaXplLWxnOiBmb250LXNpemUtbGcsXG4gIHByb2dyZXNzLWJhci1mb250LXNpemU6IGZvbnQtc2l6ZSxcbiAgcHJvZ3Jlc3MtYmFyLWZvbnQtc2l6ZS1zbTogZm9udC1zaXplLXNtLFxuICBwcm9ncmVzcy1iYXItZm9udC1zaXplLXhzOiBmb250LXNpemUteHMsXG4gIHByb2dyZXNzLWJhci1yYWRpdXM6IHJhZGl1cyxcbiAgcHJvZ3Jlc3MtYmFyLWJnOiBsYXlvdXQtYmcsXG4gIHByb2dyZXNzLWJhci1mb250LWNvbG9yOiBjb2xvci13aGl0ZSxcbiAgcHJvZ3Jlc3MtYmFyLWZvbnQtd2VpZ2h0OiBmb250LXdlaWdodC1ib2xkLFxuICBwcm9ncmVzcy1iYXItZGVmYXVsdC1iZzogY29sb3ItaW5mbyxcbiAgcHJvZ3Jlc3MtYmFyLXByaW1hcnktYmc6IGNvbG9yLXByaW1hcnksXG4gIHByb2dyZXNzLWJhci1zdWNjZXNzLWJnOiBjb2xvci1zdWNjZXNzLFxuICBwcm9ncmVzcy1iYXItaW5mby1iZzogY29sb3ItaW5mbyxcbiAgcHJvZ3Jlc3MtYmFyLXdhcm5pbmctYmc6IGNvbG9yLXdhcm5pbmcsXG4gIHByb2dyZXNzLWJhci1kYW5nZXItYmc6IGNvbG9yLWRhbmdlcixcblxuICBhbGVydC1mb250LXNpemU6IGZvbnQtc2l6ZSxcbiAgYWxlcnQtbGluZS1oZWlnaHQ6IGxpbmUtaGVpZ2h0LFxuICBhbGVydC1mb250LXdlaWdodDogZm9udC13ZWlnaHQtbm9ybWFsLFxuICBhbGVydC1mZzogY29sb3Itd2hpdGUsXG4gIGFsZXJ0LW91dGxpbmUtZmc6IGNvbG9yLWZnLFxuICBhbGVydC1iZzogY29sb3ItYmcsXG4gIGFsZXJ0LWFjdGl2ZS1iZzogY29sb3ItZmcsXG4gIGFsZXJ0LWRpc2FibGVkLWJnOiBjb2xvci1kaXNhYmxlZCxcbiAgYWxlcnQtZGlzYWJsZWQtZmc6IGNvbG9yLWZnLFxuICBhbGVydC1wcmltYXJ5LWJnOiBjb2xvci1wcmltYXJ5LFxuICBhbGVydC1pbmZvLWJnOiBjb2xvci1pbmZvLFxuICBhbGVydC1zdWNjZXNzLWJnOiBjb2xvci1zdWNjZXNzLFxuICBhbGVydC13YXJuaW5nLWJnOiBjb2xvci13YXJuaW5nLFxuICBhbGVydC1kYW5nZXItYmc6IGNvbG9yLWRhbmdlcixcbiAgYWxlcnQtaGVpZ2h0LXh4c21hbGw6IDUycHgsXG4gIGFsZXJ0LWhlaWdodC14c21hbGw6IDcycHgsXG4gIGFsZXJ0LWhlaWdodC1zbWFsbDogOTJweCxcbiAgYWxlcnQtaGVpZ2h0LW1lZGl1bTogMTEycHgsXG4gIGFsZXJ0LWhlaWdodC1sYXJnZTogMTMycHgsXG4gIGFsZXJ0LWhlaWdodC14bGFyZ2U6IDE1MnB4LFxuICBhbGVydC1oZWlnaHQteHhsYXJnZTogMTcycHgsXG4gIGFsZXJ0LXNoYWRvdzogbm9uZSxcbiAgYWxlcnQtYm9yZGVyLXJhZGl1czogcmFkaXVzLFxuICBhbGVydC1wYWRkaW5nOiAxcmVtIDEuMTI1cmVtLFxuICBhbGVydC1jbG9zYWJsZS1wYWRkaW5nOiAzcmVtLFxuICBhbGVydC1idXR0b24tcGFkZGluZzogM3JlbSxcbiAgYWxlcnQtbWFyZ2luOiBtYXJnaW4sXG5cbiAgY2hhdC1mb250LXNpemU6IGZvbnQtc2l6ZSxcbiAgY2hhdC1mZzogY29sb3Itd2hpdGUsXG4gIGNoYXQtYmc6IGNvbG9yLWJnLFxuICBjaGF0LWJvcmRlci1yYWRpdXM6IHJhZGl1cyxcbiAgY2hhdC1mZy10ZXh0OiBjb2xvci1mZy10ZXh0LFxuICBjaGF0LWhlaWdodC14eHNtYWxsOiA5NnB4LFxuICBjaGF0LWhlaWdodC14c21hbGw6IDIxNnB4LFxuICBjaGF0LWhlaWdodC1zbWFsbDogMzM2cHgsXG4gIGNoYXQtaGVpZ2h0LW1lZGl1bTogNDU2cHgsXG4gIGNoYXQtaGVpZ2h0LWxhcmdlOiA1NzZweCxcbiAgY2hhdC1oZWlnaHQteGxhcmdlOiA2OTZweCxcbiAgY2hhdC1oZWlnaHQteHhsYXJnZTogODE2cHgsXG4gIGNoYXQtYm9yZGVyOiBib3JkZXIsXG4gIGNoYXQtcGFkZGluZzogcGFkZGluZyxcbiAgY2hhdC1zaGFkb3c6IHNoYWRvdyxcbiAgY2hhdC1zZXBhcmF0b3I6IHNlcGFyYXRvcixcbiAgY2hhdC1tZXNzYWdlLWZnOiBjb2xvci13aGl0ZSxcbiAgY2hhdC1tZXNzYWdlLWJnOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICM0Y2E2ZmYsICM1OWJmZmYpLFxuICBjaGF0LW1lc3NhZ2UtcmVwbHktYmc6IGNvbG9yLWJnLWFjdGl2ZSxcbiAgY2hhdC1tZXNzYWdlLXJlcGx5LWZnOiBjb2xvci1mZy10ZXh0LFxuICBjaGF0LW1lc3NhZ2UtYXZhdGFyLWJnOiBjb2xvci1mZyxcbiAgY2hhdC1tZXNzYWdlLXNlbmRlci1mZzogY29sb3ItZmcsXG4gIGNoYXQtbWVzc2FnZS1xdW90ZS1mZzogY29sb3ItZmcsXG4gIGNoYXQtbWVzc2FnZS1xdW90ZS1iZzogY29sb3ItYmctYWN0aXZlLFxuICBjaGF0LW1lc3NhZ2UtZmlsZS1mZzogY29sb3ItZmcsXG4gIGNoYXQtbWVzc2FnZS1maWxlLWJnOiB0cmFuc3BhcmVudCxcbiAgY2hhdC1mb3JtLWJnOiB0cmFuc3BhcmVudCxcbiAgY2hhdC1mb3JtLWZnOiBjb2xvci1mZy1oZWFkaW5nLFxuICBjaGF0LWZvcm0tYm9yZGVyOiBzZXBhcmF0b3IsXG4gIGNoYXQtZm9ybS1wbGFjZWhvbGRlci1mZzogY29sb3ItZmcsXG4gIGNoYXQtZm9ybS1hY3RpdmUtYm9yZGVyOiBjb2xvci1mZyxcbiAgY2hhdC1hY3RpdmUtYmc6IGNvbG9yLWZnLFxuICBjaGF0LWRpc2FibGVkLWJnOiBjb2xvci1kaXNhYmxlZCxcbiAgY2hhdC1kaXNhYmxlZC1mZzogY29sb3ItZmcsXG4gIGNoYXQtcHJpbWFyeS1iZzogY29sb3ItcHJpbWFyeSxcbiAgY2hhdC1pbmZvLWJnOiBjb2xvci1pbmZvLFxuICBjaGF0LXN1Y2Nlc3MtYmc6IGNvbG9yLXN1Y2Nlc3MsXG4gIGNoYXQtd2FybmluZy1iZzogY29sb3Itd2FybmluZyxcbiAgY2hhdC1kYW5nZXItYmc6IGNvbG9yLWRhbmdlcixcblxuICBzcGlubmVyLWJnOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuODMpLFxuICBzcGlubmVyLWNpcmNsZS1iZzogY29sb3ItYmctYWN0aXZlLFxuICBzcGlubmVyLWZnOiBjb2xvci1mZy10ZXh0LFxuICBzcGlubmVyLWFjdGl2ZS1iZzogY29sb3ItZmcsXG4gIHNwaW5uZXItZGlzYWJsZWQtYmc6IGNvbG9yLWRpc2FibGVkLFxuICBzcGlubmVyLWRpc2FibGVkLWZnOiBjb2xvci1mZyxcbiAgc3Bpbm5lci1wcmltYXJ5LWJnOiBjb2xvci1wcmltYXJ5LFxuICBzcGlubmVyLWluZm8tYmc6IGNvbG9yLWluZm8sXG4gIHNwaW5uZXItc3VjY2Vzcy1iZzogY29sb3Itc3VjY2VzcyxcbiAgc3Bpbm5lci13YXJuaW5nLWJnOiBjb2xvci13YXJuaW5nLFxuICBzcGlubmVyLWRhbmdlci1iZzogY29sb3ItZGFuZ2VyLFxuICBzcGlubmVyLXh4c21hbGw6IDEuMjVyZW0sXG4gIHNwaW5uZXIteHNtYWxsOiAxLjVyZW0sXG4gIHNwaW5uZXItc21hbGw6IDEuNzVyZW0sXG4gIHNwaW5uZXItbWVkaXVtOiAycmVtLFxuICBzcGlubmVyLWxhcmdlOiAyLjI1cmVtLFxuICBzcGlubmVyLXhsYXJnZTogMi41cmVtLFxuICBzcGlubmVyLXh4bGFyZ2U6IDNyZW0sXG5cbiAgc3RlcHBlci1pbmRleC1zaXplOiAycmVtLFxuICBzdGVwcGVyLWxhYmVsLWZvbnQtc2l6ZTogZm9udC1zaXplLXNtLFxuICBzdGVwcGVyLWxhYmVsLWZvbnQtd2VpZ2h0OiBmb250LXdlaWdodC1ib2xkZXIsXG4gIHN0ZXBwZXItYWNjZW50LWNvbG9yOiBjb2xvci1wcmltYXJ5LFxuICBzdGVwcGVyLWNvbXBsZXRlZC1mZzogY29sb3Itd2hpdGUsXG4gIHN0ZXBwZXItZmc6IGNvbG9yLWZnLFxuICBzdGVwcGVyLWNvbXBsZXRlZC1pY29uLXNpemU6IDEuNXJlbSxcbiAgc3RlcHBlci1jb21wbGV0ZWQtaWNvbi13ZWlnaHQ6IGZvbnQtd2VpZ2h0LXVsdHJhLWJvbGQsXG4gIHN0ZXBwZXItc3RlcC1wYWRkaW5nOiBwYWRkaW5nLFxuXG4gIGFjY29yZGlvbi1wYWRkaW5nOiBwYWRkaW5nLFxuICBhY2NvcmRpb24tc2VwYXJhdG9yOiBzZXBhcmF0b3IsXG4gIGFjY29yZGlvbi1oZWFkZXItZm9udC1mYW1pbHk6IGZvbnQtc2Vjb25kYXJ5LFxuICBhY2NvcmRpb24taGVhZGVyLWZvbnQtc2l6ZTogZm9udC1zaXplLWxnLFxuICBhY2NvcmRpb24taGVhZGVyLWZvbnQtd2VpZ2h0OiBmb250LXdlaWdodC1ub3JtYWwsXG4gIGFjY29yZGlvbi1oZWFkZXItZmctaGVhZGluZzogY29sb3ItZmctaGVhZGluZyxcbiAgYWNjb3JkaW9uLWhlYWRlci1kaXNhYmxlZC1mZzogY29sb3ItZmcsXG4gIGFjY29yZGlvbi1oZWFkZXItYm9yZGVyLXdpZHRoOiAxcHgsXG4gIGFjY29yZGlvbi1oZWFkZXItYm9yZGVyLXR5cGU6IHNvbGlkLFxuICBhY2NvcmRpb24taGVhZGVyLWJvcmRlci1jb2xvcjogYWNjb3JkaW9uLXNlcGFyYXRvcixcbiAgYWNjb3JkaW9uLWJvcmRlci1yYWRpdXM6IHJhZGl1cyxcbiAgYWNjb3JkaW9uLWl0ZW0tYmc6IGNvbG9yLWJnLFxuICBhY2NvcmRpb24taXRlbS1mb250LXNpemU6IGZvbnQtc2l6ZSxcbiAgYWNjb3JkaW9uLWl0ZW0tZm9udC13ZWlnaHQ6IGZvbnQtd2VpZ2h0LW5vcm1hbCxcbiAgYWNjb3JkaW9uLWl0ZW0tZm9udC1mYW1pbHk6IGZvbnQtbWFpbixcbiAgYWNjb3JkaW9uLWl0ZW0tZmctdGV4dDogY29sb3ItZmctdGV4dCxcbiAgYWNjb3JkaW9uLWl0ZW0tc2hhZG93OiBzaGFkb3csXG5cbiAgbGlzdC1pdGVtLWJvcmRlci1jb2xvcjogdGFicy1zZXBhcmF0b3IsXG4gIGxpc3QtaXRlbS1wYWRkaW5nOiAxcmVtLFxuXG4gIGNhbGVuZGFyLXdpZHRoOiAyMS44NzVyZW0sXG4gIGNhbGVuZGFyLWhlaWdodDogMzFyZW0sXG4gIGNhbGVuZGFyLWhlYWRlci10aXRsZS1mb250LXNpemU6IGZvbnQtc2l6ZS14bGcsXG4gIGNhbGVuZGFyLWhlYWRlci10aXRsZS1mb250LXdlaWdodDogZm9udC13ZWlnaHQtYm9sZCxcbiAgY2FsZW5kYXItaGVhZGVyLXN1Yi10aXRsZS1mb250LXNpemU6IGZvbnQtc2l6ZSxcbiAgY2FsZW5kYXItaGVhZGVyLXN1Yi10aXRsZS1mb250LXdlaWdodDogZm9udC13ZWlnaHQtdGhpbixcbiAgY2FsZW5kYXItbmF2aWdhdGlvbi1idXR0b24td2lkdGg6IDEwcmVtLFxuICBjYWxlbmRhci1zZWxlY3RlZC1pdGVtLWJnOiBjb2xvci1zdWNjZXNzLFxuICBjYWxlbmRhci1ob3Zlci1pdGVtLWJnOiBjYWxlbmRhci1zZWxlY3RlZC1pdGVtLWJnLFxuICBjYWxlbmRhci10b2RheS1pdGVtLWJnOiBjb2xvci1iZy1hY3RpdmUsXG4gIGNhbGVuZGFyLWFjdGl2ZS1pdGVtLWJnOiBjb2xvci1zdWNjZXNzLFxuICBjYWxlbmRhci1mZzogY29sb3ItZmctdGV4dCxcbiAgY2FsZW5kYXItc2VsZWN0ZWQtZmc6IGNvbG9yLXdoaXRlLFxuICBjYWxlbmRhci10b2RheS1mZzogY2FsZW5kYXItZmcsXG4gIGNhbGVuZGFyLWRheS1jZWxsLXdpZHRoOiAyLjYyNXJlbSxcbiAgY2FsZW5kYXItZGF5LWNlbGwtaGVpZ2h0OiAyLjYyNXJlbSxcbiAgY2FsZW5kYXItbW9udGgtY2VsbC13aWR0aDogNC4yNXJlbSxcbiAgY2FsZW5kYXItbW9udGgtY2VsbC1oZWlnaHQ6IDIuMzc1cmVtLFxuICBjYWxlbmRhci15ZWFyLWNlbGwtd2lkdGg6IGNhbGVuZGFyLW1vbnRoLWNlbGwtd2lkdGgsXG4gIGNhbGVuZGFyLXllYXItY2VsbC1oZWlnaHQ6IGNhbGVuZGFyLW1vbnRoLWNlbGwtaGVpZ2h0LFxuICBjYWxlbmRhci1pbmFjdGl2ZS1vcGFjaXR5OiAwLjUsXG4gIGNhbGVuZGFyLWRpc2FibGVkLW9wYWNpdHk6IDAuMyxcbiAgY2FsZW5kYXItYm9yZGVyLXJhZGl1czogcmFkaXVzLFxuICBjYWxlbmRhci13ZWVrZGF5LXdpZHRoOiBjYWxlbmRhci1kYXktY2VsbC13aWR0aCxcbiAgY2FsZW5kYXItd2Vla2RheS1oZWlnaHQ6IDEuNzVyZW0sXG4gIGNhbGVuZGFyLXdlZWtkYXktZm9udC1zaXplOiBmb250LXNpemUteHMsXG4gIGNhbGVuZGFyLXdlZWtkYXktZm9udC13ZWlnaHQ6IGZvbnQtd2VpZ2h0LW5vcm1hbCxcbiAgY2FsZW5kYXItd2Vla2RheS1mZzogY29sb3ItZmcsXG4gIGNhbGVuZGFyLXdlZWtkYXktaG9saWRheS1mZzogY29sb3ItZGFuZ2VyLFxuICBjYWxlbmRhci1yYW5nZS1iZy1pbi1yYW5nZTogI2ViZmJmMixcblxuICBjYWxlbmRhci1sYXJnZS13aWR0aDogMjQuMzc1cmVtLFxuICBjYWxlbmRhci1sYXJnZS1oZWlnaHQ6IDMzLjEyNXJlbSxcbiAgY2FsZW5kYXItZGF5LWNlbGwtbGFyZ2Utd2lkdGg6IDNyZW0sXG4gIGNhbGVuZGFyLWRheS1jZWxsLWxhcmdlLWhlaWdodDogM3JlbSxcbiAgY2FsZW5kYXItbW9udGgtY2VsbC1sYXJnZS13aWR0aDogNC4yNXJlbSxcbiAgY2FsZW5kYXItbW9udGgtY2VsbC1sYXJnZS1oZWlnaHQ6IDIuMzc1cmVtLFxuICBjYWxlbmRhci15ZWFyLWNlbGwtbGFyZ2Utd2lkdGg6IGNhbGVuZGFyLW1vbnRoLWNlbGwtd2lkdGgsXG4gIGNhbGVuZGFyLXllYXItY2VsbC1sYXJnZS1oZWlnaHQ6IGNhbGVuZGFyLW1vbnRoLWNlbGwtaGVpZ2h0LFxuKTtcblxuLy8gcmVnaXN0ZXIgdGhlIHRoZW1lXG4kbmItdGhlbWVzOiBuYi1yZWdpc3Rlci10aGVtZSgkdGhlbWUsIGRlZmF1bHQpO1xuIiwiLyoqXG4gKiBAbGljZW5zZVxuICogQ29weXJpZ2h0IEFrdmVvLiBBbGwgUmlnaHRzIFJlc2VydmVkLlxuICogTGljZW5zZWQgdW5kZXIgdGhlIE1JVCBMaWNlbnNlLiBTZWUgTGljZW5zZS50eHQgaW4gdGhlIHByb2plY3Qgcm9vdCBmb3IgbGljZW5zZSBpbmZvcm1hdGlvbi5cbiAqL1xuXG5AaW1wb3J0ICcuLi9jb3JlL2Z1bmN0aW9ucyc7XG5AaW1wb3J0ICcuLi9jb3JlL21peGlucyc7XG5AaW1wb3J0ICdkZWZhdWx0JztcblxuLy8gZGVmYXVsdCB0aGUgYmFzZSB0aGVtZVxuJHRoZW1lOiAoXG4gIHJhZGl1czogMC41cmVtLFxuXG4gIGNvbG9yLWJnOiAjM2QzNzgwLFxuICBjb2xvci1iZy1hY3RpdmU6ICM0OTQyOTksXG4gIGNvbG9yLWZnOiAjYTFhMWU1LFxuICBjb2xvci1mZy1oZWFkaW5nOiAjZmZmZmZmLFxuICBjb2xvci1mZy10ZXh0OiAjZDFkMWZmLFxuICBjb2xvci1mZy1oaWdobGlnaHQ6ICMwMGY5YTYsXG5cbiAgY29sb3ItZ3JheTogcmdiYSg4MSwgMTEzLCAxNjUsIDAuMTUpLFxuICBjb2xvci1uZXV0cmFsOiB0cmFuc3BhcmVudCxcbiAgY29sb3Itd2hpdGU6ICNmZmZmZmYsXG4gIGNvbG9yLWRpc2FibGVkOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuNCksXG5cbiAgY29sb3ItcHJpbWFyeTogIzc2NTlmZixcbiAgY29sb3Itc3VjY2VzczogIzAwZDk3NyxcbiAgY29sb3ItaW5mbzogIzAwODhmZixcbiAgY29sb3Itd2FybmluZzogI2ZmYTEwMCxcbiAgY29sb3ItZGFuZ2VyOiAjZmYzODZhLFxuXG4gIGxpbmstY29sb3I6ICMwMGY5YTYsXG4gIGxpbmstY29sb3ItaG92ZXI6ICMxNGZmYmUsXG5cbiAgc2VwYXJhdG9yOiAjMzQyZTczLFxuICBzaGFkb3c6IDAgOHB4IDIwcHggMCByZ2JhKDQwLCAzNywgODksIDAuNiksXG5cbiAgY2FyZC1oZWFkZXItZm9udC13ZWlnaHQ6IGZvbnQtd2VpZ2h0LWJvbGRlcixcblxuICBsYXlvdXQtYmc6ICMyZjI5NmIsXG5cbiAgc2Nyb2xsYmFyLWZnOiAjNTU0ZGIzLFxuICBzY3JvbGxiYXItYmc6ICMzMzJlNzMsXG5cbiAgcmFkaWFsLWdyYWRpZW50OiByYWRpYWwtZ3JhZGllbnQoY2lyY2xlIGF0IDUwJSA1MCUsICM0MjNmOGMsICMzMDJjNmUpLFxuICBsaW5lYXItZ3JhZGllbnQ6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgIzE3MTc0OSwgIzQxMzc4OSksXG5cbiAgc2lkZWJhci1mZzogY29sb3Itc2Vjb25kYXJ5LFxuICBzaWRlYmFyLWJnOiBjb2xvci1iZyxcblxuICBoZWFkZXItZmc6IGNvbG9yLXdoaXRlLFxuICBoZWFkZXItYmc6IGNvbG9yLWJnLFxuXG4gIGZvb3Rlci1mZzogY29sb3ItZmcsXG4gIGZvb3Rlci1iZzogY29sb3ItYmcsXG5cbiAgYWN0aW9ucy1mZzogY29sb3ItZmcsXG4gIGFjdGlvbnMtYmc6IGNvbG9yLWJnLFxuXG4gIHVzZXItZmc6IGNvbG9yLWJnLFxuICB1c2VyLWJnOiBjb2xvci1mZyxcbiAgdXNlci1mZy1oaWdobGlnaHQ6IGNvbG9yLWZnLWhpZ2hsaWdodCxcblxuICBwb3BvdmVyLWJvcmRlcjogY29sb3ItcHJpbWFyeSxcbiAgcG9wb3Zlci1zaGFkb3c6IHNoYWRvdyxcblxuICBjb250ZXh0LW1lbnUtYWN0aXZlLWJnOiBjb2xvci1wcmltYXJ5LFxuXG4gIGZvb3Rlci1oZWlnaHQ6IGhlYWRlci1oZWlnaHQsXG5cbiAgc2lkZWJhci13aWR0aDogMTYuMjVyZW0sXG4gIHNpZGViYXItd2lkdGgtY29tcGFjdDogMy40NXJlbSxcblxuICBtZW51LWZnOiBjb2xvci1mZyxcbiAgbWVudS1iZzogY29sb3ItYmcsXG4gIG1lbnUtYWN0aXZlLWZnOiBjb2xvci13aGl0ZSxcbiAgbWVudS1ncm91cC1mZzogY29sb3Itd2hpdGUsXG4gIG1lbnUtZm9udC13ZWlnaHQ6IGZvbnQtd2VpZ2h0LW5vcm1hbCxcbiAgbWVudS1hY3RpdmUtZm9udC13ZWlnaHQ6IGZvbnQtd2VpZ2h0LWJvbGRlcixcbiAgbWVudS1zdWJtZW51LWJnOiBsYXlvdXQtYmcsXG4gIG1lbnUtc3VibWVudS1mZzogY29sb3ItZmcsXG4gIG1lbnUtc3VibWVudS1hY3RpdmUtZmc6IGNvbG9yLWZnLWhlYWRpbmcsXG4gIG1lbnUtc3VibWVudS1hY3RpdmUtYmc6IHJnYmEoMCwgMjU1LCAxNzAsIDAuMjUpLFxuICBtZW51LXN1Ym1lbnUtYWN0aXZlLWJvcmRlci1jb2xvcjogY29sb3ItZmctaGlnaGxpZ2h0LFxuICBtZW51LXN1Ym1lbnUtYWN0aXZlLXNoYWRvdzogMCAycHggMTJweCAwIHJnYmEoMCwgMjU1LCAxNzAsIDAuMjUpLFxuICBtZW51LWl0ZW0tcGFkZGluZzogMC4yNXJlbSAwLjc1cmVtLFxuICBtZW51LWl0ZW0tc2VwYXJhdG9yOiB0cmFuc3BhcmVudCxcblxuICBidG4taGVyby1zaGFkb3c6IDAgNHB4IDEwcHggMCByZ2JhKDMzLCA3LCA3NywgMC41KSxcbiAgYnRuLWhlcm8tdGV4dC1zaGFkb3c6IDAgMXB4IDNweCByZ2JhKDAsIDAsIDAsIDAuMyksXG4gIGJ0bi1oZXJvLWJldmVsLXNpemU6IDAgM3B4IDAgMCxcbiAgYnRuLWhlcm8tZ2xvdy1zaXplOiAwIDJweCA4cHggMCxcbiAgYnRuLWhlcm8tcHJpbWFyeS1nbG93LXNpemU6IGJ0bi1oZXJvLWdsb3ctc2l6ZSxcbiAgYnRuLWhlcm8tc3VjY2Vzcy1nbG93LXNpemU6IGJ0bi1oZXJvLWdsb3ctc2l6ZSxcbiAgYnRuLWhlcm8td2FybmluZy1nbG93LXNpemU6IGJ0bi1oZXJvLWdsb3ctc2l6ZSxcbiAgYnRuLWhlcm8taW5mby1nbG93LXNpemU6IGJ0bi1oZXJvLWdsb3ctc2l6ZSxcbiAgYnRuLWhlcm8tZGFuZ2VyLWdsb3ctc2l6ZTogYnRuLWhlcm8tZ2xvdy1zaXplLFxuICBidG4taGVyby1zZWNvbmRhcnktZ2xvdy1zaXplOiBidG4taGVyby1nbG93LXNpemUsXG4gIGJ0bi1zZWNvbmRhcnktYm9yZGVyOiBjb2xvci1wcmltYXJ5LFxuICBidG4tb3V0bGluZS1mZzogY29sb3ItZmctaGVhZGluZyxcbiAgYnRuLW91dGxpbmUtaG92ZXItZmc6IGNvbG9yLWZnLWhlYWRpbmcsXG4gIGJ0bi1vdXRsaW5lLWZvY3VzLWZnOiBjb2xvci1mZy1oZWFkaW5nLFxuICBidG4tZ3JvdXAtYmc6ICMzNzMyNzMsXG4gIGJ0bi1ncm91cC1zZXBhcmF0b3I6ICMzMTJjNjYsXG5cbiAgZm9ybS1jb250cm9sLWJnOiAjMzczMTdhLFxuICBmb3JtLWNvbnRyb2wtZm9jdXMtYmc6IHNlcGFyYXRvcixcbiAgZm9ybS1jb250cm9sLWJvcmRlci1jb2xvcjogc2VwYXJhdG9yLFxuICBmb3JtLWNvbnRyb2wtc2VsZWN0ZWQtYm9yZGVyLWNvbG9yOiBjb2xvci1wcmltYXJ5LFxuXG4gIGNoZWNrYm94LWJnOiB0cmFuc3BhcmVudCxcbiAgY2hlY2tib3gtc2l6ZTogMS4yNXJlbSxcbiAgY2hlY2tib3gtYm9yZGVyLXNpemU6IDJweCxcbiAgY2hlY2tib3gtYm9yZGVyLWNvbG9yOiBjb2xvci1mZyxcbiAgY2hlY2tib3gtY2hlY2ttYXJrOiB0cmFuc3BhcmVudCxcblxuICBjaGVja2JveC1jaGVja2VkLWJnOiB0cmFuc3BhcmVudCxcbiAgY2hlY2tib3gtY2hlY2tlZC1zaXplOiAxLjI1cmVtLFxuICBjaGVja2JveC1jaGVja2VkLWJvcmRlci1zaXplOiAycHgsXG4gIGNoZWNrYm94LWNoZWNrZWQtYm9yZGVyLWNvbG9yOiBjb2xvci1zdWNjZXNzLFxuICBjaGVja2JveC1jaGVja2VkLWNoZWNrbWFyazogY29sb3ItZmctaGVhZGluZyxcblxuICBjaGVja2JveC1kaXNhYmxlZC1iZzogdHJhbnNwYXJlbnQsXG4gIGNoZWNrYm94LWRpc2FibGVkLXNpemU6IDEuMjVyZW0sXG4gIGNoZWNrYm94LWRpc2FibGVkLWJvcmRlci1zaXplOiAycHgsXG4gIGNoZWNrYm94LWRpc2FibGVkLWJvcmRlci1jb2xvcjogY29sb3ItZmctaGVhZGluZyxcbiAgY2hlY2tib3gtZGlzYWJsZWQtY2hlY2ttYXJrOiBjb2xvci1mZy1oZWFkaW5nLFxuXG4gIHNlYXJjaC1iZzogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjMTcxNzQ5LCAjNDEzNzg5KSxcblxuICBzbWFydC10YWJsZS1oZWFkZXItZm9udC13ZWlnaHQ6IGZvbnQtd2VpZ2h0LW5vcm1hbCxcbiAgc21hcnQtdGFibGUtaGVhZGVyLWJnOiBjb2xvci1iZy1hY3RpdmUsXG4gIHNtYXJ0LXRhYmxlLWJnLWV2ZW46ICMzYTM0N2EsXG4gIHNtYXJ0LXRhYmxlLWJnLWFjdGl2ZTogY29sb3ItYmctYWN0aXZlLFxuXG4gIHNtYXJ0LXRhYmxlLXBhZ2luZy1ib3JkZXItY29sb3I6IGNvbG9yLXByaW1hcnksXG4gIHNtYXJ0LXRhYmxlLXBhZ2luZy1ib3JkZXItd2lkdGg6IDJweCxcbiAgc21hcnQtdGFibGUtcGFnaW5nLWZnLWFjdGl2ZTogY29sb3ItZmctaGVhZGluZyxcbiAgc21hcnQtdGFibGUtcGFnaW5nLWJnLWFjdGl2ZTogY29sb3ItcHJpbWFyeSxcbiAgc21hcnQtdGFibGUtcGFnaW5nLWhvdmVyOiByZ2JhKDAsIDAsIDAsIDAuMiksXG5cbiAgYmFkZ2UtZmctdGV4dDogY29sb3Itd2hpdGUsXG4gIGJhZGdlLXByaW1hcnktYmctY29sb3I6IGNvbG9yLXByaW1hcnksXG4gIGJhZGdlLXN1Y2Nlc3MtYmctY29sb3I6IGNvbG9yLXN1Y2Nlc3MsXG4gIGJhZGdlLWluZm8tYmctY29sb3I6IGNvbG9yLWluZm8sXG4gIGJhZGdlLXdhcm5pbmctYmctY29sb3I6IGNvbG9yLXdhcm5pbmcsXG4gIGJhZGdlLWRhbmdlci1iZy1jb2xvcjogY29sb3ItZGFuZ2VyLFxuXG4gIHNwaW5uZXItYmc6IHJnYmEoNjEsIDU1LCAxMjgsIDAuOSksXG4gIHN0ZXBwZXItYWNjZW50LWNvbG9yOiBjb2xvci1zdWNjZXNzLFxuXG4gIGNhbGVuZGFyLWFjdGl2ZS1pdGVtLWJnOiBjb2xvci1wcmltYXJ5LFxuICBjYWxlbmRhci1zZWxlY3RlZC1pdGVtLWJnOiBjb2xvci1wcmltYXJ5LFxuICBjYWxlbmRhci1yYW5nZS1iZy1pbi1yYW5nZTogIzRlNDA5NSxcbiAgY2FsZW5kYXItdG9kYXktaXRlbS1iZzogIzM1MmY2ZSxcbik7XG5cbi8vIHJlZ2lzdGVyIHRoZSB0aGVtZVxuJG5iLXRoZW1lczogbmItcmVnaXN0ZXItdGhlbWUoJHRoZW1lLCBjb3NtaWMsIGRlZmF1bHQpO1xuIiwiLyoqXG4gKiBAbGljZW5zZVxuICogQ29weXJpZ2h0IEFrdmVvLiBBbGwgUmlnaHRzIFJlc2VydmVkLlxuICogTGljZW5zZWQgdW5kZXIgdGhlIE1JVCBMaWNlbnNlLiBTZWUgTGljZW5zZS50eHQgaW4gdGhlIHByb2plY3Qgcm9vdCBmb3IgbGljZW5zZSBpbmZvcm1hdGlvbi5cbiAqL1xuXG5AaW1wb3J0ICcuLi9jb3JlL2Z1bmN0aW9ucyc7XG5AaW1wb3J0ICcuLi9jb3JlL21peGlucyc7XG5AaW1wb3J0ICdkZWZhdWx0JztcblxuLy8gZGVmYXVsdCB0aGUgYmFzZSB0aGVtZVxuJHRoZW1lOiAoXG4gIGhlYWRlci1mZzogI2Y3ZmFmYixcbiAgaGVhZGVyLWJnOiAjMTExMjE4LFxuXG4gIGxheW91dC1iZzogI2YxZjVmOCxcblxuICBjb2xvci1mZy1oZWFkaW5nOiAjMTgxODE4LFxuICBjb2xvci1mZy10ZXh0OiAjNGI0YjRiLFxuICBjb2xvci1mZy1oaWdobGlnaHQ6IGNvbG9yLWZnLFxuXG4gIHNlcGFyYXRvcjogI2NkZDVkYyxcblxuICByYWRpdXM6IDAuMTdyZW0sXG5cbiAgc2Nyb2xsYmFyLWJnOiAjZTNlOWVlLFxuXG4gIGNvbG9yLXByaW1hcnk6ICM3M2ExZmYsXG4gIGNvbG9yLXN1Y2Nlc3M6ICM1ZGNmZTMsXG4gIGNvbG9yLWluZm86ICNiYTdmZWMsXG4gIGNvbG9yLXdhcm5pbmc6ICNmZmEzNmIsXG4gIGNvbG9yLWRhbmdlcjogI2ZmNmI4MyxcblxuICBidG4tc2Vjb25kYXJ5LWJnOiAjZWRmMmY1LFxuICBidG4tc2Vjb25kYXJ5LWJvcmRlcjogI2VkZjJmNSxcblxuICBhY3Rpb25zLWZnOiAjZDNkYmU1LFxuICBhY3Rpb25zLWJnOiBjb2xvci1iZyxcblxuICBzaWRlYmFyLWJnOiAjZTNlOWVlLFxuXG4gIGJvcmRlci1jb2xvcjogI2Q1ZGJlMCxcblxuICBtZW51LWZvbnQtd2VpZ2h0OiBmb250LXdlaWdodC1ib2xkZXIsXG4gIG1lbnUtZmc6IGNvbG9yLWZnLXRleHQsXG4gIG1lbnUtYmc6ICNlM2U5ZWUsXG4gIG1lbnUtYWN0aXZlLWZnOiBjb2xvci1mZy1oZWFkaW5nLFxuICBtZW51LWFjdGl2ZS1iZzogbWVudS1iZyxcblxuICBtZW51LXN1Ym1lbnUtYmc6IG1lbnUtYmcsXG4gIG1lbnUtc3VibWVudS1mZzogY29sb3ItZmctdGV4dCxcbiAgbWVudS1zdWJtZW51LWFjdGl2ZS1mZzogY29sb3ItZmctaGVhZGluZyxcbiAgbWVudS1zdWJtZW51LWFjdGl2ZS1iZzogI2NkZDVkYyxcbiAgbWVudS1zdWJtZW51LWFjdGl2ZS1ib3JkZXItY29sb3I6IG1lbnUtc3VibWVudS1hY3RpdmUtYmcsXG4gIG1lbnUtc3VibWVudS1hY3RpdmUtc2hhZG93OiBub25lLFxuICBtZW51LXN1Ym1lbnUtaG92ZXItZmc6IG1lbnUtc3VibWVudS1hY3RpdmUtZmcsXG4gIG1lbnUtc3VibWVudS1ob3Zlci1iZzogbWVudS1iZyxcbiAgbWVudS1zdWJtZW51LWl0ZW0tYm9yZGVyLXdpZHRoOiAwLjEyNXJlbSxcbiAgbWVudS1zdWJtZW51LWl0ZW0tYm9yZGVyLXJhZGl1czogcmFkaXVzLFxuICBtZW51LXN1Ym1lbnUtaXRlbS1wYWRkaW5nOiAwLjVyZW0gMXJlbSxcbiAgbWVudS1zdWJtZW51LWl0ZW0tY29udGFpbmVyLXBhZGRpbmc6IDAgMS4yNXJlbSxcbiAgbWVudS1zdWJtZW51LXBhZGRpbmc6IDAuNXJlbSxcblxuICBidG4tYm9yZGVyLXJhZGl1czogYnRuLXNlbWktcm91bmQtYm9yZGVyLXJhZGl1cyxcblxuICBidG4taGVyby1kZWdyZWU6IDBkZWcsXG4gIGJ0bi1oZXJvLXByaW1hcnktZGVncmVlOiBidG4taGVyby1kZWdyZWUsXG4gIGJ0bi1oZXJvLXN1Y2Nlc3MtZGVncmVlOiBidG4taGVyby1kZWdyZWUsXG4gIGJ0bi1oZXJvLXdhcm5pbmctZGVncmVlOiBidG4taGVyby1kZWdyZWUsXG4gIGJ0bi1oZXJvLWluZm8tZGVncmVlOiBidG4taGVyby1kZWdyZWUsXG4gIGJ0bi1oZXJvLWRhbmdlci1kZWdyZWU6IGJ0bi1oZXJvLWRlZ3JlZSxcbiAgYnRuLWhlcm8tc2Vjb25kYXJ5LWRlZ3JlZTogYnRuLWhlcm8tZGVncmVlLFxuICBidG4taGVyby1nbG93LXNpemU6IDAgMCAyMHB4IDAsXG4gIGJ0bi1oZXJvLXByaW1hcnktZ2xvdy1zaXplOiBidG4taGVyby1nbG93LXNpemUsXG4gIGJ0bi1oZXJvLXN1Y2Nlc3MtZ2xvdy1zaXplOiBidG4taGVyby1nbG93LXNpemUsXG4gIGJ0bi1oZXJvLXdhcm5pbmctZ2xvdy1zaXplOiBidG4taGVyby1nbG93LXNpemUsXG4gIGJ0bi1oZXJvLWluZm8tZ2xvdy1zaXplOiBidG4taGVyby1nbG93LXNpemUsXG4gIGJ0bi1oZXJvLWRhbmdlci1nbG93LXNpemU6IGJ0bi1oZXJvLWdsb3ctc2l6ZSxcbiAgYnRuLWhlcm8tc2Vjb25kYXJ5LWdsb3ctc2l6ZTogMCAwIDAgMCxcbiAgYnRuLWhlcm8tYm9yZGVyLXJhZGl1czogYnRuLWJvcmRlci1yYWRpdXMsXG5cbiAgY2FyZC1zaGFkb3c6IG5vbmUsXG4gIGNhcmQtYm9yZGVyLXdpZHRoOiAxcHgsXG4gIGNhcmQtYm9yZGVyLWNvbG9yOiBib3JkZXItY29sb3IsXG4gIGNhcmQtaGVhZGVyLWJvcmRlci13aWR0aDogMCxcblxuICBsaW5rLWNvbG9yOiAjNWRjZmUzLFxuICBsaW5rLWNvbG9yLWhvdmVyOiAjN2RjZmUzLFxuICBsaW5rLWNvbG9yLXZpc2l0ZWQ6IGxpbmstY29sb3IsXG5cbiAgYWN0aW9ucy1zZXBhcmF0b3I6ICNmMWY0ZjUsXG5cbiAgbW9kYWwtc2VwYXJhdG9yOiBib3JkZXItY29sb3IsXG5cbiAgdGFicy1zZWxlY3RlZDogY29sb3ItcHJpbWFyeSxcbiAgdGFicy1zZXBhcmF0b3I6ICNlYmVjZWUsXG5cbiAgc21hcnQtdGFibGUtcGFnaW5nLWJnLWFjdGl2ZTogY29sb3ItcHJpbWFyeSxcblxuICByb3V0ZS10YWJzLXNlbGVjdGVkOiBjb2xvci1wcmltYXJ5LFxuXG4gIHBvcG92ZXItYm9yZGVyOiBjb2xvci1wcmltYXJ5LFxuXG4gIGZvb3Rlci1zaGFkb3c6IG5vbmUsXG4gIGZvb3Rlci1zZXBhcmF0b3I6IGJvcmRlci1jb2xvcixcbiAgZm9vdGVyLWZnLWhpZ2hsaWdodDogIzJhMmEyYSxcblxuICBjYWxlbmRhci10b2RheS1pdGVtLWJnOiAjYTJiMmM3LFxuICBjYWxlbmRhci1hY3RpdmUtaXRlbS1iZzogY29sb3ItcHJpbWFyeSxcbiAgY2FsZW5kYXItcmFuZ2UtYmctaW4tcmFuZ2U6ICNlM2VjZmUsXG4gIGNhbGVuZGFyLXRvZGF5LWZnOiBjb2xvci13aGl0ZSxcbik7XG5cbi8vIHJlZ2lzdGVyIHRoZSB0aGVtZVxuJG5iLXRoZW1lczogbmItcmVnaXN0ZXItdGhlbWUoJHRoZW1lLCBjb3Jwb3JhdGUsIGRlZmF1bHQpO1xuIiwiLyoqXG4gKiBAbGljZW5zZVxuICogQ29weXJpZ2h0IEFrdmVvLiBBbGwgUmlnaHRzIFJlc2VydmVkLlxuICogTGljZW5zZWQgdW5kZXIgdGhlIE1JVCBMaWNlbnNlLiBTZWUgTGljZW5zZS50eHQgaW4gdGhlIHByb2plY3Qgcm9vdCBmb3IgbGljZW5zZSBpbmZvcm1hdGlvbi5cbiAqL1xuXG5AaW1wb3J0ICcuL3NoYXBlLWJ1dHRvbnMnO1xuQGltcG9ydCAnLi9zaXplLWJ1dHRvbnMnO1xuQGltcG9ydCAnLi9kZWZhdWx0LWJ1dHRvbnMnO1xuQGltcG9ydCAnLi9oZXJvLWJ1dHRvbnMnO1xuQGltcG9ydCAnLi9vdXRsaW5lLWJ1dHRvbnMnO1xuXG5AbWl4aW4gbmItYi1idXR0b25zLXRoZW1lKCkge1xuXG4gIC5idG4ge1xuICAgIGNvbG9yOiBuYi10aGVtZShidG4tZmcpO1xuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDAuNHB4O1xuICAgIGZvbnQtd2VpZ2h0OiBuYi10aGVtZShmb250LXdlaWdodC1ib2xkZXIpO1xuICAgIGZvbnQtZmFtaWx5OiBuYi10aGVtZShidG4tZm9udC1mYW1pbHkpO1xuICAgIGJvcmRlcjogMnB4IHNvbGlkIHRyYW5zcGFyZW50O1xuICAgIHRyYW5zaXRpb246IG5vbmU7XG4gICAgY3Vyc29yOiBuYi10aGVtZShidG4tY3Vyc29yKTtcblxuICAgICY6Zm9jdXMsIC5mb2N1cyxcbiAgICAmOmhvdmVyLCAuaG92ZXIsXG4gICAgJjphY3RpdmUsIC5hY3RpdmUge1xuICAgICAgY29sb3I6IG5iLXRoZW1lKGJ0bi1mZyk7XG4gICAgICBjdXJzb3I6IG5iLXRoZW1lKGJ0bi1jdXJzb3IpO1xuICAgIH1cblxuICAgICY6bm90KDpkaXNhYmxlZCk6bm90KC5kaXNhYmxlZCkge1xuICAgICAgY3Vyc29yOiBuYi10aGVtZShidG4tY3Vyc29yKTtcbiAgICB9XG5cbiAgICBAaW5jbHVkZSBidG4tbWQoKTtcbiAgfVxuXG4gIEBpbmNsdWRlIGJ0bi1zaGFwZSgpO1xuICBAaW5jbHVkZSBidG4tc2l6ZSgpO1xuXG4gIEBpbmNsdWRlIGJ0bi1kZWZhdWx0KCk7XG4gIEBpbmNsdWRlIGJ0bi1oZXJvKCk7XG4gIEBpbmNsdWRlIGJ0bi1vdXRsaW5lKCk7XG59XG4iLCIvKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG5cbkBtaXhpbiBidG4tc2hhcGUoKSB7XG4gIC5idG4uYnRuLXJlY3RhbmdsZSB7XG4gICAgQGluY2x1ZGUgYnRuLXJlY3RhbmdsZSgpO1xuICB9XG5cbiAgLmJ0bi5idG4tc2VtaS1yb3VuZCB7XG4gICAgQGluY2x1ZGUgYnRuLXNlbWktcm91bmQoKTtcbiAgfVxuXG4gIC5idG4uYnRuLXJvdW5kIHtcbiAgICBAaW5jbHVkZSBidG4tcm91bmQoKTtcbiAgfVxufVxuXG5AbWl4aW4gYnRuLXJlY3RhbmdsZSgpIHtcbiAgQGluY2x1ZGUgYm9yZGVyLXJhZGl1cyhuYi10aGVtZShidG4tcmVjdGFuZ2xlLWJvcmRlci1yYWRpdXMpKTtcbn1cblxuQG1peGluIGJ0bi1zZW1pLXJvdW5kKCkge1xuICBAaW5jbHVkZSBib3JkZXItcmFkaXVzKG5iLXRoZW1lKGJ0bi1zZW1pLXJvdW5kLWJvcmRlci1yYWRpdXMpKTtcbn1cblxuQG1peGluIGJ0bi1yb3VuZCgpIHtcbiAgQGluY2x1ZGUgYm9yZGVyLXJhZGl1cyhuYi10aGVtZShidG4tcm91bmQtYm9yZGVyLXJhZGl1cykpO1xufVxuIiwiLyoqXG4gKiBAbGljZW5zZVxuICogQ29weXJpZ2h0IEFrdmVvLiBBbGwgUmlnaHRzIFJlc2VydmVkLlxuICogTGljZW5zZWQgdW5kZXIgdGhlIE1JVCBMaWNlbnNlLiBTZWUgTGljZW5zZS50eHQgaW4gdGhlIHByb2plY3Qgcm9vdCBmb3IgbGljZW5zZSBpbmZvcm1hdGlvbi5cbiAqL1xuXG5AbWl4aW4gYnRuLXNpemUoKSB7XG4gIC5idG4uYnRuLWxnIHtcbiAgICBAaW5jbHVkZSBidG4tbGcoKTtcbiAgfVxuXG4gIC5idG4uYnRuLW1kIHtcbiAgICBAaW5jbHVkZSBidG4tbWQoKTtcbiAgfVxuXG4gIC5idG4uYnRuLXNtIHtcbiAgICBAaW5jbHVkZSBidG4tc20oKTtcbiAgfVxuXG4gIC5idG4uYnRuLXhzIHtcbiAgICBAaW5jbHVkZSBidG4teHMoKTtcbiAgfVxufVxuXG5AbWl4aW4gYnRuLWxnKCkge1xuICBAaW5jbHVkZSBidXR0b24tc2l6ZShuYi10aGVtZShidG4tcGFkZGluZy15LWxnKSxcbiAgICAgICAgICAgICAgICAgICAgICAgbmItdGhlbWUoYnRuLXBhZGRpbmcteC1sZyksXG4gICAgICAgICAgICAgICAgICAgICAgIG5iLXRoZW1lKGJ0bi1mb250LXNpemUtbGcpLFxuICAgICAgICAgICAgICAgICAgICAgICBuYi10aGVtZShidG4tbGluZS1oZWlnaHQpLFxuICAgICAgICAgICAgICAgICAgICAgICBuYi10aGVtZShidG4tYm9yZGVyLXJhZGl1cykpO1xufVxuXG5AbWl4aW4gYnRuLW1kKCkge1xuICBAaW5jbHVkZSBidXR0b24tc2l6ZShuYi10aGVtZShidG4tcGFkZGluZy15LW1kKSxcbiAgICAgICAgICAgICAgICAgICAgICAgbmItdGhlbWUoYnRuLXBhZGRpbmcteC1tZCksXG4gICAgICAgICAgICAgICAgICAgICAgIG5iLXRoZW1lKGJ0bi1mb250LXNpemUtbWQpLFxuICAgICAgICAgICAgICAgICAgICAgICBuYi10aGVtZShidG4tbGluZS1oZWlnaHQpLFxuICAgICAgICAgICAgICAgICAgICAgICBuYi10aGVtZShidG4tYm9yZGVyLXJhZGl1cykpO1xufVxuXG5AbWl4aW4gYnRuLXNtKCkge1xuICBAaW5jbHVkZSBidXR0b24tc2l6ZShuYi10aGVtZShidG4tcGFkZGluZy15LXNtKSxcbiAgICAgICAgICAgICAgICAgICAgICAgbmItdGhlbWUoYnRuLXBhZGRpbmcteC1zbSksXG4gICAgICAgICAgICAgICAgICAgICAgIG5iLXRoZW1lKGJ0bi1mb250LXNpemUtc20pLFxuICAgICAgICAgICAgICAgICAgICAgICBuYi10aGVtZShidG4tbGluZS1oZWlnaHQpLFxuICAgICAgICAgICAgICAgICAgICAgICBuYi10aGVtZShidG4tYm9yZGVyLXJhZGl1cykpO1xufVxuXG5AbWl4aW4gYnRuLXhzKCkge1xuICBAaW5jbHVkZSBidXR0b24tc2l6ZShuYi10aGVtZShidG4tcGFkZGluZy15LXhzKSxcbiAgICAgICAgICAgICAgICAgICAgICAgbmItdGhlbWUoYnRuLXBhZGRpbmcteC14cyksXG4gICAgICAgICAgICAgICAgICAgICAgIG5iLXRoZW1lKGJ0bi1mb250LXNpemUteHMpLFxuICAgICAgICAgICAgICAgICAgICAgICBuYi10aGVtZShidG4tbGluZS1oZWlnaHQpLFxuICAgICAgICAgICAgICAgICAgICAgICBuYi10aGVtZShidG4tYm9yZGVyLXJhZGl1cykpO1xufVxuIiwiLyoqXG4gKiBAbGljZW5zZVxuICogQ29weXJpZ2h0IEFrdmVvLiBBbGwgUmlnaHRzIFJlc2VydmVkLlxuICogTGljZW5zZWQgdW5kZXIgdGhlIE1JVCBMaWNlbnNlLiBTZWUgTGljZW5zZS50eHQgaW4gdGhlIHByb2plY3Qgcm9vdCBmb3IgbGljZW5zZSBpbmZvcm1hdGlvbi5cbiAqL1xuXG5AbWl4aW4gYnRuLWRlZmF1bHQoKSB7XG5cbiAgLmJ0bi5idG4tcHJpbWFyeSB7XG4gICAgQGluY2x1ZGUgYnRuLXByaW1hcnkoKTtcbiAgfVxuXG4gIC5idG4uYnRuLXN1Y2Nlc3Mge1xuICAgIEBpbmNsdWRlIGJ0bi1zdWNjZXNzKCk7XG4gIH1cblxuICAuYnRuLmJ0bi13YXJuaW5nIHtcbiAgICBAaW5jbHVkZSBidG4td2FybmluZygpO1xuICB9XG5cbiAgLmJ0bi5idG4taW5mbyB7XG4gICAgQGluY2x1ZGUgYnRuLWluZm8oKTtcbiAgfVxuXG4gIC5idG4uYnRuLWRhbmdlciB7XG4gICAgQGluY2x1ZGUgYnRuLWRhbmdlcigpO1xuICB9XG5cbiAgLmJ0bi5idG4tc2Vjb25kYXJ5IHtcbiAgICBAaW5jbHVkZSBidG4tc2Vjb25kYXJ5KCk7XG4gIH1cbn1cblxuQG1peGluIGJ0bi1wcmltYXJ5KCkge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBuYi10aGVtZShidG4tcHJpbWFyeS1iZyk7XG5cbiAgQGluY2x1ZGUgYnRuLXByaW1hcnktZm9jdXMoKTtcbiAgQGluY2x1ZGUgYnRuLXByaW1hcnktaG92ZXIoKTtcbiAgQGluY2x1ZGUgYnRuLXByaW1hcnktYWN0aXZlKCk7XG4gIEBpbmNsdWRlIGJ0bi1kaXNhYmxlZCgpO1xuICBAaW5jbHVkZSBidG4tcHJpbWFyeS1wdWxzZSgpO1xufVxuXG5AbWl4aW4gYnRuLXN1Y2Nlc3MoKSB7XG4gIGJhY2tncm91bmQtY29sb3I6IG5iLXRoZW1lKGJ0bi1zdWNjZXNzLWJnKTtcblxuICBAaW5jbHVkZSBidG4tc3VjY2Vzcy1mb2N1cygpO1xuICBAaW5jbHVkZSBidG4tc3VjY2Vzcy1ob3ZlcigpO1xuICBAaW5jbHVkZSBidG4tc3VjY2Vzcy1hY3RpdmUoKTtcbiAgQGluY2x1ZGUgYnRuLWRpc2FibGVkKCk7XG4gIEBpbmNsdWRlIGJ0bi1zdWNjZXNzLXB1bHNlKCk7XG59XG5cbkBtaXhpbiBidG4td2FybmluZygpIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogbmItdGhlbWUoYnRuLXdhcm5pbmctYmcpO1xuXG4gIEBpbmNsdWRlIGJ0bi13YXJuaW5nLWZvY3VzKCk7XG4gIEBpbmNsdWRlIGJ0bi13YXJuaW5nLWhvdmVyKCk7XG4gIEBpbmNsdWRlIGJ0bi13YXJuaW5nLWFjdGl2ZSgpO1xuICBAaW5jbHVkZSBidG4tZGlzYWJsZWQoKTtcbiAgQGluY2x1ZGUgYnRuLXdhcm5pbmctcHVsc2UoKTtcbn1cblxuQG1peGluIGJ0bi1pbmZvKCkge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBuYi10aGVtZShidG4taW5mby1iZyk7XG5cbiAgQGluY2x1ZGUgYnRuLWluZm8tZm9jdXMoKTtcbiAgQGluY2x1ZGUgYnRuLWluZm8taG92ZXIoKTtcbiAgQGluY2x1ZGUgYnRuLWluZm8tYWN0aXZlKCk7XG4gIEBpbmNsdWRlIGJ0bi1kaXNhYmxlZCgpO1xuICBAaW5jbHVkZSBidG4taW5mby1wdWxzZSgpO1xufVxuXG5AbWl4aW4gYnRuLWRhbmdlcigpIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogbmItdGhlbWUoYnRuLWRhbmdlci1iZyk7XG5cbiAgQGluY2x1ZGUgYnRuLWRhbmdlci1mb2N1cygpO1xuICBAaW5jbHVkZSBidG4tZGFuZ2VyLWhvdmVyKCk7XG4gIEBpbmNsdWRlIGJ0bi1kYW5nZXItYWN0aXZlKCk7XG4gIEBpbmNsdWRlIGJ0bi1kaXNhYmxlZCgpO1xuICBAaW5jbHVkZSBidG4tZGFuZ2VyLXB1bHNlKCk7XG59XG5cbkBtaXhpbiBidG4tc2Vjb25kYXJ5KCkge1xuICBAaW5jbHVkZSBidG4tc2Vjb25kYXJ5LWJvcmRlcigpO1xuICBAaW5jbHVkZSBidG4tc2Vjb25kYXJ5LWZnKCk7XG4gIEBpbmNsdWRlIGJ0bi1zZWNvbmRhcnktYmcoKTtcbiAgQGluY2x1ZGUgYnRuLXNlY29uZGFyeS1mb2N1cygpO1xuICBAaW5jbHVkZSBidG4tc2Vjb25kYXJ5LWhvdmVyKCk7XG4gIEBpbmNsdWRlIGJ0bi1zZWNvbmRhcnktYWN0aXZlKCk7XG4gIEBpbmNsdWRlIGJ0bi1kaXNhYmxlZCgpO1xuICBAaW5jbHVkZSBidG4tc2Vjb25kYXJ5LXB1bHNlKCk7XG5cbiAgJjpmb2N1cywgJi5mb2N1cyxcbiAgJjpob3ZlciwgJi5ob3ZlcixcbiAgJjphY3RpdmUsICYuYWN0aXZlIHtcbiAgICBAaW5jbHVkZSBidG4tb3V0bGluZS1mZygpO1xuICB9XG59XG5cbkBmdW5jdGlvbiBidG4taG92ZXItY29sb3IoJGJnLCAkcGVyY2VudGFnZTogMTQlKSB7XG4gIEByZXR1cm4gdGludChuYi10aGVtZSgkYmcpLCAkcGVyY2VudGFnZSk7XG59XG5cbkBmdW5jdGlvbiBidG4tZm9jdXMtY29sb3IoJGJnLCAkcGVyY2VudGFnZTogMTQlKSB7XG4gIEByZXR1cm4gdGludChuYi10aGVtZSgkYmcpLCAkcGVyY2VudGFnZSk7XG59XG5cbkBmdW5jdGlvbiBidG4tYWN0aXZlLWNvbG9yKCRiZywgJHBlcmNlbnRhZ2U6IDE0JSkge1xuICBAcmV0dXJuIHNoYWRlKG5iLXRoZW1lKCRiZyksICRwZXJjZW50YWdlKTtcbn1cblxuQG1peGluIGJ0bi1vdXRsaW5lLWZvY3VzKCRmb2N1cykge1xuICAmOmZvY3VzLFxuICAmLmZvY3VzIHtcbiAgICBjb2xvcjogbmItdGhlbWUoYnRuLW91dGxpbmUtZm9jdXMtZmcpO1xuICAgIGJvcmRlci1jb2xvcjogJGZvY3VzO1xuICAgIGJveC1zaGFkb3c6IG5vbmU7XG4gIH1cbn1cblxuQG1peGluIGJ0bi1mb2N1cygkZm9jdXMpIHtcbiAgJjpmb2N1cyxcbiAgJi5mb2N1cyB7XG4gICAgY29sb3I6IG5iLXRoZW1lKGJ0bi1vdXRsaW5lLWhvdmVyLWZnKTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkZm9jdXM7XG4gICAgYm9yZGVyLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgICBib3gtc2hhZG93OiBub25lO1xuICB9XG59XG5cbkBtaXhpbiBidG4taG92ZXIoJGhvdmVyKSB7XG4gICY6aG92ZXIsXG4gICYuaG92ZXIge1xuICAgIGNvbG9yOiBuYi10aGVtZShidG4tb3V0bGluZS1ob3Zlci1mZyk7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogJGhvdmVyO1xuICAgIGJvcmRlci1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gIH1cbn1cblxuQG1peGluIGJ0bi1hY3RpdmUoJGFjdGl2ZSkge1xuICAmOmFjdGl2ZSxcbiAgJi5hY3RpdmUsXG4gICY6YWN0aXZlOmZvY3VzIHtcbiAgICBjb2xvcjogbmItdGhlbWUoYnRuLW91dGxpbmUtaG92ZXItZmcpO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICRhY3RpdmU7XG4gICAgYm9yZGVyLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgICBib3gtc2hhZG93OiBub25lO1xuICB9XG59XG5cbi8vIEZvY3VzXG5AbWl4aW4gYnRuLXByaW1hcnktZm9jdXMoKSB7XG4gIEBpbmNsdWRlIGJ0bi1mb2N1cyhidG4tZm9jdXMtY29sb3IoYnRuLXByaW1hcnktYmcpKTtcbn1cblxuQG1peGluIGJ0bi1zdWNjZXNzLWZvY3VzKCkge1xuICBAaW5jbHVkZSBidG4tZm9jdXMoYnRuLWZvY3VzLWNvbG9yKGJ0bi1zdWNjZXNzLWJnKSk7XG59XG5cbkBtaXhpbiBidG4td2FybmluZy1mb2N1cygpIHtcbiAgQGluY2x1ZGUgYnRuLWZvY3VzKGJ0bi1mb2N1cy1jb2xvcihidG4td2FybmluZy1iZykpO1xufVxuXG5AbWl4aW4gYnRuLWluZm8tZm9jdXMoKSB7XG4gIEBpbmNsdWRlIGJ0bi1mb2N1cyhidG4tZm9jdXMtY29sb3IoYnRuLWluZm8tYmcpKTtcbn1cblxuQG1peGluIGJ0bi1kYW5nZXItZm9jdXMoKSB7XG4gIEBpbmNsdWRlIGJ0bi1mb2N1cyhidG4tZm9jdXMtY29sb3IoYnRuLWRhbmdlci1iZykpO1xufVxuXG5AbWl4aW4gYnRuLXNlY29uZGFyeS1mb2N1cygpIHtcbiAgQGluY2x1ZGUgYnRuLW91dGxpbmUtZm9jdXMoYnRuLWZvY3VzLWNvbG9yKGJ0bi1zZWNvbmRhcnktYm9yZGVyLCAyMCUpKTtcbn1cblxuXG4vLyBIb3ZlclxuQG1peGluIGJ0bi1wcmltYXJ5LWhvdmVyKCkge1xuICBAaW5jbHVkZSBidG4taG92ZXIoYnRuLWhvdmVyLWNvbG9yKGJ0bi1wcmltYXJ5LWJnKSk7XG59XG5cbkBtaXhpbiBidG4tc3VjY2Vzcy1ob3ZlcigpIHtcbiAgQGluY2x1ZGUgYnRuLWhvdmVyKGJ0bi1ob3Zlci1jb2xvcihidG4tc3VjY2Vzcy1iZykpO1xufVxuXG5AbWl4aW4gYnRuLXdhcm5pbmctaG92ZXIoKSB7XG4gIEBpbmNsdWRlIGJ0bi1ob3ZlcihidG4taG92ZXItY29sb3IoYnRuLXdhcm5pbmctYmcpKTtcbn1cblxuQG1peGluIGJ0bi1pbmZvLWhvdmVyKCkge1xuICBAaW5jbHVkZSBidG4taG92ZXIoYnRuLWhvdmVyLWNvbG9yKGJ0bi1pbmZvLWJnKSk7XG59XG5cbkBtaXhpbiBidG4tZGFuZ2VyLWhvdmVyKCkge1xuICBAaW5jbHVkZSBidG4taG92ZXIoYnRuLWhvdmVyLWNvbG9yKGJ0bi1kYW5nZXItYmcpKTtcbn1cblxuQG1peGluIGJ0bi1zZWNvbmRhcnktaG92ZXIoKSB7XG4gIEBpbmNsdWRlIGJ0bi1ob3ZlcihidG4taG92ZXItY29sb3IoYnRuLXNlY29uZGFyeS1ib3JkZXIpKTtcbn1cblxuXG4vLyBBY3RpdmVcbkBtaXhpbiBidG4tcHJpbWFyeS1hY3RpdmUoKSB7XG4gIEBpbmNsdWRlIGJ0bi1hY3RpdmUoYnRuLWFjdGl2ZS1jb2xvcihidG4tcHJpbWFyeS1iZykpO1xufVxuXG5AbWl4aW4gYnRuLXN1Y2Nlc3MtYWN0aXZlKCkge1xuICBAaW5jbHVkZSBidG4tYWN0aXZlKGJ0bi1hY3RpdmUtY29sb3IoYnRuLXN1Y2Nlc3MtYmcpKTtcbn1cblxuQG1peGluIGJ0bi13YXJuaW5nLWFjdGl2ZSgpIHtcbiAgQGluY2x1ZGUgYnRuLWFjdGl2ZShidG4tYWN0aXZlLWNvbG9yKGJ0bi13YXJuaW5nLWJnKSk7XG59XG5cbkBtaXhpbiBidG4taW5mby1hY3RpdmUoKSB7XG4gIEBpbmNsdWRlIGJ0bi1hY3RpdmUoYnRuLWFjdGl2ZS1jb2xvcihidG4taW5mby1iZykpO1xufVxuXG5AbWl4aW4gYnRuLWRhbmdlci1hY3RpdmUoKSB7XG4gIEBpbmNsdWRlIGJ0bi1hY3RpdmUoYnRuLWFjdGl2ZS1jb2xvcihidG4tZGFuZ2VyLWJnKSk7XG59XG5cbkBtaXhpbiBidG4tc2Vjb25kYXJ5LWFjdGl2ZSgpIHtcbiAgQGluY2x1ZGUgYnRuLWFjdGl2ZShidG4tYWN0aXZlLWNvbG9yKGJ0bi1zZWNvbmRhcnktYm9yZGVyKSk7XG59XG5cblxuLy8gRGlzYWJsZWRcbkBtaXhpbiBidG4tZGlzYWJsZWQoKSB7XG4gICY6ZGlzYWJsZWQsICYuYnRuLWRpc2FibGVkIHtcbiAgICBvcGFjaXR5OiBuYi10aGVtZShidG4tZGlzYWJsZWQtb3BhY2l0eSk7XG4gIH1cbn1cblxuXG5AbWl4aW4gYnRuLXNlY29uZGFyeS1ib3JkZXIoKSB7XG4gIGJvcmRlcjogMnB4IHNvbGlkIG5iLXRoZW1lKGJ0bi1zZWNvbmRhcnktYm9yZGVyKTtcbn1cblxuQG1peGluIGJ0bi1zZWNvbmRhcnktZmcoKSB7XG4gIGNvbG9yOiBuYi10aGVtZShidG4tb3V0bGluZS1mZyk7XG59XG5cbkBtaXhpbiBidG4tc2Vjb25kYXJ5LWJnKCkge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBuYi10aGVtZShidG4tc2Vjb25kYXJ5LWJnKTtcbn1cblxuLy8gUHVsc2VcbkBtaXhpbiBidG4tcHJpbWFyeS1wdWxzZSgpIHtcbiAgQGluY2x1ZGUgYnRuLXB1bHNlKHByaW1hcnksIG5iLXRoZW1lKGJ0bi1wcmltYXJ5LWJnKSk7XG59XG5AbWl4aW4gYnRuLXN1Y2Nlc3MtcHVsc2UoKSB7XG4gIEBpbmNsdWRlIGJ0bi1wdWxzZShzdWNjZXNzLCBuYi10aGVtZShidG4tc3VjY2Vzcy1iZykpO1xufVxuQG1peGluIGJ0bi13YXJuaW5nLXB1bHNlKCkge1xuICBAaW5jbHVkZSBidG4tcHVsc2Uod2FybmluZywgbmItdGhlbWUoYnRuLXdhcm5pbmctYmcpKTtcbn1cbkBtaXhpbiBidG4taW5mby1wdWxzZSgpIHtcbiAgQGluY2x1ZGUgYnRuLXB1bHNlKGluZm8sIG5iLXRoZW1lKGJ0bi1pbmZvLWJnKSk7XG59XG5AbWl4aW4gYnRuLWRhbmdlci1wdWxzZSgpIHtcbiAgQGluY2x1ZGUgYnRuLXB1bHNlKGRhbmdlciwgbmItdGhlbWUoYnRuLWRhbmdlci1iZykpO1xufVxuQG1peGluIGJ0bi1zZWNvbmRhcnktcHVsc2UoKSB7XG4gIEBpbmNsdWRlIGJ0bi1wdWxzZShzZWNvbmRhcnksIG5iLXRoZW1lKGJ0bi1zZWNvbmRhcnktYm9yZGVyKSk7XG59XG4iLCIvKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG5cbkBtaXhpbiBidG4taGVybygpIHtcbiAgLmJ0bi5idG4taGVyby1wcmltYXJ5IHtcbiAgICBAaW5jbHVkZSBidG4taGVyby1wcmltYXJ5KCk7XG4gIH1cblxuICAuYnRuLmJ0bi1oZXJvLXN1Y2Nlc3Mge1xuICAgIEBpbmNsdWRlIGJ0bi1oZXJvLXN1Y2Nlc3MoKTtcbiAgfVxuXG4gIC5idG4uYnRuLWhlcm8td2FybmluZyB7XG4gICAgQGluY2x1ZGUgYnRuLWhlcm8td2FybmluZygpO1xuICB9XG5cbiAgLmJ0bi5idG4taGVyby1pbmZvIHtcbiAgICBAaW5jbHVkZSBidG4taGVyby1pbmZvKCk7XG4gIH1cblxuICAuYnRuLmJ0bi1oZXJvLWRhbmdlciB7XG4gICAgQGluY2x1ZGUgYnRuLWhlcm8tZGFuZ2VyKCk7XG4gIH1cblxuICAuYnRuLmJ0bi1oZXJvLXNlY29uZGFyeSB7XG4gICAgQGluY2x1ZGUgYnRuLWhlcm8tc2Vjb25kYXJ5KCk7XG4gIH1cbn1cblxuQG1peGluIGJ0bi1oZXJvLXByaW1hcnkoKSB7XG4gIEBpbmNsdWRlIGJ0bi1oZXJvLXByaW1hcnktZ3JhZGllbnQoKTtcbiAgQGluY2x1ZGUgYnRuLWhlcm8tcHJpbWFyeS1iZXZlbC1nbG93LXNoYWRvdygpO1xuICBAaW5jbHVkZSBidG4taGVyby1ib3JkZXItcmFkaXVzKCk7XG4gIEBpbmNsdWRlIGJ0bi1oZXJvLXRleHQoKTtcbiAgQGluY2x1ZGUgYnRuLWhlcm8tcHJpbWFyeS1mb2N1cygpO1xuICBAaW5jbHVkZSBidG4taGVyby1wcmltYXJ5LWhvdmVyKCk7XG4gIEBpbmNsdWRlIGJ0bi1oZXJvLXByaW1hcnktYWN0aXZlKCk7XG4gIEBpbmNsdWRlIGJ0bi1oZXJvLXByaW1hcnktYm9yZGVyKCk7XG4gIEBpbmNsdWRlIGJ0bi1oZXJvLWRpc2FibGVkKCk7XG4gIEBpbmNsdWRlIGJ0bi1oZXJvLWxpbmUtaGVpZ2h0KCk7XG4gIEBpbmNsdWRlIGJ0bi1oZXJvLXByaW1hcnktcHVsc2UoKTtcbn1cblxuQG1peGluIGJ0bi1oZXJvLXN1Y2Nlc3MoKSB7XG4gIEBpbmNsdWRlIGJ0bi1oZXJvLXN1Y2Nlc3MtZ3JhZGllbnQoKTtcbiAgQGluY2x1ZGUgYnRuLWhlcm8tc3VjY2Vzcy1iZXZlbC1nbG93LXNoYWRvdygpO1xuICBAaW5jbHVkZSBidG4taGVyby1ib3JkZXItcmFkaXVzKCk7XG4gIEBpbmNsdWRlIGJ0bi1oZXJvLXRleHQoKTtcbiAgQGluY2x1ZGUgYnRuLWhlcm8tc3VjY2Vzcy1mb2N1cygpO1xuICBAaW5jbHVkZSBidG4taGVyby1zdWNjZXNzLWhvdmVyKCk7XG4gIEBpbmNsdWRlIGJ0bi1oZXJvLXN1Y2Nlc3MtYWN0aXZlKCk7XG4gIEBpbmNsdWRlIGJ0bi1oZXJvLXN1Y2Nlc3MtYm9yZGVyKCk7XG4gIEBpbmNsdWRlIGJ0bi1oZXJvLWRpc2FibGVkKCk7XG4gIEBpbmNsdWRlIGJ0bi1oZXJvLWxpbmUtaGVpZ2h0KCk7XG4gIEBpbmNsdWRlIGJ0bi1oZXJvLXN1Y2Nlc3MtcHVsc2UoKTtcbn1cblxuQG1peGluIGJ0bi1oZXJvLXdhcm5pbmcoKSB7XG4gIEBpbmNsdWRlIGJ0bi1oZXJvLXdhcm5pbmctZ3JhZGllbnQoKTtcbiAgQGluY2x1ZGUgYnRuLWhlcm8td2FybmluZy1iZXZlbC1nbG93LXNoYWRvdygpO1xuICBAaW5jbHVkZSBidG4taGVyby1ib3JkZXItcmFkaXVzKCk7XG4gIEBpbmNsdWRlIGJ0bi1oZXJvLXRleHQoKTtcbiAgQGluY2x1ZGUgYnRuLWhlcm8td2FybmluZy1mb2N1cygpO1xuICBAaW5jbHVkZSBidG4taGVyby13YXJuaW5nLWhvdmVyKCk7XG4gIEBpbmNsdWRlIGJ0bi1oZXJvLXdhcm5pbmctYWN0aXZlKCk7XG4gIEBpbmNsdWRlIGJ0bi1oZXJvLXdhcm5pbmctYm9yZGVyKCk7XG4gIEBpbmNsdWRlIGJ0bi1oZXJvLWRpc2FibGVkKCk7XG4gIEBpbmNsdWRlIGJ0bi1oZXJvLWxpbmUtaGVpZ2h0KCk7XG4gIEBpbmNsdWRlIGJ0bi1oZXJvLXdhcm5pbmctcHVsc2UoKTtcbn1cblxuQG1peGluIGJ0bi1oZXJvLWluZm8oKSB7XG4gIEBpbmNsdWRlIGJ0bi1oZXJvLWluZm8tZ3JhZGllbnQoKTtcbiAgQGluY2x1ZGUgYnRuLWhlcm8taW5mby1iZXZlbC1nbG93LXNoYWRvdygpO1xuICBAaW5jbHVkZSBidG4taGVyby1ib3JkZXItcmFkaXVzKCk7XG4gIEBpbmNsdWRlIGJ0bi1oZXJvLXRleHQoKTtcbiAgQGluY2x1ZGUgYnRuLWhlcm8taW5mby1mb2N1cygpO1xuICBAaW5jbHVkZSBidG4taGVyby1pbmZvLWhvdmVyKCk7XG4gIEBpbmNsdWRlIGJ0bi1oZXJvLWluZm8tYWN0aXZlKCk7XG4gIEBpbmNsdWRlIGJ0bi1oZXJvLWluZm8tYm9yZGVyKCk7XG4gIEBpbmNsdWRlIGJ0bi1oZXJvLWRpc2FibGVkKCk7XG4gIEBpbmNsdWRlIGJ0bi1oZXJvLWxpbmUtaGVpZ2h0KCk7XG4gIEBpbmNsdWRlIGJ0bi1oZXJvLWluZm8tcHVsc2UoKTtcbn1cblxuQG1peGluIGJ0bi1oZXJvLWRhbmdlcigpIHtcbiAgQGluY2x1ZGUgYnRuLWhlcm8tZGFuZ2VyLWdyYWRpZW50KCk7XG4gIEBpbmNsdWRlIGJ0bi1oZXJvLWRhbmdlci1iZXZlbC1nbG93LXNoYWRvdygpO1xuICBAaW5jbHVkZSBidG4taGVyby1ib3JkZXItcmFkaXVzKCk7XG4gIEBpbmNsdWRlIGJ0bi1oZXJvLXRleHQoKTtcbiAgQGluY2x1ZGUgYnRuLWhlcm8tZGFuZ2VyLWZvY3VzKCk7XG4gIEBpbmNsdWRlIGJ0bi1oZXJvLWRhbmdlci1ob3ZlcigpO1xuICBAaW5jbHVkZSBidG4taGVyby1kYW5nZXItYWN0aXZlKCk7XG4gIEBpbmNsdWRlIGJ0bi1oZXJvLWRhbmdlci1ib3JkZXIoKTtcbiAgQGluY2x1ZGUgYnRuLWhlcm8tZGlzYWJsZWQoKTtcbiAgQGluY2x1ZGUgYnRuLWhlcm8tbGluZS1oZWlnaHQoKTtcbiAgQGluY2x1ZGUgYnRuLWhlcm8tZGFuZ2VyLXB1bHNlKCk7XG59XG5cbkBtaXhpbiBidG4taGVyby1zZWNvbmRhcnkoKSB7XG4gIGNvbG9yOiBuYi10aGVtZShidG4tb3V0bGluZS1mZyk7XG4gIEBpbmNsdWRlIGJ0bi1oZXJvLXNlY29uZGFyeS1iZygpO1xuICBAaW5jbHVkZSBidG4taGVyby1zZWNvbmRhcnktYmV2ZWwtZ2xvdy1zaGFkb3coKTtcbiAgQGluY2x1ZGUgYnRuLWhlcm8tYm9yZGVyLXJhZGl1cygpO1xuICBAaW5jbHVkZSBidG4taGVyby10ZXh0KCk7XG4gIEBpbmNsdWRlIGJ0bi1oZXJvLXNlY29uZGFyeS1mb2N1cygpO1xuICBAaW5jbHVkZSBidG4taGVyby1zZWNvbmRhcnktaG92ZXIoKTtcbiAgQGluY2x1ZGUgYnRuLWhlcm8tc2Vjb25kYXJ5LWFjdGl2ZSgpO1xuICBAaW5jbHVkZSBidG4taGVyby1zZWNvbmRhcnktYm9yZGVyKCk7XG4gIEBpbmNsdWRlIGJ0bi1oZXJvLWRpc2FibGVkKCk7XG4gIEBpbmNsdWRlIGJ0bi1oZXJvLXNlY29uZGFyeS1wdWxzZSgpO1xufVxuXG5AZnVuY3Rpb24gYnRuLWhlcm8tZ3JhZGllbnQtbGVmdCgkY29sb3IsICRkZWdyZWVzOiAyMGRlZykge1xuICBAcmV0dXJuIGFkanVzdC1odWUoJGNvbG9yLCAkZGVncmVlcyk7XG59XG5cbi8vIEZ1bmN0aW9ucyBmb3IgYm94LXNoYWRvd1xuQGZ1bmN0aW9uIGJ0bi1oZXJvLWJldmVsKCRjb2xvcikge1xuICBAcmV0dXJuIG5iLXRoZW1lKGJ0bi1oZXJvLWJldmVsLXNpemUpIHNoYWRlKCRjb2xvciwgMTQlKTtcbn1cblxuQGZ1bmN0aW9uIGJ0bi1oZXJvLWdsb3coJGhlcm8tZ2xvdy1zaXplLCAkY29sb3IpIHtcbiAgQHJldHVybiBuYi10aGVtZSgkaGVyby1nbG93LXNpemUpICRjb2xvcjtcbn1cblxuLy8gTGVmdCBjb2xvcnNcbkBmdW5jdGlvbiBidG4taGVyby1wcmltYXJ5LWxlZnQtY29sb3IoKSB7XG4gIEByZXR1cm4gYnRuLWhlcm8tZ3JhZGllbnQtbGVmdChuYi10aGVtZShidG4tcHJpbWFyeS1iZyksIG5iLXRoZW1lKGJ0bi1oZXJvLXByaW1hcnktZGVncmVlKSk7XG59XG5cbkBmdW5jdGlvbiBidG4taGVyby1zdWNjZXNzLWxlZnQtY29sb3IoKSB7XG4gIEByZXR1cm4gYnRuLWhlcm8tZ3JhZGllbnQtbGVmdChuYi10aGVtZShidG4tc3VjY2Vzcy1iZyksIG5iLXRoZW1lKGJ0bi1oZXJvLXN1Y2Nlc3MtZGVncmVlKSk7XG59XG5cbkBmdW5jdGlvbiBidG4taGVyby13YXJuaW5nLWxlZnQtY29sb3IoKSB7XG4gIEByZXR1cm4gYnRuLWhlcm8tZ3JhZGllbnQtbGVmdChuYi10aGVtZShidG4td2FybmluZy1iZyksIG5iLXRoZW1lKGJ0bi1oZXJvLXdhcm5pbmctZGVncmVlKSk7XG59XG5cbkBmdW5jdGlvbiBidG4taGVyby1pbmZvLWxlZnQtY29sb3IoKSB7XG4gIEByZXR1cm4gYnRuLWhlcm8tZ3JhZGllbnQtbGVmdChuYi10aGVtZShidG4taW5mby1iZyksIG5iLXRoZW1lKGJ0bi1oZXJvLWluZm8tZGVncmVlKSk7XG59XG5cbkBmdW5jdGlvbiBidG4taGVyby1kYW5nZXItbGVmdC1jb2xvcigpIHtcbiAgQHJldHVybiBidG4taGVyby1ncmFkaWVudC1sZWZ0KG5iLXRoZW1lKGJ0bi1kYW5nZXItYmcpLCBuYi10aGVtZShidG4taGVyby1kYW5nZXItZGVncmVlKSk7XG59XG5cbkBmdW5jdGlvbiBidG4taGVyby1zZWNvbmRhcnktbGVmdC1jb2xvcigpIHtcbiAgQHJldHVybiBidG4taGVyby1ncmFkaWVudC1sZWZ0KG5iLXRoZW1lKGJ0bi1zZWNvbmRhcnktYm9yZGVyKSwgbmItdGhlbWUoYnRuLWhlcm8tc2Vjb25kYXJ5LWRlZ3JlZSkpO1xufVxuXG4vLyBNaWRkbGUgY29sb3JzXG5AZnVuY3Rpb24gYnRuLWhlcm8tcHJpbWFyeS1taWRkbGUtY29sb3IoKSB7XG4gIEByZXR1cm4gbWl4KGJ0bi1oZXJvLXByaW1hcnktbGVmdC1jb2xvcigpLCBuYi10aGVtZShidG4tcHJpbWFyeS1iZykpO1xufVxuXG5AZnVuY3Rpb24gYnRuLWhlcm8tc3VjY2Vzcy1taWRkbGUtY29sb3IoKSB7XG4gIEByZXR1cm4gbWl4KGJ0bi1oZXJvLXN1Y2Nlc3MtbGVmdC1jb2xvcigpLCBuYi10aGVtZShidG4tc3VjY2Vzcy1iZykpO1xufVxuXG5AZnVuY3Rpb24gYnRuLWhlcm8td2FybmluZy1taWRkbGUtY29sb3IoKSB7XG4gIEByZXR1cm4gbWl4KGJ0bi1oZXJvLXdhcm5pbmctbGVmdC1jb2xvcigpLCBuYi10aGVtZShidG4td2FybmluZy1iZykpO1xufVxuXG5AZnVuY3Rpb24gYnRuLWhlcm8taW5mby1taWRkbGUtY29sb3IoKSB7XG4gIEByZXR1cm4gbWl4KGJ0bi1oZXJvLWluZm8tbGVmdC1jb2xvcigpLCBuYi10aGVtZShidG4taW5mby1iZykpO1xufVxuXG5AZnVuY3Rpb24gYnRuLWhlcm8tZGFuZ2VyLW1pZGRsZS1jb2xvcigpIHtcbiAgQHJldHVybiBtaXgoYnRuLWhlcm8tZGFuZ2VyLWxlZnQtY29sb3IoKSwgbmItdGhlbWUoYnRuLWRhbmdlci1iZykpO1xufVxuXG5AZnVuY3Rpb24gYnRuLWhlcm8tc2Vjb25kYXJ5LW1pZGRsZS1jb2xvcigpIHtcbiAgQHJldHVybiBtaXgoYnRuLWhlcm8tc2Vjb25kYXJ5LWxlZnQtY29sb3IoKSwgbmItdGhlbWUoYnRuLXNlY29uZGFyeS1ib3JkZXIpKTtcbn1cblxuLy8gbGlnaHQgZ3JhZGllbnRzXG5cbkBmdW5jdGlvbiBidG4taGVyby1saWdodC1ncmFkaWVudCgkY29sb3ItbGVmdCwgJGNvbG9yLXJpZ2h0KSB7XG4gICRjb2xvci1sZWZ0OiB0aW50KCRjb2xvci1sZWZ0LCAxNCUpO1xuICAkY29sb3ItcmlnaHQ6IHRpbnQoJGNvbG9yLXJpZ2h0LCAxNCUpO1xuXG4gIEByZXR1cm4gbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAkY29sb3ItbGVmdCwgJGNvbG9yLXJpZ2h0KTtcbn1cblxuQGZ1bmN0aW9uIGJ0bi1oZXJvLXByaW1hcnktbGlnaHQtZ3JhZGllbnQoKSB7XG4gICRjb2xvci1yaWdodDogbmItdGhlbWUoYnRuLXByaW1hcnktYmcpO1xuICAkY29sb3ItbGVmdDogYnRuLWhlcm8tcHJpbWFyeS1sZWZ0LWNvbG9yKCk7XG5cbiAgQHJldHVybiBidG4taGVyby1saWdodC1ncmFkaWVudCgkY29sb3ItbGVmdCwgJGNvbG9yLXJpZ2h0KTtcbn1cblxuQGZ1bmN0aW9uIGJ0bi1oZXJvLXN1Y2Nlc3MtbGlnaHQtZ3JhZGllbnQoKSB7XG4gICRjb2xvci1yaWdodDogbmItdGhlbWUoYnRuLXN1Y2Nlc3MtYmcpO1xuICAkY29sb3ItbGVmdDogYnRuLWhlcm8tc3VjY2Vzcy1sZWZ0LWNvbG9yKCk7XG5cbiAgQHJldHVybiBidG4taGVyby1saWdodC1ncmFkaWVudCgkY29sb3ItbGVmdCwgJGNvbG9yLXJpZ2h0KTtcbn1cblxuQGZ1bmN0aW9uIGJ0bi1oZXJvLXdhcm5pbmctbGlnaHQtZ3JhZGllbnQoKSB7XG4gICRjb2xvci1yaWdodDogbmItdGhlbWUoYnRuLXdhcm5pbmctYmcpO1xuICAkY29sb3ItbGVmdDogYnRuLWhlcm8td2FybmluZy1sZWZ0LWNvbG9yKCk7XG5cbiAgQHJldHVybiBidG4taGVyby1saWdodC1ncmFkaWVudCgkY29sb3ItbGVmdCwgJGNvbG9yLXJpZ2h0KTtcbn1cblxuQGZ1bmN0aW9uIGJ0bi1oZXJvLWluZm8tbGlnaHQtZ3JhZGllbnQoKSB7XG4gICRjb2xvci1yaWdodDogbmItdGhlbWUoYnRuLWluZm8tYmcpO1xuICAkY29sb3ItbGVmdDogYnRuLWhlcm8taW5mby1sZWZ0LWNvbG9yKCk7XG5cbiAgQHJldHVybiBidG4taGVyby1saWdodC1ncmFkaWVudCgkY29sb3ItbGVmdCwgJGNvbG9yLXJpZ2h0KTtcbn1cblxuQGZ1bmN0aW9uIGJ0bi1oZXJvLWRhbmdlci1saWdodC1ncmFkaWVudCgpIHtcbiAgJGNvbG9yLXJpZ2h0OiBuYi10aGVtZShidG4tZGFuZ2VyLWJnKTtcbiAgJGNvbG9yLWxlZnQ6IGJ0bi1oZXJvLWRhbmdlci1sZWZ0LWNvbG9yKCk7XG5cbiAgQHJldHVybiBidG4taGVyby1saWdodC1ncmFkaWVudCgkY29sb3ItbGVmdCwgJGNvbG9yLXJpZ2h0KTtcbn1cblxuQGZ1bmN0aW9uIGJ0bi1oZXJvLXNlY29uZGFyeS1saWdodC1ncmFkaWVudCgpIHtcbiAgJGNvbG9yLXJpZ2h0OiBuYi10aGVtZShidG4tc2Vjb25kYXJ5LWJnKTtcbiAgJGNvbG9yLWxlZnQ6IGJ0bi1oZXJvLXNlY29uZGFyeS1sZWZ0LWNvbG9yKCk7XG5cbiAgQHJldHVybiBidG4taGVyby1saWdodC1ncmFkaWVudCgkY29sb3ItbGVmdCwgJGNvbG9yLXJpZ2h0KTtcbn1cblxuLy8gZGFyayBncmFkaWVudHNcblxuQGZ1bmN0aW9uIGJ0bi1oZXJvLWRhcmstZ3JhZGllbnQoJGNvbG9yLWxlZnQsICRjb2xvci1yaWdodCkge1xuICAkY29sb3ItbGVmdDogc2hhZGUoJGNvbG9yLWxlZnQsIDE0JSk7XG4gICRjb2xvci1yaWdodDogc2hhZGUoJGNvbG9yLXJpZ2h0LCAxNCUpO1xuXG4gIEByZXR1cm4gbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAkY29sb3ItbGVmdCwgJGNvbG9yLXJpZ2h0KTtcbn1cblxuQGZ1bmN0aW9uIGJ0bi1oZXJvLXByaW1hcnktZGFyay1ncmFkaWVudCgpIHtcbiAgJGNvbG9yLXJpZ2h0OiBuYi10aGVtZShidG4tcHJpbWFyeS1iZyk7XG4gICRjb2xvci1sZWZ0OiBidG4taGVyby1wcmltYXJ5LWxlZnQtY29sb3IoKTtcblxuICBAcmV0dXJuIGJ0bi1oZXJvLWRhcmstZ3JhZGllbnQoJGNvbG9yLWxlZnQsICRjb2xvci1yaWdodCk7XG59XG5cbkBmdW5jdGlvbiBidG4taGVyby1zdWNjZXNzLWRhcmstZ3JhZGllbnQoKSB7XG4gICRjb2xvci1yaWdodDogbmItdGhlbWUoYnRuLXN1Y2Nlc3MtYmcpO1xuICAkY29sb3ItbGVmdDogYnRuLWhlcm8tc3VjY2Vzcy1sZWZ0LWNvbG9yKCk7XG5cbiAgQHJldHVybiBidG4taGVyby1kYXJrLWdyYWRpZW50KCRjb2xvci1sZWZ0LCAkY29sb3ItcmlnaHQpO1xufVxuXG5AZnVuY3Rpb24gYnRuLWhlcm8td2FybmluZy1kYXJrLWdyYWRpZW50KCkge1xuICAkY29sb3ItcmlnaHQ6IG5iLXRoZW1lKGJ0bi13YXJuaW5nLWJnKTtcbiAgJGNvbG9yLWxlZnQ6IGJ0bi1oZXJvLXdhcm5pbmctbGVmdC1jb2xvcigpO1xuXG4gIEByZXR1cm4gYnRuLWhlcm8tZGFyay1ncmFkaWVudCgkY29sb3ItbGVmdCwgJGNvbG9yLXJpZ2h0KTtcbn1cblxuQGZ1bmN0aW9uIGJ0bi1oZXJvLWluZm8tZGFyay1ncmFkaWVudCgpIHtcbiAgJGNvbG9yLXJpZ2h0OiBuYi10aGVtZShidG4taW5mby1iZyk7XG4gICRjb2xvci1sZWZ0OiBidG4taGVyby1pbmZvLWxlZnQtY29sb3IoKTtcblxuICBAcmV0dXJuIGJ0bi1oZXJvLWRhcmstZ3JhZGllbnQoJGNvbG9yLWxlZnQsICRjb2xvci1yaWdodCk7XG59XG5cbkBmdW5jdGlvbiBidG4taGVyby1kYW5nZXItZGFyay1ncmFkaWVudCgpIHtcbiAgJGNvbG9yLXJpZ2h0OiBuYi10aGVtZShidG4tZGFuZ2VyLWJnKTtcbiAgJGNvbG9yLWxlZnQ6IGJ0bi1oZXJvLWRhbmdlci1sZWZ0LWNvbG9yKCk7XG5cbiAgQHJldHVybiBidG4taGVyby1kYXJrLWdyYWRpZW50KCRjb2xvci1sZWZ0LCAkY29sb3ItcmlnaHQpO1xufVxuLy8gRW5kIGZ1bmN0aW9uc1xuXG4vLyBIZWxwIG1peGluc1xuQG1peGluIGJ0bi1oZXJvLXRleHQoKSB7XG4gIHRleHQtc2hhZG93OiBuYi10aGVtZShidG4taGVyby10ZXh0LXNoYWRvdyk7XG59XG5cbkBtaXhpbiBidG4taGVyby1ob3ZlcigkbGlnaHQtZ3JhZGllbnQpIHtcbiAgJjpob3ZlcixcbiAgLmhvdmVyIHtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiAkbGlnaHQtZ3JhZGllbnQ7XG4gIH1cbn1cblxuQG1peGluIGJ0bi1oZXJvLWZvY3VzKCRsaWdodC1ncmFkaWVudCkge1xuICAmOmZvY3VzLFxuICAuZm9jdXMge1xuICAgIGJhY2tncm91bmQtaW1hZ2U6ICRsaWdodC1ncmFkaWVudDtcbiAgfVxufVxuXG5AbWl4aW4gYnRuLWhlcm8tYWN0aXZlKCRkYXJrLWdyYWRpZW50KSB7XG4gICY6YWN0aXZlLFxuICAuYWN0aXZlIHtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiAkZGFyay1ncmFkaWVudDtcbiAgICBib3gtc2hhZG93OiBub25lO1xuICAgIGJvcmRlci1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gIH1cbn1cblxuQG1peGluIGJ0bi1oZXJvLWJvcmRlci1yYWRpdXMoKSB7XG4gIGJvcmRlci1yYWRpdXM6IG5iLXRoZW1lKGJ0bi1oZXJvLWJvcmRlci1yYWRpdXMpO1xufVxuLy8gRW5kIGhlbHAgbWl4aW5zXG5cblxuLy8gR3JhZGllbnRcbkBtaXhpbiBidG4taGVyby1wcmltYXJ5LWdyYWRpZW50KCkge1xuICAkY29sb3ItcmlnaHQ6IG5iLXRoZW1lKGJ0bi1wcmltYXJ5LWJnKTtcbiAgJGNvbG9yLWxlZnQ6IGJ0bi1oZXJvLXByaW1hcnktbGVmdC1jb2xvcigpO1xuXG4gIEBpbmNsdWRlIG5iLXJpZ2h0LWdyYWRpZW50KCRjb2xvci1sZWZ0LCAkY29sb3ItcmlnaHQpO1xufVxuXG5AbWl4aW4gYnRuLWhlcm8tc3VjY2Vzcy1ncmFkaWVudCgpIHtcbiAgJGNvbG9yLXJpZ2h0OiBuYi10aGVtZShidG4tc3VjY2Vzcy1iZyk7XG4gICRjb2xvci1sZWZ0OiBidG4taGVyby1zdWNjZXNzLWxlZnQtY29sb3IoKTtcblxuICBAaW5jbHVkZSBuYi1yaWdodC1ncmFkaWVudCgkY29sb3ItbGVmdCwgJGNvbG9yLXJpZ2h0KTtcbn1cblxuQG1peGluIGJ0bi1oZXJvLXdhcm5pbmctZ3JhZGllbnQoKSB7XG4gICRjb2xvci1yaWdodDogbmItdGhlbWUoYnRuLXdhcm5pbmctYmcpO1xuICAkY29sb3ItbGVmdDogYnRuLWhlcm8td2FybmluZy1sZWZ0LWNvbG9yKCk7XG5cbiAgQGluY2x1ZGUgbmItcmlnaHQtZ3JhZGllbnQoJGNvbG9yLWxlZnQsICRjb2xvci1yaWdodCk7XG59XG5cbkBtaXhpbiBidG4taGVyby1pbmZvLWdyYWRpZW50KCkge1xuICAkY29sb3ItcmlnaHQ6IG5iLXRoZW1lKGJ0bi1pbmZvLWJnKTtcbiAgJGNvbG9yLWxlZnQ6IGJ0bi1oZXJvLWluZm8tbGVmdC1jb2xvcigpO1xuXG4gIEBpbmNsdWRlIG5iLXJpZ2h0LWdyYWRpZW50KCRjb2xvci1sZWZ0LCAkY29sb3ItcmlnaHQpO1xufVxuXG5AbWl4aW4gYnRuLWhlcm8tZGFuZ2VyLWdyYWRpZW50KCkge1xuICAkY29sb3ItcmlnaHQ6IG5iLXRoZW1lKGJ0bi1kYW5nZXItYmcpO1xuICAkY29sb3ItbGVmdDogYnRuLWhlcm8tZGFuZ2VyLWxlZnQtY29sb3IoKTtcblxuICBAaW5jbHVkZSBuYi1yaWdodC1ncmFkaWVudCgkY29sb3ItbGVmdCwgJGNvbG9yLXJpZ2h0KTtcbn1cblxuQG1peGluIGJ0bi1oZXJvLXNlY29uZGFyeS1iZygpIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogbmItdGhlbWUoYnRuLXNlY29uZGFyeS1iZyk7XG59XG5cblxuLy8gQmV2ZWxcbkBmdW5jdGlvbiBidG4taGVyby1wcmltYXJ5LWJldmVsKCkge1xuICBAcmV0dXJuIGJ0bi1oZXJvLWJldmVsKGJ0bi1oZXJvLXByaW1hcnktbWlkZGxlLWNvbG9yKCkpO1xufVxuXG5AZnVuY3Rpb24gYnRuLWhlcm8tc3VjY2Vzcy1iZXZlbCgpIHtcbiAgQHJldHVybiBidG4taGVyby1iZXZlbChidG4taGVyby1zdWNjZXNzLW1pZGRsZS1jb2xvcigpKTtcbn1cblxuQGZ1bmN0aW9uIGJ0bi1oZXJvLXdhcm5pbmctYmV2ZWwoKSB7XG4gIEByZXR1cm4gYnRuLWhlcm8tYmV2ZWwoYnRuLWhlcm8td2FybmluZy1taWRkbGUtY29sb3IoKSk7XG59XG5cbkBmdW5jdGlvbiBidG4taGVyby1pbmZvLWJldmVsKCkge1xuICBAcmV0dXJuIGJ0bi1oZXJvLWJldmVsKGJ0bi1oZXJvLWluZm8tbWlkZGxlLWNvbG9yKCkpO1xufVxuXG5AZnVuY3Rpb24gYnRuLWhlcm8tZGFuZ2VyLWJldmVsKCkge1xuICBAcmV0dXJuIGJ0bi1oZXJvLWJldmVsKGJ0bi1oZXJvLWRhbmdlci1taWRkbGUtY29sb3IoKSk7XG59XG5cbkBmdW5jdGlvbiBidG4taGVyby1zZWNvbmRhcnktYmV2ZWwoKSB7XG4gIEByZXR1cm4gYnRuLWhlcm8tYmV2ZWwoYnRuLWhlcm8tc2Vjb25kYXJ5LW1pZGRsZS1jb2xvcigpKTtcbn1cblxuXG4vLyBHbG93XG5AZnVuY3Rpb24gYnRuLWhlcm8tcHJpbWFyeS1nbG93KCkge1xuICBAcmV0dXJuIGJ0bi1oZXJvLWdsb3coYnRuLWhlcm8tcHJpbWFyeS1nbG93LXNpemUsIGJ0bi1oZXJvLXByaW1hcnktbWlkZGxlLWNvbG9yKCkpO1xufVxuXG5AZnVuY3Rpb24gYnRuLWhlcm8tc3VjY2Vzcy1nbG93KCkge1xuICBAcmV0dXJuIGJ0bi1oZXJvLWdsb3coYnRuLWhlcm8tc3VjY2Vzcy1nbG93LXNpemUsIGJ0bi1oZXJvLXN1Y2Nlc3MtbWlkZGxlLWNvbG9yKCkpO1xufVxuXG5AZnVuY3Rpb24gYnRuLWhlcm8td2FybmluZy1nbG93KCkge1xuICBAcmV0dXJuIGJ0bi1oZXJvLWdsb3coYnRuLWhlcm8td2FybmluZy1nbG93LXNpemUsIGJ0bi1oZXJvLXdhcm5pbmctbWlkZGxlLWNvbG9yKCkpO1xufVxuXG5AZnVuY3Rpb24gYnRuLWhlcm8taW5mby1nbG93KCkge1xuICBAcmV0dXJuIGJ0bi1oZXJvLWdsb3coYnRuLWhlcm8taW5mby1nbG93LXNpemUsIGJ0bi1oZXJvLWluZm8tbWlkZGxlLWNvbG9yKCkpO1xufVxuXG5AZnVuY3Rpb24gYnRuLWhlcm8tZGFuZ2VyLWdsb3coKSB7XG4gIEByZXR1cm4gYnRuLWhlcm8tZ2xvdyhidG4taGVyby1kYW5nZXItZ2xvdy1zaXplLCBidG4taGVyby1kYW5nZXItbWlkZGxlLWNvbG9yKCkpO1xufVxuXG5AZnVuY3Rpb24gYnRuLWhlcm8tc2Vjb25kYXJ5LWdsb3coKSB7XG4gIEByZXR1cm4gYnRuLWhlcm8tZ2xvdyhidG4taGVyby1zZWNvbmRhcnktZ2xvdy1zaXplLCBidG4taGVyby1zZWNvbmRhcnktbWlkZGxlLWNvbG9yKCkpO1xufVxuXG5cbi8vIEJldmVsLWdsb3ctc2hhZG93XG5AbWl4aW4gYnRuLWhlcm8tYmV2ZWwtZ2xvdy1zaGFkb3coJGJldmVsLCAkZ2xvdywgJHNoYWRvdykge1xuICAkYm94LXNoYWRvdzogJGJldmVsLCAkZ2xvdztcbiAgQGlmICgkc2hhZG93ICE9ICdub25lJykge1xuICAgICRib3gtc2hhZG93OiAkYm94LXNoYWRvdywgJHNoYWRvdztcbiAgfVxuICBib3gtc2hhZG93OiAkYm94LXNoYWRvdztcbn1cblxuQG1peGluIGJ0bi1oZXJvLXByaW1hcnktYmV2ZWwtZ2xvdy1zaGFkb3coKSB7XG4gICRiZXZlbDogYnRuLWhlcm8tcHJpbWFyeS1iZXZlbCgpO1xuICAkZ2xvdzogYnRuLWhlcm8tcHJpbWFyeS1nbG93KCk7XG4gICRzaGFkb3c6IG5iLXRoZW1lKGJ0bi1oZXJvLXNoYWRvdyk7XG5cbiAgQGluY2x1ZGUgYnRuLWhlcm8tYmV2ZWwtZ2xvdy1zaGFkb3coJGJldmVsLCAkZ2xvdywgJHNoYWRvdyk7XG59XG5cbkBtaXhpbiBidG4taGVyby1zdWNjZXNzLWJldmVsLWdsb3ctc2hhZG93KCkge1xuICAkYmV2ZWw6IGJ0bi1oZXJvLXN1Y2Nlc3MtYmV2ZWwoKTtcbiAgJGdsb3c6IGJ0bi1oZXJvLXN1Y2Nlc3MtZ2xvdygpO1xuICAkc2hhZG93OiBuYi10aGVtZShidG4taGVyby1zaGFkb3cpO1xuXG4gIEBpbmNsdWRlIGJ0bi1oZXJvLWJldmVsLWdsb3ctc2hhZG93KCRiZXZlbCwgJGdsb3csICRzaGFkb3cpO1xufVxuXG5AbWl4aW4gYnRuLWhlcm8td2FybmluZy1iZXZlbC1nbG93LXNoYWRvdygpIHtcbiAgJGJldmVsOiBidG4taGVyby13YXJuaW5nLWJldmVsKCk7XG4gICRnbG93OiBidG4taGVyby13YXJuaW5nLWdsb3coKTtcbiAgJHNoYWRvdzogbmItdGhlbWUoYnRuLWhlcm8tc2hhZG93KTtcblxuICBAaW5jbHVkZSBidG4taGVyby1iZXZlbC1nbG93LXNoYWRvdygkYmV2ZWwsICRnbG93LCAkc2hhZG93KTtcbn1cblxuQG1peGluIGJ0bi1oZXJvLWluZm8tYmV2ZWwtZ2xvdy1zaGFkb3coKSB7XG4gICRiZXZlbDogYnRuLWhlcm8taW5mby1iZXZlbCgpO1xuICAkZ2xvdzogYnRuLWhlcm8taW5mby1nbG93KCk7XG4gICRzaGFkb3c6IG5iLXRoZW1lKGJ0bi1oZXJvLXNoYWRvdyk7XG5cbiAgQGluY2x1ZGUgYnRuLWhlcm8tYmV2ZWwtZ2xvdy1zaGFkb3coJGJldmVsLCAkZ2xvdywgJHNoYWRvdyk7XG59XG5cbkBtaXhpbiBidG4taGVyby1kYW5nZXItYmV2ZWwtZ2xvdy1zaGFkb3coKSB7XG4gICRiZXZlbDogYnRuLWhlcm8tZGFuZ2VyLWJldmVsKCk7XG4gICRnbG93OiBidG4taGVyby1kYW5nZXItZ2xvdygpO1xuICAkc2hhZG93OiBuYi10aGVtZShidG4taGVyby1zaGFkb3cpO1xuXG4gIEBpbmNsdWRlIGJ0bi1oZXJvLWJldmVsLWdsb3ctc2hhZG93KCRiZXZlbCwgJGdsb3csICRzaGFkb3cpO1xufVxuXG5AbWl4aW4gYnRuLWhlcm8tc2Vjb25kYXJ5LWJldmVsLWdsb3ctc2hhZG93KCkge1xuICAkYmV2ZWw6IGJ0bi1oZXJvLXNlY29uZGFyeS1iZXZlbCgpO1xuICAkZ2xvdzogYnRuLWhlcm8tc2Vjb25kYXJ5LWdsb3coKTtcbiAgJHNoYWRvdzogbmItdGhlbWUoYnRuLWhlcm8tc2hhZG93KTtcblxuICBAaW5jbHVkZSBidG4taGVyby1iZXZlbC1nbG93LXNoYWRvdygkYmV2ZWwsICRnbG93LCAkc2hhZG93KTtcbn1cblxuXG4vLyBCb3JkZXJcbkBtaXhpbiBidG4taGVyby1wcmltYXJ5LWJvcmRlcigpIHtcbiAgYm9yZGVyOiBub25lO1xufVxuXG5AbWl4aW4gYnRuLWhlcm8tc3VjY2Vzcy1ib3JkZXIoKSB7XG4gIGJvcmRlcjogbm9uZTtcbn1cblxuQG1peGluIGJ0bi1oZXJvLXdhcm5pbmctYm9yZGVyKCkge1xuICBib3JkZXI6IG5vbmU7XG59XG5cbkBtaXhpbiBidG4taGVyby1pbmZvLWJvcmRlcigpIHtcbiAgYm9yZGVyOiBub25lO1xufVxuXG5AbWl4aW4gYnRuLWhlcm8tZGFuZ2VyLWJvcmRlcigpIHtcbiAgYm9yZGVyOiBub25lO1xufVxuXG5AbWl4aW4gYnRuLWhlcm8tc2Vjb25kYXJ5LWJvcmRlcigpIHtcbiAgYm9yZGVyOiAycHggc29saWQgbmItdGhlbWUoYnRuLXNlY29uZGFyeS1ib3JkZXIpO1xuXG4gIEBpbmNsdWRlIG5iLWZvci10aGVtZShjb3Jwb3JhdGUpIHtcbiAgICBib3JkZXI6IG5vbmU7XG4gIH1cbn1cblxuXG4vLyBIb3ZlclxuQG1peGluIGJ0bi1oZXJvLXByaW1hcnktaG92ZXIoKSB7XG4gIEBpbmNsdWRlIGJ0bi1oZXJvLWhvdmVyKGJ0bi1oZXJvLXByaW1hcnktbGlnaHQtZ3JhZGllbnQoKSk7XG59XG5cbkBtaXhpbiBidG4taGVyby1zdWNjZXNzLWhvdmVyKCkge1xuICBAaW5jbHVkZSBidG4taGVyby1ob3ZlcihidG4taGVyby1zdWNjZXNzLWxpZ2h0LWdyYWRpZW50KCkpO1xufVxuXG5AbWl4aW4gYnRuLWhlcm8td2FybmluZy1ob3ZlcigpIHtcbiAgQGluY2x1ZGUgYnRuLWhlcm8taG92ZXIoYnRuLWhlcm8td2FybmluZy1saWdodC1ncmFkaWVudCgpKTtcbn1cblxuQG1peGluIGJ0bi1oZXJvLWluZm8taG92ZXIoKSB7XG4gIEBpbmNsdWRlIGJ0bi1oZXJvLWhvdmVyKGJ0bi1oZXJvLWluZm8tbGlnaHQtZ3JhZGllbnQoKSk7XG59XG5cbkBtaXhpbiBidG4taGVyby1kYW5nZXItaG92ZXIoKSB7XG4gIEBpbmNsdWRlIGJ0bi1oZXJvLWhvdmVyKGJ0bi1oZXJvLWRhbmdlci1saWdodC1ncmFkaWVudCgpKTtcbn1cblxuQG1peGluIGJ0bi1oZXJvLXNlY29uZGFyeS1ob3ZlcigpIHtcbiAgJjpob3ZlcixcbiAgLmhvdmVyIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKG5iLXRoZW1lKGJ0bi1zZWNvbmRhcnktYm9yZGVyKSwgMC4yKTtcbiAgfVxufVxuXG4vLyBGb2N1c1xuQG1peGluIGJ0bi1oZXJvLXByaW1hcnktZm9jdXMoKSB7XG4gIEBpbmNsdWRlIGJ0bi1oZXJvLWZvY3VzKGJ0bi1oZXJvLXByaW1hcnktbGlnaHQtZ3JhZGllbnQoKSk7XG59XG5cbkBtaXhpbiBidG4taGVyby1zdWNjZXNzLWZvY3VzKCkge1xuICBAaW5jbHVkZSBidG4taGVyby1mb2N1cyhidG4taGVyby1zdWNjZXNzLWxpZ2h0LWdyYWRpZW50KCkpO1xufVxuXG5AbWl4aW4gYnRuLWhlcm8td2FybmluZy1mb2N1cygpIHtcbiAgQGluY2x1ZGUgYnRuLWhlcm8tZm9jdXMoYnRuLWhlcm8td2FybmluZy1saWdodC1ncmFkaWVudCgpKTtcbn1cblxuQG1peGluIGJ0bi1oZXJvLWluZm8tZm9jdXMoKSB7XG4gIEBpbmNsdWRlIGJ0bi1oZXJvLWZvY3VzKGJ0bi1oZXJvLWluZm8tbGlnaHQtZ3JhZGllbnQoKSk7XG59XG5cbkBtaXhpbiBidG4taGVyby1kYW5nZXItZm9jdXMoKSB7XG4gIEBpbmNsdWRlIGJ0bi1oZXJvLWZvY3VzKGJ0bi1oZXJvLWRhbmdlci1saWdodC1ncmFkaWVudCgpKTtcbn1cblxuQG1peGluIGJ0bi1oZXJvLXNlY29uZGFyeS1mb2N1cygpIHtcbiAgJGNvbG9yOiBuYi10aGVtZShidG4tc2Vjb25kYXJ5LWJvcmRlcik7XG5cbiAgJjpmb2N1cyxcbiAgLmZvY3VzIHtcbiAgICBib3JkZXItY29sb3I6IHRpbnQoJGNvbG9yLCAxNCUpO1xuICB9XG59XG5cbi8vIEFjdGl2ZVxuQG1peGluIGJ0bi1oZXJvLXByaW1hcnktYWN0aXZlKCkge1xuICBAaW5jbHVkZSBidG4taGVyby1hY3RpdmUoYnRuLWhlcm8tcHJpbWFyeS1kYXJrLWdyYWRpZW50KCkpO1xufVxuXG5AbWl4aW4gYnRuLWhlcm8tc3VjY2Vzcy1hY3RpdmUoKSB7XG4gIEBpbmNsdWRlIGJ0bi1oZXJvLWFjdGl2ZShidG4taGVyby1zdWNjZXNzLWRhcmstZ3JhZGllbnQoKSk7XG59XG5cbkBtaXhpbiBidG4taGVyby13YXJuaW5nLWFjdGl2ZSgpIHtcbiAgQGluY2x1ZGUgYnRuLWhlcm8tYWN0aXZlKGJ0bi1oZXJvLXdhcm5pbmctZGFyay1ncmFkaWVudCgpKTtcbn1cblxuQG1peGluIGJ0bi1oZXJvLWluZm8tYWN0aXZlKCkge1xuICBAaW5jbHVkZSBidG4taGVyby1hY3RpdmUoYnRuLWhlcm8taW5mby1kYXJrLWdyYWRpZW50KCkpO1xufVxuXG5AbWl4aW4gYnRuLWhlcm8tZGFuZ2VyLWFjdGl2ZSgpIHtcbiAgQGluY2x1ZGUgYnRuLWhlcm8tYWN0aXZlKGJ0bi1oZXJvLWRhbmdlci1kYXJrLWdyYWRpZW50KCkpO1xufVxuXG5AbWl4aW4gYnRuLWhlcm8tc2Vjb25kYXJ5LWFjdGl2ZSgpIHtcbiAgJGNvbG9yOiBuYi10aGVtZShidG4tc2Vjb25kYXJ5LWJvcmRlcik7XG5cbiAgJjphY3RpdmUsXG4gIC5hY3RpdmUge1xuICAgIGJvcmRlci1jb2xvcjogc2hhZGUoJGNvbG9yLCAxNCUpO1xuICAgIGJveC1zaGFkb3c6IG5vbmU7XG4gICAgYmFja2dyb3VuZDogbm9uZTtcbiAgfVxufVxuXG5cbi8vIERpc2FibGVkXG5AbWl4aW4gYnRuLWhlcm8tZGlzYWJsZWQoKSB7XG4gICY6ZGlzYWJsZWQge1xuICAgIG9wYWNpdHk6IG5iLXRoZW1lKGJ0bi1kaXNhYmxlZC1vcGFjaXR5KTtcbiAgICBib3gtc2hhZG93OiBub25lO1xuICB9XG59XG5cbi8vIExpbmUgaGVpZ2h0XG5AZnVuY3Rpb24gYnRuLWhlcm8tbGluZS1oZWlnaHQoJGZvbnQtc2l6ZSkge1xuICBAcmV0dXJuIGNhbGMoKCN7JGZvbnQtc2l6ZX0gKiAxLjI1KSArIDRweCk7XG59XG5cbkBmdW5jdGlvbiBidG4taGVyby1saW5lLWhlaWdodC1sZygpIHtcbiAgQHJldHVybiBidG4taGVyby1saW5lLWhlaWdodChuYi10aGVtZShidG4tZm9udC1zaXplLWxnKSk7XG59XG5cbkBmdW5jdGlvbiBidG4taGVyby1saW5lLWhlaWdodC1tZCgpIHtcbiAgQHJldHVybiBidG4taGVyby1saW5lLWhlaWdodChuYi10aGVtZShidG4tZm9udC1zaXplLW1kKSk7XG59XG5cbkBmdW5jdGlvbiBidG4taGVyby1saW5lLWhlaWdodC1zbSgpIHtcbiAgQHJldHVybiBidG4taGVyby1saW5lLWhlaWdodChuYi10aGVtZShidG4tZm9udC1zaXplLXNtKSk7XG59XG5cbkBmdW5jdGlvbiBidG4taGVyby1saW5lLWhlaWdodC10bigpIHtcbiAgQHJldHVybiBidG4taGVyby1saW5lLWhlaWdodChuYi10aGVtZShidG4tZm9udC1zaXplLXRuKSk7XG59XG5cblxuQG1peGluIGJ0bi1oZXJvLWxpbmUtaGVpZ2h0KCkge1xuXG4gIGxpbmUtaGVpZ2h0OiBidG4taGVyby1saW5lLWhlaWdodC1tZCgpO1xuXG4gICYuYnRuLmJ0bi1sZyB7XG4gICAgbGluZS1oZWlnaHQ6IGJ0bi1oZXJvLWxpbmUtaGVpZ2h0LWxnKCk7XG4gIH1cblxuICAmLmJ0bi5idG4tbWQge1xuICAgIGxpbmUtaGVpZ2h0OiBidG4taGVyby1saW5lLWhlaWdodC1tZCgpO1xuICB9XG5cbiAgJi5idG4uYnRuLXNtIHtcbiAgICBsaW5lLWhlaWdodDogYnRuLWhlcm8tbGluZS1oZWlnaHQtc20oKTtcbiAgfVxuXG4gICYuYnRuLmJ0bi10biB7XG4gICAgbGluZS1oZWlnaHQ6IGJ0bi1oZXJvLWxpbmUtaGVpZ2h0LXRuKCk7XG4gIH1cbn1cblxuLy8gUHVsc2VcbkBtaXhpbiBidG4taGVyby1wcmltYXJ5LXB1bHNlKCkge1xuICBAaW5jbHVkZSBidG4tcHVsc2UoaGVyby1wcmltYXJ5LCBuYi10aGVtZShjb2xvci1wcmltYXJ5KSk7XG59XG5AbWl4aW4gYnRuLWhlcm8tc3VjY2Vzcy1wdWxzZSgpIHtcbiAgQGluY2x1ZGUgYnRuLXB1bHNlKGhlcm8tc3VjY2VzcywgbmItdGhlbWUoY29sb3Itc3VjY2VzcykpO1xufVxuQG1peGluIGJ0bi1oZXJvLWRhbmdlci1wdWxzZSgpIHtcbiAgQGluY2x1ZGUgYnRuLXB1bHNlKGhlcm8tZGFuZ2VyLCBuYi10aGVtZShjb2xvci1kYW5nZXIpKTtcbn1cbkBtaXhpbiBidG4taGVyby1pbmZvLXB1bHNlKCkge1xuICBAaW5jbHVkZSBidG4tcHVsc2UoaGVyby1pbmZvLCBuYi10aGVtZShjb2xvci1pbmZvKSk7XG59XG5AbWl4aW4gYnRuLWhlcm8td2FybmluZy1wdWxzZSgpIHtcbiAgQGluY2x1ZGUgYnRuLXB1bHNlKGhlcm8td2FybmluZywgbmItdGhlbWUoY29sb3Itd2FybmluZykpO1xufVxuQG1peGluIGJ0bi1oZXJvLXNlY29uZGFyeS1wdWxzZSgpIHtcbiAgQGluY2x1ZGUgIGJ0bi1wdWxzZShoZXJvLXNlY29uZGFyeSwgbmItdGhlbWUoYnRuLXNlY29uZGFyeS1ib3JkZXIpKTtcbn1cbiIsIi8qKlxuICogQGxpY2Vuc2VcbiAqIENvcHlyaWdodCBBa3Zlby4gQWxsIFJpZ2h0cyBSZXNlcnZlZC5cbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgTGljZW5zZS4gU2VlIExpY2Vuc2UudHh0IGluIHRoZSBwcm9qZWN0IHJvb3QgZm9yIGxpY2Vuc2UgaW5mb3JtYXRpb24uXG4gKi9cblxuQGltcG9ydCAnLi9kZWZhdWx0LWJ1dHRvbnMnO1xuXG5AbWl4aW4gYnRuLW91dGxpbmUoKSB7XG4gIC5idG4uYnRuLW91dGxpbmUtcHJpbWFyeSB7XG4gICAgQGluY2x1ZGUgYnRuLW91dGxpbmUtcHJpbWFyeSgpO1xuICB9XG5cbiAgLmJ0bi5idG4tb3V0bGluZS13YXJuaW5nIHtcbiAgICBAaW5jbHVkZSBidG4tb3V0bGluZS13YXJuaW5nKCk7XG4gIH1cblxuICAuYnRuLmJ0bi1vdXRsaW5lLXN1Y2Nlc3Mge1xuICAgIEBpbmNsdWRlIGJ0bi1vdXRsaW5lLXN1Y2Nlc3MoKTtcbiAgfVxuXG4gIC5idG4uYnRuLW91dGxpbmUtaW5mbyB7XG4gICAgQGluY2x1ZGUgYnRuLW91dGxpbmUtaW5mbygpO1xuICB9XG5cbiAgLmJ0bi5idG4tb3V0bGluZS1kYW5nZXIge1xuICAgIEBpbmNsdWRlIGJ0bi1vdXRsaW5lLWRhbmdlcigpO1xuICB9XG5cbiAgLmJ0bi5idG4tb3V0bGluZS1zZWNvbmRhcnkge1xuICAgIEBpbmNsdWRlIGJ0bi1vdXRsaW5lLXNlY29uZGFyeSgpO1xuICB9XG59XG5cbkBtaXhpbiBidG4tb3V0bGluZS1wcmltYXJ5KCkge1xuICBAaW5jbHVkZSBidG4tb3V0bGluZS1wcmltYXJ5LWJvcmRlcigpO1xuICBAaW5jbHVkZSBidG4tb3V0bGluZS1mZygpO1xuICBAaW5jbHVkZSBidG4tb3V0bGluZS1iZygpO1xuICBAaW5jbHVkZSBidG4tb3V0bGluZS1wcmltYXJ5LWZvY3VzKCk7XG4gIEBpbmNsdWRlIGJ0bi1vdXRsaW5lLXByaW1hcnktaG92ZXIoKTtcbiAgQGluY2x1ZGUgYnRuLW91dGxpbmUtcHJpbWFyeS1hY3RpdmUoKTtcbn1cblxuQG1peGluIGJ0bi1vdXRsaW5lLXdhcm5pbmcoKSB7XG4gIEBpbmNsdWRlIGJ0bi1vdXRsaW5lLXdhcm5pbmctYm9yZGVyKCk7XG4gIEBpbmNsdWRlIGJ0bi1vdXRsaW5lLWZnKCk7XG4gIEBpbmNsdWRlIGJ0bi1vdXRsaW5lLWJnKCk7XG4gIEBpbmNsdWRlIGJ0bi1vdXRsaW5lLXdhcm5pbmctZm9jdXMoKTtcbiAgQGluY2x1ZGUgYnRuLW91dGxpbmUtd2FybmluZy1ob3ZlcigpO1xuICBAaW5jbHVkZSBidG4tb3V0bGluZS13YXJuaW5nLWFjdGl2ZSgpO1xufVxuXG5AbWl4aW4gYnRuLW91dGxpbmUtc3VjY2VzcygpIHtcbiAgQGluY2x1ZGUgYnRuLW91dGxpbmUtc3VjY2Vzcy1ib3JkZXIoKTtcbiAgQGluY2x1ZGUgYnRuLW91dGxpbmUtZmcoKTtcbiAgQGluY2x1ZGUgYnRuLW91dGxpbmUtYmcoKTtcbiAgQGluY2x1ZGUgYnRuLW91dGxpbmUtc3VjY2Vzcy1mb2N1cygpO1xuICBAaW5jbHVkZSBidG4tb3V0bGluZS1zdWNjZXNzLWhvdmVyKCk7XG4gIEBpbmNsdWRlIGJ0bi1vdXRsaW5lLXN1Y2Nlc3MtYWN0aXZlKCk7XG59XG5cbkBtaXhpbiBidG4tb3V0bGluZS1pbmZvKCkge1xuICBAaW5jbHVkZSBidG4tb3V0bGluZS1pbmZvLWJvcmRlcigpO1xuICBAaW5jbHVkZSBidG4tb3V0bGluZS1mZygpO1xuICBAaW5jbHVkZSBidG4tb3V0bGluZS1iZygpO1xuICBAaW5jbHVkZSBidG4tb3V0bGluZS1pbmZvLWZvY3VzKCk7XG4gIEBpbmNsdWRlIGJ0bi1vdXRsaW5lLWluZm8taG92ZXIoKTtcbiAgQGluY2x1ZGUgYnRuLW91dGxpbmUtaW5mby1hY3RpdmUoKTtcbn1cblxuQG1peGluIGJ0bi1vdXRsaW5lLWRhbmdlcigpIHtcbiAgQGluY2x1ZGUgYnRuLW91dGxpbmUtZGFuZ2VyLWJvcmRlcigpO1xuICBAaW5jbHVkZSBidG4tb3V0bGluZS1mZygpO1xuICBAaW5jbHVkZSBidG4tb3V0bGluZS1iZygpO1xuICBAaW5jbHVkZSBidG4tb3V0bGluZS1kYW5nZXItZm9jdXMoKTtcbiAgQGluY2x1ZGUgYnRuLW91dGxpbmUtZGFuZ2VyLWhvdmVyKCk7XG4gIEBpbmNsdWRlIGJ0bi1vdXRsaW5lLWRhbmdlci1hY3RpdmUoKTtcbn1cblxuQG1peGluIGJ0bi1vdXRsaW5lLXNlY29uZGFyeSgpIHtcbiAgQGluY2x1ZGUgYnRuLW91dGxpbmUtc2Vjb25kYXJ5LWJvcmRlcigpO1xuICBAaW5jbHVkZSBidG4tb3V0bGluZS1mZygpO1xuICBAaW5jbHVkZSBidG4tb3V0bGluZS1iZygpO1xuICBAaW5jbHVkZSBidG4tb3V0bGluZS1zZWNvbmRhcnktZm9jdXMoKTtcbiAgQGluY2x1ZGUgYnRuLW91dGxpbmUtc2Vjb25kYXJ5LWhvdmVyKCk7XG4gIEBpbmNsdWRlIGJ0bi1vdXRsaW5lLXNlY29uZGFyeS1hY3RpdmUoKTtcblxuICAmOmZvY3VzLCAmLmZvY3VzLFxuICAmOmhvdmVyLCAmLmhvdmVyLFxuICAmOmFjdGl2ZSwgJi5hY3RpdmUge1xuICAgIEBpbmNsdWRlIGJ0bi1vdXRsaW5lLWZnKCk7XG4gIH1cbn1cblxuQG1peGluIGJ0bi1vdXRsaW5lLWJvcmRlcigkY29sb3IpIHtcbiAgYm9yZGVyOiAycHggc29saWQgJGNvbG9yO1xufVxuXG5AbWl4aW4gYnRuLW91dGxpbmUtZmcoKSB7XG4gIGNvbG9yOiBuYi10aGVtZShidG4tb3V0bGluZS1mZyk7XG59XG5cbkBtaXhpbiBidG4tb3V0bGluZS1iZygpIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG59XG5cbi8vIEhvdmVyXG5AbWl4aW4gYnRuLW91dGxpbmUtcHJpbWFyeS1ob3ZlcigpIHtcbiAgQGluY2x1ZGUgYnRuLXByaW1hcnktaG92ZXIoKTtcbn1cblxuQG1peGluIGJ0bi1vdXRsaW5lLXdhcm5pbmctaG92ZXIoKSB7XG4gIEBpbmNsdWRlIGJ0bi13YXJuaW5nLWhvdmVyKCk7XG59XG5cbkBtaXhpbiBidG4tb3V0bGluZS1zdWNjZXNzLWhvdmVyKCkge1xuICBAaW5jbHVkZSBidG4tc3VjY2Vzcy1ob3ZlcigpO1xufVxuXG5AbWl4aW4gYnRuLW91dGxpbmUtaW5mby1ob3ZlcigpIHtcbiAgQGluY2x1ZGUgYnRuLWluZm8taG92ZXIoKTtcbn1cblxuQG1peGluIGJ0bi1vdXRsaW5lLWRhbmdlci1ob3ZlcigpIHtcbiAgQGluY2x1ZGUgYnRuLWRhbmdlci1ob3ZlcigpO1xufVxuXG5AbWl4aW4gYnRuLW91dGxpbmUtc2Vjb25kYXJ5LWhvdmVyKCkge1xuICBAaW5jbHVkZSBidG4tc2Vjb25kYXJ5LWhvdmVyKCk7XG59XG5cbi8vIEZvY3VzXG5AbWl4aW4gYnRuLW91dGxpbmUtcHJpbWFyeS1mb2N1cygpIHtcbiAgQGluY2x1ZGUgYnRuLW91dGxpbmUtZm9jdXMoYnRuLWZvY3VzLWNvbG9yKGJ0bi1wcmltYXJ5LWJnLCAyMCUpKTtcbn1cblxuQG1peGluIGJ0bi1vdXRsaW5lLXdhcm5pbmctZm9jdXMoKSB7XG4gIEBpbmNsdWRlIGJ0bi1vdXRsaW5lLWZvY3VzKGJ0bi1mb2N1cy1jb2xvcihidG4td2FybmluZy1iZywgMjAlKSk7XG59XG5cbkBtaXhpbiBidG4tb3V0bGluZS1zdWNjZXNzLWZvY3VzKCkge1xuICBAaW5jbHVkZSBidG4tb3V0bGluZS1mb2N1cyhidG4tZm9jdXMtY29sb3IoYnRuLXN1Y2Nlc3MtYmcsIDIwJSkpO1xufVxuXG5AbWl4aW4gYnRuLW91dGxpbmUtaW5mby1mb2N1cygpIHtcbiAgQGluY2x1ZGUgYnRuLW91dGxpbmUtZm9jdXMoYnRuLWZvY3VzLWNvbG9yKGJ0bi1pbmZvLWJnLCAyMCUpKTtcbn1cblxuQG1peGluIGJ0bi1vdXRsaW5lLWRhbmdlci1mb2N1cygpIHtcbiAgQGluY2x1ZGUgYnRuLW91dGxpbmUtZm9jdXMoYnRuLWZvY3VzLWNvbG9yKGJ0bi1kYW5nZXItYmcsIDIwJSkpO1xufVxuXG5AbWl4aW4gYnRuLW91dGxpbmUtc2Vjb25kYXJ5LWZvY3VzKCkge1xuICBAaW5jbHVkZSBidG4tb3V0bGluZS1mb2N1cyhidG4tZm9jdXMtY29sb3IoYnRuLXNlY29uZGFyeS1ib3JkZXIsIDIwJSkpO1xufVxuXG5cbi8vIEFjdGl2ZVxuQG1peGluIGJ0bi1vdXRsaW5lLXByaW1hcnktYWN0aXZlKCkge1xuICBAaW5jbHVkZSBidG4tcHJpbWFyeS1hY3RpdmUoKTtcbn1cblxuQG1peGluIGJ0bi1vdXRsaW5lLXdhcm5pbmctYWN0aXZlKCkge1xuICBAaW5jbHVkZSBidG4td2FybmluZy1hY3RpdmUoKTtcbn1cblxuQG1peGluIGJ0bi1vdXRsaW5lLXN1Y2Nlc3MtYWN0aXZlKCkge1xuICBAaW5jbHVkZSBidG4tc3VjY2Vzcy1hY3RpdmUoKTtcbn1cblxuQG1peGluIGJ0bi1vdXRsaW5lLWluZm8tYWN0aXZlKCkge1xuICBAaW5jbHVkZSBidG4taW5mby1hY3RpdmUoKTtcbn1cblxuQG1peGluIGJ0bi1vdXRsaW5lLWRhbmdlci1hY3RpdmUoKSB7XG4gIEBpbmNsdWRlIGJ0bi1kYW5nZXItYWN0aXZlKCk7XG59XG5cbkBtaXhpbiBidG4tb3V0bGluZS1zZWNvbmRhcnktYWN0aXZlKCkge1xuICBAaW5jbHVkZSBidG4tc2Vjb25kYXJ5LWFjdGl2ZSgpO1xufVxuXG5cbi8vIEJvcmRlclxuQG1peGluIGJ0bi1vdXRsaW5lLXByaW1hcnktYm9yZGVyKCkge1xuICBAaW5jbHVkZSBidG4tb3V0bGluZS1ib3JkZXIobmItdGhlbWUoYnRuLXByaW1hcnktYmcpKTtcbn1cblxuQG1peGluIGJ0bi1vdXRsaW5lLXdhcm5pbmctYm9yZGVyKCkge1xuICBAaW5jbHVkZSBidG4tb3V0bGluZS1ib3JkZXIobmItdGhlbWUoYnRuLXdhcm5pbmctYmcpKTtcbn1cblxuQG1peGluIGJ0bi1vdXRsaW5lLXN1Y2Nlc3MtYm9yZGVyKCkge1xuICBAaW5jbHVkZSBidG4tb3V0bGluZS1ib3JkZXIobmItdGhlbWUoYnRuLXN1Y2Nlc3MtYmcpKTtcbn1cblxuQG1peGluIGJ0bi1vdXRsaW5lLWluZm8tYm9yZGVyKCkge1xuICBAaW5jbHVkZSBidG4tb3V0bGluZS1ib3JkZXIobmItdGhlbWUoYnRuLWluZm8tYmcpKTtcbn1cblxuQG1peGluIGJ0bi1vdXRsaW5lLWRhbmdlci1ib3JkZXIoKSB7XG4gIEBpbmNsdWRlIGJ0bi1vdXRsaW5lLWJvcmRlcihuYi10aGVtZShidG4tZGFuZ2VyLWJnKSk7XG59XG5cbkBtaXhpbiBidG4tb3V0bGluZS1zZWNvbmRhcnktYm9yZGVyKCkge1xuICBAaW5jbHVkZSBidG4tb3V0bGluZS1ib3JkZXIobmItdGhlbWUoYnRuLXNlY29uZGFyeS1ib3JkZXIpKTtcbn1cbiIsIkBpbXBvcnQgJy4uLy4uLy4uLy4uL0B0aGVtZS9zdHlsZXMvdGhlbWVzLnNjc3MnO1xuQGltcG9ydCAnfkBuZWJ1bGFyL3RoZW1lL3N0eWxlcy9nbG9iYWwvYm9vdHN0cmFwL2J1dHRvbnMnO1xuQGluY2x1ZGUgbmItaW5zdGFsbC1jb21wb25lbnQoKSB7XG4gICAgLyogbmItY2FyZC1ib2R5IHtcbiAgICBwYWRkaW5nOiAwIDEuMjVyZW0gMS4yNXJlbSAwO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC13cmFwOiB3cmFwO1xuICB9ICovXG4gICAgLmJ0biB7XG4gICAgICAgIGxpbmUtaGVpZ2h0OiAxNHB4ICFpbXBvcnRhbnQ7XG4gICAgICAgIC8vIHBhZGRpbmc6IDAuNTVyZW0gMS4wcmVtICFpbXBvcnRhbnQ7XG4gICAgfVxuICAgIC5jb250YWluZXItdGl0bGUge1xuICAgICAgICBtYXJnaW4tYm90dG9tOiAwLjI1cmVtO1xuICAgIH1cbiAgICAuc2l6ZS1jb250YWluZXIge1xuICAgICAgICBtYXJnaW46IDEuMjVyZW0gMCAwIDEuMjVyZW07XG4gICAgfVxuICAgIC5zdWJoZWFkZXIge1xuICAgICAgICBtYXJnaW4tYm90dG9tOiAwLjc1cmVtO1xuICAgICAgICBmb250LXNpemU6IDAuODc1cmVtO1xuICAgICAgICBmb250LXdlaWdodDogbmItdGhlbWUoZm9udC13ZWlnaHQtYm9sZGVyKTtcbiAgICAgICAgY29sb3I6IG5iLXRoZW1lKGNvbG9yLWZnLWhlYWRpbmcpO1xuICAgIH1cbiAgICBidXR0b24ge1xuICAgICAgICBmbG9hdDogcmlnaHQ7XG4gICAgICAgIC8vIG1hcmdpbi10b3A6IC0yMXB4O1xuICAgIH1cbiAgICBpIHtcbiAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIH1cbiAgICBuYi1jYXJkLWJvZHkge1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRTFGNUZFO1xuICAgICAgICBjb2xvcjogd2hpdGU7XG4gICAgfVxuICAgIGNvbC14bC02LFxuICAgIG5iLWNhcmQtaGVhZGVyIHtcbiAgICAgICAgYmFja2dyb3VuZDogIzFEOEVDRTtcbiAgICAgICAgY29sb3I6IHdoaXRlO1xuICAgIH1cbn1cblxuOmhvc3QgL2RlZXAvIHRhYmxlIHRib2R5IHRyOm50aC1jaGlsZChldmVuKSB7XG4gICAgYmFja2dyb3VuZDogI2U5ZWFlYSFpbXBvcnRhbnQ7XG59XG5cbiA6aG9zdCAvZGVlcC8gdGFibGUgdGJvZHkgdHI6bnRoLWNoaWxkKG9kZCkge1xuICAgIGJhY2tncm91bmQ6IHdoaXRlIWltcG9ydGFudDtcbn1cblxuLy8gOmhvc3QgL2RlZXAvIHRhYmxlIHRib2R5IHRyOmFjdGl2ZSB7XG4vLyAgIGJhY2tncm91bmQ6ICMwYjYyZTQ4YyFpbXBvcnRhbnQ7XG4vLyAgIH1cbi8vICAgOmhvc3QgL2RlZXAvIHRhYmxlIHRib2R5IHRyOmZvY3VzIHtcbi8vICAgICBiYWNrZ3JvdW5kOiAjMGI2MmU0OGMhaW1wb3J0YW50O1xuLy8gICAgIH1cbi9kZWVwLyBuZzItc21hcnQtdGFibGUtcGFnZXIgbmF2IHVsIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/ui-features/customer/customers/customers.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/ui-features/customer/customers/customers.component.ts ***!
  \*****************************************************************************/
/*! exports provided: CustomersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomersComponent", function() { return CustomersComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var _services_customer_customer_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/customer/customer.service */ "./src/app/services/customer/customer.service.ts");
/* harmony import */ var _services_token_token_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/token/token.service */ "./src/app/services/token/token.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var CustomersComponent = /** @class */ (function () {
    function CustomersComponent(CustomerServ, token) {
        this.CustomerServ = CustomerServ;
        this.token = token;
        this.creatShow = false;
        this.tableShow = false;
        this.settings = {
            add: {
                addButtonContent: '<i class="nb-plus"></i>',
                createButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
            },
            edit: {
                editButtonContent: '<i class="nb-edit"></i>',
                saveButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
            },
            delete: {
                deleteButtonContent: '<i class="nb-trash"></i>',
                confirmDelete: true,
            },
            actions: {
                edit: false,
                add: false,
                delete: false,
            },
            columns: {
                companyName: {
                    title: 'Client Name',
                    type: 'string',
                },
                sites: {
                    title: 'No.of Sites',
                    type: 'number',
                },
                quotaions: {
                    title: 'No.of Quotations',
                    type: 'number',
                },
                agreements: {
                    title: 'No.of Agreements',
                    type: 'number',
                },
            },
        };
        this.source = new ng2_smart_table__WEBPACK_IMPORTED_MODULE_1__["LocalDataSource"]();
        this.customer = {};
        this.id = '';
        this.listOfCustomer = [];
        this.moduleCreate = true;
    }
    CustomersComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.token.userAccess !== undefined) {
            this.token.userAccess
                .subscribe(function (data) {
                data.forEach(function (ele) {
                    if (ele.name === 'Client') {
                        _this.moduleCreate = ele.create;
                    }
                });
            }, function (error) {
                console.log(error);
            });
        }
        this.CustomerServ.userCustomer
            .subscribe(function (data) {
            // console.log(data)
            _this.getCustomerInfo();
            if (!data.S_NO) {
                return;
            }
            _this.tableShow = true;
            // this.CustomerServ.userCustomer1.next(data)
        }, function (error) {
            console.log(error);
        });
        this.getCustomerInfo();
    };
    /**
     *  Create New Elevator Form Open
     */
    CustomersComponent.prototype.showFun = function () {
        this.creatShow = true;
        this.tableShow = false;
        this.CustomerServ.userCustomer.next({ creatShow: true });
    };
    /**
      * Get Customer Details
      */
    CustomersComponent.prototype.getCustomerInfo = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, error_1;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        _a = this;
                        return [4 /*yield*/, this.CustomerServ.getCustomer()];
                    case 1:
                        _a.listOfCustomer = _b.sent();
                        this.listOfCustomer.map(function (data, index) { return data.S_NO = index + 1; });
                        // sort by name
                        this.listOfCustomer.sort(function (a, b) {
                            var nameA = a.companyName.toUpperCase(); // ignore upper and lowercase
                            var nameB = b.companyName.toUpperCase(); // ignore upper and lowercase
                            if (nameA < nameB) {
                                return -1;
                            }
                            if (nameA > nameB) {
                                return 1;
                            }
                            return 0;
                        });
                        this.source.load(this.listOfCustomer);
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _b.sent();
                        console.error(error_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * User Selected Row
     */
    CustomersComponent.prototype.onUserRowSelect = function (event) {
        event.data.creatShow = false;
        this.CustomerServ.userCustomer.next(event.data);
        if (event) {
            this.creatShow = false;
            return this.tableShow = true;
        }
        this.creatShow = true;
        this.tableShow = false;
    };
    /**
     *  Delete Row Data
     */
    CustomersComponent.prototype.onDeleteConfirm = function (event) {
    };
    CustomersComponent.prototype.createData = function (data) {
        this.getCustomerInfo();
        this.listOfCustomer.push(data);
        this.listOfCustomer.reverse();
        this.listOfCustomer.map(function (data, index) { return data.S_NO = index + 1; });
        this.source.load(this.listOfCustomer);
    };
    CustomersComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-customers',
            template: __webpack_require__(/*! ./customers.component.html */ "./src/app/pages/ui-features/customer/customers/customers.component.html"),
            styles: [__webpack_require__(/*! ./customers.component.scss */ "./src/app/pages/ui-features/customer/customers/customers.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_customer_customer_service__WEBPACK_IMPORTED_MODULE_2__["CustomerService"],
            _services_token_token_service__WEBPACK_IMPORTED_MODULE_3__["TokenService"]])
    ], CustomersComponent);
    return CustomersComponent;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/customer/customers/detail-customers/customer-details/customer-details.component.html":
/*!************************************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/customer/customers/detail-customers/customer-details/customer-details.component.html ***!
  \************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nb-card>\n    <nb-card-header class=\"text-center\">\n        Client Details\n        <!-- <button class=\"btn btn-outline-primary \" shape=\"semi-round\" style=\"float:right\">Submit</button> -->\n    </nb-card-header>\n    <nb-card-body>\n        <form #customerFortm=\"ngForm\" name=\"form\" (ngSubmit)=\"customerFortm.form.valid && editCustomer(customerFortm,customerFortm.form.pristine)\" novalidate>\n            <div>\n\n                <div class=\"row\">\n\n                    <div class=\"col-6\">\n                        <div class=\"form-group input-group-sm\">\n                            <label for=\"Maintenance\"> Client Name</label>\n                            <input type=\"text\" class=\"form-control\" id=\"Maintenance\" [(ngModel)]=\"customer.companyName\" name=\"companyName\" placeholder=\"Client Name\" #companyName=\"ngModel\" [ngClass]=\"{ 'is-invalid': customerFortm.submitted && companyName.invalid }\" required>\n                            <div *ngIf=\"customerFortm.submitted && companyName.invalid\" class=\"invalid-feedback\">\n                                <div *ngIf=\"companyName.errors.required\">Client Name is required</div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"col-6\">\n                        <div class=\"form-group input-group-sm\">\n                            <label for=\"Maintenance\"> Contact Person Name</label>\n                            <input type=\"text\" class=\"form-control\" id=\"Maintenance\" [(ngModel)]=\"customer.personName\" name=\"personName\" placeholder=\"Contact Person Name\" #personName=\"ngModel\" [ngClass]=\"{ 'is-invalid': customerFortm.submitted && personName.invalid }\" required>\n                            <div *ngIf=\"customerFortm.submitted && personName.invalid\" class=\"invalid-feedback\">\n                                <div *ngIf=\"personName.errors.required\">Contact Person Name is required</div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"row\">\n                    <div class=\"col-6 \">\n                        <label for=\"Maintenance\">Conisignee Address</label>\n                        <textarea class=\"form-control\" type=\"text\" [(ngModel)]=\"customer.address\" name=\"address\" placeholder=\"Conisignee Address\" #address=\"ngModel\" [ngClass]=\"{ 'is-invalid': customerFortm.submitted && address.invalid }\" required></textarea>\n                        <div *ngIf=\"customerFortm.submitted && address.invalid\" class=\"invalid-feedback\">\n                            <div *ngIf=\"address.errors.required\">Conisignee Address is required</div>\n                        </div>\n                    </div>\n                    <div class=\"col-6 \">\n                        <label for=\"Maintenance\">Billing Address</label>\n                        <textarea class=\"form-control\" type=\"text\" [(ngModel)]=\"customer.deliveryAddress\" name=\"deliveryAddress\" placeholder=\"Billing Address\" #deliveryAddress=\"ngModel\" [ngClass]=\"{ 'is-invalid': customerFortm.submitted && deliveryAddress.invalid }\" required></textarea>\n                        <div *ngIf=\"customerFortm.submitted && deliveryAddress.invalid\" class=\"invalid-feedback\">\n                            <div *ngIf=\"deliveryAddress.errors.required\">Billing Address is required</div>\n                        </div>\n                    </div>\n                </div><br>\n                <div class=\"row\">\n                    <div class=\"col-3\">\n                        <div class=\"form-group input-group-sm\">\n                            <label for=\"Maintenance\">Contact No</label>\n                            <input type=\"number\" class=\"form-control\" id=\"Maintenance\" [(ngModel)]=\"customer.mobile\" name=\"mobile\" placeholder=\"Contact No\" #mobile=\"ngModel\" [ngClass]=\"{ 'is-invalid': customerFortm.submitted && mobile.invalid }\" required>\n                            <div *ngIf=\"customerFortm.submitted && mobile.invalid\" class=\"invalid-feedback\">\n                                <div *ngIf=\"mobile.errors.required\">Contact No is required</div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"col-3\">\n                        <div class=\"form-group input-group-sm\">\n                            <label for=\"Date\"> Email id</label>\n                            <input type=\"email\" class=\"form-control\" id=\"Date\" [(ngModel)]=\"customer.email\" name=\"email\" placeholder=\"Email id\">\n\n                        </div>\n                    </div>\n                    <div class=\"col-3\">\n                        <div class=\"form-group input-group-sm\">\n                            <label for=\"Delivary\">GST Number</label>\n                            <input type=\"text\" class=\"form-control\" id=\"Delivary\" [(ngModel)]=\"customer.gst\" name=\"gst\" placeholder=\"GST\">\n\n                        </div>\n                    </div>\n                    <div class=\"col-3\">\n                        <div class=\"form-group input-group-sm\">\n                            <label for=\"Delivary\">PAN Number</label>\n                            <input type=\"text\" class=\"form-control\" [(ngModel)]=\"customer.pan\" name=\"pan\" placeholder=\"PAN\" #pan=\"ngModel\">\n                        </div>\n                    </div>\n                </div>\n                <button *ngIf='moduleCreate' class=\"btn btn-outline-primary \" type=\"button\" shape=\"semi-round\" style=\"float:right;margin-top: 3px;\" (click)=\"onUserRowSelect({data: ''})\">Add Site Details</button>\n                <label>Enter the Site Details</label>\n\n                <div class=\"col-12\" style=\"overflow: overlay;\">\n                    <ng2-smart-table [ngClass]=\"hideCols\" [settings]=\"settings\" [source]=\"source\" (deleteConfirm)=\"onDeleteConfirm($event)\" (custom)=\"onUserRowSelect($event)\" style=\"overflow: scroll;\">\n                    </ng2-smart-table>\n                </div>\n\n                <br>\n                <div class=\"row foot\" *ngIf='moduleCreate || moduleEdit'>\n                    <div class=\"col-sm-12\">\n                        <button class=\"btn btn-outline-primary \" type=\"submit\" shape=\"semi-round\" style=\"float:right;margin-top: 3px;\">Save</button>\n                    </div>\n                </div>\n            </div>\n        </form>\n    </nb-card-body>\n</nb-card>\n\n\n<modal class=\"modalfirst\" #componentInsideModalClients>\n    <ng-template #modalHeader>\n        <h4>Enter the Site Details</h4>\n    </ng-template>\n    <ng-template #modalBody>\n        <form #customerForm=\"ngForm\" name=\"form\" (ngSubmit)=\"onCreateConfirm(customerForm) && customerForm.form.valid;\" novalidate>\n            <div class=\"container\">\n                <div class=\"row\">\n                    <div class=\"col-6\">\n                        <div class=\"form-control-group input-group-sm\">\n                            <label class=\"label\" for=\"input-email\">Site Name</label>\n                            <input type=\"text\" class=\"form-control\" [(ngModel)]=\"site.siteName\" name=\"siteName\" #siteName=\"ngModel\" [ngClass]=\"{ 'is-invalid': customerForm.submitted && siteName.invalid }\" placeholder=\"Site Name\" required>\n\n                            <div *ngIf=\"customerForm.submitted && siteName.invalid\" class=\"invalid-feedback\">\n                                <div *ngIf=\"siteName.errors.required\">Site Name is required</div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"col-6\">\n                        <div class=\"form-control-group input-group-sm\">\n                            <label class=\"label\" for=\"input-email\">Site Address</label>\n                            <textarea class=\"form-control\" [(ngModel)]=\"site.address\" name=\"address\" #address=\"ngModel\" placeholder=\"Site Address\" [ngClass]=\"{ 'is-invalid': customerForm.submitted && address.invalid }\" required></textarea>\n                            <div *ngIf=\"customerForm.submitted && address.invalid\" class=\"invalid-feedback\">\n                                <div *ngIf=\"address.errors.required\">Site Address is required</div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"row\">\n                    <div class=\"col\">\n                        <div class=\"form-control-group input-group-sm\">\n                            <label class=\"label\" for=\"input-email\"> Site Mobile Number</label>\n                            <input type=\"number\" class=\"form-control\" [(ngModel)]=\"site.mobile\" name=\"mobile\" pattern=\"[0-9]\" placeholder=\"Site Mobile Number\" #mobile=\"ngModel\" [ngClass]=\"{ 'is-invalid': customerForm.submitted && mobile.invalid }\" required>\n                            <div *ngIf=\"customerForm.submitted && mobile.invalid\" class=\"invalid-feedback\">\n                                <div *ngIf=\"mobile.errors.required\">Site Mobile Number is required</div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"col\">\n                        <div class=\"form-control-group input-group-sm\">\n                            <label class=\"label\" for=\"input-email\">Contact Person</label>\n                            <input type=\"text\" class=\"form-control\" [(ngModel)]=\"site.personName\" name=\"personName\" #personName=\"ngModel\" placeholder=\"Contact Person\" [ngClass]=\"{ 'is-invalid': customerForm.submitted && personName.invalid }\" required>\n                            <div *ngIf=\"customerForm.submitted && personName.invalid\" class=\"invalid-feedback\">\n                                <div *ngIf=\"personName.errors.required\">Contact Person Name is required</div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"col mt-2\">\n                        <div class=\"form-control-group input-group-sm\">\n                            <label class=\"label\" for=\"input-email\">Contact Person Number</label>\n                            <input type=\"number\" class=\"form-control\" [(ngModel)]=\"site.personMobile\" name=\"personMobile\" pattern=\"[0-9]\" placeholder=\"Contact Person Number\" #personMobile=\"ngModel\" [ngClass]=\"{ 'is-invalid': customerForm.submitted && personMobile.invalid }\" required>\n                            <div *ngIf=\"customerForm.submitted && personMobile.invalid\" class=\"invalid-feedback\">\n                                <div *ngIf=\"personMobile.errors.required\">Contact Person Number is required</div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"col mt-2\">\n                        <div class=\"form-control-group input-group-sm\">\n                            <label class=\"label\" for=\"input-email\">Email Id</label>\n                            <input type=\"email\" class=\"form-control\" [(ngModel)]=\"site.email\" name=\"email\" #email=\"ngModel\" placeholder=\"Email Id\">\n\n                        </div>\n                    </div>\n                </div>\n                <button class=\"btn btn-outline-primary \" type=\"submit\" shape=\"semi-round\" style=\"float:right;margin-top: 3px;\">Save</button>\n            </div>\n        </form>\n\n    </ng-template>\n</modal>\n\n\n<modal class=\"modalfirst\" #componentInsideModalClientSecond>\n    <ng-template #modalHeader>\n        <h4>Edit the Site Details</h4>\n    </ng-template>\n    <ng-template #modalBody>\n        <form #customerFormss=\"ngForm\" name=\"form\" (ngSubmit)=\"editSite(customerFormss, customerFormss.form.pristine) && customerFormss.form.valid \" novalidate>\n            <div class=\"container\">\n                <div class=\"row\">\n                    <div class=\"col-6\">\n                        <div class=\"form-control-group input-group-sm\">\n                            <label class=\"label\" for=\"input-email\">Site Name</label>\n                            <input type=\"text\" class=\"form-control\" [(ngModel)]=\"site.siteName\" name=\"siteName\" #siteName=\"ngModel\" [ngClass]=\"{ 'is-invalid': customerFormss.submitted && siteName.invalid }\" required>\n\n                            <div *ngIf=\"customerFormss.submitted && siteName.invalid\" class=\"invalid-feedback\">\n                                <div *ngIf=\"siteName.errors.required\">Site Name is required</div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"col-6\">\n                        <div class=\"form-control-group input-group-sm\">\n                            <label class=\"label\" for=\"input-email\">Site Address</label>\n                            <textarea class=\"form-control\" [(ngModel)]=\"site.address\" name=\"address\" #address=\"ngModel\" [ngClass]=\"{ 'is-invalid': customerFormss.submitted && address.invalid }\" required></textarea>\n                            <div *ngIf=\"customerFormss.submitted && address.invalid\" class=\"invalid-feedback\">\n                                <div *ngIf=\"address.errors.required\">Site Address is required</div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"row\">\n                    <div class=\"col\">\n                        <div class=\"form-control-group input-group-sm\">\n                            <label class=\"label\" for=\"input-email\"> Site Mobile Number</label>\n                            <input type=\"number\" class=\"form-control\" [(ngModel)]=\"site.mobile\" name=\"mobile\" pattern=\"[0-9]\" #mobile=\"ngModel\">\n\n                        </div>\n                    </div>\n                    <div class=\"col\">\n                        <div class=\"form-control-group input-group-sm\">\n                            <label class=\"label\" for=\"input-email\">Contact Person</label>\n                            <input type=\"text\" class=\"form-control\" [(ngModel)]=\"site.personName\" name=\"personName\" #personName=\"ngModel\" [ngClass]=\"{ 'is-invalid': customerFormss.submitted && personName.invalid }\" required>\n                            <div *ngIf=\"customerFormss.submitted && personName.invalid\" class=\"invalid-feedback\">\n                                <div *ngIf=\"personName.errors.required\">Contact Person Name is required</div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"col\">\n                        <div class=\"form-control-group input-group-sm\">\n                            <label class=\"label\" for=\"input-email\">Contact Person Number</label>\n                            <input type=\"number\" class=\"form-control\" [(ngModel)]=\"site.personMobile\" name=\"personMobile\" pattern=\"[0-9]\" #personMobile=\"ngModel\" [ngClass]=\"{ 'is-invalid': customerFormss.submitted && personMobile.invalid }\" required>\n                            <div *ngIf=\"customerFormss.submitted && personMobile.invalid\" class=\"invalid-feedback\">\n                                <div *ngIf=\"personMobile.errors.required\">Contact Person Number is required</div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"col\">\n                        <div class=\"form-control-group input-group-sm\">\n                            <label class=\"label\" for=\"input-email\">Email Id</label>\n                            <input type=\"email\" class=\"form-control\" [(ngModel)]=\"site.email\" name=\"email\" #email=\"ngModel\" [ngClass]=\"{ 'is-invalid': customerFormss.submitted && email.invalid }\" required>\n                            <div *ngIf=\"customerFormss.submitted && email.invalid\" class=\"invalid-feedback\">\n                                <div *ngIf=\"email.errors.required\">Email Id is required</div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n                <button class=\"btn btn-outline-primary \" type=\"submit\" shape=\"semi-round\" style=\"float:right;margin-top: 3px;\">Save</button>\n            </div>\n        </form>\n\n    </ng-template>\n</modal>"

/***/ }),

/***/ "./src/app/pages/ui-features/customer/customers/detail-customers/customer-details/customer-details.component.scss":
/*!************************************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/customer/customers/detail-customers/customer-details/customer-details.component.scss ***!
  \************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * This is a starting point where we declare the maps of themes and globally available functions/mixins\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * This mixin generates keyfames.\n * Because of all keyframes can't be scoped,\n * we need to puts unique name in each btn-pulse call.\n */\n/*\n\nAccording to the specification (https://www.w3.org/TR/css-scoping-1/#host-selector)\n:host and :host-context are pseudo-classes. So we assume they could be combined,\nlike other pseudo-classes, even same ones.\nFor example: ':nth-of-type(2n):nth-of-type(even)'.\n\nIdeal solution would be to prepend any selector with :host-context([dir=rtl]).\nThen nebular components will behave as an html element and respond to [dir] attribute on any level,\nso direction could be overridden on any component level.\n\nImplementation code:\n\n@mixin nb-rtl() {\n  // add # to scss interpolation statement.\n  // it works in comments and we can't use it here\n  @at-root {selector-append(':host-context([dir=rtl])', &)} {\n    @content;\n  }\n}\n\nAnd when we call it somewhere:\n\n:host {\n  .some-class {\n    @include nb-rtl() {\n      ...\n    }\n  }\n}\n:host-context(...) {\n  .some-class {\n    @include nb-rtl() {\n      ...\n    }\n  }\n}\n\nResult will look like:\n\n:host-context([dir=rtl]):host .some-class {\n  ...\n}\n:host-context([dir=rtl]):host-context(...) .some-class {\n  ...\n}\n\n*\n  Side note:\n  :host-context():host selector are valid. https://lists.w3.org/Archives/Public/www-style/2015Feb/0305.html\n\n  :host-context([dir=rtl]):host-context(...) should match any permutation,\n  so order is not important.\n*\n\n\nCurrently, there're two problems with this approach:\n\nFirst, is that we can't combine :host, :host-context. Angular bugs #14349, #19199.\nFor the moment of writing, the only possible way is:\n:host {\n  :host-context(...) {\n    ...\n  }\n}\nIt doesn't work for us because mixin could be called somewhere deeper, like:\n:host {\n  p {\n    @include nb-rtl() { ... }\n  }\n}\nWe are not able to go up to :host level to place content passed to mixin.\n\nThe second problem is that we only can be sure that we appending :host-context([dir=rtl]) to another\n:host/:host-context pseudo-class when called in theme files (*.theme.scss).\n  *\n    Side note:\n    Currently, nb-install-component uses another approach where :host prepended with the theme name\n    (https://github.com/angular/angular/blob/5b96078624b0a4760f2dbcf6fdf0bd62791be5bb/packages/compiler/src/shadow_css.ts#L441),\n    but it was made to be able to use current realization of rtl and it can be rewritten back to\n    :host-context($theme) once we will be able to use multiple shadow selectors.\n  *\nBut when it's called in *.component.scss we can't be sure, that selector starts with :host/:host-context,\nbecause angular allows omitting pseudo-classes if we don't need to style :host component itself.\nWe can break such selectors, by just appending :host-context([dir=rtl]) to them.\n  ***\n    Possible solution\n    check if we in theme by some theme variables and if so append, otherwise nest like\n    @at-root :host-context([dir=rtl]) {\n      // add # to scss interpolation statement.\n      // it works in comments and we can't use it here\n      {&} {\n        @content;\n      }\n    }\n    What if :host specified? Can we add space in :host-context(...) :host?\n    Or maybe add :host selector anyway? If multiple :host selectors are allowed\n  ***\n\n\nProblems with the current approach.\n\n1. Direction can be applied only on document level, because mixin prepends theme class,\nwhich placed on the body.\n2. *.component.scss styles should be in :host selector. Otherwise angular will add host\nattribute to [dir=rtl] attribute as well.\n\n\nGeneral problems.\n\nLtr is default document direction, but for proper work of nb-ltr (means ltr only),\n[dir=ltr] should be specified at least somewhere. ':not([dir=rtl]' not applicable here,\nbecause it's satisfy any parent, that don't have [dir=rtl] attribute.\nPrevious approach was to use single rtl mixin and reset ltr properties to initial value.\nBut sometimes it's hard to find, what the previous value should be. And such mixin call looks too verbose.\n*/\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * This mixin generates keyfames.\n * Because of all keyframes can't be scoped,\n * we need to puts unique name in each btn-pulse call.\n */\n/*\n\nAccording to the specification (https://www.w3.org/TR/css-scoping-1/#host-selector)\n:host and :host-context are pseudo-classes. So we assume they could be combined,\nlike other pseudo-classes, even same ones.\nFor example: ':nth-of-type(2n):nth-of-type(even)'.\n\nIdeal solution would be to prepend any selector with :host-context([dir=rtl]).\nThen nebular components will behave as an html element and respond to [dir] attribute on any level,\nso direction could be overridden on any component level.\n\nImplementation code:\n\n@mixin nb-rtl() {\n  // add # to scss interpolation statement.\n  // it works in comments and we can't use it here\n  @at-root {selector-append(':host-context([dir=rtl])', &)} {\n    @content;\n  }\n}\n\nAnd when we call it somewhere:\n\n:host {\n  .some-class {\n    @include nb-rtl() {\n      ...\n    }\n  }\n}\n:host-context(...) {\n  .some-class {\n    @include nb-rtl() {\n      ...\n    }\n  }\n}\n\nResult will look like:\n\n:host-context([dir=rtl]):host .some-class {\n  ...\n}\n:host-context([dir=rtl]):host-context(...) .some-class {\n  ...\n}\n\n*\n  Side note:\n  :host-context():host selector are valid. https://lists.w3.org/Archives/Public/www-style/2015Feb/0305.html\n\n  :host-context([dir=rtl]):host-context(...) should match any permutation,\n  so order is not important.\n*\n\n\nCurrently, there're two problems with this approach:\n\nFirst, is that we can't combine :host, :host-context. Angular bugs #14349, #19199.\nFor the moment of writing, the only possible way is:\n:host {\n  :host-context(...) {\n    ...\n  }\n}\nIt doesn't work for us because mixin could be called somewhere deeper, like:\n:host {\n  p {\n    @include nb-rtl() { ... }\n  }\n}\nWe are not able to go up to :host level to place content passed to mixin.\n\nThe second problem is that we only can be sure that we appending :host-context([dir=rtl]) to another\n:host/:host-context pseudo-class when called in theme files (*.theme.scss).\n  *\n    Side note:\n    Currently, nb-install-component uses another approach where :host prepended with the theme name\n    (https://github.com/angular/angular/blob/5b96078624b0a4760f2dbcf6fdf0bd62791be5bb/packages/compiler/src/shadow_css.ts#L441),\n    but it was made to be able to use current realization of rtl and it can be rewritten back to\n    :host-context($theme) once we will be able to use multiple shadow selectors.\n  *\nBut when it's called in *.component.scss we can't be sure, that selector starts with :host/:host-context,\nbecause angular allows omitting pseudo-classes if we don't need to style :host component itself.\nWe can break such selectors, by just appending :host-context([dir=rtl]) to them.\n  ***\n    Possible solution\n    check if we in theme by some theme variables and if so append, otherwise nest like\n    @at-root :host-context([dir=rtl]) {\n      // add # to scss interpolation statement.\n      // it works in comments and we can't use it here\n      {&} {\n        @content;\n      }\n    }\n    What if :host specified? Can we add space in :host-context(...) :host?\n    Or maybe add :host selector anyway? If multiple :host selectors are allowed\n  ***\n\n\nProblems with the current approach.\n\n1. Direction can be applied only on document level, because mixin prepends theme class,\nwhich placed on the body.\n2. *.component.scss styles should be in :host selector. Otherwise angular will add host\nattribute to [dir=rtl] attribute as well.\n\n\nGeneral problems.\n\nLtr is default document direction, but for proper work of nb-ltr (means ltr only),\n[dir=ltr] should be specified at least somewhere. ':not([dir=rtl]' not applicable here,\nbecause it's satisfy any parent, that don't have [dir=rtl] attribute.\nPrevious approach was to use single rtl mixin and reset ltr properties to initial value.\nBut sometimes it's hard to find, what the previous value should be. And such mixin call looks too verbose.\n*/\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * This mixin generates keyfames.\n * Because of all keyframes can't be scoped,\n * we need to puts unique name in each btn-pulse call.\n */\n/*\n\nAccording to the specification (https://www.w3.org/TR/css-scoping-1/#host-selector)\n:host and :host-context are pseudo-classes. So we assume they could be combined,\nlike other pseudo-classes, even same ones.\nFor example: ':nth-of-type(2n):nth-of-type(even)'.\n\nIdeal solution would be to prepend any selector with :host-context([dir=rtl]).\nThen nebular components will behave as an html element and respond to [dir] attribute on any level,\nso direction could be overridden on any component level.\n\nImplementation code:\n\n@mixin nb-rtl() {\n  // add # to scss interpolation statement.\n  // it works in comments and we can't use it here\n  @at-root {selector-append(':host-context([dir=rtl])', &)} {\n    @content;\n  }\n}\n\nAnd when we call it somewhere:\n\n:host {\n  .some-class {\n    @include nb-rtl() {\n      ...\n    }\n  }\n}\n:host-context(...) {\n  .some-class {\n    @include nb-rtl() {\n      ...\n    }\n  }\n}\n\nResult will look like:\n\n:host-context([dir=rtl]):host .some-class {\n  ...\n}\n:host-context([dir=rtl]):host-context(...) .some-class {\n  ...\n}\n\n*\n  Side note:\n  :host-context():host selector are valid. https://lists.w3.org/Archives/Public/www-style/2015Feb/0305.html\n\n  :host-context([dir=rtl]):host-context(...) should match any permutation,\n  so order is not important.\n*\n\n\nCurrently, there're two problems with this approach:\n\nFirst, is that we can't combine :host, :host-context. Angular bugs #14349, #19199.\nFor the moment of writing, the only possible way is:\n:host {\n  :host-context(...) {\n    ...\n  }\n}\nIt doesn't work for us because mixin could be called somewhere deeper, like:\n:host {\n  p {\n    @include nb-rtl() { ... }\n  }\n}\nWe are not able to go up to :host level to place content passed to mixin.\n\nThe second problem is that we only can be sure that we appending :host-context([dir=rtl]) to another\n:host/:host-context pseudo-class when called in theme files (*.theme.scss).\n  *\n    Side note:\n    Currently, nb-install-component uses another approach where :host prepended with the theme name\n    (https://github.com/angular/angular/blob/5b96078624b0a4760f2dbcf6fdf0bd62791be5bb/packages/compiler/src/shadow_css.ts#L441),\n    but it was made to be able to use current realization of rtl and it can be rewritten back to\n    :host-context($theme) once we will be able to use multiple shadow selectors.\n  *\nBut when it's called in *.component.scss we can't be sure, that selector starts with :host/:host-context,\nbecause angular allows omitting pseudo-classes if we don't need to style :host component itself.\nWe can break such selectors, by just appending :host-context([dir=rtl]) to them.\n  ***\n    Possible solution\n    check if we in theme by some theme variables and if so append, otherwise nest like\n    @at-root :host-context([dir=rtl]) {\n      // add # to scss interpolation statement.\n      // it works in comments and we can't use it here\n      {&} {\n        @content;\n      }\n    }\n    What if :host specified? Can we add space in :host-context(...) :host?\n    Or maybe add :host selector anyway? If multiple :host selectors are allowed\n  ***\n\n\nProblems with the current approach.\n\n1. Direction can be applied only on document level, because mixin prepends theme class,\nwhich placed on the body.\n2. *.component.scss styles should be in :host selector. Otherwise angular will add host\nattribute to [dir=rtl] attribute as well.\n\n\nGeneral problems.\n\nLtr is default document direction, but for proper work of nb-ltr (means ltr only),\n[dir=ltr] should be specified at least somewhere. ':not([dir=rtl]' not applicable here,\nbecause it's satisfy any parent, that don't have [dir=rtl] attribute.\nPrevious approach was to use single rtl mixin and reset ltr properties to initial value.\nBut sometimes it's hard to find, what the previous value should be. And such mixin call looks too verbose.\n*/\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * This mixin generates keyfames.\n * Because of all keyframes can't be scoped,\n * we need to puts unique name in each btn-pulse call.\n */\n/*\n\nAccording to the specification (https://www.w3.org/TR/css-scoping-1/#host-selector)\n:host and :host-context are pseudo-classes. So we assume they could be combined,\nlike other pseudo-classes, even same ones.\nFor example: ':nth-of-type(2n):nth-of-type(even)'.\n\nIdeal solution would be to prepend any selector with :host-context([dir=rtl]).\nThen nebular components will behave as an html element and respond to [dir] attribute on any level,\nso direction could be overridden on any component level.\n\nImplementation code:\n\n@mixin nb-rtl() {\n  // add # to scss interpolation statement.\n  // it works in comments and we can't use it here\n  @at-root {selector-append(':host-context([dir=rtl])', &)} {\n    @content;\n  }\n}\n\nAnd when we call it somewhere:\n\n:host {\n  .some-class {\n    @include nb-rtl() {\n      ...\n    }\n  }\n}\n:host-context(...) {\n  .some-class {\n    @include nb-rtl() {\n      ...\n    }\n  }\n}\n\nResult will look like:\n\n:host-context([dir=rtl]):host .some-class {\n  ...\n}\n:host-context([dir=rtl]):host-context(...) .some-class {\n  ...\n}\n\n*\n  Side note:\n  :host-context():host selector are valid. https://lists.w3.org/Archives/Public/www-style/2015Feb/0305.html\n\n  :host-context([dir=rtl]):host-context(...) should match any permutation,\n  so order is not important.\n*\n\n\nCurrently, there're two problems with this approach:\n\nFirst, is that we can't combine :host, :host-context. Angular bugs #14349, #19199.\nFor the moment of writing, the only possible way is:\n:host {\n  :host-context(...) {\n    ...\n  }\n}\nIt doesn't work for us because mixin could be called somewhere deeper, like:\n:host {\n  p {\n    @include nb-rtl() { ... }\n  }\n}\nWe are not able to go up to :host level to place content passed to mixin.\n\nThe second problem is that we only can be sure that we appending :host-context([dir=rtl]) to another\n:host/:host-context pseudo-class when called in theme files (*.theme.scss).\n  *\n    Side note:\n    Currently, nb-install-component uses another approach where :host prepended with the theme name\n    (https://github.com/angular/angular/blob/5b96078624b0a4760f2dbcf6fdf0bd62791be5bb/packages/compiler/src/shadow_css.ts#L441),\n    but it was made to be able to use current realization of rtl and it can be rewritten back to\n    :host-context($theme) once we will be able to use multiple shadow selectors.\n  *\nBut when it's called in *.component.scss we can't be sure, that selector starts with :host/:host-context,\nbecause angular allows omitting pseudo-classes if we don't need to style :host component itself.\nWe can break such selectors, by just appending :host-context([dir=rtl]) to them.\n  ***\n    Possible solution\n    check if we in theme by some theme variables and if so append, otherwise nest like\n    @at-root :host-context([dir=rtl]) {\n      // add # to scss interpolation statement.\n      // it works in comments and we can't use it here\n      {&} {\n        @content;\n      }\n    }\n    What if :host specified? Can we add space in :host-context(...) :host?\n    Or maybe add :host selector anyway? If multiple :host selectors are allowed\n  ***\n\n\nProblems with the current approach.\n\n1. Direction can be applied only on document level, because mixin prepends theme class,\nwhich placed on the body.\n2. *.component.scss styles should be in :host selector. Otherwise angular will add host\nattribute to [dir=rtl] attribute as well.\n\n\nGeneral problems.\n\nLtr is default document direction, but for proper work of nb-ltr (means ltr only),\n[dir=ltr] should be specified at least somewhere. ':not([dir=rtl]' not applicable here,\nbecause it's satisfy any parent, that don't have [dir=rtl] attribute.\nPrevious approach was to use single rtl mixin and reset ltr properties to initial value.\nBut sometimes it's hard to find, what the previous value should be. And such mixin call looks too verbose.\n*/\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * This mixin generates keyfames.\n * Because of all keyframes can't be scoped,\n * we need to puts unique name in each btn-pulse call.\n */\n/*\n\nAccording to the specification (https://www.w3.org/TR/css-scoping-1/#host-selector)\n:host and :host-context are pseudo-classes. So we assume they could be combined,\nlike other pseudo-classes, even same ones.\nFor example: ':nth-of-type(2n):nth-of-type(even)'.\n\nIdeal solution would be to prepend any selector with :host-context([dir=rtl]).\nThen nebular components will behave as an html element and respond to [dir] attribute on any level,\nso direction could be overridden on any component level.\n\nImplementation code:\n\n@mixin nb-rtl() {\n  // add # to scss interpolation statement.\n  // it works in comments and we can't use it here\n  @at-root {selector-append(':host-context([dir=rtl])', &)} {\n    @content;\n  }\n}\n\nAnd when we call it somewhere:\n\n:host {\n  .some-class {\n    @include nb-rtl() {\n      ...\n    }\n  }\n}\n:host-context(...) {\n  .some-class {\n    @include nb-rtl() {\n      ...\n    }\n  }\n}\n\nResult will look like:\n\n:host-context([dir=rtl]):host .some-class {\n  ...\n}\n:host-context([dir=rtl]):host-context(...) .some-class {\n  ...\n}\n\n*\n  Side note:\n  :host-context():host selector are valid. https://lists.w3.org/Archives/Public/www-style/2015Feb/0305.html\n\n  :host-context([dir=rtl]):host-context(...) should match any permutation,\n  so order is not important.\n*\n\n\nCurrently, there're two problems with this approach:\n\nFirst, is that we can't combine :host, :host-context. Angular bugs #14349, #19199.\nFor the moment of writing, the only possible way is:\n:host {\n  :host-context(...) {\n    ...\n  }\n}\nIt doesn't work for us because mixin could be called somewhere deeper, like:\n:host {\n  p {\n    @include nb-rtl() { ... }\n  }\n}\nWe are not able to go up to :host level to place content passed to mixin.\n\nThe second problem is that we only can be sure that we appending :host-context([dir=rtl]) to another\n:host/:host-context pseudo-class when called in theme files (*.theme.scss).\n  *\n    Side note:\n    Currently, nb-install-component uses another approach where :host prepended with the theme name\n    (https://github.com/angular/angular/blob/5b96078624b0a4760f2dbcf6fdf0bd62791be5bb/packages/compiler/src/shadow_css.ts#L441),\n    but it was made to be able to use current realization of rtl and it can be rewritten back to\n    :host-context($theme) once we will be able to use multiple shadow selectors.\n  *\nBut when it's called in *.component.scss we can't be sure, that selector starts with :host/:host-context,\nbecause angular allows omitting pseudo-classes if we don't need to style :host component itself.\nWe can break such selectors, by just appending :host-context([dir=rtl]) to them.\n  ***\n    Possible solution\n    check if we in theme by some theme variables and if so append, otherwise nest like\n    @at-root :host-context([dir=rtl]) {\n      // add # to scss interpolation statement.\n      // it works in comments and we can't use it here\n      {&} {\n        @content;\n      }\n    }\n    What if :host specified? Can we add space in :host-context(...) :host?\n    Or maybe add :host selector anyway? If multiple :host selectors are allowed\n  ***\n\n\nProblems with the current approach.\n\n1. Direction can be applied only on document level, because mixin prepends theme class,\nwhich placed on the body.\n2. *.component.scss styles should be in :host selector. Otherwise angular will add host\nattribute to [dir=rtl] attribute as well.\n\n\nGeneral problems.\n\nLtr is default document direction, but for proper work of nb-ltr (means ltr only),\n[dir=ltr] should be specified at least somewhere. ':not([dir=rtl]' not applicable here,\nbecause it's satisfy any parent, that don't have [dir=rtl] attribute.\nPrevious approach was to use single rtl mixin and reset ltr properties to initial value.\nBut sometimes it's hard to find, what the previous value should be. And such mixin call looks too verbose.\n*/\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/**\n * This mixin generates keyfames.\n * Because of all keyframes can't be scoped,\n * we need to puts unique name in each btn-pulse call.\n */\n/*\n\nAccording to the specification (https://www.w3.org/TR/css-scoping-1/#host-selector)\n:host and :host-context are pseudo-classes. So we assume they could be combined,\nlike other pseudo-classes, even same ones.\nFor example: ':nth-of-type(2n):nth-of-type(even)'.\n\nIdeal solution would be to prepend any selector with :host-context([dir=rtl]).\nThen nebular components will behave as an html element and respond to [dir] attribute on any level,\nso direction could be overridden on any component level.\n\nImplementation code:\n\n@mixin nb-rtl() {\n  // add # to scss interpolation statement.\n  // it works in comments and we can't use it here\n  @at-root {selector-append(':host-context([dir=rtl])', &)} {\n    @content;\n  }\n}\n\nAnd when we call it somewhere:\n\n:host {\n  .some-class {\n    @include nb-rtl() {\n      ...\n    }\n  }\n}\n:host-context(...) {\n  .some-class {\n    @include nb-rtl() {\n      ...\n    }\n  }\n}\n\nResult will look like:\n\n:host-context([dir=rtl]):host .some-class {\n  ...\n}\n:host-context([dir=rtl]):host-context(...) .some-class {\n  ...\n}\n\n*\n  Side note:\n  :host-context():host selector are valid. https://lists.w3.org/Archives/Public/www-style/2015Feb/0305.html\n\n  :host-context([dir=rtl]):host-context(...) should match any permutation,\n  so order is not important.\n*\n\n\nCurrently, there're two problems with this approach:\n\nFirst, is that we can't combine :host, :host-context. Angular bugs #14349, #19199.\nFor the moment of writing, the only possible way is:\n:host {\n  :host-context(...) {\n    ...\n  }\n}\nIt doesn't work for us because mixin could be called somewhere deeper, like:\n:host {\n  p {\n    @include nb-rtl() { ... }\n  }\n}\nWe are not able to go up to :host level to place content passed to mixin.\n\nThe second problem is that we only can be sure that we appending :host-context([dir=rtl]) to another\n:host/:host-context pseudo-class when called in theme files (*.theme.scss).\n  *\n    Side note:\n    Currently, nb-install-component uses another approach where :host prepended with the theme name\n    (https://github.com/angular/angular/blob/5b96078624b0a4760f2dbcf6fdf0bd62791be5bb/packages/compiler/src/shadow_css.ts#L441),\n    but it was made to be able to use current realization of rtl and it can be rewritten back to\n    :host-context($theme) once we will be able to use multiple shadow selectors.\n  *\nBut when it's called in *.component.scss we can't be sure, that selector starts with :host/:host-context,\nbecause angular allows omitting pseudo-classes if we don't need to style :host component itself.\nWe can break such selectors, by just appending :host-context([dir=rtl]) to them.\n  ***\n    Possible solution\n    check if we in theme by some theme variables and if so append, otherwise nest like\n    @at-root :host-context([dir=rtl]) {\n      // add # to scss interpolation statement.\n      // it works in comments and we can't use it here\n      {&} {\n        @content;\n      }\n    }\n    What if :host specified? Can we add space in :host-context(...) :host?\n    Or maybe add :host selector anyway? If multiple :host selectors are allowed\n  ***\n\n\nProblems with the current approach.\n\n1. Direction can be applied only on document level, because mixin prepends theme class,\nwhich placed on the body.\n2. *.component.scss styles should be in :host selector. Otherwise angular will add host\nattribute to [dir=rtl] attribute as well.\n\n\nGeneral problems.\n\nLtr is default document direction, but for proper work of nb-ltr (means ltr only),\n[dir=ltr] should be specified at least somewhere. ':not([dir=rtl]' not applicable here,\nbecause it's satisfy any parent, that don't have [dir=rtl] attribute.\nPrevious approach was to use single rtl mixin and reset ltr properties to initial value.\nBut sometimes it's hard to find, what the previous value should be. And such mixin call looks too verbose.\n*/\n/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n/*\n      :host can be prefixed\n      https://github.com/angular/angular/blob/8d0ee34939f14c07876d222c25b405ed458a34d3/packages/compiler/src/shadow_css.ts#L441\n\n      We have to use :host insted of :host-context($theme), to be able to prefix theme class\n      with something defined inside of @content, by prefixing &.\n      For example this scss code:\n        .nb-theme-default {\n          .some-selector & {\n            ...\n          }\n        }\n      Will result in next css:\n        .some-selector .nb-theme-default {\n          ...\n        }\n\n      It doesn't work with :host-context because angular splitting it in two selectors and removes\n      prefix in one of the selectors.\n    */\n.nb-theme-default :host .hi {\n  border: 2px solid; }\n.nb-theme-default :host table {\n  width: -webkit-fill-available; }\n.nb-theme-default :host a {\n  cursor: pointer; }\n.nb-theme-default :host nb-card {\n  border: none; }\n.nb-theme-default :host nb-card-body {\n  background-color: #E1F5FE; }\n.nb-theme-default :host nb-card-header {\n  background: #E1F5FE; }\n.nb-theme-default :host /deep/ .modal-dialog {\n  max-width: 45%; }\n.nb-theme-default :host /deep/ ng2-smart-table i {\n  color: #000; }\n.nb-theme-default :host /deep/ ng2-smart-table .ng2-smart-title {\n  color: white; }\n.nb-theme-default :host /deep/ ng2-smart-table .ng2-smart-pagination-nav .ng2-smart-pagination {\n  background-color: #000; }\n.nb-theme-default :host /deep/ ng2-smart-table i {\n  color: #000; }\n.nb-theme-default :host /deep/ modal .modal-dialog {\n  max-width: 1000px !important; }\n.nb-theme-default :host /deep/ ng2-st-tbody-edit-delete {\n  display: -webkit-inline-box !important;\n  height: 0px !important;\n  float: left;\n  margin-left: 36px;\n  margin-top: -15px !important;\n  /* margin: auto; */ }\n.nb-theme-default :host /deep/ ng2-st-tbody-custom a.ng2-smart-action.ng2-smart-action-custom-custom {\n  width: 50px;\n  text-align: center;\n  font-size: 0.1em; }\n.nb-theme-default :host /deep/ ng2-st-tbody-custom a.ng2-smart-action.ng2-smart-action-custom-custom img {\n  width: 57% !important;\n  margin-left: 34px; }\n.nb-theme-default :host /deep/ ng2-st-tbody-custom a.ng2-smart-action.ng2-smart-action-custom-custom:hover {\n  color: #5dcfe3; }\n.nb-theme-default :host /deep/ ng2-st-tbody-edit-delete i {\n  position: relative !important;\n  top: -14px;\n  margin-left: 40px; }\n.nb-theme-default :host button {\n  float: right; }\n/*\n      :host can be prefixed\n      https://github.com/angular/angular/blob/8d0ee34939f14c07876d222c25b405ed458a34d3/packages/compiler/src/shadow_css.ts#L441\n\n      We have to use :host insted of :host-context($theme), to be able to prefix theme class\n      with something defined inside of @content, by prefixing &.\n      For example this scss code:\n        .nb-theme-default {\n          .some-selector & {\n            ...\n          }\n        }\n      Will result in next css:\n        .some-selector .nb-theme-default {\n          ...\n        }\n\n      It doesn't work with :host-context because angular splitting it in two selectors and removes\n      prefix in one of the selectors.\n    */\n.nb-theme-cosmic :host .hi {\n  border: 2px solid; }\n.nb-theme-cosmic :host table {\n  width: -webkit-fill-available; }\n.nb-theme-cosmic :host a {\n  cursor: pointer; }\n.nb-theme-cosmic :host nb-card {\n  border: none; }\n.nb-theme-cosmic :host nb-card-body {\n  background-color: #E1F5FE; }\n.nb-theme-cosmic :host nb-card-header {\n  background: #E1F5FE; }\n.nb-theme-cosmic :host /deep/ .modal-dialog {\n  max-width: 45%; }\n.nb-theme-cosmic :host /deep/ ng2-smart-table i {\n  color: #000; }\n.nb-theme-cosmic :host /deep/ ng2-smart-table .ng2-smart-title {\n  color: white; }\n.nb-theme-cosmic :host /deep/ ng2-smart-table .ng2-smart-pagination-nav .ng2-smart-pagination {\n  background-color: #000; }\n.nb-theme-cosmic :host /deep/ ng2-smart-table i {\n  color: #000; }\n.nb-theme-cosmic :host /deep/ modal .modal-dialog {\n  max-width: 1000px !important; }\n.nb-theme-cosmic :host /deep/ ng2-st-tbody-edit-delete {\n  display: -webkit-inline-box !important;\n  height: 0px !important;\n  float: left;\n  margin-left: 36px;\n  margin-top: -15px !important;\n  /* margin: auto; */ }\n.nb-theme-cosmic :host /deep/ ng2-st-tbody-custom a.ng2-smart-action.ng2-smart-action-custom-custom {\n  width: 50px;\n  text-align: center;\n  font-size: 0.1em; }\n.nb-theme-cosmic :host /deep/ ng2-st-tbody-custom a.ng2-smart-action.ng2-smart-action-custom-custom img {\n  width: 57% !important;\n  margin-left: 34px; }\n.nb-theme-cosmic :host /deep/ ng2-st-tbody-custom a.ng2-smart-action.ng2-smart-action-custom-custom:hover {\n  color: #5dcfe3; }\n.nb-theme-cosmic :host /deep/ ng2-st-tbody-edit-delete i {\n  position: relative !important;\n  top: -14px;\n  margin-left: 40px; }\n.nb-theme-cosmic :host button {\n  float: right; }\n/*\n      :host can be prefixed\n      https://github.com/angular/angular/blob/8d0ee34939f14c07876d222c25b405ed458a34d3/packages/compiler/src/shadow_css.ts#L441\n\n      We have to use :host insted of :host-context($theme), to be able to prefix theme class\n      with something defined inside of @content, by prefixing &.\n      For example this scss code:\n        .nb-theme-default {\n          .some-selector & {\n            ...\n          }\n        }\n      Will result in next css:\n        .some-selector .nb-theme-default {\n          ...\n        }\n\n      It doesn't work with :host-context because angular splitting it in two selectors and removes\n      prefix in one of the selectors.\n    */\n.nb-theme-corporate :host .hi {\n  border: 2px solid; }\n.nb-theme-corporate :host table {\n  width: -webkit-fill-available; }\n.nb-theme-corporate :host a {\n  cursor: pointer; }\n.nb-theme-corporate :host nb-card {\n  border: none; }\n.nb-theme-corporate :host nb-card-body {\n  background-color: #E1F5FE; }\n.nb-theme-corporate :host nb-card-header {\n  background: #E1F5FE; }\n.nb-theme-corporate :host /deep/ .modal-dialog {\n  max-width: 45%; }\n.nb-theme-corporate :host /deep/ ng2-smart-table i {\n  color: #000; }\n.nb-theme-corporate :host /deep/ ng2-smart-table .ng2-smart-title {\n  color: white; }\n.nb-theme-corporate :host /deep/ ng2-smart-table .ng2-smart-pagination-nav .ng2-smart-pagination {\n  background-color: #000; }\n.nb-theme-corporate :host /deep/ ng2-smart-table i {\n  color: #000; }\n.nb-theme-corporate :host /deep/ modal .modal-dialog {\n  max-width: 1000px !important; }\n.nb-theme-corporate :host /deep/ ng2-st-tbody-edit-delete {\n  display: -webkit-inline-box !important;\n  height: 0px !important;\n  float: left;\n  margin-left: 36px;\n  margin-top: -15px !important;\n  /* margin: auto; */ }\n.nb-theme-corporate :host /deep/ ng2-st-tbody-custom a.ng2-smart-action.ng2-smart-action-custom-custom {\n  width: 50px;\n  text-align: center;\n  font-size: 0.1em; }\n.nb-theme-corporate :host /deep/ ng2-st-tbody-custom a.ng2-smart-action.ng2-smart-action-custom-custom img {\n  width: 57% !important;\n  margin-left: 34px; }\n.nb-theme-corporate :host /deep/ ng2-st-tbody-custom a.ng2-smart-action.ng2-smart-action-custom-custom:hover {\n  color: #5dcfe3; }\n.nb-theme-corporate :host /deep/ ng2-st-tbody-edit-delete i {\n  position: relative !important;\n  top: -14px;\n  margin-left: 40px; }\n.nb-theme-corporate :host button {\n  float: right; }\ninput[type=number]::-webkit-inner-spin-button,\ninput[type=number]::-webkit-outer-spin-button {\n  -webkit-appearance: none;\n  margin: 0; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3ByYWthc2gvVmlwaW4vaXJvbmJpcmQvMjBGRUIyMDIwL2lyb25iaXJkLWNsaWVudHMvbm9kZV9tb2R1bGVzL0BuZWJ1bGFyL3RoZW1lL3N0eWxlcy9fdGhlbWluZy5zY3NzIiwic3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9jdXN0b21lci9jdXN0b21lcnMvZGV0YWlsLWN1c3RvbWVycy9jdXN0b21lci1kZXRhaWxzL2N1c3RvbWVyLWRldGFpbHMuY29tcG9uZW50LnNjc3MiLCIvaG9tZS9wcmFrYXNoL1ZpcGluL2lyb25iaXJkLzIwRkVCMjAyMC9pcm9uYmlyZC1jbGllbnRzL25vZGVfbW9kdWxlcy9AbmVidWxhci90aGVtZS9zdHlsZXMvY29yZS9fbWl4aW5zLnNjc3MiLCIvaG9tZS9wcmFrYXNoL1ZpcGluL2lyb25iaXJkLzIwRkVCMjAyMC9pcm9uYmlyZC1jbGllbnRzL25vZGVfbW9kdWxlcy9AbmVidWxhci90aGVtZS9zdHlsZXMvY29yZS9fZnVuY3Rpb25zLnNjc3MiLCIvaG9tZS9wcmFrYXNoL1ZpcGluL2lyb25iaXJkLzIwRkVCMjAyMC9pcm9uYmlyZC1jbGllbnRzL25vZGVfbW9kdWxlcy9AbmVidWxhci90aGVtZS9zdHlsZXMvdGhlbWVzL19kZWZhdWx0LnNjc3MiLCIvaG9tZS9wcmFrYXNoL1ZpcGluL2lyb25iaXJkLzIwRkVCMjAyMC9pcm9uYmlyZC1jbGllbnRzL25vZGVfbW9kdWxlcy9AbmVidWxhci90aGVtZS9zdHlsZXMvdGhlbWVzL19jb3NtaWMuc2NzcyIsIi9ob21lL3ByYWthc2gvVmlwaW4vaXJvbmJpcmQvMjBGRUIyMDIwL2lyb25iaXJkLWNsaWVudHMvbm9kZV9tb2R1bGVzL0BuZWJ1bGFyL3RoZW1lL3N0eWxlcy90aGVtZXMvX2NvcnBvcmF0ZS5zY3NzIiwiL2hvbWUvcHJha2FzaC9WaXBpbi9pcm9uYmlyZC8yMEZFQjIwMjAvaXJvbmJpcmQtY2xpZW50cy9ub2RlX21vZHVsZXMvQG5lYnVsYXIvdGhlbWUvc3R5bGVzL2dsb2JhbC9ib290c3RyYXAvX2JyZWFrcG9pbnRzLnNjc3MiLCIvaG9tZS9wcmFrYXNoL1ZpcGluL2lyb25iaXJkLzIwRkVCMjAyMC9pcm9uYmlyZC1jbGllbnRzL3NyYy9hcHAvcGFnZXMvdWktZmVhdHVyZXMvY3VzdG9tZXIvY3VzdG9tZXJzL2RldGFpbC1jdXN0b21lcnMvY3VzdG9tZXItZGV0YWlscy9jdXN0b21lci1kZXRhaWxzLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOzs7O0VDSUU7QURHRjs7RUNBRTtBQ1BGOzs7O0VEWUU7QUM4SkY7Ozs7RUR6SkU7QUNtTEY7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Q0QvREM7QUVySUQ7Ozs7RUYwSUU7QUcxSUY7Ozs7RUgrSUU7QUUvSUY7Ozs7RUZvSkU7QUNwSkY7Ozs7RUR5SkU7QUNpQkY7Ozs7RURaRTtBQ3NDRjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztDRDhFQztBSWxSRDs7OztFSnVSRTtBRXZSRjs7OztFRjRSRTtBQzVSRjs7OztFRGlTRTtBQ3ZIRjs7OztFRDRIRTtBQ2xHRjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztDRHNOQztBRzFaRDs7OztFSCtaRTtBRS9aRjs7OztFRm9hRTtBQ3BhRjs7OztFRHlhRTtBQy9QRjs7OztFRG9RRTtBQzFPRjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztDRDhWQztBS2xpQkQ7Ozs7RUx1aUJFO0FFdmlCRjs7OztFRjRpQkU7QUM1aUJGOzs7O0VEaWpCRTtBQ3ZZRjs7OztFRDRZRTtBQ2xYRjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztDRHNlQztBRzFxQkQ7Ozs7RUgrcUJFO0FFL3FCRjs7OztFRm9yQkU7QUNwckJGOzs7O0VEeXJCRTtBQy9nQkY7Ozs7RURvaEJFO0FDMWZGOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0NEOG1CQztBTWx6QkQ7Ozs7RU51ekJFO0FEcnNCRTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztLQ3l0QkM7QU90MEJMO0VBQ0ksaUJBQWlCLEVBQUE7QUFFckI7RUFDSSw2QkFBNkIsRUFBQTtBQUVqQztFQUNJLGVBQWUsRUFBQTtBQUtuQjtFQUNJLFlBQVksRUFBQTtBQUVoQjtFQUNJLHlCQUF3QixFQUFBO0FBRzFCO0VBQ0UsbUJBQW1CLEVBQUE7QUFHckI7RUFDRSxjQUFlLEVBQUE7QUFJbkI7RUFDSSxXQUFVLEVBQUE7QUFHWjtFQUNFLFlBQVksRUFBQTtBQUdkO0VBQ0Msc0JBQXFCLEVBQUE7QUFFdEI7RUFDRSxXQUFXLEVBQUE7QUFHYjtFQUNFLDRCQUE0QixFQUFBO0FBRTlCO0VBQ0Usc0NBQXNDO0VBQ3RDLHNCQUFzQjtFQUN0QixXQUFXO0VBQ1gsaUJBQWlCO0VBQ2pCLDRCQUE0QjtFQUM1QixrQkFBQSxFQUFtQjtBQUduQjtFQUVDLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsZ0JBQWdCLEVBQUE7QUFFbEI7RUFDQyxxQkFBcUI7RUFDckIsaUJBQWlCLEVBQUE7QUFFbkI7RUFDRyxjQUFjLEVBQUE7QUFHZDtFQUNDLDZCQUE2QjtFQUM3QixVQUFXO0VBQ1gsaUJBQWlCLEVBQUE7QUFFcEI7RUFDQyxZQUFZLEVBQUE7QVJrQ1o7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7S0M2eUJDO0FPMTVCTDtFQUNJLGlCQUFpQixFQUFBO0FBRXJCO0VBQ0ksNkJBQTZCLEVBQUE7QUFFakM7RUFDSSxlQUFlLEVBQUE7QUFLbkI7RUFDSSxZQUFZLEVBQUE7QUFFaEI7RUFDSSx5QkFBd0IsRUFBQTtBQUcxQjtFQUNFLG1CQUFtQixFQUFBO0FBR3JCO0VBQ0UsY0FBZSxFQUFBO0FBSW5CO0VBQ0ksV0FBVSxFQUFBO0FBR1o7RUFDRSxZQUFZLEVBQUE7QUFHZDtFQUNDLHNCQUFxQixFQUFBO0FBRXRCO0VBQ0UsV0FBVyxFQUFBO0FBR2I7RUFDRSw0QkFBNEIsRUFBQTtBQUU5QjtFQUNFLHNDQUFzQztFQUN0QyxzQkFBc0I7RUFDdEIsV0FBVztFQUNYLGlCQUFpQjtFQUNqQiw0QkFBNEI7RUFDNUIsa0JBQUEsRUFBbUI7QUFHbkI7RUFFQyxXQUFXO0VBQ1gsa0JBQWtCO0VBQ2xCLGdCQUFnQixFQUFBO0FBRWxCO0VBQ0MscUJBQXFCO0VBQ3JCLGlCQUFpQixFQUFBO0FBRW5CO0VBQ0csY0FBYyxFQUFBO0FBR2Q7RUFDQyw2QkFBNkI7RUFDN0IsVUFBVztFQUNYLGlCQUFpQixFQUFBO0FBRXBCO0VBQ0MsWUFBWSxFQUFBO0FSa0NaOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0tDaTRCQztBTzkrQkw7RUFDSSxpQkFBaUIsRUFBQTtBQUVyQjtFQUNJLDZCQUE2QixFQUFBO0FBRWpDO0VBQ0ksZUFBZSxFQUFBO0FBS25CO0VBQ0ksWUFBWSxFQUFBO0FBRWhCO0VBQ0kseUJBQXdCLEVBQUE7QUFHMUI7RUFDRSxtQkFBbUIsRUFBQTtBQUdyQjtFQUNFLGNBQWUsRUFBQTtBQUluQjtFQUNJLFdBQVUsRUFBQTtBQUdaO0VBQ0UsWUFBWSxFQUFBO0FBR2Q7RUFDQyxzQkFBcUIsRUFBQTtBQUV0QjtFQUNFLFdBQVcsRUFBQTtBQUdiO0VBQ0UsNEJBQTRCLEVBQUE7QUFFOUI7RUFDRSxzQ0FBc0M7RUFDdEMsc0JBQXNCO0VBQ3RCLFdBQVc7RUFDWCxpQkFBaUI7RUFDakIsNEJBQTRCO0VBQzVCLGtCQUFBLEVBQW1CO0FBR25CO0VBRUMsV0FBVztFQUNYLGtCQUFrQjtFQUNsQixnQkFBZ0IsRUFBQTtBQUVsQjtFQUNDLHFCQUFxQjtFQUNyQixpQkFBaUIsRUFBQTtBQUVuQjtFQUNHLGNBQWMsRUFBQTtBQUdkO0VBQ0MsNkJBQTZCO0VBQzdCLFVBQVc7RUFDWCxpQkFBaUIsRUFBQTtBQUVwQjtFQUNDLFlBQVksRUFBQTtBQUloQjs7RUFFRSx3QkFBd0I7RUFDeEIsU0FBUyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvdWktZmVhdHVyZXMvY3VzdG9tZXIvY3VzdG9tZXJzL2RldGFpbC1jdXN0b21lcnMvY3VzdG9tZXItZGV0YWlscy9jdXN0b21lci1kZXRhaWxzLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBAbGljZW5zZVxuICogQ29weXJpZ2h0IEFrdmVvLiBBbGwgUmlnaHRzIFJlc2VydmVkLlxuICogTGljZW5zZWQgdW5kZXIgdGhlIE1JVCBMaWNlbnNlLiBTZWUgTGljZW5zZS50eHQgaW4gdGhlIHByb2plY3Qgcm9vdCBmb3IgbGljZW5zZSBpbmZvcm1hdGlvbi5cbiAqL1xuXG5cbi8qKlxuICogVGhpcyBpcyBhIHN0YXJ0aW5nIHBvaW50IHdoZXJlIHdlIGRlY2xhcmUgdGhlIG1hcHMgb2YgdGhlbWVzIGFuZCBnbG9iYWxseSBhdmFpbGFibGUgZnVuY3Rpb25zL21peGluc1xuICovXG5cbkBpbXBvcnQgJ2NvcmUvbWl4aW5zJztcbkBpbXBvcnQgJ2NvcmUvZnVuY3Rpb25zJztcblxuJG5iLWVuYWJsZWQtdGhlbWVzOiAoKSAhZ2xvYmFsO1xuJG5iLWVuYWJsZS1jc3MtdmFyaWFibGVzOiBmYWxzZSAhZ2xvYmFsO1xuXG4kbmItdGhlbWVzOiAoKSAhZ2xvYmFsO1xuJG5iLXRoZW1lcy1ub24tcHJvY2Vzc2VkOiAoKSAhZ2xvYmFsO1xuJG5iLXRoZW1lcy1leHBvcnQ6ICgpICFnbG9iYWw7XG5cbkBmdW5jdGlvbiBuYi10aGVtZSgka2V5KSB7XG4gIEByZXR1cm4gbWFwLWdldCgkdGhlbWUsICRrZXkpO1xufVxuXG5AZnVuY3Rpb24gbmItZ2V0LXZhbHVlKCR0aGVtZSwgJGtleSwgJHZhbHVlKSB7XG4gIEBpZiAodHlwZS1vZigkdmFsdWUpID09ICdzdHJpbmcnKSB7XG4gICAgJHRtcDogbWFwLWdldCgkdGhlbWUsICR2YWx1ZSk7XG5cbiAgICBAaWYgKCR0bXAgIT0gbnVsbCkge1xuICAgICAgQHJldHVybiBuYi1nZXQtdmFsdWUoJHRoZW1lLCAkdmFsdWUsICR0bXApO1xuICAgIH1cbiAgfVxuXG4gIEByZXR1cm4gbWFwLWdldCgkdGhlbWUsICRrZXkpO1xufVxuXG5AZnVuY3Rpb24gY29udmVydC10by1jc3MtdmFyaWFibGVzKCR2YXJpYWJsZXMpIHtcbiAgJHJlc3VsdDogKCk7XG4gIEBlYWNoICR2YXIsICR2YWx1ZSBpbiAkdmFyaWFibGVzIHtcbiAgICAkcmVzdWx0OiBtYXAtc2V0KCRyZXN1bHQsICR2YXIsICctLXZhcigjeyR2YXJ9KScpO1xuICB9XG5cbiAgQGRlYnVnICRyZXN1bHQ7XG4gIEByZXR1cm4gJHJlc3VsdDtcbn1cblxuQGZ1bmN0aW9uIHNldC1nbG9iYWwtdGhlbWUtdmFycygkdGhlbWUsICR0aGVtZS1uYW1lKSB7XG4gICR0aGVtZTogJHRoZW1lICFnbG9iYWw7XG4gICR0aGVtZS1uYW1lOiAkdGhlbWUtbmFtZSAhZ2xvYmFsO1xuICBAaWYgKCRuYi1lbmFibGUtY3NzLXZhcmlhYmxlcykge1xuICAgICR0aGVtZTogY29udmVydC10by1jc3MtdmFyaWFibGVzKCR0aGVtZSkgIWdsb2JhbDtcbiAgfVxuICBAcmV0dXJuICR0aGVtZTtcbn1cblxuQGZ1bmN0aW9uIG5iLXJlZ2lzdGVyLXRoZW1lKCR0aGVtZSwgJG5hbWUsICRkZWZhdWx0OiBudWxsKSB7XG5cbiAgJHRoZW1lLWRhdGE6ICgpO1xuXG5cbiAgQGlmICgkZGVmYXVsdCAhPSBudWxsKSB7XG5cbiAgICAkdGhlbWU6IG1hcC1tZXJnZShtYXAtZ2V0KCRuYi10aGVtZXMtbm9uLXByb2Nlc3NlZCwgJGRlZmF1bHQpLCAkdGhlbWUpO1xuICAgICRuYi10aGVtZXMtbm9uLXByb2Nlc3NlZDogbWFwLXNldCgkbmItdGhlbWVzLW5vbi1wcm9jZXNzZWQsICRuYW1lLCAkdGhlbWUpICFnbG9iYWw7XG5cbiAgICAkdGhlbWUtZGF0YTogbWFwLXNldCgkdGhlbWUtZGF0YSwgZGF0YSwgJHRoZW1lKTtcbiAgICAkbmItdGhlbWVzLWV4cG9ydDogbWFwLXNldCgkbmItdGhlbWVzLWV4cG9ydCwgJG5hbWUsIG1hcC1zZXQoJHRoZW1lLWRhdGEsIHBhcmVudCwgJGRlZmF1bHQpKSAhZ2xvYmFsO1xuXG4gIH0gQGVsc2Uge1xuICAgICRuYi10aGVtZXMtbm9uLXByb2Nlc3NlZDogbWFwLXNldCgkbmItdGhlbWVzLW5vbi1wcm9jZXNzZWQsICRuYW1lLCAkdGhlbWUpICFnbG9iYWw7XG5cbiAgICAkdGhlbWUtZGF0YTogbWFwLXNldCgkdGhlbWUtZGF0YSwgZGF0YSwgJHRoZW1lKTtcbiAgICAkbmItdGhlbWVzLWV4cG9ydDogbWFwLXNldCgkbmItdGhlbWVzLWV4cG9ydCwgJG5hbWUsIG1hcC1zZXQoJHRoZW1lLWRhdGEsIHBhcmVudCwgbnVsbCkpICFnbG9iYWw7XG4gIH1cblxuICAkdGhlbWUtcGFyc2VkOiAoKTtcbiAgQGVhY2ggJGtleSwgJHZhbHVlIGluICR0aGVtZSB7XG4gICAgJHRoZW1lLXBhcnNlZDogbWFwLXNldCgkdGhlbWUtcGFyc2VkLCAka2V5LCBuYi1nZXQtdmFsdWUoJHRoZW1lLCAka2V5LCAkdmFsdWUpKTtcbiAgfVxuXG4gIC8vIGVuYWJsZSByaWdodCBhd2F5IHdoZW4gaW5zdGFsbGVkXG4gICR0aGVtZS1wYXJzZWQ6IHNldC1nbG9iYWwtdGhlbWUtdmFycygkdGhlbWUtcGFyc2VkLCAkbmFtZSk7XG4gIEByZXR1cm4gbWFwLXNldCgkbmItdGhlbWVzLCAkbmFtZSwgJHRoZW1lLXBhcnNlZCk7XG59XG5cbkBmdW5jdGlvbiBnZXQtZW5hYmxlZC10aGVtZXMoKSB7XG4gICR0aGVtZXMtdG8taW5zdGFsbDogKCk7XG5cbiAgQGlmIChsZW5ndGgoJG5iLWVuYWJsZWQtdGhlbWVzKSA+IDApIHtcbiAgICBAZWFjaCAkdGhlbWUtbmFtZSBpbiAkbmItZW5hYmxlZC10aGVtZXMge1xuICAgICAgJHRoZW1lcy10by1pbnN0YWxsOiBtYXAtc2V0KCR0aGVtZXMtdG8taW5zdGFsbCwgJHRoZW1lLW5hbWUsIG1hcC1nZXQoJG5iLXRoZW1lcywgJHRoZW1lLW5hbWUpKTtcbiAgICB9XG4gIH0gQGVsc2Uge1xuICAgICR0aGVtZXMtdG8taW5zdGFsbDogJG5iLXRoZW1lcztcbiAgfVxuXG4gIEByZXR1cm4gJHRoZW1lcy10by1pbnN0YWxsO1xufVxuXG5AbWl4aW4gaW5zdGFsbC1jc3MtdmFyaWFibGVzKCR0aGVtZS1uYW1lLCAkdmFyaWFibGVzKSB7XG4gIC5uYi10aGVtZS0jeyR0aGVtZS1uYW1lfSB7XG4gICAgQGVhY2ggJHZhciwgJHZhbHVlIGluICR2YXJpYWJsZXMge1xuICAgICAgLS0jeyR2YXJ9OiAkdmFsdWU7XG4gICAgfVxuICB9XG59XG5cbi8vIFRPRE86IHdlIGhpZGUgOmhvc3QgaW5zaWRlIG9mIGl0IHdoaWNoIGlzIG5vdCBvYnZpb3VzXG5AbWl4aW4gbmItaW5zdGFsbC1jb21wb25lbnQoKSB7XG5cbiAgJHRoZW1lcy10by1pbnN0YWxsOiBnZXQtZW5hYmxlZC10aGVtZXMoKTtcblxuICBAZWFjaCAkdGhlbWUtbmFtZSwgJHRoZW1lIGluICR0aGVtZXMtdG8taW5zdGFsbCB7XG4gICAgLypcbiAgICAgIDpob3N0IGNhbiBiZSBwcmVmaXhlZFxuICAgICAgaHR0cHM6Ly9naXRodWIuY29tL2FuZ3VsYXIvYW5ndWxhci9ibG9iLzhkMGVlMzQ5MzlmMTRjMDc4NzZkMjIyYzI1YjQwNWVkNDU4YTM0ZDMvcGFja2FnZXMvY29tcGlsZXIvc3JjL3NoYWRvd19jc3MudHMjTDQ0MVxuXG4gICAgICBXZSBoYXZlIHRvIHVzZSA6aG9zdCBpbnN0ZWQgb2YgOmhvc3QtY29udGV4dCgkdGhlbWUpLCB0byBiZSBhYmxlIHRvIHByZWZpeCB0aGVtZSBjbGFzc1xuICAgICAgd2l0aCBzb21ldGhpbmcgZGVmaW5lZCBpbnNpZGUgb2YgQGNvbnRlbnQsIGJ5IHByZWZpeGluZyAmLlxuICAgICAgRm9yIGV4YW1wbGUgdGhpcyBzY3NzIGNvZGU6XG4gICAgICAgIC5uYi10aGVtZS1kZWZhdWx0IHtcbiAgICAgICAgICAuc29tZS1zZWxlY3RvciAmIHtcbiAgICAgICAgICAgIC4uLlxuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgV2lsbCByZXN1bHQgaW4gbmV4dCBjc3M6XG4gICAgICAgIC5zb21lLXNlbGVjdG9yIC5uYi10aGVtZS1kZWZhdWx0IHtcbiAgICAgICAgICAuLi5cbiAgICAgICAgfVxuXG4gICAgICBJdCBkb2Vzbid0IHdvcmsgd2l0aCA6aG9zdC1jb250ZXh0IGJlY2F1c2UgYW5ndWxhciBzcGxpdHRpbmcgaXQgaW4gdHdvIHNlbGVjdG9ycyBhbmQgcmVtb3Zlc1xuICAgICAgcHJlZml4IGluIG9uZSBvZiB0aGUgc2VsZWN0b3JzLlxuICAgICovXG4gICAgLm5iLXRoZW1lLSN7JHRoZW1lLW5hbWV9IDpob3N0IHtcbiAgICAgICR0aGVtZTogc2V0LWdsb2JhbC10aGVtZS12YXJzKCR0aGVtZSwgJHRoZW1lLW5hbWUpO1xuICAgICAgQGNvbnRlbnQ7XG4gICAgfVxuICB9XG59XG5cbkBtaXhpbiBuYi1mb3ItdGhlbWUoJG5hbWUpIHtcbiAgQGlmICgkdGhlbWUtbmFtZSA9PSAkbmFtZSkge1xuICAgIEBjb250ZW50O1xuICB9XG59XG5cbkBtaXhpbiBuYi1leGNlcHQtdGhlbWUoJG5hbWUpIHtcbiAgQGlmICgkdGhlbWUtbmFtZSAhPSAkbmFtZSkge1xuICAgIEBjb250ZW50O1xuICB9XG59XG5cbi8vIFRPRE86IGFub3RoZXIgbWl4aW5nIGZvciB0aGUgYWxtb3N0IHNhbWUgdGhpbmdcbkBtaXhpbiBuYi1pbnN0YWxsLXJvb3QtY29tcG9uZW50KCkge1xuICBAd2FybiAnYG5iLWluc3RhbGwtcm9vdC1jb21wb25lbnRgIGlzIGRlcHJpY2F0ZWQsIHJlcGxhY2Ugd2l0aCBgbmItaW5zdGFsbC1jb21wb25lbnRgLCBhcyBgYm9keWAgaXMgcm9vdCBlbGVtZW50IG5vdyc7XG5cbiAgQGluY2x1ZGUgbmItaW5zdGFsbC1jb21wb25lbnQoKSB7XG4gICAgQGNvbnRlbnQ7XG4gIH1cbn1cblxuQG1peGluIG5iLWluc3RhbGwtZ2xvYmFsKCkge1xuICAkdGhlbWVzLXRvLWluc3RhbGw6IGdldC1lbmFibGVkLXRoZW1lcygpO1xuXG4gIEBlYWNoICR0aGVtZS1uYW1lLCAkdGhlbWUgaW4gJHRoZW1lcy10by1pbnN0YWxsIHtcbiAgICAubmItdGhlbWUtI3skdGhlbWUtbmFtZX0ge1xuICAgICAgJHRoZW1lOiBzZXQtZ2xvYmFsLXRoZW1lLXZhcnMoJHRoZW1lLCAkdGhlbWUtbmFtZSk7XG4gICAgICBAY29udGVudDtcbiAgICB9XG4gIH1cbn1cbiIsIi8qKlxuICogQGxpY2Vuc2VcbiAqIENvcHlyaWdodCBBa3Zlby4gQWxsIFJpZ2h0cyBSZXNlcnZlZC5cbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgTGljZW5zZS4gU2VlIExpY2Vuc2UudHh0IGluIHRoZSBwcm9qZWN0IHJvb3QgZm9yIGxpY2Vuc2UgaW5mb3JtYXRpb24uXG4gKi9cbi8qKlxuICogVGhpcyBpcyBhIHN0YXJ0aW5nIHBvaW50IHdoZXJlIHdlIGRlY2xhcmUgdGhlIG1hcHMgb2YgdGhlbWVzIGFuZCBnbG9iYWxseSBhdmFpbGFibGUgZnVuY3Rpb25zL21peGluc1xuICovXG4vKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG4vKipcbiAqIFRoaXMgbWl4aW4gZ2VuZXJhdGVzIGtleWZhbWVzLlxuICogQmVjYXVzZSBvZiBhbGwga2V5ZnJhbWVzIGNhbid0IGJlIHNjb3BlZCxcbiAqIHdlIG5lZWQgdG8gcHV0cyB1bmlxdWUgbmFtZSBpbiBlYWNoIGJ0bi1wdWxzZSBjYWxsLlxuICovXG4vKlxuXG5BY2NvcmRpbmcgdG8gdGhlIHNwZWNpZmljYXRpb24gKGh0dHBzOi8vd3d3LnczLm9yZy9UUi9jc3Mtc2NvcGluZy0xLyNob3N0LXNlbGVjdG9yKVxuOmhvc3QgYW5kIDpob3N0LWNvbnRleHQgYXJlIHBzZXVkby1jbGFzc2VzLiBTbyB3ZSBhc3N1bWUgdGhleSBjb3VsZCBiZSBjb21iaW5lZCxcbmxpa2Ugb3RoZXIgcHNldWRvLWNsYXNzZXMsIGV2ZW4gc2FtZSBvbmVzLlxuRm9yIGV4YW1wbGU6ICc6bnRoLW9mLXR5cGUoMm4pOm50aC1vZi10eXBlKGV2ZW4pJy5cblxuSWRlYWwgc29sdXRpb24gd291bGQgYmUgdG8gcHJlcGVuZCBhbnkgc2VsZWN0b3Igd2l0aCA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkuXG5UaGVuIG5lYnVsYXIgY29tcG9uZW50cyB3aWxsIGJlaGF2ZSBhcyBhbiBodG1sIGVsZW1lbnQgYW5kIHJlc3BvbmQgdG8gW2Rpcl0gYXR0cmlidXRlIG9uIGFueSBsZXZlbCxcbnNvIGRpcmVjdGlvbiBjb3VsZCBiZSBvdmVycmlkZGVuIG9uIGFueSBjb21wb25lbnQgbGV2ZWwuXG5cbkltcGxlbWVudGF0aW9uIGNvZGU6XG5cbkBtaXhpbiBuYi1ydGwoKSB7XG4gIC8vIGFkZCAjIHRvIHNjc3MgaW50ZXJwb2xhdGlvbiBzdGF0ZW1lbnQuXG4gIC8vIGl0IHdvcmtzIGluIGNvbW1lbnRzIGFuZCB3ZSBjYW4ndCB1c2UgaXQgaGVyZVxuICBAYXQtcm9vdCB7c2VsZWN0b3ItYXBwZW5kKCc6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSknLCAmKX0ge1xuICAgIEBjb250ZW50O1xuICB9XG59XG5cbkFuZCB3aGVuIHdlIGNhbGwgaXQgc29tZXdoZXJlOlxuXG46aG9zdCB7XG4gIC5zb21lLWNsYXNzIHtcbiAgICBAaW5jbHVkZSBuYi1ydGwoKSB7XG4gICAgICAuLi5cbiAgICB9XG4gIH1cbn1cbjpob3N0LWNvbnRleHQoLi4uKSB7XG4gIC5zb21lLWNsYXNzIHtcbiAgICBAaW5jbHVkZSBuYi1ydGwoKSB7XG4gICAgICAuLi5cbiAgICB9XG4gIH1cbn1cblxuUmVzdWx0IHdpbGwgbG9vayBsaWtlOlxuXG46aG9zdC1jb250ZXh0KFtkaXI9cnRsXSk6aG9zdCAuc29tZS1jbGFzcyB7XG4gIC4uLlxufVxuOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pOmhvc3QtY29udGV4dCguLi4pIC5zb21lLWNsYXNzIHtcbiAgLi4uXG59XG5cbipcbiAgU2lkZSBub3RlOlxuICA6aG9zdC1jb250ZXh0KCk6aG9zdCBzZWxlY3RvciBhcmUgdmFsaWQuIGh0dHBzOi8vbGlzdHMudzMub3JnL0FyY2hpdmVzL1B1YmxpYy93d3ctc3R5bGUvMjAxNUZlYi8wMzA1Lmh0bWxcblxuICA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSk6aG9zdC1jb250ZXh0KC4uLikgc2hvdWxkIG1hdGNoIGFueSBwZXJtdXRhdGlvbixcbiAgc28gb3JkZXIgaXMgbm90IGltcG9ydGFudC5cbipcblxuXG5DdXJyZW50bHksIHRoZXJlJ3JlIHR3byBwcm9ibGVtcyB3aXRoIHRoaXMgYXBwcm9hY2g6XG5cbkZpcnN0LCBpcyB0aGF0IHdlIGNhbid0IGNvbWJpbmUgOmhvc3QsIDpob3N0LWNvbnRleHQuIEFuZ3VsYXIgYnVncyAjMTQzNDksICMxOTE5OS5cbkZvciB0aGUgbW9tZW50IG9mIHdyaXRpbmcsIHRoZSBvbmx5IHBvc3NpYmxlIHdheSBpczpcbjpob3N0IHtcbiAgOmhvc3QtY29udGV4dCguLi4pIHtcbiAgICAuLi5cbiAgfVxufVxuSXQgZG9lc24ndCB3b3JrIGZvciB1cyBiZWNhdXNlIG1peGluIGNvdWxkIGJlIGNhbGxlZCBzb21ld2hlcmUgZGVlcGVyLCBsaWtlOlxuOmhvc3Qge1xuICBwIHtcbiAgICBAaW5jbHVkZSBuYi1ydGwoKSB7IC4uLiB9XG4gIH1cbn1cbldlIGFyZSBub3QgYWJsZSB0byBnbyB1cCB0byA6aG9zdCBsZXZlbCB0byBwbGFjZSBjb250ZW50IHBhc3NlZCB0byBtaXhpbi5cblxuVGhlIHNlY29uZCBwcm9ibGVtIGlzIHRoYXQgd2Ugb25seSBjYW4gYmUgc3VyZSB0aGF0IHdlIGFwcGVuZGluZyA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkgdG8gYW5vdGhlclxuOmhvc3QvOmhvc3QtY29udGV4dCBwc2V1ZG8tY2xhc3Mgd2hlbiBjYWxsZWQgaW4gdGhlbWUgZmlsZXMgKCoudGhlbWUuc2NzcykuXG4gICpcbiAgICBTaWRlIG5vdGU6XG4gICAgQ3VycmVudGx5LCBuYi1pbnN0YWxsLWNvbXBvbmVudCB1c2VzIGFub3RoZXIgYXBwcm9hY2ggd2hlcmUgOmhvc3QgcHJlcGVuZGVkIHdpdGggdGhlIHRoZW1lIG5hbWVcbiAgICAoaHR0cHM6Ly9naXRodWIuY29tL2FuZ3VsYXIvYW5ndWxhci9ibG9iLzViOTYwNzg2MjRiMGE0NzYwZjJkYmNmNmZkZjBiZDYyNzkxYmU1YmIvcGFja2FnZXMvY29tcGlsZXIvc3JjL3NoYWRvd19jc3MudHMjTDQ0MSksXG4gICAgYnV0IGl0IHdhcyBtYWRlIHRvIGJlIGFibGUgdG8gdXNlIGN1cnJlbnQgcmVhbGl6YXRpb24gb2YgcnRsIGFuZCBpdCBjYW4gYmUgcmV3cml0dGVuIGJhY2sgdG9cbiAgICA6aG9zdC1jb250ZXh0KCR0aGVtZSkgb25jZSB3ZSB3aWxsIGJlIGFibGUgdG8gdXNlIG11bHRpcGxlIHNoYWRvdyBzZWxlY3RvcnMuXG4gICpcbkJ1dCB3aGVuIGl0J3MgY2FsbGVkIGluICouY29tcG9uZW50LnNjc3Mgd2UgY2FuJ3QgYmUgc3VyZSwgdGhhdCBzZWxlY3RvciBzdGFydHMgd2l0aCA6aG9zdC86aG9zdC1jb250ZXh0LFxuYmVjYXVzZSBhbmd1bGFyIGFsbG93cyBvbWl0dGluZyBwc2V1ZG8tY2xhc3NlcyBpZiB3ZSBkb24ndCBuZWVkIHRvIHN0eWxlIDpob3N0IGNvbXBvbmVudCBpdHNlbGYuXG5XZSBjYW4gYnJlYWsgc3VjaCBzZWxlY3RvcnMsIGJ5IGp1c3QgYXBwZW5kaW5nIDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKSB0byB0aGVtLlxuICAqKipcbiAgICBQb3NzaWJsZSBzb2x1dGlvblxuICAgIGNoZWNrIGlmIHdlIGluIHRoZW1lIGJ5IHNvbWUgdGhlbWUgdmFyaWFibGVzIGFuZCBpZiBzbyBhcHBlbmQsIG90aGVyd2lzZSBuZXN0IGxpa2VcbiAgICBAYXQtcm9vdCA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkge1xuICAgICAgLy8gYWRkICMgdG8gc2NzcyBpbnRlcnBvbGF0aW9uIHN0YXRlbWVudC5cbiAgICAgIC8vIGl0IHdvcmtzIGluIGNvbW1lbnRzIGFuZCB3ZSBjYW4ndCB1c2UgaXQgaGVyZVxuICAgICAgeyZ9IHtcbiAgICAgICAgQGNvbnRlbnQ7XG4gICAgICB9XG4gICAgfVxuICAgIFdoYXQgaWYgOmhvc3Qgc3BlY2lmaWVkPyBDYW4gd2UgYWRkIHNwYWNlIGluIDpob3N0LWNvbnRleHQoLi4uKSA6aG9zdD9cbiAgICBPciBtYXliZSBhZGQgOmhvc3Qgc2VsZWN0b3IgYW55d2F5PyBJZiBtdWx0aXBsZSA6aG9zdCBzZWxlY3RvcnMgYXJlIGFsbG93ZWRcbiAgKioqXG5cblxuUHJvYmxlbXMgd2l0aCB0aGUgY3VycmVudCBhcHByb2FjaC5cblxuMS4gRGlyZWN0aW9uIGNhbiBiZSBhcHBsaWVkIG9ubHkgb24gZG9jdW1lbnQgbGV2ZWwsIGJlY2F1c2UgbWl4aW4gcHJlcGVuZHMgdGhlbWUgY2xhc3MsXG53aGljaCBwbGFjZWQgb24gdGhlIGJvZHkuXG4yLiAqLmNvbXBvbmVudC5zY3NzIHN0eWxlcyBzaG91bGQgYmUgaW4gOmhvc3Qgc2VsZWN0b3IuIE90aGVyd2lzZSBhbmd1bGFyIHdpbGwgYWRkIGhvc3RcbmF0dHJpYnV0ZSB0byBbZGlyPXJ0bF0gYXR0cmlidXRlIGFzIHdlbGwuXG5cblxuR2VuZXJhbCBwcm9ibGVtcy5cblxuTHRyIGlzIGRlZmF1bHQgZG9jdW1lbnQgZGlyZWN0aW9uLCBidXQgZm9yIHByb3BlciB3b3JrIG9mIG5iLWx0ciAobWVhbnMgbHRyIG9ubHkpLFxuW2Rpcj1sdHJdIHNob3VsZCBiZSBzcGVjaWZpZWQgYXQgbGVhc3Qgc29tZXdoZXJlLiAnOm5vdChbZGlyPXJ0bF0nIG5vdCBhcHBsaWNhYmxlIGhlcmUsXG5iZWNhdXNlIGl0J3Mgc2F0aXNmeSBhbnkgcGFyZW50LCB0aGF0IGRvbid0IGhhdmUgW2Rpcj1ydGxdIGF0dHJpYnV0ZS5cblByZXZpb3VzIGFwcHJvYWNoIHdhcyB0byB1c2Ugc2luZ2xlIHJ0bCBtaXhpbiBhbmQgcmVzZXQgbHRyIHByb3BlcnRpZXMgdG8gaW5pdGlhbCB2YWx1ZS5cbkJ1dCBzb21ldGltZXMgaXQncyBoYXJkIHRvIGZpbmQsIHdoYXQgdGhlIHByZXZpb3VzIHZhbHVlIHNob3VsZCBiZS4gQW5kIHN1Y2ggbWl4aW4gY2FsbCBsb29rcyB0b28gdmVyYm9zZS5cbiovXG4vKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG4vKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG4vKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG4vKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG4vKipcbiAqIFRoaXMgbWl4aW4gZ2VuZXJhdGVzIGtleWZhbWVzLlxuICogQmVjYXVzZSBvZiBhbGwga2V5ZnJhbWVzIGNhbid0IGJlIHNjb3BlZCxcbiAqIHdlIG5lZWQgdG8gcHV0cyB1bmlxdWUgbmFtZSBpbiBlYWNoIGJ0bi1wdWxzZSBjYWxsLlxuICovXG4vKlxuXG5BY2NvcmRpbmcgdG8gdGhlIHNwZWNpZmljYXRpb24gKGh0dHBzOi8vd3d3LnczLm9yZy9UUi9jc3Mtc2NvcGluZy0xLyNob3N0LXNlbGVjdG9yKVxuOmhvc3QgYW5kIDpob3N0LWNvbnRleHQgYXJlIHBzZXVkby1jbGFzc2VzLiBTbyB3ZSBhc3N1bWUgdGhleSBjb3VsZCBiZSBjb21iaW5lZCxcbmxpa2Ugb3RoZXIgcHNldWRvLWNsYXNzZXMsIGV2ZW4gc2FtZSBvbmVzLlxuRm9yIGV4YW1wbGU6ICc6bnRoLW9mLXR5cGUoMm4pOm50aC1vZi10eXBlKGV2ZW4pJy5cblxuSWRlYWwgc29sdXRpb24gd291bGQgYmUgdG8gcHJlcGVuZCBhbnkgc2VsZWN0b3Igd2l0aCA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkuXG5UaGVuIG5lYnVsYXIgY29tcG9uZW50cyB3aWxsIGJlaGF2ZSBhcyBhbiBodG1sIGVsZW1lbnQgYW5kIHJlc3BvbmQgdG8gW2Rpcl0gYXR0cmlidXRlIG9uIGFueSBsZXZlbCxcbnNvIGRpcmVjdGlvbiBjb3VsZCBiZSBvdmVycmlkZGVuIG9uIGFueSBjb21wb25lbnQgbGV2ZWwuXG5cbkltcGxlbWVudGF0aW9uIGNvZGU6XG5cbkBtaXhpbiBuYi1ydGwoKSB7XG4gIC8vIGFkZCAjIHRvIHNjc3MgaW50ZXJwb2xhdGlvbiBzdGF0ZW1lbnQuXG4gIC8vIGl0IHdvcmtzIGluIGNvbW1lbnRzIGFuZCB3ZSBjYW4ndCB1c2UgaXQgaGVyZVxuICBAYXQtcm9vdCB7c2VsZWN0b3ItYXBwZW5kKCc6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSknLCAmKX0ge1xuICAgIEBjb250ZW50O1xuICB9XG59XG5cbkFuZCB3aGVuIHdlIGNhbGwgaXQgc29tZXdoZXJlOlxuXG46aG9zdCB7XG4gIC5zb21lLWNsYXNzIHtcbiAgICBAaW5jbHVkZSBuYi1ydGwoKSB7XG4gICAgICAuLi5cbiAgICB9XG4gIH1cbn1cbjpob3N0LWNvbnRleHQoLi4uKSB7XG4gIC5zb21lLWNsYXNzIHtcbiAgICBAaW5jbHVkZSBuYi1ydGwoKSB7XG4gICAgICAuLi5cbiAgICB9XG4gIH1cbn1cblxuUmVzdWx0IHdpbGwgbG9vayBsaWtlOlxuXG46aG9zdC1jb250ZXh0KFtkaXI9cnRsXSk6aG9zdCAuc29tZS1jbGFzcyB7XG4gIC4uLlxufVxuOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pOmhvc3QtY29udGV4dCguLi4pIC5zb21lLWNsYXNzIHtcbiAgLi4uXG59XG5cbipcbiAgU2lkZSBub3RlOlxuICA6aG9zdC1jb250ZXh0KCk6aG9zdCBzZWxlY3RvciBhcmUgdmFsaWQuIGh0dHBzOi8vbGlzdHMudzMub3JnL0FyY2hpdmVzL1B1YmxpYy93d3ctc3R5bGUvMjAxNUZlYi8wMzA1Lmh0bWxcblxuICA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSk6aG9zdC1jb250ZXh0KC4uLikgc2hvdWxkIG1hdGNoIGFueSBwZXJtdXRhdGlvbixcbiAgc28gb3JkZXIgaXMgbm90IGltcG9ydGFudC5cbipcblxuXG5DdXJyZW50bHksIHRoZXJlJ3JlIHR3byBwcm9ibGVtcyB3aXRoIHRoaXMgYXBwcm9hY2g6XG5cbkZpcnN0LCBpcyB0aGF0IHdlIGNhbid0IGNvbWJpbmUgOmhvc3QsIDpob3N0LWNvbnRleHQuIEFuZ3VsYXIgYnVncyAjMTQzNDksICMxOTE5OS5cbkZvciB0aGUgbW9tZW50IG9mIHdyaXRpbmcsIHRoZSBvbmx5IHBvc3NpYmxlIHdheSBpczpcbjpob3N0IHtcbiAgOmhvc3QtY29udGV4dCguLi4pIHtcbiAgICAuLi5cbiAgfVxufVxuSXQgZG9lc24ndCB3b3JrIGZvciB1cyBiZWNhdXNlIG1peGluIGNvdWxkIGJlIGNhbGxlZCBzb21ld2hlcmUgZGVlcGVyLCBsaWtlOlxuOmhvc3Qge1xuICBwIHtcbiAgICBAaW5jbHVkZSBuYi1ydGwoKSB7IC4uLiB9XG4gIH1cbn1cbldlIGFyZSBub3QgYWJsZSB0byBnbyB1cCB0byA6aG9zdCBsZXZlbCB0byBwbGFjZSBjb250ZW50IHBhc3NlZCB0byBtaXhpbi5cblxuVGhlIHNlY29uZCBwcm9ibGVtIGlzIHRoYXQgd2Ugb25seSBjYW4gYmUgc3VyZSB0aGF0IHdlIGFwcGVuZGluZyA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkgdG8gYW5vdGhlclxuOmhvc3QvOmhvc3QtY29udGV4dCBwc2V1ZG8tY2xhc3Mgd2hlbiBjYWxsZWQgaW4gdGhlbWUgZmlsZXMgKCoudGhlbWUuc2NzcykuXG4gICpcbiAgICBTaWRlIG5vdGU6XG4gICAgQ3VycmVudGx5LCBuYi1pbnN0YWxsLWNvbXBvbmVudCB1c2VzIGFub3RoZXIgYXBwcm9hY2ggd2hlcmUgOmhvc3QgcHJlcGVuZGVkIHdpdGggdGhlIHRoZW1lIG5hbWVcbiAgICAoaHR0cHM6Ly9naXRodWIuY29tL2FuZ3VsYXIvYW5ndWxhci9ibG9iLzViOTYwNzg2MjRiMGE0NzYwZjJkYmNmNmZkZjBiZDYyNzkxYmU1YmIvcGFja2FnZXMvY29tcGlsZXIvc3JjL3NoYWRvd19jc3MudHMjTDQ0MSksXG4gICAgYnV0IGl0IHdhcyBtYWRlIHRvIGJlIGFibGUgdG8gdXNlIGN1cnJlbnQgcmVhbGl6YXRpb24gb2YgcnRsIGFuZCBpdCBjYW4gYmUgcmV3cml0dGVuIGJhY2sgdG9cbiAgICA6aG9zdC1jb250ZXh0KCR0aGVtZSkgb25jZSB3ZSB3aWxsIGJlIGFibGUgdG8gdXNlIG11bHRpcGxlIHNoYWRvdyBzZWxlY3RvcnMuXG4gICpcbkJ1dCB3aGVuIGl0J3MgY2FsbGVkIGluICouY29tcG9uZW50LnNjc3Mgd2UgY2FuJ3QgYmUgc3VyZSwgdGhhdCBzZWxlY3RvciBzdGFydHMgd2l0aCA6aG9zdC86aG9zdC1jb250ZXh0LFxuYmVjYXVzZSBhbmd1bGFyIGFsbG93cyBvbWl0dGluZyBwc2V1ZG8tY2xhc3NlcyBpZiB3ZSBkb24ndCBuZWVkIHRvIHN0eWxlIDpob3N0IGNvbXBvbmVudCBpdHNlbGYuXG5XZSBjYW4gYnJlYWsgc3VjaCBzZWxlY3RvcnMsIGJ5IGp1c3QgYXBwZW5kaW5nIDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKSB0byB0aGVtLlxuICAqKipcbiAgICBQb3NzaWJsZSBzb2x1dGlvblxuICAgIGNoZWNrIGlmIHdlIGluIHRoZW1lIGJ5IHNvbWUgdGhlbWUgdmFyaWFibGVzIGFuZCBpZiBzbyBhcHBlbmQsIG90aGVyd2lzZSBuZXN0IGxpa2VcbiAgICBAYXQtcm9vdCA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkge1xuICAgICAgLy8gYWRkICMgdG8gc2NzcyBpbnRlcnBvbGF0aW9uIHN0YXRlbWVudC5cbiAgICAgIC8vIGl0IHdvcmtzIGluIGNvbW1lbnRzIGFuZCB3ZSBjYW4ndCB1c2UgaXQgaGVyZVxuICAgICAgeyZ9IHtcbiAgICAgICAgQGNvbnRlbnQ7XG4gICAgICB9XG4gICAgfVxuICAgIFdoYXQgaWYgOmhvc3Qgc3BlY2lmaWVkPyBDYW4gd2UgYWRkIHNwYWNlIGluIDpob3N0LWNvbnRleHQoLi4uKSA6aG9zdD9cbiAgICBPciBtYXliZSBhZGQgOmhvc3Qgc2VsZWN0b3IgYW55d2F5PyBJZiBtdWx0aXBsZSA6aG9zdCBzZWxlY3RvcnMgYXJlIGFsbG93ZWRcbiAgKioqXG5cblxuUHJvYmxlbXMgd2l0aCB0aGUgY3VycmVudCBhcHByb2FjaC5cblxuMS4gRGlyZWN0aW9uIGNhbiBiZSBhcHBsaWVkIG9ubHkgb24gZG9jdW1lbnQgbGV2ZWwsIGJlY2F1c2UgbWl4aW4gcHJlcGVuZHMgdGhlbWUgY2xhc3MsXG53aGljaCBwbGFjZWQgb24gdGhlIGJvZHkuXG4yLiAqLmNvbXBvbmVudC5zY3NzIHN0eWxlcyBzaG91bGQgYmUgaW4gOmhvc3Qgc2VsZWN0b3IuIE90aGVyd2lzZSBhbmd1bGFyIHdpbGwgYWRkIGhvc3RcbmF0dHJpYnV0ZSB0byBbZGlyPXJ0bF0gYXR0cmlidXRlIGFzIHdlbGwuXG5cblxuR2VuZXJhbCBwcm9ibGVtcy5cblxuTHRyIGlzIGRlZmF1bHQgZG9jdW1lbnQgZGlyZWN0aW9uLCBidXQgZm9yIHByb3BlciB3b3JrIG9mIG5iLWx0ciAobWVhbnMgbHRyIG9ubHkpLFxuW2Rpcj1sdHJdIHNob3VsZCBiZSBzcGVjaWZpZWQgYXQgbGVhc3Qgc29tZXdoZXJlLiAnOm5vdChbZGlyPXJ0bF0nIG5vdCBhcHBsaWNhYmxlIGhlcmUsXG5iZWNhdXNlIGl0J3Mgc2F0aXNmeSBhbnkgcGFyZW50LCB0aGF0IGRvbid0IGhhdmUgW2Rpcj1ydGxdIGF0dHJpYnV0ZS5cblByZXZpb3VzIGFwcHJvYWNoIHdhcyB0byB1c2Ugc2luZ2xlIHJ0bCBtaXhpbiBhbmQgcmVzZXQgbHRyIHByb3BlcnRpZXMgdG8gaW5pdGlhbCB2YWx1ZS5cbkJ1dCBzb21ldGltZXMgaXQncyBoYXJkIHRvIGZpbmQsIHdoYXQgdGhlIHByZXZpb3VzIHZhbHVlIHNob3VsZCBiZS4gQW5kIHN1Y2ggbWl4aW4gY2FsbCBsb29rcyB0b28gdmVyYm9zZS5cbiovXG4vKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG4vKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG4vKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG4vKipcbiAqIFRoaXMgbWl4aW4gZ2VuZXJhdGVzIGtleWZhbWVzLlxuICogQmVjYXVzZSBvZiBhbGwga2V5ZnJhbWVzIGNhbid0IGJlIHNjb3BlZCxcbiAqIHdlIG5lZWQgdG8gcHV0cyB1bmlxdWUgbmFtZSBpbiBlYWNoIGJ0bi1wdWxzZSBjYWxsLlxuICovXG4vKlxuXG5BY2NvcmRpbmcgdG8gdGhlIHNwZWNpZmljYXRpb24gKGh0dHBzOi8vd3d3LnczLm9yZy9UUi9jc3Mtc2NvcGluZy0xLyNob3N0LXNlbGVjdG9yKVxuOmhvc3QgYW5kIDpob3N0LWNvbnRleHQgYXJlIHBzZXVkby1jbGFzc2VzLiBTbyB3ZSBhc3N1bWUgdGhleSBjb3VsZCBiZSBjb21iaW5lZCxcbmxpa2Ugb3RoZXIgcHNldWRvLWNsYXNzZXMsIGV2ZW4gc2FtZSBvbmVzLlxuRm9yIGV4YW1wbGU6ICc6bnRoLW9mLXR5cGUoMm4pOm50aC1vZi10eXBlKGV2ZW4pJy5cblxuSWRlYWwgc29sdXRpb24gd291bGQgYmUgdG8gcHJlcGVuZCBhbnkgc2VsZWN0b3Igd2l0aCA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkuXG5UaGVuIG5lYnVsYXIgY29tcG9uZW50cyB3aWxsIGJlaGF2ZSBhcyBhbiBodG1sIGVsZW1lbnQgYW5kIHJlc3BvbmQgdG8gW2Rpcl0gYXR0cmlidXRlIG9uIGFueSBsZXZlbCxcbnNvIGRpcmVjdGlvbiBjb3VsZCBiZSBvdmVycmlkZGVuIG9uIGFueSBjb21wb25lbnQgbGV2ZWwuXG5cbkltcGxlbWVudGF0aW9uIGNvZGU6XG5cbkBtaXhpbiBuYi1ydGwoKSB7XG4gIC8vIGFkZCAjIHRvIHNjc3MgaW50ZXJwb2xhdGlvbiBzdGF0ZW1lbnQuXG4gIC8vIGl0IHdvcmtzIGluIGNvbW1lbnRzIGFuZCB3ZSBjYW4ndCB1c2UgaXQgaGVyZVxuICBAYXQtcm9vdCB7c2VsZWN0b3ItYXBwZW5kKCc6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSknLCAmKX0ge1xuICAgIEBjb250ZW50O1xuICB9XG59XG5cbkFuZCB3aGVuIHdlIGNhbGwgaXQgc29tZXdoZXJlOlxuXG46aG9zdCB7XG4gIC5zb21lLWNsYXNzIHtcbiAgICBAaW5jbHVkZSBuYi1ydGwoKSB7XG4gICAgICAuLi5cbiAgICB9XG4gIH1cbn1cbjpob3N0LWNvbnRleHQoLi4uKSB7XG4gIC5zb21lLWNsYXNzIHtcbiAgICBAaW5jbHVkZSBuYi1ydGwoKSB7XG4gICAgICAuLi5cbiAgICB9XG4gIH1cbn1cblxuUmVzdWx0IHdpbGwgbG9vayBsaWtlOlxuXG46aG9zdC1jb250ZXh0KFtkaXI9cnRsXSk6aG9zdCAuc29tZS1jbGFzcyB7XG4gIC4uLlxufVxuOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pOmhvc3QtY29udGV4dCguLi4pIC5zb21lLWNsYXNzIHtcbiAgLi4uXG59XG5cbipcbiAgU2lkZSBub3RlOlxuICA6aG9zdC1jb250ZXh0KCk6aG9zdCBzZWxlY3RvciBhcmUgdmFsaWQuIGh0dHBzOi8vbGlzdHMudzMub3JnL0FyY2hpdmVzL1B1YmxpYy93d3ctc3R5bGUvMjAxNUZlYi8wMzA1Lmh0bWxcblxuICA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSk6aG9zdC1jb250ZXh0KC4uLikgc2hvdWxkIG1hdGNoIGFueSBwZXJtdXRhdGlvbixcbiAgc28gb3JkZXIgaXMgbm90IGltcG9ydGFudC5cbipcblxuXG5DdXJyZW50bHksIHRoZXJlJ3JlIHR3byBwcm9ibGVtcyB3aXRoIHRoaXMgYXBwcm9hY2g6XG5cbkZpcnN0LCBpcyB0aGF0IHdlIGNhbid0IGNvbWJpbmUgOmhvc3QsIDpob3N0LWNvbnRleHQuIEFuZ3VsYXIgYnVncyAjMTQzNDksICMxOTE5OS5cbkZvciB0aGUgbW9tZW50IG9mIHdyaXRpbmcsIHRoZSBvbmx5IHBvc3NpYmxlIHdheSBpczpcbjpob3N0IHtcbiAgOmhvc3QtY29udGV4dCguLi4pIHtcbiAgICAuLi5cbiAgfVxufVxuSXQgZG9lc24ndCB3b3JrIGZvciB1cyBiZWNhdXNlIG1peGluIGNvdWxkIGJlIGNhbGxlZCBzb21ld2hlcmUgZGVlcGVyLCBsaWtlOlxuOmhvc3Qge1xuICBwIHtcbiAgICBAaW5jbHVkZSBuYi1ydGwoKSB7IC4uLiB9XG4gIH1cbn1cbldlIGFyZSBub3QgYWJsZSB0byBnbyB1cCB0byA6aG9zdCBsZXZlbCB0byBwbGFjZSBjb250ZW50IHBhc3NlZCB0byBtaXhpbi5cblxuVGhlIHNlY29uZCBwcm9ibGVtIGlzIHRoYXQgd2Ugb25seSBjYW4gYmUgc3VyZSB0aGF0IHdlIGFwcGVuZGluZyA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkgdG8gYW5vdGhlclxuOmhvc3QvOmhvc3QtY29udGV4dCBwc2V1ZG8tY2xhc3Mgd2hlbiBjYWxsZWQgaW4gdGhlbWUgZmlsZXMgKCoudGhlbWUuc2NzcykuXG4gICpcbiAgICBTaWRlIG5vdGU6XG4gICAgQ3VycmVudGx5LCBuYi1pbnN0YWxsLWNvbXBvbmVudCB1c2VzIGFub3RoZXIgYXBwcm9hY2ggd2hlcmUgOmhvc3QgcHJlcGVuZGVkIHdpdGggdGhlIHRoZW1lIG5hbWVcbiAgICAoaHR0cHM6Ly9naXRodWIuY29tL2FuZ3VsYXIvYW5ndWxhci9ibG9iLzViOTYwNzg2MjRiMGE0NzYwZjJkYmNmNmZkZjBiZDYyNzkxYmU1YmIvcGFja2FnZXMvY29tcGlsZXIvc3JjL3NoYWRvd19jc3MudHMjTDQ0MSksXG4gICAgYnV0IGl0IHdhcyBtYWRlIHRvIGJlIGFibGUgdG8gdXNlIGN1cnJlbnQgcmVhbGl6YXRpb24gb2YgcnRsIGFuZCBpdCBjYW4gYmUgcmV3cml0dGVuIGJhY2sgdG9cbiAgICA6aG9zdC1jb250ZXh0KCR0aGVtZSkgb25jZSB3ZSB3aWxsIGJlIGFibGUgdG8gdXNlIG11bHRpcGxlIHNoYWRvdyBzZWxlY3RvcnMuXG4gICpcbkJ1dCB3aGVuIGl0J3MgY2FsbGVkIGluICouY29tcG9uZW50LnNjc3Mgd2UgY2FuJ3QgYmUgc3VyZSwgdGhhdCBzZWxlY3RvciBzdGFydHMgd2l0aCA6aG9zdC86aG9zdC1jb250ZXh0LFxuYmVjYXVzZSBhbmd1bGFyIGFsbG93cyBvbWl0dGluZyBwc2V1ZG8tY2xhc3NlcyBpZiB3ZSBkb24ndCBuZWVkIHRvIHN0eWxlIDpob3N0IGNvbXBvbmVudCBpdHNlbGYuXG5XZSBjYW4gYnJlYWsgc3VjaCBzZWxlY3RvcnMsIGJ5IGp1c3QgYXBwZW5kaW5nIDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKSB0byB0aGVtLlxuICAqKipcbiAgICBQb3NzaWJsZSBzb2x1dGlvblxuICAgIGNoZWNrIGlmIHdlIGluIHRoZW1lIGJ5IHNvbWUgdGhlbWUgdmFyaWFibGVzIGFuZCBpZiBzbyBhcHBlbmQsIG90aGVyd2lzZSBuZXN0IGxpa2VcbiAgICBAYXQtcm9vdCA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkge1xuICAgICAgLy8gYWRkICMgdG8gc2NzcyBpbnRlcnBvbGF0aW9uIHN0YXRlbWVudC5cbiAgICAgIC8vIGl0IHdvcmtzIGluIGNvbW1lbnRzIGFuZCB3ZSBjYW4ndCB1c2UgaXQgaGVyZVxuICAgICAgeyZ9IHtcbiAgICAgICAgQGNvbnRlbnQ7XG4gICAgICB9XG4gICAgfVxuICAgIFdoYXQgaWYgOmhvc3Qgc3BlY2lmaWVkPyBDYW4gd2UgYWRkIHNwYWNlIGluIDpob3N0LWNvbnRleHQoLi4uKSA6aG9zdD9cbiAgICBPciBtYXliZSBhZGQgOmhvc3Qgc2VsZWN0b3IgYW55d2F5PyBJZiBtdWx0aXBsZSA6aG9zdCBzZWxlY3RvcnMgYXJlIGFsbG93ZWRcbiAgKioqXG5cblxuUHJvYmxlbXMgd2l0aCB0aGUgY3VycmVudCBhcHByb2FjaC5cblxuMS4gRGlyZWN0aW9uIGNhbiBiZSBhcHBsaWVkIG9ubHkgb24gZG9jdW1lbnQgbGV2ZWwsIGJlY2F1c2UgbWl4aW4gcHJlcGVuZHMgdGhlbWUgY2xhc3MsXG53aGljaCBwbGFjZWQgb24gdGhlIGJvZHkuXG4yLiAqLmNvbXBvbmVudC5zY3NzIHN0eWxlcyBzaG91bGQgYmUgaW4gOmhvc3Qgc2VsZWN0b3IuIE90aGVyd2lzZSBhbmd1bGFyIHdpbGwgYWRkIGhvc3RcbmF0dHJpYnV0ZSB0byBbZGlyPXJ0bF0gYXR0cmlidXRlIGFzIHdlbGwuXG5cblxuR2VuZXJhbCBwcm9ibGVtcy5cblxuTHRyIGlzIGRlZmF1bHQgZG9jdW1lbnQgZGlyZWN0aW9uLCBidXQgZm9yIHByb3BlciB3b3JrIG9mIG5iLWx0ciAobWVhbnMgbHRyIG9ubHkpLFxuW2Rpcj1sdHJdIHNob3VsZCBiZSBzcGVjaWZpZWQgYXQgbGVhc3Qgc29tZXdoZXJlLiAnOm5vdChbZGlyPXJ0bF0nIG5vdCBhcHBsaWNhYmxlIGhlcmUsXG5iZWNhdXNlIGl0J3Mgc2F0aXNmeSBhbnkgcGFyZW50LCB0aGF0IGRvbid0IGhhdmUgW2Rpcj1ydGxdIGF0dHJpYnV0ZS5cblByZXZpb3VzIGFwcHJvYWNoIHdhcyB0byB1c2Ugc2luZ2xlIHJ0bCBtaXhpbiBhbmQgcmVzZXQgbHRyIHByb3BlcnRpZXMgdG8gaW5pdGlhbCB2YWx1ZS5cbkJ1dCBzb21ldGltZXMgaXQncyBoYXJkIHRvIGZpbmQsIHdoYXQgdGhlIHByZXZpb3VzIHZhbHVlIHNob3VsZCBiZS4gQW5kIHN1Y2ggbWl4aW4gY2FsbCBsb29rcyB0b28gdmVyYm9zZS5cbiovXG4vKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG4vKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG4vKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG4vKipcbiAqIFRoaXMgbWl4aW4gZ2VuZXJhdGVzIGtleWZhbWVzLlxuICogQmVjYXVzZSBvZiBhbGwga2V5ZnJhbWVzIGNhbid0IGJlIHNjb3BlZCxcbiAqIHdlIG5lZWQgdG8gcHV0cyB1bmlxdWUgbmFtZSBpbiBlYWNoIGJ0bi1wdWxzZSBjYWxsLlxuICovXG4vKlxuXG5BY2NvcmRpbmcgdG8gdGhlIHNwZWNpZmljYXRpb24gKGh0dHBzOi8vd3d3LnczLm9yZy9UUi9jc3Mtc2NvcGluZy0xLyNob3N0LXNlbGVjdG9yKVxuOmhvc3QgYW5kIDpob3N0LWNvbnRleHQgYXJlIHBzZXVkby1jbGFzc2VzLiBTbyB3ZSBhc3N1bWUgdGhleSBjb3VsZCBiZSBjb21iaW5lZCxcbmxpa2Ugb3RoZXIgcHNldWRvLWNsYXNzZXMsIGV2ZW4gc2FtZSBvbmVzLlxuRm9yIGV4YW1wbGU6ICc6bnRoLW9mLXR5cGUoMm4pOm50aC1vZi10eXBlKGV2ZW4pJy5cblxuSWRlYWwgc29sdXRpb24gd291bGQgYmUgdG8gcHJlcGVuZCBhbnkgc2VsZWN0b3Igd2l0aCA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkuXG5UaGVuIG5lYnVsYXIgY29tcG9uZW50cyB3aWxsIGJlaGF2ZSBhcyBhbiBodG1sIGVsZW1lbnQgYW5kIHJlc3BvbmQgdG8gW2Rpcl0gYXR0cmlidXRlIG9uIGFueSBsZXZlbCxcbnNvIGRpcmVjdGlvbiBjb3VsZCBiZSBvdmVycmlkZGVuIG9uIGFueSBjb21wb25lbnQgbGV2ZWwuXG5cbkltcGxlbWVudGF0aW9uIGNvZGU6XG5cbkBtaXhpbiBuYi1ydGwoKSB7XG4gIC8vIGFkZCAjIHRvIHNjc3MgaW50ZXJwb2xhdGlvbiBzdGF0ZW1lbnQuXG4gIC8vIGl0IHdvcmtzIGluIGNvbW1lbnRzIGFuZCB3ZSBjYW4ndCB1c2UgaXQgaGVyZVxuICBAYXQtcm9vdCB7c2VsZWN0b3ItYXBwZW5kKCc6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSknLCAmKX0ge1xuICAgIEBjb250ZW50O1xuICB9XG59XG5cbkFuZCB3aGVuIHdlIGNhbGwgaXQgc29tZXdoZXJlOlxuXG46aG9zdCB7XG4gIC5zb21lLWNsYXNzIHtcbiAgICBAaW5jbHVkZSBuYi1ydGwoKSB7XG4gICAgICAuLi5cbiAgICB9XG4gIH1cbn1cbjpob3N0LWNvbnRleHQoLi4uKSB7XG4gIC5zb21lLWNsYXNzIHtcbiAgICBAaW5jbHVkZSBuYi1ydGwoKSB7XG4gICAgICAuLi5cbiAgICB9XG4gIH1cbn1cblxuUmVzdWx0IHdpbGwgbG9vayBsaWtlOlxuXG46aG9zdC1jb250ZXh0KFtkaXI9cnRsXSk6aG9zdCAuc29tZS1jbGFzcyB7XG4gIC4uLlxufVxuOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pOmhvc3QtY29udGV4dCguLi4pIC5zb21lLWNsYXNzIHtcbiAgLi4uXG59XG5cbipcbiAgU2lkZSBub3RlOlxuICA6aG9zdC1jb250ZXh0KCk6aG9zdCBzZWxlY3RvciBhcmUgdmFsaWQuIGh0dHBzOi8vbGlzdHMudzMub3JnL0FyY2hpdmVzL1B1YmxpYy93d3ctc3R5bGUvMjAxNUZlYi8wMzA1Lmh0bWxcblxuICA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSk6aG9zdC1jb250ZXh0KC4uLikgc2hvdWxkIG1hdGNoIGFueSBwZXJtdXRhdGlvbixcbiAgc28gb3JkZXIgaXMgbm90IGltcG9ydGFudC5cbipcblxuXG5DdXJyZW50bHksIHRoZXJlJ3JlIHR3byBwcm9ibGVtcyB3aXRoIHRoaXMgYXBwcm9hY2g6XG5cbkZpcnN0LCBpcyB0aGF0IHdlIGNhbid0IGNvbWJpbmUgOmhvc3QsIDpob3N0LWNvbnRleHQuIEFuZ3VsYXIgYnVncyAjMTQzNDksICMxOTE5OS5cbkZvciB0aGUgbW9tZW50IG9mIHdyaXRpbmcsIHRoZSBvbmx5IHBvc3NpYmxlIHdheSBpczpcbjpob3N0IHtcbiAgOmhvc3QtY29udGV4dCguLi4pIHtcbiAgICAuLi5cbiAgfVxufVxuSXQgZG9lc24ndCB3b3JrIGZvciB1cyBiZWNhdXNlIG1peGluIGNvdWxkIGJlIGNhbGxlZCBzb21ld2hlcmUgZGVlcGVyLCBsaWtlOlxuOmhvc3Qge1xuICBwIHtcbiAgICBAaW5jbHVkZSBuYi1ydGwoKSB7IC4uLiB9XG4gIH1cbn1cbldlIGFyZSBub3QgYWJsZSB0byBnbyB1cCB0byA6aG9zdCBsZXZlbCB0byBwbGFjZSBjb250ZW50IHBhc3NlZCB0byBtaXhpbi5cblxuVGhlIHNlY29uZCBwcm9ibGVtIGlzIHRoYXQgd2Ugb25seSBjYW4gYmUgc3VyZSB0aGF0IHdlIGFwcGVuZGluZyA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkgdG8gYW5vdGhlclxuOmhvc3QvOmhvc3QtY29udGV4dCBwc2V1ZG8tY2xhc3Mgd2hlbiBjYWxsZWQgaW4gdGhlbWUgZmlsZXMgKCoudGhlbWUuc2NzcykuXG4gICpcbiAgICBTaWRlIG5vdGU6XG4gICAgQ3VycmVudGx5LCBuYi1pbnN0YWxsLWNvbXBvbmVudCB1c2VzIGFub3RoZXIgYXBwcm9hY2ggd2hlcmUgOmhvc3QgcHJlcGVuZGVkIHdpdGggdGhlIHRoZW1lIG5hbWVcbiAgICAoaHR0cHM6Ly9naXRodWIuY29tL2FuZ3VsYXIvYW5ndWxhci9ibG9iLzViOTYwNzg2MjRiMGE0NzYwZjJkYmNmNmZkZjBiZDYyNzkxYmU1YmIvcGFja2FnZXMvY29tcGlsZXIvc3JjL3NoYWRvd19jc3MudHMjTDQ0MSksXG4gICAgYnV0IGl0IHdhcyBtYWRlIHRvIGJlIGFibGUgdG8gdXNlIGN1cnJlbnQgcmVhbGl6YXRpb24gb2YgcnRsIGFuZCBpdCBjYW4gYmUgcmV3cml0dGVuIGJhY2sgdG9cbiAgICA6aG9zdC1jb250ZXh0KCR0aGVtZSkgb25jZSB3ZSB3aWxsIGJlIGFibGUgdG8gdXNlIG11bHRpcGxlIHNoYWRvdyBzZWxlY3RvcnMuXG4gICpcbkJ1dCB3aGVuIGl0J3MgY2FsbGVkIGluICouY29tcG9uZW50LnNjc3Mgd2UgY2FuJ3QgYmUgc3VyZSwgdGhhdCBzZWxlY3RvciBzdGFydHMgd2l0aCA6aG9zdC86aG9zdC1jb250ZXh0LFxuYmVjYXVzZSBhbmd1bGFyIGFsbG93cyBvbWl0dGluZyBwc2V1ZG8tY2xhc3NlcyBpZiB3ZSBkb24ndCBuZWVkIHRvIHN0eWxlIDpob3N0IGNvbXBvbmVudCBpdHNlbGYuXG5XZSBjYW4gYnJlYWsgc3VjaCBzZWxlY3RvcnMsIGJ5IGp1c3QgYXBwZW5kaW5nIDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKSB0byB0aGVtLlxuICAqKipcbiAgICBQb3NzaWJsZSBzb2x1dGlvblxuICAgIGNoZWNrIGlmIHdlIGluIHRoZW1lIGJ5IHNvbWUgdGhlbWUgdmFyaWFibGVzIGFuZCBpZiBzbyBhcHBlbmQsIG90aGVyd2lzZSBuZXN0IGxpa2VcbiAgICBAYXQtcm9vdCA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkge1xuICAgICAgLy8gYWRkICMgdG8gc2NzcyBpbnRlcnBvbGF0aW9uIHN0YXRlbWVudC5cbiAgICAgIC8vIGl0IHdvcmtzIGluIGNvbW1lbnRzIGFuZCB3ZSBjYW4ndCB1c2UgaXQgaGVyZVxuICAgICAgeyZ9IHtcbiAgICAgICAgQGNvbnRlbnQ7XG4gICAgICB9XG4gICAgfVxuICAgIFdoYXQgaWYgOmhvc3Qgc3BlY2lmaWVkPyBDYW4gd2UgYWRkIHNwYWNlIGluIDpob3N0LWNvbnRleHQoLi4uKSA6aG9zdD9cbiAgICBPciBtYXliZSBhZGQgOmhvc3Qgc2VsZWN0b3IgYW55d2F5PyBJZiBtdWx0aXBsZSA6aG9zdCBzZWxlY3RvcnMgYXJlIGFsbG93ZWRcbiAgKioqXG5cblxuUHJvYmxlbXMgd2l0aCB0aGUgY3VycmVudCBhcHByb2FjaC5cblxuMS4gRGlyZWN0aW9uIGNhbiBiZSBhcHBsaWVkIG9ubHkgb24gZG9jdW1lbnQgbGV2ZWwsIGJlY2F1c2UgbWl4aW4gcHJlcGVuZHMgdGhlbWUgY2xhc3MsXG53aGljaCBwbGFjZWQgb24gdGhlIGJvZHkuXG4yLiAqLmNvbXBvbmVudC5zY3NzIHN0eWxlcyBzaG91bGQgYmUgaW4gOmhvc3Qgc2VsZWN0b3IuIE90aGVyd2lzZSBhbmd1bGFyIHdpbGwgYWRkIGhvc3RcbmF0dHJpYnV0ZSB0byBbZGlyPXJ0bF0gYXR0cmlidXRlIGFzIHdlbGwuXG5cblxuR2VuZXJhbCBwcm9ibGVtcy5cblxuTHRyIGlzIGRlZmF1bHQgZG9jdW1lbnQgZGlyZWN0aW9uLCBidXQgZm9yIHByb3BlciB3b3JrIG9mIG5iLWx0ciAobWVhbnMgbHRyIG9ubHkpLFxuW2Rpcj1sdHJdIHNob3VsZCBiZSBzcGVjaWZpZWQgYXQgbGVhc3Qgc29tZXdoZXJlLiAnOm5vdChbZGlyPXJ0bF0nIG5vdCBhcHBsaWNhYmxlIGhlcmUsXG5iZWNhdXNlIGl0J3Mgc2F0aXNmeSBhbnkgcGFyZW50LCB0aGF0IGRvbid0IGhhdmUgW2Rpcj1ydGxdIGF0dHJpYnV0ZS5cblByZXZpb3VzIGFwcHJvYWNoIHdhcyB0byB1c2Ugc2luZ2xlIHJ0bCBtaXhpbiBhbmQgcmVzZXQgbHRyIHByb3BlcnRpZXMgdG8gaW5pdGlhbCB2YWx1ZS5cbkJ1dCBzb21ldGltZXMgaXQncyBoYXJkIHRvIGZpbmQsIHdoYXQgdGhlIHByZXZpb3VzIHZhbHVlIHNob3VsZCBiZS4gQW5kIHN1Y2ggbWl4aW4gY2FsbCBsb29rcyB0b28gdmVyYm9zZS5cbiovXG4vKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG4vKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG4vKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG4vKipcbiAqIFRoaXMgbWl4aW4gZ2VuZXJhdGVzIGtleWZhbWVzLlxuICogQmVjYXVzZSBvZiBhbGwga2V5ZnJhbWVzIGNhbid0IGJlIHNjb3BlZCxcbiAqIHdlIG5lZWQgdG8gcHV0cyB1bmlxdWUgbmFtZSBpbiBlYWNoIGJ0bi1wdWxzZSBjYWxsLlxuICovXG4vKlxuXG5BY2NvcmRpbmcgdG8gdGhlIHNwZWNpZmljYXRpb24gKGh0dHBzOi8vd3d3LnczLm9yZy9UUi9jc3Mtc2NvcGluZy0xLyNob3N0LXNlbGVjdG9yKVxuOmhvc3QgYW5kIDpob3N0LWNvbnRleHQgYXJlIHBzZXVkby1jbGFzc2VzLiBTbyB3ZSBhc3N1bWUgdGhleSBjb3VsZCBiZSBjb21iaW5lZCxcbmxpa2Ugb3RoZXIgcHNldWRvLWNsYXNzZXMsIGV2ZW4gc2FtZSBvbmVzLlxuRm9yIGV4YW1wbGU6ICc6bnRoLW9mLXR5cGUoMm4pOm50aC1vZi10eXBlKGV2ZW4pJy5cblxuSWRlYWwgc29sdXRpb24gd291bGQgYmUgdG8gcHJlcGVuZCBhbnkgc2VsZWN0b3Igd2l0aCA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkuXG5UaGVuIG5lYnVsYXIgY29tcG9uZW50cyB3aWxsIGJlaGF2ZSBhcyBhbiBodG1sIGVsZW1lbnQgYW5kIHJlc3BvbmQgdG8gW2Rpcl0gYXR0cmlidXRlIG9uIGFueSBsZXZlbCxcbnNvIGRpcmVjdGlvbiBjb3VsZCBiZSBvdmVycmlkZGVuIG9uIGFueSBjb21wb25lbnQgbGV2ZWwuXG5cbkltcGxlbWVudGF0aW9uIGNvZGU6XG5cbkBtaXhpbiBuYi1ydGwoKSB7XG4gIC8vIGFkZCAjIHRvIHNjc3MgaW50ZXJwb2xhdGlvbiBzdGF0ZW1lbnQuXG4gIC8vIGl0IHdvcmtzIGluIGNvbW1lbnRzIGFuZCB3ZSBjYW4ndCB1c2UgaXQgaGVyZVxuICBAYXQtcm9vdCB7c2VsZWN0b3ItYXBwZW5kKCc6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSknLCAmKX0ge1xuICAgIEBjb250ZW50O1xuICB9XG59XG5cbkFuZCB3aGVuIHdlIGNhbGwgaXQgc29tZXdoZXJlOlxuXG46aG9zdCB7XG4gIC5zb21lLWNsYXNzIHtcbiAgICBAaW5jbHVkZSBuYi1ydGwoKSB7XG4gICAgICAuLi5cbiAgICB9XG4gIH1cbn1cbjpob3N0LWNvbnRleHQoLi4uKSB7XG4gIC5zb21lLWNsYXNzIHtcbiAgICBAaW5jbHVkZSBuYi1ydGwoKSB7XG4gICAgICAuLi5cbiAgICB9XG4gIH1cbn1cblxuUmVzdWx0IHdpbGwgbG9vayBsaWtlOlxuXG46aG9zdC1jb250ZXh0KFtkaXI9cnRsXSk6aG9zdCAuc29tZS1jbGFzcyB7XG4gIC4uLlxufVxuOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pOmhvc3QtY29udGV4dCguLi4pIC5zb21lLWNsYXNzIHtcbiAgLi4uXG59XG5cbipcbiAgU2lkZSBub3RlOlxuICA6aG9zdC1jb250ZXh0KCk6aG9zdCBzZWxlY3RvciBhcmUgdmFsaWQuIGh0dHBzOi8vbGlzdHMudzMub3JnL0FyY2hpdmVzL1B1YmxpYy93d3ctc3R5bGUvMjAxNUZlYi8wMzA1Lmh0bWxcblxuICA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSk6aG9zdC1jb250ZXh0KC4uLikgc2hvdWxkIG1hdGNoIGFueSBwZXJtdXRhdGlvbixcbiAgc28gb3JkZXIgaXMgbm90IGltcG9ydGFudC5cbipcblxuXG5DdXJyZW50bHksIHRoZXJlJ3JlIHR3byBwcm9ibGVtcyB3aXRoIHRoaXMgYXBwcm9hY2g6XG5cbkZpcnN0LCBpcyB0aGF0IHdlIGNhbid0IGNvbWJpbmUgOmhvc3QsIDpob3N0LWNvbnRleHQuIEFuZ3VsYXIgYnVncyAjMTQzNDksICMxOTE5OS5cbkZvciB0aGUgbW9tZW50IG9mIHdyaXRpbmcsIHRoZSBvbmx5IHBvc3NpYmxlIHdheSBpczpcbjpob3N0IHtcbiAgOmhvc3QtY29udGV4dCguLi4pIHtcbiAgICAuLi5cbiAgfVxufVxuSXQgZG9lc24ndCB3b3JrIGZvciB1cyBiZWNhdXNlIG1peGluIGNvdWxkIGJlIGNhbGxlZCBzb21ld2hlcmUgZGVlcGVyLCBsaWtlOlxuOmhvc3Qge1xuICBwIHtcbiAgICBAaW5jbHVkZSBuYi1ydGwoKSB7IC4uLiB9XG4gIH1cbn1cbldlIGFyZSBub3QgYWJsZSB0byBnbyB1cCB0byA6aG9zdCBsZXZlbCB0byBwbGFjZSBjb250ZW50IHBhc3NlZCB0byBtaXhpbi5cblxuVGhlIHNlY29uZCBwcm9ibGVtIGlzIHRoYXQgd2Ugb25seSBjYW4gYmUgc3VyZSB0aGF0IHdlIGFwcGVuZGluZyA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkgdG8gYW5vdGhlclxuOmhvc3QvOmhvc3QtY29udGV4dCBwc2V1ZG8tY2xhc3Mgd2hlbiBjYWxsZWQgaW4gdGhlbWUgZmlsZXMgKCoudGhlbWUuc2NzcykuXG4gICpcbiAgICBTaWRlIG5vdGU6XG4gICAgQ3VycmVudGx5LCBuYi1pbnN0YWxsLWNvbXBvbmVudCB1c2VzIGFub3RoZXIgYXBwcm9hY2ggd2hlcmUgOmhvc3QgcHJlcGVuZGVkIHdpdGggdGhlIHRoZW1lIG5hbWVcbiAgICAoaHR0cHM6Ly9naXRodWIuY29tL2FuZ3VsYXIvYW5ndWxhci9ibG9iLzViOTYwNzg2MjRiMGE0NzYwZjJkYmNmNmZkZjBiZDYyNzkxYmU1YmIvcGFja2FnZXMvY29tcGlsZXIvc3JjL3NoYWRvd19jc3MudHMjTDQ0MSksXG4gICAgYnV0IGl0IHdhcyBtYWRlIHRvIGJlIGFibGUgdG8gdXNlIGN1cnJlbnQgcmVhbGl6YXRpb24gb2YgcnRsIGFuZCBpdCBjYW4gYmUgcmV3cml0dGVuIGJhY2sgdG9cbiAgICA6aG9zdC1jb250ZXh0KCR0aGVtZSkgb25jZSB3ZSB3aWxsIGJlIGFibGUgdG8gdXNlIG11bHRpcGxlIHNoYWRvdyBzZWxlY3RvcnMuXG4gICpcbkJ1dCB3aGVuIGl0J3MgY2FsbGVkIGluICouY29tcG9uZW50LnNjc3Mgd2UgY2FuJ3QgYmUgc3VyZSwgdGhhdCBzZWxlY3RvciBzdGFydHMgd2l0aCA6aG9zdC86aG9zdC1jb250ZXh0LFxuYmVjYXVzZSBhbmd1bGFyIGFsbG93cyBvbWl0dGluZyBwc2V1ZG8tY2xhc3NlcyBpZiB3ZSBkb24ndCBuZWVkIHRvIHN0eWxlIDpob3N0IGNvbXBvbmVudCBpdHNlbGYuXG5XZSBjYW4gYnJlYWsgc3VjaCBzZWxlY3RvcnMsIGJ5IGp1c3QgYXBwZW5kaW5nIDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKSB0byB0aGVtLlxuICAqKipcbiAgICBQb3NzaWJsZSBzb2x1dGlvblxuICAgIGNoZWNrIGlmIHdlIGluIHRoZW1lIGJ5IHNvbWUgdGhlbWUgdmFyaWFibGVzIGFuZCBpZiBzbyBhcHBlbmQsIG90aGVyd2lzZSBuZXN0IGxpa2VcbiAgICBAYXQtcm9vdCA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkge1xuICAgICAgLy8gYWRkICMgdG8gc2NzcyBpbnRlcnBvbGF0aW9uIHN0YXRlbWVudC5cbiAgICAgIC8vIGl0IHdvcmtzIGluIGNvbW1lbnRzIGFuZCB3ZSBjYW4ndCB1c2UgaXQgaGVyZVxuICAgICAgeyZ9IHtcbiAgICAgICAgQGNvbnRlbnQ7XG4gICAgICB9XG4gICAgfVxuICAgIFdoYXQgaWYgOmhvc3Qgc3BlY2lmaWVkPyBDYW4gd2UgYWRkIHNwYWNlIGluIDpob3N0LWNvbnRleHQoLi4uKSA6aG9zdD9cbiAgICBPciBtYXliZSBhZGQgOmhvc3Qgc2VsZWN0b3IgYW55d2F5PyBJZiBtdWx0aXBsZSA6aG9zdCBzZWxlY3RvcnMgYXJlIGFsbG93ZWRcbiAgKioqXG5cblxuUHJvYmxlbXMgd2l0aCB0aGUgY3VycmVudCBhcHByb2FjaC5cblxuMS4gRGlyZWN0aW9uIGNhbiBiZSBhcHBsaWVkIG9ubHkgb24gZG9jdW1lbnQgbGV2ZWwsIGJlY2F1c2UgbWl4aW4gcHJlcGVuZHMgdGhlbWUgY2xhc3MsXG53aGljaCBwbGFjZWQgb24gdGhlIGJvZHkuXG4yLiAqLmNvbXBvbmVudC5zY3NzIHN0eWxlcyBzaG91bGQgYmUgaW4gOmhvc3Qgc2VsZWN0b3IuIE90aGVyd2lzZSBhbmd1bGFyIHdpbGwgYWRkIGhvc3RcbmF0dHJpYnV0ZSB0byBbZGlyPXJ0bF0gYXR0cmlidXRlIGFzIHdlbGwuXG5cblxuR2VuZXJhbCBwcm9ibGVtcy5cblxuTHRyIGlzIGRlZmF1bHQgZG9jdW1lbnQgZGlyZWN0aW9uLCBidXQgZm9yIHByb3BlciB3b3JrIG9mIG5iLWx0ciAobWVhbnMgbHRyIG9ubHkpLFxuW2Rpcj1sdHJdIHNob3VsZCBiZSBzcGVjaWZpZWQgYXQgbGVhc3Qgc29tZXdoZXJlLiAnOm5vdChbZGlyPXJ0bF0nIG5vdCBhcHBsaWNhYmxlIGhlcmUsXG5iZWNhdXNlIGl0J3Mgc2F0aXNmeSBhbnkgcGFyZW50LCB0aGF0IGRvbid0IGhhdmUgW2Rpcj1ydGxdIGF0dHJpYnV0ZS5cblByZXZpb3VzIGFwcHJvYWNoIHdhcyB0byB1c2Ugc2luZ2xlIHJ0bCBtaXhpbiBhbmQgcmVzZXQgbHRyIHByb3BlcnRpZXMgdG8gaW5pdGlhbCB2YWx1ZS5cbkJ1dCBzb21ldGltZXMgaXQncyBoYXJkIHRvIGZpbmQsIHdoYXQgdGhlIHByZXZpb3VzIHZhbHVlIHNob3VsZCBiZS4gQW5kIHN1Y2ggbWl4aW4gY2FsbCBsb29rcyB0b28gdmVyYm9zZS5cbiovXG4vKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG4vKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG4vKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG4vKipcbiAqIFRoaXMgbWl4aW4gZ2VuZXJhdGVzIGtleWZhbWVzLlxuICogQmVjYXVzZSBvZiBhbGwga2V5ZnJhbWVzIGNhbid0IGJlIHNjb3BlZCxcbiAqIHdlIG5lZWQgdG8gcHV0cyB1bmlxdWUgbmFtZSBpbiBlYWNoIGJ0bi1wdWxzZSBjYWxsLlxuICovXG4vKlxuXG5BY2NvcmRpbmcgdG8gdGhlIHNwZWNpZmljYXRpb24gKGh0dHBzOi8vd3d3LnczLm9yZy9UUi9jc3Mtc2NvcGluZy0xLyNob3N0LXNlbGVjdG9yKVxuOmhvc3QgYW5kIDpob3N0LWNvbnRleHQgYXJlIHBzZXVkby1jbGFzc2VzLiBTbyB3ZSBhc3N1bWUgdGhleSBjb3VsZCBiZSBjb21iaW5lZCxcbmxpa2Ugb3RoZXIgcHNldWRvLWNsYXNzZXMsIGV2ZW4gc2FtZSBvbmVzLlxuRm9yIGV4YW1wbGU6ICc6bnRoLW9mLXR5cGUoMm4pOm50aC1vZi10eXBlKGV2ZW4pJy5cblxuSWRlYWwgc29sdXRpb24gd291bGQgYmUgdG8gcHJlcGVuZCBhbnkgc2VsZWN0b3Igd2l0aCA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkuXG5UaGVuIG5lYnVsYXIgY29tcG9uZW50cyB3aWxsIGJlaGF2ZSBhcyBhbiBodG1sIGVsZW1lbnQgYW5kIHJlc3BvbmQgdG8gW2Rpcl0gYXR0cmlidXRlIG9uIGFueSBsZXZlbCxcbnNvIGRpcmVjdGlvbiBjb3VsZCBiZSBvdmVycmlkZGVuIG9uIGFueSBjb21wb25lbnQgbGV2ZWwuXG5cbkltcGxlbWVudGF0aW9uIGNvZGU6XG5cbkBtaXhpbiBuYi1ydGwoKSB7XG4gIC8vIGFkZCAjIHRvIHNjc3MgaW50ZXJwb2xhdGlvbiBzdGF0ZW1lbnQuXG4gIC8vIGl0IHdvcmtzIGluIGNvbW1lbnRzIGFuZCB3ZSBjYW4ndCB1c2UgaXQgaGVyZVxuICBAYXQtcm9vdCB7c2VsZWN0b3ItYXBwZW5kKCc6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSknLCAmKX0ge1xuICAgIEBjb250ZW50O1xuICB9XG59XG5cbkFuZCB3aGVuIHdlIGNhbGwgaXQgc29tZXdoZXJlOlxuXG46aG9zdCB7XG4gIC5zb21lLWNsYXNzIHtcbiAgICBAaW5jbHVkZSBuYi1ydGwoKSB7XG4gICAgICAuLi5cbiAgICB9XG4gIH1cbn1cbjpob3N0LWNvbnRleHQoLi4uKSB7XG4gIC5zb21lLWNsYXNzIHtcbiAgICBAaW5jbHVkZSBuYi1ydGwoKSB7XG4gICAgICAuLi5cbiAgICB9XG4gIH1cbn1cblxuUmVzdWx0IHdpbGwgbG9vayBsaWtlOlxuXG46aG9zdC1jb250ZXh0KFtkaXI9cnRsXSk6aG9zdCAuc29tZS1jbGFzcyB7XG4gIC4uLlxufVxuOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pOmhvc3QtY29udGV4dCguLi4pIC5zb21lLWNsYXNzIHtcbiAgLi4uXG59XG5cbipcbiAgU2lkZSBub3RlOlxuICA6aG9zdC1jb250ZXh0KCk6aG9zdCBzZWxlY3RvciBhcmUgdmFsaWQuIGh0dHBzOi8vbGlzdHMudzMub3JnL0FyY2hpdmVzL1B1YmxpYy93d3ctc3R5bGUvMjAxNUZlYi8wMzA1Lmh0bWxcblxuICA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSk6aG9zdC1jb250ZXh0KC4uLikgc2hvdWxkIG1hdGNoIGFueSBwZXJtdXRhdGlvbixcbiAgc28gb3JkZXIgaXMgbm90IGltcG9ydGFudC5cbipcblxuXG5DdXJyZW50bHksIHRoZXJlJ3JlIHR3byBwcm9ibGVtcyB3aXRoIHRoaXMgYXBwcm9hY2g6XG5cbkZpcnN0LCBpcyB0aGF0IHdlIGNhbid0IGNvbWJpbmUgOmhvc3QsIDpob3N0LWNvbnRleHQuIEFuZ3VsYXIgYnVncyAjMTQzNDksICMxOTE5OS5cbkZvciB0aGUgbW9tZW50IG9mIHdyaXRpbmcsIHRoZSBvbmx5IHBvc3NpYmxlIHdheSBpczpcbjpob3N0IHtcbiAgOmhvc3QtY29udGV4dCguLi4pIHtcbiAgICAuLi5cbiAgfVxufVxuSXQgZG9lc24ndCB3b3JrIGZvciB1cyBiZWNhdXNlIG1peGluIGNvdWxkIGJlIGNhbGxlZCBzb21ld2hlcmUgZGVlcGVyLCBsaWtlOlxuOmhvc3Qge1xuICBwIHtcbiAgICBAaW5jbHVkZSBuYi1ydGwoKSB7IC4uLiB9XG4gIH1cbn1cbldlIGFyZSBub3QgYWJsZSB0byBnbyB1cCB0byA6aG9zdCBsZXZlbCB0byBwbGFjZSBjb250ZW50IHBhc3NlZCB0byBtaXhpbi5cblxuVGhlIHNlY29uZCBwcm9ibGVtIGlzIHRoYXQgd2Ugb25seSBjYW4gYmUgc3VyZSB0aGF0IHdlIGFwcGVuZGluZyA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkgdG8gYW5vdGhlclxuOmhvc3QvOmhvc3QtY29udGV4dCBwc2V1ZG8tY2xhc3Mgd2hlbiBjYWxsZWQgaW4gdGhlbWUgZmlsZXMgKCoudGhlbWUuc2NzcykuXG4gICpcbiAgICBTaWRlIG5vdGU6XG4gICAgQ3VycmVudGx5LCBuYi1pbnN0YWxsLWNvbXBvbmVudCB1c2VzIGFub3RoZXIgYXBwcm9hY2ggd2hlcmUgOmhvc3QgcHJlcGVuZGVkIHdpdGggdGhlIHRoZW1lIG5hbWVcbiAgICAoaHR0cHM6Ly9naXRodWIuY29tL2FuZ3VsYXIvYW5ndWxhci9ibG9iLzViOTYwNzg2MjRiMGE0NzYwZjJkYmNmNmZkZjBiZDYyNzkxYmU1YmIvcGFja2FnZXMvY29tcGlsZXIvc3JjL3NoYWRvd19jc3MudHMjTDQ0MSksXG4gICAgYnV0IGl0IHdhcyBtYWRlIHRvIGJlIGFibGUgdG8gdXNlIGN1cnJlbnQgcmVhbGl6YXRpb24gb2YgcnRsIGFuZCBpdCBjYW4gYmUgcmV3cml0dGVuIGJhY2sgdG9cbiAgICA6aG9zdC1jb250ZXh0KCR0aGVtZSkgb25jZSB3ZSB3aWxsIGJlIGFibGUgdG8gdXNlIG11bHRpcGxlIHNoYWRvdyBzZWxlY3RvcnMuXG4gICpcbkJ1dCB3aGVuIGl0J3MgY2FsbGVkIGluICouY29tcG9uZW50LnNjc3Mgd2UgY2FuJ3QgYmUgc3VyZSwgdGhhdCBzZWxlY3RvciBzdGFydHMgd2l0aCA6aG9zdC86aG9zdC1jb250ZXh0LFxuYmVjYXVzZSBhbmd1bGFyIGFsbG93cyBvbWl0dGluZyBwc2V1ZG8tY2xhc3NlcyBpZiB3ZSBkb24ndCBuZWVkIHRvIHN0eWxlIDpob3N0IGNvbXBvbmVudCBpdHNlbGYuXG5XZSBjYW4gYnJlYWsgc3VjaCBzZWxlY3RvcnMsIGJ5IGp1c3QgYXBwZW5kaW5nIDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKSB0byB0aGVtLlxuICAqKipcbiAgICBQb3NzaWJsZSBzb2x1dGlvblxuICAgIGNoZWNrIGlmIHdlIGluIHRoZW1lIGJ5IHNvbWUgdGhlbWUgdmFyaWFibGVzIGFuZCBpZiBzbyBhcHBlbmQsIG90aGVyd2lzZSBuZXN0IGxpa2VcbiAgICBAYXQtcm9vdCA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkge1xuICAgICAgLy8gYWRkICMgdG8gc2NzcyBpbnRlcnBvbGF0aW9uIHN0YXRlbWVudC5cbiAgICAgIC8vIGl0IHdvcmtzIGluIGNvbW1lbnRzIGFuZCB3ZSBjYW4ndCB1c2UgaXQgaGVyZVxuICAgICAgeyZ9IHtcbiAgICAgICAgQGNvbnRlbnQ7XG4gICAgICB9XG4gICAgfVxuICAgIFdoYXQgaWYgOmhvc3Qgc3BlY2lmaWVkPyBDYW4gd2UgYWRkIHNwYWNlIGluIDpob3N0LWNvbnRleHQoLi4uKSA6aG9zdD9cbiAgICBPciBtYXliZSBhZGQgOmhvc3Qgc2VsZWN0b3IgYW55d2F5PyBJZiBtdWx0aXBsZSA6aG9zdCBzZWxlY3RvcnMgYXJlIGFsbG93ZWRcbiAgKioqXG5cblxuUHJvYmxlbXMgd2l0aCB0aGUgY3VycmVudCBhcHByb2FjaC5cblxuMS4gRGlyZWN0aW9uIGNhbiBiZSBhcHBsaWVkIG9ubHkgb24gZG9jdW1lbnQgbGV2ZWwsIGJlY2F1c2UgbWl4aW4gcHJlcGVuZHMgdGhlbWUgY2xhc3MsXG53aGljaCBwbGFjZWQgb24gdGhlIGJvZHkuXG4yLiAqLmNvbXBvbmVudC5zY3NzIHN0eWxlcyBzaG91bGQgYmUgaW4gOmhvc3Qgc2VsZWN0b3IuIE90aGVyd2lzZSBhbmd1bGFyIHdpbGwgYWRkIGhvc3RcbmF0dHJpYnV0ZSB0byBbZGlyPXJ0bF0gYXR0cmlidXRlIGFzIHdlbGwuXG5cblxuR2VuZXJhbCBwcm9ibGVtcy5cblxuTHRyIGlzIGRlZmF1bHQgZG9jdW1lbnQgZGlyZWN0aW9uLCBidXQgZm9yIHByb3BlciB3b3JrIG9mIG5iLWx0ciAobWVhbnMgbHRyIG9ubHkpLFxuW2Rpcj1sdHJdIHNob3VsZCBiZSBzcGVjaWZpZWQgYXQgbGVhc3Qgc29tZXdoZXJlLiAnOm5vdChbZGlyPXJ0bF0nIG5vdCBhcHBsaWNhYmxlIGhlcmUsXG5iZWNhdXNlIGl0J3Mgc2F0aXNmeSBhbnkgcGFyZW50LCB0aGF0IGRvbid0IGhhdmUgW2Rpcj1ydGxdIGF0dHJpYnV0ZS5cblByZXZpb3VzIGFwcHJvYWNoIHdhcyB0byB1c2Ugc2luZ2xlIHJ0bCBtaXhpbiBhbmQgcmVzZXQgbHRyIHByb3BlcnRpZXMgdG8gaW5pdGlhbCB2YWx1ZS5cbkJ1dCBzb21ldGltZXMgaXQncyBoYXJkIHRvIGZpbmQsIHdoYXQgdGhlIHByZXZpb3VzIHZhbHVlIHNob3VsZCBiZS4gQW5kIHN1Y2ggbWl4aW4gY2FsbCBsb29rcyB0b28gdmVyYm9zZS5cbiovXG4vKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG4vKlxuICAgICAgOmhvc3QgY2FuIGJlIHByZWZpeGVkXG4gICAgICBodHRwczovL2dpdGh1Yi5jb20vYW5ndWxhci9hbmd1bGFyL2Jsb2IvOGQwZWUzNDkzOWYxNGMwNzg3NmQyMjJjMjViNDA1ZWQ0NThhMzRkMy9wYWNrYWdlcy9jb21waWxlci9zcmMvc2hhZG93X2Nzcy50cyNMNDQxXG5cbiAgICAgIFdlIGhhdmUgdG8gdXNlIDpob3N0IGluc3RlZCBvZiA6aG9zdC1jb250ZXh0KCR0aGVtZSksIHRvIGJlIGFibGUgdG8gcHJlZml4IHRoZW1lIGNsYXNzXG4gICAgICB3aXRoIHNvbWV0aGluZyBkZWZpbmVkIGluc2lkZSBvZiBAY29udGVudCwgYnkgcHJlZml4aW5nICYuXG4gICAgICBGb3IgZXhhbXBsZSB0aGlzIHNjc3MgY29kZTpcbiAgICAgICAgLm5iLXRoZW1lLWRlZmF1bHQge1xuICAgICAgICAgIC5zb21lLXNlbGVjdG9yICYge1xuICAgICAgICAgICAgLi4uXG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICBXaWxsIHJlc3VsdCBpbiBuZXh0IGNzczpcbiAgICAgICAgLnNvbWUtc2VsZWN0b3IgLm5iLXRoZW1lLWRlZmF1bHQge1xuICAgICAgICAgIC4uLlxuICAgICAgICB9XG5cbiAgICAgIEl0IGRvZXNuJ3Qgd29yayB3aXRoIDpob3N0LWNvbnRleHQgYmVjYXVzZSBhbmd1bGFyIHNwbGl0dGluZyBpdCBpbiB0d28gc2VsZWN0b3JzIGFuZCByZW1vdmVzXG4gICAgICBwcmVmaXggaW4gb25lIG9mIHRoZSBzZWxlY3RvcnMuXG4gICAgKi9cbi5uYi10aGVtZS1kZWZhdWx0IDpob3N0IC5oaSB7XG4gIGJvcmRlcjogMnB4IHNvbGlkOyB9XG5cbi5uYi10aGVtZS1kZWZhdWx0IDpob3N0IHRhYmxlIHtcbiAgd2lkdGg6IC13ZWJraXQtZmlsbC1hdmFpbGFibGU7IH1cblxuLm5iLXRoZW1lLWRlZmF1bHQgOmhvc3QgYSB7XG4gIGN1cnNvcjogcG9pbnRlcjsgfVxuXG4ubmItdGhlbWUtZGVmYXVsdCA6aG9zdCBuYi1jYXJkIHtcbiAgYm9yZGVyOiBub25lOyB9XG5cbi5uYi10aGVtZS1kZWZhdWx0IDpob3N0IG5iLWNhcmQtYm9keSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNFMUY1RkU7IH1cblxuLm5iLXRoZW1lLWRlZmF1bHQgOmhvc3QgbmItY2FyZC1oZWFkZXIge1xuICBiYWNrZ3JvdW5kOiAjRTFGNUZFOyB9XG5cbi5uYi10aGVtZS1kZWZhdWx0IDpob3N0IC9kZWVwLyAubW9kYWwtZGlhbG9nIHtcbiAgbWF4LXdpZHRoOiA0NSU7IH1cblxuLm5iLXRoZW1lLWRlZmF1bHQgOmhvc3QgL2RlZXAvIG5nMi1zbWFydC10YWJsZSBpIHtcbiAgY29sb3I6ICMwMDA7IH1cblxuLm5iLXRoZW1lLWRlZmF1bHQgOmhvc3QgL2RlZXAvIG5nMi1zbWFydC10YWJsZSAubmcyLXNtYXJ0LXRpdGxlIHtcbiAgY29sb3I6IHdoaXRlOyB9XG5cbi5uYi10aGVtZS1kZWZhdWx0IDpob3N0IC9kZWVwLyBuZzItc21hcnQtdGFibGUgLm5nMi1zbWFydC1wYWdpbmF0aW9uLW5hdiAubmcyLXNtYXJ0LXBhZ2luYXRpb24ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAwOyB9XG5cbi5uYi10aGVtZS1kZWZhdWx0IDpob3N0IC9kZWVwLyBuZzItc21hcnQtdGFibGUgaSB7XG4gIGNvbG9yOiAjMDAwOyB9XG5cbi5uYi10aGVtZS1kZWZhdWx0IDpob3N0IC9kZWVwLyBtb2RhbCAubW9kYWwtZGlhbG9nIHtcbiAgbWF4LXdpZHRoOiAxMDAwcHggIWltcG9ydGFudDsgfVxuXG4ubmItdGhlbWUtZGVmYXVsdCA6aG9zdCAvZGVlcC8gbmcyLXN0LXRib2R5LWVkaXQtZGVsZXRlIHtcbiAgZGlzcGxheTogLXdlYmtpdC1pbmxpbmUtYm94ICFpbXBvcnRhbnQ7XG4gIGhlaWdodDogMHB4ICFpbXBvcnRhbnQ7XG4gIGZsb2F0OiBsZWZ0O1xuICBtYXJnaW4tbGVmdDogMzZweDtcbiAgbWFyZ2luLXRvcDogLTE1cHggIWltcG9ydGFudDtcbiAgLyogbWFyZ2luOiBhdXRvOyAqLyB9XG5cbi5uYi10aGVtZS1kZWZhdWx0IDpob3N0IC9kZWVwLyBuZzItc3QtdGJvZHktY3VzdG9tIGEubmcyLXNtYXJ0LWFjdGlvbi5uZzItc21hcnQtYWN0aW9uLWN1c3RvbS1jdXN0b20ge1xuICB3aWR0aDogNTBweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDAuMWVtOyB9XG5cbi5uYi10aGVtZS1kZWZhdWx0IDpob3N0IC9kZWVwLyBuZzItc3QtdGJvZHktY3VzdG9tIGEubmcyLXNtYXJ0LWFjdGlvbi5uZzItc21hcnQtYWN0aW9uLWN1c3RvbS1jdXN0b20gaW1nIHtcbiAgd2lkdGg6IDU3JSAhaW1wb3J0YW50O1xuICBtYXJnaW4tbGVmdDogMzRweDsgfVxuXG4ubmItdGhlbWUtZGVmYXVsdCA6aG9zdCAvZGVlcC8gbmcyLXN0LXRib2R5LWN1c3RvbSBhLm5nMi1zbWFydC1hY3Rpb24ubmcyLXNtYXJ0LWFjdGlvbi1jdXN0b20tY3VzdG9tOmhvdmVyIHtcbiAgY29sb3I6ICM1ZGNmZTM7IH1cblxuLm5iLXRoZW1lLWRlZmF1bHQgOmhvc3QgL2RlZXAvIG5nMi1zdC10Ym9keS1lZGl0LWRlbGV0ZSBpIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlICFpbXBvcnRhbnQ7XG4gIHRvcDogLTE0cHg7XG4gIG1hcmdpbi1sZWZ0OiA0MHB4OyB9XG5cbi5uYi10aGVtZS1kZWZhdWx0IDpob3N0IGJ1dHRvbiB7XG4gIGZsb2F0OiByaWdodDsgfVxuXG4vKlxuICAgICAgOmhvc3QgY2FuIGJlIHByZWZpeGVkXG4gICAgICBodHRwczovL2dpdGh1Yi5jb20vYW5ndWxhci9hbmd1bGFyL2Jsb2IvOGQwZWUzNDkzOWYxNGMwNzg3NmQyMjJjMjViNDA1ZWQ0NThhMzRkMy9wYWNrYWdlcy9jb21waWxlci9zcmMvc2hhZG93X2Nzcy50cyNMNDQxXG5cbiAgICAgIFdlIGhhdmUgdG8gdXNlIDpob3N0IGluc3RlZCBvZiA6aG9zdC1jb250ZXh0KCR0aGVtZSksIHRvIGJlIGFibGUgdG8gcHJlZml4IHRoZW1lIGNsYXNzXG4gICAgICB3aXRoIHNvbWV0aGluZyBkZWZpbmVkIGluc2lkZSBvZiBAY29udGVudCwgYnkgcHJlZml4aW5nICYuXG4gICAgICBGb3IgZXhhbXBsZSB0aGlzIHNjc3MgY29kZTpcbiAgICAgICAgLm5iLXRoZW1lLWRlZmF1bHQge1xuICAgICAgICAgIC5zb21lLXNlbGVjdG9yICYge1xuICAgICAgICAgICAgLi4uXG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICBXaWxsIHJlc3VsdCBpbiBuZXh0IGNzczpcbiAgICAgICAgLnNvbWUtc2VsZWN0b3IgLm5iLXRoZW1lLWRlZmF1bHQge1xuICAgICAgICAgIC4uLlxuICAgICAgICB9XG5cbiAgICAgIEl0IGRvZXNuJ3Qgd29yayB3aXRoIDpob3N0LWNvbnRleHQgYmVjYXVzZSBhbmd1bGFyIHNwbGl0dGluZyBpdCBpbiB0d28gc2VsZWN0b3JzIGFuZCByZW1vdmVzXG4gICAgICBwcmVmaXggaW4gb25lIG9mIHRoZSBzZWxlY3RvcnMuXG4gICAgKi9cbi5uYi10aGVtZS1jb3NtaWMgOmhvc3QgLmhpIHtcbiAgYm9yZGVyOiAycHggc29saWQ7IH1cblxuLm5iLXRoZW1lLWNvc21pYyA6aG9zdCB0YWJsZSB7XG4gIHdpZHRoOiAtd2Via2l0LWZpbGwtYXZhaWxhYmxlOyB9XG5cbi5uYi10aGVtZS1jb3NtaWMgOmhvc3QgYSB7XG4gIGN1cnNvcjogcG9pbnRlcjsgfVxuXG4ubmItdGhlbWUtY29zbWljIDpob3N0IG5iLWNhcmQge1xuICBib3JkZXI6IG5vbmU7IH1cblxuLm5iLXRoZW1lLWNvc21pYyA6aG9zdCBuYi1jYXJkLWJvZHkge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjRTFGNUZFOyB9XG5cbi5uYi10aGVtZS1jb3NtaWMgOmhvc3QgbmItY2FyZC1oZWFkZXIge1xuICBiYWNrZ3JvdW5kOiAjRTFGNUZFOyB9XG5cbi5uYi10aGVtZS1jb3NtaWMgOmhvc3QgL2RlZXAvIC5tb2RhbC1kaWFsb2cge1xuICBtYXgtd2lkdGg6IDQ1JTsgfVxuXG4ubmItdGhlbWUtY29zbWljIDpob3N0IC9kZWVwLyBuZzItc21hcnQtdGFibGUgaSB7XG4gIGNvbG9yOiAjMDAwOyB9XG5cbi5uYi10aGVtZS1jb3NtaWMgOmhvc3QgL2RlZXAvIG5nMi1zbWFydC10YWJsZSAubmcyLXNtYXJ0LXRpdGxlIHtcbiAgY29sb3I6IHdoaXRlOyB9XG5cbi5uYi10aGVtZS1jb3NtaWMgOmhvc3QgL2RlZXAvIG5nMi1zbWFydC10YWJsZSAubmcyLXNtYXJ0LXBhZ2luYXRpb24tbmF2IC5uZzItc21hcnQtcGFnaW5hdGlvbiB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDA7IH1cblxuLm5iLXRoZW1lLWNvc21pYyA6aG9zdCAvZGVlcC8gbmcyLXNtYXJ0LXRhYmxlIGkge1xuICBjb2xvcjogIzAwMDsgfVxuXG4ubmItdGhlbWUtY29zbWljIDpob3N0IC9kZWVwLyBtb2RhbCAubW9kYWwtZGlhbG9nIHtcbiAgbWF4LXdpZHRoOiAxMDAwcHggIWltcG9ydGFudDsgfVxuXG4ubmItdGhlbWUtY29zbWljIDpob3N0IC9kZWVwLyBuZzItc3QtdGJvZHktZWRpdC1kZWxldGUge1xuICBkaXNwbGF5OiAtd2Via2l0LWlubGluZS1ib3ggIWltcG9ydGFudDtcbiAgaGVpZ2h0OiAwcHggIWltcG9ydGFudDtcbiAgZmxvYXQ6IGxlZnQ7XG4gIG1hcmdpbi1sZWZ0OiAzNnB4O1xuICBtYXJnaW4tdG9wOiAtMTVweCAhaW1wb3J0YW50O1xuICAvKiBtYXJnaW46IGF1dG87ICovIH1cblxuLm5iLXRoZW1lLWNvc21pYyA6aG9zdCAvZGVlcC8gbmcyLXN0LXRib2R5LWN1c3RvbSBhLm5nMi1zbWFydC1hY3Rpb24ubmcyLXNtYXJ0LWFjdGlvbi1jdXN0b20tY3VzdG9tIHtcbiAgd2lkdGg6IDUwcHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAwLjFlbTsgfVxuXG4ubmItdGhlbWUtY29zbWljIDpob3N0IC9kZWVwLyBuZzItc3QtdGJvZHktY3VzdG9tIGEubmcyLXNtYXJ0LWFjdGlvbi5uZzItc21hcnQtYWN0aW9uLWN1c3RvbS1jdXN0b20gaW1nIHtcbiAgd2lkdGg6IDU3JSAhaW1wb3J0YW50O1xuICBtYXJnaW4tbGVmdDogMzRweDsgfVxuXG4ubmItdGhlbWUtY29zbWljIDpob3N0IC9kZWVwLyBuZzItc3QtdGJvZHktY3VzdG9tIGEubmcyLXNtYXJ0LWFjdGlvbi5uZzItc21hcnQtYWN0aW9uLWN1c3RvbS1jdXN0b206aG92ZXIge1xuICBjb2xvcjogIzVkY2ZlMzsgfVxuXG4ubmItdGhlbWUtY29zbWljIDpob3N0IC9kZWVwLyBuZzItc3QtdGJvZHktZWRpdC1kZWxldGUgaSB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZSAhaW1wb3J0YW50O1xuICB0b3A6IC0xNHB4O1xuICBtYXJnaW4tbGVmdDogNDBweDsgfVxuXG4ubmItdGhlbWUtY29zbWljIDpob3N0IGJ1dHRvbiB7XG4gIGZsb2F0OiByaWdodDsgfVxuXG4vKlxuICAgICAgOmhvc3QgY2FuIGJlIHByZWZpeGVkXG4gICAgICBodHRwczovL2dpdGh1Yi5jb20vYW5ndWxhci9hbmd1bGFyL2Jsb2IvOGQwZWUzNDkzOWYxNGMwNzg3NmQyMjJjMjViNDA1ZWQ0NThhMzRkMy9wYWNrYWdlcy9jb21waWxlci9zcmMvc2hhZG93X2Nzcy50cyNMNDQxXG5cbiAgICAgIFdlIGhhdmUgdG8gdXNlIDpob3N0IGluc3RlZCBvZiA6aG9zdC1jb250ZXh0KCR0aGVtZSksIHRvIGJlIGFibGUgdG8gcHJlZml4IHRoZW1lIGNsYXNzXG4gICAgICB3aXRoIHNvbWV0aGluZyBkZWZpbmVkIGluc2lkZSBvZiBAY29udGVudCwgYnkgcHJlZml4aW5nICYuXG4gICAgICBGb3IgZXhhbXBsZSB0aGlzIHNjc3MgY29kZTpcbiAgICAgICAgLm5iLXRoZW1lLWRlZmF1bHQge1xuICAgICAgICAgIC5zb21lLXNlbGVjdG9yICYge1xuICAgICAgICAgICAgLi4uXG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICBXaWxsIHJlc3VsdCBpbiBuZXh0IGNzczpcbiAgICAgICAgLnNvbWUtc2VsZWN0b3IgLm5iLXRoZW1lLWRlZmF1bHQge1xuICAgICAgICAgIC4uLlxuICAgICAgICB9XG5cbiAgICAgIEl0IGRvZXNuJ3Qgd29yayB3aXRoIDpob3N0LWNvbnRleHQgYmVjYXVzZSBhbmd1bGFyIHNwbGl0dGluZyBpdCBpbiB0d28gc2VsZWN0b3JzIGFuZCByZW1vdmVzXG4gICAgICBwcmVmaXggaW4gb25lIG9mIHRoZSBzZWxlY3RvcnMuXG4gICAgKi9cbi5uYi10aGVtZS1jb3Jwb3JhdGUgOmhvc3QgLmhpIHtcbiAgYm9yZGVyOiAycHggc29saWQ7IH1cblxuLm5iLXRoZW1lLWNvcnBvcmF0ZSA6aG9zdCB0YWJsZSB7XG4gIHdpZHRoOiAtd2Via2l0LWZpbGwtYXZhaWxhYmxlOyB9XG5cbi5uYi10aGVtZS1jb3Jwb3JhdGUgOmhvc3QgYSB7XG4gIGN1cnNvcjogcG9pbnRlcjsgfVxuXG4ubmItdGhlbWUtY29ycG9yYXRlIDpob3N0IG5iLWNhcmQge1xuICBib3JkZXI6IG5vbmU7IH1cblxuLm5iLXRoZW1lLWNvcnBvcmF0ZSA6aG9zdCBuYi1jYXJkLWJvZHkge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjRTFGNUZFOyB9XG5cbi5uYi10aGVtZS1jb3Jwb3JhdGUgOmhvc3QgbmItY2FyZC1oZWFkZXIge1xuICBiYWNrZ3JvdW5kOiAjRTFGNUZFOyB9XG5cbi5uYi10aGVtZS1jb3Jwb3JhdGUgOmhvc3QgL2RlZXAvIC5tb2RhbC1kaWFsb2cge1xuICBtYXgtd2lkdGg6IDQ1JTsgfVxuXG4ubmItdGhlbWUtY29ycG9yYXRlIDpob3N0IC9kZWVwLyBuZzItc21hcnQtdGFibGUgaSB7XG4gIGNvbG9yOiAjMDAwOyB9XG5cbi5uYi10aGVtZS1jb3Jwb3JhdGUgOmhvc3QgL2RlZXAvIG5nMi1zbWFydC10YWJsZSAubmcyLXNtYXJ0LXRpdGxlIHtcbiAgY29sb3I6IHdoaXRlOyB9XG5cbi5uYi10aGVtZS1jb3Jwb3JhdGUgOmhvc3QgL2RlZXAvIG5nMi1zbWFydC10YWJsZSAubmcyLXNtYXJ0LXBhZ2luYXRpb24tbmF2IC5uZzItc21hcnQtcGFnaW5hdGlvbiB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDA7IH1cblxuLm5iLXRoZW1lLWNvcnBvcmF0ZSA6aG9zdCAvZGVlcC8gbmcyLXNtYXJ0LXRhYmxlIGkge1xuICBjb2xvcjogIzAwMDsgfVxuXG4ubmItdGhlbWUtY29ycG9yYXRlIDpob3N0IC9kZWVwLyBtb2RhbCAubW9kYWwtZGlhbG9nIHtcbiAgbWF4LXdpZHRoOiAxMDAwcHggIWltcG9ydGFudDsgfVxuXG4ubmItdGhlbWUtY29ycG9yYXRlIDpob3N0IC9kZWVwLyBuZzItc3QtdGJvZHktZWRpdC1kZWxldGUge1xuICBkaXNwbGF5OiAtd2Via2l0LWlubGluZS1ib3ggIWltcG9ydGFudDtcbiAgaGVpZ2h0OiAwcHggIWltcG9ydGFudDtcbiAgZmxvYXQ6IGxlZnQ7XG4gIG1hcmdpbi1sZWZ0OiAzNnB4O1xuICBtYXJnaW4tdG9wOiAtMTVweCAhaW1wb3J0YW50O1xuICAvKiBtYXJnaW46IGF1dG87ICovIH1cblxuLm5iLXRoZW1lLWNvcnBvcmF0ZSA6aG9zdCAvZGVlcC8gbmcyLXN0LXRib2R5LWN1c3RvbSBhLm5nMi1zbWFydC1hY3Rpb24ubmcyLXNtYXJ0LWFjdGlvbi1jdXN0b20tY3VzdG9tIHtcbiAgd2lkdGg6IDUwcHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAwLjFlbTsgfVxuXG4ubmItdGhlbWUtY29ycG9yYXRlIDpob3N0IC9kZWVwLyBuZzItc3QtdGJvZHktY3VzdG9tIGEubmcyLXNtYXJ0LWFjdGlvbi5uZzItc21hcnQtYWN0aW9uLWN1c3RvbS1jdXN0b20gaW1nIHtcbiAgd2lkdGg6IDU3JSAhaW1wb3J0YW50O1xuICBtYXJnaW4tbGVmdDogMzRweDsgfVxuXG4ubmItdGhlbWUtY29ycG9yYXRlIDpob3N0IC9kZWVwLyBuZzItc3QtdGJvZHktY3VzdG9tIGEubmcyLXNtYXJ0LWFjdGlvbi5uZzItc21hcnQtYWN0aW9uLWN1c3RvbS1jdXN0b206aG92ZXIge1xuICBjb2xvcjogIzVkY2ZlMzsgfVxuXG4ubmItdGhlbWUtY29ycG9yYXRlIDpob3N0IC9kZWVwLyBuZzItc3QtdGJvZHktZWRpdC1kZWxldGUgaSB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZSAhaW1wb3J0YW50O1xuICB0b3A6IC0xNHB4O1xuICBtYXJnaW4tbGVmdDogNDBweDsgfVxuXG4ubmItdGhlbWUtY29ycG9yYXRlIDpob3N0IGJ1dHRvbiB7XG4gIGZsb2F0OiByaWdodDsgfVxuXG5pbnB1dFt0eXBlPW51bWJlcl06Oi13ZWJraXQtaW5uZXItc3Bpbi1idXR0b24sXG5pbnB1dFt0eXBlPW51bWJlcl06Oi13ZWJraXQtb3V0ZXItc3Bpbi1idXR0b24ge1xuICAtd2Via2l0LWFwcGVhcmFuY2U6IG5vbmU7XG4gIG1hcmdpbjogMDsgfVxuIiwiLyoqXG4gKiBAbGljZW5zZVxuICogQ29weXJpZ2h0IEFrdmVvLiBBbGwgUmlnaHRzIFJlc2VydmVkLlxuICogTGljZW5zZWQgdW5kZXIgdGhlIE1JVCBMaWNlbnNlLiBTZWUgTGljZW5zZS50eHQgaW4gdGhlIHByb2plY3Qgcm9vdCBmb3IgbGljZW5zZSBpbmZvcm1hdGlvbi5cbiAqL1xuXG5AbWl4aW4gbmItc2Nyb2xsYmFycygkZmcsICRiZywgJHNpemUsICRib3JkZXItcmFkaXVzOiAkc2l6ZSAvIDIpIHtcbiAgOjotd2Via2l0LXNjcm9sbGJhciB7XG4gICAgd2lkdGg6ICRzaXplO1xuICAgIGhlaWdodDogJHNpemU7XG4gIH1cblxuICA6Oi13ZWJraXQtc2Nyb2xsYmFyLXRodW1iIHtcbiAgICBiYWNrZ3JvdW5kOiAkZmc7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIGJvcmRlci1yYWRpdXM6ICRib3JkZXItcmFkaXVzO1xuICB9XG5cbiAgOjotd2Via2l0LXNjcm9sbGJhci10cmFjayB7XG4gICAgYmFja2dyb3VuZDogJGJnO1xuICB9XG5cbiAgLy8gVE9ETzogcmVtb3ZlXG4gIC8vIEZvciBJbnRlcm5ldCBFeHBsb3JlclxuICBzY3JvbGxiYXItZmFjZS1jb2xvcjogJGZnO1xuICBzY3JvbGxiYXItdHJhY2stY29sb3I6ICRiZztcbn1cblxuQG1peGluIG5iLXJhZGlhbC1ncmFkaWVudCgkY29sb3ItMSwgJGNvbG9yLTIsICRjb2xvci0zKSB7XG4gIGJhY2tncm91bmQ6ICRjb2xvci0yOyAvKiBPbGQgYnJvd3NlcnMgKi9cbiAgYmFja2dyb3VuZDogLW1vei1yYWRpYWwtZ3JhZGllbnQoYm90dG9tLCBlbGxpcHNlIGNvdmVyLCAkY29sb3ItMSAwJSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkY29sb3ItMiA0NSUsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJGNvbG9yLTMgMTAwJSk7IC8qIEZGMy42LTE1ICovXG4gIGJhY2tncm91bmQ6IC13ZWJraXQtcmFkaWFsLWdyYWRpZW50KGJvdHRvbSwgZWxsaXBzZSBjb3ZlciwgJGNvbG9yLTEgMCUsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJGNvbG9yLTIgNDUlLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRjb2xvci0zIDEwMCUpOyAvKiBDaHJvbWUxMC0yNSxTYWZhcmk1LjEtNiAqL1xuICBiYWNrZ3JvdW5kOiByYWRpYWwtZ3JhZGllbnQoZWxsaXBzZSBhdCBib3R0b20sICRjb2xvci0xIDAlLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRjb2xvci0yIDQ1JSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkY29sb3ItMyAxMDAlKTsgLyogVzNDLCBJRTEwKywgRkYxNissIENocm9tZTI2KywgT3BlcmExMissIFNhZmFyaTcrICovXG4gIGZpbHRlcjogcHJvZ2lkOmR4aW1hZ2V0cmFuc2Zvcm0ubWljcm9zb2Z0LmdyYWRpZW50KHN0YXJ0Q29sb3JzdHI9JyRjb2xvci0xJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZW5kQ29sb3JzdHI9JyRjb2xvci0zJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgR3JhZGllbnRUeXBlPTEpOyAvKiBJRTYtOSBmYWxsYmFjayBvbiBob3Jpem9udGFsIGdyYWRpZW50ICovXG59XG5cbkBtaXhpbiBuYi1yaWdodC1ncmFkaWVudCgkbGVmdC1jb2xvciwgJHJpZ2h0LWNvbG9yKSB7XG4gIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgJGxlZnQtY29sb3IsICRyaWdodC1jb2xvcik7XG59XG5cbkBtaXhpbiBuYi1oZWFkaW5ncygkZnJvbTogMSwgJHRvOiA2KSB7XG4gIEBmb3IgJGkgZnJvbSAkZnJvbSB0aHJvdWdoICR0byB7XG4gICAgaCN7JGl9IHtcbiAgICAgIG1hcmdpbjogMDtcbiAgICB9XG4gIH1cbn1cblxuQG1peGluIGhvdmVyLWZvY3VzLWFjdGl2ZSB7XG4gICY6Zm9jdXMsXG4gICY6YWN0aXZlLFxuICAmOmhvdmVyIHtcbiAgICBAY29udGVudDtcbiAgfVxufVxuXG5AbWl4aW4gY2VudGVyLWhvcml6b250YWwtYWJzb2x1dGUge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIDApO1xuICBsZWZ0OiA1MCU7XG59XG5cbkBtaXhpbiBpbnN0YWxsLXRodW1iKCkge1xuICAkdGh1bWItc2VsZWN0b3JzOiAoXG4gICAgJzo6LXdlYmtpdC1zbGlkZXItdGh1bWInXG4gICAgJzo6LW1vei1yYW5nZS10aHVtYidcbiAgICAnOjotbXMtdGh1bWInXG4gICk7XG5cbiAgQGVhY2ggJHNlbGVjdG9yIGluICR0aHVtYi1zZWxlY3RvcnMge1xuICAgICYjeyRzZWxlY3Rvcn0ge1xuICAgICAgLXdlYmtpdC1hcHBlYXJhbmNlOiBub25lO1xuICAgICAgLW1vei1hcHBlYXJhbmNlOiBub25lO1xuICAgICAgQGNvbnRlbnQ7XG4gICAgfVxuICB9XG59XG5cbkBtaXhpbiBpbnN0YWxsLXRyYWNrKCkge1xuICAkdGh1bWItc2VsZWN0b3JzOiAoXG4gICAgJzo6LXdlYmtpdC1zbGlkZXItcnVubmFibGUtdHJhY2snXG4gICAgJzo6LW1vei1yYW5nZS10cmFjaydcbiAgICAnOjotbXMtdHJhY2snXG4gICk7XG5cbiAgQGVhY2ggJHNlbGVjdG9yIGluICR0aHVtYi1zZWxlY3RvcnMge1xuICAgICYjeyRzZWxlY3Rvcn0ge1xuICAgICAgLXdlYmtpdC1hcHBlYXJhbmNlOiBub25lO1xuICAgICAgLW1vei1hcHBlYXJhbmNlOiBub25lO1xuICAgICAgQGNvbnRlbnQ7XG4gICAgfVxuICB9XG59XG5cbkBtaXhpbiBpbnN0YWxsLXBsYWNlaG9sZGVyKCRjb2xvciwgJGZvbnQtc2l6ZSkge1xuICAkcGxhY2Vob2xkZXItc2VsZWN0b3JzOiAoXG4gICAgJzo6LXdlYmtpdC1pbnB1dC1wbGFjZWhvbGRlcidcbiAgICAnOjotbW96LXBsYWNlaG9sZGVyJ1xuICAgICc6LW1vei1wbGFjZWhvbGRlcidcbiAgICAnOi1tcy1pbnB1dC1wbGFjZWhvbGRlcidcbiAgKTtcblxuICAmOjpwbGFjZWhvbGRlciB7XG4gICAgQGluY2x1ZGUgcGxhY2Vob2xkZXIoJGNvbG9yLCAkZm9udC1zaXplKTtcbiAgfVxuXG4gIEBlYWNoICRzZWxlY3RvciBpbiAkcGxhY2Vob2xkZXItc2VsZWN0b3JzIHtcbiAgICAmI3skc2VsZWN0b3J9IHtcbiAgICAgIEBpbmNsdWRlIHBsYWNlaG9sZGVyKCRjb2xvciwgJGZvbnQtc2l6ZSk7XG4gICAgfVxuXG4gICAgJjpmb2N1cyN7JHNlbGVjdG9yfSB7XG4gICAgICBAaW5jbHVkZSBwbGFjZWhvbGRlci1mb2N1cygpO1xuICAgIH1cbiAgfVxufVxuXG5AbWl4aW4gcGxhY2Vob2xkZXIoJGNvbG9yLCAkZm9udC1zaXplKSB7XG4gIGNvbG9yOiAkY29sb3I7XG4gIGZvbnQtc2l6ZTogJGZvbnQtc2l6ZTtcbiAgb3BhY2l0eTogMTtcbiAgdHJhbnNpdGlvbjogb3BhY2l0eSAwLjNzIGVhc2U7XG4gIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xufVxuXG5AbWl4aW4gcGxhY2Vob2xkZXItZm9jdXMoKSB7XG4gIG9wYWNpdHk6IDA7XG4gIHRyYW5zaXRpb246IG9wYWNpdHkgMC4zcyBlYXNlO1xufVxuXG5AbWl4aW4gYW5pbWF0aW9uKCRhbmltYXRlLi4uKSB7XG4gICRtYXg6IGxlbmd0aCgkYW5pbWF0ZSk7XG4gICRhbmltYXRpb25zOiAnJztcblxuICBAZm9yICRpIGZyb20gMSB0aHJvdWdoICRtYXgge1xuICAgICRhbmltYXRpb25zOiAjeyRhbmltYXRpb25zICsgbnRoKCRhbmltYXRlLCAkaSl9O1xuXG4gICAgQGlmICRpIDwgJG1heCB7XG4gICAgICAkYW5pbWF0aW9uczogI3skYW5pbWF0aW9ucyArICcsICd9O1xuICAgIH1cbiAgfVxuICAtd2Via2l0LWFuaW1hdGlvbjogJGFuaW1hdGlvbnM7XG4gIC1tb3otYW5pbWF0aW9uOiAgICAkYW5pbWF0aW9ucztcbiAgLW8tYW5pbWF0aW9uOiAgICAgICRhbmltYXRpb25zO1xuICBhbmltYXRpb246ICAgICAgICAgJGFuaW1hdGlvbnM7XG59XG5cbkBtaXhpbiBrZXlmcmFtZXMoJGFuaW1hdGlvbk5hbWUpIHtcbiAgQC13ZWJraXQta2V5ZnJhbWVzICN7JGFuaW1hdGlvbk5hbWV9IHtcbiAgICBAY29udGVudDtcbiAgfVxuICBALW1vei1rZXlmcmFtZXMgI3skYW5pbWF0aW9uTmFtZX0ge1xuICAgIEBjb250ZW50O1xuICB9XG4gIEAtby1rZXlmcmFtZXMgI3skYW5pbWF0aW9uTmFtZX0ge1xuICAgIEBjb250ZW50O1xuICB9XG4gIEBrZXlmcmFtZXMgI3skYW5pbWF0aW9uTmFtZX0ge1xuICAgIEBjb250ZW50O1xuICB9XG59XG5cbi8qKlxuICogVGhpcyBtaXhpbiBnZW5lcmF0ZXMga2V5ZmFtZXMuXG4gKiBCZWNhdXNlIG9mIGFsbCBrZXlmcmFtZXMgY2FuJ3QgYmUgc2NvcGVkLFxuICogd2UgbmVlZCB0byBwdXRzIHVuaXF1ZSBuYW1lIGluIGVhY2ggYnRuLXB1bHNlIGNhbGwuXG4gKi9cbkBtaXhpbiBidG4tcHVsc2UoJG5hbWUsICRjb2xvcikge1xuICAmLmJ0bi1wdWxzZSB7XG4gICAgQGluY2x1ZGUgYW5pbWF0aW9uKGJ0bi0jeyRuYW1lfS1wdWxzZSAxLjVzIGluZmluaXRlKTtcbiAgfVxuXG4gIEBpbmNsdWRlIGtleWZyYW1lcyhidG4tI3skbmFtZX0tcHVsc2UpIHtcbiAgICAwJSB7XG4gICAgICBib3gtc2hhZG93OiBub25lO1xuICAgICAgb3BhY2l0eTogbmItdGhlbWUoYnRuLWRpc2FibGVkLW9wYWNpdHkpO1xuICAgIH1cbiAgICA1MCUge1xuICAgICAgYm94LXNoYWRvdzogMCAwIDFyZW0gMCAkY29sb3I7XG4gICAgICBvcGFjaXR5OiAwLjg7XG4gICAgfVxuICAgIDEwMCUge1xuICAgICAgYm94LXNoYWRvdzogbm9uZTtcbiAgICAgIG9wYWNpdHk6IG5iLXRoZW1lKGJ0bi1kaXNhYmxlZC1vcGFjaXR5KTtcbiAgICB9XG4gIH1cbn1cblxuLypcblxuQWNjb3JkaW5nIHRvIHRoZSBzcGVjaWZpY2F0aW9uIChodHRwczovL3d3dy53My5vcmcvVFIvY3NzLXNjb3BpbmctMS8jaG9zdC1zZWxlY3Rvcilcbjpob3N0IGFuZCA6aG9zdC1jb250ZXh0IGFyZSBwc2V1ZG8tY2xhc3Nlcy4gU28gd2UgYXNzdW1lIHRoZXkgY291bGQgYmUgY29tYmluZWQsXG5saWtlIG90aGVyIHBzZXVkby1jbGFzc2VzLCBldmVuIHNhbWUgb25lcy5cbkZvciBleGFtcGxlOiAnOm50aC1vZi10eXBlKDJuKTpudGgtb2YtdHlwZShldmVuKScuXG5cbklkZWFsIHNvbHV0aW9uIHdvdWxkIGJlIHRvIHByZXBlbmQgYW55IHNlbGVjdG9yIHdpdGggOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pLlxuVGhlbiBuZWJ1bGFyIGNvbXBvbmVudHMgd2lsbCBiZWhhdmUgYXMgYW4gaHRtbCBlbGVtZW50IGFuZCByZXNwb25kIHRvIFtkaXJdIGF0dHJpYnV0ZSBvbiBhbnkgbGV2ZWwsXG5zbyBkaXJlY3Rpb24gY291bGQgYmUgb3ZlcnJpZGRlbiBvbiBhbnkgY29tcG9uZW50IGxldmVsLlxuXG5JbXBsZW1lbnRhdGlvbiBjb2RlOlxuXG5AbWl4aW4gbmItcnRsKCkge1xuICAvLyBhZGQgIyB0byBzY3NzIGludGVycG9sYXRpb24gc3RhdGVtZW50LlxuICAvLyBpdCB3b3JrcyBpbiBjb21tZW50cyBhbmQgd2UgY2FuJ3QgdXNlIGl0IGhlcmVcbiAgQGF0LXJvb3Qge3NlbGVjdG9yLWFwcGVuZCgnOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pJywgJil9IHtcbiAgICBAY29udGVudDtcbiAgfVxufVxuXG5BbmQgd2hlbiB3ZSBjYWxsIGl0IHNvbWV3aGVyZTpcblxuOmhvc3Qge1xuICAuc29tZS1jbGFzcyB7XG4gICAgQGluY2x1ZGUgbmItcnRsKCkge1xuICAgICAgLi4uXG4gICAgfVxuICB9XG59XG46aG9zdC1jb250ZXh0KC4uLikge1xuICAuc29tZS1jbGFzcyB7XG4gICAgQGluY2x1ZGUgbmItcnRsKCkge1xuICAgICAgLi4uXG4gICAgfVxuICB9XG59XG5cblJlc3VsdCB3aWxsIGxvb2sgbGlrZTpcblxuOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pOmhvc3QgLnNvbWUtY2xhc3Mge1xuICAuLi5cbn1cbjpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKTpob3N0LWNvbnRleHQoLi4uKSAuc29tZS1jbGFzcyB7XG4gIC4uLlxufVxuXG4qXG4gIFNpZGUgbm90ZTpcbiAgOmhvc3QtY29udGV4dCgpOmhvc3Qgc2VsZWN0b3IgYXJlIHZhbGlkLiBodHRwczovL2xpc3RzLnczLm9yZy9BcmNoaXZlcy9QdWJsaWMvd3d3LXN0eWxlLzIwMTVGZWIvMDMwNS5odG1sXG5cbiAgOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pOmhvc3QtY29udGV4dCguLi4pIHNob3VsZCBtYXRjaCBhbnkgcGVybXV0YXRpb24sXG4gIHNvIG9yZGVyIGlzIG5vdCBpbXBvcnRhbnQuXG4qXG5cblxuQ3VycmVudGx5LCB0aGVyZSdyZSB0d28gcHJvYmxlbXMgd2l0aCB0aGlzIGFwcHJvYWNoOlxuXG5GaXJzdCwgaXMgdGhhdCB3ZSBjYW4ndCBjb21iaW5lIDpob3N0LCA6aG9zdC1jb250ZXh0LiBBbmd1bGFyIGJ1Z3MgIzE0MzQ5LCAjMTkxOTkuXG5Gb3IgdGhlIG1vbWVudCBvZiB3cml0aW5nLCB0aGUgb25seSBwb3NzaWJsZSB3YXkgaXM6XG46aG9zdCB7XG4gIDpob3N0LWNvbnRleHQoLi4uKSB7XG4gICAgLi4uXG4gIH1cbn1cbkl0IGRvZXNuJ3Qgd29yayBmb3IgdXMgYmVjYXVzZSBtaXhpbiBjb3VsZCBiZSBjYWxsZWQgc29tZXdoZXJlIGRlZXBlciwgbGlrZTpcbjpob3N0IHtcbiAgcCB7XG4gICAgQGluY2x1ZGUgbmItcnRsKCkgeyAuLi4gfVxuICB9XG59XG5XZSBhcmUgbm90IGFibGUgdG8gZ28gdXAgdG8gOmhvc3QgbGV2ZWwgdG8gcGxhY2UgY29udGVudCBwYXNzZWQgdG8gbWl4aW4uXG5cblRoZSBzZWNvbmQgcHJvYmxlbSBpcyB0aGF0IHdlIG9ubHkgY2FuIGJlIHN1cmUgdGhhdCB3ZSBhcHBlbmRpbmcgOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pIHRvIGFub3RoZXJcbjpob3N0Lzpob3N0LWNvbnRleHQgcHNldWRvLWNsYXNzIHdoZW4gY2FsbGVkIGluIHRoZW1lIGZpbGVzICgqLnRoZW1lLnNjc3MpLlxuICAqXG4gICAgU2lkZSBub3RlOlxuICAgIEN1cnJlbnRseSwgbmItaW5zdGFsbC1jb21wb25lbnQgdXNlcyBhbm90aGVyIGFwcHJvYWNoIHdoZXJlIDpob3N0IHByZXBlbmRlZCB3aXRoIHRoZSB0aGVtZSBuYW1lXG4gICAgKGh0dHBzOi8vZ2l0aHViLmNvbS9hbmd1bGFyL2FuZ3VsYXIvYmxvYi81Yjk2MDc4NjI0YjBhNDc2MGYyZGJjZjZmZGYwYmQ2Mjc5MWJlNWJiL3BhY2thZ2VzL2NvbXBpbGVyL3NyYy9zaGFkb3dfY3NzLnRzI0w0NDEpLFxuICAgIGJ1dCBpdCB3YXMgbWFkZSB0byBiZSBhYmxlIHRvIHVzZSBjdXJyZW50IHJlYWxpemF0aW9uIG9mIHJ0bCBhbmQgaXQgY2FuIGJlIHJld3JpdHRlbiBiYWNrIHRvXG4gICAgOmhvc3QtY29udGV4dCgkdGhlbWUpIG9uY2Ugd2Ugd2lsbCBiZSBhYmxlIHRvIHVzZSBtdWx0aXBsZSBzaGFkb3cgc2VsZWN0b3JzLlxuICAqXG5CdXQgd2hlbiBpdCdzIGNhbGxlZCBpbiAqLmNvbXBvbmVudC5zY3NzIHdlIGNhbid0IGJlIHN1cmUsIHRoYXQgc2VsZWN0b3Igc3RhcnRzIHdpdGggOmhvc3QvOmhvc3QtY29udGV4dCxcbmJlY2F1c2UgYW5ndWxhciBhbGxvd3Mgb21pdHRpbmcgcHNldWRvLWNsYXNzZXMgaWYgd2UgZG9uJ3QgbmVlZCB0byBzdHlsZSA6aG9zdCBjb21wb25lbnQgaXRzZWxmLlxuV2UgY2FuIGJyZWFrIHN1Y2ggc2VsZWN0b3JzLCBieSBqdXN0IGFwcGVuZGluZyA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkgdG8gdGhlbS5cbiAgKioqXG4gICAgUG9zc2libGUgc29sdXRpb25cbiAgICBjaGVjayBpZiB3ZSBpbiB0aGVtZSBieSBzb21lIHRoZW1lIHZhcmlhYmxlcyBhbmQgaWYgc28gYXBwZW5kLCBvdGhlcndpc2UgbmVzdCBsaWtlXG4gICAgQGF0LXJvb3QgOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pIHtcbiAgICAgIC8vIGFkZCAjIHRvIHNjc3MgaW50ZXJwb2xhdGlvbiBzdGF0ZW1lbnQuXG4gICAgICAvLyBpdCB3b3JrcyBpbiBjb21tZW50cyBhbmQgd2UgY2FuJ3QgdXNlIGl0IGhlcmVcbiAgICAgIHsmfSB7XG4gICAgICAgIEBjb250ZW50O1xuICAgICAgfVxuICAgIH1cbiAgICBXaGF0IGlmIDpob3N0IHNwZWNpZmllZD8gQ2FuIHdlIGFkZCBzcGFjZSBpbiA6aG9zdC1jb250ZXh0KC4uLikgOmhvc3Q/XG4gICAgT3IgbWF5YmUgYWRkIDpob3N0IHNlbGVjdG9yIGFueXdheT8gSWYgbXVsdGlwbGUgOmhvc3Qgc2VsZWN0b3JzIGFyZSBhbGxvd2VkXG4gICoqKlxuXG5cblByb2JsZW1zIHdpdGggdGhlIGN1cnJlbnQgYXBwcm9hY2guXG5cbjEuIERpcmVjdGlvbiBjYW4gYmUgYXBwbGllZCBvbmx5IG9uIGRvY3VtZW50IGxldmVsLCBiZWNhdXNlIG1peGluIHByZXBlbmRzIHRoZW1lIGNsYXNzLFxud2hpY2ggcGxhY2VkIG9uIHRoZSBib2R5LlxuMi4gKi5jb21wb25lbnQuc2NzcyBzdHlsZXMgc2hvdWxkIGJlIGluIDpob3N0IHNlbGVjdG9yLiBPdGhlcndpc2UgYW5ndWxhciB3aWxsIGFkZCBob3N0XG5hdHRyaWJ1dGUgdG8gW2Rpcj1ydGxdIGF0dHJpYnV0ZSBhcyB3ZWxsLlxuXG5cbkdlbmVyYWwgcHJvYmxlbXMuXG5cbkx0ciBpcyBkZWZhdWx0IGRvY3VtZW50IGRpcmVjdGlvbiwgYnV0IGZvciBwcm9wZXIgd29yayBvZiBuYi1sdHIgKG1lYW5zIGx0ciBvbmx5KSxcbltkaXI9bHRyXSBzaG91bGQgYmUgc3BlY2lmaWVkIGF0IGxlYXN0IHNvbWV3aGVyZS4gJzpub3QoW2Rpcj1ydGxdJyBub3QgYXBwbGljYWJsZSBoZXJlLFxuYmVjYXVzZSBpdCdzIHNhdGlzZnkgYW55IHBhcmVudCwgdGhhdCBkb24ndCBoYXZlIFtkaXI9cnRsXSBhdHRyaWJ1dGUuXG5QcmV2aW91cyBhcHByb2FjaCB3YXMgdG8gdXNlIHNpbmdsZSBydGwgbWl4aW4gYW5kIHJlc2V0IGx0ciBwcm9wZXJ0aWVzIHRvIGluaXRpYWwgdmFsdWUuXG5CdXQgc29tZXRpbWVzIGl0J3MgaGFyZCB0byBmaW5kLCB3aGF0IHRoZSBwcmV2aW91cyB2YWx1ZSBzaG91bGQgYmUuIEFuZCBzdWNoIG1peGluIGNhbGwgbG9va3MgdG9vIHZlcmJvc2UuXG4qL1xuXG5AbWl4aW4gX3ByZXBlbmQtd2l0aC1zZWxlY3Rvcigkc2VsZWN0b3IsICRwcm9wOiBudWxsLCAkdmFsdWU6IG51bGwpIHtcbiAgI3skc2VsZWN0b3J9ICYge1xuICAgIEBpZiAkcHJvcCAhPSBudWxsIHtcbiAgICAgICN7JHByb3B9OiAkdmFsdWU7XG4gICAgfVxuXG4gICAgQGNvbnRlbnQ7XG4gIH1cbn1cblxuQG1peGluIG5iLWx0cigkcHJvcDogbnVsbCwgJHZhbHVlOiBudWxsKSB7XG4gIEBpbmNsdWRlIF9wcmVwZW5kLXdpdGgtc2VsZWN0b3IoJ1tkaXI9bHRyXScsICRwcm9wLCAkdmFsdWUpIHtcbiAgICBAY29udGVudDtcbiAgfVxufVxuXG5AbWl4aW4gbmItcnRsKCRwcm9wOiBudWxsLCAkdmFsdWU6IG51bGwpIHtcbiAgQGluY2x1ZGUgX3ByZXBlbmQtd2l0aC1zZWxlY3RvcignW2Rpcj1ydGxdJywgJHByb3AsICR2YWx1ZSkge1xuICAgIEBjb250ZW50O1xuICB9O1xufVxuIiwiLyoqXG4gKiBAbGljZW5zZVxuICogQ29weXJpZ2h0IEFrdmVvLiBBbGwgUmlnaHRzIFJlc2VydmVkLlxuICogTGljZW5zZWQgdW5kZXIgdGhlIE1JVCBMaWNlbnNlLiBTZWUgTGljZW5zZS50eHQgaW4gdGhlIHByb2plY3Qgcm9vdCBmb3IgbGljZW5zZSBpbmZvcm1hdGlvbi5cbiAqL1xuXG4vLy8gU2xpZ2h0bHkgbGlnaHRlbiBhIGNvbG9yXG4vLy8gQGFjY2VzcyBwdWJsaWNcbi8vLyBAcGFyYW0ge0NvbG9yfSAkY29sb3IgLSBjb2xvciB0byB0aW50XG4vLy8gQHBhcmFtIHtOdW1iZXJ9ICRwZXJjZW50YWdlIC0gcGVyY2VudGFnZSBvZiBgJGNvbG9yYCBpbiByZXR1cm5lZCBjb2xvclxuLy8vIEByZXR1cm4ge0NvbG9yfVxuQGZ1bmN0aW9uIHRpbnQoJGNvbG9yLCAkcGVyY2VudGFnZSkge1xuICBAcmV0dXJuIG1peCh3aGl0ZSwgJGNvbG9yLCAkcGVyY2VudGFnZSk7XG59XG5cbi8vLyBTbGlnaHRseSBkYXJrZW4gYSBjb2xvclxuLy8vIEBhY2Nlc3MgcHVibGljXG4vLy8gQHBhcmFtIHtDb2xvcn0gJGNvbG9yIC0gY29sb3IgdG8gc2hhZGVcbi8vLyBAcGFyYW0ge051bWJlcn0gJHBlcmNlbnRhZ2UgLSBwZXJjZW50YWdlIG9mIGAkY29sb3JgIGluIHJldHVybmVkIGNvbG9yXG4vLy8gQHJldHVybiB7Q29sb3J9XG5AZnVuY3Rpb24gc2hhZGUoJGNvbG9yLCAkcGVyY2VudGFnZSkge1xuICBAcmV0dXJuIG1peChibGFjaywgJGNvbG9yLCAkcGVyY2VudGFnZSk7XG59XG5cbkBmdW5jdGlvbiBtYXAtc2V0KCRtYXAsICRrZXksICR2YWx1ZTogbnVsbCkge1xuICAkbmV3OiAoJGtleTogJHZhbHVlKTtcbiAgQHJldHVybiBtYXAtbWVyZ2UoJG1hcCwgJG5ldyk7XG59XG4iLCIvKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG5cbkBpbXBvcnQgJy4uL2NvcmUvZnVuY3Rpb25zJztcbkBpbXBvcnQgJy4uL2NvcmUvbWl4aW5zJztcblxuJHRoZW1lOiAoXG4gIGZvbnQtbWFpbjogdW5xdW90ZSgnXCJTZWdvZSBVSVwiLCBSb2JvdG8sIFwiSGVsdmV0aWNhIE5ldWVcIiwgQXJpYWwsIHNhbnMtc2VyaWYnKSxcbiAgZm9udC1zZWNvbmRhcnk6IGZvbnQtbWFpbixcblxuICBmb250LXdlaWdodC10aGluOiAyMDAsXG4gIGZvbnQtd2VpZ2h0LWxpZ2h0OiAzMDAsXG4gIGZvbnQtd2VpZ2h0LW5vcm1hbDogNDAwLFxuICBmb250LXdlaWdodC1ib2xkZXI6IDUwMCxcbiAgZm9udC13ZWlnaHQtYm9sZDogNjAwLFxuICBmb250LXdlaWdodC11bHRyYS1ib2xkOiA4MDAsXG5cbiAgLy8gVE9ETzogdXNlIGl0IGFzIGEgZGVmYXVsdCBmb250LXNpemVcbiAgYmFzZS1mb250LXNpemU6IDE2cHgsXG5cbiAgZm9udC1zaXplLXhsZzogMS4yNXJlbSxcbiAgZm9udC1zaXplLWxnOiAxLjEyNXJlbSxcbiAgZm9udC1zaXplOiAxcmVtLFxuICBmb250LXNpemUtc206IDAuODc1cmVtLFxuICBmb250LXNpemUteHM6IDAuNzVyZW0sXG5cbiAgcmFkaXVzOiAwLjM3NXJlbSxcbiAgcGFkZGluZzogMS4yNXJlbSxcbiAgbWFyZ2luOiAxLjVyZW0sXG4gIGxpbmUtaGVpZ2h0OiAxLjI1LFxuXG4gIGNvbG9yLWJnOiAjZmZmZmZmLFxuICBjb2xvci1iZy1hY3RpdmU6ICNlOWVkZjIsXG4gIGNvbG9yLWZnOiAjYTRhYmIzLFxuICBjb2xvci1mZy1oZWFkaW5nOiAjMmEyYTJhLFxuICBjb2xvci1mZy10ZXh0OiAjNGI0YjRiLFxuICBjb2xvci1mZy1oaWdobGlnaHQ6ICM0MGRjN2UsXG5cbiAgc2VwYXJhdG9yOiAjZWJlZWYyLFxuXG4gIGNvbG9yLWdyYXk6IHJnYmEoODEsIDExMywgMTY1LCAwLjE1KSxcbiAgY29sb3ItbmV1dHJhbDogdHJhbnNwYXJlbnQsXG4gIGNvbG9yLXdoaXRlOiAjZmZmZmZmLFxuICBjb2xvci1kaXNhYmxlZDogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjQpLFxuXG4gIGNvbG9yLXByaW1hcnk6ICM4YTdmZmYsXG4gIGNvbG9yLXN1Y2Nlc3M6ICM0MGRjN2UsXG4gIGNvbG9yLWluZm86ICM0Y2E2ZmYsXG4gIGNvbG9yLXdhcm5pbmc6ICNmZmExMDAsXG4gIGNvbG9yLWRhbmdlcjogI2ZmNGM2YSxcblxuICAvLyBUT0RPOiBtb3ZlIHRvIGNvbnN0YW50c1xuICBzb2NpYWwtY29sb3ItZmFjZWJvb2s6ICMzYjU5OTgsXG4gIHNvY2lhbC1jb2xvci10d2l0dGVyOiAjNTVhY2VlLFxuICBzb2NpYWwtY29sb3ItZ29vZ2xlOiAjZGQ0YjM5LFxuICBzb2NpYWwtY29sb3ItbGlua2VkaW46ICMwMTc3YjUsXG4gIHNvY2lhbC1jb2xvci1naXRodWI6ICM2YjZiNmIsXG4gIHNvY2lhbC1jb2xvci1zdGFja292ZXJmbG93OiAjMmY5NmU4LFxuICBzb2NpYWwtY29sb3ItZHJpYmJsZTogI2YyNjc5OCxcbiAgc29jaWFsLWNvbG9yLWJlaGFuY2U6ICMwMDkzZmEsXG5cbiAgYm9yZGVyLWNvbG9yOiBjb2xvci1ncmF5LFxuICBzaGFkb3c6IDAgMnB4IDEycHggMCAjZGZlM2ViLFxuXG4gIGxpbmstY29sb3I6ICMzZGNjNmQsXG4gIGxpbmstY29sb3ItaG92ZXI6ICMyZWU1NmIsXG4gIGxpbmstY29sb3ItdmlzaXRlZDogbGluay1jb2xvcixcblxuICBzY3JvbGxiYXItZmc6ICNkYWRhZGEsXG4gIHNjcm9sbGJhci1iZzogI2YyZjJmMixcbiAgc2Nyb2xsYmFyLXdpZHRoOiA1cHgsXG4gIHNjcm9sbGJhci10aHVtYi1yYWRpdXM6IDIuNXB4LFxuXG4gIHJhZGlhbC1ncmFkaWVudDogbm9uZSxcbiAgbGluZWFyLWdyYWRpZW50OiBub25lLFxuXG4gIGNhcmQtZm9udC1zaXplOiBmb250LXNpemUsXG4gIGNhcmQtbGluZS1oZWlnaHQ6IGxpbmUtaGVpZ2h0LFxuICBjYXJkLWZvbnQtd2VpZ2h0OiBmb250LXdlaWdodC1ub3JtYWwsXG4gIGNhcmQtZmc6IGNvbG9yLWZnLCAvLyBUT0RPOiBub3QgdXNlZFxuICBjYXJkLWZnLXRleHQ6IGNvbG9yLWZnLXRleHQsXG4gIGNhcmQtZmctaGVhZGluZzogY29sb3ItZmctaGVhZGluZywgLy8gVE9ETzogbm90IHVzZWRcbiAgY2FyZC1iZzogY29sb3ItYmcsXG4gIGNhcmQtaGVpZ2h0LXh4c21hbGw6IDk2cHgsXG4gIGNhcmQtaGVpZ2h0LXhzbWFsbDogMjE2cHgsXG4gIGNhcmQtaGVpZ2h0LXNtYWxsOiAzMzZweCxcbiAgY2FyZC1oZWlnaHQtbWVkaXVtOiA0NTZweCxcbiAgY2FyZC1oZWlnaHQtbGFyZ2U6IDU3NnB4LFxuICBjYXJkLWhlaWdodC14bGFyZ2U6IDY5NnB4LFxuICBjYXJkLWhlaWdodC14eGxhcmdlOiA4MTZweCxcbiAgY2FyZC1zaGFkb3c6IHNoYWRvdyxcbiAgY2FyZC1ib3JkZXItd2lkdGg6IDAsXG4gIGNhcmQtYm9yZGVyLXR5cGU6IHNvbGlkLFxuICBjYXJkLWJvcmRlci1jb2xvcjogY29sb3ItYmcsXG4gIGNhcmQtYm9yZGVyLXJhZGl1czogcmFkaXVzLFxuICBjYXJkLXBhZGRpbmc6IHBhZGRpbmcsXG4gIGNhcmQtbWFyZ2luOiBtYXJnaW4sXG4gIGNhcmQtaGVhZGVyLWZvbnQtZmFtaWx5OiBmb250LXNlY29uZGFyeSxcbiAgY2FyZC1oZWFkZXItZm9udC1zaXplOiBmb250LXNpemUtbGcsXG4gIGNhcmQtaGVhZGVyLWZvbnQtd2VpZ2h0OiBmb250LXdlaWdodC1ib2xkLFxuICBjYXJkLXNlcGFyYXRvcjogc2VwYXJhdG9yLFxuICBjYXJkLWhlYWRlci1mZzogY29sb3ItZmcsIC8vIFRPRE86IG5vdCB1c2VkXG4gIGNhcmQtaGVhZGVyLWZnLWhlYWRpbmc6IGNvbG9yLWZnLWhlYWRpbmcsXG4gIGNhcmQtaGVhZGVyLWFjdGl2ZS1iZzogY29sb3ItZmcsXG4gIGNhcmQtaGVhZGVyLWFjdGl2ZS1mZzogY29sb3ItYmcsXG4gIGNhcmQtaGVhZGVyLWRpc2FibGVkLWJnOiBjb2xvci1kaXNhYmxlZCxcbiAgY2FyZC1oZWFkZXItcHJpbWFyeS1iZzogY29sb3ItcHJpbWFyeSxcbiAgY2FyZC1oZWFkZXItaW5mby1iZzogY29sb3ItaW5mbyxcbiAgY2FyZC1oZWFkZXItc3VjY2Vzcy1iZzogY29sb3Itc3VjY2VzcyxcbiAgY2FyZC1oZWFkZXItd2FybmluZy1iZzogY29sb3Itd2FybmluZyxcbiAgY2FyZC1oZWFkZXItZGFuZ2VyLWJnOiBjb2xvci1kYW5nZXIsXG4gIGNhcmQtaGVhZGVyLWJvcmRlci13aWR0aDogMXB4LFxuICBjYXJkLWhlYWRlci1ib3JkZXItdHlwZTogc29saWQsXG4gIGNhcmQtaGVhZGVyLWJvcmRlci1jb2xvcjogY2FyZC1zZXBhcmF0b3IsXG5cbiAgaGVhZGVyLWZvbnQtZmFtaWx5OiBmb250LXNlY29uZGFyeSxcbiAgaGVhZGVyLWZvbnQtc2l6ZTogZm9udC1zaXplLFxuICBoZWFkZXItbGluZS1oZWlnaHQ6IGxpbmUtaGVpZ2h0LFxuICBoZWFkZXItZmc6IGNvbG9yLWZnLWhlYWRpbmcsXG4gIGhlYWRlci1iZzogY29sb3ItYmcsXG4gIGhlYWRlci1oZWlnaHQ6IDQuNzVyZW0sXG4gIGhlYWRlci1wYWRkaW5nOiAxLjI1cmVtLFxuICBoZWFkZXItc2hhZG93OiBzaGFkb3csXG5cbiAgZm9vdGVyLWhlaWdodDogNC43MjVyZW0sXG4gIGZvb3Rlci1wYWRkaW5nOiAxLjI1cmVtLFxuICBmb290ZXItZmc6IGNvbG9yLWZnLWhlYWRpbmcsXG4gIGZvb3Rlci1mZy1oaWdobGlnaHQ6IGNvbG9yLWZnLWhlYWRpbmcsXG4gIGZvb3Rlci1iZzogY29sb3ItYmcsXG4gIGZvb3Rlci1zZXBhcmF0b3I6IHNlcGFyYXRvcixcbiAgZm9vdGVyLXNoYWRvdzogc2hhZG93LFxuXG4gIGxheW91dC1mb250LWZhbWlseTogZm9udC1tYWluLFxuICBsYXlvdXQtZm9udC1zaXplOiBmb250LXNpemUsXG4gIGxheW91dC1saW5lLWhlaWdodDogbGluZS1oZWlnaHQsXG4gIGxheW91dC1mZzogY29sb3ItZmcsXG4gIGxheW91dC1iZzogI2ViZWZmNSxcbiAgbGF5b3V0LW1pbi1oZWlnaHQ6IDEwMHZoLFxuICBsYXlvdXQtY29udGVudC13aWR0aDogOTAwcHgsXG4gIGxheW91dC13aW5kb3ctbW9kZS1taW4td2lkdGg6IDMwMHB4LFxuICBsYXlvdXQtd2luZG93LW1vZGUtbWF4LXdpZHRoOiAxOTIwcHgsXG4gIGxheW91dC13aW5kb3ctbW9kZS1iZzogbGF5b3V0LWJnLFxuICBsYXlvdXQtd2luZG93LW1vZGUtcGFkZGluZy10b3A6IDQuNzVyZW0sXG4gIGxheW91dC13aW5kb3ctc2hhZG93OiBzaGFkb3csXG4gIGxheW91dC1wYWRkaW5nOiAyLjI1cmVtIDIuMjVyZW0gMC43NXJlbSxcbiAgbGF5b3V0LW1lZGl1bS1wYWRkaW5nOiAxLjVyZW0gMS41cmVtIDAuNXJlbSxcbiAgbGF5b3V0LXNtYWxsLXBhZGRpbmc6IDFyZW0gMXJlbSAwLFxuXG4gIHNpZGViYXItZm9udC1zaXplOiBmb250LXNpemUsXG4gIHNpZGViYXItbGluZS1oZWlnaHQ6IGxpbmUtaGVpZ2h0LFxuICBzaWRlYmFyLWZnOiBjb2xvci1mZy1oZWFkaW5nLFxuICBzaWRlYmFyLWJnOiBjb2xvci1iZyxcbiAgc2lkZWJhci1oZWlnaHQ6IDEwMHZoLFxuICBzaWRlYmFyLXdpZHRoOiAxNnJlbSxcbiAgc2lkZWJhci13aWR0aC1jb21wYWN0OiAzLjVyZW0sXG4gIHNpZGViYXItcGFkZGluZzogcGFkZGluZyxcbiAgc2lkZWJhci1oZWFkZXItaGVpZ2h0OiAzLjVyZW0sXG4gIHNpZGViYXItZm9vdGVyLWhlaWdodDogMy41cmVtLFxuICBzaWRlYmFyLXNoYWRvdzogc2hhZG93LFxuXG4gIG1lbnUtZm9udC1mYW1pbHk6IGZvbnQtc2Vjb25kYXJ5LFxuICBtZW51LWZvbnQtc2l6ZTogZm9udC1zaXplLFxuICBtZW51LWZvbnQtd2VpZ2h0OiBmb250LXdlaWdodC1ib2xkZXIsXG4gIG1lbnUtZmc6IGNvbG9yLWZnLXRleHQsXG4gIG1lbnUtYmc6IGNvbG9yLWJnLFxuICBtZW51LWFjdGl2ZS1mZzogY29sb3ItZmctaGVhZGluZyxcbiAgbWVudS1hY3RpdmUtYmc6IGNvbG9yLWJnLFxuICBtZW51LWFjdGl2ZS1mb250LXdlaWdodDogZm9udC13ZWlnaHQtYm9sZCxcblxuICBtZW51LXN1Ym1lbnUtYmc6IGNvbG9yLWJnLFxuICBtZW51LXN1Ym1lbnUtZmc6IGNvbG9yLWZnLXRleHQsXG4gIG1lbnUtc3VibWVudS1hY3RpdmUtZmc6IGNvbG9yLWZnLWhlYWRpbmcsXG4gIG1lbnUtc3VibWVudS1hY3RpdmUtYmc6IGNvbG9yLWJnLFxuICBtZW51LXN1Ym1lbnUtYWN0aXZlLWJvcmRlci1jb2xvcjogY29sb3ItZmctaGlnaGxpZ2h0LFxuICBtZW51LXN1Ym1lbnUtYWN0aXZlLXNoYWRvdzogbm9uZSxcbiAgbWVudS1zdWJtZW51LWhvdmVyLWZnOiBtZW51LXN1Ym1lbnUtYWN0aXZlLWZnLFxuICBtZW51LXN1Ym1lbnUtaG92ZXItYmc6IG1lbnUtc3VibWVudS1iZyxcbiAgbWVudS1zdWJtZW51LWl0ZW0tYm9yZGVyLXdpZHRoOiAwLjEyNXJlbSxcbiAgbWVudS1zdWJtZW51LWl0ZW0tYm9yZGVyLXJhZGl1czogcmFkaXVzLFxuICBtZW51LXN1Ym1lbnUtaXRlbS1wYWRkaW5nOiAwLjVyZW0gMXJlbSxcbiAgbWVudS1zdWJtZW51LWl0ZW0tY29udGFpbmVyLXBhZGRpbmc6IDAgMS4yNXJlbSxcbiAgbWVudS1zdWJtZW51LXBhZGRpbmc6IDAuNXJlbSxcblxuICBtZW51LWdyb3VwLWZvbnQtd2VpZ2h0OiBmb250LXdlaWdodC1ib2xkZXIsXG4gIG1lbnUtZ3JvdXAtZm9udC1zaXplOiAwLjg3NXJlbSxcbiAgbWVudS1ncm91cC1mZzogY29sb3ItZmcsXG4gIG1lbnUtZ3JvdXAtcGFkZGluZzogMXJlbSAxLjI1cmVtLFxuICBtZW51LWl0ZW0tcGFkZGluZzogMC42NzVyZW0gMC43NXJlbSxcbiAgbWVudS1pdGVtLXNlcGFyYXRvcjogc2VwYXJhdG9yLFxuICBtZW51LWljb24tZm9udC1zaXplOiAyLjVyZW0sXG4gIG1lbnUtaWNvbi1tYXJnaW46IDAgMC4yNXJlbSAwLFxuICBtZW51LWljb24tY29sb3I6IGNvbG9yLWZnLFxuICBtZW51LWljb24tYWN0aXZlLWNvbG9yOiBjb2xvci1mZy1oZWFkaW5nLFxuXG4gIHRhYnMtZm9udC1mYW1pbHk6IGZvbnQtc2Vjb25kYXJ5LFxuICB0YWJzLWZvbnQtc2l6ZTogZm9udC1zaXplLWxnLFxuICB0YWJzLWNvbnRlbnQtZm9udC1mYW1pbHk6IGZvbnQtbWFpbixcbiAgdGFicy1jb250ZW50LWZvbnQtc2l6ZTogZm9udC1zaXplLFxuICB0YWJzLWFjdGl2ZS1iZzogdHJhbnNwYXJlbnQsXG4gIHRhYnMtYWN0aXZlLWZvbnQtd2VpZ2h0OiBjYXJkLWhlYWRlci1mb250LXdlaWdodCxcbiAgdGFicy1wYWRkaW5nOiBwYWRkaW5nLFxuICB0YWJzLWNvbnRlbnQtcGFkZGluZzogMCxcbiAgdGFicy1oZWFkZXItYmc6IHRyYW5zcGFyZW50LFxuICB0YWJzLXNlcGFyYXRvcjogc2VwYXJhdG9yLFxuICB0YWJzLWZnOiBjb2xvci1mZyxcbiAgdGFicy1mZy10ZXh0OiBjb2xvci1mZy10ZXh0LFxuICB0YWJzLWZnLWhlYWRpbmc6IGNvbG9yLWZnLWhlYWRpbmcsXG4gIHRhYnMtYmc6IHRyYW5zcGFyZW50LFxuICB0YWJzLXNlbGVjdGVkOiBjb2xvci1zdWNjZXNzLFxuICB0YWJzLWljb24tb25seS1tYXgtd2lkdGg6IDU3NnB4LFxuXG4gIHJvdXRlLXRhYnMtZm9udC1mYW1pbHk6IGZvbnQtc2Vjb25kYXJ5LFxuICByb3V0ZS10YWJzLWZvbnQtc2l6ZTogZm9udC1zaXplLWxnLFxuICByb3V0ZS10YWJzLWFjdGl2ZS1iZzogdHJhbnNwYXJlbnQsXG4gIHJvdXRlLXRhYnMtYWN0aXZlLWZvbnQtd2VpZ2h0OiBjYXJkLWhlYWRlci1mb250LXdlaWdodCxcbiAgcm91dGUtdGFicy1wYWRkaW5nOiBwYWRkaW5nLFxuICByb3V0ZS10YWJzLWhlYWRlci1iZzogdHJhbnNwYXJlbnQsXG4gIHJvdXRlLXRhYnMtc2VwYXJhdG9yOiBzZXBhcmF0b3IsXG4gIHJvdXRlLXRhYnMtZmc6IGNvbG9yLWZnLFxuICByb3V0ZS10YWJzLWZnLWhlYWRpbmc6IGNvbG9yLWZnLWhlYWRpbmcsXG4gIHJvdXRlLXRhYnMtYmc6IHRyYW5zcGFyZW50LFxuICByb3V0ZS10YWJzLXNlbGVjdGVkOiBjb2xvci1zdWNjZXNzLFxuICByb3V0ZS10YWJzLWljb24tb25seS1tYXgtd2lkdGg6IDU3NnB4LFxuXG4gIHVzZXItZm9udC1zaXplOiBmb250LXNpemUsXG4gIHVzZXItbGluZS1oZWlnaHQ6IGxpbmUtaGVpZ2h0LFxuICB1c2VyLWJnOiBjb2xvci1iZyxcbiAgdXNlci1mZzogY29sb3ItZmcsXG4gIHVzZXItZmctaGlnaGxpZ2h0OiAjYmNjM2NjLFxuICB1c2VyLWZvbnQtZmFtaWx5LXNlY29uZGFyeTogZm9udC1zZWNvbmRhcnksXG4gIHVzZXItc2l6ZS1zbWFsbDogMS41cmVtLFxuICB1c2VyLXNpemUtbWVkaXVtOiAyLjVyZW0sXG4gIHVzZXItc2l6ZS1sYXJnZTogMy4yNXJlbSxcbiAgdXNlci1zaXplLXhsYXJnZTogNHJlbSxcblxuICBwb3BvdmVyLWZnOiBjb2xvci1mZy1oZWFkaW5nLFxuICBwb3BvdmVyLWJnOiBjb2xvci1iZyxcbiAgcG9wb3Zlci1ib3JkZXI6IGNvbG9yLXN1Y2Nlc3MsXG4gIHBvcG92ZXItc2hhZG93OiBub25lLFxuXG4gIGNvbnRleHQtbWVudS1mZzogY29sb3ItZmctaGVhZGluZyxcbiAgY29udGV4dC1tZW51LWFjdGl2ZS1mZzogY29sb3Itd2hpdGUsXG4gIGNvbnRleHQtbWVudS1hY3RpdmUtYmc6IGNvbG9yLXN1Y2Nlc3MsXG5cbiAgYWN0aW9ucy1mb250LXNpemU6IGZvbnQtc2l6ZSxcbiAgYWN0aW9ucy1mb250LWZhbWlseTogZm9udC1zZWNvbmRhcnksXG4gIGFjdGlvbnMtbGluZS1oZWlnaHQ6IGxpbmUtaGVpZ2h0LFxuICBhY3Rpb25zLWZnOiBjb2xvci1mZyxcbiAgYWN0aW9ucy1iZzogY29sb3ItYmcsXG4gIGFjdGlvbnMtc2VwYXJhdG9yOiBzZXBhcmF0b3IsXG4gIGFjdGlvbnMtcGFkZGluZzogcGFkZGluZyxcbiAgYWN0aW9ucy1zaXplLXNtYWxsOiAxLjVyZW0sXG4gIGFjdGlvbnMtc2l6ZS1tZWRpdW06IDIuMjVyZW0sXG4gIGFjdGlvbnMtc2l6ZS1sYXJnZTogMy41cmVtLFxuXG4gIHNlYXJjaC1idG4tb3Blbi1mZzogY29sb3ItZmcsXG4gIHNlYXJjaC1idG4tY2xvc2UtZmc6XHRjb2xvci1mZyxcbiAgc2VhcmNoLWJnOiBsYXlvdXQtYmcsXG4gIHNlYXJjaC1iZy1zZWNvbmRhcnk6IGNvbG9yLWZnLFxuICBzZWFyY2gtdGV4dDogY29sb3ItZmctaGVhZGluZyxcbiAgc2VhcmNoLWluZm86IGNvbG9yLWZnLFxuICBzZWFyY2gtZGFzaDogY29sb3ItZmcsXG4gIHNlYXJjaC1wbGFjZWhvbGRlcjogY29sb3ItZmcsXG5cbiAgc21hcnQtdGFibGUtaGVhZGVyLWZvbnQtZmFtaWx5OiBmb250LXNlY29uZGFyeSxcbiAgc21hcnQtdGFibGUtaGVhZGVyLWZvbnQtc2l6ZTogZm9udC1zaXplLFxuICBzbWFydC10YWJsZS1oZWFkZXItZm9udC13ZWlnaHQ6IGZvbnQtd2VpZ2h0LWJvbGQsXG4gIHNtYXJ0LXRhYmxlLWhlYWRlci1saW5lLWhlaWdodDogbGluZS1oZWlnaHQsXG4gIHNtYXJ0LXRhYmxlLWhlYWRlci1mZzogY29sb3ItZmctaGVhZGluZyxcbiAgc21hcnQtdGFibGUtaGVhZGVyLWJnOiBjb2xvci1iZyxcblxuICBzbWFydC10YWJsZS1mb250LWZhbWlseTogZm9udC1tYWluLFxuICBzbWFydC10YWJsZS1mb250LXNpemU6IGZvbnQtc2l6ZSxcbiAgc21hcnQtdGFibGUtZm9udC13ZWlnaHQ6IGZvbnQtd2VpZ2h0LW5vcm1hbCxcbiAgc21hcnQtdGFibGUtbGluZS1oZWlnaHQ6IGxpbmUtaGVpZ2h0LFxuICBzbWFydC10YWJsZS1mZzogY29sb3ItZmctaGVhZGluZyxcbiAgc21hcnQtdGFibGUtYmc6IGNvbG9yLWJnLFxuXG4gIHNtYXJ0LXRhYmxlLWJnLWV2ZW46ICNmNWY3ZmMsXG4gIHNtYXJ0LXRhYmxlLWZnLXNlY29uZGFyeTogY29sb3ItZmcsXG4gIHNtYXJ0LXRhYmxlLWJnLWFjdGl2ZTogI2U2ZjNmZixcbiAgc21hcnQtdGFibGUtcGFkZGluZzogMC44NzVyZW0gMS4yNXJlbSxcbiAgc21hcnQtdGFibGUtZmlsdGVyLXBhZGRpbmc6IDAuMzc1cmVtIDAuNXJlbSxcbiAgc21hcnQtdGFibGUtc2VwYXJhdG9yOiBzZXBhcmF0b3IsXG4gIHNtYXJ0LXRhYmxlLWJvcmRlci1yYWRpdXM6IHJhZGl1cyxcblxuICBzbWFydC10YWJsZS1wYWdpbmctYm9yZGVyLWNvbG9yOiBzZXBhcmF0b3IsXG4gIHNtYXJ0LXRhYmxlLXBhZ2luZy1ib3JkZXItd2lkdGg6IDFweCxcbiAgc21hcnQtdGFibGUtcGFnaW5nLWZnLWFjdGl2ZTogI2ZmZmZmZixcbiAgc21hcnQtdGFibGUtcGFnaW5nLWJnLWFjdGl2ZTogY29sb3Itc3VjY2VzcyxcbiAgc21hcnQtdGFibGUtcGFnaW5nLWhvdmVyOiByZ2JhKDAsIDAsIDAsIDAuMDUpLFxuXG4gIHRvYXN0ZXItYmc6IGNvbG9yLXByaW1hcnksXG4gIHRvYXN0ZXItZmctZGVmYXVsdDogY29sb3ItaW52ZXJzZSxcbiAgdG9hc3Rlci1idG4tY2xvc2UtYmc6IHRyYW5zcGFyZW50LFxuICB0b2FzdGVyLWJ0bi1jbG9zZS1mZzogdG9hc3Rlci1mZy1kZWZhdWx0LFxuICB0b2FzdGVyLXNoYWRvdzogc2hhZG93LFxuXG4gIHRvYXN0ZXItZmc6IGNvbG9yLXdoaXRlLFxuICB0b2FzdGVyLXN1Y2Nlc3M6IGNvbG9yLXN1Y2Nlc3MsXG4gIHRvYXN0ZXItaW5mbzogY29sb3ItaW5mbyxcbiAgdG9hc3Rlci13YXJuaW5nOiBjb2xvci13YXJuaW5nLFxuICB0b2FzdGVyLXdhaXQ6IGNvbG9yLXByaW1hcnksXG4gIHRvYXN0ZXItZXJyb3I6IGNvbG9yLWRhbmdlcixcblxuICBidG4tZmc6IGNvbG9yLXdoaXRlLFxuICBidG4tZm9udC1mYW1pbHk6IGZvbnQtc2Vjb25kYXJ5LFxuICBidG4tbGluZS1oZWlnaHQ6IGxpbmUtaGVpZ2h0LFxuICBidG4tZGlzYWJsZWQtb3BhY2l0eTogMC4zLFxuICBidG4tY3Vyc29yOiBkZWZhdWx0LFxuXG4gIGJ0bi1wcmltYXJ5LWJnOiBjb2xvci1wcmltYXJ5LFxuICBidG4tc2Vjb25kYXJ5LWJnOiB0cmFuc3BhcmVudCxcbiAgYnRuLWluZm8tYmc6IGNvbG9yLWluZm8sXG4gIGJ0bi1zdWNjZXNzLWJnOiBjb2xvci1zdWNjZXNzLFxuICBidG4td2FybmluZy1iZzogY29sb3Itd2FybmluZyxcbiAgYnRuLWRhbmdlci1iZzogY29sb3ItZGFuZ2VyLFxuXG4gIGJ0bi1zZWNvbmRhcnktYm9yZGVyOiAjZGFkZmU2LFxuICBidG4tc2Vjb25kYXJ5LWJvcmRlci13aWR0aDogMnB4LFxuXG4gIGJ0bi1wYWRkaW5nLXktbGc6IDAuODc1cmVtLFxuICBidG4tcGFkZGluZy14LWxnOiAxLjc1cmVtLFxuICBidG4tZm9udC1zaXplLWxnOiBmb250LXNpemUtbGcsXG5cbiAgLy8gZGVmYXVsdCBzaXplXG4gIGJ0bi1wYWRkaW5nLXktbWQ6IDAuNzVyZW0sXG4gIGJ0bi1wYWRkaW5nLXgtbWQ6IDEuNXJlbSxcbiAgYnRuLWZvbnQtc2l6ZS1tZDogMXJlbSxcblxuICBidG4tcGFkZGluZy15LXNtOiAwLjYyNXJlbSxcbiAgYnRuLXBhZGRpbmcteC1zbTogMS41cmVtLFxuICBidG4tZm9udC1zaXplLXNtOiAwLjg3NXJlbSxcblxuICBidG4tcGFkZGluZy15LXhzOiAwLjVyZW0sXG4gIGJ0bi1wYWRkaW5nLXgteHM6IDEuMjVyZW0sXG4gIGJ0bi1mb250LXNpemUteHM6IDAuNzVyZW0sXG5cbiAgYnRuLWJvcmRlci1yYWRpdXM6IHJhZGl1cyxcbiAgYnRuLXJlY3RhbmdsZS1ib3JkZXItcmFkaXVzOiAwLjI1cmVtLFxuICBidG4tc2VtaS1yb3VuZC1ib3JkZXItcmFkaXVzOiAwLjc1cmVtLFxuICBidG4tcm91bmQtYm9yZGVyLXJhZGl1czogMS41cmVtLFxuXG4gIGJ0bi1oZXJvLXNoYWRvdzogbm9uZSxcbiAgYnRuLWhlcm8tdGV4dC1zaGFkb3c6IG5vbmUsXG4gIGJ0bi1oZXJvLWJldmVsLXNpemU6IDAgMCAwIDAsXG4gIGJ0bi1oZXJvLWdsb3ctc2l6ZTogMCAwIDAgMCxcbiAgYnRuLWhlcm8tcHJpbWFyeS1nbG93LXNpemU6IGJ0bi1oZXJvLWdsb3ctc2l6ZSxcbiAgYnRuLWhlcm8tc3VjY2Vzcy1nbG93LXNpemU6IGJ0bi1oZXJvLWdsb3ctc2l6ZSxcbiAgYnRuLWhlcm8td2FybmluZy1nbG93LXNpemU6IGJ0bi1oZXJvLWdsb3ctc2l6ZSxcbiAgYnRuLWhlcm8taW5mby1nbG93LXNpemU6IGJ0bi1oZXJvLWdsb3ctc2l6ZSxcbiAgYnRuLWhlcm8tZGFuZ2VyLWdsb3ctc2l6ZTogYnRuLWhlcm8tZ2xvdy1zaXplLFxuICBidG4taGVyby1zZWNvbmRhcnktZ2xvdy1zaXplOiBidG4taGVyby1nbG93LXNpemUsXG4gIGJ0bi1oZXJvLWRlZ3JlZTogMjBkZWcsXG4gIGJ0bi1oZXJvLXByaW1hcnktZGVncmVlOiBidG4taGVyby1kZWdyZWUsXG4gIGJ0bi1oZXJvLXN1Y2Nlc3MtZGVncmVlOiBidG4taGVyby1kZWdyZWUsXG4gIGJ0bi1oZXJvLXdhcm5pbmctZGVncmVlOiAxMGRlZyxcbiAgYnRuLWhlcm8taW5mby1kZWdyZWU6IC0xMGRlZyxcbiAgYnRuLWhlcm8tZGFuZ2VyLWRlZ3JlZTogLTIwZGVnLFxuICBidG4taGVyby1zZWNvbmRhcnktZGVncmVlOiBidG4taGVyby1kZWdyZWUsXG4gIGJ0bi1oZXJvLWJvcmRlci1yYWRpdXM6IHJhZGl1cyxcblxuICBidG4tb3V0bGluZS1mZzogY29sb3ItZmctaGVhZGluZyxcbiAgYnRuLW91dGxpbmUtaG92ZXItZmc6ICNmZmZmZmYsXG4gIGJ0bi1vdXRsaW5lLWZvY3VzLWZnOiBjb2xvci1mZy1oZWFkaW5nLFxuXG4gIGJ0bi1ncm91cC1iZzogbGF5b3V0LWJnLFxuICBidG4tZ3JvdXAtZmc6IGNvbG9yLWZnLWhlYWRpbmcsXG4gIGJ0bi1ncm91cC1zZXBhcmF0b3I6ICNkYWRmZTYsXG5cbiAgZm9ybS1jb250cm9sLXRleHQtcHJpbWFyeS1jb2xvcjogY29sb3ItZmctaGVhZGluZyxcbiAgZm9ybS1jb250cm9sLWJnOiBjb2xvci1iZyxcbiAgZm9ybS1jb250cm9sLWZvY3VzLWJnOiBjb2xvci1iZyxcblxuICBmb3JtLWNvbnRyb2wtYm9yZGVyLXdpZHRoOiAycHgsXG4gIGZvcm0tY29udHJvbC1ib3JkZXItdHlwZTogc29saWQsXG4gIGZvcm0tY29udHJvbC1ib3JkZXItcmFkaXVzOiByYWRpdXMsXG4gIGZvcm0tY29udHJvbC1zZW1pLXJvdW5kLWJvcmRlci1yYWRpdXM6IDAuNzVyZW0sXG4gIGZvcm0tY29udHJvbC1yb3VuZC1ib3JkZXItcmFkaXVzOiAxLjVyZW0sXG4gIGZvcm0tY29udHJvbC1ib3JkZXItY29sb3I6ICNkYWRmZTYsXG4gIGZvcm0tY29udHJvbC1zZWxlY3RlZC1ib3JkZXItY29sb3I6IGNvbG9yLXN1Y2Nlc3MsXG5cbiAgZm9ybS1jb250cm9sLWluZm8tYm9yZGVyLWNvbG9yOiBjb2xvci1pbmZvLFxuICBmb3JtLWNvbnRyb2wtc3VjY2Vzcy1ib3JkZXItY29sb3I6IGNvbG9yLXN1Y2Nlc3MsXG4gIGZvcm0tY29udHJvbC1kYW5nZXItYm9yZGVyLWNvbG9yOiBjb2xvci1kYW5nZXIsXG4gIGZvcm0tY29udHJvbC13YXJuaW5nLWJvcmRlci1jb2xvcjogY29sb3Itd2FybmluZyxcblxuICBmb3JtLWNvbnRyb2wtcGxhY2Vob2xkZXItY29sb3I6IGNvbG9yLWZnLFxuICBmb3JtLWNvbnRyb2wtcGxhY2Vob2xkZXItZm9udC1zaXplOiAxcmVtLFxuXG4gIGZvcm0tY29udHJvbC1mb250LXNpemU6IDFyZW0sXG4gIGZvcm0tY29udHJvbC1wYWRkaW5nOiAwLjc1cmVtIDEuMTI1cmVtLFxuICBmb3JtLWNvbnRyb2wtZm9udC1zaXplLXNtOiBmb250LXNpemUtc20sXG4gIGZvcm0tY29udHJvbC1wYWRkaW5nLXNtOiAwLjM3NXJlbSAxLjEyNXJlbSxcbiAgZm9ybS1jb250cm9sLWZvbnQtc2l6ZS1sZzogZm9udC1zaXplLWxnLFxuICBmb3JtLWNvbnRyb2wtcGFkZGluZy1sZzogMS4xMjVyZW0sXG5cbiAgZm9ybS1jb250cm9sLWxhYmVsLWZvbnQtd2VpZ2h0OiA0MDAsXG5cbiAgZm9ybS1jb250cm9sLWZlZWRiYWNrLWZvbnQtc2l6ZTogMC44NzVyZW0sXG4gIGZvcm0tY29udHJvbC1mZWVkYmFjay1mb250LXdlaWdodDogZm9udC13ZWlnaHQtbm9ybWFsLFxuXG4gIGNoZWNrYm94LWJnOiB0cmFuc3BhcmVudCxcbiAgY2hlY2tib3gtc2l6ZTogMS4yNXJlbSxcbiAgY2hlY2tib3gtYm9yZGVyLXNpemU6IDJweCxcbiAgY2hlY2tib3gtYm9yZGVyLWNvbG9yOiBmb3JtLWNvbnRyb2wtYm9yZGVyLWNvbG9yLFxuICBjaGVja2JveC1jaGVja21hcms6IHRyYW5zcGFyZW50LFxuXG4gIGNoZWNrYm94LWNoZWNrZWQtYmc6IHRyYW5zcGFyZW50LFxuICBjaGVja2JveC1jaGVja2VkLXNpemU6IDEuMjVyZW0sXG4gIGNoZWNrYm94LWNoZWNrZWQtYm9yZGVyLXNpemU6IDJweCxcbiAgY2hlY2tib3gtY2hlY2tlZC1ib3JkZXItY29sb3I6IGNvbG9yLXN1Y2Nlc3MsXG4gIGNoZWNrYm94LWNoZWNrZWQtY2hlY2ttYXJrOiBjb2xvci1mZy1oZWFkaW5nLFxuXG4gIGNoZWNrYm94LWRpc2FibGVkLWJnOiB0cmFuc3BhcmVudCxcbiAgY2hlY2tib3gtZGlzYWJsZWQtc2l6ZTogMS4yNXJlbSxcbiAgY2hlY2tib3gtZGlzYWJsZWQtYm9yZGVyLXNpemU6IDJweCxcbiAgY2hlY2tib3gtZGlzYWJsZWQtYm9yZGVyLWNvbG9yOiBjb2xvci1mZy1oZWFkaW5nLFxuICBjaGVja2JveC1kaXNhYmxlZC1jaGVja21hcms6IGNvbG9yLWZnLWhlYWRpbmcsXG5cbiAgcmFkaW8tZmc6IGNvbG9yLXN1Y2Nlc3MsXG5cbiAgbW9kYWwtZm9udC1zaXplOiBmb250LXNpemUsXG4gIG1vZGFsLWxpbmUtaGVpZ2h0OiBsaW5lLWhlaWdodCxcbiAgbW9kYWwtZm9udC13ZWlnaHQ6IGZvbnQtd2VpZ2h0LW5vcm1hbCxcbiAgbW9kYWwtZmc6IGNvbG9yLWZnLXRleHQsXG4gIG1vZGFsLWZnLWhlYWRpbmc6IGNvbG9yLWZnLWhlYWRpbmcsXG4gIG1vZGFsLWJnOiBjb2xvci1iZyxcbiAgbW9kYWwtYm9yZGVyOiB0cmFuc3BhcmVudCxcbiAgbW9kYWwtYm9yZGVyLXJhZGl1czogcmFkaXVzLFxuICBtb2RhbC1wYWRkaW5nOiBwYWRkaW5nLFxuICBtb2RhbC1oZWFkZXItZm9udC1mYW1pbHk6IGZvbnQtc2Vjb25kYXJ5LFxuICBtb2RhbC1oZWFkZXItZm9udC13ZWlnaHQ6IGZvbnQtd2VpZ2h0LWJvbGRlcixcbiAgbW9kYWwtaGVhZGVyLWZvbnQtc2l6ZTogZm9udC1zaXplLWxnLFxuICBtb2RhbC1ib2R5LWZvbnQtZmFtaWx5OiBmb250LW1haW4sXG4gIG1vZGFsLWJvZHktZm9udC13ZWlnaHQ6IGZvbnQtd2VpZ2h0LW5vcm1hbCxcbiAgbW9kYWwtYm9keS1mb250LXNpemU6IGZvbnQtc2l6ZSxcbiAgbW9kYWwtc2VwYXJhdG9yOiBzZXBhcmF0b3IsXG5cbiAgYmFkZ2UtZmctdGV4dDogY29sb3Itd2hpdGUsXG4gIGJhZGdlLXByaW1hcnktYmctY29sb3I6IGNvbG9yLXByaW1hcnksXG4gIGJhZGdlLXN1Y2Nlc3MtYmctY29sb3I6IGNvbG9yLXN1Y2Nlc3MsXG4gIGJhZGdlLWluZm8tYmctY29sb3I6IGNvbG9yLWluZm8sXG4gIGJhZGdlLXdhcm5pbmctYmctY29sb3I6IGNvbG9yLXdhcm5pbmcsXG4gIGJhZGdlLWRhbmdlci1iZy1jb2xvcjogY29sb3ItZGFuZ2VyLFxuXG4gIHByb2dyZXNzLWJhci1oZWlnaHQteGxnOiAxLjc1cmVtLFxuICBwcm9ncmVzcy1iYXItaGVpZ2h0LWxnOiAxLjVyZW0sXG4gIHByb2dyZXNzLWJhci1oZWlnaHQ6IDEuMzc1cmVtLFxuICBwcm9ncmVzcy1iYXItaGVpZ2h0LXNtOiAxLjI1cmVtLFxuICBwcm9ncmVzcy1iYXItaGVpZ2h0LXhzOiAxcmVtLFxuICBwcm9ncmVzcy1iYXItYW5pbWF0aW9uLWR1cmF0aW9uOiA0MDBtcyxcbiAgcHJvZ3Jlc3MtYmFyLWZvbnQtc2l6ZS14bGc6IGZvbnQtc2l6ZS14bGcsXG4gIHByb2dyZXNzLWJhci1mb250LXNpemUtbGc6IGZvbnQtc2l6ZS1sZyxcbiAgcHJvZ3Jlc3MtYmFyLWZvbnQtc2l6ZTogZm9udC1zaXplLFxuICBwcm9ncmVzcy1iYXItZm9udC1zaXplLXNtOiBmb250LXNpemUtc20sXG4gIHByb2dyZXNzLWJhci1mb250LXNpemUteHM6IGZvbnQtc2l6ZS14cyxcbiAgcHJvZ3Jlc3MtYmFyLXJhZGl1czogcmFkaXVzLFxuICBwcm9ncmVzcy1iYXItYmc6IGxheW91dC1iZyxcbiAgcHJvZ3Jlc3MtYmFyLWZvbnQtY29sb3I6IGNvbG9yLXdoaXRlLFxuICBwcm9ncmVzcy1iYXItZm9udC13ZWlnaHQ6IGZvbnQtd2VpZ2h0LWJvbGQsXG4gIHByb2dyZXNzLWJhci1kZWZhdWx0LWJnOiBjb2xvci1pbmZvLFxuICBwcm9ncmVzcy1iYXItcHJpbWFyeS1iZzogY29sb3ItcHJpbWFyeSxcbiAgcHJvZ3Jlc3MtYmFyLXN1Y2Nlc3MtYmc6IGNvbG9yLXN1Y2Nlc3MsXG4gIHByb2dyZXNzLWJhci1pbmZvLWJnOiBjb2xvci1pbmZvLFxuICBwcm9ncmVzcy1iYXItd2FybmluZy1iZzogY29sb3Itd2FybmluZyxcbiAgcHJvZ3Jlc3MtYmFyLWRhbmdlci1iZzogY29sb3ItZGFuZ2VyLFxuXG4gIGFsZXJ0LWZvbnQtc2l6ZTogZm9udC1zaXplLFxuICBhbGVydC1saW5lLWhlaWdodDogbGluZS1oZWlnaHQsXG4gIGFsZXJ0LWZvbnQtd2VpZ2h0OiBmb250LXdlaWdodC1ub3JtYWwsXG4gIGFsZXJ0LWZnOiBjb2xvci13aGl0ZSxcbiAgYWxlcnQtb3V0bGluZS1mZzogY29sb3ItZmcsXG4gIGFsZXJ0LWJnOiBjb2xvci1iZyxcbiAgYWxlcnQtYWN0aXZlLWJnOiBjb2xvci1mZyxcbiAgYWxlcnQtZGlzYWJsZWQtYmc6IGNvbG9yLWRpc2FibGVkLFxuICBhbGVydC1kaXNhYmxlZC1mZzogY29sb3ItZmcsXG4gIGFsZXJ0LXByaW1hcnktYmc6IGNvbG9yLXByaW1hcnksXG4gIGFsZXJ0LWluZm8tYmc6IGNvbG9yLWluZm8sXG4gIGFsZXJ0LXN1Y2Nlc3MtYmc6IGNvbG9yLXN1Y2Nlc3MsXG4gIGFsZXJ0LXdhcm5pbmctYmc6IGNvbG9yLXdhcm5pbmcsXG4gIGFsZXJ0LWRhbmdlci1iZzogY29sb3ItZGFuZ2VyLFxuICBhbGVydC1oZWlnaHQteHhzbWFsbDogNTJweCxcbiAgYWxlcnQtaGVpZ2h0LXhzbWFsbDogNzJweCxcbiAgYWxlcnQtaGVpZ2h0LXNtYWxsOiA5MnB4LFxuICBhbGVydC1oZWlnaHQtbWVkaXVtOiAxMTJweCxcbiAgYWxlcnQtaGVpZ2h0LWxhcmdlOiAxMzJweCxcbiAgYWxlcnQtaGVpZ2h0LXhsYXJnZTogMTUycHgsXG4gIGFsZXJ0LWhlaWdodC14eGxhcmdlOiAxNzJweCxcbiAgYWxlcnQtc2hhZG93OiBub25lLFxuICBhbGVydC1ib3JkZXItcmFkaXVzOiByYWRpdXMsXG4gIGFsZXJ0LXBhZGRpbmc6IDFyZW0gMS4xMjVyZW0sXG4gIGFsZXJ0LWNsb3NhYmxlLXBhZGRpbmc6IDNyZW0sXG4gIGFsZXJ0LWJ1dHRvbi1wYWRkaW5nOiAzcmVtLFxuICBhbGVydC1tYXJnaW46IG1hcmdpbixcblxuICBjaGF0LWZvbnQtc2l6ZTogZm9udC1zaXplLFxuICBjaGF0LWZnOiBjb2xvci13aGl0ZSxcbiAgY2hhdC1iZzogY29sb3ItYmcsXG4gIGNoYXQtYm9yZGVyLXJhZGl1czogcmFkaXVzLFxuICBjaGF0LWZnLXRleHQ6IGNvbG9yLWZnLXRleHQsXG4gIGNoYXQtaGVpZ2h0LXh4c21hbGw6IDk2cHgsXG4gIGNoYXQtaGVpZ2h0LXhzbWFsbDogMjE2cHgsXG4gIGNoYXQtaGVpZ2h0LXNtYWxsOiAzMzZweCxcbiAgY2hhdC1oZWlnaHQtbWVkaXVtOiA0NTZweCxcbiAgY2hhdC1oZWlnaHQtbGFyZ2U6IDU3NnB4LFxuICBjaGF0LWhlaWdodC14bGFyZ2U6IDY5NnB4LFxuICBjaGF0LWhlaWdodC14eGxhcmdlOiA4MTZweCxcbiAgY2hhdC1ib3JkZXI6IGJvcmRlcixcbiAgY2hhdC1wYWRkaW5nOiBwYWRkaW5nLFxuICBjaGF0LXNoYWRvdzogc2hhZG93LFxuICBjaGF0LXNlcGFyYXRvcjogc2VwYXJhdG9yLFxuICBjaGF0LW1lc3NhZ2UtZmc6IGNvbG9yLXdoaXRlLFxuICBjaGF0LW1lc3NhZ2UtYmc6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgIzRjYTZmZiwgIzU5YmZmZiksXG4gIGNoYXQtbWVzc2FnZS1yZXBseS1iZzogY29sb3ItYmctYWN0aXZlLFxuICBjaGF0LW1lc3NhZ2UtcmVwbHktZmc6IGNvbG9yLWZnLXRleHQsXG4gIGNoYXQtbWVzc2FnZS1hdmF0YXItYmc6IGNvbG9yLWZnLFxuICBjaGF0LW1lc3NhZ2Utc2VuZGVyLWZnOiBjb2xvci1mZyxcbiAgY2hhdC1tZXNzYWdlLXF1b3RlLWZnOiBjb2xvci1mZyxcbiAgY2hhdC1tZXNzYWdlLXF1b3RlLWJnOiBjb2xvci1iZy1hY3RpdmUsXG4gIGNoYXQtbWVzc2FnZS1maWxlLWZnOiBjb2xvci1mZyxcbiAgY2hhdC1tZXNzYWdlLWZpbGUtYmc6IHRyYW5zcGFyZW50LFxuICBjaGF0LWZvcm0tYmc6IHRyYW5zcGFyZW50LFxuICBjaGF0LWZvcm0tZmc6IGNvbG9yLWZnLWhlYWRpbmcsXG4gIGNoYXQtZm9ybS1ib3JkZXI6IHNlcGFyYXRvcixcbiAgY2hhdC1mb3JtLXBsYWNlaG9sZGVyLWZnOiBjb2xvci1mZyxcbiAgY2hhdC1mb3JtLWFjdGl2ZS1ib3JkZXI6IGNvbG9yLWZnLFxuICBjaGF0LWFjdGl2ZS1iZzogY29sb3ItZmcsXG4gIGNoYXQtZGlzYWJsZWQtYmc6IGNvbG9yLWRpc2FibGVkLFxuICBjaGF0LWRpc2FibGVkLWZnOiBjb2xvci1mZyxcbiAgY2hhdC1wcmltYXJ5LWJnOiBjb2xvci1wcmltYXJ5LFxuICBjaGF0LWluZm8tYmc6IGNvbG9yLWluZm8sXG4gIGNoYXQtc3VjY2Vzcy1iZzogY29sb3Itc3VjY2VzcyxcbiAgY2hhdC13YXJuaW5nLWJnOiBjb2xvci13YXJuaW5nLFxuICBjaGF0LWRhbmdlci1iZzogY29sb3ItZGFuZ2VyLFxuXG4gIHNwaW5uZXItYmc6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC44MyksXG4gIHNwaW5uZXItY2lyY2xlLWJnOiBjb2xvci1iZy1hY3RpdmUsXG4gIHNwaW5uZXItZmc6IGNvbG9yLWZnLXRleHQsXG4gIHNwaW5uZXItYWN0aXZlLWJnOiBjb2xvci1mZyxcbiAgc3Bpbm5lci1kaXNhYmxlZC1iZzogY29sb3ItZGlzYWJsZWQsXG4gIHNwaW5uZXItZGlzYWJsZWQtZmc6IGNvbG9yLWZnLFxuICBzcGlubmVyLXByaW1hcnktYmc6IGNvbG9yLXByaW1hcnksXG4gIHNwaW5uZXItaW5mby1iZzogY29sb3ItaW5mbyxcbiAgc3Bpbm5lci1zdWNjZXNzLWJnOiBjb2xvci1zdWNjZXNzLFxuICBzcGlubmVyLXdhcm5pbmctYmc6IGNvbG9yLXdhcm5pbmcsXG4gIHNwaW5uZXItZGFuZ2VyLWJnOiBjb2xvci1kYW5nZXIsXG4gIHNwaW5uZXIteHhzbWFsbDogMS4yNXJlbSxcbiAgc3Bpbm5lci14c21hbGw6IDEuNXJlbSxcbiAgc3Bpbm5lci1zbWFsbDogMS43NXJlbSxcbiAgc3Bpbm5lci1tZWRpdW06IDJyZW0sXG4gIHNwaW5uZXItbGFyZ2U6IDIuMjVyZW0sXG4gIHNwaW5uZXIteGxhcmdlOiAyLjVyZW0sXG4gIHNwaW5uZXIteHhsYXJnZTogM3JlbSxcblxuICBzdGVwcGVyLWluZGV4LXNpemU6IDJyZW0sXG4gIHN0ZXBwZXItbGFiZWwtZm9udC1zaXplOiBmb250LXNpemUtc20sXG4gIHN0ZXBwZXItbGFiZWwtZm9udC13ZWlnaHQ6IGZvbnQtd2VpZ2h0LWJvbGRlcixcbiAgc3RlcHBlci1hY2NlbnQtY29sb3I6IGNvbG9yLXByaW1hcnksXG4gIHN0ZXBwZXItY29tcGxldGVkLWZnOiBjb2xvci13aGl0ZSxcbiAgc3RlcHBlci1mZzogY29sb3ItZmcsXG4gIHN0ZXBwZXItY29tcGxldGVkLWljb24tc2l6ZTogMS41cmVtLFxuICBzdGVwcGVyLWNvbXBsZXRlZC1pY29uLXdlaWdodDogZm9udC13ZWlnaHQtdWx0cmEtYm9sZCxcbiAgc3RlcHBlci1zdGVwLXBhZGRpbmc6IHBhZGRpbmcsXG5cbiAgYWNjb3JkaW9uLXBhZGRpbmc6IHBhZGRpbmcsXG4gIGFjY29yZGlvbi1zZXBhcmF0b3I6IHNlcGFyYXRvcixcbiAgYWNjb3JkaW9uLWhlYWRlci1mb250LWZhbWlseTogZm9udC1zZWNvbmRhcnksXG4gIGFjY29yZGlvbi1oZWFkZXItZm9udC1zaXplOiBmb250LXNpemUtbGcsXG4gIGFjY29yZGlvbi1oZWFkZXItZm9udC13ZWlnaHQ6IGZvbnQtd2VpZ2h0LW5vcm1hbCxcbiAgYWNjb3JkaW9uLWhlYWRlci1mZy1oZWFkaW5nOiBjb2xvci1mZy1oZWFkaW5nLFxuICBhY2NvcmRpb24taGVhZGVyLWRpc2FibGVkLWZnOiBjb2xvci1mZyxcbiAgYWNjb3JkaW9uLWhlYWRlci1ib3JkZXItd2lkdGg6IDFweCxcbiAgYWNjb3JkaW9uLWhlYWRlci1ib3JkZXItdHlwZTogc29saWQsXG4gIGFjY29yZGlvbi1oZWFkZXItYm9yZGVyLWNvbG9yOiBhY2NvcmRpb24tc2VwYXJhdG9yLFxuICBhY2NvcmRpb24tYm9yZGVyLXJhZGl1czogcmFkaXVzLFxuICBhY2NvcmRpb24taXRlbS1iZzogY29sb3ItYmcsXG4gIGFjY29yZGlvbi1pdGVtLWZvbnQtc2l6ZTogZm9udC1zaXplLFxuICBhY2NvcmRpb24taXRlbS1mb250LXdlaWdodDogZm9udC13ZWlnaHQtbm9ybWFsLFxuICBhY2NvcmRpb24taXRlbS1mb250LWZhbWlseTogZm9udC1tYWluLFxuICBhY2NvcmRpb24taXRlbS1mZy10ZXh0OiBjb2xvci1mZy10ZXh0LFxuICBhY2NvcmRpb24taXRlbS1zaGFkb3c6IHNoYWRvdyxcblxuICBsaXN0LWl0ZW0tYm9yZGVyLWNvbG9yOiB0YWJzLXNlcGFyYXRvcixcbiAgbGlzdC1pdGVtLXBhZGRpbmc6IDFyZW0sXG5cbiAgY2FsZW5kYXItd2lkdGg6IDIxLjg3NXJlbSxcbiAgY2FsZW5kYXItaGVpZ2h0OiAzMXJlbSxcbiAgY2FsZW5kYXItaGVhZGVyLXRpdGxlLWZvbnQtc2l6ZTogZm9udC1zaXplLXhsZyxcbiAgY2FsZW5kYXItaGVhZGVyLXRpdGxlLWZvbnQtd2VpZ2h0OiBmb250LXdlaWdodC1ib2xkLFxuICBjYWxlbmRhci1oZWFkZXItc3ViLXRpdGxlLWZvbnQtc2l6ZTogZm9udC1zaXplLFxuICBjYWxlbmRhci1oZWFkZXItc3ViLXRpdGxlLWZvbnQtd2VpZ2h0OiBmb250LXdlaWdodC10aGluLFxuICBjYWxlbmRhci1uYXZpZ2F0aW9uLWJ1dHRvbi13aWR0aDogMTByZW0sXG4gIGNhbGVuZGFyLXNlbGVjdGVkLWl0ZW0tYmc6IGNvbG9yLXN1Y2Nlc3MsXG4gIGNhbGVuZGFyLWhvdmVyLWl0ZW0tYmc6IGNhbGVuZGFyLXNlbGVjdGVkLWl0ZW0tYmcsXG4gIGNhbGVuZGFyLXRvZGF5LWl0ZW0tYmc6IGNvbG9yLWJnLWFjdGl2ZSxcbiAgY2FsZW5kYXItYWN0aXZlLWl0ZW0tYmc6IGNvbG9yLXN1Y2Nlc3MsXG4gIGNhbGVuZGFyLWZnOiBjb2xvci1mZy10ZXh0LFxuICBjYWxlbmRhci1zZWxlY3RlZC1mZzogY29sb3Itd2hpdGUsXG4gIGNhbGVuZGFyLXRvZGF5LWZnOiBjYWxlbmRhci1mZyxcbiAgY2FsZW5kYXItZGF5LWNlbGwtd2lkdGg6IDIuNjI1cmVtLFxuICBjYWxlbmRhci1kYXktY2VsbC1oZWlnaHQ6IDIuNjI1cmVtLFxuICBjYWxlbmRhci1tb250aC1jZWxsLXdpZHRoOiA0LjI1cmVtLFxuICBjYWxlbmRhci1tb250aC1jZWxsLWhlaWdodDogMi4zNzVyZW0sXG4gIGNhbGVuZGFyLXllYXItY2VsbC13aWR0aDogY2FsZW5kYXItbW9udGgtY2VsbC13aWR0aCxcbiAgY2FsZW5kYXIteWVhci1jZWxsLWhlaWdodDogY2FsZW5kYXItbW9udGgtY2VsbC1oZWlnaHQsXG4gIGNhbGVuZGFyLWluYWN0aXZlLW9wYWNpdHk6IDAuNSxcbiAgY2FsZW5kYXItZGlzYWJsZWQtb3BhY2l0eTogMC4zLFxuICBjYWxlbmRhci1ib3JkZXItcmFkaXVzOiByYWRpdXMsXG4gIGNhbGVuZGFyLXdlZWtkYXktd2lkdGg6IGNhbGVuZGFyLWRheS1jZWxsLXdpZHRoLFxuICBjYWxlbmRhci13ZWVrZGF5LWhlaWdodDogMS43NXJlbSxcbiAgY2FsZW5kYXItd2Vla2RheS1mb250LXNpemU6IGZvbnQtc2l6ZS14cyxcbiAgY2FsZW5kYXItd2Vla2RheS1mb250LXdlaWdodDogZm9udC13ZWlnaHQtbm9ybWFsLFxuICBjYWxlbmRhci13ZWVrZGF5LWZnOiBjb2xvci1mZyxcbiAgY2FsZW5kYXItd2Vla2RheS1ob2xpZGF5LWZnOiBjb2xvci1kYW5nZXIsXG4gIGNhbGVuZGFyLXJhbmdlLWJnLWluLXJhbmdlOiAjZWJmYmYyLFxuXG4gIGNhbGVuZGFyLWxhcmdlLXdpZHRoOiAyNC4zNzVyZW0sXG4gIGNhbGVuZGFyLWxhcmdlLWhlaWdodDogMzMuMTI1cmVtLFxuICBjYWxlbmRhci1kYXktY2VsbC1sYXJnZS13aWR0aDogM3JlbSxcbiAgY2FsZW5kYXItZGF5LWNlbGwtbGFyZ2UtaGVpZ2h0OiAzcmVtLFxuICBjYWxlbmRhci1tb250aC1jZWxsLWxhcmdlLXdpZHRoOiA0LjI1cmVtLFxuICBjYWxlbmRhci1tb250aC1jZWxsLWxhcmdlLWhlaWdodDogMi4zNzVyZW0sXG4gIGNhbGVuZGFyLXllYXItY2VsbC1sYXJnZS13aWR0aDogY2FsZW5kYXItbW9udGgtY2VsbC13aWR0aCxcbiAgY2FsZW5kYXIteWVhci1jZWxsLWxhcmdlLWhlaWdodDogY2FsZW5kYXItbW9udGgtY2VsbC1oZWlnaHQsXG4pO1xuXG4vLyByZWdpc3RlciB0aGUgdGhlbWVcbiRuYi10aGVtZXM6IG5iLXJlZ2lzdGVyLXRoZW1lKCR0aGVtZSwgZGVmYXVsdCk7XG4iLCIvKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG5cbkBpbXBvcnQgJy4uL2NvcmUvZnVuY3Rpb25zJztcbkBpbXBvcnQgJy4uL2NvcmUvbWl4aW5zJztcbkBpbXBvcnQgJ2RlZmF1bHQnO1xuXG4vLyBkZWZhdWx0IHRoZSBiYXNlIHRoZW1lXG4kdGhlbWU6IChcbiAgcmFkaXVzOiAwLjVyZW0sXG5cbiAgY29sb3ItYmc6ICMzZDM3ODAsXG4gIGNvbG9yLWJnLWFjdGl2ZTogIzQ5NDI5OSxcbiAgY29sb3ItZmc6ICNhMWExZTUsXG4gIGNvbG9yLWZnLWhlYWRpbmc6ICNmZmZmZmYsXG4gIGNvbG9yLWZnLXRleHQ6ICNkMWQxZmYsXG4gIGNvbG9yLWZnLWhpZ2hsaWdodDogIzAwZjlhNixcblxuICBjb2xvci1ncmF5OiByZ2JhKDgxLCAxMTMsIDE2NSwgMC4xNSksXG4gIGNvbG9yLW5ldXRyYWw6IHRyYW5zcGFyZW50LFxuICBjb2xvci13aGl0ZTogI2ZmZmZmZixcbiAgY29sb3ItZGlzYWJsZWQ6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC40KSxcblxuICBjb2xvci1wcmltYXJ5OiAjNzY1OWZmLFxuICBjb2xvci1zdWNjZXNzOiAjMDBkOTc3LFxuICBjb2xvci1pbmZvOiAjMDA4OGZmLFxuICBjb2xvci13YXJuaW5nOiAjZmZhMTAwLFxuICBjb2xvci1kYW5nZXI6ICNmZjM4NmEsXG5cbiAgbGluay1jb2xvcjogIzAwZjlhNixcbiAgbGluay1jb2xvci1ob3ZlcjogIzE0ZmZiZSxcblxuICBzZXBhcmF0b3I6ICMzNDJlNzMsXG4gIHNoYWRvdzogMCA4cHggMjBweCAwIHJnYmEoNDAsIDM3LCA4OSwgMC42KSxcblxuICBjYXJkLWhlYWRlci1mb250LXdlaWdodDogZm9udC13ZWlnaHQtYm9sZGVyLFxuXG4gIGxheW91dC1iZzogIzJmMjk2YixcblxuICBzY3JvbGxiYXItZmc6ICM1NTRkYjMsXG4gIHNjcm9sbGJhci1iZzogIzMzMmU3MyxcblxuICByYWRpYWwtZ3JhZGllbnQ6IHJhZGlhbC1ncmFkaWVudChjaXJjbGUgYXQgNTAlIDUwJSwgIzQyM2Y4YywgIzMwMmM2ZSksXG4gIGxpbmVhci1ncmFkaWVudDogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjMTcxNzQ5LCAjNDEzNzg5KSxcblxuICBzaWRlYmFyLWZnOiBjb2xvci1zZWNvbmRhcnksXG4gIHNpZGViYXItYmc6IGNvbG9yLWJnLFxuXG4gIGhlYWRlci1mZzogY29sb3Itd2hpdGUsXG4gIGhlYWRlci1iZzogY29sb3ItYmcsXG5cbiAgZm9vdGVyLWZnOiBjb2xvci1mZyxcbiAgZm9vdGVyLWJnOiBjb2xvci1iZyxcblxuICBhY3Rpb25zLWZnOiBjb2xvci1mZyxcbiAgYWN0aW9ucy1iZzogY29sb3ItYmcsXG5cbiAgdXNlci1mZzogY29sb3ItYmcsXG4gIHVzZXItYmc6IGNvbG9yLWZnLFxuICB1c2VyLWZnLWhpZ2hsaWdodDogY29sb3ItZmctaGlnaGxpZ2h0LFxuXG4gIHBvcG92ZXItYm9yZGVyOiBjb2xvci1wcmltYXJ5LFxuICBwb3BvdmVyLXNoYWRvdzogc2hhZG93LFxuXG4gIGNvbnRleHQtbWVudS1hY3RpdmUtYmc6IGNvbG9yLXByaW1hcnksXG5cbiAgZm9vdGVyLWhlaWdodDogaGVhZGVyLWhlaWdodCxcblxuICBzaWRlYmFyLXdpZHRoOiAxNi4yNXJlbSxcbiAgc2lkZWJhci13aWR0aC1jb21wYWN0OiAzLjQ1cmVtLFxuXG4gIG1lbnUtZmc6IGNvbG9yLWZnLFxuICBtZW51LWJnOiBjb2xvci1iZyxcbiAgbWVudS1hY3RpdmUtZmc6IGNvbG9yLXdoaXRlLFxuICBtZW51LWdyb3VwLWZnOiBjb2xvci13aGl0ZSxcbiAgbWVudS1mb250LXdlaWdodDogZm9udC13ZWlnaHQtbm9ybWFsLFxuICBtZW51LWFjdGl2ZS1mb250LXdlaWdodDogZm9udC13ZWlnaHQtYm9sZGVyLFxuICBtZW51LXN1Ym1lbnUtYmc6IGxheW91dC1iZyxcbiAgbWVudS1zdWJtZW51LWZnOiBjb2xvci1mZyxcbiAgbWVudS1zdWJtZW51LWFjdGl2ZS1mZzogY29sb3ItZmctaGVhZGluZyxcbiAgbWVudS1zdWJtZW51LWFjdGl2ZS1iZzogcmdiYSgwLCAyNTUsIDE3MCwgMC4yNSksXG4gIG1lbnUtc3VibWVudS1hY3RpdmUtYm9yZGVyLWNvbG9yOiBjb2xvci1mZy1oaWdobGlnaHQsXG4gIG1lbnUtc3VibWVudS1hY3RpdmUtc2hhZG93OiAwIDJweCAxMnB4IDAgcmdiYSgwLCAyNTUsIDE3MCwgMC4yNSksXG4gIG1lbnUtaXRlbS1wYWRkaW5nOiAwLjI1cmVtIDAuNzVyZW0sXG4gIG1lbnUtaXRlbS1zZXBhcmF0b3I6IHRyYW5zcGFyZW50LFxuXG4gIGJ0bi1oZXJvLXNoYWRvdzogMCA0cHggMTBweCAwIHJnYmEoMzMsIDcsIDc3LCAwLjUpLFxuICBidG4taGVyby10ZXh0LXNoYWRvdzogMCAxcHggM3B4IHJnYmEoMCwgMCwgMCwgMC4zKSxcbiAgYnRuLWhlcm8tYmV2ZWwtc2l6ZTogMCAzcHggMCAwLFxuICBidG4taGVyby1nbG93LXNpemU6IDAgMnB4IDhweCAwLFxuICBidG4taGVyby1wcmltYXJ5LWdsb3ctc2l6ZTogYnRuLWhlcm8tZ2xvdy1zaXplLFxuICBidG4taGVyby1zdWNjZXNzLWdsb3ctc2l6ZTogYnRuLWhlcm8tZ2xvdy1zaXplLFxuICBidG4taGVyby13YXJuaW5nLWdsb3ctc2l6ZTogYnRuLWhlcm8tZ2xvdy1zaXplLFxuICBidG4taGVyby1pbmZvLWdsb3ctc2l6ZTogYnRuLWhlcm8tZ2xvdy1zaXplLFxuICBidG4taGVyby1kYW5nZXItZ2xvdy1zaXplOiBidG4taGVyby1nbG93LXNpemUsXG4gIGJ0bi1oZXJvLXNlY29uZGFyeS1nbG93LXNpemU6IGJ0bi1oZXJvLWdsb3ctc2l6ZSxcbiAgYnRuLXNlY29uZGFyeS1ib3JkZXI6IGNvbG9yLXByaW1hcnksXG4gIGJ0bi1vdXRsaW5lLWZnOiBjb2xvci1mZy1oZWFkaW5nLFxuICBidG4tb3V0bGluZS1ob3Zlci1mZzogY29sb3ItZmctaGVhZGluZyxcbiAgYnRuLW91dGxpbmUtZm9jdXMtZmc6IGNvbG9yLWZnLWhlYWRpbmcsXG4gIGJ0bi1ncm91cC1iZzogIzM3MzI3MyxcbiAgYnRuLWdyb3VwLXNlcGFyYXRvcjogIzMxMmM2NixcblxuICBmb3JtLWNvbnRyb2wtYmc6ICMzNzMxN2EsXG4gIGZvcm0tY29udHJvbC1mb2N1cy1iZzogc2VwYXJhdG9yLFxuICBmb3JtLWNvbnRyb2wtYm9yZGVyLWNvbG9yOiBzZXBhcmF0b3IsXG4gIGZvcm0tY29udHJvbC1zZWxlY3RlZC1ib3JkZXItY29sb3I6IGNvbG9yLXByaW1hcnksXG5cbiAgY2hlY2tib3gtYmc6IHRyYW5zcGFyZW50LFxuICBjaGVja2JveC1zaXplOiAxLjI1cmVtLFxuICBjaGVja2JveC1ib3JkZXItc2l6ZTogMnB4LFxuICBjaGVja2JveC1ib3JkZXItY29sb3I6IGNvbG9yLWZnLFxuICBjaGVja2JveC1jaGVja21hcms6IHRyYW5zcGFyZW50LFxuXG4gIGNoZWNrYm94LWNoZWNrZWQtYmc6IHRyYW5zcGFyZW50LFxuICBjaGVja2JveC1jaGVja2VkLXNpemU6IDEuMjVyZW0sXG4gIGNoZWNrYm94LWNoZWNrZWQtYm9yZGVyLXNpemU6IDJweCxcbiAgY2hlY2tib3gtY2hlY2tlZC1ib3JkZXItY29sb3I6IGNvbG9yLXN1Y2Nlc3MsXG4gIGNoZWNrYm94LWNoZWNrZWQtY2hlY2ttYXJrOiBjb2xvci1mZy1oZWFkaW5nLFxuXG4gIGNoZWNrYm94LWRpc2FibGVkLWJnOiB0cmFuc3BhcmVudCxcbiAgY2hlY2tib3gtZGlzYWJsZWQtc2l6ZTogMS4yNXJlbSxcbiAgY2hlY2tib3gtZGlzYWJsZWQtYm9yZGVyLXNpemU6IDJweCxcbiAgY2hlY2tib3gtZGlzYWJsZWQtYm9yZGVyLWNvbG9yOiBjb2xvci1mZy1oZWFkaW5nLFxuICBjaGVja2JveC1kaXNhYmxlZC1jaGVja21hcms6IGNvbG9yLWZnLWhlYWRpbmcsXG5cbiAgc2VhcmNoLWJnOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICMxNzE3NDksICM0MTM3ODkpLFxuXG4gIHNtYXJ0LXRhYmxlLWhlYWRlci1mb250LXdlaWdodDogZm9udC13ZWlnaHQtbm9ybWFsLFxuICBzbWFydC10YWJsZS1oZWFkZXItYmc6IGNvbG9yLWJnLWFjdGl2ZSxcbiAgc21hcnQtdGFibGUtYmctZXZlbjogIzNhMzQ3YSxcbiAgc21hcnQtdGFibGUtYmctYWN0aXZlOiBjb2xvci1iZy1hY3RpdmUsXG5cbiAgc21hcnQtdGFibGUtcGFnaW5nLWJvcmRlci1jb2xvcjogY29sb3ItcHJpbWFyeSxcbiAgc21hcnQtdGFibGUtcGFnaW5nLWJvcmRlci13aWR0aDogMnB4LFxuICBzbWFydC10YWJsZS1wYWdpbmctZmctYWN0aXZlOiBjb2xvci1mZy1oZWFkaW5nLFxuICBzbWFydC10YWJsZS1wYWdpbmctYmctYWN0aXZlOiBjb2xvci1wcmltYXJ5LFxuICBzbWFydC10YWJsZS1wYWdpbmctaG92ZXI6IHJnYmEoMCwgMCwgMCwgMC4yKSxcblxuICBiYWRnZS1mZy10ZXh0OiBjb2xvci13aGl0ZSxcbiAgYmFkZ2UtcHJpbWFyeS1iZy1jb2xvcjogY29sb3ItcHJpbWFyeSxcbiAgYmFkZ2Utc3VjY2Vzcy1iZy1jb2xvcjogY29sb3Itc3VjY2VzcyxcbiAgYmFkZ2UtaW5mby1iZy1jb2xvcjogY29sb3ItaW5mbyxcbiAgYmFkZ2Utd2FybmluZy1iZy1jb2xvcjogY29sb3Itd2FybmluZyxcbiAgYmFkZ2UtZGFuZ2VyLWJnLWNvbG9yOiBjb2xvci1kYW5nZXIsXG5cbiAgc3Bpbm5lci1iZzogcmdiYSg2MSwgNTUsIDEyOCwgMC45KSxcbiAgc3RlcHBlci1hY2NlbnQtY29sb3I6IGNvbG9yLXN1Y2Nlc3MsXG5cbiAgY2FsZW5kYXItYWN0aXZlLWl0ZW0tYmc6IGNvbG9yLXByaW1hcnksXG4gIGNhbGVuZGFyLXNlbGVjdGVkLWl0ZW0tYmc6IGNvbG9yLXByaW1hcnksXG4gIGNhbGVuZGFyLXJhbmdlLWJnLWluLXJhbmdlOiAjNGU0MDk1LFxuICBjYWxlbmRhci10b2RheS1pdGVtLWJnOiAjMzUyZjZlLFxuKTtcblxuLy8gcmVnaXN0ZXIgdGhlIHRoZW1lXG4kbmItdGhlbWVzOiBuYi1yZWdpc3Rlci10aGVtZSgkdGhlbWUsIGNvc21pYywgZGVmYXVsdCk7XG4iLCIvKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG5cbkBpbXBvcnQgJy4uL2NvcmUvZnVuY3Rpb25zJztcbkBpbXBvcnQgJy4uL2NvcmUvbWl4aW5zJztcbkBpbXBvcnQgJ2RlZmF1bHQnO1xuXG4vLyBkZWZhdWx0IHRoZSBiYXNlIHRoZW1lXG4kdGhlbWU6IChcbiAgaGVhZGVyLWZnOiAjZjdmYWZiLFxuICBoZWFkZXItYmc6ICMxMTEyMTgsXG5cbiAgbGF5b3V0LWJnOiAjZjFmNWY4LFxuXG4gIGNvbG9yLWZnLWhlYWRpbmc6ICMxODE4MTgsXG4gIGNvbG9yLWZnLXRleHQ6ICM0YjRiNGIsXG4gIGNvbG9yLWZnLWhpZ2hsaWdodDogY29sb3ItZmcsXG5cbiAgc2VwYXJhdG9yOiAjY2RkNWRjLFxuXG4gIHJhZGl1czogMC4xN3JlbSxcblxuICBzY3JvbGxiYXItYmc6ICNlM2U5ZWUsXG5cbiAgY29sb3ItcHJpbWFyeTogIzczYTFmZixcbiAgY29sb3Itc3VjY2VzczogIzVkY2ZlMyxcbiAgY29sb3ItaW5mbzogI2JhN2ZlYyxcbiAgY29sb3Itd2FybmluZzogI2ZmYTM2YixcbiAgY29sb3ItZGFuZ2VyOiAjZmY2YjgzLFxuXG4gIGJ0bi1zZWNvbmRhcnktYmc6ICNlZGYyZjUsXG4gIGJ0bi1zZWNvbmRhcnktYm9yZGVyOiAjZWRmMmY1LFxuXG4gIGFjdGlvbnMtZmc6ICNkM2RiZTUsXG4gIGFjdGlvbnMtYmc6IGNvbG9yLWJnLFxuXG4gIHNpZGViYXItYmc6ICNlM2U5ZWUsXG5cbiAgYm9yZGVyLWNvbG9yOiAjZDVkYmUwLFxuXG4gIG1lbnUtZm9udC13ZWlnaHQ6IGZvbnQtd2VpZ2h0LWJvbGRlcixcbiAgbWVudS1mZzogY29sb3ItZmctdGV4dCxcbiAgbWVudS1iZzogI2UzZTllZSxcbiAgbWVudS1hY3RpdmUtZmc6IGNvbG9yLWZnLWhlYWRpbmcsXG4gIG1lbnUtYWN0aXZlLWJnOiBtZW51LWJnLFxuXG4gIG1lbnUtc3VibWVudS1iZzogbWVudS1iZyxcbiAgbWVudS1zdWJtZW51LWZnOiBjb2xvci1mZy10ZXh0LFxuICBtZW51LXN1Ym1lbnUtYWN0aXZlLWZnOiBjb2xvci1mZy1oZWFkaW5nLFxuICBtZW51LXN1Ym1lbnUtYWN0aXZlLWJnOiAjY2RkNWRjLFxuICBtZW51LXN1Ym1lbnUtYWN0aXZlLWJvcmRlci1jb2xvcjogbWVudS1zdWJtZW51LWFjdGl2ZS1iZyxcbiAgbWVudS1zdWJtZW51LWFjdGl2ZS1zaGFkb3c6IG5vbmUsXG4gIG1lbnUtc3VibWVudS1ob3Zlci1mZzogbWVudS1zdWJtZW51LWFjdGl2ZS1mZyxcbiAgbWVudS1zdWJtZW51LWhvdmVyLWJnOiBtZW51LWJnLFxuICBtZW51LXN1Ym1lbnUtaXRlbS1ib3JkZXItd2lkdGg6IDAuMTI1cmVtLFxuICBtZW51LXN1Ym1lbnUtaXRlbS1ib3JkZXItcmFkaXVzOiByYWRpdXMsXG4gIG1lbnUtc3VibWVudS1pdGVtLXBhZGRpbmc6IDAuNXJlbSAxcmVtLFxuICBtZW51LXN1Ym1lbnUtaXRlbS1jb250YWluZXItcGFkZGluZzogMCAxLjI1cmVtLFxuICBtZW51LXN1Ym1lbnUtcGFkZGluZzogMC41cmVtLFxuXG4gIGJ0bi1ib3JkZXItcmFkaXVzOiBidG4tc2VtaS1yb3VuZC1ib3JkZXItcmFkaXVzLFxuXG4gIGJ0bi1oZXJvLWRlZ3JlZTogMGRlZyxcbiAgYnRuLWhlcm8tcHJpbWFyeS1kZWdyZWU6IGJ0bi1oZXJvLWRlZ3JlZSxcbiAgYnRuLWhlcm8tc3VjY2Vzcy1kZWdyZWU6IGJ0bi1oZXJvLWRlZ3JlZSxcbiAgYnRuLWhlcm8td2FybmluZy1kZWdyZWU6IGJ0bi1oZXJvLWRlZ3JlZSxcbiAgYnRuLWhlcm8taW5mby1kZWdyZWU6IGJ0bi1oZXJvLWRlZ3JlZSxcbiAgYnRuLWhlcm8tZGFuZ2VyLWRlZ3JlZTogYnRuLWhlcm8tZGVncmVlLFxuICBidG4taGVyby1zZWNvbmRhcnktZGVncmVlOiBidG4taGVyby1kZWdyZWUsXG4gIGJ0bi1oZXJvLWdsb3ctc2l6ZTogMCAwIDIwcHggMCxcbiAgYnRuLWhlcm8tcHJpbWFyeS1nbG93LXNpemU6IGJ0bi1oZXJvLWdsb3ctc2l6ZSxcbiAgYnRuLWhlcm8tc3VjY2Vzcy1nbG93LXNpemU6IGJ0bi1oZXJvLWdsb3ctc2l6ZSxcbiAgYnRuLWhlcm8td2FybmluZy1nbG93LXNpemU6IGJ0bi1oZXJvLWdsb3ctc2l6ZSxcbiAgYnRuLWhlcm8taW5mby1nbG93LXNpemU6IGJ0bi1oZXJvLWdsb3ctc2l6ZSxcbiAgYnRuLWhlcm8tZGFuZ2VyLWdsb3ctc2l6ZTogYnRuLWhlcm8tZ2xvdy1zaXplLFxuICBidG4taGVyby1zZWNvbmRhcnktZ2xvdy1zaXplOiAwIDAgMCAwLFxuICBidG4taGVyby1ib3JkZXItcmFkaXVzOiBidG4tYm9yZGVyLXJhZGl1cyxcblxuICBjYXJkLXNoYWRvdzogbm9uZSxcbiAgY2FyZC1ib3JkZXItd2lkdGg6IDFweCxcbiAgY2FyZC1ib3JkZXItY29sb3I6IGJvcmRlci1jb2xvcixcbiAgY2FyZC1oZWFkZXItYm9yZGVyLXdpZHRoOiAwLFxuXG4gIGxpbmstY29sb3I6ICM1ZGNmZTMsXG4gIGxpbmstY29sb3ItaG92ZXI6ICM3ZGNmZTMsXG4gIGxpbmstY29sb3ItdmlzaXRlZDogbGluay1jb2xvcixcblxuICBhY3Rpb25zLXNlcGFyYXRvcjogI2YxZjRmNSxcblxuICBtb2RhbC1zZXBhcmF0b3I6IGJvcmRlci1jb2xvcixcblxuICB0YWJzLXNlbGVjdGVkOiBjb2xvci1wcmltYXJ5LFxuICB0YWJzLXNlcGFyYXRvcjogI2ViZWNlZSxcblxuICBzbWFydC10YWJsZS1wYWdpbmctYmctYWN0aXZlOiBjb2xvci1wcmltYXJ5LFxuXG4gIHJvdXRlLXRhYnMtc2VsZWN0ZWQ6IGNvbG9yLXByaW1hcnksXG5cbiAgcG9wb3Zlci1ib3JkZXI6IGNvbG9yLXByaW1hcnksXG5cbiAgZm9vdGVyLXNoYWRvdzogbm9uZSxcbiAgZm9vdGVyLXNlcGFyYXRvcjogYm9yZGVyLWNvbG9yLFxuICBmb290ZXItZmctaGlnaGxpZ2h0OiAjMmEyYTJhLFxuXG4gIGNhbGVuZGFyLXRvZGF5LWl0ZW0tYmc6ICNhMmIyYzcsXG4gIGNhbGVuZGFyLWFjdGl2ZS1pdGVtLWJnOiBjb2xvci1wcmltYXJ5LFxuICBjYWxlbmRhci1yYW5nZS1iZy1pbi1yYW5nZTogI2UzZWNmZSxcbiAgY2FsZW5kYXItdG9kYXktZmc6IGNvbG9yLXdoaXRlLFxuKTtcblxuLy8gcmVnaXN0ZXIgdGhlIHRoZW1lXG4kbmItdGhlbWVzOiBuYi1yZWdpc3Rlci10aGVtZSgkdGhlbWUsIGNvcnBvcmF0ZSwgZGVmYXVsdCk7XG4iLCIvKipcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG5cbiRncmlkLWNvbHVtbnM6IDEyICFkZWZhdWx0O1xuJGdyaWQtZ3V0dGVyLXdpZHRoLWJhc2U6IDI0cHggIWRlZmF1bHQ7XG4kZ3JpZC1ndXR0ZXItd2lkdGhzOiAoXG4gIHhzOiAkZ3JpZC1ndXR0ZXItd2lkdGgtYmFzZSxcbiAgc206ICRncmlkLWd1dHRlci13aWR0aC1iYXNlLFxuICBtZDogJGdyaWQtZ3V0dGVyLXdpZHRoLWJhc2UsXG4gIGxnOiAkZ3JpZC1ndXR0ZXItd2lkdGgtYmFzZSxcbiAgeGw6ICRncmlkLWd1dHRlci13aWR0aC1iYXNlXG4pICFkZWZhdWx0O1xuXG5cbiRncmlkLWJyZWFrcG9pbnRzOiAoXG4gIHhzOiAwLFxuICBpczogNDAwcHgsXG4gIHNtOiA1NzZweCxcbiAgbWQ6IDc2OHB4LFxuICBsZzogOTkycHgsXG4gIHhsOiAxMjAwcHgsXG4gIHh4bDogMTQwMHB4LFxuICB4eHhsOiAxNjAwcHhcbik7XG5cbiRjb250YWluZXItbWF4LXdpZHRoczogKFxuICBpczogMzgwcHgsXG4gIHNtOiA1NDBweCxcbiAgbWQ6IDcyMHB4LFxuICBsZzogOTYwcHgsXG4gIHhsOiAxMTQwcHgsXG4gIHh4bDogMTMyMHB4LFxuICB4eHhsOiAxNTAwcHhcbik7XG4iLCJAaW1wb3J0ICcuLi8uLi8uLi8uLi8uLi8uLi9AdGhlbWUvc3R5bGVzL3RoZW1lcyc7XG5AaW1wb3J0ICd+Ym9vdHN0cmFwL3Njc3MvbWl4aW5zL2JyZWFrcG9pbnRzJztcbkBpbXBvcnQgJ35AbmVidWxhci90aGVtZS9zdHlsZXMvZ2xvYmFsL2Jvb3RzdHJhcC9icmVha3BvaW50cyc7XG5cbkBpbmNsdWRlIG5iLWluc3RhbGwtY29tcG9uZW50KCkge1xuLmhpe1xuICAgIGJvcmRlcjogMnB4IHNvbGlkO1xufVxudGFibGV7XG4gICAgd2lkdGg6IC13ZWJraXQtZmlsbC1hdmFpbGFibGU7XG59XG5he1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbn1cbnRleHRhcmVhe1xuICAgIC8vIGJvcmRlcjogbm9uZTtcbn1cbm5iLWNhcmR7XG4gICAgYm9yZGVyOiBub25lO1xufVxubmItY2FyZC1ib2R5e1xuICAgIGJhY2tncm91bmQtY29sb3I6I0UxRjVGRTtcbiAgICAvLyBjb2xvcjp3aGl0ZTtcbiAgfVxuICBuYi1jYXJkLWhlYWRlcntcbiAgICBiYWNrZ3JvdW5kOiAjRTFGNUZFO1xuICAgIC8vIGNvbG9yOiB3aGl0ZTtcbiAgfVxuICAvZGVlcC8gLm1vZGFsLWRpYWxvZyB7XG4gICAgbWF4LXdpZHRoOiA0NSUgO1xuICB9XG5cbiAgXG4vZGVlcC8gbmcyLXNtYXJ0LXRhYmxlIGl7XG4gICAgY29sb3I6IzAwMDtcbiAgfVxuICBcbiAgL2RlZXAvIG5nMi1zbWFydC10YWJsZSAubmcyLXNtYXJ0LXRpdGxle1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgfVxuICBcbiAgL2RlZXAvIG5nMi1zbWFydC10YWJsZSAubmcyLXNtYXJ0LXBhZ2luYXRpb24tbmF2IC5uZzItc21hcnQtcGFnaW5hdGlvbiB7XG4gICBiYWNrZ3JvdW5kLWNvbG9yOiMwMDA7XG4gIH1cbiAgL2RlZXAvIG5nMi1zbWFydC10YWJsZSBpe1xuICAgIGNvbG9yOiAjMDAwO1xuICB9XG5cbiAgL2RlZXAvIG1vZGFsIC5tb2RhbC1kaWFsb2d7XG4gICAgbWF4LXdpZHRoOiAxMDAwcHggIWltcG9ydGFudDtcbiAgfVxuICAvZGVlcC8gbmcyLXN0LXRib2R5LWVkaXQtZGVsZXRlIHtcbiAgICBkaXNwbGF5OiAtd2Via2l0LWlubGluZS1ib3ggIWltcG9ydGFudDtcbiAgICBoZWlnaHQ6IDBweCAhaW1wb3J0YW50O1xuICAgIGZsb2F0OiBsZWZ0O1xuICAgIG1hcmdpbi1sZWZ0OiAzNnB4O1xuICAgIG1hcmdpbi10b3A6IC0xNXB4ICFpbXBvcnRhbnQ7XG4gICAgLyogbWFyZ2luOiBhdXRvOyAqL1xuICAgfVxuICAgXG4gICAgL2RlZXAvIG5nMi1zdC10Ym9keS1jdXN0b20gYS5uZzItc21hcnQtYWN0aW9uLm5nMi1zbWFydC1hY3Rpb24tY3VzdG9tLWN1c3RvbSB7XG4gICAgLy8gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICAgd2lkdGg6IDUwcHg7XG4gICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgZm9udC1zaXplOiAwLjFlbTtcbiAgIH1cbiAgIC9kZWVwLyBuZzItc3QtdGJvZHktY3VzdG9tIGEubmcyLXNtYXJ0LWFjdGlvbi5uZzItc21hcnQtYWN0aW9uLWN1c3RvbS1jdXN0b20gaW1ne1xuICAgIHdpZHRoOiA1NyUgIWltcG9ydGFudDtcbiAgICBtYXJnaW4tbGVmdDogMzRweDtcbiAgIH1cbiAgL2RlZXAvIG5nMi1zdC10Ym9keS1jdXN0b20gYS5uZzItc21hcnQtYWN0aW9uLm5nMi1zbWFydC1hY3Rpb24tY3VzdG9tLWN1c3RvbTpob3ZlciB7XG4gICAgIGNvbG9yOiAjNWRjZmUzO1xuICAgfVxuIFxuICAgICAvZGVlcC8gbmcyLXN0LXRib2R5LWVkaXQtZGVsZXRlIGkge1xuICAgICAgcG9zaXRpb246IHJlbGF0aXZlICFpbXBvcnRhbnQ7XG4gICAgICB0b3A6IC0xNHB4IDtcbiAgICAgIG1hcmdpbi1sZWZ0OiA0MHB4O1xuICAgfVxuICAgYnV0dG9ue1xuICAgIGZsb2F0OiByaWdodDtcbiAgfVxufVxuXG5pbnB1dFt0eXBlPW51bWJlcl06Oi13ZWJraXQtaW5uZXItc3Bpbi1idXR0b24sIFxuaW5wdXRbdHlwZT1udW1iZXJdOjotd2Via2l0LW91dGVyLXNwaW4tYnV0dG9uIHsgXG4gIC13ZWJraXQtYXBwZWFyYW5jZTogbm9uZTsgXG4gIG1hcmdpbjogMDsgXG59Il19 */"

/***/ }),

/***/ "./src/app/pages/ui-features/customer/customers/detail-customers/customer-details/customer-details.component.ts":
/*!**********************************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/customer/customers/detail-customers/customer-details/customer-details.component.ts ***!
  \**********************************************************************************************************************/
/*! exports provided: CustomerDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerDetailsComponent", function() { return CustomerDetailsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var angular_custom_modal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular-custom-modal */ "./node_modules/angular-custom-modal/angular-custom-modal.es5.js");
/* harmony import */ var _confirmation_dialog_confirmation_dialog_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../../confirmation-dialog/confirmation-dialog.service */ "./src/app/confirmation-dialog/confirmation-dialog.service.ts");
/* harmony import */ var _services_customer_customer_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../../../services/customer/customer.service */ "./src/app/services/customer/customer.service.ts");
/* harmony import */ var _services_customer_site_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../../../services/customer/site.service */ "./src/app/services/customer/site.service.ts");
/* harmony import */ var _services_token_token_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../../../services/token/token.service */ "./src/app/services/token/token.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








var CustomerDetailsComponent = /** @class */ (function () {
    function CustomerDetailsComponent(CustomerServ, token, SiteServ, confirmationDialogService) {
        this.CustomerServ = CustomerServ;
        this.token = token;
        this.SiteServ = SiteServ;
        this.confirmationDialogService = confirmationDialogService;
        this.customer = {};
        this.site = {};
        this.sitreTableData = [];
        this.source = new ng2_smart_table__WEBPACK_IMPORTED_MODULE_2__["LocalDataSource"]();
        // onUserRowSelect(data) {
        //   if (data.isSelected) {
        //     this.listOfSite.forEach(value => {
        //       value.PartDetails.forEach(part => {
        //         if (part.PartDetail === data.data.partName) {
        //           this.sitreTableData.push(value)
        //           return;
        //         }
        //       })
        //     })
        //     return this.source.load(this.sitreTableData);
        //   }
        // }
        this.moduleCreate = true;
        this.moduleView = true;
        this.moduleEdit = true;
        this.getCustomerDetail();
    }
    CustomerDetailsComponent.prototype.updateCols = function ($event) {
        this.hideCols = '';
        var i = 0;
        for (var colI in this.settings.columns) {
            if (this.settings.columns[colI].isSelected) {
                i++;
            }
            else {
                i++;
                this.hideCols += "hd-cl-" + i + " ";
            }
        }
    };
    /**
     * Get Site Details
     */
    CustomerDetailsComponent.prototype.getSiteInfo = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, error_1;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        _a = this;
                        return [4 /*yield*/, this.SiteServ.getSite(this.id)];
                    case 1:
                        _a.listOfSite = _b.sent();
                        this.listOfSite.reverse();
                        this.listOfSite.map(function (data, index) { return data.S_NO = index + 1; });
                        this.source.load(this.listOfSite);
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _b.sent();
                        console.error(error_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Edit Site Details
     */
    CustomerDetailsComponent.prototype.onUserRowSelect = function (event) {
        var data = event.data;
        this.site = data;
        if (!data) {
            return this.componentInsideModalClients.open();
        }
        this.componentInsideModalClientSecond.open();
    };
    CustomerDetailsComponent.prototype.editSite = function (event, touch) {
        return __awaiter(this, void 0, void 0, function () {
            var resultAleart, value, result, error_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (touch) {
                            this.componentInsideModalClientSecond.close();
                            return [2 /*return*/];
                        }
                        return [4 /*yield*/, this.confirmationDialogService.confirm('Alert Message', 'Are you sure you want to Update?')];
                    case 1:
                        resultAleart = _a.sent();
                        if (!resultAleart) return [3 /*break*/, 5];
                        value = event.value;
                        _a.label = 2;
                    case 2:
                        _a.trys.push([2, 4, , 5]);
                        return [4 /*yield*/, this.SiteServ.UpdateSiteDetails(value, this.id)];
                    case 3:
                        result = _a.sent();
                        // console.log(result, "update");
                        this.SiteServ.userSite.next(result);
                        //  const pushValue =  this.listOfSite.push(result);
                        //  console.log(pushValue);
                        this.source.load(this.listOfSite);
                        this.componentInsideModalClientSecond.close();
                        return [3 /*break*/, 5];
                    case 4:
                        error_2 = _a.sent();
                        console.log(error_2);
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    CustomerDetailsComponent.prototype.onCreateConfirm = function (event) {
        return __awaiter(this, void 0, void 0, function () {
            var FullResult, resultAleart, result, error_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        FullResult = Object.assign({ id: this.id }, event.value);
                        if (FullResult.siteName === undefined) {
                            return [2 /*return*/];
                        }
                        return [4 /*yield*/, this.confirmationDialogService.confirm('Alert Message', 'Are you sure you want to Create?')];
                    case 1:
                        resultAleart = _a.sent();
                        if (!resultAleart) return [3 /*break*/, 6];
                        _a.label = 2;
                    case 2:
                        _a.trys.push([2, 4, , 5]);
                        return [4 /*yield*/, this.SiteServ.createSite(FullResult)];
                    case 3:
                        result = _a.sent();
                        this.listOfSite.push(result);
                        // console.log(result)
                        this.source.load(this.listOfSite);
                        this.componentInsideModalClients.close();
                        return [3 /*break*/, 5];
                    case 4:
                        error_3 = _a.sent();
                        console.log(error_3);
                        return [3 /*break*/, 5];
                    case 5:
                        this.componentInsideModalClients.close();
                        return [3 /*break*/, 7];
                    case 6:
                        this.componentInsideModalClients.close();
                        _a.label = 7;
                    case 7: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Get Customer Detail
     */
    CustomerDetailsComponent.prototype.getCustomerDetail = function () {
        var _this = this;
        this.CustomerServ.userCustomer
            .subscribe(function (data) {
            if (!data.companyName) {
                return _this.getCustomerData();
            }
            _this.customer = data;
            _this.id = data.id;
            _this.getSiteInfo();
            // this.editShow = data.creatShow;
        }, function (error) {
            // console.log(error);
        });
    };
    CustomerDetailsComponent.prototype.getCustomerData = function () {
        var _this = this;
        this.CustomerServ.userCustomer1
            .subscribe(function (data) {
            _this.customer = data;
            _this.id = data.id;
            _this.getSiteInfo();
            // this.editShow = data.creatShow;
        }, function (error) {
            // console.log(error);
        });
    };
    /*
    *Edit  Customer
   **/
    CustomerDetailsComponent.prototype.editCustomer = function (customerData, touch) {
        return __awaiter(this, void 0, void 0, function () {
            var resultAleart, value, result, error_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (touch) {
                            return [2 /*return*/];
                        }
                        return [4 /*yield*/, this.confirmationDialogService.confirm('Alert Message', 'Are you sure you want to Update?')];
                    case 1:
                        resultAleart = _a.sent();
                        if (!resultAleart) return [3 /*break*/, 6];
                        value = customerData.value;
                        _a.label = 2;
                    case 2:
                        _a.trys.push([2, 4, , 5]);
                        return [4 /*yield*/, this.CustomerServ.UpdateCustomerDetails(value, this.id)];
                    case 3:
                        result = _a.sent();
                        this.CustomerServ.userCustomer.next(result);
                        return [3 /*break*/, 5];
                    case 4:
                        error_4 = _a.sent();
                        console.log(error_4);
                        return [3 /*break*/, 5];
                    case 5: return [3 /*break*/, 7];
                    case 6:
                        customerData.confirm.reject();
                        _a.label = 7;
                    case 7: return [2 /*return*/];
                }
            });
        });
    };
    /**
     *  Create,Delet And Edit Customer Site Details
     */
    CustomerDetailsComponent.prototype.onDeleteConfirm = function (event) {
        return __awaiter(this, void 0, void 0, function () {
            var resultAlert;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.confirmationDialogService.confirm('Alert Message', 'Are you sure you want to delete?')];
                    case 1:
                        resultAlert = _a.sent();
                        if (resultAlert) {
                            event.confirm.resolve();
                        }
                        else {
                            event.confirm.reject();
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    CustomerDetailsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.getSiteInfo();
        if (this.token.userAccess !== undefined) {
            this.token.userAccess
                .subscribe(function (data) {
                data.forEach(function (ele) {
                    if (ele.name === 'Client') {
                        _this.moduleCreate = ele.create;
                        _this.moduleEdit = ele.edit;
                        _this.moduleView = ele.view;
                    }
                });
            }, function (error) {
                console.log(error);
            });
        }
        if (this.moduleCreate || this.moduleEdit) {
            // console.log('false')
            this.settings = {
                // hideSubHeader: true,
                add: {
                    addButtonContent: '<i class="nb-plus"></i>',
                    createButtonContent: '<i class="nb-checkmark"></i>',
                    cancelButtonContent: '<i class="nb-close"></i>',
                    confirmCreate: true,
                    hideSubHeader: true,
                },
                edit: {
                    editButtonContent: '<i class="nb-edit"></i>',
                    saveButtonContent: '<i class="nb-checkmark"></i>',
                    cancelButtonContent: '<i class="nb-close"></i>',
                    confirmEdit: true,
                },
                delete: {
                    deleteButtonContent: '<i class="nb-trash"></i>',
                    confirmDelete: true,
                },
                actions: {
                    edit: false,
                    add: false,
                    delete: false,
                    custom: [
                        {
                            name: 'view',
                            title: '<i class="nb-edit"></i>',
                        },
                    ],
                },
                columns: {
                    siteName: {
                        title: 'Site Name',
                        type: 'string',
                    },
                    address: {
                        title: 'Address',
                        type: 'string',
                    },
                    mobile: {
                        title: 'Site Contact Number',
                        type: 'string',
                    },
                    personName: {
                        title: 'Person Name',
                        type: 'string',
                    },
                    personMobile: {
                        title: 'Contact Person Number',
                        type: 'string',
                    },
                    email: {
                        title: 'Email',
                        type: 'string',
                    },
                },
            };
        }
        else {
            this.settings = {
                // hideSubHeader: true,
                add: {
                    addButtonContent: '<i class="nb-plus"></i>',
                    createButtonContent: '<i class="nb-checkmark"></i>',
                    cancelButtonContent: '<i class="nb-close"></i>',
                    confirmCreate: true,
                    hideSubHeader: true,
                },
                edit: {
                    editButtonContent: '<i class="nb-edit"></i>',
                    saveButtonContent: '<i class="nb-checkmark"></i>',
                    cancelButtonContent: '<i class="nb-close"></i>',
                    confirmEdit: true,
                },
                delete: {
                    deleteButtonContent: '<i class="nb-trash"></i>',
                    confirmDelete: true,
                },
                actions: {
                    edit: false,
                    add: false,
                    delete: false,
                },
                columns: {
                    siteName: {
                        title: 'Site Name',
                        type: 'string',
                    },
                    address: {
                        title: 'Address',
                        type: 'string',
                    },
                    mobile: {
                        title: 'Site Contact Number',
                        type: 'string',
                    },
                    personName: {
                        title: 'Person Name',
                        type: 'string',
                    },
                    personMobile: {
                        title: 'Contact Person Number',
                        type: 'string',
                    },
                    email: {
                        title: 'Email',
                        type: 'string',
                    },
                },
            };
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('componentInsideModalClientSecond'),
        __metadata("design:type", angular_custom_modal__WEBPACK_IMPORTED_MODULE_3__["ModalComponent"])
    ], CustomerDetailsComponent.prototype, "componentInsideModalClientSecond", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('componentInsideModalClients'),
        __metadata("design:type", angular_custom_modal__WEBPACK_IMPORTED_MODULE_3__["ModalComponent"])
    ], CustomerDetailsComponent.prototype, "componentInsideModalClients", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('form'),
        __metadata("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgForm"])
    ], CustomerDetailsComponent.prototype, "form", void 0);
    CustomerDetailsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-customer-details',
            template: __webpack_require__(/*! ./customer-details.component.html */ "./src/app/pages/ui-features/customer/customers/detail-customers/customer-details/customer-details.component.html"),
            styles: [__webpack_require__(/*! ./customer-details.component.scss */ "./src/app/pages/ui-features/customer/customers/detail-customers/customer-details/customer-details.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_customer_customer_service__WEBPACK_IMPORTED_MODULE_5__["CustomerService"],
            _services_token_token_service__WEBPACK_IMPORTED_MODULE_7__["TokenService"],
            _services_customer_site_service__WEBPACK_IMPORTED_MODULE_6__["SiteService"],
            _confirmation_dialog_confirmation_dialog_service__WEBPACK_IMPORTED_MODULE_4__["ConfirmationDialogService"]])
    ], CustomerDetailsComponent);
    return CustomerDetailsComponent;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/customer/customers/detail-customers/customer-invoices/customer-invoices.component.html":
/*!**************************************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/customer/customers/detail-customers/customer-invoices/customer-invoices.component.html ***!
  \**************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\n    <br>\n    <div class=\"row\">\n        <div class=\"col-4\">\n            <p class=\"back\">Client Name: {{customer.companyName}}</p>\n        </div>\n        <div class=\"col-4\">\n            <label class=\"back\">Inv. Type</label>\n            <select class=\"form-group\" (change)=\"selectcategory(displayType)\" name=\"displayType\" style=\"float: right;\" [(ngModel)]='displayType'>\n                <option *ngFor=\"let category of catagorys\" [value]=\"category\">{{category}}</option>\n             </select>\n        </div>\n        <div class=\"col-4\">\n            <label class=\"back\">Total: {{totalAmt}}</label>\n        </div>\n    </div><br>\n    <button class=\"btn btn-outline-primary\" *ngIf='displayButton' shape=\"semi-round\" style=\"float:left\" (click)='showList()'>Create Invoices</button>\n    <button class=\"btn btn-outline-primary\" *ngIf='!displayButton' shape=\"semi-round\" style=\"float:right\" (click)='showAllInvoices()'>Show Invoices</button> <br>\n    <!-- (userRowSelect)=\"onUserRowSelect0($event)\" -->\n    <ng2-smart-table *ngIf='materials' [settings]=\"settings\" (editConfirm)=\"onSaveConfirm($event)\" [source]=\"sources\">\n    </ng2-smart-table><br>\n    <ng2-smart-table *ngIf='!materials' [settings]=\"settings1\" (custom)=\"onUserRowSelect1($event)\" (userRowSelect)=\"onUserRowSelect1($event)\" [source]=\"sources1\">\n    </ng2-smart-table><br>\n    <button class=\"btn btn-outline-primary\" *ngIf='materials' shape=\"semi-round\" style=\"float:left\" (click)='previewInvoice()'>Generate Proforma Invoice</button>\n    <button class=\"btn btn-outline-primary\" *ngIf='materials' shape=\"semi-round\" style=\"float:right\" (click)='finalInvoice()'>Generate Final invoice</button> <br>\n\n</div>\n\n\n<modal class=\"modals\" #componentInsideModal [closeOnOutsideClick]=\"false\">\n    <ng-template #modalHeader>\n        Select Materials\n    </ng-template>\n    <ng-template #modalBody>\n        <ng2-smart-table [settings]=\"setting\" (userRowSelect)=\"onUserRowSelect($event)\" [source]=\"source\">\n        </ng2-smart-table>\n        <br>\n        <button type=\"button\" (click)=\"displaySelectedItems()\" class=\"btn btn-outline-primary \" shape=\"semi-round\" style=\"float:right\">Done</button>\n        <button type=\"button\" (click)=\"closeSelectedItems()\" class=\"btn btn-outline-primary \" shape=\"semi-round\" style=\"float:left\">Cancel</button>\n    </ng-template>\n</modal>\n\n<modal class=\"modals\" #componentDispatchModal [closeOnOutsideClick]=\"false\">\n    <ng-template #modalHeader>\n        Dispatch Details\n    </ng-template>\n    <ng-template #modalBody>\n        <div class=\"row\">\n            <div class=\"col-sm-6 form-group input-group-sm\">\n                <label>Dispatched By: </label>\n                <input type=\"text\" class=\"form-control\" [(ngModel)]=\"dispatchBy\" (change)=\"selectEmployee(dispatchBy)\" placeholder=\"Enter Employee Name\">\n            </div>\n            <div class=\"col-sm-6 form-group input-group-sm\">\n                <label>Dispatched Date: </label>\n                <input type=\"date\" class=\"form-control\" [(ngModel)]=\"dispatchdate\" (change)=\"selectDate(dispatchdate)\" placeholder=\"Dispatch Date\">\n            </div>\n        </div>\n        <br>\n        <button type=\"button\" (click)=\"ShowInvoice()\" [disabled]='!detail' class=\"btn btn-outline-primary \" shape=\"semi-round\" style=\"float:right\">Done</button>\n        <button type=\"button\" (click)=\"HideInvoice()\" class=\"btn btn-outline-primary \" shape=\"semi-round\" style=\"float:left\">Cancel</button>\n    </ng-template>\n</modal>\n\n<modal class=\"modals\" #componentEwayModal [closeOnOutsideClick]=\"false\">\n    <ng-template #modalHeader>\n        E-Way Bill\n    </ng-template>\n    <ng-template #modalBody>\n        <nb-card>\n            <nb-card-body>\n                <div class=\"row\">\n                    <div class=\"col-sm-6\">\n                        <label for=\"ewayBill\">EWay Bill Number</label>\n                    </div>\n                    <div class=\"col-sm-6\">\n                        <a href=\"https://www.ewaybillgst.gov.in\" target=\"_blank\" class=\"btn btn-outline-primary\" style=\"float: right;\">Click here for Eway Bill Generation</a>\n                    </div>\n                    <div class=\"col-sm-12\">\n                        <input style=\"margin-top: 2%;\" (ngModelChange)='checkLength($event)' type=\"number\" min=\"0\" id=\"ewayBill\" class=\"form-control\" name=\"eway\" [ngModel]='eway' required placeholder=\"Enter EWay Bill Number\" />\n                    </div>\n                </div>\n                <button class=\"btn btn-outline-primary \" shape=\"semi-round\" type=\"button\" style=\"float:right; margin-top: 2%;\" (click)='updateEway()' [disabled]='nil'>Submit</button>\n            </nb-card-body>\n        </nb-card>\n    </ng-template>\n</modal>\n\n<modal class=\"modals\" #componentInvoiceModal [closeOnOutsideClick]=\"false\">\n    <ng-template #modalHeader>\n        Invoice Preview\n    </ng-template>\n    <ng-template #modalBody>\n        <nb-card>\n            <nb-card-body id=\"contentToConvert\">\n                <div style=\"border:2px solid;\">\n                    <div class=\"row\">\n                        <div class=\"col-sm-2\" style=\"border-right: 2px solid; margin-left: 1%;\">\n                            <img style=\"width: 100% !important;margin-top: 4%;\" [hidden]=\"logoRemoved\" id=\"company_logo\" [src]=\"logoSrc\" alt=\"Logo\" />\n                        </div>\n                        <div class=\"col-sm-10\" style=\"margin-right: -1%;\">\n                            <h4 class=\"centre\"><b>GST INVOICE</b></h4><br>\n                            <h5 class=\"centre\">IRONBIRD ELEVATORS PRIVATE LIMITED</h5>\n                            <h5 class=\"centre\" style=\"font-size: small;\">#21, Balaji Layout, Near Bhadrappa Layout</h5>\n                            <h5 class=\"centre\" style=\"font-size: small;\">Sanjay Nagar Post</h5>\n                            <h5 class=\"centre\" style=\"font-size: small;\">Bengaluru 560094</h5>\n                            <div class=\"row\">\n                                <div class=\"col-sm-4\" style=\"margin-left: -1%;\">\n                                    <h5 style=\"float: left;font-size: x-small;\">Web: www.ironbirdelevators.com</h5>\n                                </div>\n                                <div class=\"col-sm-5\">\n                                    <h5 style=\"float: left;font-size: x-small;margin-left: 10%;\">Email: ironbirdelevators@gmail.com</h5>\n                                </div>\n                                <div class=\"col-sm-3\">\n                                    <h5 style=\"float: right; font-size: x-small;margin-right: 4%;\">Phone No: 8495066633</h5>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"row newRow\">\n                        <div class=\"col-sm-7\" style=\"border-right: 2px solid;border-top: 2px solid;\">\n                            <h5 style=\"margin-left: 2%;margin-top: 2%;\">CONISIGNEE ADDRESS</h5>\n                        </div>\n                        <div class=\"col-sm-5\" style=\"border-top: 2px solid;\">\n                            <h5 style=\"margin-top: 2%;margin-left: 2%;\">OUR GSTIN: 29AAECI9303F1ZK</h5>\n                        </div>\n                    </div>\n                    <div class=\"row newRow\">\n                        <div class=\"col-sm-7\" style=\"border-right: 2px solid;border-top: 2px solid;\">\n                            <h5 style=\"margin-left: 2%;margin-top: 2%;font-size: small;\">{{clientName}}</h5><br>\n                            <h5 style=\"margin-left: 2%;margin-top: -5%;font-size: small;\">{{address}}</h5><br>\n                            <!-- <h5 style=\"margin-left: 2%;margin-top: -5%;font-size: small;\">BANGALORE - 560040</h5><br> -->\n                            <h5 style=\"margin-left: 2%;font-size: small;\">GSTIN: {{clientGst}}</h5><br>\n                        </div>\n                        <div class=\"col-sm-5\" style=\"border-top: 2px solid;\">\n                            <div class=\"row\">\n                                <div class=\"col-sm-6\" style=\"border-right: 2px solid;border-bottom: 2px solid;\">\n                                    <h5 class=\"new\">Invoice No</h5>\n                                </div>\n                                <div class=\"col-sm-6\" style=\"border-bottom: 2px solid;\">\n                                    <!-- <h5 style=\"margin-top: 4%;font-size: small;\">{{invoiceNo}}</h5> -->\n                                    <h5 class=\"new\">{{invoiceName}}</h5>\n                                    <!-- <h5 class=\"new\"></h5> -->\n                                </div>\n                                <div class=\"col-sm-6\" style=\"border-right: 2px solid;border-bottom: 2px solid;\">\n                                    <h5 class=\"new\">Date & Time</h5>\n                                </div>\n                                <div class=\"col-sm-6\" style=\"border-bottom: 2px solid;\">\n                                    <h5 class=\"new\">{{displaydate}} {{displaytime}}</h5>\n                                </div>\n                                <div class=\"col-sm-6\" style=\"border-right: 2px solid;\">\n                                    <h5 class=\"new\">Location</h5>\n                                </div>\n                                <div class=\"col-sm-6\">\n                                    <h5 class=\"new\">Bhadrappa Layout, Bangalore</h5>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"row newRow\">\n                        <div class=\"col-sm-7\" style=\"border-right: 2px solid;border-top: 2px solid;\">\n                            <h5 style=\"margin-left: 2%;margin-top: 2%;font-size: small;\">Delivery Address: {{location}}</h5>\n                        </div>\n                        <div class=\"col-sm-5\" style=\"border-top: 2px solid;\">\n                            <div class=\"row\">\n                                <div class=\"col-sm-6\" style=\"border-right: 2px solid;border-bottom: 2px solid;\">\n                                    <h5 class=\"new\">E Way Bill No</h5>\n                                </div>\n                                <div class=\"col-sm-6\" style=\"border-bottom: 2px solid;\">\n                                    <h5 class=\"new\">{{eway}}</h5>\n                                </div>\n                                <div class=\"col-sm-6\" style=\"border-right: 2px solid;\">\n                                    <h5 class=\"new\">State / State Code</h5>\n                                </div>\n                                <div class=\"col-sm-6\">\n                                    <h5 class=\"new\">KARNATAKA / 29</h5>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"row newRow\">\n                        <div class=\"col-sm-1 tableHeader\">\n                            <h5 style=\"font-size: small;\">SL No</h5>\n                        </div>\n                        <div class=\"col-sm-4 tableHeader\">\n                            <h5 style=\"font-size: small;\">Description of Goods</h5>\n                        </div>\n                        <div class=\"col-sm-1 tableHeader\">\n                            <h5 style=\"font-size: small;margin-left: -8%;\">HSN/SAC</h5>\n                        </div>\n                        <div class=\"col-sm-1 tableHeader\">\n                            <h5 style=\"font-size: small;\">Unit</h5>\n                        </div>\n                        <div class=\"col-sm-1 tableHeader\">\n                            <h5 style=\"font-size: small;\">Qty</h5>\n                        </div>\n                        <div class=\"col-sm-2 tableHeader\">\n                            <h5 style=\"font-size: small;\">Price</h5>\n                        </div>\n                        <div class=\"col-sm-2 tableHeader\" style=\"border-right: none !important;\">\n                            <h5 style=\"font-size: small;\">Total</h5>\n                        </div>\n                    </div>\n                    <div *ngIf='!stage'>\n                        <div class=\"row newRow col-sm-12\" style=\"border-top: 2px solid;\">\n                            <h5 style=\"font-size: small;text-align: center;\">No Items Dispatched Yet</h5>\n                        </div>\n                    </div>\n                    <div *ngIf='stage'>\n                        <div class=\"row newRow\" *ngFor='let item of updateDispatchArray; let i = index;'>\n                            <!-- add list items -->\n                            <div class=\"col-sm-1 tableHeader\">\n                                <h5 style=\"font-size: small;\">{{i+1}}</h5>\n                            </div>\n                            <div class=\"col-sm-4 tableHeader\">\n                                <h5 style=\"font-size: small;\">{{item.partName}}</h5>\n                            </div>\n                            <div class=\"col-sm-1 tableHeader\">\n                                <h5 style=\"font-size: small;margin-left: -8%;\">{{item.HSN_Code}}</h5>\n                            </div>\n                            <div class=\"col-sm-1 tableHeader\">\n                                <h5 style=\"font-size: small;\">{{item.unit}}</h5>\n                            </div>\n                            <div class=\"col-sm-1 tableHeader\">\n                                <h5 style=\"font-size: small;\">{{item.dispatched}}</h5>\n                            </div>\n                            <div class=\"col-sm-2 tableHeader\">\n                                <h5 style=\"font-size: small;text-align: right;\">{{item.unitPrice}}</h5>\n                            </div>\n                            <div class=\"col-sm-2 tableHeader\" style=\"border-right: none !important;\">\n                                <h5 style=\"font-size: small;text-align: right;\">{{item.totalINR}}</h5>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"row newRow\">\n                        <div class=\"col-sm-8\" style=\"border-right: 2px solid;border-top: 2px solid;\">\n                            <h5 style=\"font-size: small;\">Invoice Value (In Words)</h5>\n                        </div>\n                        <div class=\"col-sm-4\" style=\"border-top: 2px solid;\">\n                            <div class=\"row\">\n                                <div class=\"col-sm-6\" style=\"border-right: 2px solid\">\n                                    <h5 class=\"new\">Total</h5>\n                                </div>\n                                <div class=\"col-sm-6\">\n                                    <h5 class=\"new\" style=\"float: right;\">{{invoiceTotal}}</h5>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"row newRow\">\n                        <div class=\"col-sm-8\" style=\"border-right: 2px solid;border-top: 2px solid;\">\n                            <h5 style=\"margin-left: 2%;margin-top: 2%;font-size: small;\">{{totalInWords}}</h5>\n                        </div>\n                        <div class=\"col-sm-4\" style=\"border-top: 2px solid;\">\n                            <div class=\"row\">\n                                <div class=\"col-sm-6 tax\">\n                                    <h5 class=\"new\">C GST @ 9%</h5>\n                                </div>\n                                <div class=\"col-sm-6\" style=\"border-bottom: 2px solid;\">\n                                    <h5 class=\"new\" style=\"float: right;\">{{taxAmt}}</h5>\n                                </div>\n                                <div class=\"col-sm-6 tax\">\n                                    <h5 class=\"new\">S GST @ 9%</h5>\n                                </div>\n                                <div class=\"col-sm-6\" style=\"border-bottom: 2px solid;\">\n                                    <h5 class=\"new\" style=\"float: right;\">{{taxAmt}}</h5>\n                                </div>\n                                <div class=\"col-sm-6 tax\">\n                                    <h5 class=\"new\">I GST @ 18%</h5>\n                                </div>\n                                <div class=\"col-sm-6\" style=\"border-bottom: 2px solid;\">\n                                    <h5 class=\"new\" style=\"float: right;\">₹0.00</h5>\n                                </div>\n                                <div class=\"col-sm-6\" style=\"border-right: 2px solid;\">\n                                    <h5 class=\"new\">Grand Total</h5>\n                                </div>\n                                <div class=\"col-sm-6\">\n                                    <h5 class=\"new\" style=\"float: right;\">{{grandTotal}}</h5>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"row newRow\" style=\"border-top: 2px solid;\">\n                        <div class=\"col-sm-12\">\n                            <h5 style=\"float: right;font-size: small;margin-top: 2%;\">M/s. IRONBIRD ELEVATORS PRIVATE LIMITED</h5>\n                        </div><br><br><br><br><br><br>\n                        <div class=\"col-sm-6\">\n                            <h5 class=\"new\" style=\"margin-left: 10%;\">Receiver's Signature</h5>\n                        </div>\n                        <div class=\"col-sm-3\"></div>\n                        <div class=\"col-sm-3\">\n                            <h5 style=\"margin-top: 14%;font-size: small;\">Authorized Signatory</h5>\n                        </div>\n                    </div>\n                </div>\n            </nb-card-body>\n            <nb-card-footer>\n                <button type=\"button\" [disabled]='!length' (click)=\"createPreview()\" class=\"btn btn-outline-primary \" shape=\"semi-round\" style=\"float:left\">Download</button>\n                <button type=\"button\" (click)=\"cancel()\" class=\"btn btn-outline-primary \" shape=\"semi-round\" style=\"float:right\">Close</button>\n            </nb-card-footer>\n        </nb-card>\n        <br>\n\n    </ng-template>\n</modal>"

/***/ }),

/***/ "./src/app/pages/ui-features/customer/customers/detail-customers/customer-invoices/customer-invoices.component.scss":
/*!**************************************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/customer/customers/detail-customers/customer-invoices/customer-invoices.component.scss ***!
  \**************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".boderscss {\n  background: #fff;\n  margin: 4px;\n  border: 3px solid #dadfe6; }\n\n@media (min-width: 576px) {\n  /deep/ modal.modals .modal-dialog {\n    max-width: 1000px !important;\n    margin: 1.75rem auto; }\n  /deep/ modal.modals.moda .modal-dialog .modal-body {\n    height: 1090px; } }\n\n@media (min-width: 576px) {\n  /deep/ modal.invoice .modal-dialog {\n    max-width: 1000px !important;\n    margin: 1.75rem auto; } }\n\n.bottoms {\n  margin-bottom: 5px; }\n\n/deep/ nb-checkbox label {\n  margin-top: 27px !important; }\n\n/deep/ modal.modalfirst .modal-dialog {\n  max-width: 700px !important;\n  margin: 234px 600px auto auto !important; }\n\n.container {\n  padding-left: 0px; }\n\nnb-card {\n  border: 0px solid #d5dbe0; }\n\n[type=\"checkbox\"]:not(:checked),\n[type=\"checkbox\"]:checked {\n  position: absolute;\n  left: -9999px; }\n\n[type=\"checkbox\"]:not(:checked) + label,\n[type=\"checkbox\"]:checked + label {\n  position: relative;\n  padding-left: 1.95em; }\n\n/* checkbox aspect */\n\n[type=\"checkbox\"]:not(:checked) + label:before,\n[type=\"checkbox\"]:checked + label:before {\n  content: '';\n  position: absolute;\n  left: 0;\n  top: 0;\n  width: 1.25em;\n  height: 1.25em;\n  border: 2px solid #ccc;\n  background: #fff;\n  border-radius: 4px;\n  -webkit-box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.1);\n          box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.1); }\n\n/* checked mark aspect */\n\n[type=\"checkbox\"]:not(:checked) + label:after,\n[type=\"checkbox\"]:checked + label:after {\n  content: '\\2713\\0020';\n  position: absolute;\n  top: .15em;\n  left: .22em;\n  font-size: 1.3em;\n  line-height: 0.8;\n  color: #09ad7e;\n  -webkit-transition: all .2s;\n  transition: all .2s;\n  font-family: 'Lucida Sans Unicode', 'Arial Unicode MS', Arial; }\n\n/* checked mark aspect changes */\n\n[type=\"checkbox\"]:not(:checked) + label:after {\n  opacity: 0;\n  -webkit-transform: scale(0);\n          transform: scale(0); }\n\n[type=\"checkbox\"]:checked + label:after {\n  opacity: 1;\n  -webkit-transform: scale(1);\n          transform: scale(1); }\n\n/* disabled checkbox */\n\n[type=\"checkbox\"]:disabled:not(:checked) + label:before,\n[type=\"checkbox\"]:disabled:checked + label:before {\n  -webkit-box-shadow: none;\n          box-shadow: none;\n  border-color: #bbb;\n  background-color: #ddd; }\n\n[type=\"checkbox\"]:disabled:checked + label:after {\n  color: #999; }\n\n[type=\"checkbox\"]:disabled + label {\n  color: #aaa; }\n\n/* accessibility */\n\n/* hover style just for information */\n\nlabel:hover:before {\n  border: 2px solid #4778d9 !important; }\n\n.A4 {\n  background: white;\n  width: 21cm;\n  display: block;\n  margin: 0 auto;\n  padding: 10px 25px;\n  margin-bottom: 0.5cm;\n  -webkit-box-shadow: 0 0 0.5cm rgba(0, 0, 0, 0.5);\n          box-shadow: 0 0 0.5cm rgba(0, 0, 0, 0.5);\n  overflow-y: scroll;\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  font-size: 12pt; }\n\n.close1 {\n  text-shadow: none;\n  color: #181818;\n  padding: 1rem 1rem;\n  margin: -1rem -1rem -1rem auto;\n  cursor: pointer;\n  background-color: transparent;\n  border: 0;\n  -webkit-appearance: none;\n  float: right;\n  font-size: 1.5rem;\n  font-weight: 700;\n  line-height: 1;\n  color: #000;\n  text-shadow: 0 1px 0 #fff;\n  opacity: .5; }\n\n/deep/ modal.invoice .modal .modal-dialog .modal-content .modal-header .close {\n  display: none !important; }\n\n/deep/ modal.modals .modal .modal-dialog .modal-content .modal-header .close {\n  display: none !important; }\n\n.nb-edit {\n  color: #000 !important; }\n\n.nb-checkmark {\n  color: #000 !important; }\n\n.nb-close {\n  color: #000 !important; }\n\n.odd_row {\n  background: #f9f9f9; }\n\n.right_placeholder {\n  text-align: right; }\n\n.centre {\n  text-align: center; }\n\n.new {\n  margin-top: 6%;\n  font-size: small;\n  margin-bottom: 6%; }\n\n.newRow {\n  margin-left: -0.1%;\n  margin-right: -0.1%; }\n\n.tableHeader {\n  border-top: 2px solid;\n  border-right: 2px solid;\n  text-align: center; }\n\n.tax {\n  border-right: 2px solid;\n  border-bottom: 2px solid; }\n\n.heading {\n  background-color: #357EBD;\n  color: #FFF;\n  margin-bottom: 3em;\n  text-align: center;\n  line-height: 1.2em; }\n\na {\n  cursor: pointer; }\n\na:hover {\n  color: lightgreen; }\n\ntextarea {\n  border-left: none !important;\n  border-right: none !important;\n  border-top: none !important; }\n\ninput[type=text],\ninput[type=number],\ninput[type=date],\nselect,\ntextarea {\n  font-family: 'Source Sans Pro', Sans-serif;\n  font-size: 1em;\n  border: none;\n  border-bottom: 1px solid #d7d7d7;\n  padding: 4px;\n  outline: none; }\n\n.items-table {\n  width: 100%; }\n\n.items-table th {\n  text-align: left;\n  font-weight: 300;\n  padding: 10px 5px 2px;\n  white-space: nowrap; }\n\n.items-table input[type=text],\n.items-table input[type=number],\n.items-table input[type=date],\n.items-table select {\n  width: 100%; }\n\n#items td {\n  padding: 5px;\n  background: #eee;\n  border-bottom: 1px solid #fff; }\n\n#items .desc {\n  min-width: 200px; }\n\n#items .total-price {\n  text-align: right;\n  font-weight: 600; }\n\n.invoice-totals {\n  width: 50%;\n  float: right; }\n\n.invoice-totals th {\n  font-weight: 300; }\n\n.invoice-totals th,\n.invoice-totals td {\n  text-align: right;\n  padding: 5px;\n  border-bottom: 1px solid #d7d7d7; }\n\n.invoice-totals .tax-container td {\n  border-bottom: none;\n  padding: 0; }\n\n#taxs {\n  width: 100%; }\n\n#taxs thead th {\n  font-weight: 300;\n  background-color: #eee; }\n\n#taxs th,\n#taxs td {\n  text-align: right;\n  padding: 5px;\n  border-bottom: 1px solid #d7d7d7; }\n\nimg {\n  width: 303px !important; }\n\n@media (max-width: 1280px) {\n  img {\n    width: 167px !important; } }\n\nnb-card-body {\n  background-color: hidden !important; }\n\n@media (min-width: 576px) {\n  /deep/ modal.invoice .modal-dialog {\n    max-width: 1000px !important;\n    margin: 1.75rem auto; } }\n\n:host/deep/ ng2-st-tbody-custom a.ng2-smart-action.ng2-smart-action-custom-custom {\n  width: 50px;\n  text-align: center;\n  font-size: 0.1em; }\n\n:host/deep/ ng2-st-tbody-custom a.ng2-smart-action.ng2-smart-action-custom-custom img {\n  width: 57% !important;\n  margin-left: 34px; }\n\n:host/deep/ ng2-st-tbody-custom a.ng2-smart-action.ng2-smart-action-custom-custom:hover {\n  color: #000000; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3ByYWthc2gvVmlwaW4vaXJvbmJpcmQvMjBGRUIyMDIwL2lyb25iaXJkLWNsaWVudHMvc3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9jdXN0b21lci9jdXN0b21lcnMvZGV0YWlsLWN1c3RvbWVycy9jdXN0b21lci1pbnZvaWNlcy9jdXN0b21lci1pbnZvaWNlcy5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvcGFnZXMvdWktZmVhdHVyZXMvY3VzdG9tZXIvY3VzdG9tZXJzL2RldGFpbC1jdXN0b21lcnMvY3VzdG9tZXItaW52b2ljZXMvY3VzdG9tZXItaW52b2ljZXMuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxnQkFBZ0I7RUFDaEIsV0FBVztFQUNYLHlCQUF5QixFQUFBOztBQUc3QjtFQUNJO0lBQ0ksNEJBQTRCO0lBQzVCLG9CQUFvQixFQUFBO0VBRXhCO0lBQ0ksY0FBYyxFQUFBLEVBQ2pCOztBQUdMO0VBQ0k7SUFDSSw0QkFBNEI7SUFDNUIsb0JBQW9CLEVBQUEsRUFDdkI7O0FBR0w7RUFDSSxrQkFBa0IsRUFBQTs7QUFHdEI7RUFDSSwyQkFBMkIsRUFBQTs7QUFHL0I7RUFDSSwyQkFBMkI7RUFDM0Isd0NBQXdDLEVBQUE7O0FBRzVDO0VBQ0ksaUJBQWlCLEVBQUE7O0FBR3JCO0VBQ0kseUJBQXlCLEVBQUE7O0FDUjdCOztFRGFJLGtCQUFrQjtFQUNsQixhQUFhLEVBQUE7O0FDVGpCOztFRGNJLGtCQUFrQjtFQUNsQixvQkFBb0IsRUFBQTs7QUFLeEIsb0JBQUE7O0FDZEE7O0VEa0JJLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsT0FBTztFQUNQLE1BQU07RUFDTixhQUFhO0VBQ2IsY0FBYztFQUNkLHNCQUFzQjtFQUN0QixnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLHNEQUE2QztVQUE3Qyw4Q0FBNkMsRUFBQTs7QUFJakQsd0JBQUE7O0FDakJBOztFRHFCSSxxQkFBcUI7RUFDckIsa0JBQWtCO0VBQ2xCLFVBQVU7RUFDVixXQUFXO0VBQ1gsZ0JBQWdCO0VBQ2hCLGdCQUFnQjtFQUNoQixjQUFjO0VBQ2QsMkJBQW1CO0VBQW5CLG1CQUFtQjtFQUNuQiw2REFBNkQsRUFBQTs7QUFJakUsZ0NBQUE7O0FDcEJBO0VEdUJJLFVBQVU7RUFDViwyQkFBbUI7VUFBbkIsbUJBQW1CLEVBQUE7O0FDcEJ2QjtFRHdCSSxVQUFVO0VBQ1YsMkJBQW1CO1VBQW5CLG1CQUFtQixFQUFBOztBQUl2QixzQkFBQTs7QUN4QkE7O0VENEJJLHdCQUFnQjtVQUFoQixnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLHNCQUFzQixFQUFBOztBQ3hCMUI7RUQ0QkksV0FBVyxFQUFBOztBQ3pCZjtFRDZCSSxXQUFXLEVBQUE7O0FBSWYsa0JBQUE7O0FBR0EscUNBQUE7O0FBRUE7RUFDSSxvQ0FBbUMsRUFBQTs7QUFHdkM7RUFDSSxpQkFBaUI7RUFDakIsV0FBVztFQUVYLGNBQWM7RUFDZCxjQUFjO0VBQ2Qsa0JBQWtCO0VBQ2xCLG9CQUFvQjtFQUNwQixnREFBd0M7VUFBeEMsd0NBQXdDO0VBQ3hDLGtCQUFrQjtFQUNsQiw4QkFBc0I7VUFBdEIsc0JBQXNCO0VBQ3RCLGVBQWUsRUFBQTs7QUFHbkI7RUFDSSxpQkFBaUI7RUFDakIsY0FBYztFQUNkLGtCQUFrQjtFQUNsQiw4QkFBOEI7RUFDOUIsZUFBZTtFQUNmLDZCQUE2QjtFQUM3QixTQUFTO0VBQ1Qsd0JBQXdCO0VBQ3hCLFlBQVk7RUFDWixpQkFBaUI7RUFDakIsZ0JBQWdCO0VBQ2hCLGNBQWM7RUFDZCxXQUFXO0VBQ1gseUJBQXlCO0VBQ3pCLFdBQVcsRUFBQTs7QUFHZjtFQUNJLHdCQUF3QixFQUFBOztBQUc1QjtFQUNJLHdCQUF3QixFQUFBOztBQUc1QjtFQUNJLHNCQUFzQixFQUFBOztBQUcxQjtFQUNJLHNCQUFzQixFQUFBOztBQUcxQjtFQUNJLHNCQUFzQixFQUFBOztBQUcxQjtFQUNJLG1CQUFtQixFQUFBOztBQUd2QjtFQUNJLGlCQUFpQixFQUFBOztBQUdyQjtFQUNJLGtCQUFrQixFQUFBOztBQUd0QjtFQUNJLGNBQWM7RUFDZCxnQkFBZ0I7RUFDaEIsaUJBQWlCLEVBQUE7O0FBR3JCO0VBQ0ksa0JBQWtCO0VBQ2xCLG1CQUFtQixFQUFBOztBQUd2QjtFQUNJLHFCQUFxQjtFQUNyQix1QkFBdUI7RUFDdkIsa0JBQWtCLEVBQUE7O0FBR3RCO0VBQ0ksdUJBQXVCO0VBQ3ZCLHdCQUF3QixFQUFBOztBQUc1QjtFQUNJLHlCQUF5QjtFQUN6QixXQUFXO0VBQ1gsa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixrQkFBa0IsRUFBQTs7QUFHdEI7RUFDSSxlQUFlLEVBQUE7O0FBR25CO0VBQ0ksaUJBQWlCLEVBQUE7O0FBR3JCO0VBQ0ksNEJBQTRCO0VBQzVCLDZCQUE2QjtFQUM3QiwyQkFBMkIsRUFBQTs7QUFHL0I7Ozs7O0VBS0ksMENBQTBDO0VBQzFDLGNBQWM7RUFDZCxZQUFZO0VBQ1osZ0NBQWdDO0VBQ2hDLFlBQVk7RUFDWixhQUFhLEVBQUE7O0FBR2pCO0VBQ0ksV0FBVyxFQUFBOztBQUdmO0VBQ0ksZ0JBQWdCO0VBQ2hCLGdCQUFnQjtFQUNoQixxQkFBcUI7RUFDckIsbUJBQW1CLEVBQUE7O0FBR3ZCOzs7O0VBSUksV0FBVyxFQUFBOztBQUdmO0VBQ0ksWUFBWTtFQUNaLGdCQUFnQjtFQUNoQiw2QkFBNkIsRUFBQTs7QUFHakM7RUFDSSxnQkFBZ0IsRUFBQTs7QUFHcEI7RUFDSSxpQkFBaUI7RUFDakIsZ0JBQWdCLEVBQUE7O0FBR3BCO0VBQ0ksVUFBVTtFQUNWLFlBQVksRUFBQTs7QUFHaEI7RUFDSSxnQkFBZ0IsRUFBQTs7QUFHcEI7O0VBRUksaUJBQWlCO0VBQ2pCLFlBQVk7RUFDWixnQ0FBZ0MsRUFBQTs7QUFHcEM7RUFDSSxtQkFBbUI7RUFDbkIsVUFBVSxFQUFBOztBQUdkO0VBQ0ksV0FBVyxFQUFBOztBQUdmO0VBQ0ksZ0JBQWdCO0VBQ2hCLHNCQUFzQixFQUFBOztBQUcxQjs7RUFFSSxpQkFBaUI7RUFDakIsWUFBWTtFQUNaLGdDQUFnQyxFQUFBOztBQUdwQztFQUNJLHVCQUF1QixFQUFBOztBQUczQjtFQUNJO0lBQ0ksdUJBQXVCLEVBQUEsRUFDMUI7O0FBR0w7RUFDSSxtQ0FBbUMsRUFBQTs7QUFHdkM7RUFDSTtJQUNJLDRCQUE0QjtJQUM1QixvQkFBb0IsRUFBQSxFQUN2Qjs7QUFHTDtFQUNJLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsZ0JBQWdCLEVBQUE7O0FBR3BCO0VBQ0kscUJBQXFCO0VBQ3JCLGlCQUFpQixFQUFBOztBQUdyQjtFQUNJLGNBQWMsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VpLWZlYXR1cmVzL2N1c3RvbWVyL2N1c3RvbWVycy9kZXRhaWwtY3VzdG9tZXJzL2N1c3RvbWVyLWludm9pY2VzL2N1c3RvbWVyLWludm9pY2VzLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJvZGVyc2NzcyB7XG4gICAgYmFja2dyb3VuZDogI2ZmZjtcbiAgICBtYXJnaW46IDRweDtcbiAgICBib3JkZXI6IDNweCBzb2xpZCAjZGFkZmU2O1xufVxuXG5AbWVkaWEgKG1pbi13aWR0aDogNTc2cHgpIHtcbiAgICAvZGVlcC8gbW9kYWwubW9kYWxzIC5tb2RhbC1kaWFsb2cge1xuICAgICAgICBtYXgtd2lkdGg6IDEwMDBweCAhaW1wb3J0YW50O1xuICAgICAgICBtYXJnaW46IDEuNzVyZW0gYXV0bztcbiAgICB9XG4gICAgL2RlZXAvIG1vZGFsLm1vZGFscy5tb2RhIC5tb2RhbC1kaWFsb2cgLm1vZGFsLWJvZHkge1xuICAgICAgICBoZWlnaHQ6IDEwOTBweDtcbiAgICB9XG59XG5cbkBtZWRpYSAobWluLXdpZHRoOiA1NzZweCkge1xuICAgIC9kZWVwLyBtb2RhbC5pbnZvaWNlIC5tb2RhbC1kaWFsb2cge1xuICAgICAgICBtYXgtd2lkdGg6IDEwMDBweCAhaW1wb3J0YW50O1xuICAgICAgICBtYXJnaW46IDEuNzVyZW0gYXV0bztcbiAgICB9XG59XG5cbi5ib3R0b21zIHtcbiAgICBtYXJnaW4tYm90dG9tOiA1cHg7XG59XG5cbi9kZWVwLyBuYi1jaGVja2JveCBsYWJlbCB7XG4gICAgbWFyZ2luLXRvcDogMjdweCAhaW1wb3J0YW50O1xufVxuXG4vZGVlcC8gbW9kYWwubW9kYWxmaXJzdCAubW9kYWwtZGlhbG9nIHtcbiAgICBtYXgtd2lkdGg6IDcwMHB4ICFpbXBvcnRhbnQ7XG4gICAgbWFyZ2luOiAyMzRweCA2MDBweCBhdXRvIGF1dG8gIWltcG9ydGFudDtcbn1cblxuLmNvbnRhaW5lciB7XG4gICAgcGFkZGluZy1sZWZ0OiAwcHg7XG59XG5cbm5iLWNhcmQge1xuICAgIGJvcmRlcjogMHB4IHNvbGlkICNkNWRiZTA7XG59XG5cblt0eXBlPVwiY2hlY2tib3hcIl06bm90KDpjaGVja2VkKSxcblt0eXBlPVwiY2hlY2tib3hcIl06Y2hlY2tlZCB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGxlZnQ6IC05OTk5cHg7XG59XG5cblt0eXBlPVwiY2hlY2tib3hcIl06bm90KDpjaGVja2VkKStsYWJlbCxcblt0eXBlPVwiY2hlY2tib3hcIl06Y2hlY2tlZCtsYWJlbCB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHBhZGRpbmctbGVmdDogMS45NWVtO1xuICAgIC8vIGN1cnNvcjogcG9pbnRlcjtcbn1cblxuXG4vKiBjaGVja2JveCBhc3BlY3QgKi9cblxuW3R5cGU9XCJjaGVja2JveFwiXTpub3QoOmNoZWNrZWQpK2xhYmVsOmJlZm9yZSxcblt0eXBlPVwiY2hlY2tib3hcIl06Y2hlY2tlZCtsYWJlbDpiZWZvcmUge1xuICAgIGNvbnRlbnQ6ICcnO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBsZWZ0OiAwO1xuICAgIHRvcDogMDtcbiAgICB3aWR0aDogMS4yNWVtO1xuICAgIGhlaWdodDogMS4yNWVtO1xuICAgIGJvcmRlcjogMnB4IHNvbGlkICNjY2M7XG4gICAgYmFja2dyb3VuZDogI2ZmZjtcbiAgICBib3JkZXItcmFkaXVzOiA0cHg7XG4gICAgYm94LXNoYWRvdzogaW5zZXQgMCAxcHggM3B4IHJnYmEoMCwgMCwgMCwgLjEpO1xufVxuXG5cbi8qIGNoZWNrZWQgbWFyayBhc3BlY3QgKi9cblxuW3R5cGU9XCJjaGVja2JveFwiXTpub3QoOmNoZWNrZWQpK2xhYmVsOmFmdGVyLFxuW3R5cGU9XCJjaGVja2JveFwiXTpjaGVja2VkK2xhYmVsOmFmdGVyIHtcbiAgICBjb250ZW50OiAnXFwyNzEzXFwwMDIwJztcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAuMTVlbTtcbiAgICBsZWZ0OiAuMjJlbTtcbiAgICBmb250LXNpemU6IDEuM2VtO1xuICAgIGxpbmUtaGVpZ2h0OiAwLjg7XG4gICAgY29sb3I6ICMwOWFkN2U7XG4gICAgdHJhbnNpdGlvbjogYWxsIC4ycztcbiAgICBmb250LWZhbWlseTogJ0x1Y2lkYSBTYW5zIFVuaWNvZGUnLCAnQXJpYWwgVW5pY29kZSBNUycsIEFyaWFsO1xufVxuXG5cbi8qIGNoZWNrZWQgbWFyayBhc3BlY3QgY2hhbmdlcyAqL1xuXG5bdHlwZT1cImNoZWNrYm94XCJdOm5vdCg6Y2hlY2tlZCkrbGFiZWw6YWZ0ZXIge1xuICAgIG9wYWNpdHk6IDA7XG4gICAgdHJhbnNmb3JtOiBzY2FsZSgwKTtcbn1cblxuW3R5cGU9XCJjaGVja2JveFwiXTpjaGVja2VkK2xhYmVsOmFmdGVyIHtcbiAgICBvcGFjaXR5OiAxO1xuICAgIHRyYW5zZm9ybTogc2NhbGUoMSk7XG59XG5cblxuLyogZGlzYWJsZWQgY2hlY2tib3ggKi9cblxuW3R5cGU9XCJjaGVja2JveFwiXTpkaXNhYmxlZDpub3QoOmNoZWNrZWQpK2xhYmVsOmJlZm9yZSxcblt0eXBlPVwiY2hlY2tib3hcIl06ZGlzYWJsZWQ6Y2hlY2tlZCtsYWJlbDpiZWZvcmUge1xuICAgIGJveC1zaGFkb3c6IG5vbmU7XG4gICAgYm9yZGVyLWNvbG9yOiAjYmJiO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNkZGQ7XG59XG5cblt0eXBlPVwiY2hlY2tib3hcIl06ZGlzYWJsZWQ6Y2hlY2tlZCtsYWJlbDphZnRlciB7XG4gICAgY29sb3I6ICM5OTk7XG59XG5cblt0eXBlPVwiY2hlY2tib3hcIl06ZGlzYWJsZWQrbGFiZWwge1xuICAgIGNvbG9yOiAjYWFhO1xufVxuXG5cbi8qIGFjY2Vzc2liaWxpdHkgKi9cblxuXG4vKiBob3ZlciBzdHlsZSBqdXN0IGZvciBpbmZvcm1hdGlvbiAqL1xuXG5sYWJlbDpob3ZlcjpiZWZvcmUge1xuICAgIGJvcmRlcjogMnB4IHNvbGlkICM0Nzc4ZDkhaW1wb3J0YW50O1xufVxuXG4uQTQge1xuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgIHdpZHRoOiAyMWNtO1xuICAgIC8vIGhlaWdodDogMzAuN2NtO1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIG1hcmdpbjogMCBhdXRvO1xuICAgIHBhZGRpbmc6IDEwcHggMjVweDtcbiAgICBtYXJnaW4tYm90dG9tOiAwLjVjbTtcbiAgICBib3gtc2hhZG93OiAwIDAgMC41Y20gcmdiYSgwLCAwLCAwLCAwLjUpO1xuICAgIG92ZXJmbG93LXk6IHNjcm9sbDtcbiAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAgIGZvbnQtc2l6ZTogMTJwdDtcbn1cblxuLmNsb3NlMSB7XG4gICAgdGV4dC1zaGFkb3c6IG5vbmU7XG4gICAgY29sb3I6ICMxODE4MTg7XG4gICAgcGFkZGluZzogMXJlbSAxcmVtO1xuICAgIG1hcmdpbjogLTFyZW0gLTFyZW0gLTFyZW0gYXV0bztcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gICAgYm9yZGVyOiAwO1xuICAgIC13ZWJraXQtYXBwZWFyYW5jZTogbm9uZTtcbiAgICBmbG9hdDogcmlnaHQ7XG4gICAgZm9udC1zaXplOiAxLjVyZW07XG4gICAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgICBsaW5lLWhlaWdodDogMTtcbiAgICBjb2xvcjogIzAwMDtcbiAgICB0ZXh0LXNoYWRvdzogMCAxcHggMCAjZmZmO1xuICAgIG9wYWNpdHk6IC41O1xufVxuXG4vZGVlcC8gbW9kYWwuaW52b2ljZSAubW9kYWwgLm1vZGFsLWRpYWxvZyAubW9kYWwtY29udGVudCAubW9kYWwtaGVhZGVyIC5jbG9zZSB7XG4gICAgZGlzcGxheTogbm9uZSAhaW1wb3J0YW50O1xufVxuXG4vZGVlcC8gbW9kYWwubW9kYWxzIC5tb2RhbCAubW9kYWwtZGlhbG9nIC5tb2RhbC1jb250ZW50IC5tb2RhbC1oZWFkZXIgLmNsb3NlIHtcbiAgICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XG59XG5cbi5uYi1lZGl0IHtcbiAgICBjb2xvcjogIzAwMCAhaW1wb3J0YW50O1xufVxuXG4ubmItY2hlY2ttYXJrIHtcbiAgICBjb2xvcjogIzAwMCAhaW1wb3J0YW50O1xufVxuXG4ubmItY2xvc2Uge1xuICAgIGNvbG9yOiAjMDAwICFpbXBvcnRhbnQ7XG59XG5cbi5vZGRfcm93IHtcbiAgICBiYWNrZ3JvdW5kOiAjZjlmOWY5O1xufVxuXG4ucmlnaHRfcGxhY2Vob2xkZXIge1xuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xufVxuXG4uY2VudHJlIHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5uZXcge1xuICAgIG1hcmdpbi10b3A6IDYlO1xuICAgIGZvbnQtc2l6ZTogc21hbGw7XG4gICAgbWFyZ2luLWJvdHRvbTogNiU7XG59XG5cbi5uZXdSb3cge1xuICAgIG1hcmdpbi1sZWZ0OiAtMC4xJTtcbiAgICBtYXJnaW4tcmlnaHQ6IC0wLjElO1xufVxuXG4udGFibGVIZWFkZXIge1xuICAgIGJvcmRlci10b3A6IDJweCBzb2xpZDtcbiAgICBib3JkZXItcmlnaHQ6IDJweCBzb2xpZDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi50YXgge1xuICAgIGJvcmRlci1yaWdodDogMnB4IHNvbGlkO1xuICAgIGJvcmRlci1ib3R0b206IDJweCBzb2xpZDtcbn1cblxuLmhlYWRpbmcge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMzNTdFQkQ7XG4gICAgY29sb3I6ICNGRkY7XG4gICAgbWFyZ2luLWJvdHRvbTogM2VtO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBsaW5lLWhlaWdodDogMS4yZW07XG59XG5cbmEge1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbn1cblxuYTpob3ZlciB7XG4gICAgY29sb3I6IGxpZ2h0Z3JlZW47XG59XG5cbnRleHRhcmVhIHtcbiAgICBib3JkZXItbGVmdDogbm9uZSAhaW1wb3J0YW50O1xuICAgIGJvcmRlci1yaWdodDogbm9uZSAhaW1wb3J0YW50O1xuICAgIGJvcmRlci10b3A6IG5vbmUgIWltcG9ydGFudDtcbn1cblxuaW5wdXRbdHlwZT10ZXh0XSxcbmlucHV0W3R5cGU9bnVtYmVyXSxcbmlucHV0W3R5cGU9ZGF0ZV0sXG5zZWxlY3QsXG50ZXh0YXJlYSB7XG4gICAgZm9udC1mYW1pbHk6ICdTb3VyY2UgU2FucyBQcm8nLCBTYW5zLXNlcmlmO1xuICAgIGZvbnQtc2l6ZTogMWVtO1xuICAgIGJvcmRlcjogbm9uZTtcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2Q3ZDdkNztcbiAgICBwYWRkaW5nOiA0cHg7XG4gICAgb3V0bGluZTogbm9uZTtcbn1cblxuLml0ZW1zLXRhYmxlIHtcbiAgICB3aWR0aDogMTAwJTtcbn1cblxuLml0ZW1zLXRhYmxlIHRoIHtcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgIGZvbnQtd2VpZ2h0OiAzMDA7XG4gICAgcGFkZGluZzogMTBweCA1cHggMnB4O1xuICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XG59XG5cbi5pdGVtcy10YWJsZSBpbnB1dFt0eXBlPXRleHRdLFxuLml0ZW1zLXRhYmxlIGlucHV0W3R5cGU9bnVtYmVyXSxcbi5pdGVtcy10YWJsZSBpbnB1dFt0eXBlPWRhdGVdLFxuLml0ZW1zLXRhYmxlIHNlbGVjdCB7XG4gICAgd2lkdGg6IDEwMCU7XG59XG5cbiNpdGVtcyB0ZCB7XG4gICAgcGFkZGluZzogNXB4O1xuICAgIGJhY2tncm91bmQ6ICNlZWU7XG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNmZmY7XG59XG5cbiNpdGVtcyAuZGVzYyB7XG4gICAgbWluLXdpZHRoOiAyMDBweDtcbn1cblxuI2l0ZW1zIC50b3RhbC1wcmljZSB7XG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cblxuLmludm9pY2UtdG90YWxzIHtcbiAgICB3aWR0aDogNTAlO1xuICAgIGZsb2F0OiByaWdodDtcbn1cblxuLmludm9pY2UtdG90YWxzIHRoIHtcbiAgICBmb250LXdlaWdodDogMzAwO1xufVxuXG4uaW52b2ljZS10b3RhbHMgdGgsXG4uaW52b2ljZS10b3RhbHMgdGQge1xuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICAgIHBhZGRpbmc6IDVweDtcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2Q3ZDdkNztcbn1cblxuLmludm9pY2UtdG90YWxzIC50YXgtY29udGFpbmVyIHRkIHtcbiAgICBib3JkZXItYm90dG9tOiBub25lO1xuICAgIHBhZGRpbmc6IDA7XG59XG5cbiN0YXhzIHtcbiAgICB3aWR0aDogMTAwJTtcbn1cblxuI3RheHMgdGhlYWQgdGgge1xuICAgIGZvbnQtd2VpZ2h0OiAzMDA7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2VlZTtcbn1cblxuI3RheHMgdGgsXG4jdGF4cyB0ZCB7XG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gICAgcGFkZGluZzogNXB4O1xuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZDdkN2Q3O1xufVxuXG5pbWcge1xuICAgIHdpZHRoOiAzMDNweCAhaW1wb3J0YW50O1xufVxuXG5AbWVkaWEobWF4LXdpZHRoOjEyODBweCkge1xuICAgIGltZyB7XG4gICAgICAgIHdpZHRoOiAxNjdweCAhaW1wb3J0YW50O1xuICAgIH1cbn1cblxubmItY2FyZC1ib2R5IHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBoaWRkZW4gIWltcG9ydGFudDtcbn1cblxuQG1lZGlhIChtaW4td2lkdGg6IDU3NnB4KSB7XG4gICAgL2RlZXAvIG1vZGFsLmludm9pY2UgLm1vZGFsLWRpYWxvZyB7XG4gICAgICAgIG1heC13aWR0aDogMTAwMHB4ICFpbXBvcnRhbnQ7XG4gICAgICAgIG1hcmdpbjogMS43NXJlbSBhdXRvO1xuICAgIH1cbn1cblxuOmhvc3QvZGVlcC8gbmcyLXN0LXRib2R5LWN1c3RvbSBhLm5nMi1zbWFydC1hY3Rpb24ubmcyLXNtYXJ0LWFjdGlvbi1jdXN0b20tY3VzdG9tIHtcbiAgICB3aWR0aDogNTBweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgZm9udC1zaXplOiAwLjFlbTtcbn1cblxuOmhvc3QvZGVlcC8gbmcyLXN0LXRib2R5LWN1c3RvbSBhLm5nMi1zbWFydC1hY3Rpb24ubmcyLXNtYXJ0LWFjdGlvbi1jdXN0b20tY3VzdG9tIGltZyB7XG4gICAgd2lkdGg6IDU3JSAhaW1wb3J0YW50O1xuICAgIG1hcmdpbi1sZWZ0OiAzNHB4O1xufVxuXG46aG9zdC9kZWVwLyBuZzItc3QtdGJvZHktY3VzdG9tIGEubmcyLXNtYXJ0LWFjdGlvbi5uZzItc21hcnQtYWN0aW9uLWN1c3RvbS1jdXN0b206aG92ZXIge1xuICAgIGNvbG9yOiAjMDAwMDAwO1xufSIsIi5ib2RlcnNjc3Mge1xuICBiYWNrZ3JvdW5kOiAjZmZmO1xuICBtYXJnaW46IDRweDtcbiAgYm9yZGVyOiAzcHggc29saWQgI2RhZGZlNjsgfVxuXG5AbWVkaWEgKG1pbi13aWR0aDogNTc2cHgpIHtcbiAgL2RlZXAvIG1vZGFsLm1vZGFscyAubW9kYWwtZGlhbG9nIHtcbiAgICBtYXgtd2lkdGg6IDEwMDBweCAhaW1wb3J0YW50O1xuICAgIG1hcmdpbjogMS43NXJlbSBhdXRvOyB9XG4gIC9kZWVwLyBtb2RhbC5tb2RhbHMubW9kYSAubW9kYWwtZGlhbG9nIC5tb2RhbC1ib2R5IHtcbiAgICBoZWlnaHQ6IDEwOTBweDsgfSB9XG5cbkBtZWRpYSAobWluLXdpZHRoOiA1NzZweCkge1xuICAvZGVlcC8gbW9kYWwuaW52b2ljZSAubW9kYWwtZGlhbG9nIHtcbiAgICBtYXgtd2lkdGg6IDEwMDBweCAhaW1wb3J0YW50O1xuICAgIG1hcmdpbjogMS43NXJlbSBhdXRvOyB9IH1cblxuLmJvdHRvbXMge1xuICBtYXJnaW4tYm90dG9tOiA1cHg7IH1cblxuL2RlZXAvIG5iLWNoZWNrYm94IGxhYmVsIHtcbiAgbWFyZ2luLXRvcDogMjdweCAhaW1wb3J0YW50OyB9XG5cbi9kZWVwLyBtb2RhbC5tb2RhbGZpcnN0IC5tb2RhbC1kaWFsb2cge1xuICBtYXgtd2lkdGg6IDcwMHB4ICFpbXBvcnRhbnQ7XG4gIG1hcmdpbjogMjM0cHggNjAwcHggYXV0byBhdXRvICFpbXBvcnRhbnQ7IH1cblxuLmNvbnRhaW5lciB7XG4gIHBhZGRpbmctbGVmdDogMHB4OyB9XG5cbm5iLWNhcmQge1xuICBib3JkZXI6IDBweCBzb2xpZCAjZDVkYmUwOyB9XG5cblt0eXBlPVwiY2hlY2tib3hcIl06bm90KDpjaGVja2VkKSxcblt0eXBlPVwiY2hlY2tib3hcIl06Y2hlY2tlZCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogLTk5OTlweDsgfVxuXG5bdHlwZT1cImNoZWNrYm94XCJdOm5vdCg6Y2hlY2tlZCkgKyBsYWJlbCxcblt0eXBlPVwiY2hlY2tib3hcIl06Y2hlY2tlZCArIGxhYmVsIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBwYWRkaW5nLWxlZnQ6IDEuOTVlbTsgfVxuXG4vKiBjaGVja2JveCBhc3BlY3QgKi9cblt0eXBlPVwiY2hlY2tib3hcIl06bm90KDpjaGVja2VkKSArIGxhYmVsOmJlZm9yZSxcblt0eXBlPVwiY2hlY2tib3hcIl06Y2hlY2tlZCArIGxhYmVsOmJlZm9yZSB7XG4gIGNvbnRlbnQ6ICcnO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDA7XG4gIHRvcDogMDtcbiAgd2lkdGg6IDEuMjVlbTtcbiAgaGVpZ2h0OiAxLjI1ZW07XG4gIGJvcmRlcjogMnB4IHNvbGlkICNjY2M7XG4gIGJhY2tncm91bmQ6ICNmZmY7XG4gIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgYm94LXNoYWRvdzogaW5zZXQgMCAxcHggM3B4IHJnYmEoMCwgMCwgMCwgMC4xKTsgfVxuXG4vKiBjaGVja2VkIG1hcmsgYXNwZWN0ICovXG5bdHlwZT1cImNoZWNrYm94XCJdOm5vdCg6Y2hlY2tlZCkgKyBsYWJlbDphZnRlcixcblt0eXBlPVwiY2hlY2tib3hcIl06Y2hlY2tlZCArIGxhYmVsOmFmdGVyIHtcbiAgY29udGVudDogJ1xcMjcxM1xcMDAyMCc7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAuMTVlbTtcbiAgbGVmdDogLjIyZW07XG4gIGZvbnQtc2l6ZTogMS4zZW07XG4gIGxpbmUtaGVpZ2h0OiAwLjg7XG4gIGNvbG9yOiAjMDlhZDdlO1xuICB0cmFuc2l0aW9uOiBhbGwgLjJzO1xuICBmb250LWZhbWlseTogJ0x1Y2lkYSBTYW5zIFVuaWNvZGUnLCAnQXJpYWwgVW5pY29kZSBNUycsIEFyaWFsOyB9XG5cbi8qIGNoZWNrZWQgbWFyayBhc3BlY3QgY2hhbmdlcyAqL1xuW3R5cGU9XCJjaGVja2JveFwiXTpub3QoOmNoZWNrZWQpICsgbGFiZWw6YWZ0ZXIge1xuICBvcGFjaXR5OiAwO1xuICB0cmFuc2Zvcm06IHNjYWxlKDApOyB9XG5cblt0eXBlPVwiY2hlY2tib3hcIl06Y2hlY2tlZCArIGxhYmVsOmFmdGVyIHtcbiAgb3BhY2l0eTogMTtcbiAgdHJhbnNmb3JtOiBzY2FsZSgxKTsgfVxuXG4vKiBkaXNhYmxlZCBjaGVja2JveCAqL1xuW3R5cGU9XCJjaGVja2JveFwiXTpkaXNhYmxlZDpub3QoOmNoZWNrZWQpICsgbGFiZWw6YmVmb3JlLFxuW3R5cGU9XCJjaGVja2JveFwiXTpkaXNhYmxlZDpjaGVja2VkICsgbGFiZWw6YmVmb3JlIHtcbiAgYm94LXNoYWRvdzogbm9uZTtcbiAgYm9yZGVyLWNvbG9yOiAjYmJiO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZGRkOyB9XG5cblt0eXBlPVwiY2hlY2tib3hcIl06ZGlzYWJsZWQ6Y2hlY2tlZCArIGxhYmVsOmFmdGVyIHtcbiAgY29sb3I6ICM5OTk7IH1cblxuW3R5cGU9XCJjaGVja2JveFwiXTpkaXNhYmxlZCArIGxhYmVsIHtcbiAgY29sb3I6ICNhYWE7IH1cblxuLyogYWNjZXNzaWJpbGl0eSAqL1xuLyogaG92ZXIgc3R5bGUganVzdCBmb3IgaW5mb3JtYXRpb24gKi9cbmxhYmVsOmhvdmVyOmJlZm9yZSB7XG4gIGJvcmRlcjogMnB4IHNvbGlkICM0Nzc4ZDkgIWltcG9ydGFudDsgfVxuXG4uQTQge1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgd2lkdGg6IDIxY207XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBtYXJnaW46IDAgYXV0bztcbiAgcGFkZGluZzogMTBweCAyNXB4O1xuICBtYXJnaW4tYm90dG9tOiAwLjVjbTtcbiAgYm94LXNoYWRvdzogMCAwIDAuNWNtIHJnYmEoMCwgMCwgMCwgMC41KTtcbiAgb3ZlcmZsb3cteTogc2Nyb2xsO1xuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICBmb250LXNpemU6IDEycHQ7IH1cblxuLmNsb3NlMSB7XG4gIHRleHQtc2hhZG93OiBub25lO1xuICBjb2xvcjogIzE4MTgxODtcbiAgcGFkZGluZzogMXJlbSAxcmVtO1xuICBtYXJnaW46IC0xcmVtIC0xcmVtIC0xcmVtIGF1dG87XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gIGJvcmRlcjogMDtcbiAgLXdlYmtpdC1hcHBlYXJhbmNlOiBub25lO1xuICBmbG9hdDogcmlnaHQ7XG4gIGZvbnQtc2l6ZTogMS41cmVtO1xuICBmb250LXdlaWdodDogNzAwO1xuICBsaW5lLWhlaWdodDogMTtcbiAgY29sb3I6ICMwMDA7XG4gIHRleHQtc2hhZG93OiAwIDFweCAwICNmZmY7XG4gIG9wYWNpdHk6IC41OyB9XG5cbi9kZWVwLyBtb2RhbC5pbnZvaWNlIC5tb2RhbCAubW9kYWwtZGlhbG9nIC5tb2RhbC1jb250ZW50IC5tb2RhbC1oZWFkZXIgLmNsb3NlIHtcbiAgZGlzcGxheTogbm9uZSAhaW1wb3J0YW50OyB9XG5cbi9kZWVwLyBtb2RhbC5tb2RhbHMgLm1vZGFsIC5tb2RhbC1kaWFsb2cgLm1vZGFsLWNvbnRlbnQgLm1vZGFsLWhlYWRlciAuY2xvc2Uge1xuICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7IH1cblxuLm5iLWVkaXQge1xuICBjb2xvcjogIzAwMCAhaW1wb3J0YW50OyB9XG5cbi5uYi1jaGVja21hcmsge1xuICBjb2xvcjogIzAwMCAhaW1wb3J0YW50OyB9XG5cbi5uYi1jbG9zZSB7XG4gIGNvbG9yOiAjMDAwICFpbXBvcnRhbnQ7IH1cblxuLm9kZF9yb3cge1xuICBiYWNrZ3JvdW5kOiAjZjlmOWY5OyB9XG5cbi5yaWdodF9wbGFjZWhvbGRlciB7XG4gIHRleHQtYWxpZ246IHJpZ2h0OyB9XG5cbi5jZW50cmUge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7IH1cblxuLm5ldyB7XG4gIG1hcmdpbi10b3A6IDYlO1xuICBmb250LXNpemU6IHNtYWxsO1xuICBtYXJnaW4tYm90dG9tOiA2JTsgfVxuXG4ubmV3Um93IHtcbiAgbWFyZ2luLWxlZnQ6IC0wLjElO1xuICBtYXJnaW4tcmlnaHQ6IC0wLjElOyB9XG5cbi50YWJsZUhlYWRlciB7XG4gIGJvcmRlci10b3A6IDJweCBzb2xpZDtcbiAgYm9yZGVyLXJpZ2h0OiAycHggc29saWQ7XG4gIHRleHQtYWxpZ246IGNlbnRlcjsgfVxuXG4udGF4IHtcbiAgYm9yZGVyLXJpZ2h0OiAycHggc29saWQ7XG4gIGJvcmRlci1ib3R0b206IDJweCBzb2xpZDsgfVxuXG4uaGVhZGluZyB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMzNTdFQkQ7XG4gIGNvbG9yOiAjRkZGO1xuICBtYXJnaW4tYm90dG9tOiAzZW07XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbGluZS1oZWlnaHQ6IDEuMmVtOyB9XG5cbmEge1xuICBjdXJzb3I6IHBvaW50ZXI7IH1cblxuYTpob3ZlciB7XG4gIGNvbG9yOiBsaWdodGdyZWVuOyB9XG5cbnRleHRhcmVhIHtcbiAgYm9yZGVyLWxlZnQ6IG5vbmUgIWltcG9ydGFudDtcbiAgYm9yZGVyLXJpZ2h0OiBub25lICFpbXBvcnRhbnQ7XG4gIGJvcmRlci10b3A6IG5vbmUgIWltcG9ydGFudDsgfVxuXG5pbnB1dFt0eXBlPXRleHRdLFxuaW5wdXRbdHlwZT1udW1iZXJdLFxuaW5wdXRbdHlwZT1kYXRlXSxcbnNlbGVjdCxcbnRleHRhcmVhIHtcbiAgZm9udC1mYW1pbHk6ICdTb3VyY2UgU2FucyBQcm8nLCBTYW5zLXNlcmlmO1xuICBmb250LXNpemU6IDFlbTtcbiAgYm9yZGVyOiBub25lO1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2Q3ZDdkNztcbiAgcGFkZGluZzogNHB4O1xuICBvdXRsaW5lOiBub25lOyB9XG5cbi5pdGVtcy10YWJsZSB7XG4gIHdpZHRoOiAxMDAlOyB9XG5cbi5pdGVtcy10YWJsZSB0aCB7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIGZvbnQtd2VpZ2h0OiAzMDA7XG4gIHBhZGRpbmc6IDEwcHggNXB4IDJweDtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDsgfVxuXG4uaXRlbXMtdGFibGUgaW5wdXRbdHlwZT10ZXh0XSxcbi5pdGVtcy10YWJsZSBpbnB1dFt0eXBlPW51bWJlcl0sXG4uaXRlbXMtdGFibGUgaW5wdXRbdHlwZT1kYXRlXSxcbi5pdGVtcy10YWJsZSBzZWxlY3Qge1xuICB3aWR0aDogMTAwJTsgfVxuXG4jaXRlbXMgdGQge1xuICBwYWRkaW5nOiA1cHg7XG4gIGJhY2tncm91bmQ6ICNlZWU7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZmZmOyB9XG5cbiNpdGVtcyAuZGVzYyB7XG4gIG1pbi13aWR0aDogMjAwcHg7IH1cblxuI2l0ZW1zIC50b3RhbC1wcmljZSB7XG4gIHRleHQtYWxpZ246IHJpZ2h0O1xuICBmb250LXdlaWdodDogNjAwOyB9XG5cbi5pbnZvaWNlLXRvdGFscyB7XG4gIHdpZHRoOiA1MCU7XG4gIGZsb2F0OiByaWdodDsgfVxuXG4uaW52b2ljZS10b3RhbHMgdGgge1xuICBmb250LXdlaWdodDogMzAwOyB9XG5cbi5pbnZvaWNlLXRvdGFscyB0aCxcbi5pbnZvaWNlLXRvdGFscyB0ZCB7XG4gIHRleHQtYWxpZ246IHJpZ2h0O1xuICBwYWRkaW5nOiA1cHg7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZDdkN2Q3OyB9XG5cbi5pbnZvaWNlLXRvdGFscyAudGF4LWNvbnRhaW5lciB0ZCB7XG4gIGJvcmRlci1ib3R0b206IG5vbmU7XG4gIHBhZGRpbmc6IDA7IH1cblxuI3RheHMge1xuICB3aWR0aDogMTAwJTsgfVxuXG4jdGF4cyB0aGVhZCB0aCB7XG4gIGZvbnQtd2VpZ2h0OiAzMDA7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlZWU7IH1cblxuI3RheHMgdGgsXG4jdGF4cyB0ZCB7XG4gIHRleHQtYWxpZ246IHJpZ2h0O1xuICBwYWRkaW5nOiA1cHg7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZDdkN2Q3OyB9XG5cbmltZyB7XG4gIHdpZHRoOiAzMDNweCAhaW1wb3J0YW50OyB9XG5cbkBtZWRpYSAobWF4LXdpZHRoOiAxMjgwcHgpIHtcbiAgaW1nIHtcbiAgICB3aWR0aDogMTY3cHggIWltcG9ydGFudDsgfSB9XG5cbm5iLWNhcmQtYm9keSB7XG4gIGJhY2tncm91bmQtY29sb3I6IGhpZGRlbiAhaW1wb3J0YW50OyB9XG5cbkBtZWRpYSAobWluLXdpZHRoOiA1NzZweCkge1xuICAvZGVlcC8gbW9kYWwuaW52b2ljZSAubW9kYWwtZGlhbG9nIHtcbiAgICBtYXgtd2lkdGg6IDEwMDBweCAhaW1wb3J0YW50O1xuICAgIG1hcmdpbjogMS43NXJlbSBhdXRvOyB9IH1cblxuOmhvc3QvZGVlcC8gbmcyLXN0LXRib2R5LWN1c3RvbSBhLm5nMi1zbWFydC1hY3Rpb24ubmcyLXNtYXJ0LWFjdGlvbi1jdXN0b20tY3VzdG9tIHtcbiAgd2lkdGg6IDUwcHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAwLjFlbTsgfVxuXG46aG9zdC9kZWVwLyBuZzItc3QtdGJvZHktY3VzdG9tIGEubmcyLXNtYXJ0LWFjdGlvbi5uZzItc21hcnQtYWN0aW9uLWN1c3RvbS1jdXN0b20gaW1nIHtcbiAgd2lkdGg6IDU3JSAhaW1wb3J0YW50O1xuICBtYXJnaW4tbGVmdDogMzRweDsgfVxuXG46aG9zdC9kZWVwLyBuZzItc3QtdGJvZHktY3VzdG9tIGEubmcyLXNtYXJ0LWFjdGlvbi5uZzItc21hcnQtYWN0aW9uLWN1c3RvbS1jdXN0b206aG92ZXIge1xuICBjb2xvcjogIzAwMDAwMDsgfVxuIl19 */"

/***/ }),

/***/ "./src/app/pages/ui-features/customer/customers/detail-customers/customer-invoices/customer-invoices.component.ts":
/*!************************************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/customer/customers/detail-customers/customer-invoices/customer-invoices.component.ts ***!
  \************************************************************************************************************************/
/*! exports provided: CustomerInvoicesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerInvoicesComponent", function() { return CustomerInvoicesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var angular_custom_modal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! angular-custom-modal */ "./node_modules/angular-custom-modal/angular-custom-modal.es5.js");
/* harmony import */ var jspdf__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! jspdf */ "./node_modules/jspdf/dist/jspdf.min.js");
/* harmony import */ var jspdf__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(jspdf__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var html2canvas__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! html2canvas */ "./node_modules/html2canvas/dist/npm/index.js");
/* harmony import */ var html2canvas__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(html2canvas__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _confirmation_dialog_confirmation_dialog_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../../../confirmation-dialog/confirmation-dialog.service */ "./src/app/confirmation-dialog/confirmation-dialog.service.ts");
/* harmony import */ var _services_currency_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../../../services/currency.service */ "./src/app/services/currency.service.ts");
/* harmony import */ var _services_Inventory_inventory_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../../../services/Inventory/inventory.service */ "./src/app/services/Inventory/inventory.service.ts");
/* harmony import */ var _services_customer_customer_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../../../services/customer/customer.service */ "./src/app/services/customer/customer.service.ts");
/* harmony import */ var _services_jobs_invoice_challans_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../../../../services/jobs/invoice-challans.service */ "./src/app/services/jobs/invoice-challans.service.ts");
/* harmony import */ var _services_settings_settings_services_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../../../../services/settings/settings-services.service */ "./src/app/services/settings/settings-services.service.ts");
/* harmony import */ var _services_admin_isertParts_insert_parts_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../../../../services/admin/isertParts/insert-parts.service */ "./src/app/services/admin/isertParts/insert-parts.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};












var CustomerInvoicesComponent = /** @class */ (function () {
    function CustomerInvoicesComponent(partDetailsServ, InventoryServ, currency, // VIPIN CODE FOR CURRENCY
    confirmationDialogService, settingservice, invoices, CustomerServ) {
        var _this = this;
        this.partDetailsServ = partDetailsServ;
        this.InventoryServ = InventoryServ;
        this.currency = currency;
        this.confirmationDialogService = confirmationDialogService;
        this.settingservice = settingservice;
        this.invoices = invoices;
        this.CustomerServ = CustomerServ;
        this.source = new ng2_smart_table__WEBPACK_IMPORTED_MODULE_1__["LocalDataSource"]();
        this.sources = new ng2_smart_table__WEBPACK_IMPORTED_MODULE_1__["LocalDataSource"]();
        this.sources1 = new ng2_smart_table__WEBPACK_IMPORTED_MODULE_1__["LocalDataSource"]();
        this.displayType = 'All';
        this.catagorys = ['All', 'Invoice', 'Challan', 'General Proforma Invoice', 'General Final Invoice'];
        this.displayButton = true;
        this.totalAmt = 0;
        this.regExpr = /[₹,]/g;
        this.stage = false;
        this.length = false;
        this.displayList = [];
        this.detail = false;
        this.materials = false;
        this.eway = 'NIL';
        this.nil = true;
        this.partsTableData = [];
        this.designations = ['First', 'Advance', 'Progress', 'Proforma'];
        this.updateDispatchArray = [];
        this.logoSrc = '../../../../../assets/images/logos.png';
        this.invoiceArray = [];
        this.invoiceArray1 = [];
        this.setting = {
            selectMode: 'multi',
            add: {
                addButtonContent: '<i class="nb-plus"></i>',
                createButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
                confirmCreate: true,
            },
            edit: {
                editButtonContent: '<i class="nb-edit"></i>',
                saveButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
                confirmSave: true,
            },
            delete: {
                deleteButtonContent: '<i class="nb-trash"></i>',
                confirmDelete: true,
            },
            actions: {
                edit: false,
                add: false,
                delete: false,
            },
            columns: {
                partName: {
                    title: 'Material Name',
                    type: 'string',
                },
                HSN_Code: {
                    title: 'HSN Code',
                    type: 'string',
                },
                stages: {
                    title: 'Stage',
                    type: 'string',
                },
            },
        };
        this.settings = {
            add: {
                addButtonContent: '<i class="nb-plus"></i>',
                createButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
                confirmCreate: true,
            },
            edit: {
                editButtonContent: '<i class="nb-edit"></i>',
                saveButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
                confirmSave: true,
            },
            delete: {
                deleteButtonContent: '<i class="nb-trash"></i>',
                confirmDelete: true,
            },
            actions: {
                edit: true,
                add: false,
                delete: false,
                position: 'right'
            },
            columns: {
                partName: {
                    title: 'Material Name',
                    type: 'string',
                    editable: false
                },
                totalQty: {
                    title: 'Available',
                    type: 'string',
                    editable: false
                },
                unit: {
                    title: 'Unit',
                    type: 'html',
                    editor: {
                        type: 'list',
                        config: {
                            list: [
                                { value: 'No', title: 'No' },
                                { value: 'MTR', title: 'MTR' },
                                { value: 'LTR', title: 'LTR' },
                                { value: 'SET', title: 'SET' }
                            ],
                        },
                    }
                },
                unitPrice: {
                    title: 'Price',
                    type: 'string',
                    editable: true,
                    valuePrepareFunction: function (value) { return value === 'Total' ? value : _this.currency.convertToInrFormat(value); },
                },
                dispatched: {
                    title: 'Dispatched Qty',
                    type: 'number',
                },
            },
        };
        this.settings1 = {
            add: {
                addButtonContent: '<i class="nb-plus"></i>',
                createButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
            },
            edit: {
                editButtonContent: '<i class="nb-edit"></i>',
                saveButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
            },
            delete: {
                deleteButtonContent: '<i class="nb-trash"></i>',
                confirmDelete: true,
            },
            actions: {
                edit: false,
                add: false,
                delete: false,
                custom: [
                    {
                        name: 'view',
                        title: '<img src="./assets/icons/eye-outline.png" >',
                    },
                ],
                position: 'right'
            },
            columns: {
                // fileName: {
                //   title: 'File Name',
                //   type: 'string',
                // },
                invoiceType: {
                    title: 'Invoice Type',
                    type: 'string',
                },
                amt: {
                    title: 'Invoice Amount',
                    type: 'string',
                },
            },
        };
        this.port = this.settingservice.port;
    }
    CustomerInvoicesComponent.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.getCustomerDetail();
                this.getInvoiceDetails();
                return [2 /*return*/];
            });
        });
    };
    /**
        * Get Jobs Detail
        */
    CustomerInvoicesComponent.prototype.getCustomerDetail = function () {
        var _this = this;
        this.CustomerServ.userCustomer
            .subscribe(function (data) {
            _this.customer = data;
            _this.id = data.id;
            _this.location = data.deliveryAddress;
            _this.address = data.address;
            _this.clientGst = data.gst;
            _this.clientName = data.companyName;
            var _a = _this.getDateFunction(), day = _a.day, fullYear = _a.fullYear, month = _a.month, time = _a.time;
            _this.displaydate = day + '-' + month + '-' + fullYear;
            _this.displaytime = time;
        }, function (error) {
            console.log(error);
        });
    };
    CustomerInvoicesComponent.prototype.getInvoiceDetails = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.totalAmt = 0;
                        this.invoiceArray = [];
                        return [4 /*yield*/, this.CustomerServ.getInvoices(this.id)];
                    case 1:
                        result = _a.sent();
                        if (result !== null) {
                            if (result.length !== 0) {
                                result.generalInvoices.forEach(function (ele) {
                                    var obj = {};
                                    if (_this.displayType === 'General Final Invoice' && ele.originalname.split('_')[0] === 'Final') {
                                        _this.totalAmt += +ele.totalAmt.replace(_this.regExpr, '').split('.')[0];
                                        Object.assign(obj, { 'path': ele.filename, 'fileName': ele.originalname, 'amt': ele.totalAmt, 'invoiceType': ele.originalname.split('_')[0] });
                                        _this.invoiceArray.push(obj);
                                    }
                                    if (_this.displayType === 'General Proforma Invoice' && ele.originalname.split('_')[0] === 'Proforma') {
                                        _this.totalAmt += +ele.totalAmt.replace(_this.regExpr, '').split('.')[0];
                                        Object.assign(obj, { 'path': ele.filename, 'fileName': ele.originalname, 'amt': ele.totalAmt, 'invoiceType': ele.originalname.split('_')[0] });
                                        _this.invoiceArray.push(obj);
                                    }
                                    if (_this.displayType === 'All') {
                                        _this.totalAmt += +ele.totalAmt.replace(_this.regExpr, '').split('.')[0];
                                        Object.assign(obj, { 'path': ele.filename, 'fileName': ele.originalname, 'amt': ele.totalAmt, 'invoiceType': ele.originalname.split('_')[0] });
                                        _this.invoiceArray.push(obj);
                                    }
                                });
                            }
                        }
                        if (this.displayType === 'All') {
                            this.getInvoice();
                        }
                        else {
                            this.totalAmt = this.currency.convertToInrFormat(this.totalAmt);
                            this.sources1.load(this.invoiceArray);
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    CustomerInvoicesComponent.prototype.closeSelectedItems = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.componentInsideModal.close();
                return [2 /*return*/];
            });
        });
    };
    // Dispaly Popup and Get all Materials List
    CustomerInvoicesComponent.prototype.showList = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        this.displayButton = false;
                        this.materials = true;
                        this.displayList = [];
                        _a = this;
                        return [4 /*yield*/, this.partDetailsServ.getPartDetails()];
                    case 1:
                        _a.listOfInventory = _b.sent();
                        setTimeout(function () { return __awaiter(_this, void 0, void 0, function () {
                            var _a, count;
                            var _this = this;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        _a = this;
                                        return [4 /*yield*/, this.InventoryServ.getInventory()];
                                    case 1:
                                        _a.listOfPrice = _b.sent();
                                        count = 0;
                                        this.listOfInventory.forEach(function (ele) {
                                            count = 0;
                                            _this.listOfPrice.forEach(function (ele1, i) {
                                                if (ele1.PartDetails[0].partNumber === ele.HSN_Code && count === 0) {
                                                    var price = parseInt(_this.listOfPrice[i].PartDetails[0].unitPrice);
                                                    price = _this.currency.convertToInrFormat(price);
                                                    Object.assign(ele, { unitPrice: price }, { totalQty: _this.listOfPrice[i].PartDetails[0].available }, { unit: _this.listOfPrice[i].PartDetails[0].unit });
                                                    count++;
                                                }
                                            });
                                        });
                                        this.listOfInventory.forEach(function (ele, i) {
                                            if (ele.totalQty !== undefined && ele.totalQty !== null && ele.totalQty !== 'null' && ele.unitPrice !== undefined && ele.unitPrice !== null && ele.unitPrice !== 'null') {
                                                _this.displayList.push(ele);
                                            }
                                        });
                                        this.source.load(this.displayList);
                                        this.componentInsideModal.open();
                                        return [2 /*return*/];
                                }
                            });
                        }); }, 2000);
                        return [2 /*return*/];
                }
            });
        });
    };
    // to get all the materials selected
    CustomerInvoicesComponent.prototype.onUserRowSelect = function (event) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.partsTableData = event.selected;
                this.sources.load(this.partsTableData);
                return [2 /*return*/];
            });
        });
    };
    CustomerInvoicesComponent.prototype.displaySelectedItems = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.componentInsideModal.close();
                return [2 /*return*/];
            });
        });
    };
    CustomerInvoicesComponent.prototype.onUserRowSelect0 = function (event) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                console.log(event.data);
                return [2 /*return*/];
            });
        });
    };
    // update the edited materials
    CustomerInvoicesComponent.prototype.onSaveConfirm = function (event) {
        return __awaiter(this, void 0, void 0, function () {
            var resultAleart, data_1, unitPrice, totalINR, total;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.confirmationDialogService.confirm('Alert Message', 'Are you sure you want to Update?')];
                    case 1:
                        resultAleart = _a.sent();
                        if (resultAleart) {
                            data_1 = event.newData;
                            unitPrice = data_1.unitPrice;
                            if (unitPrice[0] === '₹') {
                                unitPrice = unitPrice.replace(this.regExpr, "");
                            }
                            totalINR = this.currency.convertToInrFormat(data_1.dispatched * unitPrice);
                            total = data_1.dispatched * unitPrice;
                            if (data_1.unitPrice[0] !== '₹') {
                                data_1.unitPrice = this.currency.convertToInrFormat(data_1.unitPrice);
                            }
                            Object.assign(data_1, { totalINR: totalINR }, { total: total });
                            if (this.updateDispatchArray.length === 0) {
                                this.updateDispatchArray.push(data_1);
                            }
                            else {
                                this.updateDispatchArray.forEach(function (ele, i) {
                                    if (ele.HSN_Code === data_1.HSN_Code) {
                                        _this.updateDispatchArray.splice(i, 1);
                                    }
                                });
                                this.updateDispatchArray.push(data_1);
                            }
                            this.calculateAmt();
                            event.confirm.resolve();
                        }
                        else {
                            event.confirm.reject();
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    // to calculate amt after each material is edited
    CustomerInvoicesComponent.prototype.calculateAmt = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.invoiceTotal = 0;
                this.updateDispatchArray.forEach(function (ele) {
                    _this.invoiceTotal = parseFloat(ele.total) + parseFloat(_this.invoiceTotal);
                });
                this.taxAmt = ((this.invoiceTotal * 9) / 100);
                this.grandTotal = this.invoiceTotal + (2 * this.taxAmt);
                this.grandTotal = this.grandTotal.toString();
                if (this.grandTotal.split('.')[1][0] >= 5) {
                    this.grandTotal = parseInt(this.grandTotal.split('.')[0]) + 1;
                }
                else {
                    this.grandTotal = parseInt(this.grandTotal.split('.')[0]);
                }
                this.checkTotal = this.grandTotal;
                this.totalInWords = this.currency.convertNumToWords(this.grandTotal);
                this.grandTotal = this.currency.convertToInrFormat(parseFloat(this.grandTotal).toFixed(2));
                this.taxAmt = this.currency.convertToInrFormat(parseFloat(this.taxAmt).toFixed(2));
                this.invoiceTotal = this.currency.convertToInrFormat(this.invoiceTotal);
                return [2 /*return*/];
            });
        });
    };
    // to show the invoice preview if Proforma is selected
    CustomerInvoicesComponent.prototype.previewInvoice = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.invoiceType = 'Proforma';
                if (this.updateDispatchArray.length > 0) {
                    this.stage = true;
                    this.length = true;
                }
                this.invoiceCount();
                this.componentInvoiceModal.open();
                return [2 /*return*/];
            });
        });
    };
    CustomerInvoicesComponent.prototype.cancel = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.componentInvoiceModal.close();
                return [2 /*return*/];
            });
        });
    };
    // to get the dispatch employee
    CustomerInvoicesComponent.prototype.selectEmployee = function (dispatch) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.dispatchBy = dispatch;
                if (this.dispatchBy !== undefined && this.dispatchdate !== undefined) {
                    this.detail = true;
                }
                return [2 /*return*/];
            });
        });
    };
    // to get the dispatch date
    CustomerInvoicesComponent.prototype.selectDate = function (date) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.dispatchdate = date;
                if (this.dispatchBy !== undefined && this.dispatchdate !== undefined) {
                    this.detail = true;
                }
                return [2 /*return*/];
            });
        });
    };
    // to show invoice if general is selected
    CustomerInvoicesComponent.prototype.ShowInvoice = function () {
        return __awaiter(this, void 0, void 0, function () {
            var resultAleart;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!(this.checkTotal < 50000)) return [3 /*break*/, 1];
                        this.componentDispatchModal.close();
                        this.componentInvoiceModal.open();
                        return [3 /*break*/, 3];
                    case 1: return [4 /*yield*/, this.confirmationDialogService.confirm('Alert Message', 'Total Invoice amount is either equal to or greater then ₹50,000. Do you want to Proceed?')];
                    case 2:
                        resultAleart = _a.sent();
                        if (resultAleart) {
                            this.componentDispatchModal.close();
                            this.componentEwayModal.open();
                        }
                        _a.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    CustomerInvoicesComponent.prototype.updateEway = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.componentEwayModal.close();
                this.componentInvoiceModal.open();
                return [2 /*return*/];
            });
        });
    };
    // check eway number
    CustomerInvoicesComponent.prototype.checkLength = function (e) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                if (e.toString().length === 12) {
                    this.nil = false;
                    this.eway = e;
                }
                else {
                    this.nil = true;
                }
                return [2 /*return*/];
            });
        });
    };
    // pdf view
    CustomerInvoicesComponent.prototype.createPreview = function () {
        return __awaiter(this, void 0, void 0, function () {
            var data, resultAleart;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        data = document.getElementById('contentToConvert');
                        return [4 /*yield*/, this.confirmationDialogService.confirm('Alert Message', 'Are you sure you want to Download it?')];
                    case 1:
                        resultAleart = _a.sent();
                        if (resultAleart) {
                            html2canvas__WEBPACK_IMPORTED_MODULE_4___default()(data).then(function (canvas) { return __awaiter(_this, void 0, void 0, function () {
                                var imgWidth, pageHeight, imgHeight, heightLeft, img, pdf, position;
                                var _this = this;
                                return __generator(this, function (_a) {
                                    imgWidth = 208;
                                    pageHeight = 272;
                                    imgHeight = canvas.height * imgWidth / canvas.width;
                                    heightLeft = imgHeight;
                                    img = canvas.toDataURL('image/png');
                                    pdf = new jspdf__WEBPACK_IMPORTED_MODULE_3__('p', 'mm', 'a4', true);
                                    position = 0;
                                    while (heightLeft >= 0) {
                                        position = heightLeft - imgHeight;
                                        pdf.addImage(img, 'png', 0, position, imgWidth, imgHeight, '', 'FAST');
                                        pdf.addPage();
                                        heightLeft -= pageHeight;
                                    }
                                    setTimeout(function () { return __awaiter(_this, void 0, void 0, function () {
                                        var genetratedPdf, res;
                                        return __generator(this, function (_a) {
                                            switch (_a.label) {
                                                case 0:
                                                    pdf.save(this.invoiceType + '_invoices_' + this.invoiceNo); // Generated PDF 
                                                    genetratedPdf = pdf.output('blob');
                                                    this.data = new FormData();
                                                    this.data.append("general_invoice", genetratedPdf, this.invoiceType + "_invoices_" + this.invoiceNo);
                                                    if (this.dispatchBy !== undefined && this.dispatchdate !== undefined) {
                                                        this.data.append('dispatchBy', this.dispatchBy);
                                                        this.data.append('dispatchdate', this.dispatchdate);
                                                    }
                                                    this.data.append('totalAmt', this.grandTotal);
                                                    this.data.append('date', this.displaydate);
                                                    this.data.append('invoiceNo', this.invoiceNo);
                                                    this.data.append('invoiceType', this.invoiceType);
                                                    this.data.append('clientId', this.id);
                                                    return [4 /*yield*/, this.CustomerServ.createGeneralInvoice(this.data)];
                                                case 1:
                                                    res = _a.sent();
                                                    this.dispatchdate = '';
                                                    this.dispatchBy = '';
                                                    return [2 /*return*/];
                                            }
                                        });
                                    }); }, 1000);
                                    return [2 /*return*/];
                                });
                            }); });
                        }
                        this.componentInvoiceModal.close();
                        this.getInvoiceDetails();
                        this.materials = false;
                        return [2 /*return*/];
                }
            });
        });
    };
    // date function
    CustomerInvoicesComponent.prototype.getDateFunction = function () {
        var date = new Date();
        var day = date.getDate().toString();
        var month = date.getMonth() + 1;
        var fullYear = date.getFullYear().toString();
        var hh = date.getHours();
        var min = date.getMinutes();
        var ampm = (hh >= 12) ? 'PM' : 'AM';
        hh = hh % 12;
        hh = hh ? hh : 12;
        hh = hh < 10 ? '0' + hh : hh;
        min = min < 10 ? '0' + min : min;
        var time = hh + ":" + min + " " + ampm;
        return { day: day, month: month, fullYear: fullYear, time: time };
    };
    // to count the invoice
    CustomerInvoicesComponent.prototype.invoiceCount = function () {
        return __awaiter(this, void 0, void 0, function () {
            var invoice;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.CustomerServ.getGeneralInvoice()];
                    case 1:
                        invoice = _a.sent();
                        this.invoiceNo = 0;
                        this.invoiceName = '';
                        invoice.forEach(function (ele) {
                            ele.generalInvoices.forEach(function (ele1) {
                                if (ele1.originalname.split('_')[0] === 'Proforma' && _this.invoiceType === 'Proforma') {
                                    _this.invoiceNo++;
                                }
                                if (ele1.originalname.split('_')[0] === 'Final' && _this.invoiceType === 'Final') {
                                    _this.invoiceNo++;
                                }
                            });
                        });
                        this.invoiceNo += 1;
                        if (this.invoiceNo === 0) {
                            this.invoiceNo = '001';
                        }
                        else if (this.invoiceNo <= 9) {
                            this.invoiceNo = '00' + this.invoiceNo;
                        }
                        else if (this.invoiceNo > 9 && this.invoiceNo < 100) {
                            this.invoiceNo = '0' + this.invoiceNo;
                        }
                        if (this.invoiceType === 'Proforma') {
                            this.invoiceName = 'Proforma_' + this.invoiceNo;
                        }
                        else {
                            this.invoiceName = 'Final_' + this.invoiceNo;
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    // final invoice
    CustomerInvoicesComponent.prototype.finalInvoice = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.invoiceType = 'Final';
                if (this.updateDispatchArray.length > 0) {
                    this.stage = true;
                    this.length = true;
                }
                this.invoiceCount();
                this.componentDispatchModal.open();
                return [2 /*return*/];
            });
        });
    };
    CustomerInvoicesComponent.prototype.HideInvoice = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.componentDispatchModal.close();
                return [2 /*return*/];
            });
        });
    };
    CustomerInvoicesComponent.prototype.showAllInvoices = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.materials = false;
                this.displayButton = true;
                return [2 /*return*/];
            });
        });
    };
    CustomerInvoicesComponent.prototype.selectcategory = function (e) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.displayType = e;
                if (this.displayType === 'Invoice' || this.displayType === 'Challan') {
                    this.getInvoice();
                }
                else {
                    this.getInvoiceDetails();
                }
                return [2 /*return*/];
            });
        });
    };
    CustomerInvoicesComponent.prototype.onUserRowSelect1 = function (event) {
        if (event.data.path.split('/')[0] !== 'uploads') {
            window.open("//" + location.hostname + ":" + this.port + "/invoices/" + event.data.path, '_blank');
        }
        else {
            window.open("//" + location.hostname + ":" + this.port + "/invoices/" + event.data.filename, '_blank');
        }
    };
    CustomerInvoicesComponent.prototype.getInvoice = function () {
        return __awaiter(this, void 0, void 0, function () {
            var invoice;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (this.displayType !== 'All') {
                            this.invoiceArray = [];
                            this.totalAmt = 0;
                        }
                        this.invoiceArrayChallan = [];
                        this.invoiceArrayInvoice = [];
                        return [4 /*yield*/, this.invoices.getAllInvoices()];
                    case 1:
                        invoice = _a.sent();
                        invoice.forEach(function (ele) { return __awaiter(_this, void 0, void 0, function () {
                            var _this = this;
                            return __generator(this, function (_a) {
                                if (ele.clientId === this.id) {
                                    if (ele.stage1.length > 0) {
                                        ele.stage1.forEach(function (ele2) {
                                            Object.assign(ele2, { 'jobNo': ele.jobNo });
                                            if (ele2.fieldname === 'invoice_challan') {
                                                _this.totalAmt += +ele2.totalAmt.replace(_this.regExpr, '').split('.')[0];
                                                Object.assign(ele2, { 'customerName': ele.customerName, 'amt': ele2.totalAmt });
                                                _this.invoiceArrayInvoice.push(ele2);
                                            }
                                            if (ele2.fieldname === 'challan_invoice') {
                                                _this.totalAmt += +ele2.totalAmt.replace(_this.regExpr, '').split('.')[0];
                                                Object.assign(ele2, { 'customerName': ele.customerName, 'amt': ele2.totalAmt });
                                                _this.invoiceArrayChallan.push(ele2);
                                            }
                                        });
                                    }
                                    if (ele.stage2.length > 0) {
                                        ele.stage2.forEach(function (ele3) {
                                            Object.assign(ele3, { 'jobNo': ele.jobNo });
                                            if (ele3.fieldname === 'invoice_challan') {
                                                _this.totalAmt += +ele3.totalAmt.replace(_this.regExpr, '').split('.')[0];
                                                Object.assign(ele3, { 'customerName': ele.customerName, 'amt': ele3.totalAmt });
                                                _this.invoiceArrayInvoice.push(ele3);
                                            }
                                            if (ele3.fieldname === 'challan_invoice') {
                                                _this.totalAmt += +ele3.totalAmt.replace(_this.regExpr, '').split('.')[0];
                                                Object.assign(ele3, { 'customerName': ele.customerName, 'amt': ele3.totalAmt });
                                                _this.invoiceArrayChallan.push(ele3);
                                            }
                                        });
                                    }
                                    if (ele.stage3.length > 0) {
                                        ele.stage3.forEach(function (ele4) {
                                            Object.assign(ele4, { 'jobNo': ele.jobNo });
                                            if (ele4.fieldname === 'invoice_challan') {
                                                _this.totalAmt += +ele4.totalAmt.replace(_this.regExpr, '').split('.')[0];
                                                Object.assign(ele4, { 'customerName': ele.customerName, 'amt': ele4.totalAmt });
                                                _this.invoiceArrayInvoice.push(ele4);
                                            }
                                            if (ele4.fieldname === 'challan_invoice') {
                                                _this.totalAmt += +ele4.totalAmt.replace(_this.regExpr, '').split('.')[0];
                                                Object.assign(ele4, { 'customerName': ele.customerName, 'amt': ele4.totalAmt });
                                                _this.invoiceArrayChallan.push(ele4);
                                            }
                                        });
                                    }
                                    if (ele.stageCommissioning.length > 0) {
                                        ele.stageCommissioning.forEach(function (ele5) {
                                            Object.assign(ele5, { 'jobNo': ele.jobNo });
                                            if (ele5.fieldname === 'invoice_challan') {
                                                _this.totalAmt += +ele5.totalAmt.replace(_this.regExpr, '').split('.')[0];
                                                Object.assign(ele5, { 'customerName': ele.customerName, 'amt': ele5.totalAmt });
                                                _this.invoiceArrayInvoice.push(ele5);
                                            }
                                            if (ele5.fieldname === 'challan_invoice') {
                                                _this.totalAmt += +ele5.totalAmt.replace(_this.regExpr, '').split('.')[0];
                                                Object.assign(ele5, { 'customerName': ele.customerName, 'amt': ele5.totalAmt });
                                                _this.invoiceArrayChallan.push(ele5);
                                            }
                                        });
                                    }
                                }
                                return [2 /*return*/];
                            });
                        }); });
                        if (this.displayType === 'Invoice') {
                            this.invoiceArray = this.invoiceArrayInvoice;
                        }
                        else if (this.displayType === 'Challan') {
                            this.invoiceArray = this.invoiceArrayChallan;
                        }
                        else {
                            this.invoiceArrayInvoice.forEach(function (ele) {
                                _this.invoiceArray.push(ele);
                            });
                            this.invoiceArrayChallan.forEach(function (ele) {
                                _this.invoiceArray.push(ele);
                            });
                        }
                        this.invoiceArray.sort(function (a, b) {
                            return a.invoiceNo - b.invoiceNo;
                        });
                        this.invoiceArray.reverse();
                        this.sources1.load(this.invoiceArray);
                        this.totalAmt = this.currency.convertToInrFormat(this.totalAmt);
                        return [2 /*return*/];
                }
            });
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('componentInsideModal'),
        __metadata("design:type", angular_custom_modal__WEBPACK_IMPORTED_MODULE_2__["ModalComponent"])
    ], CustomerInvoicesComponent.prototype, "componentInsideModal", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('componentInvoiceModal'),
        __metadata("design:type", angular_custom_modal__WEBPACK_IMPORTED_MODULE_2__["ModalComponent"])
    ], CustomerInvoicesComponent.prototype, "componentInvoiceModal", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('componentDispatchModal'),
        __metadata("design:type", angular_custom_modal__WEBPACK_IMPORTED_MODULE_2__["ModalComponent"])
    ], CustomerInvoicesComponent.prototype, "componentDispatchModal", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('componentEwayModal'),
        __metadata("design:type", angular_custom_modal__WEBPACK_IMPORTED_MODULE_2__["ModalComponent"])
    ], CustomerInvoicesComponent.prototype, "componentEwayModal", void 0);
    CustomerInvoicesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-customer-invoices',
            template: __webpack_require__(/*! ./customer-invoices.component.html */ "./src/app/pages/ui-features/customer/customers/detail-customers/customer-invoices/customer-invoices.component.html"),
            styles: [__webpack_require__(/*! ./customer-invoices.component.scss */ "./src/app/pages/ui-features/customer/customers/detail-customers/customer-invoices/customer-invoices.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_admin_isertParts_insert_parts_service__WEBPACK_IMPORTED_MODULE_11__["InsertPartsService"],
            _services_Inventory_inventory_service__WEBPACK_IMPORTED_MODULE_7__["InventoryService"],
            _services_currency_service__WEBPACK_IMPORTED_MODULE_6__["CurrencyService"],
            _confirmation_dialog_confirmation_dialog_service__WEBPACK_IMPORTED_MODULE_5__["ConfirmationDialogService"],
            _services_settings_settings_services_service__WEBPACK_IMPORTED_MODULE_10__["SettingsServicesService"],
            _services_jobs_invoice_challans_service__WEBPACK_IMPORTED_MODULE_9__["InvoiceChallansService"],
            _services_customer_customer_service__WEBPACK_IMPORTED_MODULE_8__["CustomerService"]])
    ], CustomerInvoicesComponent);
    return CustomerInvoicesComponent;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/customer/customers/detail-customers/customer-jobs/customer-jobs.component.html":
/*!******************************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/customer/customers/detail-customers/customer-jobs/customer-jobs.component.html ***!
  \******************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nb-card-body>\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col-4\">\n                <p class=\"back\">Client Name: {{customer.companyName}}</p>\n            </div>\n        </div>\n    </div>\n    <h4 class=\"text-center\">Agreement details </h4>\n    <div class=\"container\">\n\n        <ng2-smart-table [settings]=\"settings\" [source]=\"source\" (deleteConfirm)=\"onDeleteConfirm($event)\" (userRowSelect)=\"onUserRowSelect($event)\">\n        </ng2-smart-table>\n\n    </div>\n</nb-card-body>"

/***/ }),

/***/ "./src/app/pages/ui-features/customer/customers/detail-customers/customer-jobs/customer-jobs.component.scss":
/*!******************************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/customer/customers/detail-customers/customer-jobs/customer-jobs.component.scss ***!
  \******************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "nb-card-body {\n  color: white; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3ByYWthc2gvVmlwaW4vaXJvbmJpcmQvMjBGRUIyMDIwL2lyb25iaXJkLWNsaWVudHMvc3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9jdXN0b21lci9jdXN0b21lcnMvZGV0YWlsLWN1c3RvbWVycy9jdXN0b21lci1qb2JzL2N1c3RvbWVyLWpvYnMuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFFSSxZQUFXLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9jdXN0b21lci9jdXN0b21lcnMvZGV0YWlsLWN1c3RvbWVycy9jdXN0b21lci1qb2JzL2N1c3RvbWVyLWpvYnMuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJuYi1jYXJkLWJvZHl7XG4gICAgLy8gYmFja2dyb3VuZC1jb2xvcjojRTFGNUZFO1xuICAgIGNvbG9yOndoaXRlO1xuICB9XG4gICJdfQ== */"

/***/ }),

/***/ "./src/app/pages/ui-features/customer/customers/detail-customers/customer-jobs/customer-jobs.component.ts":
/*!****************************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/customer/customers/detail-customers/customer-jobs/customer-jobs.component.ts ***!
  \****************************************************************************************************************/
/*! exports provided: CustomerJobsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerJobsComponent", function() { return CustomerJobsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var _services_customer_customer_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../../services/customer/customer.service */ "./src/app/services/customer/customer.service.ts");
/* harmony import */ var _services_customer_site_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../../../services/customer/site.service */ "./src/app/services/customer/site.service.ts");
/* harmony import */ var _services_jobs_total_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../../../services/jobs/total.service */ "./src/app/services/jobs/total.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};







var CustomerJobsComponent = /** @class */ (function () {
    function CustomerJobsComponent(totalJobsServ, router, CustomerServ, SiteServ, datePipe) {
        this.totalJobsServ = totalJobsServ;
        this.router = router;
        this.CustomerServ = CustomerServ;
        this.SiteServ = SiteServ;
        this.datePipe = datePipe;
        this.customer = {};
        this.settings = {
            add: {
                addButtonContent: '<i class="nb-plus"></i>',
                createButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
            },
            edit: {
                editButtonContent: '<i class="nb-edit"></i>',
                saveButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
            },
            delete: {
                deleteButtonContent: '<i class="nb-trash"></i>',
                confirmDelete: true,
            },
            actions: {
                edit: false,
                add: false,
                delete: false,
                position: 'right'
            },
            columns: {
                // id: {
                //   title: 'SI NO',
                //   type: 'number',
                // }, 
                agreementId: {
                    title: 'Agreement No',
                    type: 'string',
                },
                agreementdate: {
                    title: 'Agreement Date',
                    type: 'string',
                    valuePrepareFunction: function (value) {
                        return value.split('-').reverse().join('/');
                    },
                },
                siteName: {
                    title: 'Site Name',
                    type: 'string',
                },
                totalJobPrice: {
                    title: 'Amount',
                    type: 'string',
                },
            },
        };
        this.source = new ng2_smart_table__WEBPACK_IMPORTED_MODULE_3__["LocalDataSource"]();
    }
    CustomerJobsComponent.prototype.onDeleteConfirm = function (event) {
        if (window.confirm('Are you sure you want to delete?')) {
            event.confirm.resolve();
        }
        else {
            event.confirm.reject();
        }
    };
    CustomerJobsComponent.prototype.onUserRowSelect = function (event) {
        // console.log(event);
        var id = event.data.id;
        if (this.router.url === "/pages/customers") {
            this.router.navigate(["/pages/Main_Jobs/" + id]);
            return;
        }
    };
    CustomerJobsComponent.prototype.ngOnInit = function () {
        this.getCustomerDetail();
    };
    /**
     * Get Customer Detail
     */
    CustomerJobsComponent.prototype.getCustomerDetail = function () {
        var _this = this;
        this.CustomerServ.userCustomer
            .subscribe(function (data) {
            _this.customer = data;
            _this.id = data.id;
            _this.getAgreementInfo(data.id);
            // console.log(this.id)
        }, function (error) {
            // console.log(error);
        });
    };
    /**
      * Get Customer Detail from agreements
      */
    CustomerJobsComponent.prototype.getAgreementInfo = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.totalJobsServ.getJobListClintId(id)
                        // console.log(result)
                    ];
                    case 1:
                        result = _a.sent();
                        // console.log(result)
                        this.source.load(result);
                        return [2 /*return*/];
                }
            });
        });
    };
    CustomerJobsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-customer-jobs',
            template: __webpack_require__(/*! ./customer-jobs.component.html */ "./src/app/pages/ui-features/customer/customers/detail-customers/customer-jobs/customer-jobs.component.html"),
            styles: [__webpack_require__(/*! ./customer-jobs.component.scss */ "./src/app/pages/ui-features/customer/customers/detail-customers/customer-jobs/customer-jobs.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_jobs_total_service__WEBPACK_IMPORTED_MODULE_6__["TotalJobsService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _services_customer_customer_service__WEBPACK_IMPORTED_MODULE_4__["CustomerService"],
            _services_customer_site_service__WEBPACK_IMPORTED_MODULE_5__["SiteService"],
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["DatePipe"]])
    ], CustomerJobsComponent);
    return CustomerJobsComponent;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/customer/customers/detail-customers/customer-quotations/customer-quotations.component.html":
/*!******************************************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/customer/customers/detail-customers/customer-quotations/customer-quotations.component.html ***!
  \******************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nb-card-body>\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col-4\">\n                <p class=\"back\">Client Name: {{customer.companyName}}</p>\n            </div>\n        </div>\n    </div>\n    <h4 class=\"text-center\">Quotation details</h4>\n    <div class=\"container\">\n        <ng2-smart-table [settings]=\"settings\" [source]=\"source\" (deleteConfirm)=\"onDeleteConfirm($event)\" (userRowSelect)=\"onUserRowSelect($event)\">\n        </ng2-smart-table>\n        <br>\n    </div>\n</nb-card-body>"

/***/ }),

/***/ "./src/app/pages/ui-features/customer/customers/detail-customers/customer-quotations/customer-quotations.component.scss":
/*!******************************************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/customer/customers/detail-customers/customer-quotations/customer-quotations.component.scss ***!
  \******************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/deep/ .modal-dialog {\n  max-width: 22%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3ByYWthc2gvVmlwaW4vaXJvbmJpcmQvMjBGRUIyMDIwL2lyb25iaXJkLWNsaWVudHMvc3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9jdXN0b21lci9jdXN0b21lcnMvZGV0YWlsLWN1c3RvbWVycy9jdXN0b21lci1xdW90YXRpb25zL2N1c3RvbWVyLXF1b3RhdGlvbnMuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBU0U7RUFDRSxjQUFlLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9jdXN0b21lci9jdXN0b21lcnMvZGV0YWlsLWN1c3RvbWVycy9jdXN0b21lci1xdW90YXRpb25zL2N1c3RvbWVyLXF1b3RhdGlvbnMuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBuYi1jYXJkLWJvZHl7XG4gICAgLy8gYmFja2dyb3VuZC1jb2xvcjojRTFGNUZFO1xuICAgIC8vIGNvbG9yOndoaXRlO1xuICAvLyB9XG5cbiAgLy8gbmItY2FyZC1oZWFkZXJ7XG4gIC8vICAgYmFja2dyb3VuZDogIzFEOEVDRTtcbiAgLy8gICBjb2xvcjogd2hpdGU7XG4gIC8vIH1cbiAgL2RlZXAvIC5tb2RhbC1kaWFsb2cge1xuICAgIG1heC13aWR0aDogMjIlIDtcbiAgfSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/ui-features/customer/customers/detail-customers/customer-quotations/customer-quotations.component.ts":
/*!****************************************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/customer/customers/detail-customers/customer-quotations/customer-quotations.component.ts ***!
  \****************************************************************************************************************************/
/*! exports provided: CustomerQuotationsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerQuotationsComponent", function() { return CustomerQuotationsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var _services_customer_customer_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../services/customer/customer.service */ "./src/app/services/customer/customer.service.ts");
/* harmony import */ var _services_customer_site_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../../services/customer/site.service */ "./src/app/services/customer/site.service.ts");
/* harmony import */ var _services_admin_quotation_quotation_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../../../services/admin/quotation/quotation.service */ "./src/app/services/admin/quotation/quotation.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






var CustomerQuotationsComponent = /** @class */ (function () {
    function CustomerQuotationsComponent(router, CustomerServ, quotationServ, SiteServ) {
        this.router = router;
        this.CustomerServ = CustomerServ;
        this.quotationServ = quotationServ;
        this.SiteServ = SiteServ;
        this.customer = {};
        this.settings = {
            add: {
                addButtonContent: '<i class="nb-plus"></i>',
                createButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
            },
            edit: {
                editButtonContent: '<i class="nb-edit"></i>',
                saveButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
            },
            delete: {
                deleteButtonContent: '<i class="nb-trash"></i>',
                confirmDelete: true,
            },
            actions: {
                edit: false,
                add: false,
                delete: false,
                position: 'right'
            },
            columns: {
                quoteNo: {
                    title: 'Quotation No',
                    type: 'string',
                },
                qouteDate: {
                    title: 'Qoute Date',
                    type: 'string',
                    valuePrepareFunction: function (value) {
                        return value.split('-').reverse().join('/');
                    },
                },
                siteName: {
                    title: 'Site Name',
                    type: 'string',
                },
                TotalPrice: {
                    title: 'Total Amount',
                    type: 'string',
                },
            },
        };
        this.source = new ng2_smart_table__WEBPACK_IMPORTED_MODULE_2__["LocalDataSource"]();
        this.getCustomerDetail();
        this.getCustomerDetails();
    }
    CustomerQuotationsComponent.prototype.onDeleteConfirm = function (event) {
        if (window.confirm('Are you sure you want to delete?')) {
            event.confirm.resolve();
        }
        else {
            event.confirm.reject();
        }
    };
    CustomerQuotationsComponent.prototype.onUserRowSelect = function (event) {
        // console.log(event.data);
        var id = event.data.id;
        if (this.router.url === "/pages/customers") {
            this.router.navigate(["/pages/MainQuotation/" + id]); //for the case 'the user logout I want him to be redirected to home.'
        }
        else {
            // Stay here or do something
            // for the case 'However if he is on a safe page I don't want to redirected him.'
        }
    };
    CustomerQuotationsComponent.prototype.ngOnInit = function () {
        this.getCustomerDetail();
        // this.getCustomerDetails();
    };
    /**
       * Get Customer Detail from qutotation table
       */
    CustomerQuotationsComponent.prototype.getCustomerDetails = function () {
        var _this = this;
        this.CustomerServ.userCustomer
            .subscribe(function (data) {
            _this.customer = data;
            _this.id = data.id;
            // this.getSiteInfo();
            // this.editShow = data.creatShow;
        }, function (error) {
            console.log(error);
        });
    };
    /**
    * Get Customer Detail
    */
    CustomerQuotationsComponent.prototype.getCustomerDetail = function () {
        var _this = this;
        this.CustomerServ.userCustomer
            .subscribe(function (data) {
            _this.customer = data;
            _this.id = data.id;
            _this.getQuotationInfo(data.id);
            // console.log(data.id);
        }, function (error) {
            console.log(error);
        });
    };
    CustomerQuotationsComponent.prototype.getQuotationInfo = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.quotationServ.getQuotationByID(id)];
                    case 1:
                        result = _a.sent();
                        this.source.load(result);
                        return [2 /*return*/];
                }
            });
        });
    };
    CustomerQuotationsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-customer-quotations',
            template: __webpack_require__(/*! ./customer-quotations.component.html */ "./src/app/pages/ui-features/customer/customers/detail-customers/customer-quotations/customer-quotations.component.html"),
            styles: [__webpack_require__(/*! ./customer-quotations.component.scss */ "./src/app/pages/ui-features/customer/customers/detail-customers/customer-quotations/customer-quotations.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _services_customer_customer_service__WEBPACK_IMPORTED_MODULE_3__["CustomerService"],
            _services_admin_quotation_quotation_service__WEBPACK_IMPORTED_MODULE_5__["QuotationService"],
            _services_customer_site_service__WEBPACK_IMPORTED_MODULE_4__["SiteService"]])
    ], CustomerQuotationsComponent);
    return CustomerQuotationsComponent;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/customer/customers/detail-customers/detail-customers.component.html":
/*!*******************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/customer/customers/detail-customers/detail-customers.component.html ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nb-card>\n    <ngb-tabset>\n        <ngb-tab title=\"Details\" *ngIf='clientdetails'>\n            <ng-template ngbTabContent>\n                <ngx-customer-details></ngx-customer-details>\n            </ng-template>\n        </ngb-tab>\n        <ngb-tab title=\"Quotation\" *ngIf='clientquotation'>\n            <ng-template ngbTabContent>\n                <ngx-customer-quotations></ngx-customer-quotations>\n            </ng-template>\n        </ngb-tab>\n        <ngb-tab title=\"Agreements\" *ngIf='clientagreement'>\n            <ng-template ngbTabContent>\n                <ngx-customer-jobs></ngx-customer-jobs>\n            </ng-template>\n        </ngb-tab>\n        <ngb-tab title=\"Invoices\" *ngIf='clientinvoices'>\n            <ng-template ngbTabContent>\n                <div class=\"container\">\n                    <ngx-customer-invoices></ngx-customer-invoices>\n                </div>\n\n            </ng-template>\n        </ngb-tab>\n    </ngb-tabset>\n</nb-card>"

/***/ }),

/***/ "./src/app/pages/ui-features/customer/customers/detail-customers/detail-customers.component.scss":
/*!*******************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/customer/customers/detail-customers/detail-customers.component.scss ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "nb-card, ngb-tabset {\n  background: #E1F5FE; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3ByYWthc2gvVmlwaW4vaXJvbmJpcmQvMjBGRUIyMDIwL2lyb25iaXJkLWNsaWVudHMvc3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9jdXN0b21lci9jdXN0b21lcnMvZGV0YWlsLWN1c3RvbWVycy9kZXRhaWwtY3VzdG9tZXJzLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksbUJBQW1CLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9jdXN0b21lci9jdXN0b21lcnMvZGV0YWlsLWN1c3RvbWVycy9kZXRhaWwtY3VzdG9tZXJzLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsibmItY2FyZCwgbmdiLXRhYnNldHtcbiAgICBiYWNrZ3JvdW5kOiAjRTFGNUZFO1xuICAgIC8vIGNvbG9yOiB3aGl0ZTtcbiAgfVxuXG4vLyBuZ2ItdGFic2V0IG5nYi10YWIgdWwgbGkgYTphY3RpdmV7XG4vLyAgICAgYmFja2dyb3VuZC1jb2xvcjojMDI3N0JEICFpbXBvcnRhbnQ7XG5cbi8vIH0iXX0= */"

/***/ }),

/***/ "./src/app/pages/ui-features/customer/customers/detail-customers/detail-customers.component.ts":
/*!*****************************************************************************************************!*\
  !*** ./src/app/pages/ui-features/customer/customers/detail-customers/detail-customers.component.ts ***!
  \*****************************************************************************************************/
/*! exports provided: DetailCustomersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailCustomersComponent", function() { return DetailCustomersComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_token_token_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../services/token/token.service */ "./src/app/services/token/token.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DetailCustomersComponent = /** @class */ (function () {
    function DetailCustomersComponent(token) {
        this.token = token;
        this.clientagreement = false;
        this.clientdetails = false;
        this.clientinvoices = false;
        this.clientquotation = false;
    }
    DetailCustomersComponent.prototype.ngOnInit = function () {
        this.tokens = this.token.getDecodedToken();
        this.clientdetails = this.tokens.rolesHideAndShow.clientdetails;
        this.clientagreement = this.tokens.rolesHideAndShow.clientagreement;
        this.clientinvoices = this.tokens.rolesHideAndShow.clientinvoices;
        this.clientquotation = this.tokens.rolesHideAndShow.clientquotation;
    };
    DetailCustomersComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-detail-customers',
            template: __webpack_require__(/*! ./detail-customers.component.html */ "./src/app/pages/ui-features/customer/customers/detail-customers/detail-customers.component.html"),
            styles: [__webpack_require__(/*! ./detail-customers.component.scss */ "./src/app/pages/ui-features/customer/customers/detail-customers/detail-customers.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_token_token_service__WEBPACK_IMPORTED_MODULE_1__["TokenService"]])
    ], DetailCustomersComponent);
    return DetailCustomersComponent;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/customer/customers/form-can-deactivate.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/ui-features/customer/customers/form-can-deactivate.ts ***!
  \*****************************************************************************/
/*! exports provided: FormCanDeactivate */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormCanDeactivate", function() { return FormCanDeactivate; });
/* harmony import */ var _shared_component_can_deactivate__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../shared/component-can-deactivate */ "./src/app/shared/component-can-deactivate.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

var FormCanDeactivate = /** @class */ (function (_super) {
    __extends(FormCanDeactivate, _super);
    function FormCanDeactivate() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    FormCanDeactivate.prototype.canDeactivate = function () {
        return this.form.submitted || !this.form.dirty;
    };
    return FormCanDeactivate;
}(_shared_component_can_deactivate__WEBPACK_IMPORTED_MODULE_0__["ComponentCanDeactivate"]));



/***/ }),

/***/ "./src/app/pages/ui-features/customer/customers/new-customer/new-customer.component.html":
/*!***********************************************************************************************!*\
  !*** ./src/app/pages/ui-features/customer/customers/new-customer/new-customer.component.html ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nb-card [hidden]=\"formShow\">\n    <nb-card-header class=\"text-center\">\n        Client Details\n\n    </nb-card-header>\n    <nb-card-body>\n        <form #customerFortm=\"ngForm\" name=\"form\" (ngSubmit)=\"customerFortm.form.valid && createNewCustomer(customerFortm)\" novalidate>\n\n            <div>\n\n                <div class=\"row\">\n\n                    <div class=\"col-6\">\n                        <div class=\"form-group input-group-sm\">\n                            <label for=\"Maintenance\"> Client Name</label>\n                            <input type=\"text\" class=\"form-control\" [(ngModel)]=\"customer.companyName\" name=\"companyName\" placeholder=\"Client Name\" #companyName=\"ngModel\" [ngClass]=\"{ 'is-invalid': customerFortm.submitted && companyName.invalid }\" required>\n                            <div *ngIf=\"customerFortm.submitted && companyName.invalid\" class=\"invalid-feedback\">\n                                <div *ngIf=\"companyName.errors.required\">Client Name is required</div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"col-6\">\n                        <div class=\"form-group input-group-sm\">\n                            <label for=\"Maintenance\"> Contact Person Name</label>\n                            <input type=\"text\" class=\"form-control\" [(ngModel)]=\"customer.personName\" name=\"personName\" placeholder=\"Contact Person Name\" #personName=\"ngModel\" [ngClass]=\"{ 'is-invalid': customerFortm.submitted && personName.invalid }\" required>\n                            <div *ngIf=\"customerFortm.submitted && personName.invalid\" class=\"invalid-feedback\">\n                                <div *ngIf=\"personName.errors.required\">Contact Person Name is required</div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"row\">\n                    <div class=\"col-6 \">\n                        <label for=\"Maintenance\">Conisignee Address</label>\n                        <textarea class=\"form-control\" type=\"text\" [(ngModel)]=\"customer.address\" name=\"address\" placeholder=\"Conisignee Address\" #address=\"ngModel\" [ngClass]=\"{ 'is-invalid': customerFortm.submitted && address.invalid }\" required></textarea>\n                        <div *ngIf=\"customerFortm.submitted && address.invalid\" class=\"invalid-feedback\">\n                            <div *ngIf=\"address.errors.required\">Conisignee Address is required</div>\n                        </div>\n                    </div>\n                    <div class=\"col-6 \">\n                        <label for=\"Maintenance\">Billing Address</label>\n                        <textarea class=\"form-control\" type=\"text\" [(ngModel)]=\"customer.deliveryAddress\" name=\"deliveryAddress\" placeholder=\"Delivery Address\" #deliveryAddress=\"ngModel\" [ngClass]=\"{ 'is-invalid': customerFortm.submitted && deliveryAddress.invalid }\" required></textarea>\n                        <div *ngIf=\"customerFortm.submitted && deliveryAddress.invalid\" class=\"invalid-feedback\">\n                            <div *ngIf=\"deliveryAddress.errors.required\">Billing Address is required</div>\n                        </div>\n                    </div>\n                </div><br>\n                <div class=\"row\">\n                    <div class=\"col-3\">\n                        <div class=\"form-group input-group-sm\">\n                            <label for=\"Maintenance\">Contact No</label>\n                            <input type=\"number\" class=\"form-control\" [(ngModel)]=\"customer.mobile\" name=\"mobile\" placeholder=\"Contact No\" #mobile=\"ngModel\" [ngClass]=\"{ 'is-invalid': customerFortm.submitted && mobile.invalid }\" required>\n                            <div *ngIf=\"customerFortm.submitted && mobile.invalid\" class=\"invalid-feedback\">\n                                <div *ngIf=\"mobile.errors.required\">Contact No is required</div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"col-3\">\n                        <div class=\"form-group input-group-sm\">\n                            <label for=\"Date\"> Email id</label>\n                            <input type=\"email\" class=\"form-control\" [(ngModel)]=\"customer.email\" name=\"email\" placeholder=\"Email id\">\n\n                        </div>\n                    </div>\n                    <div class=\"col-3\">\n                        <div class=\"form-group input-group-sm\">\n                            <label for=\"Delivary\">GST Number</label>\n                            <input type=\"text\" class=\"form-control\" [(ngModel)]=\"customer.gst\" name=\"gst\" placeholder=\"GST\" #gst=\"ngModel\">\n                        </div>\n                    </div>\n                    <div class=\"col-3\">\n                        <div class=\"form-group input-group-sm\">\n                            <label for=\"Delivary\">PAN Number</label>\n                            <input type=\"text\" class=\"form-control\" [(ngModel)]=\"customer.pan\" name=\"pan\" placeholder=\"PAN\" #pan=\"ngModel\">\n                        </div>\n                    </div>\n                </div>\n                <div class=\"container-fluid\">\n                    <div class=\"row foot\">\n                        <div class=\"col\">\n                            <button class=\"btn btn-outline-primary\" type=\"submit\" shape=\"semi-round\" style=\"float:right;margin-top: 3px;\">Save</button>\n                        </div>\n                    </div>\n\n                </div>\n            </div>\n            <br>\n\n\n        </form>\n    </nb-card-body>\n</nb-card>"

/***/ }),

/***/ "./src/app/pages/ui-features/customer/customers/new-customer/new-customer.component.scss":
/*!***********************************************************************************************!*\
  !*** ./src/app/pages/ui-features/customer/customers/new-customer/new-customer.component.scss ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".hi {\n  border: 2px solid; }\n\ntable {\n  width: -webkit-fill-available; }\n\n.btn {\n  line-height: 12px !important;\n  padding: 0.55rem 1.0rem !important; }\n\na {\n  cursor: pointer; }\n\ntextarea {\n  border: none; }\n\nnb-card-body {\n  background-color: #e1f5fe; }\n\nnb-card-header {\n  background: #1d8ece;\n  color: white; }\n\n/deep/ .modal-dialog {\n  max-width: 22%; }\n\ninput[type=\"number\"]::-webkit-inner-spin-button,\ninput[type=\"number\"]::-webkit-outer-spin-button {\n  -webkit-appearance: none;\n  margin: 0; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3ByYWthc2gvVmlwaW4vaXJvbmJpcmQvMjBGRUIyMDIwL2lyb25iaXJkLWNsaWVudHMvc3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9jdXN0b21lci9jdXN0b21lcnMvbmV3LWN1c3RvbWVyL25ldy1jdXN0b21lci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGlCQUFpQixFQUFBOztBQUduQjtFQUNFLDZCQUE2QixFQUFBOztBQUcvQjtFQUNFLDRCQUE0QjtFQUM1QixrQ0FBa0MsRUFBQTs7QUFHcEM7RUFDRSxlQUFlLEVBQUE7O0FBR2pCO0VBQ0UsWUFBWSxFQUFBOztBQUdkO0VBQ0UseUJBQXlCLEVBQUE7O0FBSTNCO0VBQ0UsbUJBQW1CO0VBQ25CLFlBQVksRUFBQTs7QUFHZDtFQUNFLGNBQWMsRUFBQTs7QUFHaEI7O0VBRUUsd0JBQXdCO0VBQ3hCLFNBQVMsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VpLWZlYXR1cmVzL2N1c3RvbWVyL2N1c3RvbWVycy9uZXctY3VzdG9tZXIvbmV3LWN1c3RvbWVyLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmhpIHtcbiAgYm9yZGVyOiAycHggc29saWQ7XG59XG5cbnRhYmxlIHtcbiAgd2lkdGg6IC13ZWJraXQtZmlsbC1hdmFpbGFibGU7XG59XG5cbi5idG4ge1xuICBsaW5lLWhlaWdodDogMTJweCAhaW1wb3J0YW50O1xuICBwYWRkaW5nOiAwLjU1cmVtIDEuMHJlbSAhaW1wb3J0YW50O1xufVxuXG5hIHtcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuXG50ZXh0YXJlYSB7XG4gIGJvcmRlcjogbm9uZTtcbn1cblxubmItY2FyZC1ib2R5IHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2UxZjVmZTtcbiAgLy8gY29sb3I6d2hpdGU7XG59XG5cbm5iLWNhcmQtaGVhZGVyIHtcbiAgYmFja2dyb3VuZDogIzFkOGVjZTtcbiAgY29sb3I6IHdoaXRlO1xufVxuXG4vZGVlcC8gLm1vZGFsLWRpYWxvZyB7XG4gIG1heC13aWR0aDogMjIlO1xufVxuXG5pbnB1dFt0eXBlPVwibnVtYmVyXCJdOjotd2Via2l0LWlubmVyLXNwaW4tYnV0dG9uLFxuaW5wdXRbdHlwZT1cIm51bWJlclwiXTo6LXdlYmtpdC1vdXRlci1zcGluLWJ1dHRvbiB7XG4gIC13ZWJraXQtYXBwZWFyYW5jZTogbm9uZTtcbiAgbWFyZ2luOiAwO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/ui-features/customer/customers/new-customer/new-customer.component.ts":
/*!*********************************************************************************************!*\
  !*** ./src/app/pages/ui-features/customer/customers/new-customer/new-customer.component.ts ***!
  \*********************************************************************************************/
/*! exports provided: NewCustomerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewCustomerComponent", function() { return NewCustomerComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var _form_can_deactivate__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../form-can-deactivate */ "./src/app/pages/ui-features/customer/customers/form-can-deactivate.ts");
/* harmony import */ var _confirmation_dialog_confirmation_dialog_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../confirmation-dialog/confirmation-dialog.service */ "./src/app/confirmation-dialog/confirmation-dialog.service.ts");
/* harmony import */ var _services_customer_customer_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../../services/customer/customer.service */ "./src/app/services/customer/customer.service.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






var NewCustomerComponent = /** @class */ (function (_super) {
    __extends(NewCustomerComponent, _super);
    function NewCustomerComponent(CustomerServ, confirmationDialogService) {
        var _this = _super.call(this) || this;
        _this.CustomerServ = CustomerServ;
        _this.confirmationDialogService = confirmationDialogService;
        _this.customer = {};
        _this.formShow = false;
        _this.customerChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        _this.source = new ng2_smart_table__WEBPACK_IMPORTED_MODULE_2__["LocalDataSource"]();
        _this.getCustomerDetail();
        return _this;
    }
    /**
     * Get Customer Detail
     */
    NewCustomerComponent.prototype.getCustomerDetail = function () {
        var _this = this;
        this.CustomerServ.userCustomer
            .subscribe(function (data) {
            _this.customer = data;
            _this.formShow = false;
            // this.editShow = data.creatShow;
        }, function (error) {
            console.log(error);
        });
    };
    /**
    *  Create  Customer
    */
    NewCustomerComponent.prototype.createNewCustomer = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            var resultAleart, value, result, getData, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.confirmationDialogService.confirm('Alert Message', 'Are you sure you want to create?')];
                    case 1:
                        resultAleart = _a.sent();
                        if (!resultAleart) return [3 /*break*/, 6];
                        value = data.value;
                        _a.label = 2;
                    case 2:
                        _a.trys.push([2, 4, , 5]);
                        return [4 /*yield*/, this.CustomerServ.createCustomer(value)];
                    case 3:
                        result = _a.sent();
                        this.customerChange.emit(result);
                        result.creatShow = false;
                        this.CustomerServ.userCustomer.next(result);
                        getData = Object.assign({}, result, value);
                        this.CustomerServ.userCustomer1.next(getData);
                        this.formShow = true;
                        return [3 /*break*/, 5];
                    case 4:
                        error_1 = _a.sent();
                        console.log(error_1);
                        return [3 /*break*/, 5];
                    case 5:
                        data.reset();
                        return [3 /*break*/, 7];
                    case 6:
                        data.confirm.reject();
                        _a.label = 7;
                    case 7: return [2 /*return*/];
                }
            });
        });
    };
    /**
     *  Create,Delet And Edit Customer Site Details
     */
    NewCustomerComponent.prototype.onDeleteConfirm = function (event) {
        if (window.confirm('Are you sure you want to delete?')) {
            event.confirm.resolve();
        }
        else {
            event.confirm.reject();
        }
    };
    NewCustomerComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], NewCustomerComponent.prototype, "customerChange", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('form'),
        __metadata("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgForm"])
    ], NewCustomerComponent.prototype, "form", void 0);
    NewCustomerComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-new-customer',
            template: __webpack_require__(/*! ./new-customer.component.html */ "./src/app/pages/ui-features/customer/customers/new-customer/new-customer.component.html"),
            styles: [__webpack_require__(/*! ./new-customer.component.scss */ "./src/app/pages/ui-features/customer/customers/new-customer/new-customer.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_customer_customer_service__WEBPACK_IMPORTED_MODULE_5__["CustomerService"],
            _confirmation_dialog_confirmation_dialog_service__WEBPACK_IMPORTED_MODULE_4__["ConfirmationDialogService"]])
    ], NewCustomerComponent);
    return NewCustomerComponent;
}(_form_can_deactivate__WEBPACK_IMPORTED_MODULE_3__["FormCanDeactivate"]));



/***/ }),

/***/ "./src/app/services/customer/site.service.ts":
/*!***************************************************!*\
  !*** ./src/app/services/customer/site.service.ts ***!
  \***************************************************/
/*! exports provided: SiteService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SiteService", function() { return SiteService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _settings_settings_services_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../settings/settings-services.service */ "./src/app/services/settings/settings-services.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SiteService = /** @class */ (function () {
    function SiteService(http, settings) {
        this.http = http;
        this.settings = settings;
        this.userSite = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]({});
        this.API_URL = this.settings.BASE_API_URL;
    }
    SiteService.prototype.createSite = function (data) {
        var _this = this;
        var url = this.API_URL + "/customer/site-details";
        return new Promise(function (resolve, reject) {
            _this.http.post(url, data)
                .subscribe(resolve, reject);
        });
    };
    SiteService.prototype.getSite = function (id) {
        var _this = this;
        var url = this.API_URL + "/customer/site-details/" + id;
        return new Promise(function (resolve, reject) {
            _this.http.get(url)
                .subscribe(resolve, reject);
        });
    };
    SiteService.prototype.UpdateSiteDetails = function (value, id) {
        var _this = this;
        var url = this.API_URL + "/customer/site-details/" + id;
        return new Promise(function (resolve, reject) {
            _this.http.put(url, value)
                .subscribe(resolve, reject);
        });
    };
    SiteService.prototype.getSiteByID = function (id) {
        var _this = this;
        var url = this.API_URL + "/customer/site-details/" + id;
        return new Promise(function (resolve, reject) {
            _this.http.get(url)
                .subscribe(resolve, reject);
        });
    };
    SiteService.prototype.removeSite = function (value) {
        var _this = this;
        var id = value.id;
        var url = this.API_URL + "/customer/site-details/" + id;
        return new Promise(function (resolve, reject) {
            _this.http.delete(url)
                .subscribe(resolve, reject);
        });
    };
    SiteService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _settings_settings_services_service__WEBPACK_IMPORTED_MODULE_3__["SettingsServicesService"]])
    ], SiteService);
    return SiteService;
}());



/***/ }),

/***/ "./src/app/shared/can-deactivate.guard.ts":
/*!************************************************!*\
  !*** ./src/app/shared/can-deactivate.guard.ts ***!
  \************************************************/
/*! exports provided: CanDeactivateGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CanDeactivateGuard", function() { return CanDeactivateGuard; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var CanDeactivateGuard = /** @class */ (function () {
    function CanDeactivateGuard() {
    }
    CanDeactivateGuard.prototype.canDeactivate = function (component) {
        if (!component.canDeactivate()) {
            if (confirm("You have unsaved changes! If you leave, your changes will be lost.")) {
                return true;
            }
            else {
                return false;
            }
        }
        return true;
    };
    CanDeactivateGuard = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])()
    ], CanDeactivateGuard);
    return CanDeactivateGuard;
}());



/***/ }),

/***/ "./src/app/shared/component-can-deactivate.ts":
/*!****************************************************!*\
  !*** ./src/app/shared/component-can-deactivate.ts ***!
  \****************************************************/
/*! exports provided: ComponentCanDeactivate */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ComponentCanDeactivate", function() { return ComponentCanDeactivate; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ComponentCanDeactivate = /** @class */ (function () {
    function ComponentCanDeactivate() {
    }
    ComponentCanDeactivate.prototype.unloadNotification = function ($event) {
        if (!this.canDeactivate()) {
            $event.returnValue = true;
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('window:beforeunload', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], ComponentCanDeactivate.prototype, "unloadNotification", null);
    return ComponentCanDeactivate;
}());



/***/ })

}]);
//# sourceMappingURL=default~app-pages-pages-module~customer-customer-module~ui-features-ui-features-module.js.map