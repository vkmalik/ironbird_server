(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["amc-amc-module"],{

/***/ "./src/app/services/quotations/site/site.service.ts":
/*!**********************************************************!*\
  !*** ./src/app/services/quotations/site/site.service.ts ***!
  \**********************************************************/
/*! exports provided: SiteService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SiteService", function() { return SiteService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _settings_settings_services_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../settings/settings-services.service */ "./src/app/services/settings/settings-services.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SiteService = /** @class */ (function () {
    function SiteService(http, settings) {
        this.http = http;
        this.settings = settings;
        this.API_URL = this.settings.BASE_API_URL;
    }
    SiteService.prototype.getSiteDetails = function (id) {
        var _this = this;
        var url = this.API_URL + "/customer/site-details/" + id;
        return new Promise(function (resolve, reject) {
            _this.http.get(url)
                .subscribe(resolve, reject);
        });
    };
    SiteService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _settings_settings_services_service__WEBPACK_IMPORTED_MODULE_2__["SettingsServicesService"]])
    ], SiteService);
    return SiteService;
}());



/***/ })

}]);
//# sourceMappingURL=amc-amc-module.js.map