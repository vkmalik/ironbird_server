(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~app-pages-pages-module~ui-features-ui-features-module"],{

/***/ "./src/app/pages/ui-features/accounts/accounts.component.html":
/*!********************************************************************!*\
  !*** ./src/app/pages/ui-features/accounts/accounts.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nb-card>\n    <nb-card-header>\n        Hello\n    </nb-card-header>\n    <nb-card-body>\n        yo\n    </nb-card-body>\n</nb-card>"

/***/ }),

/***/ "./src/app/pages/ui-features/accounts/accounts.component.scss":
/*!********************************************************************!*\
  !*** ./src/app/pages/ui-features/accounts/accounts.component.scss ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VpLWZlYXR1cmVzL2FjY291bnRzL2FjY291bnRzLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/pages/ui-features/accounts/accounts.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/pages/ui-features/accounts/accounts.component.ts ***!
  \******************************************************************/
/*! exports provided: AccountsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountsComponent", function() { return AccountsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AccountsComponent = /** @class */ (function () {
    function AccountsComponent() {
    }
    AccountsComponent.prototype.ngOnInit = function () {
    };
    AccountsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-accounts',
            template: __webpack_require__(/*! ./accounts.component.html */ "./src/app/pages/ui-features/accounts/accounts.component.html"),
            styles: [__webpack_require__(/*! ./accounts.component.scss */ "./src/app/pages/ui-features/accounts/accounts.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], AccountsComponent);
    return AccountsComponent;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/accounts/accounts.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/ui-features/accounts/accounts.module.ts ***!
  \***************************************************************/
/*! exports provided: AccountsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountsModule", function() { return AccountsModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var _nebular_theme__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @nebular/theme */ "./node_modules/@nebular/theme/index.js");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var _accounts_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./accounts.component */ "./src/app/pages/ui-features/accounts/accounts.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var routes = [{
        path: '',
        component: _accounts_component__WEBPACK_IMPORTED_MODULE_6__["AccountsComponent"],
        children: [
            {
                path: 'accounts',
                component: _accounts_component__WEBPACK_IMPORTED_MODULE_6__["AccountsComponent"],
            }
        ]
    }];
var AccountsModule = /** @class */ (function () {
    function AccountsModule() {
    }
    AccountsModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_accounts_component__WEBPACK_IMPORTED_MODULE_6__["AccountsComponent"]],
            imports: [
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModule"],
                _nebular_theme__WEBPACK_IMPORTED_MODULE_4__["NbCardModule"],
                ng2_smart_table__WEBPACK_IMPORTED_MODULE_5__["Ng2SmartTableModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes),
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"]
            ]
        })
    ], AccountsModule);
    return AccountsModule;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/search-fields/search-fields.component.html":
/*!******************************************************************************!*\
  !*** ./src/app/pages/ui-features/search-fields/search-fields.component.html ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n  <div class=\"col-md-6\">\n    <nb-card>\n      <nb-card-header>\n        Layout Rotate Search\n      </nb-card-header>\n      <nb-card-body>\n        <nb-search type=\"rotate-layout\" tag=\"rotate-layout\"></nb-search>\n      </nb-card-body>\n    </nb-card>\n  </div>\n  <div class=\"col-md-6\">\n    <nb-card>\n      <nb-card-header>\n        Modal Zoomin Search\n      </nb-card-header>\n      <nb-card-body>\n        <nb-search type=\"modal-zoomin\" tag=\"modal-zoomin\"></nb-search>\n      </nb-card-body>\n    </nb-card>\n  </div>\n  <div class=\"col-md-6\">\n    <nb-card>\n      <nb-card-header>\n        Modal Move Search\n      </nb-card-header>\n      <nb-card-body>\n        <nb-search type=\"modal-move\" tag=\"modal-move\"></nb-search>\n      </nb-card-body>\n    </nb-card>\n  </div>\n  <div class=\"col-md-6\">\n    <nb-card>\n      <nb-card-header>\n        Modal Drop Search\n      </nb-card-header>\n      <nb-card-body>\n        <nb-search type=\"modal-drop\" tag=\"modal-drop\"></nb-search>\n      </nb-card-body>\n    </nb-card>\n  </div>\n  <div class=\"col-md-6\">\n    <nb-card>\n      <nb-card-header>\n        Modal Half Search\n      </nb-card-header>\n      <nb-card-body>\n        <nb-search type=\"modal-half\" tag=\"modal-half\"></nb-search>\n      </nb-card-body>\n    </nb-card>\n  </div>\n  <div class=\"col-md-6\">\n    <nb-card>\n      <nb-card-header>\n        Curtain Search\n      </nb-card-header>\n      <nb-card-body>\n        <nb-search type=\"curtain\" tag=\"curtain\"></nb-search>\n      </nb-card-body>\n    </nb-card>\n  </div>\n  <div class=\"col-md-6\">\n    <nb-card>\n      <nb-card-header>\n        Column Curtain Search\n      </nb-card-header>\n      <nb-card-body>\n        <nb-search type=\"column-curtain\" tag=\"column-curtain\"></nb-search>\n      </nb-card-body>\n    </nb-card>\n  </div>\n</div>\n\n"

/***/ }),

/***/ "./src/app/pages/ui-features/search-fields/search-fields.component.ts":
/*!****************************************************************************!*\
  !*** ./src/app/pages/ui-features/search-fields/search-fields.component.ts ***!
  \****************************************************************************/
/*! exports provided: SearchComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchComponent", function() { return SearchComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var SearchComponent = /** @class */ (function () {
    function SearchComponent() {
    }
    SearchComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-search-fields',
            template: __webpack_require__(/*! ./search-fields.component.html */ "./src/app/pages/ui-features/search-fields/search-fields.component.html"),
        })
    ], SearchComponent);
    return SearchComponent;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/ui-features-routing.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/pages/ui-features/ui-features-routing.module.ts ***!
  \*****************************************************************/
/*! exports provided: UiFeaturesRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UiFeaturesRoutingModule", function() { return UiFeaturesRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ui_features_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ui-features.component */ "./src/app/pages/ui-features/ui-features.component.ts");
/* harmony import */ var _search_fields_search_fields_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./search-fields/search-fields.component */ "./src/app/pages/ui-features/search-fields/search-fields.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var routes = [{
        path: '',
        component: _ui_features_component__WEBPACK_IMPORTED_MODULE_2__["UiFeaturesComponent"],
        children: [
            {
                path: 'admin',
                loadChildren: './admin/admin.module#AdminModule',
            },
            {
                path: 'Jobs',
                loadChildren: './jobs/jobs.module#JobsModule',
            },
            {
                path: 'quotation',
                loadChildren: './quotation/quotation.module#QuotationModule',
            },
            {
                path: 'amc',
                loadChildren: './amc/amc.module#AMCModule',
            },
            {
                path: 'inventory',
                loadChildren: './inventory/inventory.module#InventoryModule',
            },
            {
                path: 'customer',
                loadChildren: './customer/customer.module#CustomerModule',
            },
            {
                path: 'suppliers',
                loadChildren: './suppliers/suppliers.module#SuppliersModule',
            },
            {
                path: 'hr-payroll',
                loadChildren: './hr-payroll/hr-payroll.module#HrPayrollModule',
            },
            {
                path: 'search-fields',
                component: _search_fields_search_fields_component__WEBPACK_IMPORTED_MODULE_3__["SearchComponent"],
            },
            {
                path: 'Marketing',
                loadChildren: './marketing-leads/marketing-leads.module#MarketingLeadsModule',
            },
            {
                path: 'Invoice',
                loadChildren: './invoices/invoices.module#InvoicesModule',
            },
            {
                path: 'Notification',
                loadChildren: './notification/notification.module#NotificationModule',
            },
            {
                path: 'accounts',
                loadChildren: './accounts/accounts.module#AccountsModule',
            },
        ],
    }];
var UiFeaturesRoutingModule = /** @class */ (function () {
    function UiFeaturesRoutingModule() {
    }
    UiFeaturesRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
        })
    ], UiFeaturesRoutingModule);
    return UiFeaturesRoutingModule;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/ui-features.component.ts":
/*!************************************************************!*\
  !*** ./src/app/pages/ui-features/ui-features.component.ts ***!
  \************************************************************/
/*! exports provided: UiFeaturesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UiFeaturesComponent", function() { return UiFeaturesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var UiFeaturesComponent = /** @class */ (function () {
    function UiFeaturesComponent() {
    }
    UiFeaturesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-ui-features',
            template: "\n    <router-outlet></router-outlet>\n  ",
        })
    ], UiFeaturesComponent);
    return UiFeaturesComponent;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/ui-features.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/ui-features/ui-features.module.ts ***!
  \*********************************************************/
/*! exports provided: UiFeaturesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UiFeaturesModule", function() { return UiFeaturesModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _theme_theme_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../@theme/theme.module */ "./src/app/@theme/theme.module.ts");
/* harmony import */ var _ui_features_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ui-features-routing.module */ "./src/app/pages/ui-features/ui-features-routing.module.ts");
/* harmony import */ var _ui_features_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ui-features.component */ "./src/app/pages/ui-features/ui-features.component.ts");
/* harmony import */ var _admin_admin_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./admin/admin.module */ "./src/app/pages/ui-features/admin/admin.module.ts");
/* harmony import */ var _amc_amc_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./amc/amc.module */ "./src/app/pages/ui-features/amc/amc.module.ts");
/* harmony import */ var _customer_customer_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./customer/customer.module */ "./src/app/pages/ui-features/customer/customer.module.ts");
/* harmony import */ var _hr_payroll_hr_payroll_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./hr-payroll/hr-payroll.module */ "./src/app/pages/ui-features/hr-payroll/hr-payroll.module.ts");
/* harmony import */ var _inventory_inventory_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./inventory/inventory.module */ "./src/app/pages/ui-features/inventory/inventory.module.ts");
/* harmony import */ var _invoices_invoices_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./invoices/invoices.module */ "./src/app/pages/ui-features/invoices/invoices.module.ts");
/* harmony import */ var _jobs_jobs_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./jobs/jobs.module */ "./src/app/pages/ui-features/jobs/jobs.module.ts");
/* harmony import */ var _marketing_leads_marketing_leads_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./marketing-leads/marketing-leads.module */ "./src/app/pages/ui-features/marketing-leads/marketing-leads.module.ts");
/* harmony import */ var _notification_notification_module__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./notification/notification.module */ "./src/app/pages/ui-features/notification/notification.module.ts");
/* harmony import */ var _quotation_quotation_module__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./quotation/quotation.module */ "./src/app/pages/ui-features/quotation/quotation.module.ts");
/* harmony import */ var _search_fields_search_fields_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./search-fields/search-fields.component */ "./src/app/pages/ui-features/search-fields/search-fields.component.ts");
/* harmony import */ var _suppliers_suppliers_module__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./suppliers/suppliers.module */ "./src/app/pages/ui-features/suppliers/suppliers.module.ts");
/* harmony import */ var _accounts_accounts_module__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./accounts/accounts.module */ "./src/app/pages/ui-features/accounts/accounts.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

















var components = [
    _ui_features_component__WEBPACK_IMPORTED_MODULE_3__["UiFeaturesComponent"],
    _search_fields_search_fields_component__WEBPACK_IMPORTED_MODULE_14__["SearchComponent"],
];
var UiFeaturesModule = /** @class */ (function () {
    function UiFeaturesModule() {
    }
    UiFeaturesModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _theme_theme_module__WEBPACK_IMPORTED_MODULE_1__["ThemeModule"],
                _ui_features_routing_module__WEBPACK_IMPORTED_MODULE_2__["UiFeaturesRoutingModule"],
                _jobs_jobs_module__WEBPACK_IMPORTED_MODULE_10__["JobsModule"],
                _admin_admin_module__WEBPACK_IMPORTED_MODULE_4__["AdminModule"],
                _quotation_quotation_module__WEBPACK_IMPORTED_MODULE_13__["QuotationModule"],
                _amc_amc_module__WEBPACK_IMPORTED_MODULE_5__["AMCModule"],
                _inventory_inventory_module__WEBPACK_IMPORTED_MODULE_8__["InventoryModule"],
                _customer_customer_module__WEBPACK_IMPORTED_MODULE_6__["CustomerModule"],
                _suppliers_suppliers_module__WEBPACK_IMPORTED_MODULE_15__["SuppliersModule"],
                _hr_payroll_hr_payroll_module__WEBPACK_IMPORTED_MODULE_7__["HrPayrollModule"],
                _marketing_leads_marketing_leads_module__WEBPACK_IMPORTED_MODULE_11__["MarketingLeadsModule"],
                _invoices_invoices_module__WEBPACK_IMPORTED_MODULE_9__["InvoicesModule"],
                _notification_notification_module__WEBPACK_IMPORTED_MODULE_12__["NotificationModule"],
                _accounts_accounts_module__WEBPACK_IMPORTED_MODULE_16__["AccountsModule"]
            ],
            declarations: components.slice(),
            entryComponents: [
                _ui_features_component__WEBPACK_IMPORTED_MODULE_3__["UiFeaturesComponent"]
            ],
            exports: [
                _jobs_jobs_module__WEBPACK_IMPORTED_MODULE_10__["JobsModule"],
                _admin_admin_module__WEBPACK_IMPORTED_MODULE_4__["AdminModule"],
                _quotation_quotation_module__WEBPACK_IMPORTED_MODULE_13__["QuotationModule"],
                _amc_amc_module__WEBPACK_IMPORTED_MODULE_5__["AMCModule"],
                _inventory_inventory_module__WEBPACK_IMPORTED_MODULE_8__["InventoryModule"],
                _customer_customer_module__WEBPACK_IMPORTED_MODULE_6__["CustomerModule"],
                _suppliers_suppliers_module__WEBPACK_IMPORTED_MODULE_15__["SuppliersModule"],
                _hr_payroll_hr_payroll_module__WEBPACK_IMPORTED_MODULE_7__["HrPayrollModule"],
                _marketing_leads_marketing_leads_module__WEBPACK_IMPORTED_MODULE_11__["MarketingLeadsModule"],
                _invoices_invoices_module__WEBPACK_IMPORTED_MODULE_9__["InvoicesModule"],
                _notification_notification_module__WEBPACK_IMPORTED_MODULE_12__["NotificationModule"],
                _accounts_accounts_module__WEBPACK_IMPORTED_MODULE_16__["AccountsModule"]
            ]
        })
    ], UiFeaturesModule);
    return UiFeaturesModule;
}());



/***/ })

}]);
//# sourceMappingURL=default~app-pages-pages-module~ui-features-ui-features-module.js.map