(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~app-pages-pages-module~invoices-invoices-module~ui-features-ui-features-module"],{

/***/ "./src/app/pages/ui-features/invoices/invoice-list/invoice-list.component.html":
/*!*************************************************************************************!*\
  !*** ./src/app/pages/ui-features/invoices/invoice-list/invoice-list.component.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n    <div class=\"col-xl-4\">\n        <nb-card>\n            <nb-card-header>\n                Invoices List\n\n                <select class=\"form-group\" (change)=\"selectcategory(displayType)\" name=\"displayType\" style=\"width: 31%;float: right;\" [(ngModel)]='displayType'>\n                    <!-- <option value=\"\" disabled>Select Year</option> -->\n                    <option *ngFor=\"let category of catagorys\" [value]=\"category\">{{category}}</option>\n                 </select>\n\n                <!-- <input list=\"browsers\" type=\"text\" name=\"category\" class=\"form-control\" (change)=\"selectcategory(category)\" [(ngModel)]='category' placeholder=\"Select Category\" style=\"float: right;width: 50%;\">\n\n                <datalist id=\"browsers\">\n                        <option *ngFor=\"let category of catagorys;let i = index\" [value]=\"category\">{{category}}</option>\n                </datalist> -->\n            </nb-card-header>\n            <nb-card-body>\n                <ng2-smart-table [settings]=\"settings\" [source]=\"source\" (userRowSelect)=\"onUserRowSelect($event)\"></ng2-smart-table>\n            </nb-card-body>\n        </nb-card>\n    </div>\n    <div class=\"col-xl-8\" *ngIf='display' style=\"height: 100vh !important;\">\n        <nb-card>\n            <nb-card-header style=\"display: flex;\">\n                <label>Customer Name: {{customerName}}</label>\n                <label style=\"margin-left: 5%;\">Stage: {{stage}}</label>\n            </nb-card-header>\n        </nb-card>\n        <ngx-extended-pdf-viewer [src]=\"filePath\" useBrowserLocale=\"true\" [showPrintButton]=\"false\" [showOpenFileButton]='false' height='90vh' [showBookmarkButton]='false'>\n        </ngx-extended-pdf-viewer>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/pages/ui-features/invoices/invoice-list/invoice-list.component.scss":
/*!*************************************************************************************!*\
  !*** ./src/app/pages/ui-features/invoices/invoice-list/invoice-list.component.scss ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "nb-card-body {\n  background-color: #E1F5FE;\n  color: white; }\n\ncol-xl-6,\nnb-card-header {\n  background: #1D8ECE;\n  color: white; }\n\nnb-card {\n  background-color: #E1F5FE; }\n\n:host /deep/ table tbody tr:nth-child(even) {\n  background: #e9eaea !important; }\n\n:host /deep/ table tbody tr:nth-child(odd) {\n  background: white !important; }\n\n:host /deep/ ng2-smart-table .ng2-smart-pagination {\n  background-color: black; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3ByYWthc2gvVmlwaW4vaXJvbmJpcmQvMjBGRUIyMDIwL2lyb25iaXJkLWNsaWVudHMvc3JjL2FwcC9wYWdlcy91aS1mZWF0dXJlcy9pbnZvaWNlcy9pbnZvaWNlLWxpc3QvaW52b2ljZS1saXN0LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kseUJBQXlCO0VBQ3pCLFlBQVksRUFBQTs7QUFHaEI7O0VBRUksbUJBQW1CO0VBQ25CLFlBQVksRUFBQTs7QUFHaEI7RUFDSSx5QkFBeUIsRUFBQTs7QUFHN0I7RUFDSSw4QkFBNkIsRUFBQTs7QUFHaEM7RUFDRyw0QkFBMkIsRUFBQTs7QUFHOUI7RUFDRyx1QkFDSixFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvdWktZmVhdHVyZXMvaW52b2ljZXMvaW52b2ljZS1saXN0L2ludm9pY2UtbGlzdC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIm5iLWNhcmQtYm9keSB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0UxRjVGRTtcbiAgICBjb2xvcjogd2hpdGU7XG59XG5cbmNvbC14bC02LFxubmItY2FyZC1oZWFkZXIge1xuICAgIGJhY2tncm91bmQ6ICMxRDhFQ0U7XG4gICAgY29sb3I6IHdoaXRlO1xufVxuXG5uYi1jYXJkIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRTFGNUZFO1xufVxuXG46aG9zdCAvZGVlcC8gdGFibGUgdGJvZHkgdHI6bnRoLWNoaWxkKGV2ZW4pIHtcbiAgICBiYWNrZ3JvdW5kOiAjZTllYWVhIWltcG9ydGFudDtcbn1cblxuIDpob3N0IC9kZWVwLyB0YWJsZSB0Ym9keSB0cjpudGgtY2hpbGQob2RkKSB7XG4gICAgYmFja2dyb3VuZDogd2hpdGUhaW1wb3J0YW50O1xufVxuXG4gOmhvc3QgL2RlZXAvIG5nMi1zbWFydC10YWJsZSAubmcyLXNtYXJ0LXBhZ2luYXRpb24ge1xuICAgIGJhY2tncm91bmQtY29sb3I6IGJsYWNrXG59Il19 */"

/***/ }),

/***/ "./src/app/pages/ui-features/invoices/invoice-list/invoice-list.component.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/pages/ui-features/invoices/invoice-list/invoice-list.component.ts ***!
  \***********************************************************************************/
/*! exports provided: InvoiceListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InvoiceListComponent", function() { return InvoiceListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var _services_jobs_invoice_challans_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/jobs/invoice-challans.service */ "./src/app/services/jobs/invoice-challans.service.ts");
/* harmony import */ var _services_settings_settings_services_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/settings/settings-services.service */ "./src/app/services/settings/settings-services.service.ts");
/* harmony import */ var _services_token_token_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../services/token/token.service */ "./src/app/services/token/token.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






var InvoiceListComponent = /** @class */ (function () {
    function InvoiceListComponent(userServ, route, settingservice, invoices) {
        this.userServ = userServ;
        this.route = route;
        this.settingservice = settingservice;
        this.invoices = invoices;
        this.settings = {
            add: {
                addButtonContent: '<i class="nb-plus"></i>',
                createButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
            },
            edit: {
                editButtonContent: '<i class="nb-edit"></i>',
                saveButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
                confirmSave: true
            },
            delete: {
                deleteButtonContent: '<i class="nb-trash"></i>',
                confirmDelete: true,
            },
            view: {
                viewButtonContent: '<i  class="nb-edit"></i>'
            },
            actions: {
                position: 'right',
                edit: false,
                add: false,
                delete: false
            },
            columns: {
                invoiceNo: {
                    title: 'Invoice No',
                    type: 'number',
                    filter: true,
                },
                dateCreated: {
                    title: 'Date Created',
                    type: 'string',
                    valuePrepareFunction: function (value) {
                        return value.split('-').join('/');
                    },
                    filter: true
                },
                jobNo: {
                    title: 'Job No',
                    type: 'string',
                    filter: true
                },
            },
        };
        this.source = new ng2_smart_table__WEBPACK_IMPORTED_MODULE_2__["LocalDataSource"]();
        this.invoiceArray = [];
        this.invoiceArrayInvoice = [];
        this.invoiceArrayChallan = [];
        this.invoiceArray1 = [];
        this.displayType = 'Invoice';
        this.catagorys = ['Invoice', 'Challan'];
        this.display = false;
        this.port = this.settingservice.port;
    }
    InvoiceListComponent.prototype.ngOnInit = function () {
        this.getInvoice();
    };
    InvoiceListComponent.prototype.getInvoice = function () {
        return __awaiter(this, void 0, void 0, function () {
            var invoice, invoiceName;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.invoiceArray = [];
                        this.invoiceArrayChallan = [];
                        this.invoiceArrayInvoice = [];
                        return [4 /*yield*/, this.invoices.getAllInvoices()];
                    case 1:
                        invoice = _a.sent();
                        invoiceName = this.route.snapshot.paramMap.get('id');
                        invoice.forEach(function (ele) { return __awaiter(_this, void 0, void 0, function () {
                            var _this = this;
                            return __generator(this, function (_a) {
                                if (ele.stage1.length > 0) {
                                    ele.stage1.forEach(function (ele2) {
                                        Object.assign(ele2, { 'jobNo': ele.jobNo });
                                        if (ele2.fieldname === 'invoice_challan' && _this.displayType === 'Invoice') {
                                            Object.assign(ele2, { 'customerName': ele.customerName });
                                            _this.invoiceArrayInvoice.push(ele2);
                                        }
                                        if (ele2.fieldname === 'challan_invoice' && _this.displayType === 'Challan') {
                                            Object.assign(ele2, { 'customerName': ele.customerName });
                                            _this.invoiceArrayChallan.push(ele2);
                                        }
                                    });
                                }
                                if (ele.stage2.length > 0) {
                                    ele.stage2.forEach(function (ele3) {
                                        Object.assign(ele3, { 'jobNo': ele.jobNo });
                                        if (ele3.fieldname === 'invoice_challan' && _this.displayType === 'Invoice') {
                                            Object.assign(ele3, { 'customerName': ele.customerName });
                                            _this.invoiceArrayInvoice.push(ele3);
                                        }
                                        if (ele3.fieldname === 'challan_invoice' && _this.displayType === 'Challan') {
                                            Object.assign(ele3, { 'customerName': ele.customerName });
                                            _this.invoiceArrayChallan.push(ele3);
                                        }
                                    });
                                }
                                if (ele.stage3.length > 0) {
                                    ele.stage3.forEach(function (ele4) {
                                        Object.assign(ele4, { 'jobNo': ele.jobNo });
                                        if (ele4.fieldname === 'invoice_challan' && _this.displayType === 'Invoice') {
                                            Object.assign(ele4, { 'customerName': ele.customerName });
                                            _this.invoiceArrayInvoice.push(ele4);
                                        }
                                        if (ele4.fieldname === 'challan_invoice' && _this.displayType === 'Challan') {
                                            Object.assign(ele4, { 'customerName': ele.customerName });
                                            _this.invoiceArrayChallan.push(ele4);
                                        }
                                    });
                                }
                                if (ele.stageCommissioning.length > 0) {
                                    ele.stageCommissioning.forEach(function (ele5) {
                                        Object.assign(ele5, { 'jobNo': ele.jobNo });
                                        if (ele5.fieldname === 'invoice_challan' && _this.displayType === 'Invoice') {
                                            Object.assign(ele5, { 'customerName': ele.customerName });
                                            _this.invoiceArrayInvoice.push(ele5);
                                        }
                                        if (ele5.fieldname === 'challan_invoice' && _this.displayType === 'Challan') {
                                            Object.assign(ele5, { 'customerName': ele.customerName });
                                            _this.invoiceArrayChallan.push(ele5);
                                        }
                                    });
                                }
                                return [2 /*return*/];
                            });
                        }); });
                        if (this.displayType === 'Invoice') {
                            this.invoiceArray = this.invoiceArrayInvoice;
                        }
                        else {
                            this.invoiceArray = this.invoiceArrayChallan;
                        }
                        this.invoiceArray.sort(function (a, b) {
                            return a.invoiceNo - b.invoiceNo;
                        });
                        this.invoiceArray.reverse();
                        if (invoiceName) {
                            this.invoiceArray.forEach(function (ele) {
                                if (ele.filename === invoiceName) {
                                    _this.invoiceArray1.push(ele);
                                }
                            });
                            return [2 /*return*/, this.source.load(this.invoiceArray1)];
                        }
                        else {
                            return [2 /*return*/, this.source.load(this.invoiceArray)];
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    InvoiceListComponent.prototype.selectcategory = function (e) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.displayType = e;
                this.getInvoice();
                return [2 /*return*/];
            });
        });
    };
    InvoiceListComponent.prototype.onUserRowSelect = function (event) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.stage = event.data.stage;
                this.customerName = event.data.customerName;
                // this.filePath = `http://${location.hostname}:80/invoices/${event.data.filename}`
                // this.filePath = `http://${location.hostname}:4000/invoices/${event.data.filename}`
                this.filePath = "http://" + location.hostname + ":" + this.port + "/invoices/" + event.data.filename;
                this.display = true;
                return [2 /*return*/];
            });
        });
    };
    InvoiceListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-invoice-list',
            template: __webpack_require__(/*! ./invoice-list.component.html */ "./src/app/pages/ui-features/invoices/invoice-list/invoice-list.component.html"),
            styles: [__webpack_require__(/*! ./invoice-list.component.scss */ "./src/app/pages/ui-features/invoices/invoice-list/invoice-list.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_token_token_service__WEBPACK_IMPORTED_MODULE_5__["TokenService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _services_settings_settings_services_service__WEBPACK_IMPORTED_MODULE_4__["SettingsServicesService"],
            _services_jobs_invoice_challans_service__WEBPACK_IMPORTED_MODULE_3__["InvoiceChallansService"]])
    ], InvoiceListComponent);
    return InvoiceListComponent;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/invoices/invoices-routing.module.ts":
/*!***********************************************************************!*\
  !*** ./src/app/pages/ui-features/invoices/invoices-routing.module.ts ***!
  \***********************************************************************/
/*! exports provided: InvoicesRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InvoicesRoutingModule", function() { return InvoicesRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _invoices_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./invoices.component */ "./src/app/pages/ui-features/invoices/invoices.component.ts");
/* harmony import */ var _invoice_list_invoice_list_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./invoice-list/invoice-list.component */ "./src/app/pages/ui-features/invoices/invoice-list/invoice-list.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var routes = [{
        path: '',
        component: _invoices_component__WEBPACK_IMPORTED_MODULE_2__["InvoicesComponent"],
        children: [
            {
                path: 'Invoices',
                component: _invoice_list_invoice_list_component__WEBPACK_IMPORTED_MODULE_3__["InvoiceListComponent"],
            },
            {
                path: 'Invoices/:id',
                component: _invoice_list_invoice_list_component__WEBPACK_IMPORTED_MODULE_3__["InvoiceListComponent"],
            }
        ]
    }];
var InvoicesRoutingModule = /** @class */ (function () {
    function InvoicesRoutingModule() {
    }
    InvoicesRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], InvoicesRoutingModule);
    return InvoicesRoutingModule;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/invoices/invoices.component.html":
/*!********************************************************************!*\
  !*** ./src/app/pages/ui-features/invoices/invoices.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/pages/ui-features/invoices/invoices.component.scss":
/*!********************************************************************!*\
  !*** ./src/app/pages/ui-features/invoices/invoices.component.scss ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VpLWZlYXR1cmVzL2ludm9pY2VzL2ludm9pY2VzLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/pages/ui-features/invoices/invoices.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/pages/ui-features/invoices/invoices.component.ts ***!
  \******************************************************************/
/*! exports provided: InvoicesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InvoicesComponent", function() { return InvoicesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var InvoicesComponent = /** @class */ (function () {
    function InvoicesComponent() {
    }
    InvoicesComponent.prototype.ngOnInit = function () {
    };
    InvoicesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngx-invoices',
            template: __webpack_require__(/*! ./invoices.component.html */ "./src/app/pages/ui-features/invoices/invoices.component.html"),
            styles: [__webpack_require__(/*! ./invoices.component.scss */ "./src/app/pages/ui-features/invoices/invoices.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], InvoicesComponent);
    return InvoicesComponent;
}());



/***/ }),

/***/ "./src/app/pages/ui-features/invoices/invoices.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/ui-features/invoices/invoices.module.ts ***!
  \***************************************************************/
/*! exports provided: InvoicesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InvoicesModule", function() { return InvoicesModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var _nebular_theme__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @nebular/theme */ "./node_modules/@nebular/theme/index.js");
/* harmony import */ var ngx_extended_pdf_viewer__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-extended-pdf-viewer */ "./node_modules/ngx-extended-pdf-viewer/fesm5/ngx-extended-pdf-viewer.js");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var _invoices_routing_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./invoices-routing.module */ "./src/app/pages/ui-features/invoices/invoices-routing.module.ts");
/* harmony import */ var _invoices_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./invoices.component */ "./src/app/pages/ui-features/invoices/invoices.component.ts");
/* harmony import */ var _invoice_list_invoice_list_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./invoice-list/invoice-list.component */ "./src/app/pages/ui-features/invoices/invoice-list/invoice-list.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var InvoicesModule = /** @class */ (function () {
    function InvoicesModule() {
    }
    InvoicesModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_invoices_component__WEBPACK_IMPORTED_MODULE_8__["InvoicesComponent"], _invoice_list_invoice_list_component__WEBPACK_IMPORTED_MODULE_9__["InvoiceListComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _invoices_routing_module__WEBPACK_IMPORTED_MODULE_7__["InvoicesRoutingModule"],
                ng2_smart_table__WEBPACK_IMPORTED_MODULE_6__["Ng2SmartTableModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"],
                _nebular_theme__WEBPACK_IMPORTED_MODULE_4__["NbCardModule"],
                ngx_extended_pdf_viewer__WEBPACK_IMPORTED_MODULE_5__["NgxExtendedPdfViewerModule"]
            ]
        })
    ], InvoicesModule);
    return InvoicesModule;
}());



/***/ })

}]);
//# sourceMappingURL=default~app-pages-pages-module~invoices-invoices-module~ui-features-ui-features-module.js.map