(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["inventory-inventory-module"],{

/***/ "./src/app/services/Inventory/inventory.service.ts":
/*!*********************************************************!*\
  !*** ./src/app/services/Inventory/inventory.service.ts ***!
  \*********************************************************/
/*! exports provided: InventoryService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InventoryService", function() { return InventoryService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _settings_settings_services_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../settings/settings-services.service */ "./src/app/services/settings/settings-services.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var InventoryService = /** @class */ (function () {
    function InventoryService(http, settings) {
        this.http = http;
        this.settings = settings;
        this.userInventory = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]({});
        this.API_URL = this.settings.BASE_API_URL;
    }
    InventoryService.prototype.createInventory = function (data) {
        var _this = this;
        var url = this.API_URL + "/inventory/inventory-details";
        return new Promise(function (resolve, reject) {
            _this.http.post(url, data)
                .subscribe(resolve, reject);
        });
    };
    InventoryService.prototype.getInventory = function () {
        var _this = this;
        var url = this.API_URL + "/inventory/inventory-details";
        return new Promise(function (resolve, reject) {
            _this.http.get(url)
                .subscribe(resolve, reject);
        });
    };
    InventoryService.prototype.UpdateInventoryDetails = function (value, id) {
        var _this = this;
        // console.log(value, id)
        var url = this.API_URL + "/inventory/inventory-details/" + id;
        return new Promise(function (resolve, reject) {
            _this.http.put(url, value)
                .subscribe(resolve, reject);
        });
    };
    InventoryService.prototype.getInventoryByID = function (id) {
        var _this = this;
        var url = this.API_URL + "/inventory/inventory-details/" + id;
        return new Promise(function (resolve, reject) {
            _this.http.get(url)
                .subscribe(resolve, reject);
        });
    };
    InventoryService.prototype.deleteInventoryByID = function (id) {
        var _this = this;
        var url = this.API_URL + "/inventory/inventory-details/" + id;
        return new Promise(function (resolve, reject) {
            _this.http.delete(url)
                .subscribe(resolve, reject);
        });
    };
    InventoryService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _settings_settings_services_service__WEBPACK_IMPORTED_MODULE_3__["SettingsServicesService"]])
    ], InventoryService);
    return InventoryService;
}());



/***/ }),

/***/ "./src/app/services/suppliers/supplier.service.ts":
/*!********************************************************!*\
  !*** ./src/app/services/suppliers/supplier.service.ts ***!
  \********************************************************/
/*! exports provided: SupplierService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SupplierService", function() { return SupplierService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _settings_settings_services_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../settings/settings-services.service */ "./src/app/services/settings/settings-services.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SupplierService = /** @class */ (function () {
    function SupplierService(http, settings) {
        this.http = http;
        this.settings = settings;
        this.suppliers = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]({});
        this.API_URL = this.settings.BASE_API_URL;
    }
    SupplierService.prototype.createSupplier = function (data) {
        var _this = this;
        var url = this.API_URL + "/suppliers/suppliers-details";
        return new Promise(function (resolve, reject) {
            _this.http.post(url, data)
                .subscribe(resolve, reject);
        });
    };
    SupplierService.prototype.getSupplier = function () {
        var _this = this;
        var url = this.API_URL + "/suppliers/suppliers-details";
        return new Promise(function (resolve, reject) {
            _this.http.get(url)
                .subscribe(resolve, reject);
        });
    };
    // Vipin Code
    SupplierService.prototype.updateSupplierItems = function (value, id) {
        var _this = this;
        var url = this.API_URL + "/suppliers/suppliers-details/items/" + id;
        return new Promise(function (resolve, reject) {
            _this.http.put(url, value)
                .subscribe(resolve, reject);
        });
    };
    SupplierService.prototype.addSupplierItems = function (value, id) {
        var _this = this;
        var url = this.API_URL + "/suppliers/suppliers-details/add/" + id;
        return new Promise(function (resolve, reject) {
            _this.http.put(url, value)
                .subscribe(resolve, reject);
        });
    };
    // Vipin Code
    SupplierService.prototype.UpdateSupplierDetails = function (value, id) {
        var _this = this;
        var url = this.API_URL + "/suppliers/suppliers-details/" + id;
        return new Promise(function (resolve, reject) {
            _this.http.put(url, value)
                .subscribe(resolve, reject);
        });
    };
    SupplierService.prototype.getSupplierByID = function (id) {
        var _this = this;
        var url = this.API_URL + "/suppliers/suppliers-details/" + id;
        return new Promise(function (resolve, reject) {
            _this.http.get(url)
                .subscribe(resolve, reject);
        });
    };
    SupplierService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _settings_settings_services_service__WEBPACK_IMPORTED_MODULE_3__["SettingsServicesService"]])
    ], SupplierService);
    return SupplierService;
}());



/***/ })

}]);
//# sourceMappingURL=inventory-inventory-module.js.map